package com.ucatch.fastservice.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ucatch.fastservice.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class joblist extends Fragment {


    public joblist() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_joblst, container, false);

        return view;
    }

}
