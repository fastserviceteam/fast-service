<?php

/**
 * Class Admin
 * handles all admin queries
 */

class AdminClass
{

    /**
    * @var object $conn PDO database variable
    */
    private $conn = null;
    /**
    * @var object $db MySQLi Procedural database variable
    */
    private $db   = null;
    /**
    * @var array $errors error messages
    */    
    private $errors = array();
    /**
    * @var array $mgss success messages
    */
    private $mgss = array();
    /**
    * @var boolean $isadminLoggedIn checking login status
    */
    private $isadminLoggedIn = false;
    /**
    * @var object $admin_loginId set admin pkey as id
    */
    private $admin_loginId = null;
    /**
    * @var object $fastService_fadminEmail_Session admin email session var
    */
    private $fastService_fadminEmail_Session = "";
    /**
    * @var object $fastService_tadminEmail_Session admin email (@domain truncated)
    */
    private $fastService_tadminEmail_Session = "";
    /**
    * @var object $fastService_adminPriviledge admin priviledge
    */
    private $fastService_adminPriviledge = "";
    /**
    * @var object $cookie_key cookie key
    */
    private $cookie_key = "$2y$10$6NSuXqiKw/uPUglAIsZdRObVVDK4rjXHr";
    /**
    * @var string $pic_blank blank image
    */
    private $pic_blank = "pic_blank.jpg";
    /**
    * @var string $clientSideBannerDir advert banners upload directory
    */
    private $clientSideBannerDir = "/client/_assets/clientUploads/";
    /**
    * @var string $clientProfilePicDir client profile picture
    */
    private $clientProfilePicDir = "/client/_assets/clientUploads/profile/";


    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$registration = new Registration();"
     */
    public function __construct()
    {   

        $this->dbConnectPDO(); // pdo connection
        $this->dbConnectMysqliProced(); // mysqli procedural connection

        if (isset($_GET['logout']))
        {
            $this->_adminlogout();
        }
        
        elseif (isset($_SESSION['fastService_fadminEmail_Session'])) 
        {
            $this->loginWithSessionData();
        } 
        
        elseif (isset($_COOKIE['fastservice_data'])) 
        {
            $this->loginWithCookieData();        
        }
        
    } // end

    /**
     * this method --WILL-- check whether the're any jobs and adverts which have 
     * expired and ends their run
     *
     * @return array $advertsjobsExpiryList a json array of expired jobs and adverts which we show as a 
     *                                     notification to admin
     * @access public
     */
    function checkAdvertsJobsExpiry() {

        $advertsjobsExpiryList = json_encode(array("advertsTokens" => 1, "jobsToken" => 2));

        return $advertsjobsExpiryList;
    } // end


    /**
     * database connection - PDO
     *
     * @return void
     * @access private
     */
    private function dbConnectPDO()
    {
        if($this->conn != null)
        {
            return true;
        }
        else
        {
            try
            {
                $this->conn = new PDO('mysql:host='. DB_HOST .';dbname='. DB_DATABASE . ';charset=utf8', DB_USER, DB_PASSWORD);
                // set the PDO error mode to exception
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $e)
            {
                $this->errs[] = "Connection failed: " . $e->getMessage();
            }
        }

    } // @endof dbConnect method

    /**
     * database connection - MySQLi Procedural
     */
    private function dbConnectMysqliProced()
    {
        if($this->db != null)
        {
            return true;
        }
        else
        {
            $this->db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die("Connection failed: " . mysqli_connect_error());
        }
    } // Mysqli procedural
    
        
    /**
     * admin login
     *
     * @param string $fastserviceEmail email address
     * @param string $fastservicePriviledge access priviledge
     * @param string $fastservicePassword password
     * @param string $fastservice_remember_me remember me
     */
    function adminLoginRequest($fastserviceEmail, $fastservicePriviledge, $fastservicePassword, $fastservice_remember_me)
    {
        
        if(empty($fastserviceEmail))
        {
             $this->errors[] = EMPTY_EMAIL; 
        }
        elseif(empty($fastservicePriviledge))
        {
             $this->errors[] = EMPTY_PRIVILEDGE;
        }
        elseif(empty($fastservicePassword))
        {
             $this->errors[] = EMPTY_ADMIN_PWD;
        }
        elseif(strlen($fastservicePassword) < 8)
        {
            $this->errors[] = MIN_PASSWORD_LENGTH;
        }
        elseif(!filter_var($fastserviceEmail, FILTER_VALIDATE_EMAIL))
        {
            $this->errors[] = INVALID_EMAIL_ADDRESS;
        }
        else
        {
            // check email and password
            $g_q1 = $this->conn->prepare("select admin_id, admin_email, admin_priviledge, admin_password FROM admin_login where admin_email = :admin_email");
            $g_q1->execute([
                "admin_email" => $fastserviceEmail
            ]);
            $g1 = $g_q1->fetchObject();
            
            // check email
            if($g1)
            {
                // check access level
                if($g1->admin_priviledge !== $fastservicePriviledge)
                {
                    $this->errors[] = WRONG_PRIVILEDGE;
                }
                else
                {
                
                // check pasword
                if(password_verify($fastservicePassword, $g1->admin_password))
                {
                    
                    
                    // login option - remembe me
                    if(!empty($fastservice_remember_me))
                    {
                        $this->rememberMeDataLogin($g1->admin_email, $g1->admin_id);
                    }
                    else
                    {
                        $this->delRememberMeCookie();
                    }
                    
                    // assign session variables
                    $_SESSION["fastService_fadminEmail_Session"] = $g1->admin_email; 
                    $_SESSION["fastService_fadminId_Session"]    = $g1->admin_id; 
                    $_SESSION["fastService_adminPriviledge"]     = $g1->admin_priviledge;
                    $_SESSION["admin_loginStatus"] = 1; 

                    // --- >> extract name from user email address
                    $emailAddressUName = strstr($g1->admin_email, '@', true);
                    $_SESSION['fastService_uadminEmail_Session'] = $emailAddressUName;

                    $this->admin_loginId    = $g1->admin_id;
                    $this->fastService_fadminEmail_Session = $g1->admin_email;
                    $this->fastService_uadminEmail_Session = $emailAddressUName;
                    $this->fastService_adminPriviledge     = $g1->admin_priviledge;


                    $this->isadminLoggedIn = true;
                    
                    
                $this->mgss[] = "Info: login successful; if you are automatically redirected, click <a href='a_dashboard.php' class='text-maroon'>here!</a>";
                    
                                    ?>
<script>
    // --- >> redirect
    $(function(){
        //$("#loginform").slideUp();
        setTimeout('window.location.href = "a_dashboard.php"; ', 2000);
    });
</script>

                <?php
                    
                } // right password
                else
                {
                     $this->errors[] = WRONG_PASSWORD;
                } // wrong password
                
            } // right priviledge
                
            } // right email
            else
            {
                $this->errors[] = INTERNAL_ERROR;
            } // wrong email
            
        } // else
        
    } // adminLoginRequest

    
    /**
     * login with post data
     */
    private function loginWithSessionData()
    {        
        $this->fastService_fadminEmail_Session = $_SESSION['fastService_fadminEmail_Session'];
        $this->fastService_uadminEmail_Session = $_SESSION['fastService_uadminEmail_Session'];
        $this->fastService_adminPriviledge     = $_SESSION["fastService_adminPriviledge"];

        $this->isadminLoggedIn = true;
    } // end


    /**
     * login with cookie
     */
    private function loginWithCookieData()
    {
    if (isset($_COOKIE['fastservice_data'])) {

        list ($admin_id, $token, $hash) = explode(':', $_COOKIE['fastservice_data']);


        if ($hash == hash('sha256', $admin_id . ':' . $token . $this->cookie_key) && !empty($token)) 
        {        

            $sth = $this->conn->prepare("select * from admin_login where admin_id = :admin_id and remember_token = :remember_token");
            $sth->execute([
                'admin_id' => $admin_id,
                'remember_token' => $token
            ]);

            $result_row = $sth->fetchObject();

            if ($result_row) 
            {                 
                // assign session variables
                $_SESSION["fastService_fadminEmail_Session"] = $result_row->admin_email; 
                $_SESSION["fastService_fadminId_Session"]    = $result_row->admin_id; 
                $_SESSION["fastService_adminPriviledge"]     = $result_row->admin_priviledge;
                $_SESSION["admin_loginStatus"] = 1; 

                // --- >> extract name from user email address
                $emailAddressUName = strstr($result_row->admin_email, '@', true);
                $_SESSION['fastService_uadminEmail_Session'] = $emailAddressUName;

                $this->admin_loginId    = $result_row->admin_id;
                $this->fastService_fadminEmail_Session = $result_row->admin_email;
                $this->fastService_uadminEmail_Session = $emailAddressUName;
                $this->fastService_adminPriviledge     = $result_row->admin_priviledge;

                $this->isadminLoggedIn = true;

                $this->rememberMeDataLogin($result_row->admin_email, $result_row->admin_id);
                return true;
            }
        }
        $this->delRememberMeCookie();
        $this->errors[] = MESSAGE_COOKIE_INVALID;
    }
    return false;
    }


    /**
     * add remember me
     * @param $adminEmailAddress email address
     * @param $adminPkeyId primary key of table
     */
    private function rememberMeDataLogin($adminEmailAddress, $adminPkeyId)
    {
        $random_token_string = hash('sha256', mt_rand());

        $i_q3 = $this->conn->prepare("UPDATE admin_login SET remember_token = :remember_token WHERE admin_email = :admin_email and admin_id = :admin_id");
        $i3 = $i_q3->execute([
            'remember_token' => $random_token_string,
            'admin_email' => $adminEmailAddress,
            'admin_id' => $adminPkeyId
        ]);

        if ($i3) 
        {

            $cookie_string_first_part = $adminPkeyId . ':' . $random_token_string;
            $cookie_string_hash       = hash('sha256', $cookie_string_first_part . $this->cookie_key);
            $cookie_string            = $cookie_string_first_part . ':' . $cookie_string_hash;

            setcookie('fastservice_data', $cookie_string, time() + COOKIE_RUNTIME, "/");
        }
    } // rememberMeDataLogin    
    

    /**
     * delete remember me
     */
    private function delRememberMeCookie()
    {
        $d_q1 = $this->conn->prepare("UPDATE admin_login SET remember_token = NULL WHERE admin_id = :admin_id");
        $d1   = $d_q1->execute([
            'admin_id' => $_SESSION['fastService_fadminId_Session']
        ]);
        if ($d1) 
        {
           setcookie('fastservice_data', false, time() - (3600 * 3650), "/");
        }
    } // delete rememberme cookie 
    

    /**
     * is logged in
     *
     * @return bool
    */
    function _isLoggedIn()
    {	
        if (isset($_SESSION['admin_loginStatus']) AND $_SESSION['admin_loginStatus'] == 1) {            
            return true;
        }
        // default return
        return false;
        
    } // _isLoggedIn
    

    /**
     * logout
     */
    function _adminlogout()
    {           
        $this->delRememberMeCookie();
        
        unset($_SESSION["fastService_fadminEmail_Session"]);        
        unset($_SESSION["fastService_fadminId_Session"]);
        unset($_SESSION["admin_loginStatus"]);   
        
        $this->isadminLoggedIn = false;

    } // --- >> @enof logout method


    // -------------------- messages ~ START -------------------- //

    /**
     * new messages list
     */
    function _adminMessages() {

        $f_q1 = $this->conn->prepare($this->_getInbox()." where inbox_email = :inbox_email and msg_read = :msg_read ORDER BY inbox_date_time DESC LIMIT 5");
        $f_q1->execute([
            "inbox_email" => FAST_SERVICE_ADMIN_EMAIL,
            "msg_read" => 0
        ]);
        $f1 = $f_q1->fetchAll();
        if ($f1) {
            ?>
        <ul>
            <?php

            // does user have profile picture
            $get_pp_q = $this->conn->prepare("select profile_picture from user_profile where ur_email = :ur_email");

            foreach ($f1 as $row) {

                $get_pp_q->execute([
                    "ur_email" => $row['sender_email']
                ]);

                $get_pp = $get_pp_q->fetchObject();

                $profilePicCheck = $get_pp->profile_picture != NULL ? $get_pp->profile_picture : "user.png";

                $profile = $this->clientProfilePicDir.$profilePicCheck;

                // variables
                $msg_id = $row['inbox_id'];
            ?>
            <li>
                <a href="javascript:void(0)" class="clearfix" onclick="showthisMessagefunc('<?php echo $msg_id; ?>')">
                    <figure class="image">
                        <img src="<?php echo $profile;?>" class="img-circle" style="width: 35px; height: 35px">
                    </figure>
                    <span class="title">From: <?php  echo $row['sender_names']; ?></span>
                    <span class="message">Subj: <?php  echo $row['inbox_subject']; ?> </span>
                </a>
            </li>

            <?php
            } // foreach loop
            ?>
        </ul>
        <hr>
    <div class="text-center">
        <a href="javascript:void(0)" onclick="loadTemplateView('mail')" class="view-more blue-text">View All</a>
    </div>

            <?php
        } // messages are available
        else {
            echo "<p class='blue-text text-center t-12'>No messages available!</p>";
        }
    } // end


    /** 
     * shown number of new messages 
     * in floating and label badge
     */
    function _messagesBadgeCounter() {

        $g_q3 = mysqli_query($this->db, $this->_getInbox()." where inbox_email = '".FAST_SERVICE_ADMIN_EMAIL."' and msg_read = 0");
        if (mysqli_num_rows($g_q3) > 0) {
            echo mysqli_num_rows($g_q3);
        } else {
            echo 0;
        } // else
        
    } // end


    /**
     * messages page / section
     */
    function _messagesSection() {

        $f_q1 = $this->conn->prepare("select inbox_id, inbox_email, inbox_subject, inbox_message, inbox_date_time, inbox_status, sender_names, sender_email, msg_read from msg_inbox where inbox_email = :inbox_email and msg_read = :msg_read ORDER BY inbox_date_time DESC");
        $f_q1->execute([
            "inbox_email" => FAST_SERVICE_ADMIN_EMAIL,
            "msg_read" => 0
        ]);
        $f1 = $f_q1->fetchAll();
        
        return $f1;
    } // end

    // -------------------- messages ~ END -------------------- //


    /**
     * total counter
     * @return integer
     */
    function _totalNotifnCounter()
    {
        $alerts_q1 = mysqli_query($this->db, $this->_getJobs()." where approved = 'no'");
        $alerts1   = mysqli_num_rows($alerts_q1);
        
        $alerts_q2 = mysqli_query($this->db, $this->_getAdverts()." where approved = 'no' and cancelled = 'no'");
        $alerts2   = mysqli_num_rows($alerts_q2);
 
        return $alerts1 + $alerts2;
        
    } // end


    /**
     * adverts + jobs alerts counter
     * 
     * @return array
     */
    function _advertsjobsAlerts()
    {

        $jobsAlerts_q1 = mysqli_query($this->db, $this->_getJobs()." where approved = 'no' order by date_posted desc");
        $jobsAlerts1   = mysqli_num_rows($jobsAlerts_q1);

        $jobsAlerts = $jobsAlerts1 > 0 ? $jobsAlerts1 : 0;

        $advertAlerts_q1 = mysqli_query($this->db, $this->_getAdverts()." where approved = 'no' and cancelled = 'no' order by date_post desc");
        $advertAlerts1   = mysqli_num_rows($advertAlerts_q1);

        $advertAlerts = $advertAlerts1 > 0 ? $advertAlerts1 : 0;

        return json_encode(array("jobsCount" => $jobsAlerts, "advertsCount" => $advertAlerts));

    } // end

    
    // -------------------- >> ADVERTS STARTS << -------------------- //    
    
    /** 
     * adverts alerts counter requiring approval; 
     * this is displayed on notification bar and also on left side navigation bar
     *
     * @return int $alerts2 if adverts requiring approval exists; 0 if none exists
     * @access public
     */
    function _advertAlert()
    {
        $alerts_q2 = mysqli_query($this->db, $this->_getAdverts()." where approved = 'no' order by date_post desc");
        $alerts2   = mysqli_num_rows($alerts_q2);
        
        if ($alerts2 > 0) 
        {
            return $alerts2;
        }
        else
        {
            return 0;
        } 
    } // _advertAlert counter

    /**
     * advert list requiring approval
     */
    function _approveAdvertListModal()
    {

        $g_q2 = $this->conn->prepare("select business_profile_id, advert_post_id, advert_serial, advert_title, date_format(start_period, '%d %M, %Y') as sdate, date_format(end_period, '%d %M, %Y') as edate, date_format(date_post, '%d %M, %Y') as dateP, primary_banner_1 from advert_posts where approved = :approved and cancelled = :cancelled order by date_post desc");
        $g_q2->execute([
            "approved" => "no",
            "cancelled" => "no"
        ]);
        $g2 = $g_q2->fetchAll();
            
        if ($g2) 
        {
            ?>
        <div id="advertapprovalDiv">
            <table class="table table-hover">

                <thead class="text-blue">

                    <th>
                        Business
                    </th>
                    <th>
                        Advert Title
                    </th>
                    <th>
                        Date Posted
                    </th>
                    <!--
                    <th>
                        Start Date
                    </th>
                    <th>
                        End Date
                    </th>
                    -->
                    <th colspan="3" class="text-center">
                        Action
                    </th>
                </thead>

                <tbody class="text-blue-dark">

                    <?php 
            // get business name
            $bname_q = $this->conn->prepare("select Business_Name from business_profile where business_profile_id = :business_profile_id");
            
            foreach($g2 as $r) { 
                    
                $bname_q->execute([
                    "business_profile_id" => $r['business_profile_id']
                ]);

                $bname = $bname_q->fetchObject();   

                $bannersCheck    = $r['primary_banner_1'] != NULL ? "yes" : "no";             

                    
            ?>
                    <tr id="advert_approval_tr<?php echo $r['advert_post_id']; ?>">
                        <td>
                            <?php echo $bname ? $bname->Business_Name  : 'N/A'; ?>
                        </td>
                        <td>
                            <?php echo $r['advert_title']; ?>
                        </td>
                        <td>
                            <?php echo $r['dateP']; ?>
                        </td>
                        <!--
                        <td>
                            <?php # echo $r['sdate']; ?>
                        </td>
                        <td>
                            <?php # echo $r['edate']; ?>
                        </td>
                        -->
                        <td>

                            <a href="javascript:void(0)" title="view advert details" class="green-text"><i class="fa fa-eye approvalIcons t-20" onclick="switchToAdvertDetails('<?php echo $r['advert_post_id']; ?>', '<?php echo $bannersCheck; ?>')"></i></a>
                        </td>
                        <td>
                            <a href="javascript:void(0)" title="approve advert" class="purple-text" onclick="approveAdvertfunc('<?php echo $r['advert_post_id']; ?>')" id="advert_approval_td<?php echo $r['advert_post_id']; ?>"><i class="fa fa-check-circle-o approvalIcons t-20"></i></a>
                        </td>
                        <td>
                            <a href="javascript:void(0)" title="cancel advert" class="red-text" onclick="cancelAdvertfunc('<?php echo $r['advert_post_id']; ?>', '<?php echo $r['advert_serial']; ?>', 2)"><i class="fa fa-times-circle-o approvalIcons t-20"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            
        </div>
          <!-- advertapprovalDiv -->

           
       <div id="advertDetailsDiv" class="display-none">

       </div>
        <!-- advertDetailsDiv -->

           
       <div id="CancelAdvertFormDiv" class="display-none">

       </div>
        <!-- advertDetailsDiv -->

            
            <?php
        }
        else {
            echo "<p class='text-center text-info'>Info: No adverts available for approval!</p>";
        }

    } // _approveAdvertListModal


    /**
     * cancel advert form
     *
     * @param int $canceladvertPkey advert tbl primary key
     * @param string $canceladvertSerial advert serial token
     * @param int $advertView advert view id to differentiate between approval list and mgmt view
     * @return void
     * @access public
     */
    function _cancelAdvertForm($canceladvertPkey, $canceladvertSerial, $advertView) {

        $cancelAdvert_q = $this->conn->prepare($this->_getAdverts()." where advert_post_id = :advert_post_id and advert_serial = :advert_serial");
        $cancelAdvert_q->execute([
            "advert_post_id" => $canceladvertPkey,
            "advert_serial" => $canceladvertSerial
        ]);
        $cancelAdvert = $cancelAdvert_q->fetchObject();

        if ($cancelAdvert) {

            if ($cancelAdvert->cancelled == '') {
     
            ?>
    <div class="row" id="cancelAdvert_wrapper">
        <div class="col-md-12">
            <!--<span id="cancel-advert-span"></span>-->

            <form role="form" id="cancelAdvertForm">

                <!-- hidden -->
                <input type="hidden" value="<?php echo $canceladvertPkey; ?>" name="dataCancel_advertPkey" id="dataCancel_advertPkey">
                <input type="hidden" value="<?php echo $canceladvertSerial; ?>" name="dataCancel_advertSerial" id="dataCancel_advertSerial">
                <input type="hidden" value="<?php echo $cancelAdvert->loggedEmail; ?>" name="dataCancel_advertEmail" id="dataCancel_advertEmail">
                <input type="hidden" value="<?php echo $cancelAdvert->advert_title; ?>" name="dataCancel_advertTitle" id="dataCancel_advertTitle">
                <input type="hidden" value="<?php echo $advertView; ?>" name="dataCancel_advertView" id="dataCancel_advertView">

                <!-- fields -->
                <div class="form-group">
                    <label for="dataCancel_AdvertReason">Reason - <small class="text-warning">150 characters for each reason</small></label>
                    <textarea name="dataCancel_AdvertReason[]" id="dataCancel_AdvertReason" placeholder="enter reason for advert cancelling" maxlength="150" class="form-control" cols="2"></textarea>
                </div>
                <div class="form-group" id="append_reasons_div">
                </div>

                <div class="form-group">
                    <button type="button" id="add_advert_creason" class="btn btn-xs btn-flat bg-teal">Add</button>
                    <button type="button" id="remove_advert_creason" class="btn btn-xs btn-flat bg-red">Remove</button>            
                </div>

                <div class="form-group">
                    <div class="text-right">
                        <button type="button" class="btn btn-flat bg-olive" onclick="cancelAdvertfunction()" id="btnCancelAdvert">Submit</button>                
                    </div>
                </div>
            </form>
        </div>
    </div>        

<?php if ($advertView == 2) {
?>
<hr>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-xs bg-blue btn-flat" id="backtoAdvertApprovaltblBtn" onclick="switchToAdvertList()"><i class="fa fa-angle-left"></i> Back</button>
        </div>
    </div>
<?php } ?>


    <script>
        $(function(){

            // --- >> dynamic additions of inputs for advert cancelling << ---
            var inputCount = 1;
            $("#add_advert_creason").on("click", function() {  
                $("#append_reasons_div").append('<div class="form-group"><textarea name="dataCancel_AdvertReason[]" placeholder="enter reason for advert cancelling" maxlength="150" class="form-control" cols="1"></textarea></div>');  
            });  
            $("#remove_advert_creason").on("click", function() {  
                $("#append_reasons_div").children().last().remove();  
            }); 
        });
    </script>

            <?php
        
            } // advert is being cancelled
            else {
                ?>
                <form id="undoAdvertCancellingForm">
                    <!-- hidden -->
                    <input type="hidden" value="<?php echo $canceladvertPkey; ?>" id="undoCancel_advertPkey">
                    <input type="hidden" value="<?php echo $canceladvertSerial; ?>" id="undoCancel_advertSerial">
                    <input type="hidden" value="<?php echo $cancelAdvert->loggedEmail; ?>" id="undoCancel_advertEmail">
                    <input type="hidden" value="<?php echo $cancelAdvert->advert_title; ?>" id="undoCancel_advertTitle">
                    <input type="hidden" value="<?php echo $advertView; ?>" id="undoCancel_advertView">

                    <div class="form-group text-center">
                        <button type="button" onclick="undoAdvertCancelfunc()" class="btn btn-flat blue-gradient text-white"><i class="fa fa-undo"></i> Undo Advert Cancelling</button>
                    </div>
                </form>
                <span id="undo-feedback"></span>
            <?php
            } // uncancelling advert

        } // query ok

    } // end


    /**
     * cancel advert saving
     *
     * @param int $dataCancel_advertPkey advert tbl primary key
     * @param string $dataCancel_advertSerial advert serial token
     * @param array $dataCancel_AdvertReason reason for cancelling advert
     * @param string $dataCancel_advertEmail email for person who created the advert
     * @param string $dataCancel_advertTitle advert title
     */
    function _cancelAdvertSave($dataCancel_advertPkey, $dataCancel_advertSerial, $dataCancel_AdvertReason, $dataCancel_advertEmail, $dataCancel_advertTitle) {

        $bool = false;

        // save cancelled advert
        $save_cancelAdvert_q = $this->conn->prepare("insert into cancelled_adverts (advert_post_id, advert_serial, reason) values (:advert_post_id, :advert_serial, :reason)");

        // update advert_posts tbl
        $update_advert_tbl_q = $this->conn->prepare("update advert_posts set cancelled = :cancelled where advert_post_id = :advert_post_id and advert_serial = :advert_serial");
        $update_advert_tbl   = $update_advert_tbl_q->execute([
            "cancelled" => "yes",
            "advert_post_id" => $dataCancel_advertPkey,
            "advert_serial" => $dataCancel_advertSerial
        ]); 

        // we're going to concat the advert cancelling reasons array into one string separated by commas
        $reasons = "";

        // run foreach loop
        foreach ($dataCancel_AdvertReason as $key => $value) {
            $save_cancelAdvert   = $save_cancelAdvert_q->execute([
                "advert_post_id" => $dataCancel_advertPkey,
                "advert_serial" => $dataCancel_advertSerial,
                "reason" => trim($value)
            ]); 

            $reasons .=  trim($value).", ";

            if ($save_cancelAdvert) {
                 $bool = true;
             } else {
                 $bool = false;
             }         
        } // loop

        if ($bool && $update_advert_tbl && $this->_advertcancellingNotifn($dataCancel_advertEmail, $reasons, $dataCancel_advertTitle)) {
            echo 1;
        } else {
            echo 2;
        } // else

    } // end

    /**
     * send advert cancelling notification
     *
     * @param string $dataCancel_advertEmail email for person who created the advert
     * @param string $reasons reason for cancelling advert
     * @param string $dataCancel_advertTitle advert title
     *
     * @return bool
     */
    private function _advertcancellingNotifn($dataCancel_advertEmail, $reasons, $dataCancel_advertTitle) {
        
        $msg_body = "<p>Your advert titled <strong>".$dataCancel_advertTitle."</strong>, has been <b class='text-danger'>Cancelled</b> because it failed to meet one or more of our Regulations and Standards as Below.</p>";
        $msg_body .= "<p>".$reasons."</p>";
        
        $emailNotifn_q = $this->conn->prepare("insert into msg_inbox (
            inbox_email,
            inbox_subject,
            inbox_message,
            sender_names,
            sender_email) 
        values (
            :inbox_email,
            :inbox_subject,
            :inbox_message,
            :sender_names,
            :sender_email           
            )");
        $emailNotifn = $emailNotifn_q->execute([
            "inbox_email" => $dataCancel_advertEmail,
            "inbox_subject" => "Advert Notice",
            "inbox_message" => $msg_body,
            "sender_names" => "Fast Service",
            "sender_email" => "admin.fastservice@donot.reply.com"
        ]);
        
        if($emailNotifn) {
            return true;
        } else {
            return false;
        } // else

    } // end

    /**
     * undo advert cancelling
     *
     * @param $undoadvertCancelPkey advert tbl primary key
     * @param $undoadvertCancelSerial advert serial token
     * @param $advertView advert view id 1 or 2
     */
    function _undoAdvertCancelling($undoadvertCancelPkey, $undoadvertCancelSerial, $advertView) {

        // undo cancelled advert
        $undo_cancelAdvert_q = $this->conn->prepare("delete from cancelled_adverts where advert_post_id = :advert_post_id and advert_serial = :advert_serial");
        $undo_cancelAdvert   = $undo_cancelAdvert_q->execute([
            "advert_post_id" => $undoadvertCancelPkey,
            "advert_serial" => $undoadvertCancelSerial
        ]); 

        // update advert_posts tbl
        $update_advert_tbl_q = $this->conn->prepare("update advert_posts set cancelled = :cancelled where advert_post_id = :advert_post_id and advert_serial = :advert_serial");
        $update_advert_tbl   = $update_advert_tbl_q->execute([
            "cancelled" => "no",
            "advert_post_id" => $undoadvertCancelPkey,
            "advert_serial" => $undoadvertCancelSerial
        ]); 

        if ($undo_cancelAdvert && $update_advert_tbl) {
            echo 1;
        } else {
            echo 2;
        } // else
    } // end


    // ADVERT DETAILS MODAL VIEW ~ START
        
    /**
     * view advert details
     *
     * @param $selectedadvertId advert tbl primary key
     */
    function _viewAdvertDetails($selectedadvertId) {
        
        $l_q1 = $this->conn->prepare($this->_getAdverts()." where advert_post_id = :advert_post_id");
        $l_q1->execute([
            "advert_post_id" => $selectedadvertId
        ]);
        $l1 = $l_q1->fetchObject();

        $advertpostedObj = new DateTime($l1->date_post);
        $advertposted = $advertpostedObj->format('d M, Y');


        $primarybanner    = $l1->primary_banner_1 != NULL ? $l1->primary_banner_1 : $this->pic_blank;
        $secondarybanner1 = $l1->s_banner_2 != NULL ? $l1->s_banner_2 : $this->pic_blank;
        $secondarybanner2 = $l1->s_banner_3 != NULL ? $l1->s_banner_3 : $this->pic_blank;

        $bannerp1 = $this->clientSideBannerDir.$primarybanner;
        $bannerp2 = $this->clientSideBannerDir.$secondarybanner1;
        $bannerp3 = $this->clientSideBannerDir.$secondarybanner2;

            
        if ($l1) 
        {
            # -- if advert has no banners; just show only advert details --
            if ($l1->primary_banner_1 == NULL && $l1->s_banner_2 == NULL && $l1->s_banner_3 == NULL) {
                $this->_advertDetailsNoBanners($advertposted, $l1, $selectedadvertId);
            }
            # -- if advert has banners; show banner carousel with advert details --
            else if ($l1->primary_banner_1 != NULL || $l1->s_banner_2 != NULL || $l1->s_banner_3  != NULL) {
                $this->_advertDetailsWithBanners($advertposted, $l1, $selectedadvertId, $bannerp1, $bannerp2, $bannerp3);
            }
        }
        else
        {
            echo "<p class='text-center text-danger'>Error: either selected advert is no longer available or has been discontinued!</p>";
        } // else
    } // end



    /**
     * Advert with banners
     *
     * @param string $advertposted advert posted date
     * @param object $l1 advert tbl fetchObject query
     * @param int $selectedadvertId advert tbl primary key
     * @param string $bannerp1 advert primary banner
     * @param string $bannerp2 advert banner 2
     * @param string $bannerp3 advert banner 3
     * @return void
     */
    private function _advertDetailsWithBanners($advertposted, $l1, $selectedadvertId, $bannerp1, $bannerp2, $bannerp3) {

    ?>
        <div id="advert-details-withbanner" class="text-blue-dark advert-containerWrapper">
        
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-5">

                            <div class="owl-carousel" data-plugin-carousel data-plugin-options='{ "autoPlay": 5000, "items": 1,  "navigation": false, "pagination": true }'>
                                <div class="item">
                                    <img src="<?php echo $bannerp1; ?>" id="mainBannerImageView" class="img-responsive" alt="primary-banner">
                                </div>
                                <div class="item">
                                    <img src="<?php echo $bannerp2; ?>" id="mainBannerImageView" class="img-responsive" alt="second-banner">
                                </div>
                                <div class="item">
                                    <img src="<?php echo $bannerp3; ?>" id="mainBannerImageView" class="img-responsive" alt="third-banner">
                                </div>
                            </div>

                </div>


                <div class="col-sm-12 col-xs-12 col-md-7">
                        <h3 class="hr_font_weight_600 purple-text text-center"><?php echo $l1->advert_title; ?></h3>                

                    <p class="text-justify">
                        <?php 
                            echo trim(htmlspecialchars_decode($l1->description), " ");
                        ?> 
                    </p>
                    <p class="text-right">
                        <span class="text-maroon">Posted on:</span> <span class="blue-text"><?php echo $advertposted; ?></span>
                    </p>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <button type="button" class="btn btn-xs bg-blue" id="backtoAdvertApprovaltblBtn" onclick="switchToAdvertList()"><i class="fa fa-angle-left"></i> Back</button>
                </div>
            </div>

        </div>
        <!-- advert-details-withbanner -->

        <script>
            // Carousel
            (function( $ ) {

                'use strict';

                if ( $.isFunction($.fn[ 'owlCarousel' ]) ) {

                    $(function() {
                        $('[data-plugin-carousel]').each(function() {
                            var $this = $( this ),
                                opts = {};

                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;

                            $this.themePluginCarousel(opts);
                        });
                    });

                }

            }).apply(this, [ jQuery ]);
        </script>

    <?php
    } // end

    /**
     * Advert with no banners
     *
     * @param string $advertposted
     * @param object $l1 advert tbl fetcObject
     * @param int $selectedadvertId advert tbl primary key
     * @return void
     */
    private function _advertDetailsNoBanners($advertposted, $l1, $selectedadvertId) {

    ?>
        <div id="advert-details-nobanner" class="text-blue-dark advert-containerWrapper">
            <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h3 class="hr_font_weight_600 purple-text text-center"><?php echo $l1->advert_title; ?></h3>                

                    <p class="text-justify">
                        <?php 
                            echo trim(htmlspecialchars_decode($l1->description), " ");
                        ?> 
                    </p>

                    <p class="text-right">
                        <span class="text-maroon">Posted on:</span> <span class="blue-text"><?php echo $advertposted; ?></span>
                    </p>

                </div>
                <!-- col-sm-12 col-md-12 -->

            </div>
            <!-- row -->

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <button type="button" class="btn btn-xs bg-blue" id="backtoAdvertApprovaltblBtn" onclick="switchToAdvertList()"><i class="fa fa-angle-left"></i> Back</button>
                </div>
            </div>

        </div>

    <?php

    } // end

    // ADVERT DETAILS ~ END


        
    /**
     * approve advert
     *
     * @param int $advertApprovalId advert tbl primary key
     * @return void
     */
    function _approveAdvertMethod($advertApprovalId)
    {
        // get logged email
        $getemail_q = $this->conn->prepare("select loggedEmail, advert_title, date_format(start_period, '%d %M, %Y') as sdate, date_format(end_period, '%d %M, %Y') as edate from advert_posts where advert_post_id = :advert_post_id");
        $getemail_q->execute([
            "advert_post_id" => $advertApprovalId
        ]);
        $getemail = $getemail_q->fetchObject();
        
        if($getemail) {
        
            $sEmail       = $getemail->loggedEmail;
            $sAdvertTitle = $getemail->advert_title;
            $startDate    = $getemail->sdate;
            $sEndDate     = $getemail->edate;
            

            if ($advertApprovalId == 'yes') {

                // update approval tbl

                $approveAdvert_q = $this->conn->prepare("update advert_posts set approved = :approved, cancelled = :cancelled where advert_post_id = :advert_post_id");
                $approveAdvert   = $approveAdvert_q->execute([
                    "approved" => "yes",
                    "cancelled" => "no",
                    "advert_post_id" => $advertApprovalId
                ]);

                // if advert was originally cancelled; remove cancelled tbl
                if ($this->_adOriginallyCancelled($advertApprovalId)) {
                    // delete / remove
                    $delete_cancelledAd_q = $this->conn->prepare("delete from cancelled_adverts where advert_post_id = :advert_post_id");
                    $delete_cancelledAd   = $delete_cancelledAd_q->execute([
                        "advert_post_id" => $advertApprovalId
                    ]);
                } //
                
                // if update was and email submission was okay
                if(($approveAdvert) && $this->advertNotifnEmail($sEmail, $sAdvertTitle, $startDate, $sEndDate)) {

                    echo 1;
                }
                else {
                    echo 2;
                }    

            }   // approve advert
            else {

                // update approval tbl

                $disapproveAdvert_q = $this->conn->prepare("update advert_posts set approved = :approved, cancelled = :cancelled where advert_post_id = :advert_post_id");
                $disapproveAdvert   = $disapproveAdvert_q->execute([
                    "approved" => "no",
                    "cancelled" => "no",
                    "advert_post_id" => $advertApprovalId
                ]);
                if($disapproveAdvert) {

                    echo 4;
                }
                else {
                    echo 5;
                }  

            }    // disapprove advert   
        }
        else {
            echo 3;
        }
    } // end // approve advert


    /**
     * was advert original cancelled
     *
     * @param int $advertApprovalId advert tbl primary key
     * @return bool if advert was originally cancelled, return TRUE; False if not
     * @access private
     */
    private function _adOriginallyCancelled($advertApprovalId) {
        $wasAd_cancelled_q = $this->conn->prepare("select * from cancelled_adverts where advert_post_id = :advert_post_id");
        $wasAd_cancelled_q->execute([
            "advert_post_id" => $advertApprovalId
        ]);
        $wasAd_cancelled = $wasAd_cancelled_q->fetchObject();

        if ($wasAd_cancelled) {            
            return true;
        } else {
            return false;
        } // else

    } // end

        
    /**
     * notification email for advert
     *
     * @param string $sEmail advert created email
     * @param string $sAdvertTitle advert title
     * @param string $startDate advert running start date
     * @param string $sEndDate advert running end date
     *
     * @return bool advert notification was successful, return TRUE; FALSE if it failed
     * @access private
     */
    private function advertNotifnEmail($sEmail, $sAdvertTitle, $startDate, $sEndDate) {
        
        $msg_body = "<p>Your advert titled <strong>".$sAdvertTitle."</strong>, has been successfully approved for display.</p>";
        $msg_body .= "<p>It will run from <b>".$startDate."</b> to <b>".$sEndDate."</b>.</p>";
        
        $emailNotifn_q = $this->conn->prepare("insert into msg_inbox (
            inbox_email,
            inbox_subject,
            inbox_message,
            sender_names,
            sender_email) 
        values (
            :inbox_email,
            :inbox_subject,
            :inbox_message,
            :sender_names,
            :sender_email           
            )");
        $emailNotifn = $emailNotifn_q->execute([
            "inbox_email" => $sEmail,
            "inbox_subject" => "Approval Notice",
            "inbox_message" => $msg_body,
            "sender_names" => "Fast Service",
            "sender_email" => "admin.fastservice@donot.reply.com"
        ]);
        
        if($emailNotifn) {
            return true;
        } else {
            return false;
        } // else
            
    } // end

    /**
     * start of advert list
     *
     * @return object $f returning an object array for adverts list
     */
    function _advertlist() {

        /*
        $g_q2 = $this->conn->prepare("select business_profile_id, advert_post_id, advert_serial, advert_title, date_format(start_period, '%d %M, %Y') as sdate, date_format(end_period, '%d %M, %Y') as edate, date_format(date_post, '%d %M, %Y') as dateP, primary_banner_1 from advert_posts where approved = :approved and cancelled = :cancelled order by date_post desc");
        $g_q2->execute([
            "approved" => "no",
            "cancelled" => "no"
        ]);
        $g2 = $g_q2->fetchAll();
        */

        $f_q = $this->conn->prepare($this->_getAdverts());
        $f_q->execute();
        $f = $f_q->fetchAll();

        return $f;
    } // end

    /**
     * reformat database date format
     *
     * @return string eg. 25 Aug, 2019
     * @access public
     */
    function _returnformatedDate($dateValue){
        $datetimeObj = new DateTime($dateValue);
        return $datetimeObj->format('d M, Y');
    } // format date


    /**
     * adverts list filter search and select
     *
     * @param string $filterSearchKeyword the search keyword or term
     * @param string $filterSelectOrder order by term; asc or desc
     * @param string $filterSelectFilter select filter dropdown
     *
     * @return void
     * @access public
     */
    function _advertfilterSelect($filterSearchKeyword, $filterSelectOrder, $filterSelectFilter) {

        $query = $this->_getAdverts()."where 1=1";

        $whereSQL = $orderSQL = '';
        $keywords = $_POST['filterSearchKeyword'];
        $sortBy   = $_POST['filterSelectOrder'];
        $filterBy = $_POST['filterSelectFilter'];

        if(!empty($keywords)){
            $whereSQL = "WHERE advert_title like '%".$filterSearchKeyword."%' or publish like '%".$filterSearchKeyword."%' or loggedEmail like '%".$filterSearchKeyword."%' or date_post like '%".$filterSearchKeyword."%' or start_period like '%".$filterSearchKeyword."%' or end_period like '%".$filterSearchKeyword."%' or description like '%".$filterSearchKeyword."%' or Business_Name like '%".$filterSearchKeyword."%'";
        }

        if (!empty($filterBy)) {

           if ($filterBy == 'approved') {
               $whereSQL = "WHERE approved = 'yes'";
           }
           elseif ($filterBy == 'unapproved') {
               $whereSQL = "WHERE approved = 'no'";
           }
           elseif ($filterBy == 'cancelled') {
               $whereSQL = "WHERE cancelled = 'yes'";
           }
           elseif ($filterBy == 'havebanners') {
               $whereSQL = "WHERE primary_banner_1 IS NOT NULL";
           }
           elseif ($filterBy == 'nobanners') {
               $whereSQL = "WHERE primary_banner_1 IS NULL";
           }
        }
        if(!empty($sortBy)){
            $orderSQL = " ORDER BY date_post ".$sortBy;
        }else{
            $orderSQL = " ORDER BY date_post DESC ";
        }


        if (!empty($keywords) && !empty($filterBy)) {

           if ($filterBy == 'approved') {
               $whereSQL = "WHERE (advert_title like '%".$filterSearchKeyword."%' or publish like '%".$filterSearchKeyword."%' or loggedEmail like '%".$filterSearchKeyword."%' or date_post like '%".$filterSearchKeyword."%' or start_period like '%".$filterSearchKeyword."%' or end_period like '%".$filterSearchKeyword."%' or description like '%".$filterSearchKeyword."%' or Business_Name like '%".$filterSearchKeyword."%') and approved = 'yes'";
           }
           elseif ($filterBy == 'unapproved') {
               $whereSQL = "WHERE (advert_title like '%".$filterSearchKeyword."%' or publish like '%".$filterSearchKeyword."%' or loggedEmail like '%".$filterSearchKeyword."%' or date_post like '%".$filterSearchKeyword."%' or start_period like '%".$filterSearchKeyword."%' or end_period like '%".$filterSearchKeyword."%' or description like '%".$filterSearchKeyword."%' or Business_Name like '%".$filterSearchKeyword."%') and approved = 'no'";
           }
           elseif ($filterBy == 'cancelled') {
               $whereSQL = "WHERE (advert_title like '%".$filterSearchKeyword."%' or publish like '%".$filterSearchKeyword."%' or loggedEmail like '%".$filterSearchKeyword."%' or date_post like '%".$filterSearchKeyword."%' or start_period like '%".$filterSearchKeyword."%' or end_period like '%".$filterSearchKeyword."%' or description like '%".$filterSearchKeyword."%' or Business_Name like '%".$filterSearchKeyword."%') and cancelled = 'yes'";
           }
           elseif ($filterBy == 'havebanners') {
               $whereSQL = "WHERE (advert_title like '%".$filterSearchKeyword."%' or publish like '%".$filterSearchKeyword."%' or loggedEmail like '%".$filterSearchKeyword."%' or date_post like '%".$filterSearchKeyword."%' or start_period like '%".$filterSearchKeyword."%' or end_period like '%".$filterSearchKeyword."%' or description like '%".$filterSearchKeyword."%' or Business_Name like '%".$filterSearchKeyword."%') and primary_banner_1 IS NOT NULL";
           }
           elseif ($filterBy == 'nobanners') {
               $whereSQL = "WHERE (advert_title like '%".$filterSearchKeyword."%' or publish like '%".$filterSearchKeyword."%' or loggedEmail like '%".$filterSearchKeyword."%' or date_post like '%".$filterSearchKeyword."%' or start_period like '%".$filterSearchKeyword."%' or end_period like '%".$filterSearchKeyword."%' or description like '%".$filterSearchKeyword."%' or Business_Name like '%".$filterSearchKeyword."%') and primary_banner_1 IS NULL";
           }
        }



         $f_q = mysqli_query($this->db, "select * from advert_posts ".$whereSQL.$orderSQL);
        if (mysqli_num_rows($f_q) > 0) {

            $sil = $this->conn->prepare("SELECT Business_Name, Business_Phone FROM business_profile WHERE business_profile_id = :business_profile_id");

            while($vo = mysqli_fetch_assoc($f_q))
            {

                $post_id = $vo['post_id'];
                $advertSerial = $vo['advert_serial'];

                $sil->execute(["business_profile_id" => $vo['business_profile_id']]);

                $re = $sil->fetchObject();
                $advertPostDateTimeObj = new DateTime($vo['date_post']);
                $advertPostDateTime = $advertPostDateTimeObj->format('d M, Y - h:i a');
    ?>
    <section class="panel panel-featured panel-featured-success">
        <header class="panel-heading">
        <h2 class="panel-title hr_font_weight_700"><?php echo $vo['advert_title']; ?></h2>
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
            <a href="#" class="fa fa-times"></a>
        </div>
        </header>
        <div class="panel-body mdb-color-text">
            <?php echo trim(htmlspecialchars_decode($vo['description']), " ") ?>
            <div class="text-navy">
                <p class="t-15">
                    Business Name: <?php echo $re ? $re->Business_Name : "N/A"; ?>
                </p>
                <p class="t-15">
                    <?php echo "Runs from <b>" . $this->_returnformatedDate($vo['start_period']) . "</b> To <b>" . $this->_returnformatedDate($vo['end_period']) . "</b>"; ?>
                </p>
                <p class="t-15">
                    Tel: <i class="fa fa-phone text-olive"></i> <?php echo $re->Business_Phone; ?>
                </p>
                <p class="t-15">
                    <?php 
                        $run = $vo['status'] == 0 ? "Ended" : "On-going";

                        echo "
                        <span class='light-blue-text'>Admin Approved:</span> <a href='javascript:void(0)' class='text-capitalize mdb-color-text data-admin-approved' data-type='select' data-title='Approval status' data-pk='".$post_id."' data-name='approved' data-placeholder='required' data-value='".$vo['approved']."'>" . $vo['approved']. "</a> - 
                        <span class='light-blue-text'>Client Published:</span> <span class='text-capitalize mdb-color-text'>". $vo['publish'].
                        "</span> - 
                        <span class='light-blue-text'>Cancelled:</span> <a href='#advertCancel-Modal' data-toggle='modal' class='text-capitalize mdb-color-text data-admin-cancelled' onclick='cancelAdvertfunc2(".$post_id.")' id='advert-cancelled".$post_id."'>" .$vo['cancelled']."</a> - 
                        <span class='light-blue-text'>Run:</span> <span class='text-capitalize mdb-color-text'>" .$run."</span>"; 

        ?>
        <input type="hidden" id="advert_post_id<?php echo $post_id; ?>" value="<?php echo $post_id; ?>">
        <input type="hidden" id="advert_serial_id<?php echo $post_id; ?>" value="<?php echo $advertSerial; ?>">
        <input type="hidden" id="advert_view<?php echo $post_id; ?>" value="1">
                </p>
                <?php if ($vo['primary_banner_1'] != NULL) { ?>
                <div class="t-15 btn-group">
                        <a class="image-popup-vertical-fit btn btn-default btn-flat" href="<?php echo '../client/_assets/clientUploads/'.$vo['primary_banner_1']; ?>">
                            <img class="img-responsive display-none" src="<?php echo '../client/_assets/clientUploads/'.$vo['primary_banner_1']; ?>" width="75">
                            <i class="fa fa-file-picture-o text-olive"></i> Banner 1
                        </a>
                <?php if ($vo['s_banner_2'] != NULL || $vo['s_banner_3'] != NULL) { ?>
                        <a class="image-popup-vertical-fit btn btn-default btn-flat" href="<?php echo '../client/_assets/clientUploads/'.$vo['s_banner_2']; ?>">
                            <img class="img-responsive display-none" src="<?php echo '../client/_assets/clientUploads/'.$vo['s_banner_2']; ?>" width="75">
                            <i class="fa fa-file-picture-o text-olive"></i> Banner 2
                        </a>        

                        <a class="image-popup-vertical-fit btn btn-default btn-flat" href="<?php echo '../client/_assets/clientUploads/'.$vo['s_banner_3']; ?>">
                            <img class="img-responsive display-none" src="<?php echo '../client/_assets/clientUploads/'.$vo['s_banner_3']; ?>" width="75">
                            <i class="fa fa-file-picture-o text-olive"></i> Banner 3
                        </a>    
                <?php } ?>  
                </div>
                <?php } ?>
            </div>
            <!-- text-navy -->

        </div>
        <!-- panel body -->
        <div class="panel-footer">
            <div class="row">
                <div class="col-xs-7">
                    <p class="mdb-color-text"><?php echo "<b>Posted:</b> " . $advertPostDateTime; ?></p>
                </div>
                <div class="col-xs-5">
                    <p class="mdb-color-text"><?php echo "<b>By:</b> " . $vo['loggedEmail']; ?></p>
                </div>
            </div>
        </div>
    </section>
    <?php
            } // while loop
           
        } // if
        else {
            echo "<p class='text-center text-warning hr_font_weight_600'>No Result available</p>";
        }

    } // end
    # ------ end of advert list


    /**
     * trending adverts
     */
    function _getTrendingAdverts() {

       // get views
        $a_q1 = $this->conn->prepare("SELECT advert_post_id FROM advert_views ORDER BY advert_view_numbers DESC");
        $a_q1->execute();
        $a1   = $a_q1->fetchAll();

        if ($a1) {
            ?>
            <div id="marquee_trending_adverts">

            <?php
            // get advert information
            $a_q2 = $this->conn->prepare($this->_getAdverts() . " where advert_post_id = :advert_post_id");

            foreach ($a1 as $row) {
                
                $a_q2->execute([
                    "advert_post_id" => $row['advert_post_id']
                ]);

                $a2 = $a_q2->fetchObject();

                if ($a2) {
                    ?>
                    <div class="alert alert-dark">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <a href="javascript:void(0)" class="alert-link trending-ads-link">
                            <strong><?php echo $a2->advert_title; ?></strong>
                        </a>.
                    </div>
                    <?php
                }

            } // foreach loop
            ?>
    </div>
    <script>
        
             // trending adverts marquee
            $('#marquee_trending_adverts').marquee({ 
                pauseOnHover: true, 
                direction: 'up', 
                duration: 10000, 
                startVisible: true
            });

    </script>
            <?php
        }
        else {
            echo "<p class='text-center'>No Advert Trends</p>";
        }

    } // end

    // -------------------- >> ADVERTS ENDS << -------------------- //   
    
    
    // -------------------- >> JOBS STARTS << -------------------- //   

    /**
     * jobs alerts counter requiring approval
     *
     * @return int $alerts1 if adverts requiring approval exists; 
     *                      0 if no jobs requiring approval exists
     * @access public
     */
    function _jobAlert()
    {
        $alerts_q1 = mysqli_query($this->db, $this->_getJobs()." where approved = 'no' order by date_posted desc");
        $alerts1   = mysqli_num_rows($alerts_q1);
        
        if ($alerts1 > 0) 
        {
            return $alerts1;
        }
        else
        {
            return 0;
        }
    } // _jobAlert counter

    /**
     * job list requiring approval
     *
     * @return array $g2 jobs list requiring approval array; 
     *                   use foreach loop to navigate through list
     * @access public
     */
    function approveJobListModal()
    {
        ?>
          <div class="table-responsive">

            <table class="table table-condensed mb-none" id="jobs-requriring-approval-dataTable" style="width: 100% !important">
               <thead class="bg-blue">
                    <th>Job Title</th>
                    <th>Email</th>
                    <th class="text-center">Action</th>
                </thead>
            </table>
            
          </div>
  <script>
      $(function(){
            /**
            * JOBS
            * -----------------------*/
            // load jobs requiring approval
            var datatablevalue = 1;
            var jobsrequiringapprovallist = $('#jobs-requriring-approval-dataTable').DataTable({
               "processing": false,
               "serverSide": true,
               "info": false,
               "ordering": false,
               "autoWidth": false,
               "dom": '<"row"<"col-sm-6 col-md-6"l><"col-sm-6 col-md-6"f>>'+'<"row"<"col-sm-12 col-md-12"t>>'+'<"row"<"col-sm-12 col-md-12"p>>',
               "ajax":{
                  url :"_admin-requests.php",
                  type: "post",
                  data: {jobs_requiring_approval:datatablevalue},
                  error: function(){
                     $(".dataTable_gridClass").html("");
                     $("#rttlice-grid").append('<tbody class="dataTable_gridClass"><tr><th colspan="3">No records found!</th></tr></tbody>');
                     $("#dataTable-grid_processing").css("display","none");                     
                  }
               }
            });

            setInterval(() => {
                jobsrequiringapprovallist.ajax.reload( null, false);
            }, 3000);
        });
  </script>

        <?php

    } // end


    /**
     * load server-side processing for jobs requiring approval
     * datatable
     *
     * @return void
     * @access public
     */
    function loadJobsRequiringApprovalDatatbl() {
        $columns = array( 
            0 =>'job_vacancy', 
            1 => 'loggedin_email'
        );

        // getting total number records without any search
        $j_q1 = "select job_vacancy, loggedin_email, job_id, job_serial";
        $j_q1.= " from jobs where approved = 'no'";
        $j1 = mysqli_query($this->db, $j_q1) or die("Unknown Error Occured1!");

        $totalData = mysqli_num_rows($j1);
        $totalFiltered = $totalData;
    
        $j_q1 = "select job_vacancy, loggedin_email, job_id, job_serial";
        $j_q1.=" from jobs where 1=1 and approved = 'no'";

        if( !empty($_REQUEST['search']['value']) ) { 
            $j_q1.=" AND ( job_vacancy LIKE '".$_REQUEST['search']['value']."%' ";    
            $j_q1.=" OR loggedin_email LIKE '".$_REQUEST['search']['value']."%' )";
        }
        $j1=mysqli_query($this->db, $j_q1) or die("Unknown Error Occured2!");
        $totalFiltered = mysqli_num_rows($j1);

       // $j_q1.=" ORDER BY ". $columns[$_REQUEST['order'][0]['column']]."   ".$_REQUEST['order'][0]['dir']."  LIMIT ".$_REQUEST['start']." ,".$_REQUEST['length']."   ";
   
       // $j1=mysqli_query($this->db, $j_q1) or die($j_q1);

        $data = array();

        while( $row=mysqli_fetch_array($j1) ) {  // preparing an array
            
            $nestedData =   array(); 

            $job_id    = $row["job_id"];
            $indexId   = $row["job_serial"];

            $viewJobDetails = '<a href="javascript:void(0)" class="btn bg-olive btn-xs btn-flat" id="a_view_approved_td'.$job_id.'" onclick="swipeview_jobDetails('.$job_id.')">View</a>&nbsp;';

            $approveJob = '<a href="javascript:void(0)" class="btn btn-warning btn-xs btn-flat" id="a_approved_td'.$job_id.'" onclick="approveJobfunction('.$job_id.')">Approve</a>';

            $nestedData[] = $row["job_vacancy"];
            $nestedData[] = $row["loggedin_email"];
            $nestedData[] = $viewJobDetails.$approveJob;

            $data[] = $nestedData;
        }

        $json_data = array(
                    "draw"            => intval( $_REQUEST['draw'] ),
                    "recordsTotal"    => intval( $totalData ),
                    "recordsFiltered" => intval( $totalFiltered ),
                    "data"            => $data
                    );

        echo json_encode($json_data);   

    } // end
    
    
    /**
     * load selected job details either for 
     * viewing or approving
     *
     * @param int $selectedJobId job tbl primary key
     * @return void
     * @access public
     */
    function _loadJobDetails($selectedJobId)
    {

        $f_q4 = $this->conn->prepare($this->_getJobs()." where job_id = :job_id");
        $f_q4->execute([
            "job_id" => $selectedJobId
        ]);
        $f4 = $f_q4->fetchObject();

        if ($f4) 
        {

            // --- >> get business profile
            $f_q5 = $this->conn->prepare($this->_businessprofile()." where business_profile_id = :business_profile_id");
            $f_q5->execute([
                "business_profile_id" => $f4->business_profile_id
            ]);
            $f5 = $f_q5->fetchObject();

            // --- >> get responsibilities
            $f_q12 = $this->conn->prepare("select responsibility from job_responsibility where job_id = :job_id");
            $f_q12->execute([
                "job_id" => $selectedJobId
            ]);
            $f12 = $f_q12->fetchAll();        


            $selectedjobpostedObj = new DateTime($f4->date_posted);
            $selectedjobposted    = $selectedjobpostedObj->format('d M, Y');

            $selectedjobdeadline = $f4->deadline != 'N/A' ? $this->_jobdeadlineDate($f4->deadline) : 'N/A';

            $jobDuration = $f4->duration != 'N/A' ? $f4->duration. ' | '  : '';
            ?>
            <input type="hidden" value="<?php echo $selectedJobId; ?>" id="data_job_id_awaiting_approval">
            <div class="row mdb-color-text">
                <div class="col-xs-12 grey-text text-center">
                    <h3><?php echo  $f4->job_vacancy; ?></h3>
                    <h4><?php echo  $f5->Business_Name; ?></h4>
                    <h5> 
                        <span><?php echo $jobDuration . $f4->job_type. ' | '. $f4->salary. ' | '. $selectedjobposted; ?></span>
                    </h5>
                </div>
            </div>
            <div class="ln_solid"></div>

            <div class="row mdb-color-text">
                <div class="col-xs-12">                
                    <h4 class="brown-text">Job Summary</h4>
                    <p>
                        <span><b>Function:</b> <?php echo  $f4->job_function; ?></span><br>
                        <span><b>Deadline:</b> <?php echo $selectedjobdeadline; ?></span><br>
                        <span><b>Duty Station:</b> <?php echo  $f4->location; ?></span>                    
                    </p>

                    <h4 class="brown-text">Qualifications</h4>
                    <p class="text-justify">
                        <?php echo  $f4->job_qualification; ?>
                    </p>

                    <h4 class="brown-text">Key Duties and Responsibilities</h4>
                    <p class="text-justify">
                        <ul>
                        <?php foreach ($f12 as $row) 
                        {
                        ?>
                            <li><?php echo $row['responsibility']; ?></li>
                        <?php
                        } 
                        ?>
                        </ul>
                    </p>
                    <h4 class="brown-text">Job application procedure</h4>
                    <p class="text-justify">
                        <?php echo  $f4->application_mode; ?>
                    </p>
                </div>
            </div>
            
            <?php
        }
        else
        {
            $this->errs[] = FAILED_TO_LOAD_JOB_DETAILS;
        } // job details loading failure
        
    } // _loadJobDetails

    
    /**
     * approve a job
     *
     * @param integer jbApprovalId job tbl primary key
     * @return void
     * @access public
     */
    function _approveJobMethod($jbApprovalId)
    {     
        $approve_job_q1 = $this->conn->prepare("update jobs set approved = :approved where job_id = :job_id");
        $approve_job   = $approve_job_q1->execute([
            "approved" => "yes",
            "job_id" => $jbApprovalId
        ]);
        
        if($approve_job)
        {
            echo 1;
        }
        else
        {
            echo 2;
        }
        
    } // _approveJobMethod()


    // Jobs Management Page
    
    /**
     * return jobs list under job mgmt page
     *
     * @return object $g_q we shall use a mysqli_num_rows to check if
     *                     jobs are available and then a while loop to load jobs under jobs mgmt page
     * @access public
     */
    function _returnJobsList() {
        $g_q = mysqli_query($this->db, $this->_getJobs());
        return $g_q;
    } // end

    /**
     * approve or disapprove a job
     *
     * @param int $jbApprovalId job tbl primary key
     * @param string $dataJob_value approval value; either yes or no
     * @return void
     * @access public
     */
     function dis_or_approveJobMethod($jbApprovalId, $dataJob_value) {  

       // check whether its approving or disapproving
        if ($dataJob_value == 'yes') 
        {
            $approve_job_q1 = $this->conn->prepare("update jobs set approved = :approved where job_id = :job_id");
            $approve_job   = $approve_job_q1->execute([
                "approved" => $dataJob_value,
                "job_id" => $jbApprovalId
            ]);
            
            if($approve_job)
            {
                echo 1;
            }
            else
            {
                echo 2;
            } 
        }  // approving
        else {
            $approve_job_q2 = $this->conn->prepare("update jobs set approved = :approved where job_id = :job_id");
            $approve_job2   = $approve_job_q2->execute([
                "approved" => $dataJob_value,
                "job_id" => $jbApprovalId
            ]);
            
            if($approve_job2)
            {
                echo 4;
            }
            else
            {
                echo 5;
            } 
        } // disapproving
    
    } // end

    /**
     * job statistics under job management
     *
     * @return void
     * @access public
     */
    function numberOfJobsUnderEachBusiness() {

        $g_q = $this->conn->prepare("SELECT business_profile_id, count(*) as numberOfJobs from jobs where business_profile_id IS NOT NULL group by business_profile_id order by business_profile_id asc");
        $g_q->execute();
        $g = $g_q->fetchAll();

        if ($g) 
        {

            // get business name associated with job
            $g_q1 = $this->conn->prepare("select Business_Name from business_profile where business_profile_id = :business_profile_id");

            ?>
            <ul class="list-group">

                <?php
                    foreach($g as $row) {

                        $g_q1->execute([
                            "business_profile_id" => $row['business_profile_id']
                        ]);
                        $g1 = $g_q1->fetchObject();
                ?>

                <li class="list-group-item">
                    <strong>
                        <?php echo $g1->Business_Name; ?>
                    </strong>
                    <span class="badge bg-olive"><?php echo $row['numberOfJobs']; ?></span>
                </li>

            <?php } // loop ?>

            </ul>
            <!-- list group -->

            <?php
            
        } else {
            echo "<p class='text-center text-olive'>No Business has created a Job yet!</p>";
        } // else

    } // end

    /**
     * Job analysis
     *
     * @param string $dataInputSearchJob search term
     * @param string $dataSelectApprovalJob approval value; yes or no
     * @param string $dataSelectOrderJob ordering in either asc or desc
     * @param string $dataSelectDistrictJob districts
     *
     * @return void
     * @access public
     */
    function jobAnalysis($dataInputSearchJob, $dataSelectApprovalJob, $dataSelectOrderJob, $dataSelectDistrictJob) {

        $whereSQL = $orderSQL = '';

         // search filter
        if(!empty($dataInputSearchJob)){
            $whereSQL = "WHERE job_vacancy LIKE '%".$dataInputSearchJob."%' or job_district LIKE '%".$dataInputSearchJob."%' or job_type LIKE '%".$dataInputSearchJob."%' or loggedin_email LIKE '%".$dataInputSearchJob."%' or date_posted LIKE '%".$dataInputSearchJob."%' or salary LIKE '%".$dataInputSearchJob."%' or approved LIKE '%".$dataInputSearchJob."%' or application_mode LIKE '%".$dataInputSearchJob."%'";
        }
        // order by filter
        if(!empty($dataSelectOrderJob)){
            $orderSQL = " ORDER BY date_posted ".$dataSelectOrderJob;
        }else{
            $orderSQL = " ORDER BY date_posted DESC ";
        }
        // approval filter
        if(!empty($dataSelectApprovalJob)){
            $whereSQL = "WHERE approved = '$dataSelectApprovalJob'";
        }

        // district filter
        if(!empty($dataSelectDistrictJob)){
            $whereSQL = "WHERE job_district = '$dataSelectDistrictJob'";
        }

        // double filter
        if(!empty($dataSelectApprovalJob) && !empty($dataSelectDistrictJob)){
            $whereSQL = "WHERE approved = '$dataSelectApprovalJob' AND job_district = '$dataSelectDistrictJob'";
        }
        if(!empty($dataInputSearchJob) && !empty($dataSelectDistrictJob)){
            $whereSQL = "WHERE (job_vacancy LIKE '%".$dataInputSearchJob."%' or job_district LIKE '%".$dataInputSearchJob."%' or job_type LIKE '%".$dataInputSearchJob."%' or loggedin_email LIKE '%".$dataInputSearchJob."%' or date_posted LIKE '%".$dataInputSearchJob."%' or salary LIKE '%".$dataInputSearchJob."%' or approved LIKE '%".$dataInputSearchJob."%' or application_mode LIKE '%".$dataInputSearchJob."%') AND job_district = '$dataSelectDistrictJob'";
        }
        if(!empty($dataInputSearchJob) && !empty($dataSelectApprovalJob)){
            $whereSQL = "WHERE (job_vacancy LIKE '%".$dataInputSearchJob."%' or job_district LIKE '%".$dataInputSearchJob."%' or job_type LIKE '%".$dataInputSearchJob."%' or loggedin_email LIKE '%".$dataInputSearchJob."%' or date_posted LIKE '%".$dataInputSearchJob."%' or salary LIKE '%".$dataInputSearchJob."%' or approved LIKE '%".$dataInputSearchJob."%' or application_mode LIKE '%".$dataInputSearchJob."%') AND approved = '$dataSelectApprovalJob'";
        }

        // tripple filter
        if(!empty($dataInputSearchJob) && !empty($dataSelectApprovalJob) && !empty($dataSelectDistrictJob)){
            $whereSQL = "WHERE (job_vacancy LIKE '%".$dataInputSearchJob."%' or job_district LIKE '%".$dataInputSearchJob."%' or job_type LIKE '%".$dataInputSearchJob."%' or loggedin_email LIKE '%".$dataInputSearchJob."%' or date_posted LIKE '%".$dataInputSearchJob."%' or salary LIKE '%".$dataInputSearchJob."%' or approved LIKE '%".$dataInputSearchJob."%' or application_mode LIKE '%".$dataInputSearchJob."%') AND approved = '$dataSelectApprovalJob' AND job_district = '$dataSelectDistrictJob'";
        }

        $oop = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

        $analysis_q = $oop->query("select * from jobs ".$whereSQL.$orderSQL);

        if($analysis_q->num_rows > 0)
        {
            ?>

            <div class="alert alert-sm bg-olive text-center">
                <button type="button" class="close text-white" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $analysis_q->num_rows." results found"; ?>                    
            </div>

            <?php

            // get business name associated with job
            $sql1 = $this->conn->prepare("SELECT Business_Name from  business_profile where business_profile_id = :business_profile_id");

            // get job responsibilities
            $sql2 = $this->conn->prepare("SELECT responsibility from job_responsibility where job_id = :job_id and job_serial = :job_serial");

            //while($woq = mysqli_fetch_assoc($analysis_q))
            while($woq = $analysis_q->fetch_assoc())
            {

                $biz_id     = $woq['business_profile_id'];  
                $job_id     = $woq['job_id'];   
                $job_serial = $woq['job_serial']; 

                $sql1->execute([
                    "business_profile_id" => $biz_id
                ]); 
                $qo = $sql1->fetchObject();

                $sql2->execute([
                    "job_id" => $job_id,
                    "job_serial" => $job_serial
                ]);
                $jresp = $sql2->fetchAll();
?>
                        <section class="panel panel-featured panel-featured-purple">
                            <header class="panel-heading">
                                <div class="panel-actions more-details-panel-actions">
                                    <a href="#" class="fa fa-caret-up more-details-caret" data-toggle="tooltip" title="View more details"></a>
                                    <a href="#" class="fa fa-times" data-toggle="tooltip" title="Close"></a>
                                </div>

                                <h2 class="panel-title hr_font_weight_800">
                                    <?php echo $woq['job_vacancy']; ?>
                                </h2>
                                <p class="panel-subtitle hr_font_weight_600">
                                    <?php echo $qo->Business_Name; ?>
                                </p>
                                <p>
                                    <?php echo $woq['jcName']; ?>
                                </p>
                            </header>
                            <div class="panel-body text-navy t-15 display-none">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <p>
                                            <strong>District:</strong> <?php echo $woq['job_district']; ?>
                                        </p>
                                        <p>
                                            <strong>Location:</strong> <?php echo $woq['location']; ?>
                                        </p>   
                                        <p>
                                            <strong>Posted:</strong> <?php echo $this->_returnformatedDate($woq['date_posted']); ?>
                                        </p>                                    
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <p>
                                            <strong>Type:</strong> <?php echo $woq['job_type']; ?>
                                        </p>
                                        <p>
                                            <strong>Salary:</strong> <?php echo $woq['salary']; ?>
                                        </p>  
                                        <p>
                                            <strong>Deadline:</strong> <?php echo $woq['deadline'] != 'N/A' ? $this->_returnformatedDate($woq['deadline']) : $woq['deadline']; ?>
                                        </p>                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                        <h4 class="pink-text">Qualifications</h4>
                                        <span class="text-justify mdb-color-text">
                                            <?php echo  $woq['job_qualification']; ?>
                                        </span>

                                        <h4 class="pink-text">Key Duties and Responsibilities</h4>
                                        <p class="text-justify mdb-color-text">
                                            <ul>
                                            <?php foreach ($jresp as $row) 
                                            {
                                            ?>
                                                <li><?php echo $row['responsibility']; ?></li>
                                            <?php
                                            } 
                                            ?>
                                            </ul>
                                        </p>
                                        <h4 class="pink-text">Job application procedure</h4>
                                        <p class="text-justify mdb-color-text">
                                            <?php echo  $woq['application_mode']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6 col-md-6">
                                        <p>
                                            <strong>Approved:</strong> 
                                            <a href='javascript:void(0)' class='text-capitalize datajob-admin-approved' data-type='select' data-title='Approval status' data-pk='<?php echo $job_id; ?>' data-name='approved' data-placeholder='required' data-value='<?php echo $woq['approved']; ?>'>
                                                <?php echo $woq['approved']; ?>                                                     
                                            </a>
                                        </p>
                                    </div>
                                    <div class="col-sm-6 col-xs-6 col-md-6">
                                        <p>
                                            <strong>Posted By:</strong> <span><?php echo $woq['loggedin_email']; ?></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        
                        </section>
                <?php
            } // while loop
            ?>
<script>
    // Panels
(function( $ ) {

    $(function() {
        $('.panel')
            .on( 'click', '.panel-actions a.fa-caret-up', function( e ) {
                e.preventDefault();

                var $this,
                    $panel;

                $this = $( this );
                $panel = $this.closest( '.panel' );

                $this
                    .removeClass( 'fa-caret-up' )
                    .addClass( 'fa-caret-down' );

                $panel.find('.panel-body, .panel-footerc').slideDown( 200 );
            })
            .on( 'click', '.panel-actions a.fa-caret-down', function( e ) {
                e.preventDefault();

                var $this,
                    $panel;

                $this = $( this );
                $panel = $this.closest( '.panel' );

                $this
                    .removeClass( 'fa-caret-down' )
                    .addClass( 'fa-caret-up' );

                $panel.find('.panel-body, .panel-footerc').slideUp( 200 );
            })
            .on( 'click', '.panel-actions a.fa-times', function( e ) {
                e.preventDefault();

                var $panel,
                    $row;

                $panel = $(this).closest('.panel');

                if ( !!( $panel.parent('div').attr('class') || '' ).match( /col-(xs|sm|md|lg)/g ) && $panel.siblings().length === 0 ) {
                    $row = $panel.closest('.row');
                    $panel.parent('div').remove();
                    if ( $row.children().length === 0 ) {
                        $row.remove();
                    }
                } else {
                    $panel.remove();
                }
            });
    });

})( jQuery );
</script>

            <?php
        }
        else
        {
            echo '
                <div class="alert alert-danger text-center">
                    <strong class="text-maroon">Info: No Results found!</strong>
                </div>';
        }

    } // end

    
    // -------------------- >> JOB ENDS << -------------------- // 


    // -------------------- >> REPORTS STARTS << -------------------- // 

    /**
     * Default jobs list
     *
     * @return void
     * @access public
     */
    function jobReportDefaultViewList() {

        $jobQ = mysqli_query($this->db, "select job_id, date_format(date_posted, '%d-%m-%Y') as date_created, date_format(deadline, '%d-%m-%Y') as job_deadline, Business_Name, job_vacancy, loggedin_email, approved, job_serial, status from jobs order by job_id");

        ?>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-striped mb-none" id="datatable-jobsReports" style="width: 100%">
            <thead class="bg-olive">
                <tr>
                    <th>#</th>
                    <th>Posted On</th>
                    <th>Expires On</th>
                    <th>Business Name</th>
                    <th>Job Title</th>
                    <th>Owner</th>
                    <th>Approved</th>
                    <th>Current Status</th>
                </tr>
            </thead>
            <tbody class="mdb-color-text">
                <?php while($row = mysqli_fetch_assoc($jobQ)){ ?>
                    <tr>
                        <td>
                            <?php echo $row['job_id']; ?>
                        </td>
                        <td>
                            <?php echo $row['date_created']; ?>
                        </td>
                        <td>
                            <?php echo $row['job_deadline']; ?>
                        </td>
                        <td>
                            <?php echo $row['Business_Name']; ?>
                        </td>
                        <td>
                            <?php echo $row['job_vacancy']; ?>
                        </td>
                        <td>
                            <?php echo $row['loggedin_email']; ?>
                        </td>
                        <td class="text-capitalize">
                            <?php echo $row['approved']; ?>
                        </td>
                        <td>
                            <?php 
                                if($row['status'] == 0) {
                                    echo "Inactive";
                                } else if ($row['status'] == 1) {
                                    echo "Active";
                                } else {
                                    echo "Ended";
                                }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
        
        <script>
        // datatable init
        (function( $ ) {
            'use strict';

            var jobsdatatableInit = function() {
                var $table = $('#datatable-jobsReports');
                $table.DataTable({
                    dom: "<'row mt-md mb-xs'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12 col-xs-12 col-md-12't>><'row mt-xs'<'col-md-4 col-sm-4 col-lg-4 col-xs-4'l><'col-md-8 col-xs-8 col-sm-8'p>>",
                    buttons: [
                            {
                                extend: 'excelHtml5',
                                filename: 'fast-service-all-client-created-jobs',
                                title: "Fast Service",
                                messageTop: "All Client Jobs" ,
                                text: ' Export to Excel',
                                className: "btn btn-flat fa fa-file-excel-o bg-maroon"
                            }
                        ]
                });
            };

            // function calls
            $(function() {
                jobsdatatableInit();
            });

        }).apply( this, [ jQuery ]);

        </script>
      <?php  

    } // end

    /**
     * Job reports dates onchange filter
     *
     * @param string $datainputFromDate from date
     * @param string $datainputToDate to date
     * @param string $dataselectjobsBusinessid business tbl primary key
     * @return void
     * @access public
     */
    function jobReportsDateFilter($datainputFromDate, $datainputToDate, $dataselectjobsBusinessid) {

        if ($dataselectjobsBusinessid == "all") {
            $this->_jobsReportsAllBusinesses($datainputFromDate, $datainputToDate);
        } // all businesses
        else {
            $this->_jobsReportsSelectedBusinesses($datainputFromDate, $datainputToDate, $dataselectjobsBusinessid);
        } // else // selected business

    } // end


    /**
     * get all jobs reports for all businesses
     *
     * @param string $datainputFromDate from date
     * @param string $datainputToDate to date
     * @param string $dataselectjobsBusinessid business tbl primary key
     *
     * @return void
     * @access private
     */
    private function _jobsReportsSelectedBusinesses($datainputFromDate, $datainputToDate, $dataselectjobsBusinessid) {

        $f_q = $this->conn->prepare("select job_id, date_format(date_posted, '%d-%m-%Y') as date_created, date_format(deadline, '%d-%m-%Y') as job_deadline, Business_Name, job_vacancy, loggedin_email, approved, job_serial, status from jobs where (date_posted between :datainputFromDate and :datainputToDate) and business_profile_id = :business_profile_id order by job_id");
        $f_q->execute([
            "datainputFromDate" => $datainputFromDate,
            "datainputToDate" => $datainputToDate,
            "business_profile_id" => $dataselectjobsBusinessid
        ]);
        $f = $f_q->fetchAll();

        if ($f) {
            ?>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-striped mb-none" id="datatable-jobsReport-selectedBusiness">
            <thead class="bg-olive">
                <tr>
                    <th>#</th>
                    <th>Posted On</th>
                    <th>Expires On</th>
                    <th>Job Title</th>
                    <th>Owner</th>
                    <th>Approved</th>
                    <th>Current Status</th>
                </tr>
            </thead>
            <tbody class="mdb-color-text">
                <?php
                $bnameArray = array();

                 foreach($f as $row){ 
                    $bnameArray[0] = $row['Business_Name'];
                    ?>
                    <tr>
                        <td>
                            <?php echo $row['job_id']; ?>
                        </td>
                        <td>
                            <?php echo $row['date_created']; ?>
                        </td>
                        <td>
                            <?php echo $row['job_deadline']; ?>
                        </td>
                        <td>
                            <?php echo $row['job_vacancy']; ?>
                        </td>
                        <td>
                            <?php echo $row['loggedin_email']; ?>
                        </td>
                        <td class="text-capitalize">
                            <?php echo $row['approved']; ?>
                        </td>
                        <td>
                            <?php 
                                if($row['status'] == 0) {
                                    echo "Inactive";
                                } else if ($row['status'] == 1) {
                                    echo "Active";
                                } else {
                                    echo "Ended";
                                }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
        
        <script>
        // datatable init
        (function( $ ) {
            'use strict';

            var businessName = '<?php echo $bnameArray[0]; ?>';
            var jobFromDate = '<?php echo $datainputFromDate; ?>';
            var jobToDate = '<?php echo $datainputToDate; ?>';

            var jobsdatatableInit2 = function() {

                var $table = $('#datatable-jobsReport-selectedBusiness');

                $table.DataTable({
                    dom: "<'row mt-md mb-xs'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12 col-xs-12 col-md-12't>><'row mt-xs'<'col-md-4 col-sm-4 col-lg-4 col-xs-4'l><'col-md-8 col-xs-8 col-sm-8'p>>",
                    buttons: [
                            {
                                extend: 'excelHtml5',
                                filename: 'Fast-Service-Jobs-Under-'+businessName+'-from-'+jobFromDate+'-to-'+jobToDate,
                                title: "Fast Service - Jobs",
                                messageTop: businessName+ ' From '+jobFromDate+' To '+jobToDate,
                                text: ' Export to Excel',
                                className: "btn btn-flat fa fa-file-excel-o bg-maroon"
                            }
                        ]
                });
            };

            // function calls
            $(function() {
                jobsdatatableInit2();
            });

        }).apply( this, [ jQuery ]);
        
        </script>

            <?php
        } else {
            echo "<div class='alert alert-sm alert-info text-center'><strong class='text-maroon'>Info: No results available based on Business and dates parameters selected!</strong></div>";
        }

    } // end


    /**
     * get all jobs reports for all businesses
     *
     * @param string $datainputFromDate from date
     * @param string $datainputToDate to date
     *
     * @return void
     * @access private
     */
    private function _jobsReportsAllBusinesses($datainputFromDate, $datainputToDate) {

        $f_q = $this->conn->prepare("select job_id, date_format(date_posted, '%d-%m-%Y') as date_created, date_format(deadline, '%d-%m-%Y') as job_deadline, Business_Name, job_vacancy, loggedin_email, approved, job_serial, status from jobs where (date_posted between :datainputFromDate and :datainputToDate) order by job_id");
        $f_q->execute([
            "datainputFromDate" => $datainputFromDate,
            "datainputToDate" => $datainputToDate
        ]);
        $f = $f_q->fetchAll();

        if ($f) {
            ?>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-striped mb-none" id="datatable-jobsReport-allBusinesses">
            <thead class="bg-olive">
                <tr>
                    <th>#</th>
                    <th>Posted On</th>
                    <th>Expires On</th>
                    <th>Business Name</th>
                    <th>Job Title</th>
                    <th>Owner</th>
                    <th>Approved</th>
                    <th>Current Status</th>
                </tr>
            </thead>
            <tbody class="mdb-color-text">
                <?php foreach($f as $row){ ?>
                    <tr>
                        <td>
                            <?php echo $row['job_id']; ?>
                        </td>
                        <td>
                            <?php echo $row['date_created']; ?>
                        </td>
                        <td>
                            <?php echo $row['job_deadline']; ?>
                        </td>
                        <td>
                            <?php echo $row['Business_Name']; ?>
                        </td>
                        <td>
                            <?php echo $row['job_vacancy']; ?>
                        </td>
                        <td>
                            <?php echo $row['loggedin_email']; ?>
                        </td>
                        <td class="text-capitalize">
                            <?php echo $row['approved']; ?>
                        </td>
                        <td>
                            <?php 
                                if($row['status'] == 0) {
                                    echo "Inactive";
                                } else if ($row['status'] == 1) {
                                    echo "Active";
                                } else {
                                    echo "Ended";
                                }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
        
        <script>
        // datatable init
        (function( $ ) {
            'use strict';

            var jobFromDate = '<?php echo $datainputFromDate; ?>';
            var jobToDate = '<?php echo $datainputToDate; ?>';

            var jobsdatatableInit3 = function() {

                var $table = $('#datatable-jobsReport-allBusinesses');

                $table.DataTable({
                    dom: "<'row mt-md mb-xs'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12 col-xs-12 col-md-12't>><'row mt-xs'<'col-md-4 col-sm-4 col-lg-4 col-xs-4'l><'col-md-8 col-xs-8 col-sm-8'p>>",
                    buttons: [
                            {
                                extend: 'excelHtml5',
                                filename: 'fast-service-all-client-created-jobs-from-'+jobFromDate+'-to-'+jobToDate,
                                title: "Fast Service - Jobs",
                                messageTop: "From "+jobFromDate+' To '+jobToDate,
                                text: ' Export to Excel',
                                className: "btn btn-flat fa fa-file-excel-o bg-maroon"
                            }
                        ]
                });
            };

            // function calls
            $(function() {
                jobsdatatableInit3();
            });

        }).apply( this, [ jQuery ]);
        
        </script>

            <?php
        } else {
            echo "<div class='alert alert-sm alert-info text-center'><strong class='text-maroon'>Info: No results available based on dates parameters selected!</strong></div>";
        }

    } // end


    /**
     * Default adverts reports list
     *
     * @return void
     * @access public
     */
    function advertReportDefaultViewList() {

        $advertQ1 = $this->conn->prepare("select advert_post_id, date_format(date_post, '%d-%m-%Y') as date_created, date_format(start_period, '%d-%m-%Y') as start_date, date_format(end_period, '%d-%m-%Y') as end_date, approved, status, publish, advert_title, loggedEmail, Business_Name from advert_posts order by advert_post_id asc");
        $advertQ1->execute();
        $advert1 = $advertQ1->fetchAll();
        if ($advert1) {

        ?>
    <div class="table-responsive">
        
        <table class="table table-bordered table-condensed table-striped mb-none" id="datatable-advertReports" style="width: 100%">
            <thead class="bg-olive">
                <tr>
                    <th>#</th>
                    <th>Created On</th>
                    <th>Starts on</th>
                    <th>Expires On</th>
                    <th>Business Name</th>
                    <th>Job Title</th>
                    <th>Owner</th>
                    <th>Approved</th>
                    <th>Current Status</th>
                </tr>
            </thead>
            <tbody class="mdb-color-text">
                <?php foreach($advert1 as $row){ ?>
                    <tr>
                        <td>
                            <?php echo $row['advert_post_id']; ?>
                        </td>
                        <td>
                            <?php echo $row['date_created']; ?>
                        </td>
                        <td>
                            <?php echo $row['start_date']; ?>
                        </td>
                        <td>
                            <?php echo $row['end_date']; ?>
                        </td>
                        <td>
                            <?php echo $row['Business_Name']; ?>
                        </td>
                        <td>
                            <?php echo $row['advert_title']; ?>
                        </td>
                        <td>
                            <?php echo $row['loggedEmail']; ?>
                        </td>
                        <td class="text-capitalize">
                            <?php echo $row['approved']; ?>
                        </td>
                        <td>
                            <?php 
                                if($row['status'] == 0) {
                                    echo "Inactive";
                                } else if ($row['status'] == 1) {
                                    echo "Active";
                                } else {
                                    echo "Ended";
                                }
                            ?>
                        </td>
                    </tr>
                <?php } } ?>
            </tbody>
        </table>

    </div>
        
        <script>
        // datatable init
        (function( $ ) {
            'use strict';

             var advertsDatatableInit = function() {

                var $table = $('#datatable-advertReports');
                $table.DataTable({
                    dom: "<'row mt-md mb-xs'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12 col-xs-12 col-md-12't>><'row mt-xs'<'col-md-4 col-sm-4 col-lg-4 col-xs-4'l><'col-md-8 col-xs-8 col-sm-8'p>>",
                    buttons: [
                                {
                                    extend: 'excelHtml5',
                                    filename: 'fast-service-all-client-created-adverts',
                                    className: "btn btn-flat fa fa-file-excel-o bg-maroon",
                                    text: ' Export to Excel',
                                    title: "Fast Service - Adverts",
                                    messageTop: "All Client Adverts" 
                                }/*,
                                {
                                    extend: 'pdfHtml5',
                                    text: ' Export to PDF',
                                    filename: 'fast-service-all-client-created-adverts',
                                    title: 'Fast Service',
                                    messageTop: 'All Client Adverts',
                                    className: 'btn bg-fuchsia btn-flat fa fa-file-pdf-o',
                                    pageSize: 'A4',
                                    orientation: "portrait",
                                    customize: function(doc) {
                                        doc.defaultStyle.fontSize = 9;
                                        doc.styles.tableHeader.fontSize = 10;
                                        doc.content.forEach(function(item) {
                                        })
                                    }
                                }*/
                            ]
                    });
                };


            // function calls
            $(function() {
                advertsDatatableInit();
            });

        }).apply( this, [ jQuery ]);
        
        </script>
      <?php  

    } // end


    /**
     * Adverts reports filter
     *
     * @param string $datainputFromDate from date
     * @param string $datainputToDate to date
     * @param string $dataselectadvertsBusinessid business tbl primary key
     * @return void
     * @access public
     */
    function advertsReportsDateFilter($datainputFromDate, $datainputToDate, $dataselectadvertsBusinessid) {

        if ($dataselectadvertsBusinessid == "all") {
            $this->_advertsReportsAllBusinesses($datainputFromDate, $datainputToDate);
        } // all businesses
        else {
            $this->_advertsReportsSelectedBusinesses($datainputFromDate, $datainputToDate, $dataselectadvertsBusinessid);
        } // else // selected business

    } // end


    /**
     * Adverts reports - we're fetching all client created adverts based on selected 'from to' date parameters
     *
     * @param string $datainputFromDate the from date parameter
     * @param string $datainputToDate the to date parameter
     *
     * @return void
     * @access private
     */
    private function _advertsReportsAllBusinesses($datainputFromDate, $datainputToDate) {

        $advertQ = $this->conn->prepare("select advert_post_id, date_format(date_post, '%d-%m-%Y') as date_created, date_format(start_period, '%d-%m-%Y') as start_date, date_format(end_period, '%d-%m-%Y') as end_date, approved, status, advert_title, loggedEmail, Business_Name from advert_posts where (date_post between :datainputFromDate and :datainputToDate) order by advert_post_id asc");
        $advertQ->execute([
            "datainputFromDate" => $datainputFromDate,
            "datainputToDate" => $datainputToDate
        ]);
        $advert = $advertQ->fetchAll();

        if ($advert) {

        ?>
    <div class="table-responsive">
        
        <table class="table table-bordered table-condensed table-striped mb-none" id="datatable-advertReports-allbusinesses" style="width: 100%">
            <thead class="bg-olive">
                <tr>
                    <th>#</th>
                    <th>Created On</th>
                    <th>Starts on</th>
                    <th>Expires On</th>
                    <th>Business Name</th>
                    <th>Job Title</th>
                    <th>Owner</th>
                    <th>Approved</th>
                    <th>Current Status</th>
                </tr>
            </thead>
            <tbody class="mdb-color-text">
                <?php foreach($advert as $row){ ?>
                    <tr>
                        <td>
                            <?php echo $row['advert_post_id']; ?>
                        </td>
                        <td>
                            <?php echo $row['date_created']; ?>
                        </td>
                        <td>
                            <?php echo $row['start_date']; ?>
                        </td>
                        <td>
                            <?php echo $row['end_date']; ?>
                        </td>
                        <td>
                            <?php echo $row['Business_Name']; ?>
                        </td>
                        <td>
                            <?php echo $row['advert_title']; ?>
                        </td>
                        <td>
                            <?php echo $row['loggedEmail']; ?>
                        </td>
                        <td class="text-capitalize">
                            <?php echo $row['approved']; ?>
                        </td>
                        <td>
                            <?php 
                                if($row['status'] == 0) {
                                    echo "Inactive";
                                } else if ($row['status'] == 1) {
                                    echo "Active";
                                } else {
                                    echo "Ended";
                                }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
        
        <script>
        // datatable init
        (function( $ ) {
            'use strict';

            var advertFromDate = '<?php echo $datainputFromDate; ?>';
            var advertToDate = '<?php echo $datainputToDate; ?>';

            var advertsDatatableInit2 = function() {
                
                var $table = $('#datatable-advertReports-allbusinesses');

                $table.DataTable({
                    dom: "<'row mt-md mb-xs'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12 col-xs-12 col-md-12't>><'row mt-xs'<'col-md-4 col-sm-4 col-lg-4 col-xs-4'l><'col-md-8 col-xs-8 col-sm-8'p>>",
                    buttons: [
                            {
                                extend: 'excelHtml5',
                                filename: 'fast-service-client-created-adverts-from-'+advertFromDate+'-to-'+advertToDate,
                                title: "Fast Service - Adverts",
                                messageTop: 'From '+advertFromDate+' To '+advertToDate,
                                text: ' Export to Excel',
                                className: "btn btn-flat fa fa-file-excel-o bg-maroon"
                            }
                        ]
                });
            };

            // function calls
            $(function() {
                advertsDatatableInit2();
            });

        }).apply( this, [ jQuery ]);
        
        </script>
      <?php  
            }  else {
            echo "<div class='alert alert-sm alert-info text-center'><strong class='text-maroon'>Info: No results available based on dates parameters selected!</strong></div>";
        } // else

    } // end


    /**
     * Adverts reports - we're fetching all the adverts created by a selected business
     *
     * @param string $datainputFromDate the from date parameter
     * @param string $datainputToDate the to date parameter
     * @param string $dataselectadvertsBusinessid the selected business
     *
     * @return void
     * @access private
     */
    private function _advertsReportsSelectedBusinesses($datainputFromDate, $datainputToDate, $dataselectadvertsBusinessid) {

        $advertQ = $this->conn->prepare("SELECT advert_post_id, date_format(date_post, '%d-%m-%Y') as date_created, date_format(start_period, '%d-%m-%Y') as start_date, date_format(end_period, '%d-%m-%Y') as end_date, approved, status, advert_title, loggedEmail, Business_Name from advert_posts where (date_post between :datainputFromDate and :datainputToDate) and business_profile_id = :business_profile_id order by advert_post_id asc");
        $advertQ->execute([
            "datainputFromDate" => $datainputFromDate,
            "datainputToDate" => $datainputToDate,
            "business_profile_id" => $dataselectadvertsBusinessid
        ]);
        $advert = $advertQ->fetchAll();

        if ($advert) {

        ?>
    <div class="table-responsive">
        
        <table class="table table-bordered table-condensed table-striped mb-none" id="datatable-advertReports-selectedBusiness" style="width: 100%">
            <thead class="bg-olive">
                <tr>
                    <th>#</th>
                    <th>Created On</th>
                    <th>Starts on</th>
                    <th>Expires On</th>
                    <th>Job Title</th>
                    <th>Owner</th>
                    <th>Approved</th>
                    <th>Current Status</th>
                </tr>
            </thead>
            <tbody class="mdb-color-text">
                <?php 
                    $bnameArray2 = array();

                    foreach($advert as $row){ 

                        $bnameArray2[0] = $row['Business_Name'];
                ?>
                    <tr>
                        <td>
                            <?php echo $row['advert_post_id']; ?>
                        </td>
                        <td>
                            <?php echo $row['date_created']; ?>
                        </td>
                        <td>
                            <?php echo $row['start_date']; ?>
                        </td>
                        <td>
                            <?php echo $row['end_date']; ?>
                        </td>
                        <td>
                            <?php echo $row['advert_title']; ?>
                        </td>
                        <td>
                            <?php echo $row['loggedEmail']; ?>
                        </td>
                        <td class="text-capitalize">
                            <?php echo $row['approved']; ?>
                        </td>
                        <td>
                            <?php 
                                if($row['status'] == 0) {
                                    echo "Inactive";
                                } else if ($row['status'] == 1) {
                                    echo "Active";
                                } else {
                                    echo "Ended";
                                }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>
        
        <script>
        // datatable init
        (function( $ ) {
            'use strict';

            var businessName = '<?php echo $bnameArray2[0]; ?>';
            var advertFromDate = '<?php echo $datainputFromDate; ?>';
            var advertToDate = '<?php echo $datainputToDate; ?>';

            var advertsDatatableInit3 = function() {
                
                var $table = $('#datatable-advertReports-selectedBusiness');

                $table.DataTable({
                    dom: "<'row mt-md mb-xs'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12 col-xs-12 col-md-12't>><'row mt-xs'<'col-md-4 col-sm-4 col-lg-4 col-xs-4'l><'col-md-8 col-xs-8 col-sm-8'p>>",
                    buttons: [
                                {
                                    extend: 'excelHtml5',
                                    filename: 'Fast-Service-adverts-Under-'+businessName+'-from-'+advertFromDate+'-to-'+advertToDate,
                                    title: "Fast Service - Adverts",                     
                                    messageTop: businessName+' From '+advertFromDate+' To '+advertToDate,
                                    text: ' Export to Excel',
                                    className: "btn btn-flat fa fa-file-excel-o bg-maroon"
                                }
                            ]
                });
            };

            // function calls
            $(function() {
                advertsDatatableInit3();
            });

        }).apply( this, [ jQuery ]);
        
        </script>
      <?php  
            }  else {
            echo "<div class='alert alert-sm alert-info text-center'><strong class='text-maroon'>Info: No results available based on business and dates parameters selected!</strong></div>";
        } // else

    } // end


    /**
     * reports business name
     *
     * @return object $g returning the business name and associated business tbl primary key
     * @access public
     */
    function businessinfo() {
        $g_q = $this->conn->prepare("SELECT Business_Name, business_profile_id from business_profile order by Business_Name asc");
        $g_q->execute();
        $g  = $g_q->fetchAll();
        return $g;
    }



    // -------------------- >> REPORTS ENDS << -------------------- // 
    
    
    // -------------------- >> GENERAL METHODS << ------------- //   

    /**
     * return inbox messages
     *
     * @return object $f_q1
     */
    private function _getInbox()
    {
        $f_q1 = "select * from msg_inbox";
        return $f_q1;
    } // _getInbox

    /**
     * return jobs
     *
     * @return object $f_q1
     */
    private function _getJobs()
    {
        $f_q1 = "select * from jobs";
        return $f_q1;
    } // _getJobs

    /**
     * return adverts
     *
     * @return object $f_q1
     */
    private function _getAdverts()
    {
        $f_q1 = "select * from advert_posts";
        return $f_q1;
    } // _getAdverts
    
    /**
     * return business information
     *
     * @return object $f_q1
     */
    private function _businessprofile()
    {
        $f_q1 = "select * from business_profile";
        return $f_q1;
    } // _businessprofile
    
    /**
     * format date
     *
     * @param string $deadlinedate
     * @return object $jobdeadline
     */
    private function _jobdeadlineDate($deadlinedate)
    {
        $jobdeadlineObj = new DateTime($deadlinedate);
        $jobdeadline    = $jobdeadlineObj->format('d M, Y');
        return $jobdeadline;
    } // end
    
    
    /**
     * feedbacks
     *
     * @return void
     * @access public
     */
    function feedbacks()
    {
        foreach($this->errors as $errs)
        {
            echo "<p class='text-danger'>".$errs."<p>";
        }
        foreach($this->mgss as $errs)
        {
            echo "<p class='text-success'>".$errs."<p>";
        }
    } // feedbacks   

    /**
     * return districts in uganda
     *
     * @return object $return_districts districts name in uganda
     * @access public
     */
    function _districts()
    {
        $return_districts_q = $this->conn->prepare("select district_name from districts order by district_name asc");
        $return_districts_q->execute();
        $return_districts  = $return_districts_q->fetchAll();

        return $return_districts;
    } // end 
    

} // end of class







