<?php

/**
 * the pagination class is required
 * 
 * this class has methods intended to provide pagination of information
 */
require_once "_paginationClass.php";

/**
 * the main class
 * 
 * this class extends to the pagination class since there're some
 * methods in it which we intend to use
 */
class globalClass extends Pagination
{

    // {{{ properties

    /**
     * the PDO database connection
     * 
     * @var object $conn
     * @access private
     */
    private $conn = null;

    /**
     * the MySQLi Procedural connection
     * 
     * @var object
     * @access private
     */
    private $db   = null;

    /**
     * the MySQLi Object-oriented connection
     * 
     * @var object
     * @access private
     */
    private $obj  = '';

    /**
     * error messages
     * 
     * @var object
     * @access private
     */
    private $errs = array();

    /**
     * success messages
     * 
     * @var array
     * @access private
     */
    private $mgss = array();

    /**
     * checking login status
     * 
     * @var bool
     * @access private
     */
    private $isLoggedIn = false;

    /**
     *  the session id for user login
     * 
     * @var int
     * @access private
     */
    private $user_id = null;

    /**
     * the user email session
     * 
     * @var string
     * @access private
     */
    private $user_email = "";

    /**
     * the user email (@domain truncated)
     * 
     * @var string
     * @access private
     */
    private $emailUname = "";

    /**
     * the advert banners upload directory
     * 
     * @var string
     * @access private
     */
    private $bannerDir = "././_assets/clientUploads/";

    /**
     * the general client upload directory
     * 
     * @var string
     * @access private
     */
    private $clientSideBannerDir = '_assets/clientUploads/';

    /**
     * image upload maximum width
     * 
     * @var integer
     * @access private
     */
    private $maxWidth = 500;

    /**
     * image upload maximum height
     * 
     * @var integer
     * @access private
     */
    private $maxHeight = 500;


    // }}}
    
    /**
     * the __constructor is the first method that runs whenever the class is instantiated
     * so we're calling the database connection methods here.
     * session is startd here also
     * we're also running logout code and login 
     */
    function __construct()
    {   
        // start session
        session_start();   

        // database connection method invokes
        $this->dbConnectPDO(); // pdo connection
        $this->dbConnectMysqliProced(); // mysqli procedural connection
        $this->objO(); // MySQLi Object-oriented
        
        // invokes adverts & jobs which have expired or have reached their running / start period
        $this->checkAdsandJobsApprovalsHaveExpiredorApproved();

        /*
        * check if user has an ongoing session or 
        * had originally logged in with cookie
        */
        if (isset($_GET['logout'])) {
           $this->_logout();
        }
        elseif (!empty($_SESSION['ur_email']) && ($_SESSION['user_logged_in'] == 1)) 
        {
            $this->loginWithSessionData();
        } 
        elseif (isset($_COOKIE['rememberme'])) 
        {
            $this->loginWithCookieData();        
        }        
    } // end

    /**
     * handles PDO database connection
     *
     * @return void
     * @access private
     */
    private function dbConnectPDO()
    {
        if($this->conn != null)
        {
            return true;
        }
        else
        {
            try
            {
                $this->conn = new PDO('mysql:host='. DB_HOST .';dbname='. DB_DATABASE . ';charset=utf8', DB_USER, DB_PASSWORD);
                // set the PDO error mode to exception
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $e)
            {
                $this->errs[] = "Connection failed: " . $e->getMessage();
            }
        }
        
    } // end


    /**
     * handles MySQLi Procedural database connection
     *
     * @return void
     * @access private
     */
    private function dbConnectMysqliProced()
    {
        if($this->db != null)
        {
            return true;
        }
        else
        {
            $this->db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die("Connection failed: " . mysqli_connect_error());
        }
    } // end


    /**
     * handles MySQLi Object-oriented database connection
     *
     * @return void
     * @access private
     */
    private function objO() {
        $this->obj = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
        // Check connection
        if ($this->obj->connect_error) {
            die("Connection failed: " . $this->obj->connect_error);
        }
    } // end

    /**
     * queries method that have expired or approved
     *
     * approved adverts or jobs are displayed and expired ones are removed from view. note that the method performs this irrespective of 
     * currently active session. this method runs in at __construct() as its the first thing that needs to be executed
     * 
     * @return void
     *                                     
     * @access private
     */
    private function checkAdsandJobsApprovalsHaveExpiredorApproved() {

        /*
        * check adverts approvals; 
        * the start period should be greated or less than today.
        * we're using CURDATE() function which returns date in YYYY-mm-dd format that is MYSQL readable 
        * and less or equal to the end period
        */
        $c_q1 = $this->conn->prepare($this->_adverts()." where approved = :approved and (start_period <= CURDATE() and CURDATE() <= end_period)");
        $c_q1->execute([
            "approved" => "yes"
        ]);
        $c1 = $c_q1->fetchAll();

        if ($c1) 
        {
            // if true, then call method that update adverts "publish" column.
            if ($this->_displayPublishedAdverts()) {
                
            } else {

            }
        } // 

        /*
        * adverts which have 
        *
        * the current date should be greater than the --end_period-- and approved = yes or no; it may be approved or not
        *
        */
        $c_q2 = $this->conn->prepare($this->_adverts()." where (approved = :approved or approved = :approved2) and CURDATE() > end_period");
        $c_q2->execute([
            "approved" => "yes",
            "approved2" => "no"
        ]);
        $c2 = $c_q2->fetchAll();

        if ($c2) {
            // if true, then call method that update adverts "publish" column.
            if ($this->_disableExpiredAdverts()) {
                //echo 'okay';
            } else {
               // echo 'failed';
            }    
        }

        /*
        * check jobs approvals
        * the date posted should be greated or less than today.
        * we're using CURDATE() function which returns date in YYYY-mm-dd format that is MYSQL readable 
        * and less or equal to the deadline
        *
        */
        $c_q3 = $this->conn->prepare($this->_jobs(). " where approved = :approved and status = :status and date_posted <= CURDATE() and CURDATE() <= deadline");
        $c_q3->execute([
            'approved' => "yes",
            'status' => 0
        ]);
        $c3 = $c_q3->fetchAll();

        if ($c3) 
        {
            // if true, then call method that update jobs "publish" column.
            if ($this->_displayPublishedJobs()) {
                
            } else {

            }
        }

        /*
        * jobs which have expired
        *
        * the current date should be greater than the deadline and approved = yes or no; it may be approved or not
        */
        $c_q4 = $this->conn->prepare($this->_jobs(). " where (approved = :approved or approved = :approved2)  and (CURDATE() > deadline)");
        $c_q4->execute([
            "approved" => "yes",
            "approved2" => "no"
        ]);
        $c4 = $c_q4->fetchAll();

        if ($c4) {
            // if true, then call method that update jobs "status" column.
            if ($this->_disableExpiredJobs()) {
                
            } else {

            }                
        }

    } // end


    /**
     * disables expired jobs
     *
     * @return bool TRUE if update was successful; FALSE if update was a failure
     * @access private
     */
    private function _disableExpiredJobs() {
        $update_q = $this->conn->prepare('update jobs set status = :status where (approved = :approved or approved = :approved2) and (CURDATE() > deadline)');
        $update_check = $update_q->execute([
            'status' => 2,
            'approved' => 'yes',
            'approved2' => 'no'
        ]);

        if ($update_check) {
            return true;
        } else {
            return false;
        }

    } // end


    /**
     * handles that update of jobs tbl under publish column
     * 
     * jobs which have been approved and have reached their start period are displayed by updating the status column to 1
     *
     * @return bool TRUE if update was successful; FALSE if update was a failure
     * @access private
     */
    private function _displayPublishedJobs() {
        $update_q = $this->conn->prepare('UPDATE jobs set status = :status where approved = :approved and (date_posted <= CURDATE() and CURDATE() <= deadline)');
        $update_check = $update_q->execute([
            'status' => 1,
            'approved' => 'yes'
        ]);

        if ($update_check) {
            return true;
        } else {
            return false;
        }

    } // end


    /**
     * handles that update of adverts tbl under publish column
     * 
     * adverts which have been approved and have reached their start period are displayed by updating the publish column to 'yes' 
     * and status to 1 - meaning running; 
     *
     * @return bool TRUE if update was successful; FALSE if update was a failure
     * @access private
     */
    private function _displayPublishedAdverts() {
        $update_ad_q1 = $this->conn->prepare('UPDATE advert_posts set publish = :publish, status = :status where approved = :approved and (start_period <= CURDATE() and CURDATE() <= end_period)');
        $update_ad_check1 = $update_ad_q1->execute([
            'publish' => 'yes',
            'status' => 1,
            'approved' => 'yes'
        ]);

        if ($update_ad_check1) {
            return true;
        } else {
            return false;
        }

    } // end

    /**
     * disable expired adverts
     * 
     * @return bool
     * @access private
     * 
     */
    private function _disableExpiredAdverts() {
        $update_ad_q = $this->conn->prepare('update advert_posts set publish = :publish, status = :status where (approved = :approved or approved = :approved2) and CURDATE() > end_period');
        $update_ad_check = $update_ad_q->execute([
            'publish' => 'no',
            'status' => 2,
            'approved' => 'yes',
            'approved2' => 'no'
        ]);

        if ($update_ad_check) {
            return true;
        } else {
            return false;
        }

    } // end


    /* -------------------------------------------------------- */
    /* dashboard stats
    /* -------------------------------------------------------- */

    /**
     * number of services
     *
     * @return void
     * @access public
     */
    function _numTotalServices()
    {
        /*
        $g_q2 = $this->conn->prepare("select count(*) as numServices from services where serviceEntrantEmail = :serviceEntrantEmail");
        $g_q2->execute([
            "serviceEntrantEmail" => $_SESSION['ur_email']
        ]);
        $g2 = $g_q2->fetchObject();

        echo $g2 ? $g2->numServices : 0;
        */
        echo 0;
    } // end

    /**
     * number of customers / clients
     *
     * @return void
     * @access public
     */
    function _numClients()
    {
        echo 0;
    } // end

    /**
     * total revenue generated
     *
     * @return void
     * @access public
     */
    function _revenueGen()
    {
     echo '4,567';
    } // end

    /**
     * services of current date
     *
     * @return void
     * @access public
     */
    function _numServicesToday()
    {
        echo '2';
    } // end


    /* -------------------------------------------------------- */
    /* fetching returns
    /* -------------------------------------------------------- */


    /**
     * fetching from ur_login table
     *
     * @return object $f_q12 login info
     * @access private
     */
    private function _ur_login_table()
    {
        $f_q1 = "select * from ur_login";
        return $f_q1;
    } // end

    /**
     * business information
     *
     * @return object $f_q3 business profile
     * @access private
     */
    private function _businessprofile()
    {
        $f_q3 = "select * from business_profile";
        return $f_q3;
    } // end


    /**
     * fetching client messages in inbox
     *
     * @return object $f_q2 messages inbox
     * @access private
     */
    private function _msg_inbox()
    {
        $f_q2 = "select * from msg_inbox";
        return $f_q2;
    } // end


    /**
     * fetching client messages in sent
     *
     * @return object $f_q4 messages sent
     * @access private
     */
    private function _msg_outbox()
    {
        $f_q4 = "select * from msg_outbox";
        return $f_q4;
    } // end


    /**
     * fetching client messages in bin
     *
     * @return object $f_q5 messages bin
     * @access private
     */
    private function _msg_bin()
    {
        $f_q5 = "select * from msg_bin";
        return $f_q5;
    } // end

    /**
     * fetching from user_profile table
     *
     * @return object $f_q6 client profile
     * @access private
     */
    private function user_profile()
    {
        $f_q6 = "select * from user_profile";
        return $f_q6;
    } // end

    /**
     * fetching business categories
     *
     * @return object $f_q7 categories
     * @access private
     */
    private function _categories()
    {
        $f_q7 = "select category_id, category_name, industry_id from categories";
        return $f_q7;
    } // end

    /**
     * fetching services offered
     *
     * @return object $f_q8 services
     * @access private
     */
    private function _services()
    {
        $f_q8 = "select service_id, industry_id, category_id, service_name from services";
        return $f_q8;
    } // end

    /**
     * fetching business profile
     *
     * @return object $f_q9 business profile
     * @access private
     */
    private function _business_profile()
    {
        $f_q9 = "select business_profile_id, Business_Name, Business_Address, Business_Phone, Business_Email, Business_Category, Services_Offered, Geographical_Area, vander_latitude, vander_longitude, date_time_creation from business_profile";
        return $f_q9;
    } // end

    /**
     * fetching jobs
     *
     * @return object $f_q10 jobs
     * @access private
     */
    private function _jobs()
    {
        $f_q10 = "SELECT job_id,business_profile_id,job_vacancy,location,job_type,job_qualification,salary, application_mode, date_posted, deadline, duration FROM jobs";
        return $f_q10;
    } // end

    /**
     * fetching all adverts
     *
     * @return object $f_q11 advert posts
     * @access private
     */
    private function _adverts()
    {
        $f_q11 = "select * from advert_posts";
        return $f_q11;
    } // end

    /**
     * advert likes stats
     *
     * @return object $f_q12 advert likes
     * @access private
     */
    private function _advertLikeStats()
    {
        $f_q12 = "select * from advert_likes";
        return $f_q12;    
    } // end

    /**
     * advert views stats
     *
     * @return object $f_q12 advert views
     * @access private
     */
    private function _advertViewStats()
    {
        $f_q12 = "select * from advert_views";
        return $f_q12;    
    } // end

    /* -------------------------------------------------------- */
    /* arguments manipulations
    /* -------------------------------------------------------- */

    /**
     * changing date format
     *
     * @param string $dateArg date which we need to change its format
     * @return string $returnDate the now new date format; ie day month, year
     * @access private
     */
    private function _returnDateFormat($dateArg)
    {    
        $returnDateObj = new DateTime($dateArg);
        $returnDate = $returnDateObj->format('d M, Y'); // day month, year eg. 9 April, 2019

        return $returnDate;
    }  // end



    /* -------------------------------------------------------- */
    /* server interactions
    /* -------------------------------------------------------- */

    /**
     * profile picture
     *
     * @return string $get_session_pic->profile_picture the client profile image name; else return 'user.png'
     *                                                  which is no picture image
     * @access public
     */
    function sessionProfilePicture()
    {
        /*
        $_globalObj->_sessionProfilePicture()
        <?php echo '_assets/clientUploads/profile/'.$_globalObj->_sessionProfilePicture(); ?>
        */

        $get_session_pic_q = $this->conn->prepare("select profile_picture from user_profile where ur_email = :ur_email");
        $get_session_pic_q->execute([
            "ur_email" => $_SESSION['ur_email']
        ]);
        $get_session_pic = $get_session_pic_q->fetchObject();
        if ($get_session_pic) 
        {
           //echo '_assets/clientUploads/profile/'.$get_session_pic->profile_picture;
            return $get_session_pic->profile_picture ? $get_session_pic->profile_picture : 'nophoto.jpg';
        }
        else
        {
            //echo "_assets/clientUploads/profile/nophoto.jpg";
            return 'nophoto.jpg';
        }

    } // end


    /**
     * my favorite jobs datatable
     *
     * @return void
     * @access public
     */
    function _favoritejobsList()
    {
    //SELECT `job_id`, `have_business`, `business_profile_id`, `job_vacancy`, `job_district`, `location`, `job_type`, `job_qualification`, `salary`, `application_mode`, `date_posted`, `deadline`, `duration`, `loggedin_email`

        $loggedInEmail = $_SESSION['ur_email'];

            $columns = array( 
            // datatable column index  => database column name
                0 => 'date_posted',
                1 => 'job_vacancy', 
                2 => 'job_district',
                3 => 'location',
                4 => 'job_type',
                5 => 'deadline'
            );

            // getting total number records without any search
            $a1_Query = "select job_id, job_vacancy, job_district,
     location, job_type, date_posted, deadline ";
            $a1_Query.= " from jobs where loggedin_email = '$loggedInEmail'";
            $query = mysqli_query($this->db, $a1_Query) or die("Unknown Error Occured!");

            $totalData = mysqli_num_rows($query);
            $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.

        
            $a1_Query = "select job_id, job_vacancy, job_district,
     location, job_type, date_posted, deadline ";
            $a1_Query.=" from jobs WHERE 1=1 and loggedin_email = '$loggedInEmail' ";

            if( !empty($_REQUEST['search']['value']) ) { 
                $a1_Query.=" AND ( job_vacancy LIKE '".$_REQUEST['search']['value']."%' ";    
                $a1_Query.=" OR job_district LIKE '".$_REQUEST['search']['value']."%' ";
                $a1_Query.=" OR job_type LIKE '".$_REQUEST['search']['value']."%' ";
                $a1_Query.=" OR location LIKE '".$_REQUEST['search']['value']."%' ";
                $a1_Query.=" OR date_posted LIKE '".$_REQUEST['search']['value']."%' )";
            }
            $query=mysqli_query($this->db, $a1_Query) or die("Unknown Error Occured!");
            $totalFiltered = mysqli_num_rows($query);
            $a1_Query.=" ORDER BY ". $columns[$_REQUEST['order'][0]['column']]."   ".$_REQUEST['order'][0]['dir']."  LIMIT ".$_REQUEST['start']." ,".$_REQUEST['length']."   ";
        
            $query=mysqli_query($this->db, $a1_Query) or die("Unknown Error Occured!");

        $data = array();

    while( $row=mysqli_fetch_array($query) ) {  // preparing an array
        
        $nestedData =   array(); 

        /*
        $dt_q1 = mysqli_query($this->db, $this->_businessprofile()." where business_profile_id = ".$row['business_profile_id']);

        $dt1 = mysqli_fetch_assoc($dt_q1);
        */

        // get fav jobs
        $get_fav_jobs_q = $this->conn->prepare("select * from favorites where job_id = :job_id and email_address = :email_address and fav_status = :fav_status");
        $get_fav_jobs_q->execute([
            "job_id" => $row['job_id'],
            "email_address" => $loggedInEmail,
            "fav_status" => 1
        ]);
        $get_fav_jobs = $get_fav_jobs_q->fetchAll();

        // == 
        if($get_fav_jobs)
        {

            $ViewJob = '<a href="#dataModalJobs" data-toggle="modal" title="view job" onclick="jobDetailsfunc('.$row['job_id'].')" class="btn btn-info btn-flat btn-xs">View Details</a> - ';
            $removeFromFav = '<href="javascript:void(0)" title="add this job to favorites" onclick="removeFromFavFunction('.$row['job_id'].')" class="btn bg-maroon btn-flat btn-xs" id="data-rem-favorite'.$row['job_id'].'">Remove from favorites</a>';

            $nestedData[] = $row["date_posted"];
            $nestedData[] = $row["job_vacancy"];
            $nestedData[] = $row["job_district"];
            $nestedData[] = $row["location"];
            $nestedData[] = $row["job_type"];
            $nestedData[] = $row["deadline"];
            $nestedData[] = $ViewJob.$removeFromFav;

            $data[] = $nestedData;

        }
    }

        $json_data = array(
                    "draw"            => intval( $_REQUEST['draw'] ),
                    "recordsTotal"    => intval( $totalData ),
                    "recordsFiltered" => intval( $totalFiltered ),
                    "data"            => $data
                    );

        echo json_encode($json_data); 

    } // end


    /**
     * add job to favorites
     * 
     * @param int $jobIdToAddToFavorites the job id to added to favorites
     * 
     * @return void
     */
    function _addingJobTofavorites($jobIdToAddToFavorites)
    {
        // check if current user has already added job to favorites
        $check_if_added_to_favorities_q = $this->conn->prepare("select fav_status from favorites where job_id = :job_id and email_address = :email_address");
        $check_if_added_to_favorities_q->execute([
            "job_id" => $jobIdToAddToFavorites,
            "email_address" => $_SESSION['ur_email']
        ]);
        $check_if_added_to_favorities = $check_if_added_to_favorities_q->fetchObject();

        // if true; already in favorites; so remove from favorites; just delete
        if ($check_if_added_to_favorities->fav_status == 1) 
        {
            /*
            $rem_from_favorites_q = $this->conn->prepare("update favorites set fav_status = :fav_status where email_address = :email_address and job_id = :job_id");
            $rem_from_favorites = $rem_from_favorites_q->execute([
                "fav_status" => 0,
                "job_id" => $jobIdToAddToFavorites,
                "email_address" => $_SESSION['ur_email']
            ]);
            */
            $rem_from_favorites_q = $this->conn->prepare("delete from favorites where email_address = :email_address and job_id = :job_id");
            $rem_from_favorites = $rem_from_favorites_q->execute([
                "job_id" => $jobIdToAddToFavorites,
                "email_address" => $_SESSION['ur_email']
            ]);
            
            if ($rem_from_favorites) 
            {
            echo 1;
            }
            else
            {
                echo 2;
            }
        }
        // if false; insert as new favorite
        else
        {
            $add_to_favorites_q = $this->conn->prepare("insert into favorites(job_id, email_address, fav_status) values (:job_id, :email_address, :fav_status)");
            $add_to_favorites = $add_to_favorites_q->execute([
                "job_id" => $jobIdToAddToFavorites,
                "email_address" => $_SESSION['ur_email'],
                "fav_status" => 1
            ]);
            
            if ($add_to_favorites) 
            {
            echo 3;
            }
            else
            {
                echo 4;
            }
        } // else

    } // end



    /**
     * displays list of jobs
     * 
     * @return void
     */
    function _returnJobList() {

        $job_list_query = $this->conn->prepare("select job_id, business_profile_id, job_vacancy,location, job_type, salary, date_posted, deadline from jobs where approved = :approved and status = :status and date_posted <= CURDATE() and CURDATE() <= deadline order by job_vacancy");
        $job_list_query->execute([
            'approved' => 'yes',
            'status' => 1
        ]);
        $job_list = $job_list_query->fetchAll();

        if ($job_list) 
        {
            
            $business_name_query = $this->conn->prepare("select Business_Name from business_profile where business_profile_id = :business_profile_id");
            $_check_if_fav_q = $this->conn->prepare("select email_address, job_id, fav_status from favorites where job_id = :job_id and email_address = :email_address");

            // return  $job_list;
        foreach($job_list as $row) 
        {
            // get business name
            $business_name_query->execute([
            "business_profile_id" => $row['business_profile_id']
            ]);
            $business_name = $business_name_query->fetchObject();  


            // get if added to favorites
            $_check_if_fav_q->execute([
                "job_id" => $row['job_id'],
                "email_address" => $_SESSION['ur_email']
            ]);
            $_check_if_fav = $_check_if_fav_q->fetchObject();


            // get date count since posted
            $todayDate = date('Y-m-d');

            $date1 = new DateTime($row['date_posted']);
            $date2 = new DateTime($todayDate);
            $days  = $date2->diff($date1)->format('%a');
        ?>  

            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">

                <!--
                    <div class="panel panel-default">
                    <div class="panel-heading">Panel Heading</div>
                    <div class="panel-body">Panel Content</div>
                    <div class="panel-footer">Panel Footer</div>
                    </div>
                -->

                <div class="card_container">

                <div class="card_header bg-olive">                          
                    <h3 class="job_title_tr"><?php echo $row['job_vacancy']; ?></h3>                       
                </div>

                <div class="card_container_body">
                    <div class="job-brief-details">                            
                        <p>
                        <?php echo $business_name ? $business_name->Business_Name : "<a href='#' class='text-info' data-title='This job has no business associated with it' data-toggle='tooltip'><i class='fa fa-question-circle'></i></a>"; ?>     
                    </p>
                        <p>     
                        <?php echo $row['location']; ?>                
                        </p>
                        <p>  
                        <?php echo $row['job_type']; ?>                  
                        </p>
                        <div class="row">
                            <div class="col-xs-6">
                                <p class="job_salary_footer_tr"> 
                                    <?php echo "<strong>Ush.</strong> ". $row['salary']; ?>                   
                                </p>                
                            </div>
                            <div class="col-xs-6">
                                <p class="job_salary_footer_tr text-right"> 
                                    <?php # echo "Posted: " . $days . 'd'; ?>
                                    <?php echo "Posted: " . $row['date_posted']; ?> 
                                </p>                
                            </div>
                        </div>
                        <div class="list-divider"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <p class="job_salary_footer_tr2"> 
                                        <a title="Click for more details" href="#dataModalJobs" data-toggle="modal" onclick="jobDetailsfunc('<?php echo $row['job_id']; ?>')" class="text-maroon">View Details</a>                  
                                    </p>                
                                </div>
                                <div class="col-xs-6">
                                    <p class="job_salary_footer_tr2 text-right"> 
                                        <a href="javascript:void(0)" title="add this job to favorites" onclick="addJobtofavfun('<?php echo $row['job_id']; ?>')" class="text-maroon" id="data-favorite<?php echo $row['job_id']; ?>">
                                            <?php echo $_check_if_fav->fav_status == 0 ? "Add to favorites" : "Remove from favorites";?>
                                        </a>
                                    </p>                
                                </div>
                            </div> 
                            <!-- row -->
                    </div>
                    <!-- advert-brief-details -->
                </div>
                <!-- card_container_body -->
                </div>
                <!-- card_container -->
                
            </div>
            <!-- col-sm-4 -->
                            
    <?php 
    }
        }
        else
        {
        // echo "<p class='text-center'>Info: no job list available!</p>";
        }
    } // end


    /**
     * job ordering filter
     * 
     * @param string $orderingValue ordering value; asc or desc
     * 
     * @return void
     */
    function _jobOrderFilter($orderingValue)
    {

        $job_list_query = $this->conn->prepare("select job_id, business_profile_id, job_district, job_vacancy, location, job_type, salary, date_posted, deadline from jobs where approved = :approved and status = :status and date_posted <= CURDATE() and CURDATE() <= deadline order by date_posted ".$orderingValue);
        $job_list_query->execute([
            'approved' => 'yes',
            'status' => 1
        ]);
        $job_list = $job_list_query->fetchAll();

        // return  $job_list;
        foreach($job_list as $row) 
        {
            // get business name
            $business_name_query = $this->conn->prepare("select Business_Name from business_profile where business_profile_id = :business_profile_id");
            $business_name_query->execute([
            "business_profile_id" => $row['business_profile_id']
            ]);
            $business_name = $business_name_query->fetchObject();  

            // get if added to favorites
            $_check_if_fav_q = $this->conn->prepare("select email_address, job_id, fav_status from favorites where job_id = :job_id and email_address = :email_address");
            $_check_if_fav_q->execute([
                "job_id" => $row['job_id'],
                "email_address" => $_SESSION['ur_email']
            ]);
            $_check_if_fav = $_check_if_fav_q->fetchObject();


            // get date count since posted
            $todayDate = date('Y-m-d');

            $date1 = new DateTime($row['date_posted']);
            $date2 = new DateTime($todayDate);
            $days  = $date2->diff($date1)->format('%a');

        ?>      
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">

                <div class="card_container">

                <div class="card_header bg-olive">                          
                    <h3 class="job_title_tr"><?php echo $row['job_vacancy']; ?></h3>                        
                </div>

                <div class="card_container_body">
                    <div class="job-brief-details">                            
                        <p>   
                        <?php echo $business_name ? $business_name->Business_Name : "<a href='#' class='text-info' data-title='This job has no business associated with it' data-toggle='tooltip'><i class='fa fa-question-circle'></i></a>"; ?>                
                        </p>
                        <p>     
                        <?php echo $row['location']; ?>                
                        </p>
                        <p>  
                        <?php echo $row['job_type']; ?>                  
                        </p>
                        <div class="row">
                            <div class="col-xs-6">
                                <p class="job_salary_footer_tr"> 
                                    <?php echo $row['salary']; ?>                   
                                </p>                
                            </div>
                            <div class="col-xs-6">
                                <p class="job_salary_footer_tr text-right"> 
                                    <?php echo $row['date_posted']; ?> 
                                </p>                
                            </div>
                        </div>
                        <div class="list-divider"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <p class="job_salary_footer_tr2"> 
                                        <a title="Click for more details" href="#dataModalJobs" data-toggle="modal" onclick="jobDetailsfunc('<?php echo $row['job_id']; ?>')" class="text-maroon">View Details</a>                  
                                    </p>                
                                </div>
                                <div class="col-xs-6">
                                    <p class="job_salary_footer_tr2 text-right"> 
                                        <a href="javascript:void(0)" title="add this job to favorites" onclick="addJobtofavfun('<?php echo $row['job_id']; ?>')" class="text-maroon" id="data-favorite<?php echo $row['job_id']; ?>">
                                            <?php echo $_check_if_fav->fav_status == 0 ? "Add to favorites" : "Remove from favorites";?>
                                        </a>
                                    </p>                
                                </div>
                            </div>
                    </div>
                </div>
                </div>
                <!-- card_container -->
                
            </div>
            <!-- col-sm-4 -->

                            
    <?php 
        } // foreach loop

    } // end



    /**
     * jobs search results filter
     * 
     * @param string $jobSearchKeyWord job search term
     * 
     * @return void
     */
    function _jobSearchFilter($jobSearchKeyWord)
    {

        $job_list_query = $this->conn->prepare("SELECT job_id, business_profile_id, job_district, job_vacancy, location, job_type, salary, date_posted, deadline from jobs where approved = :approved and status = :status and date_posted <= CURDATE() and CURDATE() <= deadline and (job_vacancy like '%".$jobSearchKeyWord."%' or job_district like '%".$jobSearchKeyWord."%' or location like '%".$jobSearchKeyWord."%' or job_type like '%".$jobSearchKeyWord."%' or date_posted like '%".$jobSearchKeyWord."%')");
        $job_list_query->execute([
            'approved' => 'yes',
            'status' => 1
        ]);
        $job_list = $job_list_query->fetchAll();

        if ( $job_list ) 
        {
            // return  $job_list;
        foreach($job_list as $row) 
        {
            // get business name
            $business_name_query = $this->conn->prepare("select Business_Name from business_profile where business_profile_id = :business_profile_id");
            $business_name_query->execute([
            "business_profile_id" => $row['business_profile_id']
            ]);
            $business_name = $business_name_query->fetchObject();    

            // get if added to favorites
            $_check_if_fav_q = $this->conn->prepare("select email_address, job_id, fav_status from favorites where job_id = :job_id and email_address = :email_address");
            $_check_if_fav_q->execute([
                "job_id" => $row['job_id'],
                "email_address" => $_SESSION['ur_email']
            ]);
            $_check_if_fav = $_check_if_fav_q->fetchObject();
        ?>      

            <div class="col-md-6 col-sm-12">

                <div class="card_container">

                <div class="card_header bg-olive">                          
                    <h3 class="job_title_tr"><?php echo $row['job_vacancy']; ?></h3>                        
                </div>

                <div class="card_container_body">
                    <div class="job-brief-details">                            
                        <p>   
                        <?php echo $business_name ? $business_name->Business_Name : "<a href='#' class='text-info' data-title='This job has no business associated with it' data-toggle='tooltip'><i class='fa fa-question-circle'></i></a>"; ?>                
                        </p>
                        <p>     
                        <?php echo $row['location']; ?>                
                        </p>
                        <p>  
                        <?php echo $row['job_type']; ?>                  
                        </p>
                        <div class="row">
                            <div class="col-xs-6">
                                <p class="job_salary_footer_tr"> 
                                    <?php echo "<strong>Ush.</strong> ". $row['salary']; ?>                   
                                </p>                
                            </div>
                            <div class="col-xs-6">
                                <p class="job_salary_footer_tr text-right"> 
                                    <?php echo $row['date_posted']; ?> 
                                </p>                
                            </div>
                        </div>
                        <div class="list-divider"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <p class="job_salary_footer_tr2"> 
                                        <a title="Click for more details" href="#dataModalJobs" data-toggle="modal" onclick="jobDetailsfunc('<?php echo $row['job_id']; ?>')" class="text-maroon">View Details</a>                  
                                    </p>                
                                </div>
                                <div class="col-xs-6">
                                    <p class="job_salary_footer_tr2 text-right"> 
                                        <a href="javascript:void(0)" title="add this job to favorites" onclick="addJobtofavfun('<?php echo $row['job_id']; ?>')" class="text-maroon" id="data-favorite<?php echo $row['job_id']; ?>">
                                            <?php echo $_check_if_fav->fav_status == 0 ? "Add to favorites" : "Remove from favorites";?>
                                        </a>
                                    </p>                
                                </div>
                            </div>
                    </div>
                </div>
                </div>
                <!-- card_container -->
                
            </div>
            <!-- col-sm-4 -->
                            
    <?php 
    }
        }
        else
        {
        echo "<p class='text-center'>Info: no jobs available based on search query!</p>";
        }

    } // end


    /**
     * search for services offered
     * 
     * @param string $search_param services search term
     * 
     * @return void
     */
    function _returnBusinessServicesOffered($search_param)
    {

        $s_q2 = $this->conn->prepare("select Services_Offered from business_profile where Services_Offered like '%".$search_param."%'");
        $s_q2->execute();
        
        $servicesOfferedData = array();

        while ($row = $s_q2->fetch(PDO::FETCH_ASSOC)) 
        {
            $servicesOfferedData[] = $row['Services_Offered'];
        }

        echo json_encode($servicesOfferedData);

    } // end

    /**
     * request for service saving
     * 
     * @param string $my_location service location
     * @param string $where_to_deliver_service where to deliver the service
     * @param string $sr_latlng
     * @param string $sr_longitude
     * @param string $sr_latitude
     * @param string $servicerequired the services required
     * @param string $loggedin_email current session email
     * @param string $quotation_details service quotation details
     * @param string $date_of_servicedelivery date of service delivery
     * @param string $time_of_servicedelivery time of service delivery
     * 
     * @return void
     */
    function _requestService($my_location, $where_to_deliver_service, $sr_latlng, $sr_longitude, $sr_latitude, $servicerequired, $loggedin_email, $quotation_details, $date_of_servicedelivery, $time_of_servicedelivery)
    {

        if (empty($my_location)) 
        {
        echo '<p class="text-center text-danger">'.EMPTY_SERVICE_LOCATION.'</p>';
        }
        elseif (empty($where_to_deliver_service)) 
        {
        echo '<p class="text-center text-danger">'.EMPTY_SERVICE_DELIVERY_POINT.'</p>';
        }
        elseif (empty($servicerequired)) 
        {
        echo '<p class="text-center text-danger">'.EMPTY_SERVICE_REQUIRE.'</p>';
        }
        elseif (empty($quotation_details)) 
        {
        echo '<p class="text-center text-danger">'.EMPTY_SERVICE_QUOT.'</p>';
        }
        elseif (empty($date_of_servicedelivery)) 
        {
        echo '<p class="text-center text-danger">'.EMPTY_SERVICE_DELIV_DATE.'</p>';
        }
        elseif (empty($time_of_servicedelivery)) 
        {
        echo '<p class="text-center text-danger">'.EMPTY_SERVICE_DELIV_TIME.'</p>';
        }
        elseif (strlen($quotation_details) > 200) 
        {
        echo '<p class="text-center text-danger">'.SERVICE_QUOT_MAX_200_EXCEEDED.'</p>';
        }
        else
        {

    /*
                    client_email,
                    location,
                    service_address,
                    service_needed,
                    area_selected,
                    quotation_details,
                    vander_appointed,
                    status,
                    vander_quotation,
                    longitude,
                    latitude,
                    vander_latitude,
                    vander_longitude,
                    time_taken,
                    date_delivery,
                    time_delivery) 
                values(
                    :client_email,
                    :location,
                    :service_address,
                    :service_needed,
                    :area_selected,
                    :quotation_details,
                    :vander_appointed,
                    :status,
                    :vander_quotation,
                    :longitude,
                    :latitude,
                    :vander_latitude,
                    :vander_longitude,
                    :time_taken,
                    :date_delivery,
                    :time_delivery)");

    */
            $i_q6 = $this->conn->prepare("insert into client_quotation(
                    client_email,
                    location,
                    service_address,
                    service_needed,
                    quotation_details,
                    date_delivery,
                    time_delivery) 
                values(
                    :client_email,
                    :location,
                    :service_address,
                    :service_needed,
                    :quotation_details,
                    :date_delivery,
                    :time_delivery)");

            $i6 = $i_q6->execute([
                    'client_email' => $loggedin_email,
                    'location' => $my_location,
                    'service_address' => $where_to_deliver_service,
                    'service_needed' => $servicerequired,
                    //'area_selected' => $ekglmrg,
                    'quotation_details' => $quotation_details,
                    //'vander_appointed' => $ekglmrg,
                    //'status' => $ekglmrg,
                // 'vander_quotation' => $quotation_details,
                // 'longitude' => $ekglmrg,
                // 'latitude' => $ekglmrg,
                // 'vander_latitude' => $ekglmrg,
                // 'vander_longitude' => $ekglmrg,
                    'date_delivery' => $date_of_servicedelivery,
                    'time_delivery' => $time_of_servicedelivery
            ]);

            if ($i6) 
            {
                echo '<p class="text-center text-success">'.SERVICE_REQUEST_SUBMITTED.'</p>';
                ?>
    <script>
        $(function(){
            //$("#dataForm_ServiceQuotation")[0].reset();
            setTimeout('window.location.href = "dashboard.php"; ',5000);
        })
    </script>
                <?php
            }
            else
            {
                echo '<p class="text-center text-danger">'.SERVICE_REQUEST_ERROR.'</p>';
            }

        } // >> else validation

    } // end


    /**
     * business profile list datatable
     */
    function _myBusinessProfileList()
    {
        $loggedInEmail = $_SESSION['ur_email'];

            $columns = array( 
            // datatable column index  => database column name
                0 => 'date_time_creation',
                1 => 'Business_Category', 
                2 => 'Business_Name',
            // 3 => 'Business_Address',
            // 3 => 'Business_Phone',
                3 => 'Services_Offered',
                4 => 'Geographical_Area'
            );

            // getting total number records without any search
            $a1_Query = "select business_profile_id, Business_Name, Business_Address, Business_Phone, Business_Email, Business_Category, Services_Offered, Geographical_Area, date_format(date_time_creation, '%d-%m-%Y') as date_creation ";
            $a1_Query.= " from business_profile where loggedin_email = '$loggedInEmail'";
            $query = mysqli_query($this->db, $a1_Query) or die("Unknown Error Occured!");

            $totalData = mysqli_num_rows($query);
            $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.

        
            $a1_Query = "select business_profile_id, Business_Name, Business_Address, Business_Phone, Business_Email, Business_Category, Services_Offered, Geographical_Area, date_format(date_time_creation, '%d-%m-%Y') as date_creation ";
            $a1_Query.=" from business_profile WHERE 1=1 and loggedin_email = '$loggedInEmail' ";

            if( !empty($_REQUEST['search']['value']) ) { 
                $a1_Query.=" AND ( Business_Name LIKE '".$_REQUEST['search']['value']."%' ";    
                $a1_Query.=" OR Business_Address LIKE '".$_REQUEST['search']['value']."%' "; 
                $a1_Query.=" OR Business_Email LIKE '".$_REQUEST['search']['value']."%' "; 
                $a1_Query.=" OR Business_Category LIKE '".$_REQUEST['search']['value']."%' ";
                $a1_Query.=" OR Geographical_Area LIKE '".$_REQUEST['search']['value']."%' ";
                $a1_Query.=" OR Services_Offered LIKE '".$_REQUEST['search']['value']."%' )";
            }
            $query=mysqli_query($this->db, $a1_Query) or die("Unknown Error Occured!");
            $totalFiltered = mysqli_num_rows($query);
            $a1_Query.=" ORDER BY ". $columns[$_REQUEST['order'][0]['column']]."   ".$_REQUEST['order'][0]['dir']."  LIMIT ".$_REQUEST['start']." ,".$_REQUEST['length']."   ";
        
            $query=mysqli_query($this->db, $a1_Query) or die("Unknown Error Occured!");

        $data = array();

    while( $row=mysqli_fetch_array($query) ) {  // preparing an array
        
        $nestedData =   array(); 

        // == 
        $deleteBusiness = '<a href="javascript:void(0);" onclick="deletebusinessFunction('.$row['business_profile_id'].')" title="delete business"><span class="label label-danger">Delete</span></a>';

        $editBusiness = '<a href="javascript:void(0);" title="edit business"><span class="label label-info">Edit</span></a>-';

        $ViewBusiness = '<a href="javascript:void(0);" title="view business"><span class="label label-success">View</span></a>-';

        $nestedData[] = $row["date_creation"];
        $nestedData[] = $row["Business_Category"];
        $nestedData[] = $row["Business_Name"];
        //$nestedData[] = $row["Business_Address"];
        //$nestedData[] = $row["Business_Phone"];
        $nestedData[] = $row["Services_Offered"];
        $nestedData[] = $row["Geographical_Area"];
        $nestedData[] = $ViewBusiness.$editBusiness.$deleteBusiness;

        $data[] = $nestedData;
    }

        $json_data = array(
                    "draw"            => intval( $_REQUEST['draw'] ),
                    "recordsTotal"    => intval( $totalData ),
                    "recordsFiltered" => intval( $totalFiltered ),
                    "data"            => $data
                    );

        echo json_encode($json_data); 
    } // end


    /**
     * handles the deletion of ones' business profile
     * 
     * @param int $businessProfilePkey the id associated with the business profile to be deleted
     * 
     * @return void
     */
    function _deleteMyBusiness($businessProfilePkey)
    {
        $d_q6 = $this->conn->prepare("delete from business_profile where business_profile_id = :business_profile_id");
        $d6   = $d_q6->execute([
            "business_profile_id" => $businessProfilePkey
        ]);
        if ($d6) 
        {
            echo "Business deleted!";
        }
        else
        {
            echo "Failed to delete business!";
        }

    } // end

    /**
     * return list of services offered
     * 
     * @return object $a6 if the services are available
     */
    function _servicesOffered()
    {
        $a_q6 = $this->conn->prepare($this->_services(). " order by service_name");
        $a_q6->execute();
        $a6   = $a_q6->fetchAll();

        if ($a6) 
        {
        return $a6;
        }
        else
        {
            echo "No services available!";
        }
    } // end

    /**
     * returns list of categories for businesses
     * 
     * @return object $a5 if the categories are availables;
     */
    function _businessCategories()
    {
        $a_q5 = $this->conn->prepare($this->_categories(). " order by category_name");
        $a_q5->execute();
        $a5   = $a_q5->fetchAll();

        if ($a5) 
        {
        return $a5;
        }
        else
        {
            echo "No categories available!";
        }
    } // end

    /**
     * show messages notification for current session
     * 
     * @param string $loggedEmailAddress current session email address
     */
    function _retrieveMessages($loggedEmailAddress)
    {

        $a_q1 = $this->conn->prepare($this->_msg_inbox()."  where inbox_email = :inbox_email order by inbox_date_time desc limit 4");
        $a_q1->execute([
            "inbox_email" => $loggedEmailAddress
        ]);
        $a1 = $a_q1->fetchAll();

        if ($a1) 
        {
        foreach ($a1 as $row) 
        {
            ?>
                <li>
                <a>
                    <span class="image"><img src="_assets/images/img.png" alt="Profile Image"></span>
                    <span>
                    <span><?php echo $row['sender_names']; ?></span>
                    <span class="time"><?php echo $this->time_elapsed_string($row['inbox_date_time']); ?></span>
                    </span>
                    <span class="message">
                    <?php echo strlen($row['inbox_message']) > 70 ? substr($row['inbox_message'], 0, 70).'...' : $row['inbox_message']; ?>
                    </span>
                </a>
                </li>
            <?php
        }
        }
        else
        {
            echo "<p class='text-center text-warning'>No new messages available</p>";
        }
        
    } // end


    /**
     * handles the filtering of messages dropdown
     */
    function _mailListfilter($mailListfilter) {

        if ($mailListfilter == 'inbox') {

            $this->_messagesInbox();
        } // showing inbox mail list

        else if ($mailListfilter == 'sent') {

            $this->_messagesSentBox();
        } // showing sent or outbox mail list

        else {//if ($mailListfilter == 'trash') {

            $this->_messagesBinBox();
        } // showing trash mail list

    } // end


    /**
     * shows lists of received messages
     * 
     * @return void
     * @access public
     */
    function _messagesInbox()
    {
        $m_q1 = $this->conn->prepare($this->_msg_inbox()."  where inbox_email = :inbox_email");
        $m_q1->execute([
            "inbox_email" => $_SESSION['ur_email']
        ]);
        $m1 = $m_q1->fetchAll();
    
        if ($m1) {
            
            foreach ($m1 as $row) {

                // read status
                $msgRead = $row['msg_read'] == 0 ? '<i class="fa fa-circle text-primary"></i>' : '<i class="fa fa-circle-o text-primary"></i>';
            ?>
            <a href="javascript:void(0)" onclick="showMessageBody('<?php echo $row['inbox_id']; ?>', '<?php echo $row['inbox_status']; ?>')" title="View mail" id="inbox_message_link<?php echo $row['inbox_id']; ?>">
                <div class="mail_list">
                <div class="left">
                    <span class="i_top" id="update_i_top<?php echo $row['inbox_id']; ?>" style="font-size: 14px;"><?php echo $msgRead; ?> </span>
                    <span class="i_bottom" style="font-size: 16px;"><i class="fa fa-angle-double-right text-warning hr_font_weight_600"></i></span>
                </div>
                <div class="right">
                    <h3><?php echo $row['sender_names']; ?> <small><?php echo $this->_mailListDateFormat($row['inbox_date_time']); ?></small></h3>
                    <p class="mail_subject"><?php echo strlen($row['inbox_subject']) > 50 ? substr($row['inbox_subject'], 0, 50).'...' : $row['inbox_subject']; ?></p>
                </div>
                </div>
            </a>

            <?php
            } // loop
        } // if
        else {
            echo "<p class='text-center text-warning'>No messages available</p>";
        }

    } // end


    /**
     * shows lists of sent messages
     * 
     * @return void
     * @access public
     */
    function _messagesSentBox()
    {
    //outbox_id, outbox_names, outbox_email, outbox_subject, outbox_message, outbox_date_time, outbox_status
        $a_q3 = $this->conn->prepare($this->_msg_outbox()."  where sender_email = :sender_email order by outbox_date_time desc");
        $a_q3->execute([
            "sender_email" => $_SESSION['ur_email']
        ]);
        $a3 = $a_q3->fetchAll();

        if ($a3) 
        {
        foreach ($a3 as $row) 
        {
            ?>
                <a href="javascript:void(0)" onclick="showMessageBody('<?php echo $row['outbox_id']; ?>', '<?php echo $row['outbox_status']; ?>')" id="sent_message_link<?php echo $row['outbox_id']; ?>">
                <div class="mail_list">
                    <div class="left">
                    <span class="i_top" style="font-size: 14px;"><i class="fa fa-dot-circle-o text-primary"></i> </span>
                    <span class="i_bottom" style="font-size: 15px;"><i class="fa fa-angle-double-right text-warning hr_font_weight_600"></i></span>
                    </div>
                    <div class="right">
                    <h3><?php echo $row['outbox_email']; ?></h3>
                    <p class="mail_subject"><?php echo strlen($row['outbox_subject']) > 50 ? substr($row['outbox_subject'], 0, 50).'...' : $row['outbox_subject']; ?></p>
                    <small><i class="fa fa-calendar text-teal"></i> <?php echo $this->_mailListDateFormat($row['outbox_date_time']); ?></small>
                    </div>
                </div>
                </a>
            <?php
        } // loop
        }
        else
        {
            echo "<p class='text-center text-warning'>No sent messages available</p>";
        }

    } // end


    /**
     * shows lists of bin messages
     * 
     * @return void
     * @access public
     */
    function _messagesBinBox()
    {
    //bin_id, bin_names, bin_email, bin_subject, bin_message, bin_date_time, msg_box, msg_date_time
        $a_q4 = $this->conn->prepare($this->_msg_bin()." where bin_email = :bin_email or sender_email = :sender_email order by bin_date_time desc");
        $a_q4->execute([
            "bin_email" => $_SESSION['ur_email'],
            "sender_email" => $_SESSION['ur_email']
        ]);
        $a4 = $a_q4->fetchAll();

        if ($a4) 
        {
        foreach ($a4 as $row) 
        {

            $msgCat = $row['msg_box'] == RECEIVED ? "Inbox" : "Outbox";

            if ($row['msg_box'] == RECEIVED) {

                //$msgRead = $row['msg_read'] == 0 ? '<i class="fa fa-circle text-primary"></i>' : '<i class="fa fa-circle-o text-primary"></i>';
    
            ?>
            <a href="javascript:void(0)" onclick="showMessageBody('<?php echo $row['bin_id']; ?>', '<?php echo $row['bin_status']; ?>')" title="View mail" id="bin_message_link<?php echo $row['bin_id']; ?>">
                <div class="mail_list">
                <div class="left">
                    <span class="i_top" style="font-size: 14px;"><i class="fa fa-circle-o text-primary"></i> </span>
                    <span class="i_bottom" style="font-size: 16px;"><i class="fa fa-angle-double-right text-warning hr_font_weight_600"></i></span>
                </div>
                <div class="right">
                    <h3><?php echo $row['sender_names']; ?></h3>
                    <p class="mail_subject"><?php echo strlen($row['bin_subject']) > 50 ? substr($row['bin_subject'], 0, 50).'...' : $row['bin_subject']; ?></p>
                    <small><span class="text-warning">From:</span> <?php echo $msgCat; ?> - <i class="fa fa-calendar text-warning"></i> <?php echo $this->_mailListDateFormat($row['msg_date_time']); ?></small>
                </div>
                </div>
            </a>

            <?php
                } // from inbox
                else {
                    ?>
                <a href="javascript:void(0)" onclick="showMessageBody('<?php echo $row['bin_id']; ?>', '<?php echo $row['bin_status']; ?>')" id="bin_message_link<?php echo $row['bin_id']; ?>">
                <div class="mail_list">
                    <div class="left">
                    <span class="i_top" style="font-size: 14px;"><i class="fa fa-dot-circle-o text-primary"></i> </span>
                    <span class="i_bottom" style="font-size: 15px;"><i class="fa fa-angle-double-right text-warning hr_font_weight_600"></i></span>
                    </div>
                    <div class="right">
                    <h3><?php echo $row['bin_email']; ?></h3>
                    <p class="mail_subject"><?php echo strlen($row['bin_subject']) > 50 ? substr($row['bin_subject'], 0, 50).'...' : $row['bin_subject']; ?></p>
                    <small><span class="text-warning">From:</span> <?php echo $msgCat; ?> - <i class="fa fa-calendar text-warning"></i> <?php echo $this->_mailListDateFormat($row['msg_date_time']); ?></small>
                    </div>
                </div>
                </a>
                <?php

                } // from outbox
        } // loop
        }
        else
        {
            echo "<p class='text-center text-warning'>No deleted messages!</p>";
        }

    } // @endof _messagesBinBox

    /**
     * handles the formatting of mail list dates
     * 
     * @param object $inbox_date_time the date to be formatted
     * 
     * @return object $msgDateObj formated date
     * @access private
     */
    private function _mailListDateFormat($inbox_date_time) 
    {
        // format date time
        $msgDateObj = new DateTime($inbox_date_time);
        return $msgDateObj->format('d M, Y');
    } // end

    /**
     * messages count for current session
     * 
     * @param string $loggedEmailAddress session email address
     * 
     * @return void
     * @access public
     */
    function _receivedMessagesCount($loggedEmailAddress)
    {
        $c_q1 = $this->conn->prepare("select count(*) as numNewMessages from msg_inbox where inbox_email = :inbox_email");
        $c_q1->execute([
            "inbox_email" => $loggedEmailAddress
        ]);
        $c1 = $c_q1->fetchObject();

        if ($c1) 
        {
            echo $c1->numNewMessages;
        }

    } // end
    

    /**
     * MAIN messages to viewing method
     * 
     * @param int $messagePkeyId message associated id
     * @param int $messageStatus status associated with each box
     * 
     * @return void
     * @access public
     */
    function _showSelectedMessagePreview($messagePkeyId, $messageStatus)
    {

    /*
    * $messageStatus => Sent, Received, Deleted
    */
        // --- >> inbox
        if ($messageStatus == RECEIVED) 
        {
        $this->_previewInbox($messagePkeyId);
        }
        // --- >> outbox
        elseif ($messageStatus == SENT) 
        {
            $this->_previewOutbox($messagePkeyId);
        }
        // --- >> bin
        elseif ($messageStatus == DELETED) 
        {
            $this->_previewBinbox($messagePkeyId);
        }

    } // @endof _showSelectedMessagePreview



/* ------------ previewing method calls ------------ */

    /**
     * previewing message from inbox
     * 
     * @param int $messagePkeyId message associated id
     * 
     * @return void
     * @access private
     */
    private function _previewInbox($messagePkeyId)
    {
        $p_q1 = $this->conn->prepare($this->_msg_inbox(). " where inbox_id = :inbox_id");
        $p_q1->execute([
            "inbox_id" => $messagePkeyId
        ]);
        $p1 = $p_q1->fetchObject();

        if ($p1) 
        {

    // update message read status
        $update_read_q = $this->conn->prepare("update msg_inbox set msg_read = :msg_read where inbox_id = :inbox_id");
        $update_read_q->execute([
            "msg_read" => 1,
            "inbox_id" => $messagePkeyId
        ]);

    // format date time
    $msgDateObj = new DateTime($p1->inbox_date_time);
    $msddate    =  $msgDateObj->format('h:i a d M Y');

    // variables
    $msg_status = $p1->inbox_status;

            ?>

        <div class="mail_heading row">
            <div class="col-md-8 noprint">
            <div class="btn-group">
                <!--<button class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Reply</button>
                <button class="btn btn-sm btn-flat bg-olive" type="button"  data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i> Forward</button>-->
                <button class="btn btn-sm btn-info btn-flat" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-sm btn-danger btn-flat" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash" onclick="deleteMessage('<?php echo $msg_status; ?>', '<?php echo $messagePkeyId; ?>')"><i class="fa fa-trash-o"></i> Delete</button>
            </div>
            </div>
            <div class="col-md-4 text-right">
            <p class="date"> <?php echo $msddate; ?></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <h3> <?php echo $p1->inbox_subject; ?></h3>
            </div>
        </div>

        <div class="sender-info">
            <div class="row">
            <div class="col-md-12">
                <strong><?php echo $p1->sender_names; ?></strong>
                <span>(<?php echo $p1->sender_email; ?>)</span> to
                <strong>me</strong>
                <a class="sender-dropdown" onclick="loadMailDetails()"><i class="fa fa-chevron-down"></i></a>
                <div class="element-hidden" id="mail-brief-details" style="display: none">
                    <small class="text-navy"><?php echo "Me: ". $p1->inbox_email; ?></small>
                </div>
            </div>
            </div>
        </div>
        <div class="view-mail">
            <p>
                <?php echo $p1->inbox_message; ?>
            </p>
        </div>
        <!-- view-mail ~ message body -->

            <?php
        }

    } // --- << endof inbox call

    /**
     * previewing message from outbox
     * 
     * @param int $messagePkeyId message associated id
     * 
     * @return void
     * @access private
     */
    private function _previewOutbox($messagePkeyId)
    {
        $p_q1 = $this->conn->prepare($this->_msg_outbox(). " where outbox_id = :outbox_id");
        $p_q1->execute([
            "outbox_id" => $messagePkeyId
        ]);
        $p1 = $p_q1->fetchObject();

        if ($p1) 
        {
        
        // format date time
    $msgDateObj = new DateTime($p1->outbox_date_time);
    $msddate    =  $msgDateObj->format('h:i a d M Y');

    // variables
    $msg_status = $p1->outbox_status;
            ?>

        <div class="mail_heading row">
            <div class="col-md-8 noprint">
            <div class="btn-group">
                <!--<button class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Reply</button>
                <button class="btn btn-sm btn-flat bg-olive" type="button"  data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i> Forward</button>-->
                <button class="btn btn-sm btn-info btn-flat" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-sm btn-danger btn-flat" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash" onclick="deleteMessage('<?php echo $msg_status; ?>', '<?php echo $messagePkeyId; ?>')"><i class="fa fa-trash-o"></i> Delete</button>
            </div>
            </div>
            <div class="col-md-4 text-right">
            <p class="date"> <?php echo $msddate; ?></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <h3> <?php echo $p1->outbox_subject; ?></h3>
            </div>
        </div>

        <div class="sender-info">
            <div class="row">
            <div class="col-md-12">
                <strong>me</strong> to
                <strong><?php echo $p1->outbox_email; ?></strong>
                <a class="sender-dropdown" onclick="loadMailDetails()"><i class="fa fa-chevron-down"></i></a>
                <div class="element-hidden" id="mail-brief-details" style="display: none">
                    <small class="text-navy"><?php echo "Me: ". $p1->sender_names . '-' .$p1->sender_email; ?></small>
                </div>
            </div>
            </div>
        </div>
        <div class="view-mail">
            <p>
                <?php echo $p1->outbox_message; ?>
            </p>
        </div>
        <!-- view-mail ~ message body -->

            <?php
        }

    } // --- << endof outbox call

    /**
     * previewing message from bin
     * 
     * @param int $messagePkeyId message associated id
     * 
     * @return void
     * @access private
     */
    private function _previewBinbox($messagePkeyId)
    {
        $p_q1 = $this->conn->prepare($this->_msg_bin(). " where bin_id = :bin_id");
        $p_q1->execute([
            "bin_id" => $messagePkeyId
        ]);
        $p1 = $p_q1->fetchObject();

        if ($p1) 
        {
        
    // format date time
    $msgDateObj = new DateTime($p1->msg_date_time);
    $msddate    =  $msgDateObj->format('h:i a d M Y');

    // variables
    $msg_status  = $p1->bin_status;
    $bin_id      = $p1->bin_id;

    if ($p1->msg_box == RECEIVED) {
            ?>

        <div class="mail_heading row">
            <div class="col-md-8 noprint">
            <div class="btn-group">
                <button class="btn btn-sm btn-info btn-flat" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-sm btn-danger btn-flat" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash" onclick="deleteMessage('<?php echo $msg_status; ?>', '<?php echo $bin_id; ?>')"><i class="fa fa-trash-o"></i> Delete</button>
            </div>
            </div>
            <div class="col-md-4 text-right">
            <p class="date"> <?php echo $msddate; ?></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <h3> <?php echo $p1->bin_subject; ?></h3>
            </div>
        </div>

        <div class="sender-info">
            <div class="row">
            <div class="col-md-12">
                <strong><?php echo $p1->sender_names; ?></strong>
                <span>(<?php echo $p1->sender_email; ?>)</span> to
                <strong>me</strong>
                <a class="sender-dropdown" onclick="loadMailDetails()"><i class="fa fa-chevron-down"></i></a>
                <div class="element-hidden" id="mail-brief-details" style="display: none">
                    <small class="text-navy"><?php echo "Me: ". $p1->bin_email; ?></small>
                </div>
            </div>
            </div>
        </div>
        <div class="view-mail">
            <p>
                <?php echo $p1->bin_message; ?>
            </p>
        </div>
        <!-- view-mail ~ message body -->

            <?php
            } // from inbox

            else {
                ?>

        <div class="mail_heading row">
            <div class="col-md-8 noprint">
            <div class="btn-group">
                <button class="btn btn-sm btn-info btn-flat" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-sm btn-danger btn-flat" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash" onclick="deleteMessage('<?php echo $msg_status; ?>', '<?php echo $messagePkeyId; ?>')"><i class="fa fa-trash-o"></i> Delete</button>
            </div>
            </div>
            <div class="col-md-4 text-right">
            <p class="date"> <?php echo $msddate; ?></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <h3> <?php echo $p1->bin_subject; ?></h3>
            </div>
        </div>

        <div class="sender-info">
            <div class="row">
            <div class="col-md-12">
                <strong>me</strong> to
                <strong><?php echo $p1->bin_email; ?></strong>
                <a class="sender-dropdown" onclick="loadMailDetails()"><i class="fa fa-chevron-down"></i></a>
                <div class="element-hidden" id="mail-brief-details" style="display: none">
                    <small class="text-navy"><?php echo "Me: ". $p1->sender_names . '-' .$p1->sender_email; ?></small>
                </div>
            </div>
            </div>
        </div>
        <div class="view-mail">
            <p>
                <?php echo $p1->bin_message; ?>
            </p>
        </div>
        <!-- view-mail ~ message body -->
        
            <?php
            } // from outbox

        }

    } // --- << endof bin box call

/* ------------ ------------ ------------ ---------- */


    /**
     * MAIN messsage deletions method client side
     * 
     * @param int $delMessageId message associated id
     * @param int $delMessageStatus message status to help identify which box to delete from and to
     * 
     * @return void
     * @access public
     */
    function _deleteMessage($delMessageId, $delMessageStatus)
    {
        if ($delMessageStatus == DELETED) 
        {
        
            $d_q2 = $this->conn->prepare("delete from msg_bin where bin_id = :bin_id");
            $d2   = $d_q2->execute([
                "bin_id" => $delMessageId
            ]);
            if ($d2) 
            {
                echo 1;
            }
            else
            {
                echo 3;     
            }
        }
        elseif ($delMessageStatus == SENT) 
        {
            if ($this->returnOutboxdata($delMessageId)) 
            {
                echo 2;
                $this->deleteFromOutbox($delMessageId);
            }
            else
            {
                echo 4;
            }
        }
        else if ($delMessageStatus == RECEIVED) 
        {  

        if ($this->returnInboxData($delMessageId)) 
            {
                echo 5;
                $this->deleteFromInbox($delMessageId);
            }
            else
            {
                echo 4;
            }  
        }

    } // end


    /**
     * temp delete from outbox to bin
     * 
     * @param int $delMessageId inbox message associated id
     * 
     * @return bool TRUE if deletion was successful; FALSE if deletion failed
     * @access private
     */
    private function returnOutboxdata($delMessageId)
    {
        $r_q5 = $this->conn->prepare($this->_msg_outbox(). " where outbox_id = :outbox_id");
        $r_q5->execute([
            "outbox_id" => $delMessageId
        ]);
        $r5 = $r_q5->fetchObject();
        if ($r5) 
        {
            
            $u_q1 = $this->conn->prepare("insert into msg_bin (msg_id, bin_email, bin_subject, bin_message, msg_box, msg_date_time, sender_names, sender_email) values (:msg_id, :bin_email, :bin_subject, :bin_message, :msg_box, :msg_date_time, :sender_names, :sender_email)");
            $u1  = $u_q1->execute([
                "msg_id" => $delMessageId,
                "bin_email" => $r5->outbox_email,
                "bin_subject" => $r5->outbox_subject,
                "bin_message" => $r5->outbox_message,
                "msg_box" => $r5->outbox_status,
                "msg_date_time" => $r5->outbox_date_time,
                "sender_names" => $r5->sender_names,
                "sender_email" => $r5->sender_email
            ]);
            if ($u1) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    } // end

    /**
     * delete from outbox to bin
     * 
     * @param int $delMessageId inbox message associated id
     * 
     * @return void
     * @access private
     */
    private function deleteFromOutbox($delMessageId)
    {
        $d_q4 = $this->conn->prepare("delete from msg_outbox where outbox_id = :outbox_id");
        $d4 = $d_q4->execute([
            "outbox_id" => $delMessageId
        ]);
    } // end

    /**
     * temp delete from inbox to bin
     * 
     * @param int $delMessageId inbox message associated id
     * 
     * @return bool TRUE if deletion was successful; FALSE if deletion failed
     * @access private
     */
    private function returnInboxData($delMessageId)
    {
        $r_q6 = $this->conn->prepare("select * from msg_inbox where inbox_id = :inbox_id");
        $r_q6->execute([
            "inbox_id" => $delMessageId
        ]);
        $r6 = $r_q6->fetchObject();
        if ($r6) 
        {
            
            $u_q2 = $this->conn->prepare("insert into msg_bin (msg_id, bin_email, bin_subject, bin_message, msg_box, msg_date_time, sender_names, sender_email) values (:msg_id, :bin_email, :bin_subject, :bin_message, :msg_box, :msg_date_time, :sender_names, :sender_email)");
            $u2  = $u_q2->execute([
                "msg_id" => $delMessageId,
                "bin_email" => $r6->inbox_email,
                "bin_subject" => $r6->inbox_subject,
                "bin_message" => $r6->inbox_message,
                "msg_box" => $r6->inbox_status,
                "msg_date_time" => $r6->inbox_date_time,
                "sender_names" => $r6->sender_names,
                "sender_email" => $r6->sender_email
            ]);
            if ($u2) 
            {
            return true;
            }
            else
            {
                return false;
            }        
        } // if
    } // end

    /**
     * deletes a message from inbox
     * 
     * @param int $delMessageId message id associated with the message
     * 
     * @return void
     * @access private
     */
    private function deleteFromInbox($delMessageId)
    {
        $d_q5 = $this->conn->prepare("delete from msg_inbox where inbox_id = :inbox_id");
        $d5 = $d_q5->execute([
            "inbox_id" => $delMessageId
        ]);
    } // end


    /**
     * retrieves jobs
     * 
     * @return void
     */
    function _retrieveJobs()
    {
        $r_q3 = $this->conn->prepare($this->_jobs()." where approved = :approved and status = :status and date_posted <= CURDATE() and CURDATE() <= deadline order by date_posted");
        $r_q3->execute([
            'approved' => 'yes',
            'status' => 1
        ]);
        $r3 = $r_q3->fetchAll();

        if ($r3) 
        {

            foreach($r3 as $row) { 

            $r_q4 = $this->conn->prepare($this->_businessprofile()." where business_profile_id = :business_profile_id");
            $r_q4->execute([
                "business_profile_id" => $row['business_profile_id']
            ]);
            $r4 = $r_q4->fetchObject();

            $jobpostedObj = new DateTime($row['date_posted']);
            $jobposted = $jobpostedObj->format('d M, Y');

        $deadlinedate = $row['deadline'] != 'N/A' ? $this->_jobdeadlineDate($row['deadline']) : 'N/A';


            ?> 
            <a href="#dataModalJobs" data-toggle="modal" onclick="jobDetailsfunc('<?php echo $row['job_id']; ?>')" title="Click for more details">
                <div class="panel_items">
                        
                    <h4 class="jobs_title"><strong>Title:</strong> <span class="job_sub_title"><?php echo $row['job_vacancy']; ?></span></h4>
                    <ul class="job_content">
                        <li><b>Type:</b> <span class="text-muted"><?php echo $row['job_type']; ?></span></li>
                        <li><b>Location:</b> <span class="text-muted"><?php echo $row['location']; ?></span></li>
                        <li><b>Posted:</b> <span class="text-muted"><?php echo $jobposted; ?></span></li>
                        <li><b>Deadline:</b> <span class="text-muted"><?php echo $deadlinedate; ?></span></li>
                    </ul>   

                </div>            
            </a>

            <?php 
            } 
        }
        else
        {
            echo "<p class='text-center text-info'>No Jobs available yet!</p>";
        }

    } // end


    /**
     * loads datatable for posted jobs
     * 
     * @return void
     */
    function _otherPostedJobs()
    {
        $r_q7 = $this->conn->prepare($this->_jobs()." order by date_posted");
        $r_q7->execute();
        $r7 = $r_q7->fetchAll();

        if ($r7) 
        {
            ?>
            <br>
    <table class="table table-bordered table-condensed table-hover dataTableGeneral">

        <thead class="bg-olive text-white">
            <th>Date Posted</th>
            <th>Title</th>
            <th>Company</th>
            <th>Location</th>
            <th>Salary</th>
            <th>Action</th>
        </thead>

        <tbody>
            
            <?php 
            $c1 = 1;
            foreach($r7 as $row) { 

            $r_q8 = $this->conn->prepare($this->_businessprofile()." where business_profile_id = :business_profile_id");
            $r_q8->execute([
                "business_profile_id" => $row['business_profile_id']
            ]);
            $r8 = $r_q8->fetchObject();

            $jobpostedObj = new DateTime($row['date_posted']);
            $jobposted    = $jobpostedObj->format('d M, Y');

        $deadlinedate = $row['deadline'] != 'N/A' ? $this->_jobdeadlineDate($row['deadline']) : 'N/A';
            ?> 
            <tr>
                <td><?php echo $jobposted; ?></td>
                <td><?php echo $row['job_vacancy']; ?></td>
                <td><?php echo $r8->Business_Name; ?></td>
                <td><?php echo $row['location']; ?></td>
                <td><?php echo $row['salary']; ?></td>
                <td>
                    <a href="#dataModalJobs" data-toggle="modal" onclick="jobDetailsfunc('<?php echo $row['job_id']; ?>')" title="Click for more details" class="btn btn-flat bg-blue btn-xs job_details_a">
                        View details
                    </a>                
                </td>
            </tr>

            <?php } ?>  
            
            
        </tbody>
        
    </table>

        <?php
        }
        else
        {
            echo "<p class='text-center text-info'>No Jobs available yet!</p>";
        }

    } // end

    /**
     * formats the job deadline date to clear format
     * 
     * @param object $deadlinedate the date which needs to be formatted
     * 
     * @return object $jobdeadline the formatted date
     * @access private
     */
    private function _jobdeadlineDate($deadlinedate)
    {
        $jobdeadlineObj = new DateTime($deadlinedate);
        $jobdeadline    = $jobdeadlineObj->format('d M, Y');

        return $jobdeadline;
    } // end


    /**
     * load selected job details in a modal
     * 
     * @param int $selectedJobId the job id associated with the job we want to load details
     * @return void
     * @access public
     */
    function _loadSelectedJobDetails($selectedJobId)
    {
        $f_q4 = $this->conn->prepare($this->_jobs()." where job_id = :job_id");
        $f_q4->execute([
            "job_id" => $selectedJobId
        ]);
        $f4 = $f_q4->fetchObject();

        if ($f4) 
        {

            // --- >> get business profile
            $f_q5 = $this->conn->prepare($this->_businessprofile()." where business_profile_id = :business_profile_id");
            $f_q5->execute([
                "business_profile_id" => $f4->business_profile_id
            ]);
            $f5 = $f_q5->fetchObject();

            // --- >> get responsibilities
            //$f_q12 = $this->conn->prepare("select responsibility from job_responsibility where business_profile_id = :business_profile_id and job_id = :job_id");
            $f_q12 = $this->conn->prepare("select responsibility from job_responsibility where job_id = :job_id");
            $f_q12->execute([
                "job_id" => $selectedJobId
            ]);
            $f12 = $f_q12->fetchAll();        


            $selectedjobpostedObj = new DateTime($f4->date_posted);
            $selectedjobposted    = $selectedjobpostedObj->format('d M, Y');

            $selectedjobdeadline = $f4->deadline != 'N/A' ? $this->_jobdeadlineDate($f4->deadline) : 'N/A';

            $jobDuration = $f4->duration != 'N/A' ? $f4->duration. ' | '  : '';

            ?>

            <div class="row">
                <div class="col-xs-3">
                    <h3 class="fa_job_icon"><i class="fa fa-briefcase fa-1x"></i></h3>
                </div>

                <div class="col-xs-9">
                    <h3><?php echo  $f4->job_vacancy; ?></h3>
                    <h4><?php echo  $f5->Business_Name; ?></h4>
                    <h5> 
                        <span><?php echo $jobDuration . $f4->job_type. ' | '. $f4->salary; ?></span>
                        <span class="duration_posted"><?php echo $selectedjobposted; ?></span>
                    </h5>
                </div>
            </div>
            <div class="ln_solid"></div>

            <div class="row">
                <div class="col-xs-12">                
                    <h4 class="text-navy">Job Summary</h4>
                    <p>
                        <span><b>Function:</b> <?php echo  $f4->job_function; ?></span><br>
                        <span><b>Deadline:</b> <?php echo $selectedjobdeadline; ?></span><br>
                        <span><b>Duty Station:</b> <?php echo  $f4->location; ?></span>                    
                    </p>
                    <h4 class="text-navy">Qualifications</h4>
                    <span class="text-justify">
                        <?php echo  $f4->job_qualification; ?>
                    </span>

                    <h4 class="text-navy">Key Duties and Responsibilities</h4>
                    <p class="text-justify">
                        <ul>
                        <?php foreach ($f12 as $row) 
                        {
                        ?>
                            <li><?php echo $row['responsibility']; ?></li>
                        <?php
                        } 
                        ?>
                        </ul>
                    </p>
                    <h4 class="text-navy">Job application procedure</h4>
                    <p class="text-justify">
                        <?php echo  $f4->application_mode; ?>
                    </p>
                </div>
            </div>
        


            <?php
        }
        else
        {
            $this->errs[] = FAILED_TO_LOAD_JOB_DETAILS;
        }
    } // end


    /**
     * query business profile names
     * 
     * @return object $f11 if successful; 'No Business created yet!' message
     * @access public
     */
    function _businessnames()
    {
    /*
        $f_q11 = $this->conn->prepare("select business_profile_id, Business_Name from business_profile where loggedin_email = :loggedin_email order by Business_Name");
        $f_q11->execute([
            "loggedin_email" => $_SESSION['ur_email']
        ]);
        */
        $f_q11 = $this->conn->prepare("select business_profile_id, Business_Name from business_profile where Business_Email = :Business_Email order by Business_Name desc");
        $f_q11->execute([
            "Business_Email" => $_SESSION['ur_email']
        ]);
        $f11   = $f_q11->fetchAll();

        if ($f11) 
        {
            return $f11;
        }
        else
        {
            echo "No Business created yet!";
        }
    } // emd

    /**
     * generate job serial token
     *
     * @return string $j unique job serial indentifier
     * @access private
     */
    private function _jobserial() {
        $j = "job-".sha1(uniqid(mt_rand(), true)).date('his');
        return $j;
    } // end


    /**
     * displays associated job name for selected job token
     * 
     * @return void
     * @access public
     * 
    */
    function loadAssocJobName($jctoken) {

        $g_q = $this->conn->prepare("select jcName from job_category where jcToken = :jcToken");
        $g_q->execute([
            "jcToken" => $jctoken
        ]);
        $g = $g_q->fetchObject();

        echo $g ? $g->jcName : $jctoken;
    } //


    /**
     * create new job
     *
     * @param string $job_categorytokenname job category unique string token
     * @param string $job_categoryname job category name associated with string token
     * @param string $job_have_a_business does the job have a business
     * @param int $job_businessid business tbl pkey
     * @param string $job_title job title
     * @param string $job_district district of job
     * @param string $job_location location of job
     * @param string $job_type job type; fulltime, parttime etc
     * @param string $job_salary job salary
     * @param string $job_posted_on date of job post
     * @param string $job_deadline job deadline
     * @param string $job_qualification qualifications of job
     * @param string $job_responsibility job responsibility
     * @param string $job_how_to_apply how to apply for job
     * @param string $job_duration duration of job running
     *
     * @return void
     * @access public
     */
    function createNewJob($job_categorytokenname, $job_categoryname, $job_have_a_business, $job_businessid, $job_title, $job_district, $job_location, $job_type, $job_salary, $job_posted_on, $job_deadline, $job_qualification, $job_responsibility, $job_how_to_apply, $job_duration)
    {

        if (empty($job_have_a_business)) 
        {
            $this->errs[] = EMPTY_WHETHER_YOU_HAVE_A_BUSINESS;
        }
        elseif (empty($job_title)) 
        {
            $this->errs[] = EMPTY_JOB_TITLE;
        }
        elseif (empty($job_categorytokenname)) 
        {
            $this->errs[] = EMPTY_JOB_CATEGORY;
        }
        elseif (empty($job_district)) 
        {
            $this->errs[] = EMPTY_JOB_DISTRICT;
        }
        elseif (empty($job_location)) 
        {
            $this->errs[] = EMPTY_JOB_LOCATION;
        }
        elseif (empty($job_type)) 
        {
            $this->errs[] = EMPTY_JOB_TYPE;
        }
        elseif (empty($job_salary)) 
        {
            $this->errs[] = EMPTY_JOB_SALARY;
        }
        elseif (empty($job_posted_on)) 
        {
            $this->errs[] = EMPTY_POSTED_ON;
        }
        elseif (empty($job_deadline)) 
        {
            $this->errs[] = EMPTY_DEADLINE;
        }
        elseif (empty($job_qualification)) 
        {
            $this->errs[] = EMPTY_QUALIFICATION;
        }
        elseif (strlen($job_qualification) > 800) 
        {
            $this->errs[] = "Error: job qualifications should not exceed 800 characters";
        }
        elseif (empty($job_responsibility)) 
        {
            $this->errs[] = EMPTY_JOB_RESP;
        }
        elseif (empty($job_how_to_apply)) 
        {
            $this->errs[] = EMPTY_HOW_TO_APPLY;
        }
        else if (($job_have_a_business == "yes") && empty($job_businessid)) 
        {
            $this->errs[] = EMPTY_COMPANY_NAME;
        }
        else
        {
            $jobserial = $this->_jobserial();

                $i_q5 = $this->conn->prepare("insert into jobs(
                    have_business,
                    business_profile_id,
                    jcToken,
                    jcName,
                    job_vacancy,
                    job_district,
                    location,
                    job_type,
                    job_qualification,
                    salary,
                    application_mode,
                    date_posted,
                    deadline,
                    duration,
                    loggedin_email,
                    job_serial,
                    Business_Name) 
                values (
                    :have_business,
                    :business_profile_id,
                    :jcToken,
                    :jcName,
                    :job_vacancy,
                    :job_district,
                    :location,
                    :job_type,
                    :job_qualification,
                    :salary,
                    :application_mode,
                    :date_posted,
                    :deadline,
                    :duration,
                    :loggedin_email,
                    :job_serial,
                    :Business_Name)");

                $i5 = $i_q5->execute([
                    'have_business' => $job_have_a_business,
                    'business_profile_id' => $job_have_a_business == "yes" ? $job_businessid : NULL,
                    'jcToken' => $job_categorytokenname,
                    'jcName' => $job_categoryname,
                    'job_vacancy' => trim($job_title),
                    'job_district' => $job_district,
                    'location' => trim($job_location),
                    'job_type' => $job_type,
                    'job_qualification' => $job_qualification,
                    'salary' => $job_salary,
                    'application_mode' => $job_how_to_apply,
                    'date_posted' => $job_posted_on,
                    'deadline' => !empty($job_deadline) ? $job_deadline : 'N/A',
                    'duration' => !empty($job_duration) ? $job_duration : 'N/A',
                    'loggedin_email' => $_SESSION['ur_email'],
                    'job_serial' => $jobserial,
                    'Business_Name' => $job_have_a_business == "yes" ? $this->_getBusinessName($job_businessid) : 'n\a'
                ]);

                if ($i5) 
                {
                $lastInserted_JobId = $this->conn->lastInsertId();

                    $i_q51 = $this->conn->prepare("insert into job_responsibility(
                        business_profile_id,
                        job_id,
                        responsibility,
                        job_serial) 
                    values (
                        :business_profile_id,
                        :job_id,
                        :responsibility,
                        :job_serial)");

                    foreach ($job_responsibility as $key => $value) 
                    {
                        $i51 = $i_q51->execute([
                            'business_profile_id' => $job_have_a_business == "yes" ? $job_businessid : NULL,
                            'job_id' => $lastInserted_JobId,
                            'responsibility' => $value,
                            'job_serial' => $jobserial
                        ]);
                    }

                    $this->mgss[] = NEW_JOB_CREATED;
                    ?>
    <script>
        $(function(){
            $("#dataForm_createNewJob")[0].reset();
            $("#dataInput_job_businessname").val('').trigger("change");
        });
    </script>
                    <?php
                }
                else
                {
                    $this->errs[] = FAILED_CREATE_JOB;
                }

        } // --- << else validation

    } // end


    /**
     * elapsed time frame
     *
     * @param string $datetime date and time string
     * @param string $now current datetime
     * @return string $minutes elasped time frame; 2 days ago
     */
    function time_elapsed_string($datetime, $now = "now")
    {
        date_default_timezone_set("Africa/Nairobi");
    /*
        $secondsPerMinute = 60;
        $secondsPerHour = 3600;
        $secondsPerDay = 86400;
        $secondsPerMonth = 2592000;
        $secondsPerYear = 31104000;
        // finds the past in datetime
        $past = strtotime($past);
        // finds the current datetime
        $now = time();
        
        // creates the "time ago" string. This always starts with an "about..."
        $timeAgo = "";
        
        // finds the time difference
        $timeDifference = $now - $past;
        
        // less than 29secs
        if($timeDifference <= 29) {
        $timeAgo = "less than a minute";
        }
        // more than 29secs and less than 1min29secss
        else if($timeDifference > 29 && $timeDifference <= 89) {
        $timeAgo = "1 minute";
        }
        // between 1min30secs and 44mins29secs
        else if($timeDifference > 89 &&
        $timeDifference <= (($secondsPerMinute * 44) + 29)
        ) {
        $minutes = floor($timeDifference / $secondsPerMinute);
        $timeAgo = $minutes." minutes";
        }
        // between 44mins30secs and 1hour29mins29secs
        else if(
        $timeDifference > (($secondsPerMinute * 44) + 29)
        &&
        $timeDifference < (($secondsPerMinute * 89) + 29)
        ) {
        $timeAgo = "about 1 hour";
        }
        // between 1hour29mins30secs and 23hours59mins29secs
        else if(
        $timeDifference > (
            ($secondsPerMinute * 89) +
            29
        )
        &&
        $timeDifference <= (
            ($secondsPerHour * 23) +
            ($secondsPerMinute * 59) +
            29
        )
        ) {
        $hours = floor($timeDifference / $secondsPerHour);
        $timeAgo = $hours." hours";
        }
        // between 23hours59mins30secs and 47hours59mins29secs
        else if(
        $timeDifference > (
            ($secondsPerHour * 23) +
            ($secondsPerMinute * 59) +
            29
        )
        &&
        $timeDifference <= (
            ($secondsPerHour * 47) +
            ($secondsPerMinute * 59) +
            29
        )
        ) {
        $timeAgo = "1 day";
        }
        // between 47hours59mins30secs and 29days23hours59mins29secs
        else if(
        $timeDifference > (
            ($secondsPerHour * 47) +
            ($secondsPerMinute * 59) +
            29
        )
        &&
        $timeDifference <= (
            ($secondsPerDay * 29) +
            ($secondsPerHour * 23) +
            ($secondsPerMinute * 59) +
            29
        )
        ) {
        $days = floor($timeDifference / $secondsPerDay);
        $timeAgo = $days." days";
        }
        // between 29days23hours59mins30secs and 59days23hours59mins29secs
        else if(
        $timeDifference > (
            ($secondsPerDay * 29) +
            ($secondsPerHour * 23) +
            ($secondsPerMinute * 59) +
            29
        )
        &&
        $timeDifference <= (
            ($secondsPerDay * 59) +
            ($secondsPerHour * 23) +
            ($secondsPerMinute * 59) +
            29
        )
        ) {
        $timeAgo = "about 1 month";
        }
        // between 59days23hours59mins30secs and 1year (minus 1sec)
        else if(
        $timeDifference > (
            ($secondsPerDay * 59) + 
            ($secondsPerHour * 23) +
            ($secondsPerMinute * 59) +
            29
        )
        &&
        $timeDifference < $secondsPerYear
        ) {
        $months = round($timeDifference / $secondsPerMonth);
        // if months is 1, then set it to 2, because we are "past" 1 month
        if($months == 1) {
            $months = 2;
        }
        
        $timeAgo = $months." months";
        }
        // between 1year and 2years (minus 1sec)
        else if(
        $timeDifference >= $secondsPerYear
        &&
        $timeDifference < ($secondsPerYear * 2)
        ) {
        $timeAgo = "about 1 year";
        }
        // 2years or more
        else {
        $years = floor($timeDifference / $secondsPerYear);
        $timeAgo = "over ".$years." years";
        }
        return $timeAgo." ago";
    */
        
            $now = new DateTime;
            $ago = new DateTime($datetime);
            
            $diff = $now->diff($ago);
            
            $diff->w  = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;
            
            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second'         
            );
            
            foreach($string as $k => &$v)
            {
                if($diff->$k)
                {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                }
                else
                {
                        unset($string[$k]);
                }
            }
            if(!$full) $string = array_slice($string, 0, $full);
            //return $string ? implode(', ', $string) . ' ago' : 'just now';

            if ($string) 
            {
                $str = implode(', ', $string);
                list($hours, $minutes, $seconds) = explode(", ",$str);

                return $minutes. ' ' .$seconds.' ago';
            }

            //$str =  $string ? implode(', ', $string) : '';           

            //echo $minutes. ' ' .$seconds;
    } // end


    /**
     * Register new account **Main** method
     *
     * @param string $input_fullNames client full names
     * @param string $input_phoneNumber client phone or mobile number
     * @param string $input_valid_emailAddress client email address
     * @param string $input_strong_password client new password
     * @param string $input_confirm_strong_password client confirming new password
     * 
     * @return void
     * @access public
     */
    function registerNewAccount($input_fullNames,  $input_phoneNumber,  $input_valid_emailAddress,  $input_strong_password, $input_confirm_strong_password)
    {
        if (empty($input_fullNames) || empty($input_phoneNumber) || empty($input_valid_emailAddress) || empty($input_strong_password) || empty($input_confirm_strong_password)) 
        {
            $this->errs[] = EMPTY_INPUT_FIELDS;
        }
        else if (!filter_var($input_valid_emailAddress, FILTER_VALIDATE_EMAIL)) 
        {
            $this->errs[] = INVALID_EMAIL_ADDRESS;
        }
        else if(strlen($input_phoneNumber) < 6 || strlen($input_phoneNumber) > 13)
        {
            $this->errs[] = INVALID_PHONE_NUMBER_LENGTH;
        }
        else if (strlen($input_strong_password) < 8 || strlen($input_confirm_strong_password) < 8) 
        {
            $this->errs[] = MIN_PASSWORD_LENGTH;
        }
        else if ($input_strong_password != $input_confirm_strong_password)
        {
            $this->errs[] = PASSWORDS_ARE_NOT_MATCHING;
        }
        else if (strlen($input_fullNames) < 3)
        {
            $this->errs[] = INVALID_NAMES_LENGTH;
        }

        else
        {

            // --- << check if email address or phone number exists
            $c_q1 = $this->conn->prepare("select ur_email, phone_no from user_profile where ur_email = :ur_email or phone_no = :phone_no");
            $c_q1->execute([
                "ur_email" => $input_valid_emailAddress,
                "phone_no" => $input_phoneNumber
            ]);
            $c1  = $c_q1->fetchObject();

            if ($c1) 
            {
                $this->errs[] = EMAIL_ADDRESS_OR_PHONE_NUMBER_ALREADY_EXISTS;
            }
            else
            {
                // --- >> return insertion method is boolean
                if ($this->_insertNewAccount($input_fullNames,  $input_phoneNumber,  $input_valid_emailAddress,  $input_strong_password)) 
                {
                $this->mgss[] = NEW_ACCOUNT_SUCCESSFULLY_CREATED;
    ?>
    <script>
    $(function(){
        $("#dataForm-account-registration-client-form")[0].reset();
    });
    </script>
        <?php
                }
                else
                {
                    $this->errs[] = FAILED_TO_CREATE_ACCOUNT;
                }
            }
        } 
    }  // end

    /**
     * reset registration button to its default view; 
     * 
     * @return void
     * @access private
     */
    private function _resetAccountButton()
    {
    ?>
    <script>
    $(function(){
        $("#registrationSubmitBtn").html("Register");
        $("#registrationSubmitBtn").prop("disabled", false);
    });
    </script>
        <?php
    } // end

    /**
     * handles saving of client account information as new registration; 
     * this method is called from **_registerNewAccount** method
     * 
     * @param string $input_fullNames the client full names
     * @param string $input_phoneNumber client phone / mobile contact
     * @param string $input_valid_emailAddress client valid email address
     * @param string $input_strong_password atmost 8 character strong password
     * 
     * @return bool TRUE if saving was successful or FALSE if it failed
     * @access private
     */
    private function _insertNewAccount($input_fullNames,  $input_phoneNumber,  $input_valid_emailAddress,  $input_strong_password)
    {
        // --- >> saving in table 1
        $i_q1 = $this->conn->prepare("INSERT INTO user_profile (names, phone_no, ur_email) VALUES (:names, :phone_no, :ur_email)");
        $i1 = $i_q1->execute([
            "names" => $input_fullNames, 
            "phone_no" =>  $input_phoneNumber, 
            "ur_email" => $input_valid_emailAddress
        ]);  
        
        // --- << crypt the user's password with PHP 5.5's password_hash() function, results in a 60 character
        $passwordHashing = password_hash($input_strong_password, PASSWORD_DEFAULT);
        
        // --- >> saving in table 2
        $i_q2 = $this->conn->prepare("INSERT INTO ur_login (ur_email, ur_pas, ur_type) VALUES (:ur_email, :ur_pas, :ur_type)");
        $i2 = $i_q2->execute([
            "ur_email" => $input_valid_emailAddress, 
            "ur_pas" => $passwordHashing, 
            "ur_type" => UR_TYPE // this is a global defined as 'user' in _config.php file
        ]);  

        // --- >> checking insertion
        if (($i1) && ($i2)) 
        {
        return true;
        }
        else
        {
            return false;
        }
    } // end

    /**
     * send registration notice to email using PHPMailer
     * 
     * @param string
     * 
     * @return bool
     * @access private
     */
    private function _successfulRegistrationNotice($input_valid_emailAddress) {

        // Instantiate mailer object
        $clientRegNotice   = new PHPMailer\PHPMailer\PHPMailer(); 

        $clientRegNotice->Host     = EMAIL_SMTP_HOST;
        $clientRegNotice->SMTPAuth = EMAIL_SMTP_AUTH;
        $clientRegNotice->Username = EMAIL_SMTP_USERNAME;
        $clientRegNotice->Password = EMAIL_SMTP_PASSWORD;
        $clientRegNotice->SMTPSecure   = EMAIL_SMTP_ENCRYPTION;
        $clientRegNotice->Port     = EMAIL_SMTP_PORT;

        $clientRegNotice->From         = DO_NOT_REPLY_EMAIL;
        $clientRegNotice->FromName     = f_f;
        $clientRegNotice->AddAddress($input_valid_emailAddress);
        $clientRegNotice->Subject      = "Fast Service Registration";      

        $clientRegNotice->IsHTML(true);
        $clientRegNotice->Body = '
            Thank you for joining the Fast Service family. By registrating under us, you will have access to a variety of products and services provided by different people and you will also have the priviledge to create your own services / products and share.        
        ';

        if($clientRegNotice->Send()) 
        {                                   
            return true;
        } 
        else 
        {
            //echo $clientRegNotice->ErrorInfo;
            return false;
        }

    } // end



    /**
     * client login method
     * 
     * @param string $input_login_email_address client login email address
     * @param string $input_login_password client login password
     * @param string $input_remember_me if remember me checkbox was checked; we're using cookies for this
     * 
     * @return void
     * @access public
     */
    function login($input_login_email_address,  $input_login_password, $input_remember_me)
    {
        $_SERVER['DOCUMENT_ROOT'];
        
        $system_folder ="fastservice"; 

        defined ("BASE_URL") or  define ("BASE_URL", $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME']);
        defined ("SYSTEM_URL") or  define ("SYSTEM_URL", BASE_URL.'/'.$system_folder);

        $input_login_email_address = trim($input_login_email_address);
        $input_login_password = trim($input_login_password);

        // --- >> validation << --- //
        if (empty($input_login_email_address) || empty($input_login_password)) 
        {
            echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".EMPTY_INPUT_FIELDS."</div>";
        }
        elseif (!filter_var($input_login_email_address, FILTER_VALIDATE_EMAIL)) 
        {
            echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".INVALID_EMAIL_ADDRESS."</div>";
        }
        else
        {
            // get client information
            $c_q2 = $this->conn->prepare($this->_ur_login_table(). " where ur_email = :ur_email");
            $c_q2->execute([
                "ur_email" => $input_login_email_address
            ]);  
            $c2 = $c_q2->fetchObject();

            if ($c2) 
            {
                if (password_verify($input_login_password, $c2->ur_pas)) 
                {
                    // --- >> was remember me checked
                    if (isset($input_remember_me)) 
                    {
                        $this->_addrememberMeCookie($c2->ur_email, $c2->ur_login_id);
                    }
                    else
                    {
                        $this->_deleteRememberMeCookie();
                    }
        
                    // get client default profile pic
                    $c_q21 = $this->conn->prepare("select profile_picture from user_profile where ur_email = :ur_email");
                    $c_q21->execute([
                        "ur_email" => $input_login_email_address
                    ]);  
                    $c21 = $c_q21->fetchObject();

                    // --- >> assign sessions

                    $_SESSION["ur_email"]       = $c2->ur_email; 
                    $_SESSION["ur_login_id"]    = $c2->ur_login_id; 
                    $_SESSION["user_profile_pic"]    = $c21->profile_picture ? $c21->profile_picture : 'nophoto.jpg'; 
                    $_SESSION["ur_type"]    = $c2->ur_type; 
                    $_SESSION["user_logged_in"] = 1; 

                    // --- >> extract name from user email address
                    $emailAddressUName = strstr($c2->ur_email, '@', true);
                    $_SESSION['emailUname'] = $emailAddressUName;

                    $this->user_id    = $c2->ur_login_id;
                    $this->user_email = $c2->ur_email;
                    $this->emailUname = $emailAddressUName;

        
                    $this->isLoggedIn = true;

                    echo "<div class='alert alert-success text-center' role='alert'>Info: login successful; if you are automatically redirected, click <a href='client/dashboard.php' class='alert-link'>here!</a></div>";
                    
                    if($c2->ur_type=='user'){
                    ?>
                                
    <script>
        // --- >> redirect
        $(function(){
            $("#login-fieldset-container").slideUp();
            setTimeout('window.location.href = "client/dashboard.php"; ', 2000);
        });
    </script>

                <?php
                }			
                elseif($c2->ur_type=='admin'){
                ?>			
                    
    <script>
        // --- >> redirect
        $(function(){
            $("#login-fieldset-container").slideUp();
            setTimeout('window.location.href = "webapp/main/admin/a_dashboard.php"; ', 2000);
        });
    </script>

                    <?php
                }
            }
                else
                {
                    echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".WRONG_PASSWORD."</div>";
                }           
            }
            else
            {
                echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".WRONG_EMAIL."</div>";
            }
        } // --- >> else

            ?>
<script>    
    // fadeout toggle
    setTimeout(() => {

        $('[data-fade-out]').each(function() {
            var $this = $(this)
            $this.fadeOut("slow") 
        });        
      
    }, 5000); 
</script>

            <?php

    } // end


    /**
     * resets password
     * 
     * @param string $registeredEmailAddressUpdatePassword registered client email for which the password is to be reset
     * 
     * @return void
     * @access public
     */
    function resetPassword($registeredEmailAddressUpdatePassword) {

        if (empty($registeredEmailAddressUpdatePassword)) {
            echo "<p class='text-danger text-center'>".EMPTY_EMAIL."</p>";
        }
        else if (!filter_var($registeredEmailAddressUpdatePassword, FILTER_VALIDATE_EMAIL)) {
            echo "<p class='text-danger text-center'>".INVALID_EMAIL_ADDRESS."</p>";
        } else {
            // check if email is correct
            $f_q = $this->conn->prepare("select ur_login_id, ur_email from ur_login where ur_email = :ur_email");
            $f_q->execute([
                "ur_email" => $registeredEmailAddressUpdatePassword
            ]);
            $f = $f_q->fetchObject();

            if ($f) {

                // generate reset token
                $generate_reset_token = sha1(uniqid(mt_rand(), true)).date('Ymdhis').sha1(uniqid(mt_rand(), true));

                // reset expiry date and time
                $expiryDateFormat = mktime(date("H"), date("i"), date("s"), date("m"), date("d")+1, date("Y"));
                $expiryDate = date("Y-m-d H:i:s", $expiryDateFormat);

                // update `ur_login` tbl
                $u_q = $this->conn->prepare("update ur_login set reset_password_token = :reset_password_token, reset_password_timestamp = :reset_password_timestamp where ur_email = :ur_email");
                $u   = $u_q->execute([
                    "reset_password_token" => $generate_reset_token,
                    "reset_password_timestamp" => $expiryDate,
                    "ur_email" => $registeredEmailAddressUpdatePassword
                ]);

                if ($u) {

                    // general url
                    $passwordResetlink = FF_URL.'?confirmedEmailAddress='.urlencode($registeredEmailAddressUpdatePassword).'&stringpath='.urlencode($generate_reset_token).'&autogenerated_int='.urlencode($f->ur_login_id);

                    // http://fastserviceshared/welcome.php?confirmedEmailAddress=ruperic25@gmail.com&stringpath=650be4d41a2037aea78e542d5afb9f4d8c5136a3201909131123398b21291e406656001e0579807468364e36fec82f&autogenerated_int=1

                    if ($this->_sendPasswordResetEmail($passwordResetlink, $registeredEmailAddressUpdatePassword)) {
                        echo "<p class='text-success text-center'>".PASSWORD_RESET_LINK_SENT."</p>";
                    } else {

                    } // failed to send password reset link

                } else {
                    echo "<p class='text-danger text-center'>".UNKNOW_ERROR_OCCURRED."</p>";

                } // unknown error
                
            } else {
                echo "<p class='text-danger text-center'>".UNKNOWN_EMAIL_ADDRESS_ENTERED."</p>";

            } // wrong email address

        } // else validation

    } // end

    /**
     * sends password reset link to client email
     * 
     * making use of the PHPMailer class to send email; hence method will not work ie. return FALSE if no internet connection
     * 
     * @param string $passwordResetlink the password reset URL
     * @param string $registeredEmailAddressUpdatePassword email to receive the reset link; it should be registered within the system
     * 
     * @return bool TRUE - email was successfully sent, FALSE - failed to send email
     * @access private
     */
    private function _sendPasswordResetEmail($passwordResetlink, $input_valid_emailAddress) {

       // Instantiate mailer object
        $pwdResetObj   = new PHPMailer\PHPMailer\PHPMailer(); 

        $pwdResetObj->Host     = EMAIL_SMTP_HOST;
        $pwdResetObj->SMTPAuth = EMAIL_SMTP_AUTH;
        $pwdResetObj->Username = EMAIL_SMTP_USERNAME;
        $pwdResetObj->Password = EMAIL_SMTP_PASSWORD;
        $pwdResetObj->SMTPSecure   = EMAIL_SMTP_ENCRYPTION;
        $pwdResetObj->Port     = EMAIL_SMTP_PORT;

        $pwdResetObj->From         = DO_NOT_REPLY_EMAIL;
        $pwdResetObj->FromName     = f_f;
        $pwdResetObj->AddAddress($input_valid_emailAddress);
        $pwdResetObj->Subject      = "Fast Service Password Reset";      

        $pwdResetObj->IsHTML(true);
        $pwdResetObj->Body = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Password Reset</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <!-- body -->
    <body style="margin: 0; padding: 0;">
        <!-- main table -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                        <tr>
                            <td align="center" bgcolor="#dc3545" style="padding: 5px 0;">
                                <img src="././_assets/images/email/emailBanner.png" alt="Fast Service" width="300" height="64" style="display: block;" />
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#454d54" style="padding: 40px 30px 40px 30px; color: #eeeeee">
                                
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="font-size: 20px; padding: 5px 0; text-decoration: underline;">
                                            <b>Forgot Your Password?</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 20px 0 5px 0; line-height: 18px">
                                            We noticed that you requested to reset your password. Please click on the link below or copy and paste it in your browser address bar.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 0 15px 0; line-height: 18px">This link is only valid till midnight from time of receiving it and can only be used once.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            '.$passwordResetlink.'
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#000000" style="padding: 30px 10px 30px 10px; color: #eeeeee">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="padding: 1px 0;">                                                        
                                                        BANDA COMMUNITY PREMISES Kyambogo road,
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        Next To Banda Police Post, Kampala Uganda.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        P.O Box 183, Kyambogo
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        Mobile: +256 (0) 200 923 850 / +256 (0) 703 303 010
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        Email: bandacomputercenter@gmail.com
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        Website: www.bandacomputercenter.com/
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>

                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        <a href="https://www.twitter.com/fastservice">
                                                            <img src="././_assets/images/email/facebook.png" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        <a href="https://www.twitter.com/fastservice">
                                                            <img src="././_assets/images/email/twitter.png" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding: 5px 0; color: #ffffff"  bgcolor="#dc3545">
                                copyright &copy; '.date("Y").' Fast Service
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- main table -->
    </body>
    <!-- body -->
</html>';

        if($pwdResetObj->Send()) 
        {                                   
            return true;
        } 
        else 
        {
            echo "<p class='text-danger text-center'>".$pwdResetObj->ErrorInfo." No internet connection!</p>";
            return false;
        }

    } // end

    /**
     * password reset link validation
     * 
     * method redirection when user visits his/her email, then clicks on reset password link. we handle the validation here
     * to check that it matches with that in the database and if the link has expired
     * 
     * @param string $emailAddress client's email addess
     * @param string $verificationToken auto-generated token
     * @param int $loginId ur_login primary key check with email
     * 
     * @return bool TRUE, password reset if ok. FALSE, password reset has failed or link expired
     * @access public
     */
    function validatePasswordreset($emailAddress, $verificationToken, $loginId) {

        $check_q = $this->conn->prepare("select * from ur_login where ur_login_id = :ur_login_id and ur_email = :ur_email and reset_password_token = :reset_password_token");
        $check_q->execute([
            'ur_login_id' => $loginId,
            'ur_email' => $emailAddress,
            'reset_password_token' => $verificationToken
        ]);
        $check = $check_q->fetchObject();

        $currentDateTime = date('Y-m-d H:i:s');

        // reset link

        if ($check) {
            
            if($check->reset_password_timestamp >= $currentDateTime){
                 return true;
            }
            else{
                 echo "<div class='alert alert-info text-center' role='alert'>".PASSWORD_RESET_LINK_HAS_EXPIRED."</div>";                
                return false;
            }
           
        }  // else get parameters are invalid
        else {
            echo "<div class='alert alert-info text-center' role='alert'>".PASSWORD_RESET_LINK_DEACTIVATED."</div>";  
            return false;          
        }

    } // end

    /**
     * handles final password reset
     * 
     * @param string $newPassword new more than 8 character password
     * @param string $confirmedNewPassword confirmation of new password
     * @param string $emailAddress client email address
     * @param string $resetToken reset password token
     * @param string $urLoginPkey `ur_login` tbl primary key
     * 
     * @return void
     * @access public
     */
    function finalPasswordResetSubmission($newPassword, $confirmedNewPassword, $emailAddress, $resetToken, $urLoginPkey) {

        // validation
        if ($newPassword != $confirmedNewPassword) {
            echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".PASSWORDS_ARE_NOT_MATCHING."</div>"; 
        }
        else if (strlen($newPassword) < 8) {
            echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".MIN_PASSWORD_LENGTH."</div>"; 
        }
        else if (empty($newPassword)) {
            echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".EMPTY_NEW_PASSWORD."</div>";            
        }
        else if (empty($confirmedNewPassword)) {
            echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".EMPTY_CONFIRM_PASSWORD."</div>";            
        }
        else {

            // password hashing
            $hashNewPassword = password_hash($newPassword, PASSWORD_DEFAULT);
            // validate email
            $emailAddress = filter_var($emailAddress, FILTER_SANITIZE_EMAIL);
            $emailAddress = filter_var($emailAddress, FILTER_VALIDATE_EMAIL);

            // check GET info
            $check_par_q = $this->conn->prepare("select * from ur_login where ur_login_id = :ur_login_id and ur_email = :ur_email and reset_password_token = :reset_password_token");
            $check_par_q->execute([
                "ur_login_id" => $urLoginPkey,
                "ur_email" => $emailAddress,
                "reset_password_token" => $resetToken
            ]);
            $check_par = $check_par_q->fetchObject();

            if ($check_par) {

                // update
                $update_login_info_q = $this->conn->prepare("update ur_login set reset_status = :reset_status, reset_password_token = :reset_password_token, reset_password_timestamp = :reset_password_timestamp, ur_pas = :ur_pas where ur_login_id = :ur_login_id and ur_email = :ur_email");

                $update_login_info = $update_login_info_q->execute([
                    "reset_status" => 1,
                    "reset_password_token" => NULL,
                    "reset_password_timestamp" => NULL,
                    "ur_pas" => $hashNewPassword,
                    "ur_login_id" => $urLoginPkey,
                    "ur_email" => $emailAddress
                ]);

                if ($update_login_info) {
                    ?>
                    <script>
                        var pageToredirectTo = "http://fastserviceshared/welcome.php?";
                        var redirectcountDown = 5;
                        function redirectTimingFunction() {
                            if(redirectcountDown > 0){
                                redirectcountDown--;
                                setTimeout("redirectTimingFunction()", 1000);
                            }
                            else {                                      
                                window.location.replace("http://fastserviceshared/welcome.php?");
                            }
                        }          
                    </script>

                    <?php
                    echo "<div class='alert alert-success text-center' role='alert'>".PASSSWORD_SUCCESSFUL_RESET." ".REDIRECTED_SHORTLY."</div><script>redirectTimingFunction();</script>"; 
                    
                } // pwd reset successful 
                else {


                } // failed to reset password
                
            } // information exists
            else {

                echo "<div class='alert alert-danger text-center' data-fade-out role='alert'>".FAILED_TO_CONNECT_ACCOUNT."</div>"; 
            } // failed to load information

        } // end of validation
            ?>
<script>    
    // fadeout toggle
    setTimeout(() => {

        $('[data-fade-out]').each(function() {
            var $this = $(this)
            $this.fadeOut("slow") 
        });        
      
    }, 5000); 
</script>

            <?php

    } // end



    /**
     * handles client login using PHP sessions. this method is called under the "__construct()"
     * 
     * @return void
     * @access private
     */
    private function loginWithSessionData()
    {    
        $this->user_email = $_SESSION['ur_email'];
        $this->emailUname = $_SESSION['emailUname'];
        
        $this->isLoggedIn = true;
    } // end
        
    /**
     * handles client login with cookies; incase the remember me checkbox was checked, once the site is visited, he/she
     * is automatically logged in. this method is called under the "__construct()"
     * 
     * @return bool
     * @access private
     */
    private function loginWithCookieData()
    {
    if (isset($_COOKIE['rememberme'])) {
        
        list ($user_id, $token, $hash) = explode(':', $_COOKIE['rememberme']);
        /*
    1:5d7d517ffe62f72e72791796493db90178f2187ae854ed0b1d09e01fffaa5030:5fc6c5063fe4fe3b6c2c6ca0fced811f2d77783263ab5196fb4535a52240cb5f
        */
        
        if ($hash == hash('sha256', $user_id . ':' . $token . COOKIE_SECRET_KEY) && !empty($token)) 
        {        
                
            $sth = $this->conn->prepare($this-> _ur_login_table(). " WHERE ur_login_id = :ur_login_id and remember_me_token = :remember_me_token");
            $sth->execute([
                'ur_login_id' => $user_id,
                'remember_me_token' => $token
            ]);
                
            $result_row = $sth->fetchObject();
            
            if ($result_row) 
            {    
        
                // get client default profile pic
                $c_q21 = $this->conn->prepare("select profile_picture from user_profile where ur_email = :ur_email");
                $c_q21->execute([
                    "ur_email" => $result_row->ur_email
                ]);  
                $c21 = $c_q21->fetchObject();


                $_SESSION["ur_email"]       = $result_row->ur_email; 
                $_SESSION["ur_login_id"]    = $result_row->ur_login_id;
                $_SESSION["user_profile_pic"]    = $c21->profile_picture ? $c21->profile_picture : 'nophoto.jpg'; 
                $_SESSION["user_logged_in"] = 1;          

                // --- >> extract name from user email address
                $emailAddressUName = strstr($result_row->ur_email, '@', true);
                $_SESSION['emailUname'] = $emailAddressUName;

                $this->user_id    = $c2->ur_login_id;
                $this->user_email = $c2->ur_email;
                $this->emailUname = $emailAddressUName;

                $this->isLoggedIn = true;
                
                $this->_addrememberMeCookie($result_row->ur_email, $user_id);
                return true;
            }
        }
        $this->_deleteRememberMeCookie();
        $this->errs[] = MESSAGE_COOKIE_INVALID;
    }
    return false;
    } // end

    /**
     * handles remember me checked input during login.
     * 
     * @param string $userEmailAddress client email address
     * @param int $userPkeyId client id
     * 
     * @return void
     * @access private
     */
    private function _addrememberMeCookie($userEmailAddress, $userPkeyId)
    {
        $random_token_string = hash('sha256', mt_rand());

        $i_q3 = $this->conn->prepare("UPDATE ur_login SET remember_me_token = :remember_me_token WHERE ur_email = :ur_email");
        $i3 = $i_q3->execute([
            'remember_me_token' => $random_token_string,
            'ur_email' => $userEmailAddress
        ]);
    
        if ($i3) 
        {
        
            $cookie_string_first_part = $userPkeyId . ':' . $random_token_string;
            $cookie_string_hash       = hash('sha256', $cookie_string_first_part . COOKIE_SECRET_KEY);
            $cookie_string            = $cookie_string_first_part . ':' . $cookie_string_hash;

            setcookie('rememberme', $cookie_string, time() + COOKIE_RUNTIME, "/", COOKIE_DOMAIN);
        }
    } // end

    /**
     * this method deletes remember me cookie token when client logs out by clicking the logout button
     * 
     */
    private function _deleteRememberMeCookie()
    {
        $d_q1 = $this->conn->prepare("UPDATE ur_login SET remember_me_token = NULL WHERE ur_login_id = :ur_login_id");
        $d1   = $d_q1->execute([
            'ur_login_id' => $_SESSION['ur_login_id']
        ]);
        if ($d1) 
        {
        setcookie('rememberme', false, time() - (3600 * 3650), COOKIE_DOMAIN);
        }   
    } // end

    /**
     * this checks whether the client is logged in such that he/she is directed to dashboard; if not, login page
     * 
     * @return bool TRUE, redirect to dasboard; FALSE, redirect to login page
     */
    function _isLoggedIn()
    {
        return $this->isLoggedIn;
    } // end

    /**
     * handles client logout from dashboard; notice we call the "_deleteRememberMeCookie()" method if the cookie was set
     * through the remember me checkbox. the "__construct()" handles this method
     */
    function _logout()
    {
        $this->_deleteRememberMeCookie();

        //session_destroy();
        unset($_SESSION["ur_email"]); 
        unset($_SESSION["ur_login_id"]); 
        unset($_SESSION['emailUname']);
        unset($_SESSION["user_logged_in"]);
        unset($_SESSION["user_profile_pic"]);

        $this->isLoggedIn = false;
        //header("Location: ../index.php");
    /*
        ?>
    <script>
        // --- >> redirect
        $(function(){
            setTimeout('window.location.href = "index.php"; ', 1);
        });
    </script>

        <?php
        */

    } // end


    /**
     * Sends an email from the Home section (not logged in)
     * 
     * @param string $fast_service_sender_names sender names
     * @param string $fast_service_email sender valid email address
     * @param string $fast_service_subject message subject
     * @param string $fast_service_sent_text message
     * @param string $fast_service_msg_date messahe submission date
     * 
     * @return void
     */
    function sendEmail($fast_service_sender_names, $fast_service_email, $fast_service_subject, $fast_service_sent_text,$fast_service_msg_date)
    {
        if(empty($fast_service_sender_names))
        {
            $this->errs[] = EMPTY_NAMES;
        }
        elseif(empty($fast_service_email))
        {
            $this->errs[] = EMPTY_EMAIL;            
        }
        elseif(empty($fast_service_subject))
        {
            $this->errs[] = EMPTY_SUBJECT;            
        }
        elseif(empty($fast_service_sent_text))
        {
            $this->errs[] = EMPTY_MESSAGE;            
        }
        elseif(strlen($fast_service_sent_text) > 150)
        {
            $this->errs[] = MESSAGE_NOT_GREATER_THAN_150;
        }
        elseif(!filter_var($fast_service_email, FILTER_VALIDATE_EMAIL))
        {
            $this->errs[] =INVALID_EMAIL_ADDRESS;
        }
        elseif(strlen($fast_service_subject) > 25)
        {
            $this->errs[] = SUBJECT_NOT_GREATER_THAN_25;
        }
        elseif(strlen($fast_service_sender_names) > 15)
        {
            $this->errs[] = NAMES_NOT_GREATER_THAN_15;
        }
        else
        {
        
        /*
            $sqx = $this->conn->prepare("INSERT INTO messages (sender_names, email, subject, sent_text, msg_date, sent_time, status) VALUES (:sender_names, :email, :subject, :sent_text, :msg_date, :sent_time, :status)");
            $qum = $sqx->execute([
                "sender_names" => $fast_service_sender_names, 
                "email" =>  $fast_service_email, 
                "subject" => $fast_service_subject, 
                "sent_text" => $fast_service_sent_text, 
                "msg_date" => $fast_service_msg_date,
                "sent_time" => date('h:i:s'),
                "status" => 'sent'
            ]);  
        */

            if($this->_phpmailerSendEmail_homePage($fast_service_sender_names, $fast_service_email, $fast_service_subject, $fast_service_sent_text))
            {
                $this->mgss[] = EMAIL_SUCCESSFULLY_SENT;
                ?>
                <script>
                    $(function() {
                        $("#dataFastService-SendEmail")[0].reset()
                    });
                </script>

                <?php
                
               // $this->_clearFormFields();
            }
            else
            {
                $this->errs[] = FAILED_TO_SEND_EMAIL;
            }            
            
        } // else
        
    } // end

    /**
     * send email using phpmailer class; this method is then called from "_sendEmail()" method
     * 
     * @param string $fast_service_sender_names sender names
     * @param string $fast_service_email sender valid email address
     * @param string $fast_service_subject message subject
     * @param string $fast_service_sent_text message
     * @param string $fast_service_msg_date messahe submission date
     * 
     * @return bool
     */
    private function _phpmailerSendEmail_homePage($fast_service_sender_names, $fast_service_email, $fast_service_subject, $fast_service_sent_text)
    {
        $mail = new PHPMailer\PHPMailer\PHPMailer(); // --- >> define new object

        // ---- << these parameters are defined in _config.php file in _modules/config directory
        $mail->SMTPDebug  = EMAIL_SMTP_DEBUG;                                                                      
        $mail->Host       = EMAIL_SMTP_HOST;  
        $mail->SMTPAuth   = EMAIL_SMTP_AUTH;                               
        $mail->Username   = EMAIL_SMTP_USERNAME;                
        $mail->Password   = EMAIL_SMTP_PASSWORD;                          
        $mail->SMTPSecure = EMAIL_SMTP_ENCRYPTION;                           
        $mail->Port       = EMAIL_SMTP_PORT;
        // ---- >> ---- ---- ---- ---- ---- ---- << ---- //

        $mail->From       = $fast_service_email; // --- >> from email
        $mail->FromName   = $fast_service_sender_names; // --- >> from names
        $mail->AddAddress(FAST_SERVICE_ADMIN_EMAIL); // --- >> fast service admin email defined in _config.php file
        $mail->Subject    = $fast_service_subject; // --- >> mail subject    
        
        $mail->IsHTML(true); // --- >> set true if html elements are to be define
        $mail->Body = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Contact us</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <!-- body -->
    <body style="margin: 0; padding: 0;">
        <!-- main table -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                        <tr>
                            <td align="center" bgcolor="#dc3545" style="padding: 5px 0;">
                                <img src="http://fastserviceug.com/_assets/images/email/emailBanner.png" alt="Fast Service" width="300" height="64" style="display: block;" />
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#343a40" style="padding: 40px 30px 40px 30px; color: #eeeeee">
                                
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="padding: 20px 0 5px 0; line-height: 18px">
                                            '.$fast_service_sent_text.'
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#000000" style="padding: 30px 10px 30px 10px; color: #eeeeee">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="padding: 1px 0;">                                                        
                                                        BANDA COMMUNITY PREMISES Kyambogo road,
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        Next To Banda Police Post, Kampala Uganda.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        P.O Box 183, Kyambogo
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        Mobile: +256 (0) 200 923 850 / +256 (0) 703 303 010
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        Email: bandacomputercenter@gmail.com
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        Website: www.bandacomputercenter.com/
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>

                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        <a href="https://www.twitter.com/fastservice">
                                                            <img src="http://fastserviceug.com/_assets/images/email/facebook.png" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 1px 0;">
                                                        <a href="https://www.twitter.com/fastservice">
                                                            <img src="http://fastserviceug.com/_assets/images/email/twitter.png" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding: 5px 0; color: #ffffff"  bgcolor="#dc3545">
                                copyright &copy; '.date("Y").' Fast Service
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- main table -->
    </body>
    <!-- body -->
</html>';

        if($mail->Send()) 
        {
            return true;
        }
        else 
        {
            return false;
            $this->errs[] = $mail->ErrorInfo;
        }

    } // end

    /**
     * send email from client side (logged in); the email information is saved in the database
     * 
     * @param string $receivingEmailAddress the email which is to receive the message
     * @param string $receivingSubject the subject
     * @param string $receivingMessage the message being sent
     * @param string $sendingEmail the email which is sending the message
     * 
     * @return void
     */
    function _clientSendEmail($receivingEmailAddress, $receivingSubject, $receivingMessage, $sendingEmail)
    {
    if(empty($receivingEmailAddress))
        {
            $this->errs[] = EMPTY_EMAIL;            
        }
        elseif(empty($receivingSubject))
        {
            $this->errs[] = EMPTY_SUBJECT;            
        }
        elseif(empty($receivingSubject))
        {
            $this->errs[] = EMPTY_MESSAGE;            
        }
        /*
        elseif(strlen($receivingMessage) > 150)
        {
            $this->errs[] = MESSAGE_NOT_GREATER_THAN_150;
        }
        */
        elseif(!filter_var($receivingEmailAddress, FILTER_VALIDATE_EMAIL))
        {
            $this->errs[] =INVALID_EMAIL_ADDRESS;
        }
        elseif(strlen($receivingSubject) > 100)
        {
            $this->errs[] = SUBJECT_NOT_GREATER_THAN_100;
        }
        else
        {
            // --- >> get sender names
            $g_q1 = $this->conn->prepare("select names from user_profile where ur_email = :ur_email");
            $g_q1->execute([
                "ur_email" => $sendingEmail
            ]);
            $g1 = $g_q1->fetchObject();

            // --- >> message composed saved to sender outbox
            $s_q1 = $this->conn->prepare("INSERT INTO msg_outbox (outbox_email, outbox_subject, outbox_message, sender_names, sender_email) VALUES (:outbox_email, :outbox_subject, :outbox_message, :sender_names, :sender_email)");
            $s1 = $s_q1->execute([
                "outbox_email" => $receivingEmailAddress,
                "outbox_subject" =>  $receivingSubject, 
                "outbox_message" => $receivingMessage,
                "sender_names" => $g1->names, 
                "sender_email" => $sendingEmail 
            ]); 

            // --- >> message sent saved to receiver inbox
            $s_q2 = $this->conn->prepare("INSERT INTO msg_inbox (inbox_email, inbox_subject, inbox_message, sender_names, sender_email) VALUES (:inbox_email, :inbox_subject, :inbox_message, :sender_names, :sender_email)");
            $s2 = $s_q2->execute([
                "inbox_email" => $receivingEmailAddress,
                "inbox_subject" =>  $receivingSubject, 
                "inbox_message" => $receivingMessage,
                "sender_names" => $g1->names, 
                "sender_email" => $sendingEmail 
            ]); 
            
            if($s1 && $s2)
            {
                echo 1; 
            }
            else
            {
                echo 2;
            }            
            
        } // else
        
    } // end


    /* --------------------------------------------------- */
    /* business
    /* --------------------------------------------------- */

    /**
     * handles the creatiion of a new business
     * 
     * @param string $business_name business name
     * @param string $business_category which category the business falls in; these categories are created by the admin
     * @param string $business_pcontact the business contact information
     * @param string $business_emailaddress business email address
     * @param string $business_servicesoffered services which the business offers
     * @param string $geog_Area NULL
     * @param string $geog_latlng NULL
     * @param string $geog_longitude NULL
     * @param string $geog_latitude NULL
     * @param string $loggedInemailAddress the currently logged in email session
     * 
     * @return void
     */
    function _createNewBusiness($business_name, $business_category, $business_pcontact, $business_emailaddress, $business_address, $business_servicesoffered, $geog_Area, $geog_latlng, $geog_longitude, $geog_latitude, $loggedInemailAddress)
    {
        if (empty($business_name)) 
        {
        $this->errs[] = EMPTY_BUSINESS_NAME;
        }
        elseif (empty($business_category)) 
        {
        $this->errs[] = EMPTY_BUSINESS_CATEGORY;
        }
        elseif (empty($business_pcontact)) 
        {
        $this->errs[] = EMPTY_BUSINESS_PCONTACT;
        }
        elseif (empty($business_emailaddress)) 
        {
        $this->errs[] = EMPTY_BUSINESS_EMAIL;
        }
        elseif (empty($business_address)) 
        {
        $this->errs[] = EMPTY_BUSINESS_COUNTRY;
        }
        elseif (empty($business_servicesoffered)) 
        {
        $this->errs[] = EMPTY_BUSINESS_SERVICES;
        }
        elseif (empty($geog_Area)) 
        {
        $this->errs[] = EMPTY_BUSINESS_GEOG_LOCATION;
        }
        elseif (!filter_var($business_emailaddress, FILTER_VALIDATE_EMAIL)) 
        {
        $this->errs[] = INVALID_EMAIL_ADDRESS;
        }
        elseif (strlen($business_pcontact) > 13 || strlen($business_pcontact) < 6) 
        {
            $this->errs[] = INVALID_PHONE_NUMBER_LENGTH;
        }
        else
        {

            $i_q4 = $this->conn->prepare("insert into business_profile(
                        Business_Name,
                        Business_Address,
                        Business_Phone,
                        Business_Email,
                        Business_Category,
                        Services_Offered,
                        Geographical_Area,
                        vander_latitude,
                        vander_longitude,
                        loggedin_email) 
                values (
                        :Business_Name,
                        :Business_Address,
                        :Business_Phone,
                        :Business_Email,
                        :Business_Category,
                        :Services_Offered,
                        :Geographical_Area,
                        :vander_latitude,
                        :vander_longitude,
                        :loggedin_email)");
            
            $i4 = $i_q4->execute([ 
                'Business_Name' => $business_name, 
                'vander_longitude' => $geog_longitude, 
                'vander_latitude' => $geog_latitude, 
                'Business_Address' => $business_address, 
                'Business_Phone' => $business_pcontact, 
                'Business_Email' => $business_emailaddress, 
                'Business_Category' => $business_category, 
                'Services_Offered' => $business_servicesoffered, 
                'Geographical_Area' => $geog_Area,
                'loggedin_email'=> $loggedInemailAddress
            ]);

            if ($i4) 
            {
            $this->mgss[] = NEW_BUSINESS_CREATED;
            ?>
    <script>
        $(function(){
            $("#dataForm_createNewBusiness")[0].reset();
            $(".select2services").val('').trigger("change");
        });
    </script>
            <?php
            }
            else
            {
                $this->errs[] = FAILED_CREATE_BUSINESS;
            }
        } // --- >> else validation

    } // end



/* --------------------------------------------------- */
/* advert
/* --------------------------------------------------- */

    /**
     * handles the creation of a new advert
     * 
     * @param string $advert_businessName incase the advert has the business, then we get the business id
     * @param string $advert_have_a_business if the advert is associated with an business
     * @param string $advert_title the advert title
     * @param string $advert_publish whether the advert is to be pubished as soon as the admin approves it
     * @param string $advert_period the date range the advet will run
     * @param string $advert_loggedInEmail the currently logged in email session
     * @param string $advert_details details concerning the advert
     * @param string $advert_bannerName this is the primary banner; banner name
     * @param string $advert_bannerTmp primary banner temp name
     * @param string $advert_bannerSize primary banner size; max size is 1Mb
     * @param string $advert_bannerTwoName this is the second banner
     * @param string $advert_bannerTwoTmp second banner temp name
     * @param string $advert_bannerTwoSize second banner size
     * @param string $advert_bannerThreeName this is the third banner
     * @param string $advert_bannerThreeTmp third banner temp name
     * @param string $advert_bannerThreeSize third banner size
     * @param string $advert_bannersYesNo if the advert has banners; max of 3 are allowed
     * 
     * @return void
     * @access public
     */
    function _addNewAdvert(
            $advert_businessName,
            $advert_have_a_business,
            $advert_title, 
            //$advert_allowComments, 
            $advert_publish, 
            $advert_period, 
            $advert_loggedInEmail,
            $advert_details,
            $advert_bannerName, 
            $advert_bannerTmp,
            $advert_bannerSize,
            $advert_bannerTwoName,
            $advert_bannerTwoTmp,
            $advert_bannerTwoSize, 
            $advert_bannerThreeName, 
            $advert_bannerThreeTmp, 
            $advert_bannerThreeSize,
            $advert_bannersYesNo)
    {

        $advert_details = htmlspecialchars(trim($advert_details, ' '));

        if (empty($advert_have_a_business)) {
            $this->errs[] = EMPTY_WHETHER_YOU_HAVE_A_BUSINESS;
        }
        else if (($advert_have_a_business == "yes") && empty($advert_businessName)) {
            $this->errs[] = SELECT_BUSINESS_NAME;
        }
        else if (empty($advert_title)) 
        {
        $this->errs[] = EMPTY_ADVERT_TITLE;
        }
        else if (empty($advert_publish)) 
        {
        $this->errs[] = SELECT_WHETHER_TO_PUBLISH;
        }
        else if (empty($advert_period)) 
        {
        $this->errs[] = SELECT_PERIOD;
        }
        else if (empty($advert_details)) 
        {
        $this->errs[] = EMPTY_ADVERT_DETAILS;
        }
        elseif (strlen($advert_details) > 800) 
        {
        $this->errs[] = ADVERT_DETAILS_800_CHAR;
        }
        elseif (empty($advert_bannersYesNo)) 
        {
        $this->errs[] = DISPLAY_BANNER_YES_NO;
        }
        else
        {

            $advertSerialToken = $this->_advertSerial(); // advert serial

            if ($advert_bannersYesNo == 'yes') 
            {

                if(!$advert_bannerName) 
                {
                    $this->errs[] = PRIMARY_BANNER_NOT_SELECTED;
                }
                else
                {

                    // --- >> validate banners one by one
                    if ($advert_bannerName &&
                        !$advert_bannerTwoName &&
                        !$advert_bannerThreeName) 
                    {
                        if ($this->_bannerValidation(
                            $advert_bannerName, 
                            $advert_bannerTmp,
                            $advert_bannerSize))
                        {

    $banner1_ext  = strtolower(pathinfo($advert_bannerName,PATHINFO_EXTENSION));
    $banner1_extFile = 'PB-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner1_ext;
                            
                            if($this->_saveAdvertQuery(
                                    $advert_businessName,
                                    $advert_have_a_business,
                                    $advert_title,
                                    $advert_publish, 
                                    $advert_period, 
                                    $advert_loggedInEmail,
                                    $advert_details,
                                    $advert_bannerName = $banner1_extFile, 
                                    $advert_bannerTwoName = NULL, 
                                    $advert_bannerThreeName = NULL,
                                    $advertSerialToken))
                            {
                                $this->mgss[] = NEW_ADVERT_CREATED;

    // move uploads
    move_uploaded_file($advert_bannerTmp, $this->bannerDir.$banner1_extFile);
                            
                            // call clear fields method
                            $this->_resetAdvertFields();

                            } // if
                            else {
                                $this->errs[] = FAILED_TO_SAVE_ADVERT;
                            } // else
                        
                        } // validation  
                                
                    } // banner 1

                    else if (
                        $advert_bannerName &&
                        $advert_bannerTwoName &&
                        !$advert_bannerThreeName)
                    {
                        if ($this->_bannerValidation(
                            $advert_bannerTwoName, 
                            $advert_bannerTwoTmp,
                            $advert_bannerTwoSize)        
                            &&
                            $this->_bannerValidation(
                            $advert_bannerName, 
                            $advert_bannerTmp,
                            $advert_bannerSize))
                        {

    $banner1_ext  = strtolower(pathinfo($advert_bannerName,PATHINFO_EXTENSION));
    $banner1_extFile = 'PB-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner1_ext;

    $banner2_ext  = strtolower(pathinfo($advert_bannerTwoName,PATHINFO_EXTENSION));
    $banner2_extFile = 'B2-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner2_ext;

                            if($this->_saveAdvertQuery(
                                    $advert_businessName,
                                    $advert_have_a_business,
                                    $advert_title,
                                    $advert_publish, 
                                    $advert_period, 
                                    $advert_loggedInEmail,
                                    $advert_details,
                                    $advert_bannerName = $banner1_extFile,
                                    $advert_bannerTwoName = $banner2_extFile,
                                    $advert_bannerThreeName = NULL,
                                    $advertSerialToken))
                            {
                                $this->mgss[] = NEW_ADVERT_CREATED;

    // move uploads
    move_uploaded_file($advert_bannerTmp, $this->bannerDir.$banner1_extFile);
    move_uploaded_file($advert_bannerTwoTmp, $this->bannerDir.$banner2_extFile);
                            
                            // call clear fields method
                            $this->_resetAdvertFields();

                            } // if
                            else {
                                $this->errs[] = FAILED_TO_SAVE_ADVERT;
                            } // else
                        
                        } // validation  

                    } // banner 1 and 2

                    else if ($advert_bannerName && !$advert_bannerTwoName && $advert_bannerThreeName) 
                    {
                        if ($this->_bannerValidation(
                            $advert_bannerThreeName, 
                            $advert_bannerThreeTmp,
                            $advert_bannerThreeSize)
                            &&
                            $this->_bannerValidation(
                            $advert_bannerName, 
                            $advert_bannerTmp,
                            $advert_bannerSize))
                        {

    $banner1_ext  = strtolower(pathinfo($advert_bannerName,PATHINFO_EXTENSION));
    $banner1_extFile = 'PB-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner1_ext;

    $banner3_ext  = strtolower(pathinfo($advert_bannerThreeName,PATHINFO_EXTENSION));
    $banner3_extFile = 'B3-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner3_ext;

                            if($this->_saveAdvertQuery(
                                    $advert_businessName,
                                    $advert_have_a_business,
                                    $advert_title,
                                    $advert_publish, 
                                    $advert_period, 
                                    $advert_loggedInEmail,
                                    $advert_details,
                                    $advert_bannerName = $banner1_extFile,
                                    $advert_bannerTwoName = NULL,
                                    $advert_bannerThreeName = $banner3_extFile,
                                    $advertSerialToken))
                            {
                                $this->mgss[] = NEW_ADVERT_CREATED;

    // move uploads
    move_uploaded_file($advert_bannerTmp, $this->bannerDir.$banner1_extFile);
    move_uploaded_file($advert_bannerThreeTmp, $this->bannerDir.$banner3_extFile);
                            
                            // call clear fields method
                            $this->_resetAdvertFields();

                            } // if
                            else {
                                $this->errs[] = FAILED_TO_SAVE_ADVERT;
                            } // else
                        
                        } // validation  
                                
                    } // banner 1 and 3

                    else if ($advert_bannerName && $advert_bannerTwoName && $advert_bannerThreeName) 
                    {
                        if ($this->_bannerValidation(
                            $advert_bannerThreeName, 
                            $advert_bannerThreeTmp,
                            $advert_bannerThreeSize)
                            &&
                            $this->_bannerValidation(
                            $advert_bannerName, 
                            $advert_bannerTmp,
                            $advert_bannerSize)
                            &&
                            $this->_bannerValidation(
                            $advert_bannerTwoName, 
                            $advert_bannerTwoTmp,
                            $advert_bannerTwoSize))
                        {

    $banner1_ext  = strtolower(pathinfo($advert_bannerName,PATHINFO_EXTENSION));
    $banner1_extFile = 'PB-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner1_ext;

    $banner2_ext  = strtolower(pathinfo($advert_bannerTwoName,PATHINFO_EXTENSION));
    $banner2_extFile = 'B2-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner2_ext;

    $banner3_ext  = strtolower(pathinfo($advert_bannerThreeName,PATHINFO_EXTENSION));
    $banner3_extFile = 'B3-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner3_ext;

                            if($this->_saveAdvertQuery(
                                    $advert_businessName,
                                    $advert_have_a_business,
                                    $advert_title,
                                    $advert_publish, 
                                    $advert_period, 
                                    $advert_loggedInEmail,
                                    $advert_details,
                                    $advert_bannerName = $banner1_extFile,
                                    $advert_bannerTwoName = $banner2_extFile,
                                    $advert_bannerThreeName = $banner3_extFile,
                                    $advertSerialToken))
                            {
                                $this->mgss[] = NEW_ADVERT_CREATED;

    // move uploads
    move_uploaded_file($advert_bannerTmp, $this->bannerDir.$banner1_extFile);
    move_uploaded_file($advert_bannerTwoTmp, $this->bannerDir.$banner2_extFile);
    move_uploaded_file($advert_bannerThreeTmp, $this->bannerDir.$banner3_extFile);
                            
                            // call clear fields method
                            $this->_resetAdvertFields();

                            } // if
                            else {
                                $this->errs[] = FAILED_TO_SAVE_ADVERT;
                            } // else
                        
                        } // validation  
                                
                    } // banner 1, 2 and 3

                } // primary banner was selected

            } // banners was selected for upload     

            else {

                if($this->_saveAdvertQueryNoBanners(
                    $advert_businessName,
                    $advert_have_a_business,
                    $advert_title,
                    $advert_publish, 
                    $advert_period, 
                    $advert_loggedInEmail,
                    $advert_details,
                    $advertSerialToken))
                {
                    $this->mgss[] = NEW_ADVERT_CREATED;
                    
                    // call clear fields method
                    $this->_resetAdvertFields();

                } // if
                else {
                    $this->errs[] = FAILED_TO_SAVE_ADVERT;
                } // else

            }   // no banner was selected     

        } // else validation

    } // endof _addNewAdvert



    /**
     * reset create new advert fields on successful submission
     * 
     * @return void
     * @access private
     */
    private function _resetAdvertFields()
    {
        ?>
        <script>
            $(function(){

                // reset form values
                $("#dataForm_createNewAdvert")[0].reset();

                // reset image preview
                $(".imagePreview").css("background-image", "url('_assets/images/pic_blank.jpg')");
                $(".imagePreview2").css("background-image", "url('_assets/images/pic_blank.jpg')");
                $(".imagePreview3").css("background-image", "url('_assets/images/pic_blank.jpg')");

                // hide delete button
                $("#deleteBanner1, #deleteBanner2, #deleteBanner3").addClass("displayClass");
                // change select text
                $("#selectTextOne").html("Select");
                // change select text
                $("#selectTextTwo").html("Select");
                // change select text
                $("#selectTextThree").html("Select");

                // clear stubborn bootstrap wysiwyg
                $("#dataFormAdvert_details").val('');
            });
        </script>
        <?php
    } // end

    /**
     * get business name from advert being saved
     * 
     * @param int $businessid the business profile pkey
     * @return string $g->Business_Name if the advert has a business, return business name; else return 'N/A'
     * 
     * @access private
     */
    private function _getBusinessNameforSavedAdvert($businessid) {
        $g_q = $this->conn->prepare("select Business_Name from business_profile where business_profile_id = :business_profile_id");
        $g_q->execute([
            "business_profile_id" => $businessid
        ]);
        $g = $g_q->fetchObject();

        return $g ? $g->Business_Name : 'N/A';
    }  // end

    /**
     * handles saving of new advert with banners; this method is called from "_addNewAdvert(..)"
     * 
     * @param string $advert_businessName incase the advert has the business, then we get the business id
     * @param string $advert_have_a_business if the advert is associated with an business
     * @param string $advert_title the advert title
     * @param string $advert_publish whether the advert is to be pubished as soon as the admin approves it
     * @param string $advert_period the date range the advet will run
     * @param string $advert_loggedInEmail the currently logged in email session
     * @param string $advert_details details concerning the advert
     * @param string $advert_banner1 this is the primary banner
     * @param string $advert_banner2 this is the second banner
     * @param string $advert_banner3 this is the third banner
     * @param string $advertSerialToken unique banner token
     * 
     * @return bool TRUE if saving was successful; FALSE if it failed
     */
    private function _saveAdvertQuery(
            $advert_businessName,
            $advert_have_a_business,
            $advert_title,
            $advert_publish, 
            $advert_period, 
            $advert_loggedInEmail,
            $advert_details,
            $advert_banner1, 
            $advert_banner2,
            $advert_banner3,
            $advertSerialToken)
    {

        // separate date range
        list($advertStartDate, $advertEndPeriod) = explode(' - ', $advert_period);

        $businessid = $advert_have_a_business == "yes" ? $advert_businessName : 0;

        // save advert
        $s_q3 = $this->conn->prepare("insert into advert_posts(
                business_profile_id,
                advert_title,
                start_period,
                end_period,
                publish,
                description,
                primary_banner_1,
                s_banner_2,
                s_banner_3,
                loggedEmail,
                status,
                advert_serial,
                Business_Name
                )
            values(     
                :business_profile_id,               
                :advert_title,
                :start_period,
                :end_period,
                :publish,
                :description,
                :primary_banner_1,
                :s_banner_2,
                :s_banner_3,
                :loggedEmail,
                :status,
                :advert_serial,
                :Business_Name)");

        $s3 = $s_q3->execute([
                'business_profile_id' => $advert_have_a_business == "yes" ? $advert_businessName : "N/A",
                'advert_title' => $advert_title,
                'start_period' => $advertStartDate,
                'end_period' => $advertEndPeriod,
                'publish' => $advert_publish,
                'description' => $advert_details,
                'primary_banner_1' => $advert_banner1,
                's_banner_2' => $advert_banner2,
                's_banner_3' => $advert_banner3,
                'loggedEmail' => $advert_loggedInEmail,
                'status' => $advert_publish == 'yes' ? 1 : 0,
                'advert_serial' => $advertSerialToken,
                'Business_Name' => $this->_getBusinessNameforSavedAdvert($businessid)
        ]);

        if ($s3) {
            return true;
        }
        else
        {
            return false;
        }
    } // end


    /**
     * handles saving of new adverts having no banners; this method is called from "_addNewAdvert(..)"
     * 
     * @param string $advert_businessName incase the advert has the business, then we get the business id
     * @param string $advert_have_a_business if the advert is associated with an business
     * @param string $advert_title the advert title
     * @param string $advert_publish whether the advert is to be pubished as soon as the admin approves it
     * @param string $advert_period the date range the advet will run
     * @param string $advert_loggedInEmail the currently logged in email session
     * @param string $advert_details details concerning the advert
     * @param string $advertSerialToken unique banner token
     * 
     * @return bool TRUE if saving was successful; FALSE if it failed
     */
    private function _saveAdvertQueryNoBanners(
                    $advert_businessName,
                    $advert_have_a_business,
                    $advert_title,
                    $advert_publish, 
                    $advert_period, 
                    $advert_loggedInEmail,
                    $advert_details,
                    $advertSerialToken)
    {

        // separate date range
        list($advertStartDate, $advertEndPeriod) = explode(' - ', $advert_period);

        $businessid = $advert_have_a_business == "yes" ? $advert_businessName : 0;

        // save advert
        $s_q4 = $this->conn->prepare("insert into advert_posts(
                business_profile_id,
                advert_title,
                start_period,
                end_period,
                publish,
                description,
                loggedEmail,
                status,
                advert_serial,
                Business_Name
                )
            values( 
                :business_profile_id,                   
                :advert_title,
                :start_period,
                :end_period,
                :publish,
                :description,
                :loggedEmail,
                :status,
                :advert_serial,
                :Business_Name)");

        $s4 = $s_q4->execute([
                'business_profile_id' => $advert_have_a_business == "yes" ? $advert_businessName : "N/A",
                'advert_title' => $advert_title,
                'start_period' => $advertStartDate,
                'end_period' => $advertEndPeriod,
                'publish' => $advert_publish,
                'description' => $advert_details,
                'loggedEmail' => $advert_loggedInEmail,
                'status' => $advert_publish == 'yes' ? 1 : 0,
                'advert_serial' => $advertSerialToken,
                'Business_Name' => $this->_getBusinessNameforSavedAdvert($businessid)
        ]);

        if ($s4) {
            return true;
        }
        else
        {
            return false;
        }
    } // end

    /**
     * advert banner validation method
     * 
     * @param string $bannerName the name of the banner
     * @param string $bannerTmp the temp name of the banner
     * @param string $bannerSize the size of the banner
     * 
     * @return bool TRUE if the banner passes the validation; FALSE if it fails
     */
    private function _bannerValidation($bannerName, $bannerTmp, $bannerSize)
    {    
        $bannerFile = $this->bannerDir . basename($bannerName);
        $bannerType  = strtolower(pathinfo($bannerFile,PATHINFO_EXTENSION));

        /*
        $checkBanner = getimagesize($bannerTmp);

        if($checkBanner == false) {
            $this->errs[] = INVALID_FILE_MIME;
            return false;
        }
        */
        // Check if file already exists
        if (file_exists($bannerFile)) 
        {
            $this->errs[] = FILE_ALREADY_EXISTS;
            return false;
        }
        // Check file size
        elseif ($bannerSize > 1000000) 
        {
            $this->errs[] = FILE_MAX_SIZE;
            return false;
        }
        elseif($bannerType != "jpg" && $bannerType != "png" && $bannerType != "jpeg" && $bannerType != "gif")
        {
            $this->errs[] = IMAGES_ONLY;
            return false;               
        }
        else
        {           
            return true;
        }

    } // end

    /**
     * generates a unique advert serial token
     * 
     * @return string
     */
    private function _advertSerial()
    {
        return "ad".sha1(uniqid(mt_rand(), true));
    } // end


    /**
     * this method displays a selected adverts' details in a modal for viewing
     * 
     * @param int $selectedadvertId the id associated with the advert whose information we want to view
     * @return void
     */
    function _loadSelectedAdvertDetails($selectedadvertId) 
    {
        $l_q1 = $this->conn->prepare($this->_adverts()." where advert_post_id = :advert_post_id");
        $l_q1->execute([
            "advert_post_id" => $selectedadvertId
        ]);
        $l1 = $l_q1->fetchObject();

        $advertpostedObj = new DateTime($l1->date_post);
        $advertposted = $advertpostedObj->format('d M, Y');

        $primarybanner    = $l1->primary_banner_1 != NULL ? $l1->primary_banner_1 : "pic_blank.jpg";
        $secondarybanner1 = $l1->s_banner_2 != NULL ? $l1->s_banner_2 : "pic_blank.jpg";
        $secondarybanner2 = $l1->s_banner_3 != NULL ? $l1->s_banner_3 : "pic_blank.jpg";


        $bannerp1 = $this->clientSideBannerDir.$primarybanner;
        $banners1 = $this->clientSideBannerDir.$secondarybanner1;
        $banners2 = $this->clientSideBannerDir.$secondarybanner2;

        if ($l1) 
        {

            // --- >> get likes
            $getAdvertLikesQ = $this->conn->prepare($this->_advertLikeStats(). " where advert_post_id = :advert_post_id");
            $getAdvertLikesQ->execute([
                "advert_post_id" => $l1->advert_post_id
            ]);
            $getAdvertLikes = $getAdvertLikesQ->fetchObject();

            // --- >> get views
            $getAdvertViewsQ = $this->conn->prepare($this->_advertViewStats(). " where advert_post_id = :advert_post_id");
            $getAdvertViewsQ->execute([
                "advert_post_id" => $l1->advert_post_id
            ]);
            $getAdvertViews = $getAdvertViewsQ->fetchObject();

            // --- >> update views
            $this->updateAdvertViews($selectedadvertId);
    ?>

        <div class="advert-containerWrapper">

            <div class="row">
                
                <div class="col-md-2 col-xs-5">

                    <ul class="advertVerticalSlide">
                        <li data-thumb="<?php echo $bannerp1; ?>">
                            <div class="thumb-image detail_images bannerThumbNails"> <img src="<?php echo $bannerp1; ?>" class="img-responsive" alt="" onmouseover="document.getElementById('mainBannerImageView').src=this.src;document.getElementById('mainBannerImageView').src=this.src;"> </div>
                        </li>
                        <li data-thumb="<?php echo $banners1; ?>">
                            <div class="thumb-image bannerThumbNails"> <img src="<?php echo $banners1; ?>" class="img-responsive" alt="" onmouseover="document.getElementById('mainBannerImageView').src=this.src;document.getElementById('mainBannerImageView').src=this.src;"> </div>
                        </li>
                        <li data-thumb="<?php echo $banners2; ?>">
                        <div class="thumb-image bannerThumbNails"> <img src="<?php echo $banners2; ?>" class="img-responsive" alt="" onmouseover="document.getElementById('mainBannerImageView').src=this.src;document.getElementById('mainBannerImageView').src=this.src;"> </div>
                        </li> 
                    </ul>
                    
                </div>
                
                <div class="col-md-4 col-xs-7">
                    <label id="mainBannerView">
                        <!-- /. data-imagezoom="true"  -->
                        <img src="<?php echo $bannerp1; ?>" id="mainBannerImageView" class="img-responsive" alt="">
                    </label>
                </div>
                
                <div class="col-md-6 col-xs-12">
                    <h3><?php echo $l1->advert_title; ?></h3>
                    <p id="advert-description">
                        <?php 
                            echo trim(htmlspecialchars_decode($l1->description), " ");
                        ?> 
                    </p>
                    <p>
                        Posted on: <?php echo $advertposted; ?>
                    </p>

                    <div class="stat_wrapper">
                        <p>
                            <a href="javascript:void(0)" class="btn btn-round bg-maroon btn-xs text-white" <?php 
                                    echo $getAdvertLikes->liked_by_email == $l1->loggedEmail ? "title='You have already liked this advert' data-toggle='tooltip'" : ''; ?>>
                                <?php echo $getAdvertLikes->number_likes ? $getAdvertLikes->number_likes : 0 ;?> Likes
                            </a>
                    
                            <a href="javascript:void(0)" class="btn bg-teal btn-round btn-xs text-white"><?php echo $getAdvertViews->advert_view_numbers ? $getAdvertViews->advert_view_numbers : 0;?> Views
                            </a>

                        </p>
                    </div>

                </div>

            </div><!-- /. row -->

            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                </div>
            </div>

        </div><!-- /. advert-containerWrapper -->
    <?php 
        }
        else
        {
            echo "<p class='text-center text-danger'>Error: either selected advert is no longer available or has been discontinued!</p>";
        } // else

    } // end


    /**
     * handles async update of advert views stats
     * 
     * @param int $selectedadvertId the id of the advert currently being viewed
     * @return void
     */
    private function updateAdvertViews($selectedadvertId) {

        // --- >> check whether advert is already there << ---

        // --- >> if so; update view count << --- 
        $c_ad_view_q = $this->conn->prepare($this->_advertViewStats(). " where advert_post_id = :advert_post_id");
        $c_ad_view_q->execute([
            "advert_post_id" => $selectedadvertId
        ]);
        $c_ad_view = $c_ad_view_q->fetchObject();

        if ($c_ad_view) 
        {
            $u_ad_view_q = $this->conn->prepare("update advert_views set advert_view_numbers = :advert_view_numbers where advert_post_id = :advert_post_id");
            $u_ad_view_q->execute([
                "advert_view_numbers" => 1 + $c_ad_view->advert_view_numbers,
                "advert_post_id" => $selectedadvertId
            ]);
        }

        // --- >> else insert new view << --- 
        else
        {
            $i_ad_view_q = $this->conn->prepare("insert into advert_views (advert_post_id, advert_view_numbers) values (:advert_post_id, :advert_view_numbers)");
            $i_ad_view_q->execute([
                "advert_post_id" => $selectedadvertId,
                "advert_view_numbers" => 1
            ]);
        }

    } // end


    /**
     * handles when a person likes an adverts
     * 
     * @param int $advertPkeyCounter the id of the advert being liked
     * @param string $likedAdvertLoggedInEmail the email address of the person who is liking the advert
     * 
     * @return void
     */
    function _likeAdvertMethod($advertPkeyCounter, $likedAdvertLoggedInEmail) 
    {
        // check whether advert was already liked
        $checkAdvertLikedQ = $this->conn->prepare($this->_advertLikeStats(). " where advert_post_id = :advert_post_id and liked_by_email = :liked_by_email");
        $checkAdvertLiked = $checkAdvertLikedQ->execute([
            "advert_post_id" => $advertPkeyCounter, 
            "liked_by_email" => $likedAdvertLoggedInEmail
        ]);
        $checkAdvertLiked = $checkAdvertLikedQ->fetchObject();

        // unlike advert
        if ($checkAdvertLiked) 
        {
            $dAdvertStatQuery = $this->conn->prepare("delete from advert_likes where advert_post_id = :advert_post_id and liked_by_email = :liked_by_email");
            $dAdvertStat = $dAdvertStatQuery->execute([
                "advert_post_id" => $advertPkeyCounter, 
                "liked_by_email" => $likedAdvertLoggedInEmail
            ]);
            if ($dAdvertStat) 
            {
                echo 2;
            }
            else
            {
                echo 3;
            }
        }

        // like advert
        else
        {

            $iAdvertStatQuery = $this->conn->prepare("insert into advert_likes(
                advert_post_id, 
                number_likes, 
                liked_by_email) 
            values(
                :advert_post_id, 
                :number_likes, 
                :liked_by_email)");

            $iAdvertStat = $iAdvertStatQuery->execute([
                "advert_post_id" => $advertPkeyCounter, 
                "number_likes" => 1,
                "liked_by_email" => $likedAdvertLoggedInEmail
            ]);
            if ($iAdvertStat) 
            {
                echo 1;
            }
            else
            {
                echo 3;
            }
        }
    } // end


    /**
     * loads list of adverts which are displayed on the right-side tab on the client-side
     * 
     * @return void
     */
    function _retrieveAdverts()
    {
        $r_q1 = $this->conn->prepare($this->_adverts()." where approved = :approved and status = :status and publish = :publish order by date_post limit 7");
        $r_q1->execute([
            "approved" => "yes",
            "status" => 1,
            "publish" => "yes"
        ]);
        $r1 = $r_q1->fetchAll();

        if ($r1) 
        {
        
        // check if logged in user fav an advert
        $fetch_advert_fav_q = $this->conn->prepare("select fav_id from favorites where advert_post_id = :advert_post_id and email_address = :email_address and fav_status = :fav_status");


        foreach($r1 as $row) { 

    /*
            $r_q2 = $this->conn->prepare($this->_businessprofile()." where business_profile_id = :business_profile_id");
            $r_q2->execute([
                "business_profile_id" => $row['post_id']
            ]);
            $r2 = $r_q2->fetchObject();
    */
            /*
            $r_q2 = $this->conn->prepare($this->_businessprofile());
            $r_q2->execute();
            $r2 = $r_q2->fetchObject();
            */

            $advertpostedObj = new DateTime($row['date_post']);
            $advertposted = $advertpostedObj->format('d M, Y');

            $bannerAvailable = $row['primary_banner_1'] != NULL ? $row['primary_banner_1'] : "pic_blank.jpg";

            $uploadDir = '_assets/clientUploads/'.$bannerAvailable;

            $checkIfadvertWasLikedQuery = $this->conn->prepare($this->_advertLikeStats()." where liked_by_email = :liked_by_email and advert_post_id = :advert_post_id");
            $checkIfadvertWasLikedQuery->execute([
                "liked_by_email" => $_SESSION["ur_email"],
                "advert_post_id" => $row["advert_post_id"]
            ]);
            $checkIfadvertWasLiked = $checkIfadvertWasLikedQuery->fetchObject();

            // --- >> was advert added to favorites
            $fetch_advert_fav_q->execute([
                "advert_post_id" => $row['advert_post_id'],
                "email_address" => $_SESSION['ur_email'],
                "fav_status" => 1
            ]);
            $advertFavoriteOrNot = $fetch_advert_fav_q->fetchObject();

            ?> 
            <div class="panel_items">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                    <img src="<?php echo $uploadDir; ?>">  
                    </div>

                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <h4 class="post_title"><?php echo $row['advert_title']; ?></h4>   
                    </div> 
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">                            
                        <div class="advert_body">
                            <p class="text-muted">
                                <?php 
                                //echo trim(htmlspecialchars_decode($row['description']), " ");

                                echo strlen(trim(htmlspecialchars_decode($row['description']), " ")) > 70 ? substr(trim(htmlspecialchars_decode($row['description']), " "), 0, 100).'...' : trim(htmlspecialchars_decode($row['description']), " ");
                                ?>                                    
                            </p>
                        </div> 
                    </div>
                </div> 

                <div class="row">
                    <div class="col-xs-6">
                        <small class="posted_on"><strong>Posted on:</strong> <?php echo $advertposted; ?></small> 
                    </div>
                    <div class="col-xs-6">
                        <ul class="rightSideActionMenu">
                            <li>
                                <a class="fa-2c" href="#dataModalAdverts" title="view more" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id'];?>')"><i class="fa fa-location-arrow text-olive"></i>
                                </a>
                            </li>
                            <li>
                            <a class="fa-2c" href="javascript:void(0)" title="Like" id="advertLikeLink<?php echo $row['advert_post_id'];?>" onclick="likethisAdvertfunction('<?php echo $row['advert_post_id'];?>', '<?php echo $_SESSION["ur_email"]; ?>')">
                                <?php echo $checkIfadvertWasLiked == true ? '<i class="fa fa-thumbs-up text-info"></i>' : '<i class="fa fa-thumbs-o-up text-info"></i>'; ?>
                            </a>
                            </li>
                            <li>
                        <a class="fa-2c" href="javascript:void(0)" title="Favorite" id="advertFavLink<?php echo $row['advert_post_id'];?>" onclick="favoriteAdvertfunction('<?php echo $row['advert_post_id'];?>')">
                            <?php echo $advertFavoriteOrNot == true ? '<i class="fa fa-heart text-maroon"></i>' : '<i class="fa fa-heart-o text-maroon"></i>'; ?>
                        </a>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>     

            <?php } ?>
        <br>
    <p class="text-center text-bold text-danger text-underline">
        <a href="_navigation.php?pageNavigation=myAdvertList">View all adverts</a>
    </p>

        <?php
        }
        else
        {
            echo "<p class='text-center text-info'>No Adverts available yet!</p>";
        }

    } // end


    /**
     * handles the retrieval of a selected advert likes and views counter to display on advertsList.php
     * 
     * @param int $advertPostId the advert post id whose likes and views we need
     * @return array 'viewsObj' for views count; 'likesObj' for likes count
     */
    function _ret_Views_and_Likes_for_advertList($advertPostId)
    {
            // --- >> get likes
        $getAdvertLikesQ = $this->conn->prepare($this->_advertLikeStats(). " where advert_post_id = :advert_post_id");
        $getAdvertLikesQ->execute([
            "advert_post_id" => $advertPostId
        ]);
        $getAdvertLikes = $getAdvertLikesQ->fetchObject();

        // --- >> get views
        $getAdvertViewsQ = $this->conn->prepare($this->_advertViewStats(). " where advert_post_id = :advert_post_id");
        $getAdvertViewsQ->execute([
            "advert_post_id" => $advertPostId
        ]);
        $getAdvertViews = $getAdvertViewsQ->fetchObject();

        return json_encode(array('viewsObj' => $getAdvertViews->advert_view_numbers, 'likesObj' => $getAdvertLikes->number_likes));
    } // end


    /**
     * checks whether a selected advert was liked or not such that we can 
     * handle the like and unlike changes appropriately
     * 
     * @param int $advertPostId we pass the advert id we need to check for this
     * @return bool TRUE then the advert was already liked, we change its state to unlike; FALSE the advert not yet liked, so
     *              we change its stated to liked
     */
    function _ret_if_advert_was_liked($advertPostId) 
    {
        $checkIfadvertWasLikedQuery = $this->conn->prepare($this->_advertLikeStats()." where liked_by_email = :liked_by_email and advert_post_id = :advert_post_id");
        $checkIfadvertWasLikedQuery->execute([
        "liked_by_email" => $_SESSION["ur_email"],
        "advert_post_id" => $advertPostId
        ]);
        $checkIfadvertWasLiked = $checkIfadvertWasLikedQuery->fetchObject();

        if ($checkIfadvertWasLiked) {
            return true;
        }
        else
        {
            return false;
        }
    } // end


    /**
     * this loads the list of adverts which have been approved by admin, still active and haved been published by client under the
     * advertsList section
     * 
     * this is where we also employ a method from the pagination class we extended to paginate the adverts list
     * 
     * @return void
     */
    function _returnAdvertList() 
    {
        
        $limit = 10;
        
        //get number of rows
        $queryNum = $this->obj->query("SELECT COUNT(*) as postNum FROM advert_posts");
        $resultNum = $queryNum->fetch_assoc();
        $rowCount = $resultNum['postNum'];
        
        //initialize pagination class
        $pagConfig = array(
            'totalRows' => $rowCount,
            'perPage' => $limit,
            'link_func' => 'searchFilter'
        );
        $pagination =  new Pagination($pagConfig);


        // limit the number of items to show to 9
        $r_q1 = $this->obj->query("SELECT advert_post_id, business_profile_id, advert_title, date_post, start_period, primary_banner_1, loggedEmail, description FROM advert_posts where status = '1' AND approved = 'yes' AND publish = 'yes' ORDER BY advert_post_id DESC LIMIT $limit");
        
        if ($r_q1->num_rows > 0) 
        {

        ?>

    <div id="advertsListContent">
        
        <?php
        
        // check if logged in user fav an advert
        $fetch_advert_fav_q = $this->conn->prepare("select fav_id from favorites where advert_post_id = :advert_post_id and email_address = :email_address and fav_status = :fav_status");


        while($row = $r_q1->fetch_assoc())
        {

        // --- >> change date format
        $advertpostedObj = new DateTime($row['start_period']);
        $advertposted = $advertpostedObj->format('d M, Y');

        // --- >> banner
        $bannerAvailable = $row['primary_banner_1'] != NULL ? $row['primary_banner_1'] : "pic_blank.jpg";
        $uploadDir = '././_assets/clientUploads/'.$bannerAvailable;

        // --- >> get views and likes method
        $advert_views = json_decode($this->_ret_Views_and_Likes_for_advertList($row['advert_post_id']));

        // --- >> was advert liked : boolean return
        $advertLikedOrNot = $this->_ret_if_advert_was_liked($row['advert_post_id']);

        // --- >> was advert added to favorites
        $fetch_advert_fav_q->execute([
            "advert_post_id" => $row['advert_post_id'],
            "email_address" => $_SESSION['ur_email'],
            "fav_status" => 1
        ]);
        $advertFavoriteOrNot = $fetch_advert_fav_q->fetchObject();

        ?>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 adverts_scroll_wrapper" id="<?php echo $row['advert_post_id']; ?>">
        
        <div class="adverts_panel_container">
            <div class="data-jplist-item-name">
            
            <div class="advertPolaroid_container">
                <div class="advertPolaroid_title">
                <h4 class="">
                    <?php echo $row['advert_title']; ?>
                </h4>
                </div>

                <div class="advert_view_list advert-view-first">

                <img src="<?php echo $uploadDir; ?>" alt="banner" style="width: 100%; height: 230px" class="adert_banner_image">

                <div class="advert_mask">
                    <p></p>
                    <div class="overflow_tools overflow-tools-bottom">
                    <a href="#" class="btn btn-xs btn-round bg-red">
                        <span class="badge"><?php echo $advert_views->likesObj ? $advert_views->likesObj : 0; ?></span> Likes                                     
                    </a>
                    <a href="#" class="btn btn-xs btn-round bg-blue">
                        <span class="badge"><?php echo $advert_views->viewsObj ? $advert_views->viewsObj : 0; ?></span> Views
                    </a>
                    </div>
                </div>
                <!-- /. advert_mask -->
                
                </div>
                <!-- /. advert_view_list , advert-view-first -->

                <div class="advert_description_wrapper">
                    <p class="advert_description">
                    <?php  echo strlen(trim(htmlspecialchars_decode($row['description']), " ")) > 70 ? substr(trim(htmlspecialchars_decode($row['description']), " "), 0, 100).'...' : trim(htmlspecialchars_decode($row['description']), " ");?> <a href="#dataModalAdverts" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id'];?>')" class="text-maroon read_advert_details">read more</a>                                 
                    </p>

                    <div class="row">
                    <div class="col-xs-6">
                        <p class="text-warning">
                        <small><?php echo "Posted: ". $advertposted; ?></small>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="text-right text-navy">
                        <a href="javascript:void(0)" title="Like" id="advertLikeLink<?php echo $row['advert_post_id'];?>" onclick="likethisAdvertfunction('<?php echo $row['advert_post_id'];?>', '<?php echo $_SESSION["ur_email"]; ?>')">
                            <?php echo $advertLikedOrNot == true ? '<i class="fa fa-thumbs-up text-info"></i>' : '<i class="fa fa-thumbs-o-up text-info"></i>'; ?>
                        </a>
                        &nbsp;
                        <a href="javascript:void(0)" title="Favorite" id="advertFavLink<?php echo $row['advert_post_id'];?>" onclick="favoriteAdvertfunction('<?php echo $row['advert_post_id'];?>')">
                            <?php echo $advertFavoriteOrNot == true ? '<i class="fa fa-heart text-maroon"></i>' : '<i class="fa fa-heart-o text-maroon"></i>'; ?>
                        </a>
                        </p>
                    </div>                                    
                    </div>
                </div>
                <!-- /. advert_description -->

                </div>
                <!-- /.advertPolaroid_container -->

            </div>
            <!-- /. data-jplist-item-name -->
        </div>
        <!-- /. adverts_panel_container -->
    </div> 
    <!-- /. col-xs-12 col-sm-4 col-md-4 -->


        <?php
        } // while loop
        ?>

    </div>
    <!-- /. advertsListContent -->                 

    <!-- /. load more animation -->
    <!--
    <div class="loadingAdverts_wrapper">
        <img src="_assets/images/advertLoader.gif" alt="Loading adverts..." class="">
    </div>
    -->
    <!-- loadingAdverts_wrapper -->

    <!-- start of pagination -->
    <div class="row">
        <div class="col-sm-12 col-md-12 col-xs-12">
            <div class="pagination_wrapper">
                <?php  echo $pagination->createLinks(); ?>            
            </div>        
        </div>
    </div>
    <!-- /. end of pagination -->
    <?php 

        } // adverts are available
        else
        {
            echo "<p class='text-center text-info'>Info: No Adverts Currently Available!</p>";
        } // else

    } // end


    /**
     * handles the advert list pagination search and order operation
     * 
     * @param int $page the page number
     * @param string $keywords the search term we're looking for.
     * @param string $sortBy the order by option; either asc or desc
     * 
     * @return void
     * @access public
     */
    function _advertListPagination($page, $keywords, $sortBy)
    {        
        $start = !empty($page) ? $page : 0;
        $limit = 10;

        //set conditions for search
        $whereSQL = $orderSQL = '';

        if(!empty($keywords)){
            $whereSQL = "WHERE advert_title LIKE '%".$keywords."%' AND status = '1' AND approved = 'yes' AND publish = 'yes'";
        } else{
            $whereSQL = "WHERE status = '1' AND approved = 'yes' AND publish = 'yes'";
        }
        if(!empty($sortBy)){
            $orderSQL = " ORDER BY start_period ".$sortBy;
        }else{
            $orderSQL = " ORDER BY start_period DESC ";
        }
        //get number of rows

        $queryNum = $this->obj->query("SELECT COUNT(*) as postNum FROM advert_posts ".$whereSQL.$orderSQL);
        $resultNum = $queryNum->fetch_assoc();
        $rowCount = $resultNum['postNum'];

        //initialize pagination class
        $pagConfig = array(
            'currentPage' => $start,
            'totalRows' => $rowCount,
            'perPage' => $limit,
            'link_func' => 'searchFilter'
        );
        $pagination =  new Pagination($pagConfig);
        
        //get rows
        $query = $this->obj->query("SELECT * FROM advert_posts $whereSQL $orderSQL LIMIT $start,$limit");
        
        if($query->num_rows > 0){ 

    ?>

    <div id="advertsListContent">
        
        <?php
        
        // check if logged in user fav an advert
        $fetch_advert_fav_q = $this->conn->prepare("select fav_id from favorites where advert_post_id = :advert_post_id and email_address = :email_address and fav_status = :fav_status");
        

        while($row = $query->fetch_assoc()){    


                // --- >> change date format
                $advertpostedObj = new DateTime($row['start_period']);
                $advertposted = $advertpostedObj->format('d M, Y');

                // --- >> banner
                $bannerAvailable = $row['primary_banner_1'] != NULL ? $row['primary_banner_1'] : "pic_blank.jpg";
                $uploadDir = '././_assets/clientUploads/'.$bannerAvailable;

                // --- >> get views and likes method
                $advert_views = json_decode($this->_ret_Views_and_Likes_for_advertList($row['advert_post_id']));

                // --- >> was advert liked : boolean return
                $advertLikedOrNot = $this->_ret_if_advert_was_liked($row['advert_post_id']);

                // --- >> was advert added to favorites
                $fetch_advert_fav_q->execute([
                    "advert_post_id" => $row['advert_post_id'],
                    "email_address" => $_SESSION['ur_email'],
                    "fav_status" => 1
                ]);
                $advertFavoriteOrNot = $fetch_advert_fav_q->fetchObject();         

            ?>


    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 adverts_scroll_wrapper" id="<?php echo $row['advert_post_id']; ?>">
        
        <div class="adverts_panel_container">
            <div class="data-jplist-item-name">
            
            <div class="advertPolaroid_container">
                <div class="advertPolaroid_title">
                <h4 class="">
                    <?php echo $row['advert_title']; ?>
                </h4>
                </div>

                <div class="advert_view_list advert-view-first">

                <img src="<?php echo $uploadDir; ?>" alt="banner" style="width: 100%; height: 230px" class="adert_banner_image">

                <div class="advert_mask">
                    <p></p>
                    <div class="overflow_tools overflow-tools-bottom">
                    <a href="#" class="btn btn-xs btn-round bg-red">
                        <span class="badge"><?php echo $advert_views->likesObj ? $advert_views->likesObj : 0; ?></span> Likes                                     
                    </a>
                    <a href="#" class="btn btn-xs btn-round bg-blue">
                        <span class="badge"><?php echo $advert_views->viewsObj ? $advert_views->viewsObj : 0; ?></span> Views
                    </a>
                    </div>
                </div>
                <!-- /. advert_mask -->
                
                </div>
                <!-- /. advert_view_list , advert-view-first -->

                <div class="advert_description_wrapper">
                    <p class="advert_description">
                    <?php  echo strlen(trim(htmlspecialchars_decode($row['description']), " ")) > 70 ? substr(trim(htmlspecialchars_decode($row['description']), " "), 0, 100).'...' : trim(htmlspecialchars_decode($row['description']), " ");?> <a href="#dataModalAdverts" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id'];?>')" class="text-maroon read_advert_details">read more</a>                                 
                    </p>

                    <div class="row">
                    <div class="col-xs-6">
                        <p class="text-warning">
                        <small><?php echo "Posted: ". $advertposted; ?></small>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="text-right text-navy">
                        <a href="javascript:void(0)" title="Like" id="advertLikeLink<?php echo $row['advert_post_id'];?>" onclick="likethisAdvertfunction('<?php echo $row['advert_post_id'];?>', '<?php echo $_SESSION["ur_email"]; ?>')">
                            <?php echo $advertLikedOrNot == true ? '<i class="fa fa-thumbs-up text-info"></i>' : '<i class="fa fa-thumbs-o-up text-info"></i>'; ?>
                        </a>
                        &nbsp;
                        <a href="javascript:void(0)" title="Favorite" id="advertFavLink<?php echo $row['advert_post_id'];?>" onclick="favoriteAdvertfunction('<?php echo $row['advert_post_id'];?>')">
                            <?php echo $advertFavoriteOrNot == true ? '<i class="fa fa-heart text-maroon"></i>' : '<i class="fa fa-heart-o text-maroon"></i>'; ?>
                        </a>
                        </p>
                    </div>                                    
                    </div>
                </div>
                <!-- /. advert_description -->

                </div>
                <!-- /.advertPolaroid_container -->

            </div>
            <!-- /. data-jplist-item-name -->
        </div>
        <!-- /. adverts_panel_container -->
    </div> 
    <!-- /. col-xs-12 col-sm-4 col-md-4 -->

            <?php } // while loop ?>

        </div>
        <!-- /. advertsListContent -->                 

        <!-- start of pagination -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12">
                <div class="pagination_wrapper">
                    <?php echo $pagination->createLinks(); ?>            
                </div>        
            </div>
        </div>
        <!-- /. end of pagination -->
    <?php 
        }

    } // end


    /** 
     * load more adverts in advertList for default view; this method was originally being used for infinite scroll effect
     * 
     * @param int $advertLastId the last advert id in the group scroll before loading more
     */
    function _loadMoreAdvertsMethod($advertLastId)
    {
        $l_q2 = $this->conn->prepare("select advert_post_id, business_profile_id, advert_title, date_post, start_period, primary_banner_1, loggedEmail, description from advert_posts WHERE advert_post_id > :advert_post_id and status = :status and approved = :approved and publish = :publish limit 3");
        $l_q2->execute([
            "advert_post_id" => $advertLastId,
            "status" => 1,
            "approved" => "yes",
            "publish" => "yes"
        ]);
        $l2 = $l_q2->fetchAll();

        foreach($l2 as $row)
        {

            // --- >> change date format
            $advertpostedObj = new DateTime($row['start_period']);
            $advertposted = $advertpostedObj->format('d M, Y');

            // --- >> banner
            $bannerAvailable = $row['primary_banner_1'] != NULL ? $row['primary_banner_1'] : "pic_blank.jpg";
            $uploadDir = '_assets/clientUploads/'.$bannerAvailable;

            // --- >> get views and likes method
            $advert_views = json_decode($this->_ret_Views_and_Likes_for_advertList($row['advert_post_id']));

            // --- >> was advert liked : boolean return
            $advertLikedOrNot = $this->_ret_if_advert_was_liked($row['advert_post_id']);

    ?>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 adverts_scroll_wrapper" id="<?php echo $row['advert_post_id']; ?>">
        
        <div class="adverts_panel_container">
            <div class="data-jplist-item-name">
            
            <div class="advertPolaroid_container">
                <div class="advertPolaroid_title">
                <h4 class="">
                    <?php echo $row['advert_title']; ?>
                </h4>
                </div>

                <div class="advert_view_list advert-view-first">

                <img src="<?php echo $uploadDir; ?>" alt="banner" style="width: 100%; height: 230px" class="adert_banner_image">

                <div class="advert_mask">
                    <p></p>
                    <div class="overflow_tools overflow-tools-bottom">
                    <a href="#" class="btn btn-xs btn-round bg-red">
                        <span class="badge"><?php echo $advert_views->likesObj ? $advert_views->likesObj : 0; ?></span> Likes                                     
                    </a>
                    <a href="#" class="btn btn-xs btn-round bg-blue">
                        <span class="badge"><?php echo $advert_views->viewsObj ? $advert_views->viewsObj : 0; ?></span> Views
                    </a>
                    </div>
                </div>
                <!-- /. advert_mask -->
                
                </div>
                <!-- /. advert_view_list , advert-view-first -->

                <div class="advert_description_wrapper">
                    <p class="advert_description">
                    <?php  echo strlen(trim(htmlspecialchars_decode($row['description']), " ")) > 70 ? substr(trim(htmlspecialchars_decode($row['description']), " "), 0, 100).'...' : trim(htmlspecialchars_decode($row['description']), " ");?> <a href="#dataModalAdverts" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id'];?>')" class="text-maroon read_advert_details">read more</a>                                 
                    </p>

                    <div class="row">
                    <div class="col-xs-6">
                        <p class="text-warning">
                        <small><?php echo "Posted: ". $advertposted; ?></small>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="text-right text-navy">
                        <a  href="javascript:void(0)" title="Like" id="advertLikeLink<?php echo $row['advert_post_id'];?>" onclick="likethisAdvertfunction('<?php echo $row['advert_post_id'];?>', '<?php echo $_SESSION["ur_email"]; ?>')">
                            <?php echo $advertLikedOrNot == true ? '<i class="fa fa-thumbs-up text-info"></i>' : '<i class="fa fa-thumbs-o-up text-info"></i>'; ?>
                        </a>
                        &nbsp;
                        <a href="javascript:void(0)" title="Favorite" id="advertFavLink<?php echo $row['advert_post_id'];?>" onclick="favoriteAdvertfunction('<?php echo $row['advert_post_id'];?>')">
                            <?php echo $advertFavoriteOrNot == true ? '<i class="fa fa-heart text-maroon"></i>' : '<i class="fa fa-heart-o text-maroon"></i>'; ?>
                        </p>
                    </div>                                    
                    </div>
                </div>
                <!-- /. advert_description -->

                </div>
                <!-- /.advertPolaroid_container -->

            </div>
            <!-- /. data-jplist-item-name -->
        </div>
        <!-- /. adverts_panel_container -->
    </div> 
    <!-- /. col-md-4 -->
    <?php
        } // while

    } // end


    /**
     * handles the ordering by of adverts list; this method was originally being used for infinite scroll effect
     * 
     * @param int $ordAdvertsVal the advert id to start ordering from
     */
    function _advertsOrderFilter($ordAdvertsVal)
    {
        // fetch all data
        $r_q2 = mysqli_query($this->db, "select * from advert_posts where status = '1' and approved = 'yes' and publish = 'yes'");
        // total results
        $totatAdverts = mysqli_num_rows($r_q2);


        // limit the number of items to show to 9
        $r_q2 = mysqli_query($this->db, "select advert_post_id, business_profile_id, advert_title, date_post, start_period, primary_banner_1, loggedEmail, description from advert_posts where status = '1' and approved = 'yes' and publish = 'yes' order by start_period ".$ordAdvertsVal." limit 9");
        
        if ($totatAdverts > 0) 
        {

        ?>

    <div id="advertsListContentFilter">

        <input type="hidden" id="dataAdverts_totalSortedCount" value="<?php echo $totatAdverts; ?>">
        <input type="hidden" id="dataOrderValue" value="<?php echo $ordAdvertsVal; ?>">
        <?php
        while($row = mysqli_fetch_assoc($r_q2))
        {

        // --- >> change date format
        $advertpostedObj = new DateTime($row['start_period']);
        $advertposted = $advertpostedObj->format('d M, Y');
        $advertposted2 = $advertpostedObj->format('Y.m.d');

        // --- >> banner
        $bannerAvailable = $row['primary_banner_1'] != NULL ? $row['primary_banner_1'] : "pic_blank.jpg";
        $uploadDir = '././_assets/clientUploads/'.$bannerAvailable;

        // --- >> get views and likes method
        $advert_views = json_decode($this->_ret_Views_and_Likes_for_advertList($row['advert_post_id']));

        // --- >> was advert liked : boolean return
        $advertLikedOrNot = $this->_ret_if_advert_was_liked($row['advert_post_id']);

        ?>

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 adverts_filter_scroll_wrapper" id="<?php echo $row['start_period']; ?>">
        
        <div class="adverts_panel_container">
            <div class="data-jplist-item-name">
            
            <div class="advertPolaroid_container">
                <div class="advertPolaroid_title">
                <h4 class="">
                    <?php echo $row['advert_title']; ?>
                </h4>
                </div>

                <div class="advert_view_list advert-view-first">

                <img src="<?php echo $uploadDir; ?>" alt="banner" style="width: 100%; height: 230px" class="adert_banner_image">

                <div class="advert_mask">
                    <p></p>
                    <div class="overflow_tools overflow-tools-bottom">
                    <a href="#" class="btn btn-xs btn-round bg-red">
                        <span class="badge"><?php echo $advert_views->likesObj ? $advert_views->likesObj : 0; ?></span> Likes                                     
                    </a>
                    <a href="#" class="btn btn-xs btn-round bg-blue">
                        <span class="badge"><?php echo $advert_views->viewsObj ? $advert_views->viewsObj : 0; ?></span> Views
                    </a>
                    </div>
                </div>
                <!-- /. advert_mask -->
                
                </div>
                <!-- /. advert_view_list , advert-view-first -->

                <div class="advert_description_wrapper">
                    <p class="advert_description">
                    <?php  echo strlen(trim(htmlspecialchars_decode($row['description']), " ")) > 70 ? substr(trim(htmlspecialchars_decode($row['description']), " "), 0, 100).'...' : trim(htmlspecialchars_decode($row['description']), " ");?> <a href="#dataModalAdverts" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id'];?>')" class="text-maroon read_advert_details">read more</a>                                 
                    </p>

                    <div class="row">
                    <div class="col-xs-6">
                        <p class="text-warning">
                        <small><?php echo "Posted: ". $advertposted; ?></small>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="text-right text-navy">
                        <a  href="javascript:void(0)" title="Like" id="advertLikeLink<?php echo $row['advert_post_id'];?>" onclick="likethisAdvertfunction('<?php echo $row['advert_post_id'];?>', '<?php echo $_SESSION["ur_email"]; ?>')">
                            <?php echo $advertLikedOrNot == true ? '<i class="fa fa-thumbs-up text-info"></i>' : '<i class="fa fa-thumbs-o-up text-info"></i>'; ?>
                        </a>
                        &nbsp;
                        <a href="javascript:void(0)" title="Favorite" id="advertFavLink<?php echo $row['advert_post_id'];?>" onclick="favoriteAdvertfunction('<?php echo $row['advert_post_id'];?>')">
                            <?php echo $advertFavoriteOrNot == true ? '<i class="fa fa-heart text-maroon"></i>' : '<i class="fa fa-heart-o text-maroon"></i>'; ?>
                        </p>
                    </div>                                    
                    </div>
                </div>
                <!-- /. advert_description -->

                </div>
                <!-- /.advertPolaroid_container -->

            </div>
            <!-- /. data-jplist-item-name -->
        </div>
        <!-- /. adverts_panel_container -->
    </div> 
    <!-- /. col-xs-12 col-sm-4 col-md-4 -->


        <?php
        } // while loop
        ?>


    </div>
    <!-- /. advertsListContent -->                 

    <!-- /. load more animation -->
    <!--
    <div class="loadingAdverts_wrapper">
        <img src="_assets/images/advertLoader.gif" alt="Loading adverts..." class="">
    </div>
    -->
    <!-- loadingAdverts_wrapper_order -->
    <?php
        } // adverts are available
        else
        {
            echo "<p class='text-center text-info'>Info: No Adverts Currently Available!</p>";
        } // else
    
    } // end



    /**
     * load more adverts in advertList for ordered / sorted view; this method was originally being used for infinite scroll effect
     * 
     * @param int $advertLastOrderDate
     * @param int $dataOrderValue
     */
    function _loadMoreAdvertsOrderedMethod($advertLastOrderDate, $dataOrderValue)
    {
        $equate = $dataOrderValue == 'asc' ? '>' : '<';

        // advert_post_id < :advert_post_id and 


        $l_q3 = $this->conn->prepare("select advert_post_id, business_profile_id, advert_title, date_post, start_period, primary_banner_1, loggedEmail, description from advert_posts WHERE start_period ".$equate." :start_period and status = :status and approved = :approved and publish = :publish order by start_period ".$dataOrderValue." limit 3");
        $l_q3->execute([
            "start_period" => $advertLastOrderDate,
            "status" => 1,
            "approved" => "yes",
            "publish" => "yes"
        ]);
        $l3 = $l_q3->fetchAll();

        foreach($l3 as $row)
        {

            // --- >> change date format
            $advertpostedObj = new DateTime($row['start_period']);
            $advertposted = $advertpostedObj->format('d M, Y');

            // --- >> banner
            $bannerAvailable = $row['primary_banner_1'] != NULL ? $row['primary_banner_1'] : "pic_blank.jpg";
            $uploadDir = '_assets/clientUploads/'.$bannerAvailable;

            // --- >> get views and likes method
            $advert_views = json_decode($this->_ret_Views_and_Likes_for_advertList($row['advert_post_id']));

            // --- >> was advert liked : boolean return
            $advertLikedOrNot = $this->_ret_if_advert_was_liked($row['advert_post_id']);

    ?>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 adverts_filter_scroll_wrapper" id="<?php echo $row['start_period']; ?>">
        
        <div class="adverts_panel_container">
            <div class="data-jplist-item-name">
            
            <div class="advertPolaroid_container">
                <div class="advertPolaroid_title">
                <h4 class="">
                    <?php echo $row['advert_title']; ?>
                </h4>
                </div>

                <div class="advert_view_list advert-view-first">

                <img src="<?php echo $uploadDir; ?>" alt="banner" style="width: 100%; height: 230px" class="adert_banner_image">

                <div class="advert_mask">
                    <p></p>
                    <div class="overflow_tools overflow-tools-bottom">
                    <a href="#" class="btn btn-xs btn-round bg-red">
                        <span class="badge"><?php echo $advert_views->likesObj ? $advert_views->likesObj : 0; ?></span> Likes                                     
                    </a>
                    <a href="#" class="btn btn-xs btn-round bg-blue">
                        <span class="badge"><?php echo $advert_views->viewsObj ? $advert_views->viewsObj : 0; ?></span> Views
                    </a>
                    </div>
                </div>
                <!-- /. advert_mask -->
                
                </div>
                <!-- /. advert_view_list , advert-view-first -->

                <div class="advert_description_wrapper">
                    <p class="advert_description">
                    <?php  echo strlen(trim(htmlspecialchars_decode($row['description']), " ")) > 70 ? substr(trim(htmlspecialchars_decode($row['description']), " "), 0, 100).'...' : trim(htmlspecialchars_decode($row['description']), " ");?> <a href="#dataModalAdverts" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id'];?>')" class="text-maroon read_advert_details">read more</a>                                 
                    </p>

                    <div class="row">
                    <div class="col-xs-6">
                        <p class="text-warning">
                        <small><?php echo "Posted: ". $advertposted; ?></small>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="text-right text-navy">
                        <a  href="javascript:void(0)" title="Like" id="advertLikeLink<?php echo $row['advert_post_id'];?>" onclick="likethisAdvertfunction('<?php echo $row['advert_post_id'];?>', '<?php echo $_SESSION["ur_email"]; ?>')">
                            <?php echo $advertLikedOrNot == true ? '<i class="fa fa-thumbs-up text-info"></i>' : '<i class="fa fa-thumbs-o-up text-info"></i>'; ?>
                        </a>
                        &nbsp;
                        <a href="javascript:void(0)" title="Favorite" id="advertFavLink<?php echo $row['advert_post_id'];?>" onclick="favoriteAdvertfunction('<?php echo $row['advert_post_id'];?>')">
                            <?php echo $advertFavoriteOrNot == true ? '<i class="fa fa-heart text-maroon"></i>' : '<i class="fa fa-heart-o text-maroon"></i>'; ?>
                        </p>
                    </div>                                    
                    </div>
                </div>
                <!-- /. advert_description -->

                </div>
                <!-- /.advertPolaroid_container -->

            </div>
            <!-- /. data-jplist-item-name -->
        </div>
        <!-- /. adverts_panel_container -->
    </div> 
    <!-- /. col-md-4 -->
    <?php
        } // while

    } // end


    /**
     * handles adverts search fiter using autocomplete jQuery plugin
     * 
     * @param string $advertTerm the term being searched
     * 
     * @return void
     * @access public
     */
    function _returnAdvertsJson($advertTerm)
    {
        $l_q3 = $this->conn->prepare("select advert_post_id, advert_title from advert_posts WHERE advert_title like '%".$advertTerm."%' and status = :status and approved = :approved and publish = :publish");
        $l_q3->execute([
            "status" => 1,
            "approved" => "yes",
            "publish" => "yes"
        ]);
        $reply                  = array();
        $reply["query"]         = $advertTerm;
        $reply["suggestions"]   = array();

        while ( $row = $l_q3->fetch(PDO::FETCH_ASSOC) ) 
        {    
            $reply["suggestions"][]   = array('key' => $row['advert_post_id'], 'value' => htmlentities(stripslashes($row['advert_title'])));
        }
        echo json_encode($reply);
    } // end


    /**
     * add or remove advert to / from favorites
     * 
     * @param int $advertIdToAddToFav the advert we intend to remove or add to favorites
     * 
     * @return void
     */
    function _addingAdvertTofavorites($advertIdToAddToFav)
    {
        // check if current user has already added job to favorites
        $check_if_added_to_favorities_q = $this->conn->prepare("select fav_status from favorites where advert_post_id = :advert_post_id and email_address = :email_address");
        $check_if_added_to_favorities_q->execute([
            "advert_post_id" => $advertIdToAddToFav,
            "email_address" => $_SESSION['ur_email']
        ]);
        $check_if_added_to_favorities = $check_if_added_to_favorities_q->fetchObject();

        // if true; already in favorites; so remove from favorites; just delete
        if ($check_if_added_to_favorities->fav_status == 1) 
        {
            $rem_from_favorites_q = $this->conn->prepare("delete from favorites where email_address = :email_address and advert_post_id = :advert_post_id");
            $rem_from_favorites = $rem_from_favorites_q->execute([
                "advert_post_id" => $advertIdToAddToFav,
                "email_address" => $_SESSION['ur_email']
            ]);
            
            if ($rem_from_favorites) 
            {
            echo 1;
            }
            else
            {
                echo 2;
            }
        }
        // if false; insert as new favorite
        else
        {
            $add_to_favorites_q = $this->conn->prepare("insert into favorites(advert_post_id, email_address, fav_status) values (:advert_post_id, :email_address, :fav_status)");
            $add_to_favorites = $add_to_favorites_q->execute([
                "advert_post_id" => $advertIdToAddToFav,
                "email_address" => $_SESSION['ur_email'],
                "fav_status" => 1
            ]);
            
            if ($add_to_favorites) 
            {
            echo 3;
            }
            else
            {
                echo 4;
            }
        } // else

    } // end



/* --------------------------------------------------- */
/* Profile
/* --------------------------------------------------- */


    /**
     * changes profile image
     * 
     * @param $profileImageValue image name
     * @param $profileImageSize image size
     * @param $profileImagetmpName tmp
     * 
     * @return void
     * @access public
     */
    function clientChangingProfileImage($profileImageValue, $profileImageSize, $profileImagetmpName) {

        // get old profile pic
        $g_q1 = $this->conn->prepare("select profile_picture from user_profile where ur_email = :ur_email");
        $g_q1->execute([
            "ur_email" => $_SESSION['ur_email']
        ]);
        $g1 = $g_q1->fetchObject();

        // get image dimension
        $clientPhotoDimensions = @getimagesize($profileImagetmpName);
        $imgClientHeight = $clientPhotoDimensions[0];
        $imgClientWidth  = $clientPhotoDimensions[1];

        // get image path; extensions
        $target_dir = "././_assets/clientUploads/profile/";
        $target_file = $target_dir . basename($profileImageValue);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        /*
        * validation starts here
        *
        */
        // Check if file already exists
        if (file_exists($target_file)) {
            echo 3;
        }
        // Check file size
        else if ($profileImageSize > 500000) {
            echo 2;
        }
        // Allow certain file formats
        else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            echo 1;
        }
        else {

            // check image dimensions
            if ($imgClientWidth == $this->maxWidth && $imgClientHeight == $this->maxHeight) {
            
                $u_q3 = $this->conn->prepare("update user_profile set profile_picture = :profile_picture where ur_email = :ur_email");
                $u3 = $u_q3->execute([
                    "profile_picture" => $profileImageValue,
                    "ur_email" => $_SESSION['ur_email']
                ]);

                if ($u3) {

                    echo 6; // success
                    move_uploaded_file($profileImagetmpName, $target_file); // move new image to upload dir

                     // we don't want to delete the default 'no image' pic
                    if($g1->profile_picture != 'nophoto.jpg') {
                        unlink('././_assets/clientUploads/profile/'.$g1->profile_picture);
                    }

                } else {
                    echo 4;
                }
            } // if dimensions is ok
            else {
                echo 5;
            } // else wrong dimensions

        } // else validation      

    } //


    /**
     * editing form for client profile
     *
     * @return void
     * @access public
     */
    function clientEditingform() {

        $userProfile = json_decode($this->_userProfile());

        ?>
        <form role="form" id="UpdatingProfileFormId" enctype="multipart/form-data">

            <div class="row">

                <div class="col-xs-12 col-md-6 col-sm-12">

                    <div class="form-group">
                      <label for="editing_fullnames">Full Names</label>
                      <input type="text" class="form-control" value="<?php echo $userProfile->full_names; ?>" autocomplete="off" id="editing_fullnames" name="editing_fullnames" placeholder="your full names" required>
                    </div>
                    
                    <div class="form-group">
                      <label for="editing_gender">Gender</label>
                      <div id="editing_gender" class="py-7">
                        <label class="radio-inline"><input type="radio" value="Male" name="editing_gender">Male</label>
                        <label class="radio-inline"><input type="radio" value="Female" name="editing_gender" required>Female</label>
                      </div>
                    </div>
                        
                    <div class="form-group">
                      <label for="editing_profession">Profession</label>
                      <input type="text" class="form-control" value="<?php echo $userProfile->profession; ?>" autocomplete="off" id="editing_profession" name="editing_profession" placeholder="your profession" required>
                    </div>
                    
                </div>
                <!-- left -->

                <div class="col-xs-12 col-md-6 col-sm-12">
                    <div class="form-group">
                      <label for="editing_nationality">Nationality</label>
                      <input name="editing_nationality" id="editing_nationality" class="form-control" value="<?php echo $userProfile->nationality; ?>" autocomplete="off" required>
                    </div>
                    
                    <div class="form-group">
                      <label for="editing_currentLocation">Current Location</label>
                      <input type="text" name="editing_currentLocation" id="editing_currentLocation" class="form-control" value="<?php echo $userProfile->current_address; ?>" autocomplete="off" required placeholder="your current location">
                    </div>

                    <div class="form-group">
                      <label for="editing_phoneNumber">Phone Number</label>
                      <input type="text" class="form-control" value="<?php echo $userProfile->phone_number; ?>" autocomplete="off" name="editing_phoneNumber" id="editing_phoneNumber" placeholder="your phone number" required>
                    </div>                    
                </div>
                <!-- right -->

            </div>
            <!-- row -->

            <div class="form-group">  
                <button type="submit" class="btn btn-flat c_btnSix btn-md" id="updateProfileBtn">Done</button>
            </div>

        </form>
        <script>
            $(function(){
                // update profile
                $("#UpdatingProfileFormId").on("submit", function(e)
                {
                  e.preventDefault();

                $("#updateProfileBtn").prop("disabled", true);
                $("#updateProfileBtn").html("Please wait...")
                  $.ajax({
                    url: "_server_requests.php",
                    method: "post", 
                    data: new FormData(this),
                    contentType: false, 
                    cache: false,        
                    processData:false, 
                    success: function(data){
                      bootbox.alert(data)
                      $("#updateProfileBtn").prop("disabled", false);
                      $("#updateProfileBtn").html("Done")
                    }
                  });
                });
            });
        </script>
        <?php

    } // end


    /**
     * handles currently logged in user information for profile section
     *
     * @return object array if current session is ok;
     * @access public
     */
    function _userProfile()
    {
        $p_q4 = $this->conn->prepare("select * from ur_login where ur_email = :ur_email");
        $p_q4->execute([
            "ur_email" => $_SESSION['ur_email']
        ]);
        $p4 = $p_q4->fetchObject();

        if ($p4) 
        {
            $p_q5 = $this->conn->prepare("select * from user_profile where ur_email = :ur_email");
            $p_q5->execute([
                "ur_email" => $_SESSION['ur_email']
            ]);
            $p5 = $p_q5->fetchObject();

            $userProfilePic = $p5->profile_picture ? $p5->profile_picture : 'nophoto.jpg';

        return json_encode(array(
                "full_names" => $p5->names,
                "phone_number" => $p5->phone_no,
                "email_address" => $p4->ur_email,
                "gender" => $p5->gender,
                "date_of_birth" => $p5->date_of_birth,
                "nationality" => $p5->nationality,
                "current_address" => $p5->current_address,
                "profession" => $p5->profession,
                "profile_picture" => trim($userProfilePic)
        ));
        }
        else
        {
            echo "<p>".PROFILE_LOADING_ERROR."</p>";
        }
    } // end

    /**
     * fetches profile information of the current logged in session of client
     * 
     * @param string $currentSessionEmailAddress the current session email address
     */
    function _queryProfileOfActiveSession($currentSessionEmailAddress)
    {
        $p_q4 = $this->conn->prepare("select * from ur_login where ur_email = :ur_email");
        $p_q4->execute([
            "ur_email" => $currentSessionEmailAddress
        ]);
        $p4 = $p_q4->fetchObject();

        if ($p4) 
        {
            $p_q5 = $this->conn->prepare("select * from user_profile where ur_email = :ur_email");
            $p_q5->execute([
                "ur_email" => $currentSessionEmailAddress
            ]);
            $p5 = $p_q5->fetchObject();

            $userProfilePic = $p5->profile_picture ? $p5->profile_picture : 'nophoto.jpg';

        echo json_encode(array(
                "full_names" => $p5->names,
                "phone_number" => $p5->phone_no,
                "email_address" => $p4->ur_email,
                "gender" => $p5->gender,
                "date_of_birth" => $p5->date_of_birth,
                "nationality" => $p5->nationality,
                "current_address" => $p5->current_address,
                "profession" => $p5->profession,
                "profile_picture" => trim($userProfilePic)
        ));
        }
        else
        {
            echo "<p>".PROFILE_LOADING_ERROR."</p>";
        }
    } // end


    /**
     * deals with basic profile update
     * 
     * @param string $editing_fullnames the new full names
     * @param string $editing_gender the new gender
     * @param string $editing_profession the new profession
     * @param string $editing_nationality nationality
     * @param string $editing_currentLocation current location
     * @param string $editing_phoneNumber phone number
     * @param string $profile_pic_name profile picture name
     * @param string $profile_pic_size profile picture size
     * @param string $profile_pic_tmp profile picture temp name
     * 
     * @return void
     * @access public 
     */
    function updateBasicProfile($editing_fullnames,$editing_gender,$editing_profession,$editing_nationality,$editing_currentLocation,$editing_phoneNumber)
    {
        if (empty($editing_fullnames)) 
        {
        $this->errs[] = ENTER_YOUR_FULL_NAMES;
        }
        elseif (empty($editing_gender)) 
        {
        $this->errs[] = PLEASE_SELECT_GENDER;
        }
        elseif (empty($editing_profession)) 
        {
        $this->errs[] = PLEASE_ENTER_PROFESSION;
        }
        elseif (empty($editing_nationality)) 
        {
        $this->errs[] = PLEASE_SELECT_NATIONALITY;
        }
        elseif (empty($editing_currentLocation)) 
        {
        $this->errs[] = EMPTY_CURRENT_LOCATION;
        }
        elseif (empty($editing_phoneNumber)) 
        {
        $this->errs[] = PLEASE_ENTER_TEL;
        }
        else
        {
            /*
            if (!empty($profile_pic_name))
            {
                $target_dir = "././_assets/clientUploads/profile/";
                $target_file = $target_dir . basename($profile_pic_name);
                
                if($this->_uploadProfile($profile_pic_name,$profile_pic_size))
                {
                    if ($this->_profileBasicUpdate($editing_fullnames, $editing_gender, $editing_profession, $profile_pic_name)) 
                    {

                        move_uploaded_file($profile_pic_tmp, $target_file);
                        $this->mgss[] = PROFILE_UPDATED_SUCCESSFULLY;

                    }
                    else
                    {
                        $this->errs[] = FAILED_TO_UPDATE_PROFILE;
                    }

                } // _uploadProfile

            } // picture was selected
            else
            {
            */
                if ($this->_profileBasicUpdate($editing_fullnames,$editing_gender,$editing_profession,$editing_nationality,$editing_currentLocation,$editing_phoneNumber)) 
                {
                    $this->mgss[] = PROFILE_UPDATED_SUCCESSFULLY;
                }
                else
                {
                    $this->errs[] = FAILED_TO_UPDATE_PROFILE;
                }
            //} // picture was not selected
        
        } // validation

    } // end




    /**
     * handles the client profile update; refer to "_updateBasicProfile(...)'
     * 
     * @param string $editing_fullnames the new full names
     * @param string $editing_gender the new gender
     * @param string $editing_profession the new profession
     * @param string $editing_nationality nationality
     * @param string $editing_currentLocation current location
     * @param string $editing_phoneNumber phone number
     * @param string $profile_pic_name profile picture name
     * 
     * @return bool TRUE if profile was successfully updated; FALSE if profile update failed
     * @access private
     */
    private function _profileBasicUpdate($editing_fullnames,$editing_gender,$editing_profession,$editing_nationality,$editing_currentLocation,$editing_phoneNumber)
    {


        $u_q1 = $this->conn->prepare("update user_profile set 
            names = :names,
            gender = :gender,
            profession = :profession
        where ur_email = :ur_email");

        $u1 = $u_q1->execute([
            "names" => trim($editing_fullnames),
            "gender" => $editing_gender,
            "profession" => trim($editing_profession),
            "ur_email" => $_SESSION['ur_email']
        ]);

        $u_q2 = $this->conn->prepare("update user_profile set 
            phone_no = :phone_no,
            nationality = :nationality,
            current_address = :current_address
        where ur_email = :ur_email");

        $u2 = $u_q2->execute([
            "phone_no" => trim($editing_phoneNumber),
            "nationality" => trim($editing_nationality),
            "current_address" => trim($editing_currentLocation),
            "ur_email" => $_SESSION['ur_email']
        ]);

        if ($u1 && $u2) 
        {
            return true;
        }
        else
        {
            return false;
        }

        /*
        $check_q = $this->conn->prepare("select profile_picture from user_profile where ur_email = :ur_email and profile_picture IS NOT NULL");
        $check_q->execute([
            "ur_email" => $_SESSION['ur_email']
        ]);
        $check_p = $check_q->fetchObject();

        // if he/she already has a profile picture
        if($check_p)
        {
            // is he/she trying to update
            if (!empty($profile_pic_name)) 
            {
                $u_q3 = $this->conn->prepare("update user_profile set 
                    names = :names,
                    gender = :gender,
                    profession = :profession,
                    profile_picture = :profile_picture
                where ur_email = :ur_email");

                $u3 = $u_q3->execute([
                    "names" => $editing_fullnames,
                    "gender" => $editing_gender,
                    "profession" => $editing_profession,
                    "profile_picture" => $profile_pic_name,
                    "ur_email" => $_SESSION['ur_email']
                ]);

                if ($u3) 
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {

                $u_q3 = $this->conn->prepare("update user_profile set 
                    names = :names,
                    gender = :gender,
                    profession = :profession
                where ur_email = :ur_email");

                $u3 = $u_q3->execute([
                    "names" => $editing_fullnames,
                    "gender" => $editing_gender,
                    "profession" => $editing_profession,
                    "ur_email" => $_SESSION['ur_email']
                ]);

                if ($u3) 
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        // he/she has no profile picture
        else
        {
            // is he/she inserting new picture
            if (!empty($profile_pic_name)) 
            {
                $u_q3 = $this->conn->prepare("update user_profile set 
                    names = :names,
                    gender = :gender,
                    profession = :profession,
                    profile_picture = :profile_picture
                where ur_email = :ur_email");

                $u3 = $u_q3->execute([
                    "names" => $editing_fullnames,
                    "gender" => $editing_gender,
                    "profession" => $editing_profession,
                    "profile_picture" => $profile_pic_name,
                    "ur_email" => $_SESSION['ur_email']
                ]);

                if ($u3) 
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $u_q3 = $this->conn->prepare("update user_profile set 
                    names = :names,
                    gender = :gender,
                    profession = :profession
                where ur_email = :ur_email");

                $u3 = $u_q3->execute([
                    "names" => $editing_fullnames,
                    "gender" => $editing_gender,
                    "profession" => $editing_profession,
                    "ur_email" => $_SESSION['ur_email']
                ]);

                if ($u3) 
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        */

    } // end



    /**
     * validate client profile picture; called from "_updateBasicProfile(...)"
     * 
     * @param string $profile_pic_name profile picture name
     * @param string $profile_pic_size profile picture size
     * 
     * @return bool TRUE if validations is successful; FALSE if validation fails
     */
    function _uploadProfile($profile_pic_name,$profile_pic_size)
    {
        $target_dir = "././_assets/clientUploads/profile/";
        $target_file = $target_dir . basename($profile_pic_name);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        // Check if file already exists
        if (file_exists($target_file)) 
        {
            $this->errs[] =  FILE_ALREADY_EXISTS;
            return false;
        }
        // Check file size
        else if ($profile_pic_size > 500000) 
        {
            $this->errs[] = PROFILE_MAX_SIZE;
            return false;
        }
        // Allow certain file formats
        else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") 
        {
            $this->errs[] = PROFILE_INVALID_FORMAT;
            return false;
        }
        else
        {
            return true;
        }
    } // end

    /**
     * handles client basic location information update
     * 
     * @param string $editing_nationality client new nationality
     * @param string $editing_currentLocation client new location
     * @param string $editing_phoneNumber client phone number
     * 
     * @return void
     */
    function _updateLocationProfile($editing_nationality,$editing_currentLocation,$editing_phoneNumber)
    {
        if (empty($editing_nationality)) 
        {
        $this->errs[] = PLEASE_SELECT_NATIONALITY;
        }
        elseif (empty($editing_currentLocation)) 
        {
        $this->errs[] = EMPTY_CURRENT_LOCATION;
        }
        elseif (empty($editing_phoneNumber)) 
        {
        $this->errs[] = PLEASE_ENTER_TEL;
        }
        elseif (strlen($editing_phoneNumber) > 13) 
        {
        $this->errs[] = LONG_PHONE_NUMBER;
        }
        elseif (strlen($editing_phoneNumber) < 10) 
        {
        $this->errs[] = SHORT_PHONE_NUMBER;
        }
        else
        {
            if ($this->_profileLocationUpdate($editing_nationality, $editing_currentLocation, $editing_phoneNumber)) 
            {
                $this->mgss[] = PROFILE_UPDATED_SUCCESSFULLY;
            }
            else
            {
                $this->errs[] = FAILED_TO_UPDATE_PROFILE;
            }
        }

    } // end


    /**
     * handles the client location update; refe to "_updateLocationProfile(...)"
     * 
     * @param string $editing_nationality client new nationality
     * @param string $editing_currentLocation client new location
     * @param string $editing_phoneNumber client phone number
     * 
     * @return bool TRUE if location update was succssful; FALSE if it failed
     * @access private
     */
    private function _profileLocationUpdate($editing_nationality, $editing_currentLocation, $editing_phoneNumber)
    {
        $u_q3 = $this->conn->prepare("update user_profile set 
            phone_no = :phone_no,
            nationality = :nationality,
            current_address = :current_address
        where ur_email = :ur_email");

        $u3 = $u_q3->execute([
            "phone_no" => $editing_phoneNumber,
            "nationality" => $editing_nationality,
            "current_address" => $editing_currentLocation,
            "ur_email" => $_SESSION['ur_email']
        ]);

        if ($u3) 
        {
            return true;
        }
        else
        {
            return false;
        }
    } // end

    /**
     * update current password method; refer to call in "_updatecurrentPassword(...)"
     * 
     * @param string $dataInputEmailSession current email address associated with password
     * @param string $new_password_hash new password
     * 
     * @return bool TRUE if current password was successfully updated; FALSE if current password failed
     * @access private
     */
    private function _updatePassword($dataInputEmailSession, $new_password_hash)
    {
        $update_current_password_q = $this->conn->prepare("update ur_login set ur_pas = :ur_pas where ur_email = :ur_email");
        $update_current_password = $update_current_password_q->execute([
            "ur_pas" => $new_password_hash,
            "ur_email" => $dataInputEmailSession
        ]); 
        if ($update_current_password) 
        {
            return true;
        }
        else
        {
            return false;
        }
    } // end

    /**
     * email validation whether its registered within the system
     * 
     * @param string $dataInputEmailSession the email address which needs to be checked
     * 
     * @return bool TRUE if the email is regisered; FALSE if its not
     * @access private
     */
    private function _checkEmailSession($dataInputEmailSession)
    {
        $check_current_password_q = $this->conn->prepare($this->_ur_login_table()." where ur_email = :ur_email");
        $check_current_password_q->execute([
        "ur_email" => $dataInputEmailSession
        ]);
        $check_current_password = $check_current_password_q->fetchObject();
        if ($check_current_password) 
        {
            return true;
        }
        else
        {
            return false;
        }
    } // end

    /**
     * Main password change method. this is done when the client is currently logged in
     * 
     * @param string $dataCurrentPassword current password
     * @param string $dataNewPassword new password
     * @param string $dataConfirmNewPassword confirming the new password
     * @param string $dataInputEmailSession current email address associated with password
     * 
     */
    function _updatecurrentPassword($dataCurrentPassword, $dataNewPassword, $dataConfirmNewPassword, $dataInputEmailSession)
    {
        if (empty($dataCurrentPassword)) 
        {
        $this->errs[] = EMPTY_CURRENT_PASSWORD;
        }
        else if (empty($dataNewPassword)) 
        {
            $this->errs[] = EMPTY_NEW_PASSWORD;
        }
        else if (empty($dataConfirmNewPassword)) 
        {
            $this->errs[] = EMPTY_CONFIRM_PASSWORD;
        }
        else if (strlen($dataCurrentPassword) < 8 || strlen($dataNewPassword) < 8 || strlen($dataConfirmNewPassword) < 8)
        {
            $this->errs[] = MIN_PASSWORD_LENGTH;
        }
        else if ($dataNewPassword != $dataConfirmNewPassword) 
        {
        $this->errs[] = PASSWORDS_ARE_NOT_MATCHING;
        }
        else
        {
            // --- confirm current password
            $check_current_password_q = $this->conn->prepare($this->_ur_login_table()." where ur_email = :ur_email");
            $check_current_password_q->execute([
                "ur_email" => $dataInputEmailSession
            ]);
            $check_current_password = $check_current_password_q->fetchObject();

            if ($check_current_password) 
            {
                if (!password_verify($dataCurrentPassword, $check_current_password->ur_pas)) 
                {
                $this->errs[] = INVALID_CURRENT_PASSWORD;
                }
                else
                {
                    $new_password_hash = password_hash($dataNewPassword, PASSWORD_DEFAULT);

                    // call updating password method
                    if ($this->_updatePassword($dataInputEmailSession, $new_password_hash)) 
                    {
    ?>
    <!--<meta http-equiv="refresh" content="10;url=dashboard.php?logout" />-->
    <script>
        var logoutPage = "dashboard.php?logout";
        var timerS = 7;

        function logoutFunction(){
            var elementId = document.getElementById("logout-span-timer");

            if(timerS > 0){
                timerS--;
                elementId.innerHTML = timerS+ "s";
                setTimeout("logoutFunction()", 1000);
            }
            else {                                      
                window.location.href = logoutPage;
            }
        }
    </script>
    <?php
                    $this->mgss[] = PASSSWORD_SUCCESSFUL_UPDATED . "<br> If you are not automatically logged out in <span id='logout-span-timer'><script>logoutFunction();</script></span>, click <a href='dashboard.php?logout' class='text-underline text-maroon'>here</a>";

                    }
                    else
                    {
                        $this->errs[] = FAILED_TO_UPDATE_PASSWORD;
                    }

                } // else ; right current password
            
            } // user exists
            else
            {
                $this->errs[] = INTERNAL_ERROR;
            } // else user info fetching error

        } // else

    } // end


    /**
     * handles the deletion of the client account
     * 
     * @param string $dataConfirmAgreement has the agreement been checked
     * @param string $confirmDeletionPassword the client password
     * @param string $dataDeleteEmailSession client email address associated with the account
     * 
     * @return void
     */
    function _deleteUserAccount($dataConfirmAgreement,$confirmDeletionPassword,$dataDeleteEmailSession)
    {
        if (empty($dataConfirmAgreement)) 
        {
        $this->errs[] = ACCEPT_AGREEMENT;
        }
        else if (empty($confirmDeletionPassword)) 
        {
            $this->errs[] = EMPTY_CURRENT_PASSWORD;
        }
        else if (strlen($confirmDeletionPassword) < 8)
        {
            $this->errs[] = MIN_PASSWORD_LENGTH;
        }
        else
        {
            // --- confirm current password
            $check_current_password_q = $this->conn->prepare($this->_ur_login_table()." where ur_email = :ur_email");
            $check_current_password_q->execute([
                "ur_email" => $dataDeleteEmailSession
            ]);
            $check_current_password = $check_current_password_q->fetchObject();

            if ($check_current_password) 
            {
                if (!password_verify($confirmDeletionPassword, $check_current_password->ur_pas)) 
                {
                $this->errs[] = INVALID_CURRENT_PASSWORD;
                }
                else
                {
                    $this->mgss[] = "deletion successful";
                /**
                 // deletion
                    $d_q80 = $this->conn->prepare("delete from ur_login where ur_email = :ur_email");
                    $d80   = $d_q80->execute([
                        "ur_email" => $dataDeleteEmailSession
                    ]);
                    $d_q81 = $this->conn->prepare("delete from user_profile where ur_email = :ur_email");
                    $d80   = $d_q81->execute([
                        "ur_email" => $dataDeleteEmailSession
                    ]);
                    $d_q82 = $this->conn->prepare("delete from advert_likes where liked_by_email = :ur_email");
                    $d80   = $d_q82->execute([
                        "ur_email" => $dataDeleteEmailSession
                    ]);
                    $d_q83 = $this->conn->prepare("delete from advert_posts where loggedEmail = :ur_email");
                    $d80   = $d_q83->execute([
                        "ur_email" => $dataDeleteEmailSession
                    ]);
                    $d_q84 = $this->conn->prepare("delete from jobs where loggedin_email = :ur_email");
                    $d80   = $d_q84->execute([
                        "ur_email" => $dataDeleteEmailSession
                    ]);
                    */

                } // else ; right current password
            
            }
            else
            {
                $this->errs[] = INTERNAL_ERROR;
            } // else user info fetching error

        } // else

    } // end



    /**
     * handles loading of favorite adverts of client under his/her profile
     */
    function _favoriteadvertsList()
    {

        $get_fav_adverts_q = $this->conn->prepare("select fav_id, advert_post_id, email_address, fav_status from favorites where email_address = :email_address and fav_status = :fav_status");

        $get_fav_adverts_q->execute([
            "email_address" => $_SESSION['ur_email'],
            "fav_status" => 1
        ]);
        $get_fav_adverts = $get_fav_adverts_q->fetchAll();


        if ($get_fav_adverts) 
        {
        
        $get_adverts_list_q = $this->conn->prepare("select advert_post_id, advert_title, start_period, end_period, description, loggedEmail, primary_banner_1 from advert_posts where advert_post_id = :advert_post_id");

            // check if logged in user fav an advert
        $fetch_advert_fav_q = $this->conn->prepare("select fav_id from favorites where advert_post_id = :advert_post_id and email_address = :email_address and fav_status = :fav_status");

        $checkIfadvertWasLikedQuery = $this->conn->prepare($this->_advertLikeStats()." where liked_by_email = :liked_by_email and advert_post_id = :advert_post_id");


        foreach($get_fav_adverts as $row)
        {   

            $get_adverts_list_q->execute([
                "advert_post_id" => $row['advert_post_id']
            ]);

            $get_adverts_list = $get_adverts_list_q->fetchObject();

        if ($get_adverts_list) 
        {
            $advertpostedObj = new DateTime($get_adverts_list->date_post);
            $advertposted = $advertpostedObj->format('d M, Y');

            $bannerAvailable = $get_adverts_list->primary_banner_1 != NULL ? $get_adverts_list->primary_banner_1 : "pic_blank.jpg";

            $uploadDir = '_assets/clientUploads/'.$bannerAvailable;

            $checkIfadvertWasLikedQuery->execute([
                "liked_by_email" => $_SESSION["ur_email"],
                "advert_post_id" => $row["advert_post_id"]
            ]);
            $checkIfadvertWasLiked = $checkIfadvertWasLikedQuery->fetchObject();


            // --- >> was advert added to favorites
            $fetch_advert_fav_q->execute([
                "advert_post_id" => $row['advert_post_id'],
                "email_address" => $_SESSION['ur_email'],
                "fav_status" => 1
            ]);
            $advertFavoriteOrNot = $fetch_advert_fav_q->fetchObject();


                ?>
        <div class="col-xs-12 col-sm-4 col-md-4">
            
            <div class="fav-adverts-panels">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="text-white text-center"><?php echo $get_adverts_list->advert_title; ?></h4> 
                    </div>
                    <div class="panel-body">                
                        <p class="text-muted">
                            <?php
                            echo strlen(trim(htmlspecialchars_decode($get_adverts_list->description), " ")) > 70 ? substr(trim(htmlspecialchars_decode($get_adverts_list->description), " "), 0, 100).'...' : trim(htmlspecialchars_decode($get_adverts_list->description), " ");
                            ?>                                    
                        </p>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-5">
                                <small>Posted: <?php echo $advertposted; ?></small>
                            </div>

                            <div class="col-xs-7">

                                <ul class="adverts_fav_footer">
                                    <li>
                                        <a class="fa-2c" href="#dataModalAdverts" title="view more" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id'];?>')"><i class="fa fa-location-arrow text-olive"></i>
                                        </a>
                                    </li>
                                    <li>
                                    <a class="fa-2c" href="javascript:void(0)" title="Like" id="advertLikeLink<?php echo $row['advert_post_id'];?>" onclick="likethisAdvertfunction('<?php echo $row['advert_post_id'];?>', '<?php echo $_SESSION["ur_email"]; ?>')">
                                        <?php echo $checkIfadvertWasLiked == true ? '<i class="fa fa-thumbs-up text-info"></i>' : '<i class="fa fa-thumbs-o-up text-info"></i>'; ?>
                                    </a>
                                    </li>
                                    <li>
                                <a class="fa-2c" href="javascript:void(0)" title="Favorite" id="advertFavLink<?php echo $row['advert_post_id'];?>" onclick="favoriteAdvertfunction('<?php echo $row['advert_post_id'];?>')">
                                    <?php echo $advertFavoriteOrNot == true ? '<i class="fa fa-heart text-maroon"></i>' : '<i class="fa fa-heart-o text-maroon"></i>'; ?>
                                </a>
                                    </li>
                                </ul> 
                                
                            </div>
                        </div>               
                    </div>
                </div>
                <!-- panel -->
            </div>
            <!-- fav-adverts-panels -->
        </div>
        <!-- col-xs-12 col-sm-4 col-md-4 -->

                <?php 

                }

            }   // foreach loop
        }   
        else
        {
            echo "<p class='text-center text-info'>". NO_FAVORITES_ADVERTS_AVAILABLE ."</p>";
        } 
    } // end of _favoriteadvertsList


    /**
     * jobs created by client grid view
     * 
     * @param string $jobEmailSession current session email address
     * 
     * @return void
     */
    function sessionJobsList($jobEmailSession)
    {
        // , $changeViewInputJobs

        $get_jobs_query = $this->conn->prepare("select job_id, date_format(date_posted, '%d %M, %Y') as jobDateCreated, date_format(deadline, '%d %M, %Y') as jobDateDeadline, job_vacancy, status, approved from jobs where loggedin_email = :loggedin_email");
        $get_jobs_query->execute([
            "loggedin_email" => $jobEmailSession
        ]);   

        $jobDataArray = Array();

        $jobs_query = $get_jobs_query->fetchAll();

        if ($jobs_query) 
        { 
            ?>

        <ul class="activities-ul">
            <?php
        foreach($jobs_query as $row) 
        {              
            ?>
            <li><strong>Title:</strong> <?php echo $row['job_vacancy']; ?></li>
            <li><strong>Posted:</strong> <?php echo $row['jobDateCreated']; ?></li>
            <li><strong>Deadline:</strong> <?php echo $row['jobDateDeadline']; ?></li>
            <li class="text-capitalize"><strong>Approved:</strong> <?php echo $row['approved']; ?></li>
            <li><strong>Status:</strong> 
                <?php 
                    if($row['status'] == 1) {
                        echo  "Active";
                    } else if ($row['status'] == 2) {
                        echo "Ended";
                    } else {
                        echo "Inactive";
                    }
                ?>
            </li>
            <li>
                <a href="#dataModalJobs" data-toggle="modal" onclick="jobDetailsfunc('<?php echo $row['job_id']; ?>')" class="btn btn-primary btn-flat btn-xs">
                    View
                </a>
                <!--
                <a href="javascript:void(0)" class="btn btn-info btn-flat btn-xs" onclick="jobEditfunc('<?php // echo $row['job_id']; ?>')">
                    Edit
                </a>
                -->
                <a href="#" class="btn btn-danger btn-flat btn-xs" onclick="deletejobFunction('<?php echo $row['job_id']; ?>')">
                    Delete
                </a>
            </li>

            <li class="list-divider"></li>
            <?php
        } // loop
        ?>
        </ul>
        <?php         

        } // check query
        else
        {
            echo "<h5 class='text-info text-center'>".NO_JOBS_CREATED_YET."</h5>";
        }    

    } // end


    /**
     * jobs created by client in grid view
     *
     * @param string $jobEmailSession current session email address
     * @return void
     * @access public
     */
    function sessionJobsGrid($jobEmailSession) {

        $get_jobs_query = $this->conn->prepare("select job_id, date_format(date_posted, '%d %M, %Y') as jobDateCreated, date_format(deadline, '%d %M, %Y') as jobDateDeadline, job_vacancy, status, approved from jobs where loggedin_email = :loggedin_email");
        $get_jobs_query->execute([
            "loggedin_email" => $jobEmailSession
        ]);   

        $jobDataArray = Array();

        $jobs_query = $get_jobs_query->fetchAll();

        if ($jobs_query) 
        {  
            ?>
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
            <?php
        foreach($jobs_query as $row) 
        {              
            ?>
            <div class="col-sm-3 col-xs-3 col-md-4">   
                <p><strong>Title:</strong> <?php echo $row['job_vacancy']; ?></p>
                <p><strong>Posted:</strong> <?php echo $row['jobDateCreated']; ?></p>
                <p><strong>Deadline:</strong> <?php echo $row['jobDateDeadline']; ?></p>
                <p class="text-capitalize"><strong>Approved:</strong> <?php echo $row['approved']; ?></p>
                <p><strong>Status:</strong> 
                    <?php 
                        if($row['status'] == 1) {
                            echo  "Active";
                        } else if ($row['status'] == 2) {
                            echo "Ended";
                        } else {
                            echo "Inactive";
                        }
                    ?>
                </p>
                <p>
                    <a href="#dataModalJobs" data-toggle="modal" onclick="jobDetailsfunc('<?php echo $row['job_id']; ?>')" class="btn btn-primary btn-flat btn-xs">View</a>

                <!--
                    <a href="javascript:void(0)" class="btn btn-info btn-flat btn-xs" onclick="jobEditfunc('<?php // echo $row['job_id']; ?>')">
                        Edit
                    </a>
                -->
                    <a href="javascript:void(0)" class="btn btn-danger btn-flat btn-xs" onclick="deletejobFunction('<?php echo $row['job_id']; ?>')">
                        Delete
                    </a>
                </p>
                <p class="list-divider"></p>
            </div> 
            <?php
        } // loop
        ?>
    </div>
</div>
        <?php
        } // check query
        else
        {
            echo "<h5 class='text-info text-center'>".NO_JOBS_CREATED_YET."</h5>";
        }

    } // end

    /**
     * displays the edit jobs form
     * 
     * @param int $sessionEditJobArgs the job id whose details is loaded in the form for editing
     * 
     * @return void
     */
    function _sessionEditJobs($sessionEditJobArgs)
    {
        // 1
        $jobs_edit_query1 = $this->conn->prepare("select * from jobs where job_id = :job_id");
        $jobs_edit_query1->execute([
            "job_id" => $sessionEditJobArgs
        ]); 
        $jobs_edit1 = $jobs_edit_query1->fetchObject();

        // 2
        $jobs_edit_query2 = $this->conn->prepare("select job_resp_id, job_id, responsibility from job_responsibility where job_id = :job_id");
        $jobs_edit_query2->execute([
            "job_id" => $sessionEditJobArgs
        ]); 
        $jobs_edit2 = $jobs_edit_query2->fetchAll();

    if ($jobs_edit1)
    {
        ?>
    <form id="dataForm_editJob" role="form" class="mdb-color-text">

        <!-- hidden elements -->
        <input type="hidden" value="<?php echo $sessionEditJobArgs; ?>" name="dataEdit_job_id">
        <input type="hidden" id="deleteJobSessionInputEdit<?php echo $sessionEditJobArgs; ?>" value="<?php echo $sessionEditJobArgs; ?>" class="deleteSessionInputEditClass">
    
        <!-- visible elements-->
        <div class="form-group row">
            <div class="col-xs-12">
            <label for="dataEdit_job_do_you_have_a_business">Do you have a business?</label>
                <div id="dataEdit_job_do_you_have_a_business">
                <label class="radio-inline"><input type="radio" class="dataEdit_job_have_a_businessClass" name="dataEdit_job_have_a_business" value="yes">Yes</label>
                <label class="radio-inline"><input type="radio" class="dataEdit_job_have_a_businessClass" name="dataEdit_job_have_a_business" value="no" required>No</label>                      
                </div>
            </div>
        </div>

        <div class="form-group row item_display_none" id="business_name_toggle">
            <div class="col-xs-12">
            <label for="dataEdit_job_businessname">Business Name</label>
                <select name="dataEdit_job_businessname" id="dataEdit_job_businessname" class="form-control" style="width: 100%" data-placeholder="--- select business name ---">
                <option></option>
                <?php foreach( $this->_businessnames() as $row ) { ?>
                    <option value="<?php echo $row['business_profile_id']; ?>"><?php echo $row['Business_Name']; ?></option>
                <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_title">Job Title</label>
                <input type="text" id="dataEdit_job_title" class="form-control" name="dataEdit_job_title" placeholder="enter job title" maxlength="70" autocomplete="off" value="<?php echo $jobs_edit1->job_vacancy; ?>" required>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_district">Job District</label>
                <select name="dataEdit_job_district" id="dataEdit_job_district" class="form-control" style="width: 100%" data-placeholder="--- select district ---">
                <option></option>
                <?php foreach( $this->_districts() as $row ) { ?>
                    <option><?php echo $row['district_name']; ?></option>
                <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_location">Job Location / Duty Station</label>
                <input type="text" id="dataEdit_job_location" class="form-control" name="dataEdit_job_location" placeholder="enter job location" maxlength="100" autocomplete="off" value="<?php echo $jobs_edit1->location; ?>" required>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
            <label for="dataEdit_job_type">Job Type</label>
            <select id="dataEdit_job_type" name="dataEdit_job_type" class="form-control" required>
                <option selected disabled> --- select job type --- </option> 
                <option>Full time</option>   
                <option>Part time</option>
                <option>Contract</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_salary">Job Salary</label>
                <input type="text" id="dataEdit_job_salary" class="form-control" name="dataEdit_job_salary" placeholder="enter job salary" maxlength="100" autocomplete="off" aria-describedby="jobsalaryHelp" value="<?php echo $jobs_edit1->salary; ?>" required>
                <small id="jobsalaryHelp" class="text-maroon">eg. Confidential or 150000 etc</small>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_posted_on">To be posted on</label>
                <input type="text" id="dataEdit_job_posted_on" class="form-control datepicker3" name="dataEdit_job_posted_on" placeholder="select post date" autocomplete="off" value="<?php echo $jobs_edit1->date_posted; ?>" required>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_deadline">Deadline</label>
                <input type="text" id="dataEdit_job_deadline" class="form-control datepicker3" name="dataEdit_job_deadline" placeholder="select deadline" autocomplete="off" value="<?php echo $jobs_edit1->deadline; ?>" required>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_duration">Duration (Optional)</label>
                <input type="text" id="dataEdit_job_duration" class="form-control" name="dataEdit_job_duration" placeholder="enter duration" autocomplete="off" aria-describedby='helpDuration' value="<?php echo $jobs_edit1->duration; ?>">
                <small id="helpDuration" class="text-warning">eg. 3 months, 1 week</small>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_qualification">Qualifications</label>
                <textarea id="dataEdit_job_qualification" class="form-control bp-wysihtml5" name="dataEdit_job_qualification" placeholder="enter qualifications" aria-describedby="qualificationsHelp"><?php echo trim($jobs_edit1->job_qualification); ?></textarea>
                <small id="qualificationsHelp" class="text-maroon">Please be precise. 800 characters ONLY</small>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_responsibility">Responsibilities (150 characters for each responsibility)</label>
                <?php foreach($jobs_edit2 as $row) { ?>
                <p><input type="text" class="form-control" name="dataEdit_job_responsibility[]" placeholder="enter responsibility" autocomplete="off" maxlength="150" value="<?php echo $row['responsibility']; ?>"> </p>

                <input type="hidden" value="<?php echo $row['job_resp_id']; ?>" name="dataEdit_job_resp_id[]">
            <?php } ?>

            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <label for="dataEdit_job_how_to_apply">How to apply</label>
                <textarea id="dataEdit_job_how_to_apply" class="form-control" name="dataEdit_job_how_to_apply" placeholder="enter how to apply" maxlength="200" aria-describedby="howtoapplyHelp"> <?php echo trim($jobs_edit1->application_mode); ?></textarea>
                <small id="howtoapplyHelp" class="text-maroon">200 characters ONLY</small>
            </div>
        </div>

        <div class="ln_solid"></div>

        <div class="form-group row">
            <div class="col-xs-12">
            <button type="submit" class="btn c_btn c_btnTwo btn-flat" id="dataBtn_updateJob">Update</button>
            </div>
        </div>

        </form>
        <script>
        $(function(){
                        // --- >> select2 for selecting businesses available
            $("#dataEdit_job_businessname").select2({
            placeholder: '--- select company name ---',
            tags: true, 
            allowClear: true
            });

            // --- >> select2 for selecting districts
            $("#dataEdit_job_district").select2({
                placeholder: '--- select district ---',
                tags: true, 
                allowClear: true        
            });

            // --- >> does user have a business 
            $(".dataEdit_job_have_a_businessClass").on('click', function() {
                var businessYesNoValue = $("input[name='dataEdit_job_have_a_business']:checked").val();
                if (businessYesNoValue == "yes") 
                {
                    $("#business_name_toggle").slideDown();
                } else {
                    $("#business_name_toggle").slideUp();
                }
            });

            // --- >> datepicker
            $('.datepicker3').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true,
                todayHighlight:true
            });
            //bootstrap WYSIHTML5 - text editor
            $(".bp-wysihtml5").wysihtml5({
                toolbar:{
                    "image": false,
                    "link": false,
                    "blockquote": false,
                    "color": true
                }
            });  

            // --- >> update
            $("#dataForm_editJob").on("submit", function(e)
            {
            e.preventDefault();

                $("#dataBtn_updateJob").prop("disabled", true);
                $("#dataBtn_updateJob").html("please wait");

            $.ajax({
                url: "_server_requests.php",
                method: "post",
                data: new FormData(this),
                contentType: false, 
                cache: false,        
                processData:false, 
                success: function(data)
                {
                bootbox.alert(data);
                    $("#dataBtn_updateJob").prop("disabled", false);
                    $("#dataBtn_updateJob").html("Update");  
                } 
                });          
            });
    });
    </script>
        <?php
        }
        else
        {

        }

    } // end

    /**
     * handles the updating of the new details concerning the job
     * 
     * @param string $job_have_a_business does the job have a business; yes or no
     * @param string $job_businessid if yes, the associated business id
     * @param string $job_title job title
     * @param string $job_district district location of the job
     * @param string $job_location the job location
     * @param string $job_type the job type
     * @param string $job_salary job salary
     * @param string $job_posted_on date the job is posted
     * @param string $job_deadline the job deadline
     * @param string $job_qualification job qualifications ; html markup
     * @param string $job_responsibility job responsibilities; array
     * @param string $job_how_to_apply how to apply for the job
     * @param string $job_duration job duration ; optional field
     * @param string $data_job_id the id of the job we need to update
     * @param string $data_job_resp_id the job_responsibilities id since the're stored in their own tbl
     * 
     * @return void
     */
    function _updateJob($job_have_a_business, $job_businessid, $job_title, $job_district, $job_location, $job_type, $job_salary, $job_posted_on, $job_deadline, $job_qualification, $job_responsibility, $job_how_to_apply, $job_duration, $data_job_id, $data_job_resp_id)
    {

        if (empty($job_have_a_business)) 
        {
            $this->errs[] = EMPTY_WHETHER_YOU_HAVE_A_BUSINESS;
        }
        elseif (empty($job_title)) 
        {
            $this->errs[] = EMPTY_JOB_TITLE;
        }
        elseif (empty($job_district)) 
        {
            $this->errs[] = EMPTY_JOB_DISTRICT;
        }
        elseif (empty($job_location)) 
        {
            $this->errs[] = EMPTY_JOB_LOCATION;
        }
        elseif (empty($job_type)) 
        {
            $this->errs[] = EMPTY_JOB_TYPE;
        }
        elseif (empty($job_salary)) 
        {
            $this->errs[] = EMPTY_JOB_SALARY;
        }
        elseif (empty($job_posted_on)) 
        {
            $this->errs[] = EMPTY_POSTED_ON;
        }
        elseif (empty($job_qualification)) 
        {
            $this->errs[] = EMPTY_QUALIFICATION;
        }
        elseif (empty($job_responsibility)) 
        {
            $this->errs[] = EMPTY_JOB_RESP;
        }
        elseif (empty($job_how_to_apply)) 
        {
            $this->errs[] = EMPTY_HOW_TO_APPLY;
        }
        else if (($job_have_a_business == "yes") && empty($job_businessid)) 
        {
            $this->errs[] = EMPTY_COMPANY_NAME;
        }
        else
        {
            $bool = false;

            // --- << update
                $u_q4 = $this->conn->prepare("update jobs set
                    have_business = :have_business,
                    business_profile_id = :business_profile_id,
                    job_district = :job_district,
                    job_vacancy = :job_vacancy,
                    location = :location,
                    job_type = :job_type,
                    job_qualification = :job_qualification,
                    salary = :salary,
                    application_mode = :application_mode,
                    date_posted = :date_posted,
                    deadline = :deadline,
                    duration = :duration where job_id = :job_id");

                $u4 = $u_q4->execute([
                    'have_business' => $job_have_a_business,
                    'business_profile_id' => $job_have_a_business == "yes" ? $job_businessid : NULL,
                    'job_district' => $job_district,
                    'job_vacancy' => $job_title,
                    'location' => $job_location,
                    'job_type' => $job_type,
                    'job_qualification' => $job_qualification,
                    'salary' => $job_salary,
                    'application_mode' => trim($job_how_to_apply),
                    'date_posted' => $job_posted_on,
                    'deadline' => !empty($job_deadline) ? $job_deadline : 'N/A',
                    'duration' => !empty($job_duration) ? $job_duration : 'N/A',
                    'job_id' => $data_job_id
                ]);


                    $u_q5 = $this->conn->prepare("update job_responsibility set
                        business_profile_id = :business_profile_id,
                        responsibility = :responsibility where job_resp_id = :job_resp_id and job_id = :job_id");

                    foreach ($data_job_resp_id as $key => $value) 
                    {
                        if(empty($job_responsibility[$key]))
                        {
                            $this->errs[] = EMPTY_JOB_RESP;
                        }
                        else{

                        $u5 = $u_q5->execute([
                            'business_profile_id' => $job_have_a_business == "yes" ? $job_businessid : NULL,
                            'responsibility' => $job_responsibility[$key],
                            'job_resp_id' => $value,
                            'job_id' => $data_job_id
                        ]);

                            if ($u5) 
                            {
                                $bool = true;
                            }
                            else
                            {
                                $bool = false;
                            }
                        }
                    } // foreach loop

                if ($u4 && $bool) 
                {
                    $this->mgss[] = JOB_INFORMATION_UPDATED;
    
                }
                else
                {
                    $this->errs[] = FAILED_JOB_UPDATE;
                }

        } // --- << else validation

    } // end


    /**
     * get current session likes
     * 
     * @param string $likesEmailSession the current session email whose likes count we need
     * 
     * @return void
     * @access public
     */
    function _sessionLikes($likesEmailSession)
    {
        $get_likes_q = $this->conn->prepare("select advert_post_id from advert_likes where liked_by_email = :session_email");
        $get_likes_q->execute([
            "session_email" => $likesEmailSession
        ]);
        $get_likes = $get_likes_q->fetchAll();


        if ($get_likes) 
        {
            $get_advert_info = $this->conn->prepare("select advert_post_id, business_profile_id, advert_title, date_format(date_post, '%d %M, %Y') as advert_created_date, loggedEmail from advert_posts where advert_post_id = :advert_post_id");


            foreach($get_likes as $row)
            {
                $get_advert_info->execute([
                    "advert_post_id" => $row['advert_post_id']
                ]);
                ?>
            <?php
            while($likes = $get_advert_info->fetch(PDO::FETCH_ASSOC))
            {         
            ?>  
        <div class="col-sm-4 col-xs-4">   
                <p><strong>Advert Title:</strong> <?php echo $likes['advert_title']; ?></p>
                <p><strong>Posted:</strong> <?php echo $likes['advert_created_date']; ?></p>
                <p>
                    <a href="#dataModalAdverts" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $likes['advert_post_id']; ?>')" class="btn btn-primary btn-flat btn-xs">View</a>

                    <a href="javascript:void(0)" id="advertLikeLink<?php echo $likes['advert_post_id']; ?>" onclick="likethisAdvertfunction2('<?php echo $likes['advert_post_id']; ?>')" class="btn btn-danger btn-flat btn-xs">Unlike</a></p>
                <p class="list-divider"></p>
        </div>
                <?php
            }
            ?>  

                <?php
            }
        }
        else
        {
            echo "<h5 class='text-info text-center hr_font_weight_600'>".NO_LIKES_YET."</h5>";
        } // else

    } // end


    /**
     * adverts created by client in list view (default)
     * 
     * @param string $advertsEmailSession the current session email whose adverts we need to fetch
     * 
     * @return void
     * @access public
     */
    function sessionCreatedAdvertsList($advertsEmailSession)
    {
        $get_adverts_q = $this->conn->prepare("select advert_post_id, business_profile_id, advert_title, 
            date_format(date_post, '%d %M, %Y') as advert_created_date,
            date_format(start_period, '%d %M, %Y') as advert_start_date,
            date_format(end_period, '%d %M, %Y') as advert_end_date,
            publish,
            status,
            approved from advert_posts where loggedEmail = :loggedEmail");
        $get_adverts_q->execute([
            "loggedEmail" => $advertsEmailSession
        ]);
        $get_adverts = $get_adverts_q->fetchAll();

        $advertsArray = array();

        if ($get_adverts) 
        {
            ?>
            <ul class="activities-ul">
            <?php
            foreach($get_adverts as $row)
            {
            ?>
                <li><strong>Title:</strong> <?php echo $row['advert_title']; ?></li>
                <li><strong>Created on:</strong> <?php echo $row['advert_created_date']; ?></li>
                <li><strong>Starting on:</strong> <?php echo $row['advert_start_date']; ?></li>
                <li><strong>Ending on:</strong> <?php echo $row['advert_end_date']; ?></li>
                <li><strong>Approved:</strong> <?php echo $row['approved']; ?></li>
                <li><strong>Status:</strong> 
                    <?php 
                        if($row['status'] == 0) {
                            echo "Inactive";
                        } else if ($row['status'] == 1) {
                            echo "Active";
                        } else {
                            echo "Ended";
                        }
                    ?>
                </li>
                <li>
                    <a href="#dataModalAdverts" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id']; ?>')" class="btn btn-primary btn-flat btn-xs">View</a>
                <!--
                    <a href="javascript:void(0)" class="btn btn-info btn-flat btn-xs" onclick="advertEditfunc('<?php // echo $row['advert_post_id']; ?>')">
                        Edit
                    </a>
                -->
                    <a href="#" id="deleteAdvertId<?php echo $row['advert_post_id']; ?>" class="btn btn-danger btn-flat btn-xs" onclick="deleteAdvertFunction('<?php echo $row['advert_post_id']; ?>')">
                        Delete
                    </a>
                </li>
                <li class="list-divider"></li>
            <?php
            }
            ?>
            </ul>
            <?php
        }
        else
        {
            echo "<h5 class='text-info text-center'>".NO_ADVERTS_CREATED_YET."</h5>";
        } // else

    } // end


    /**
     * adverts created by client in grid view
     * 
     * @param string $advertsEmailSession the current session email whose adverts we need to fetch
     * 
     * @return void
     * @access public
     */
    function sessionCreatedAdvertsGrid($advertsEmailSession)
    {
        $get_adverts_q = $this->conn->prepare("select advert_post_id, business_profile_id, advert_title, date_format(date_post, '%d %M, %Y') as advert_created_date,
            date_format(start_period, '%d %M, %Y') as advert_start_date,
            date_format(end_period, '%d %M, %Y') as advert_end_date,
            publish,
            status,
            approved from advert_posts where loggedEmail = :loggedEmail");
        $get_adverts_q->execute([
            "loggedEmail" => $advertsEmailSession
        ]);
        $get_adverts = $get_adverts_q->fetchAll();

        $advertsArray = array();

        if ($get_adverts) 
        {
            ?>
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">

            <?php
            foreach($get_adverts as $row)
            {
            ?>
            <div class="col-md-6 col-sm-6 col-xs-3">   
                <p><strong>Advert Title:</strong> <?php echo $row['advert_title']; ?></p>
                <p><strong>Posted:</strong> <?php echo $row['advert_created_date']; ?></p>
                <p><strong>Starting on:</strong> <?php echo $row['advert_start_date']; ?></p>
                <p><strong>Ending on:</strong> <?php echo $row['advert_end_date']; ?></p>
                <p><strong>Approved:</strong> <?php echo $row['approved']; ?></p>
                <p><strong>Status:</strong> 
                    <?php 
                        if($row['status'] == 0) {
                            echo "Inactive";
                        } else if ($row['status'] == 1) {
                            echo "Active";
                        } else {
                            echo "Ended";
                        }
                    ?>
                </p>
                <p>
                    <a href="#dataModalAdverts" data-toggle="modal" onclick="loadAdvertDetailsfunction('<?php echo $row['advert_post_id']; ?>')" class="btn btn-primary btn-flat btn-xs">View</a>
                <!--
                    <a href="javascript:void(0)" class="btn btn-info btn-flat btn-xs" onclick="advertEditfunc('<?php // echo $row['advert_post_id']; ?>')">
                        Edit
                    </a>
                -->
                    <a href="#" id="deleteAdvertId<?php echo $row['advert_post_id']; ?>" class="btn btn-danger btn-flat btn-xs" onclick="deleteAdvertFunction('<?php echo $row['advert_post_id']; ?>')">
                        Delete
                    </a>
                </p>
                <p class="list-divider"></p>
            </div>
            <?php
            } // loop
            ?>

                </div>
            </div>

            <?php
        }
        else
        {
            echo "<h5 class='text-info text-center'>".NO_ADVERTS_CREATED_YET."</h5>";
        } // else

    } // end
    

    /**
     * this method shows the advert editing form
     * 
     * @param int $sessionAdvertEditArgs the advert post id associated with the advert the client wants
     *                                   to edit
     * @return void
     * @access public
     */
    function _editingSessionAdvert($sessionAdvertEditArgs)
    {

        $editing_advert_q = $this->conn->prepare($this->_adverts(). " where advert_post_id = :advert_post_id");
        $editing_advert_q->execute([
            "advert_post_id" => $sessionAdvertEditArgs
        ]);
        $editing_advert = $editing_advert_q->fetchObject();

        if ($editing_advert) 
        {
        ?>

    <form id="dataForm-editing-advert" class="form-vertical form-label-left mdb-color-text" role="form" enctype="multipart/form-data">

    <input type="hidden" value="<?php echo $_SESSION["ur_email"]; ?>" name="editAdvert_loggedInEmail">

    <input type="hidden" value="<?php echo $sessionAdvertEditArgs; ?>" name="editAdvert_currentAdvert">

    <input type="hidden" value="<?php echo $editing_advert->publish; ?>" name="editAdvert_publish">

    <input type="hidden" id="deleteSessionInputEdit<?php echo $sessionAdvertEditArgs; ?>" value="<?php echo $sessionAdvertEditArgs; ?>" class="deleteSessionInputEditClass">
    
    <div class="form-group">
        <label for="editAdvert_have_a_businessId">Do you have a business?</label>
            <div id="editAdvert_have_a_businessId">
            <label class="radio-inline"><input type="radio" class="editAdvert_have_a_businessClass" name="editAdvert_have_a_business" value="yes">Yes</label>
            <label class="radio-inline"><input type="radio" class="editAdvert_have_a_businessClass" name="editAdvert_have_a_business" value="no">No</label>                      
            </div>
    </div>

    <div class="form-group item_display_none" id="business_name_toggle">
        <label for="editAdvert_businessName">Business Name</label>
            <select name="editAdvert_businessName" id="editAdvert_businessName" class="form-control" style="width: 100%" data-placeholder="--- select business ---">
            <option></option>
            <?php foreach( $this->_businessnames() as $row ) { ?>
                <option value="<?php echo $row['business_profile_id']; ?>"><?php echo $row['Business_Name']; ?></option>
            <?php } ?>
            </select>
    </div>

        <div class="form-group">
        <label for="editAdvert_title">Advert Title</label>
        <input type="text" id="editAdvert_title" class="form-control" name="editAdvert_title" required placeholder="advert title" autocomplete="off" value="<?php echo $editing_advert->advert_title; ?>">                   
        </div>

        <div class="form-group">
            <label for="editAdvert_period">Advert Period</label>
            <div class="controls">
                <div class="input-prepend input-group">
                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                <input type="text" name="editAdvert_period" id="editAdvert_period" class="form-control daterange_picker_one" value="<?php echo $editing_advert->start_period.' - '.$editing_advert->end_period; ?>" required>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="editAdvert_details">Advert Details</label>    

        <textarea id="editAdvert_details" class="form-control bp-wysihtml5" aria-describedy='helperInfor_characters' name="editAdvert_details" maxlength="800">
            <?php echo htmlspecialchars_decode($editing_advert->description); ?>
        </textarea>
        
            <small class="text-danger" id="helperInfor_characters">800 characters ONLY</small>

        </div>

    <!-- /.
        <div class="form-group">
            <label for="editAdvert_bannersToUploadid">Do you have banners to upload?</label>
            <div id="editAdvert_bannersToUploadid">
                <label class="radio-inline"><input type="radio" value="yes" name="editAdvert_bannersYesNo" class="dataclass_banners" id="checkYes">Yes</label>
                <label class="radio-inline"><input type="radio" value="no" name="editAdvert_bannersYesNo" class="dataclass_banners" id="checkNo" required>No</label>
            </div>
        </div>

        <div class="form-group" id="toggleBannerUpload_formGroup">
            <label>Upload Banners <span class="fa fa-question-circle text-info fa-2x" style="cursor: pointer;" title="You can upload a maximum of 3 banners. The first banner is REQUIRED and is considered as your primary/main banner which is shown when advertising" data-toggle="tooltip"></span></label>
    ./-->


            <!-- allow 3 banners only -->
            
    <!-- /.
        <span class="uploadErrors_span"></span>

            <div class="row">
    ./-->

                <!-- banner 1 -->

                
    <!-- /.
        <div class="col-md-4">
                <div class="imgUp">
                    <div class="imagePreview imagePreviewClass"></div>
                    <label class="btn bg-purple btn-flat selectFileToUploadClass" id="selectFileToUpload"><span id="selectTextOne">Select</span>
                        <input type="file" class="uploadFile img" value="" id="advert_bannerOne" name="advert_bannerOne" style="width: 0px;height: 0px;overflow: hidden;">
                    </label>
                    <span class="fa fa-times iclassBanners displayClass" onclick="deleteBanner(1)" id="deleteBanner1" title="delete this banner"></span>
                </div>  
                <p class="text-center"><small class="text-red"><i class="fa fa-arrow-circle-up fa-2x"></i> This is your primary banner</small></p>
                </div>
    ./-->

                <!-- banner 2 -->            
                
    <!-- /.
        <div class="col-md-4">
                <div class="imgUp2 imgUpClass">
                    <div class="imagePreview2 imagePreviewClass"></div>
                    <label class="btn bg-purple btn-flat selectFileToUploadClass" id="selectFileToUpload2"><span id="selectTextTwo">Select</span>
                        <input type="file" class="uploadFile2 img" value="" id="advert_bannerTwo" name="advert_bannerTwo" style="width: 0px;height: 0px;overflow: hidden;">
                    </label>
                    <span class="fa fa-times iclassBanners displayClass" onclick="deleteBanner(2)" id="deleteBanner2" title="delete this banner"></span>
                </div>                            
                </div>
    ./-->

                <!-- banner 3 -->            
                
    <!-- /.
        <div class="col-md-4">
                <div class="imgUp3 imgUpClass">
                    <div class="imagePreview3 imagePreviewClass"></div>
                    <label class="btn bg-purple btn-flat selectFileToUploadClass" id="selectFileToUpload3"><span id="selectTextThree">Select</span>
                        <input type="file" class="uploadFile3 img" value="" id="advert_bannerThree" name="advert_bannerThree" style="width: 0px;height: 0px;overflow: hidden;">
                    </label>
                    <span class="fa fa-times iclassBanners displayClass" onclick="deleteBanner(3)" id="deleteBanner3" title="delete this banner"></span>
                </div>                             
                </div>
            </div>
        </div>
    ./-->

    <div class="ln_solid"></div>

    <div class="form-group row">
        <div class="col-xs-10">
        <button type="submit" class="btn c_btn c_btnOne btn-flat" id="dataBtn_updateAdvert">Update</button>
        </div>
    </div>

    </form>

    <script>
    $(function(){

    // --- >>  if there were banners
        // -- >> show banner thumbnails  

        //var bOneCheck = "<?php #echo !is_null($editing_advert->primary_banner_1) ? 1 : 2; ?>";
        //var bTwoCheck = "<?php #echo !is_null($editing_advert->s_banner_2) ? 1 : 2; ?>";
        //var bThreeCheck = "<?php #echo !is_null($editing_advert->s_banner_3) ? 1 : 2; ?>";
        /**
        var banner1 = "<?php #echo !is_null($editing_advert->primary_banner_1) ? '_assets/clientUploads/'.$editing_advert->primary_banner_1 : '_assets/images/pic_blank.jpg'; ?>";

        var banner2 = "<?php #echo !is_null($editing_advert->s_banner_2) ? '_assets/clientUploads/'.$editing_advert->s_banner_2 : '_assets/images/pic_blank.jpg'; ?>";

        var banner3 = "<?php #echo !is_null($editing_advert->s_banner_3) ? '_assets/clientUploads/'.$editing_advert->s_banner_3 : '_assets/images/pic_blank.jpg'; ?>";
        **/
    /**
        if (bOneCheck == 1) 
        {

            // check yes
            $("#checkYes").prop("checked", true)

            // slidedown thumbnails
            $("#toggleBannerUpload_formGroup").show();

            // display banner 1
            var banner1 = "<?php # echo '_assets/clientUploads/'.$editing_advert->primary_banner_1; ?>";

            $('.imagePreview').css({"background-image":"url("+banner1+")", "width":"200px", "height":"200px"}); 

            // show delete button
            $("#deleteBanner1").removeClass("displayClass");

            // change select text
            $("#selectTextOne").html("Change");       
        }

        if (bTwoCheck == 1) 
        {
            var banner2 = "<?php # echo '_assets/clientUploads/'.$editing_advert->s_banner_2; ?>";

            $('.imagePreview2').css({"background-image":"url("+banner2+")", "width":"200px", "height":"200px"});

            // show delete button
            $("#deleteBanner2").removeClass("displayClass");

            // change select text
            $("#selectTextTwo").html("Change");   
        }

        if (bThreeCheck == 1) 
        {
            var banner3 = "<?php # echo '_assets/clientUploads/'.$editing_advert->s_banner_3; ?>";

            $('.imagePreview3').css({"background-image":"url("+banner3+")", "width":"200px", "height":"200px"});

            // show delete button
            $("#deleteBanner3").removeClass("displayClass");

            // change select text
            $("#selectTextThree").html("Change");       
        }

    */
        

        // --- >> update
        $("#dataForm-editing-advert").on("submit", function(e)
        {
        e.preventDefault();

            $("#dataBtn_updateAdvert").prop("disabled", true);
            $("#dataBtn_updateAdvert").html("please wait");

        $.ajax({
            url: "_server_requests.php",
            method: "post",
            data: new FormData(this),
            contentType: false, 
            cache: false,        
            processData:false, 
            success: function(data)
            {
            bootbox.alert(data);
                $("#dataBtn_updateAdvert").prop("disabled", false);
                $("#dataBtn_updateAdvert").html("Update");  
            } 
            });          
        });

        // init daterangepicker
        $('.daterange_picker_one').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }      
        });

        // do you have banners
        $(".dataclass_banners").on("click", function(){
            let clickedRadioBtnValue = $(this).val();

            if (clickedRadioBtnValue == 'yes') 
            {
                $("#toggleBannerUpload_formGroup").slideDown("slow");
            }
            else{
                $("#toggleBannerUpload_formGroup").slideUp("slow");
            }
        });

        // >> ---- accepted file extensions
        let exceptedExtensions = ["jpg", "png", "jpeg", "gif"];

        // 1
        $(document).on("change",".uploadFile", function()
        {

            // --- >> validate
            var bannerOneValue = $("#advert_bannerOne").val();
            // size
            var bannerOneSize = $("#advert_bannerOne")[0].files[0].size;

            if ($.inArray(bannerOneValue.split('.').pop().toLowerCase(), exceptedExtensions) == -1) 
            {                
                _errorNotfify("Error: invalid file format selected; only jpg, jpeg, gif and png allowed!")
            }
            else if (bannerOneSize > 1000000)
            {
                _errorNotfify("Error: maximum file size should not exceed 1 Mb")
            }
            else
            {                

                var uploadFile = $(this);            

                // show delete button
                $("#deleteBanner1").removeClass("displayClass");

                // change select text
                $("#selectTextOne").html("Change");


                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        
                if (/^image/.test( files[0].type))
                { // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file
        
                    reader.onloadend = function(){ // set image data as background of div
                        //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                        uploadFile.closest(".imgUp").find('.imagePreview').css({"background-image":"url("+this.result+")", "width":"200px", "height":"200px"});
                    }
                } 

            } // else     
        });

        // 2
        $(document).on("change",".uploadFile2", function()
        {

            // --- >> validate
            var bannerTwoValue = $("#advert_bannerTwo").val();
            // size
            var bannerTwoSize = $("#advert_bannerTwo")[0].files[0].size;

            if ($.inArray(bannerTwoValue.split('.').pop().toLowerCase(), exceptedExtensions) == -1) 
            {                
                _errorNotfify("Error: invalid file format selected; only jpg, jpeg, gif and png allowed!")
            }
            else if (bannerTwoSize > 1000000)
            {
                _errorNotfify("Error: maximum file size should not exceed 1 Mb")
            }
            else
            { 
                // show delete button
                $("#deleteBanner2").removeClass("displayClass");

                // change select text
                $("#selectTextTwo").html("Change");
                    
                var uploadFile = $(this);

                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        
                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file
        
                    reader.onloadend = function(){ // set image data as background of div
                        //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                        uploadFile.closest(".imgUp2").find('.imagePreview2').css({"background-image":"url("+this.result+")", "width":"200px", "height":"200px"});
                    }
                }     
            } // else     
        });

        // 3
        $(document).on("change",".uploadFile3", function()
        {

            // --- >> validate
            var bannerThreeValue = $("#advert_bannerThree").val();
            // size
            var bannerThreeSize = $("#advert_bannerThree")[0].files[0].size;

            if ($.inArray(bannerThreeValue.split('.').pop().toLowerCase(), exceptedExtensions) == -1) 
            {                
                _errorNotfify("Error: invalid file format selected; only jpg, jpeg, gif and png allowed!")
            }
            else if (bannerThreeSize > 1000000)
            {
                _errorNotfify("Error: maximum file size should not exceed 1 Mb")
            }
            else
            { 
                // show delete button
                $("#deleteBanner3").removeClass("displayClass");

                // change select text
                $("#selectTextThree").html("Change");
                    
                var uploadFile = $(this);

                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        
                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file
        
                    reader.onloadend = function(){ // set image data as background of div
                        //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                        uploadFile.closest(".imgUp3").find('.imagePreview3').css({"background-image":"url("+this.result+")", "width":"200px", "height":"200px"});
                    }
                }      
            }    
        });

        //bootstrap WYSIHTML5 - text editor
        $(".bp-wysihtml5").wysihtml5({
            toolbar:{
                "image": false
            }
        });       

        // --- >> select2 for selecting businesses available
        $("#editAdvert_businessName").select2({
            placeholder: '--- select Business ---',
            tags: true, 
            allowClear: true,
            escapeMarkup: function (markup) { return markup; },
            language: {
                noResults: function () {
                    return "No Business Created Yet. <a href='_navigation.php?pageNavigation=createNewBusiness'>Create one?</a>";
            }
            }
        });
        
        // --- >> does user have a business
        $(".editAdvert_have_a_businessClass").on('click', function() {
            var businessYesNoValue = $("input[name='editAdvert_have_a_business']:checked").val();
            if (businessYesNoValue == "yes") 
            {
            $("#business_name_toggle").slideDown();
            } else {
            $("#business_name_toggle").slideUp();
            }
        });  

    });

    // --- >>> delete banner
    function deleteBanner(bannerId) 
    {
        // deleting banner 1
        if (bannerId == 1) 
        {
            // $(this).parent().remove();
            $(".imagePreview").css("background-image", "url('_assets/images/pic_blank.jpg')");
            // clear file
            $("#advert_bannerOne").val("");
            // hide delete button
            $("#deleteBanner1").addClass("displayClass");
            // change select text
            $("#selectTextOne").html("Select");
        }

        // deleting banner 2
        else if (bannerId == 2) 
        {
            // $(this).parent().remove();
            $(".imagePreview2").css("background-image", "url('_assets/images/pic_blank.jpg')");
            // clear file
            $("#advert_bannerTwo").val("");
            // hide delete button
            $("#deleteBanner2").addClass("displayClass");
            // change select text
            $("#selectTextTwo").html("Select");
        }

        // deleting banner 3
        else if (bannerId == 3) 
        {
            // $(this).parent().remove();
            $(".imagePreview3").css("background-image", "url('_assets/images/pic_blank.jpg')");
            // clear file
            $("#advert_bannerThree").val("");
            // hide delete button
            $("#deleteBanner3").addClass("displayClass");
            // change select text
            $("#selectTextThree").html("Select");
        }

    }
    // pnotify error function
    function _errorNotfify(errorMsg) {

        // notify
        new PNotify({
        title: "Upload Notice",
        type: "error",
        text: errorMsg,
        nonblock: {
            nonblock: false
        },
        delay: 6000,
        buttons: {
            closer: true,
            closer_hover: true,
            show_on_nonblock: false
        },
        styling: 'bootstrap3'
        });
    }
    </script>

        <?php
        } // if
        else
        {
        echo "<p class='text-warning text-center'>".FAILED_TO_LOAD_ADVERT_FOR_EDITING."</p>";
        }

    } // end


    /**
     * the main client advert update method
     * 
     * @param int $advert_businessName this is actually the business id not the name
     * @param string $advert_have_a_business does the client have a business which he/she 
     *                                       can associate with this advert; yes or no
     * @param string $advert_title the advert title
     * @param string $advert_publish incase the client wants the advert to be publised immediately once the admin approves it.
     * @param string $advert_period the period the advert will likely run / take
     * @param string $advert_loggedInEmail the email address of the client who is creating the advert
     * @param string $advert_details the details of the advert; this is an html markup
     * @param int $advert_currentAdvert the advert post id or primary to be associated with the update
     * 
     * @return void
     * @access public
     */
    function _updateSessionAdvert(
            $advert_have_a_business,
            $advert_businessName,
            $advert_currentAdvert, 
            $advert_title,
            $advert_publish, 
            $advert_period, 
            $advert_loggedInEmail,
            $advert_details/*,
    		$advert_bannerName, 
            $advert_bannerTmp,
            $advert_bannerSize,
            $advert_bannerTwoName,
            $advert_bannerTwoTmp,
            $advert_bannerTwoSize, 
            $advert_bannerThreeName, 
            $advert_bannerThreeTmp, 
            $advert_bannerThreeSize,
            $advert_bannersYesNo*/)
    {

        $advert_details = htmlspecialchars(trim($advert_details, ' '));

        if (empty($advert_have_a_business)) {
            $this->errs[] = EMPTY_WHETHER_YOU_HAVE_A_BUSINESS;
        }
        else if (($advert_have_a_business == "yes") && empty($advert_businessName)) {
            $this->errs[] = SELECT_BUSINESS_NAME;
        }
        else if (empty($advert_title)) 
        {
           $this->errs[] = EMPTY_ADVERT_TITLE;
        }
        /*
        else if (empty($advert_publish)) 
        {
           $this->errs[] = SELECT_WHETHER_TO_PUBLISH;
        }
        */
        else if (empty($advert_period)) 
        {
           $this->errs[] = SELECT_PERIOD;
        }
        else if (empty($advert_details)) 
        {
           $this->errs[] = EMPTY_ADVERT_DETAILS;
        }
        elseif (strlen($advert_details) > 800) 
        {
           $this->errs[] = ADVERT_DETAILS_800_CHAR;
        }
        /*
        elseif (empty($advert_bannersYesNo)) 
        {
           $this->errs[] = DISPLAY_BANNER_YES_NO;
        }
        */
        else
        {

            if($this->_updateAdvertQueryNoBanners(
                $advert_businessName,
                $advert_have_a_business,
                $advert_title,
                $advert_publish, 
                $advert_period, 
                $advert_loggedInEmail,
                $advert_details,
                $advert_currentAdvert))
            {
                 $this->mgss[] = ADVERT_DETAILS_UPDATED;            

            } // if
            else {
                $this->errs[] = FAILED_TO_UPDATE_ADVERT_DETAILS;
            } // else

    		
    	/**
            if ($advert_bannersYesNo == 'yes') 
            {

                if(!$advert_bannerName) 
                {
                    $this->errs[] = PRIMARY_BANNER_NOT_SELECTED;
                }
                else
                {

                    // --- >> validate banners one by one
                    if ($advert_bannerName &&
                        !$advert_bannerTwoName &&
                        !$advert_bannerThreeName) 
                    {
                        if ($this->_bannerValidation(
                            $advert_bannerName, 
                            $advert_bannerTmp,
                            $advert_bannerSize))
                        {

    $banner1_ext  = strtolower(pathinfo($advert_bannerName,PATHINFO_EXTENSION));
    $banner1_extFile = 'PB-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner1_ext;
                            
                            if($this->_saveAdvertQuery(
                                    $advert_title,
                                    $advert_publish, 
                                    $advert_period, 
                                    $advert_loggedInEmail,
                                    $advert_details,
                                    $advert_bannerName = $banner1_extFile, 
                                    $advert_bannerTwoName = NULL, 
                                    $advert_bannerThreeName = NULL))
                            {
                                 $this->mgss[] = NEW_ADVERT_CREATED;

    // move uploads
    move_uploaded_file($advert_bannerTmp, $this->bannerDir.$banner1_extFile);
                            
                            // call clear fields method
                            $this->_resetAdvertFields();

                            } // if
                            else {
                                $this->errs[] = FAILED_TO_SAVE_ADVERT;
                            } // else
                        
                        } // validation  
                                  
                    } // banner 1

                    else if (
                        $advert_bannerName &&
                        $advert_bannerTwoName &&
                        !$advert_bannerThreeName)
                    {
                        if ($this->_bannerValidation(
                            $advert_bannerTwoName, 
                            $advert_bannerTwoTmp,
                            $advert_bannerTwoSize)        
                            &&
                            $this->_bannerValidation(
                            $advert_bannerName, 
                            $advert_bannerTmp,
                            $advert_bannerSize))
                        {

    $banner1_ext  = strtolower(pathinfo($advert_bannerName,PATHINFO_EXTENSION));
    $banner1_extFile = 'PB-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner1_ext;

    $banner2_ext  = strtolower(pathinfo($advert_bannerTwoName,PATHINFO_EXTENSION));
    $banner2_extFile = 'B2-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner2_ext;

                            if($this->_saveAdvertQuery(
                                    $advert_title,
                                    $advert_publish, 
                                    $advert_period, 
                                    $advert_loggedInEmail,
                                    $advert_details,
                                    $advert_bannerName = $banner1_extFile,
                                    $advert_bannerTwoName = $banner2_extFile,
                                    $advert_bannerThreeName = NULL))
                            {
                                 $this->mgss[] = NEW_ADVERT_CREATED;

    // move uploads
    move_uploaded_file($advert_bannerTmp, $this->bannerDir.$banner1_extFile);
    move_uploaded_file($advert_bannerTwoTmp, $this->bannerDir.$banner2_extFile);
                            

                            } // if
                            else {
                                $this->errs[] = FAILED_TO_SAVE_ADVERT;
                            } // else
                        
                        } // validation  

                    } // banner 1 and 2

                    else if ($advert_bannerName && !$advert_bannerTwoName && $advert_bannerThreeName) 
                    {
                        if ($this->_bannerValidation(
                            $advert_bannerThreeName, 
                            $advert_bannerThreeTmp,
                            $advert_bannerThreeSize)
                            &&
                            $this->_bannerValidation(
                            $advert_bannerName, 
                            $advert_bannerTmp,
                            $advert_bannerSize))
                        {

    $banner1_ext  = strtolower(pathinfo($advert_bannerName,PATHINFO_EXTENSION));
    $banner1_extFile = 'PB-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner1_ext;

    $banner3_ext  = strtolower(pathinfo($advert_bannerThreeName,PATHINFO_EXTENSION));
    $banner3_extFile = 'B3-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner3_ext;

                            if($this->_saveAdvertQuery(
                                    $advert_title,
                                    $advert_publish, 
                                    $advert_period, 
                                    $advert_loggedInEmail,
                                    $advert_details,
                                    $advert_bannerName = $banner1_extFile,
                                    $advert_bannerTwoName = NULL,
                                    $advert_bannerThreeName = $banner3_extFile))
                            {
                                 $this->mgss[] = NEW_ADVERT_CREATED;

    // move uploads
    move_uploaded_file($advert_bannerTmp, $this->bannerDir.$banner1_extFile);
    move_uploaded_file($advert_bannerThreeTmp, $this->bannerDir.$banner3_extFile);
                            
                            // call clear fields method
                            $this->_resetAdvertFields();

                            } // if
                            else {
                                $this->errs[] = FAILED_TO_SAVE_ADVERT;
                            } // else
                        
                        } // validation  
                                  
                    } // banner 1 and 3

                    else if ($advert_bannerName && $advert_bannerTwoName && $advert_bannerThreeName) 
                    {
                        if ($this->_bannerValidation(
                            $advert_bannerThreeName, 
                            $advert_bannerThreeTmp,
                            $advert_bannerThreeSize)
                            &&
                            $this->_bannerValidation(
                            $advert_bannerName, 
                            $advert_bannerTmp,
                            $advert_bannerSize)
                            &&
                            $this->_bannerValidation(
                            $advert_bannerTwoName, 
                            $advert_bannerTwoTmp,
                            $advert_bannerTwoSize))
                        {

    $banner1_ext  = strtolower(pathinfo($advert_bannerName,PATHINFO_EXTENSION));
    $banner1_extFile = 'PB-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner1_ext;

    $banner2_ext  = strtolower(pathinfo($advert_bannerTwoName,PATHINFO_EXTENSION));
    $banner2_extFile = 'B2-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner2_ext;

    $banner3_ext  = strtolower(pathinfo($advert_bannerThreeName,PATHINFO_EXTENSION));
    $banner3_extFile = 'B3-'.date('s').sha1(uniqid(mt_rand(), true)).".".$banner3_ext;

                            if($this->_saveAdvertQuery(
                                    $advert_title,
                                    $advert_publish, 
                                    $advert_period, 
                                    $advert_loggedInEmail,
                                    $advert_details,
                                    $advert_bannerName = $banner1_extFile,
                                    $advert_bannerTwoName = $banner2_extFile,
                                    $advert_bannerThreeName = $banner3_extFile))
                            {
                                 $this->mgss[] = NEW_ADVERT_CREATED;

    // move uploads
    move_uploaded_file($advert_bannerTmp, $this->bannerDir.$banner1_extFile);
    move_uploaded_file($advert_bannerTwoTmp, $this->bannerDir.$banner2_extFile);
    move_uploaded_file($advert_bannerThreeTmp, $this->bannerDir.$banner3_extFile);
                            

                            } // if
                            else {
                                $this->errs[] = FAILED_TO_SAVE_ADVERT;
                            } // else
                        
                        } // validation  
                                  
                    } // banner 1, 2 and 3

                } // primary banner was selected

            } // banners was selected for upload     

            else {

                if($this->_saveAdvertQueryNoBanners(
                    $advert_title,
                    $advert_publish, 
                    $advert_period, 
                    $advert_loggedInEmail,
                    $advert_details))
                {
                     $this->mgss[] = NEW_ADVERT_CREATED;
                    

                } // if
                else {
                    $this->errs[] = FAILED_TO_SAVE_ADVERT;
                } // else

            }   // no banner was selected   
    */		

        } // else validation

    } // end


    /**
     * this updates client advert having no banners from **_updateSessionAdvert()** method
     * 
     * @param int $advert_businessName this is actually the business id not the name
     * @param string $advert_have_a_business does the client have a business which he/she 
     *                                       can associate with this advert; yes or no
     * @param string $advert_title the advert title
     * @param string $advert_publish incase the client wants the advert to be publised immediately once the admin approves it.
     * @param string $advert_period the period the advert will likely run / take
     * @param string $advert_loggedInEmail the email address of the client who is creating the advert
     * @param string $advert_details the details of the advert; this is an html markup
     * @param int $advert_currentAdvert the advert post id or primary to be associated with the update
     * 
     * @return bool $u6 TRUE if update was successful; FALSE if it failed
     * @access private
     */
    private function _updateAdvertQueryNoBanners(
                    $advert_businessName,
                    $advert_have_a_business,
                    $advert_title,
                    $advert_publish, 
                    $advert_period, 
                    $advert_loggedInEmail,
                    $advert_details,
                    $advert_currentAdvert)
    {

        // separate date range
        list($advertStartDate, $advertEndPeriod) = explode(' - ', $advert_period);

        // save advert
        $u_q6 = $this->conn->prepare("update advert_posts set 
                business_profile_id = :business_profile_id,
                advert_title = :advert_title,
                start_period = :start_period,
                end_period = :end_period,
                publish = :publish,
                description = :description,
                status  = :status
            where loggedEmail = :loggedEmail and advert_post_id = :advert_post_id");

        $u6 = $u_q6->execute([
                'business_profile_id' => $advert_have_a_business == "yes" ? $advert_businessName : "N/A",
                'advert_title' => $advert_title,
                'start_period' => $advertStartDate,
                'end_period' => $advertEndPeriod,
                'publish' => $advert_publish,
                'description' => $advert_details,
                'status' => $advert_publish == 'yes' ? 1 : 0,
                'loggedEmail' => $advert_loggedInEmail,
                'advert_post_id' => $advert_currentAdvert
        ]);

        if ($u6) 
        {
            return true;
        }
        else
        {
            return false;
        }

    } // end

    // ---- >> client delete activities << ---- //

    /**
     * this method deletes client created jobs
     * 
     * @param int sessionDeleteArgs the job id we intend to delete
     * @return void
     * @access public
     */ 
    function _deleteSessionJobs($sessionDeleteArgs)
    {
        $delete_jobs_q1 = $this->conn->prepare("delete from jobs where job_id = :job_id");
        $delete_jobs1 = $delete_jobs_q1->execute([
            "job_id" => $sessionDeleteArgs
        ]);  
        $delete_jobs_q2 = $this->conn->prepare("delete from job_responsibility where job_id = :job_id");
        $delete_jobs2   = $delete_jobs_q2->execute([
            "job_id" => $sessionDeleteArgs
        ]);   

        if ($delete_jobs1 && $delete_jobs2) 
        {
           echo JOB_DELETED;
        }
        else
        {
           echo FAILED_DELETE_JOB;
        }

    } // end


    /**
     *  delete an advert
     * 
     * @param int $sessionAdvertDeleteArgs advert posts tbl primary key; 
     *                                     we're passing the advert id we intend to delete
     * @return void
     * @access public
     */
    function _deleteSessionAdvert($sessionAdvertDeleteArgs)
    {
        $delete_adverts_q2 = $this->conn->prepare("delete from advert_posts where advert_post_id = :advert_post_id and loggedEmail = :loggedEmail");
        $delete_adverts2   = $delete_adverts_q2->execute([
            "advert_post_id" => $sessionAdvertDeleteArgs,
            "loggedEmail" => $_SESSION['ur_email']
        ]);  

        if ($delete_adverts2) 
        {

            // --- if he/she liked his/her advert; delete likes
            $delete_adverts_q1 = $this->conn->prepare("delete from advert_likes where advert_post_id = :advert_post_id and liked_by_email = :liked_by_email");
            $delete_adverts1   = $delete_adverts_q1->execute([
                "advert_post_id" => $sessionAdvertDeleteArgs,
                "liked_by_email" => $_SESSION['ur_email']
            ]);   


          // --- >> check if banners were uploaded
            $check_for_banners_q = $this->conn->prepare("select primary_banner_1, s_banner_2, s_banner_3 from advert_posts where advert_post_id = :advert_post_id") ;
            $check_for_banners_q->execute([
                "advert_post_id" => $sessionAdvertDeleteArgs
            ]);
            $check_for_banners = $check_for_banners_q->fetchObject();

            // delete banner 1
            $check_for_banners->primary_banner_1 ? unlink('.././_assets/clientUploads/'.$check_for_banners->primary_banner_1) : '';
            // delete banner 2
            $check_for_banners->primary_banner_1 ? unlink('.././_assets/clientUploads/'.$check_for_banners->s_banner_2) : '';
            // delete banner 3
           $check_for_banners->primary_banner_1 ? unlink('.././_assets/clientUploads/'.$check_for_banners->s_banner_3) : '';

           echo ADVERT_DELETED;
        }
        else
        {
            echo ADVERT_DELETION_FAILED;
        }

    } // end

    // ---- >> client stats activities << ---- //

    /**
     * displays client stats as animated progress bars under his/her profile
     *
     * @return array
     * @access public
     */
    function clientActivityStats() {
        // number of adverts
        $stats_q1 = $this->conn->prepare("select count(advert_post_id) as advertsNum from advert_posts where loggedEmail = :loggedEmail");
        $stats_q1->execute([
            "loggedEmail" => $_SESSION['ur_email']
        ]);
        $advertsNum = $stats_q1->fetchObject();

        // number of jobs
        $stats_q2 = $this->conn->prepare("select count(job_id) as jobsNum from jobs where loggedin_email = :loggedin_email");
        $stats_q2->execute([
            "loggedin_email" => $_SESSION['ur_email']
        ]);
        $jobsNum = $stats_q2->fetchObject();

        // number of advert likes
        $stats_q3 = $this->conn->prepare("select count(advert_like_id) as advertsLikeNum from advert_likes where liked_by_email = :liked_by_email");
        $stats_q3->execute([
            "liked_by_email" => $_SESSION['ur_email']
        ]);
        $advertsLikeNum = $stats_q3->fetchObject();

        // number of favorites; ie. jobs and adverts
        $stats_q4 = $this->conn->prepare("select count(fav_id) as favNumber from favorites where email_address = :email_address");
        $stats_q4->execute([
            "email_address" => $_SESSION['ur_email']
        ]);
        $favNumber = $stats_q4->fetchObject();

        return json_encode(array(
            "advertsNumber" => $advertsNum->advertsNum,
            "jobsNumber" => $jobsNum->jobsNum,
            "advertsLikesNumber" => $advertsLikeNum->advertsLikeNum,
            "favNumber" => $favNumber->favNumber
        ));

    } // end


    /* --------------------------------------------------- */
    /* general
    /* --------------------------------------------------- */

    /**
     * return districts in uganda
     *
     * @return object $return_districts districts name in uganda
     * @access public
     */
    function _districts()
    {
        $return_districts_q = $this->conn->prepare("select district_name from districts order by district_name asc");
        $return_districts_q->execute();
        $return_districts  = $return_districts_q->fetchAll();

        return $return_districts;
    } // end

    /**
     * this method queries and return the nationalities in the world
     *
     * @return object $return_nationality nationalities in the world
     * @access public
     */
    function _nationality()
    {
        $return_nationality_q = $this->conn->prepare("select * from nationality order by name");
        $return_nationality_q->execute();
        $return_nationality   = $return_nationality_q->fetchAll();

        return $return_nationality;
    } // end

    /**
     * this method gets the business name of registered businesses from the business_profile tbl
     *
     * @param string $businessId the id associated with the business whose name we need
     * @return string $r business name associated with respective id
     * @access private
     */
    private function _getBusinessName($businessId) {
        $g_q = $this->conn->prepare("select Business_Name from business_profile where business_profile_id = :business_profile_id");
        $g_q->execute([
            "business_profile_id" => $businessId
        ]);
        $g = $g_q->fetchObject();
        $r = $g ? $g->Business_Name : 'n\a';
        return $r;
    } // end

    /**
     * feedback method
     *
     * @return void
     * @access public
     */
    function _feedbacks()
    {
        foreach($this->errs as $err)
        {
            echo "<p class='text-center text-danger ui-toggle-display'>".$err."</p>";
        }
        foreach($this->mgss as $mgs)
        {
            echo "<p class='text-center text-success ui-toggle-display'>".$mgs."</p>";
        }
        ?>
    	
    <script>

    $(function(){
       //setTimeout(()=>{ $('.ui-toggle-display').fadeOut(); }, 3000) ;
    });
    </script>
        <?php
        
    } // end


    /**
     * clear form method
     *
     * @return void
     * @access private
     */
    private function _clearFormFields()
    {
        ?>
    <script>
    $(function(){
       $(".clear_form_after_submission")[0].reset();
    });
    </script>
        <?php
    } // end


    /**
     * return list for job categories
     * 
     * @return object
     * @access public
    */
    function jobCategories() {
        $jc_q = $this->conn->prepare("select * from job_category order by jcName");
        $jc_q->execute();
        $jc = $jc_q->fetchAll();
        return $jc;
    } // end

} // << --- @endof globalClass