<?php
    /* ----------------------------- */
    /* fast-service database globals
    /* ----------------------------- */
    
    define("DB_HOST", "localhost");
    define("DB_USER", "root");
    define("DB_PASSWORD", ""); // Admin@123
    define("DB_DATABASE", "bcd");

    // --- >> misc
    define("f_f", "Fast Service");
    define("UR_TYPE", "user");
    define("SENT", "Sent");
    define("RECEIVED", "Received");
    define("DELETED", "Deleted");
    define("FAST_SERVICE_ADMIN_EMAIL", "info@fastserviceug.com");
    define("DO_NOT_REPLY_EMAIL", "donot.reply@fastservice.com");
    define("ugx", "Ugx. ");
    define("FF_URL","https://fastservice.co.ug"); // change this to right one

    // --- >> login cookie
    define("COOKIE_NAME", "fastservicetxt"); // --- >> cookie name
    define("COOKIE_RUNTIME", 604800); // -->> duration of cookie; 1 week from current login
    define("COOKIE_DOMAIN", "fastservice"); // --- >> domain cookie should cover; for this case, entire website
    define("COOKIE_SECRET_KEY", "4f5481a071e2f454e885a9f32bf56801aab57172"); // --- cookie key / value; 

    // --- >> default timezone
    date_default_timezone_set("Africa/Nairobi");
    define('CURRENT_DATE', date('Y-m-d'));

    /* ----------------------------------------------------------- */
    /*  phpmailer email definitions
    /* ----------------------------------------------------------- */ 
    define("EMAIL_USE_SMTP", false);
    define("EMAIL_SMTP_HOST", "localhost");
    define("EMAIL_SMTP_AUTH", true);
    define("EMAIL_SMTP_USERNAME", "root");
    define("EMAIL_SMTP_PASSWORD", "");
    define("EMAIL_SMTP_PORT", 465);
    define("EMAIL_SMTP_ENCRYPTION", "ssl");
    define("EMAIL_SMTP_DEBUG", 2);
    #define("EMAIL_SMTP_PORT", 587);
    #define("EMAIL_SMTP_ENCRYPTION", "tls");


