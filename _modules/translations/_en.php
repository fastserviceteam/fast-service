<?php

/* ---------------------------------------- */
/* server, form etc, feedbacks definitions
/* ---------------------------------------- */

// --- >> send email << --- //
define("EMPTY_NAMES", "Error: please enter your names!");
define("EMPTY_SUBJECT", "Error: please type your subject!");
define("EMPTY_MESSAGE", "Error: please type your message!");
define("EMPTY_EMAIL", "Error: please enter your valid email address!");
define("MESSAGE_NOT_GREATER_THAN_150", "Error: message should not exceed 150 characters!");
define("INVALID_EMAIL_ADDRESS", "Error: invalid email address entered!");
define("SUBJECT_NOT_GREATER_THAN_100", "Error: subject should not exceed 100 characters!");
define("NAMES_NOT_GREATER_THAN_15", "Error: names should not exceed 15 characters!");

define("EMAIL_SUCCESSFULLY_SENT", "Info: your email was successfully sent! We shall reply to you as soon as we can.");
define("FAILED_TO_SEND_EMAIL", "Error: your email was not sent! Please try again later.");

// --- >> open account << --- //
define("EMPTY_INPUT_FIELDS", "Error: please check for empty fields!");
define("INVALID_PHONE_NUMBER_LENGTH", "Error: invalid length entered for phone number!");
define("MIN_PASSWORD_LENGTH", "Error: password should be more than 8 characters!");
define("PASSWORDS_ARE_NOT_MATCHING", "Error: your passwords are not matching!");
define("INVALID_NAMES_LENGTH", "Error: invalid length enter for your names!");
define("NEW_ACCOUNT_SUCCESSFULLY_CREATED", "Info: your account was successfully created!");
define("FAILED_TO_CREATE_ACCOUNT", "Error: your account was not created. please try again!");
define("EMAIL_ADDRESS_ALREADY_EXISTS", "Error: this email address is already registered!");
define("EMAIL_ADDRESS_OR_PHONE_NUMBER_ALREADY_EXISTS", "Error: email address or phone number already exists!");

// --- >> login in << --- //
define("WRONG_EMAIL_OR_PASSWORD", "Error: invalid email address or password");
define("WRONG_EMAIL", "Error: wrong email address entered!");
define("WRONG_PASSWORD", "Error: wrong password entered!");
define("FAILED_TO_LOGIN", "Error: failed to login; please try again later!");
define("SERVER_TEMP_DOWN", "Notice: server is temporarily down; please try again later!");
define("MESSAGE_COOKIE_INVALID", "Error: invalid cookie!");

// --- >> reset password << --- //
define("UNKNOWN_EMAIL_ADDRESS_ENTERED", "Error; email address entered is not registered within the system");
define("UNKNOW_ERROR_OCCURRED", "Unknown error occured; please try again later");
define("PASSWORD_RESET_LINK_SENT", "Info: a password reset link was sent to your email; please check your email for it.");
define("PASSWORD_RESET_LINK_HAS_EXPIRED", "Error: Your password reset link has expired. Please request for another one!");
define("PASSWORD_RESET_LINK_DEACTIVATED", "Error: password reset link has been deactivated. Please request for another one!");
define("FAILED_TO_CONNECT_ACCOUNT", "Error: failed to connect to server account. please try again later!");
define("PASSSWORD_SUCCESSFUL_RESET", "Info: your password was successfully reset!");
define("REDIRECTED_SHORTLY", "You will be redirected shortly to login.");

// --- >> business.php << --- //
define("EMPTY_BUSINESS_NAME", "Error: please enter business name");
define("EMPTY_BUSINESS_CATEGORY", "Error: please enter business category");
define("EMPTY_BUSINESS_PCONTACT", "Error: please enter business phone contact");
define("EMPTY_BUSINESS_EMAIL", "Error: please enter business email address");
define("EMPTY_BUSINESS_COUNTRY", "Error: please select business country");
define("EMPTY_BUSINESS_SERVICES", "Error: please select service(s) offered");
define("EMPTY_BUSINESS_GEOG_LOCATION", "Error: please enter business area of operation");
define("NEW_BUSINESS_CREATED", "Info: new business successfully created");
define("FAILED_CREATE_BUSINESS", "Error: failed to create business; please try again later!");

// --- >> job  << --- //
define("EMPTY_JOB_CATEGORY", "Error; please select job category");
define("EMPTY_COMPANY_NAME", "Error: please select company name");
define('EMPTY_JOB_TITLE', 'Error: please enter job title');
define('EMPTY_JOB_DISTRICT', 'Error: please enter job district');
define('EMPTY_JOB_LOCATION', 'Error: please enter job location');
define('EMPTY_JOB_TYPE', 'Error: please select job type');
define('EMPTY_JOB_SALARY', 'Error: please enter job salary');
define('EMPTY_POSTED_ON', 'Error: please select posting date');
define('EMPTY_DEADLINE', 'Error: please select job deadline');
define('EMPTY_QUALIFICATION', 'Error: please enter job qualification(s)');
define('EMPTY_JOB_RESP', 'Error: please enter atleast one job responsibility');
define('EMPTY_HOW_TO_APPLY', 'Error: please enter application process');
define('NEW_JOB_CREATED', 'Info: new job successfully created!');
define('FAILED_CREATE_JOB', 'Error: failed to create new job. please try again');
define("FAILED_TO_LOAD_JOB_DETAILS", "Error: failed to load selected job details!");
define("EMPTY_WHETHER_YOU_HAVE_A_BUSINESS", "Error: please check whether you have a business or not!");
    
// --- >> service request << --- //
define('EMPTY_SERVICE_LOCATION', 'Error: please enter your current location');
define('EMPTY_SERVICE_DELIVERY_POINT', 'Error: please enter service delivery point!');
define('EMPTY_SERVICE_REQUIRE', 'Error: please select the service you want!');
define('EMPTY_SERVICE_QUOT', 'Error: please enter service quotation description');
define('EMPTY_SERVICE_DELIV_DATE', 'Error: please select service delivery date!');
define('EMPTY_SERVICE_DELIV_TIME', 'Error: please pick service delivery time!');
define('SERVICE_QUOT_MAX_200_EXCEEDED', 'Error: service quotation should not exceed 200 characters!');

define('SERVICE_REQUEST_SUBMITTED', 'Info: your service request was successfully submitted! please wait for response from provider.');
define('SERVICE_REQUEST_ERROR', 'Error: failed to submit service request; please try again later');

// --- >> adverts << --- //
define("SELECT_BUSINESS_NAME", "Error: please select business name");
define("EMPTY_ADVERT_TITLE", "Error: please enter advert title");
define('SELECT_WHETHER_TO_PUBLISH', "Error: please check whether to publish advert");
define('SELECT_PERIOD', "Error: please select advert running period!");
define('EMPTY_ADVERT_DETAILS', "Error: please enter advert details");
define("ADVERT_DETAILS_800_CHAR", "Error: ONLY 800 characters allowed for advert details");
define('FAILED_TO_SAVE_ADVERT', 'Error: failed to save advert!');
define('NEW_ADVERT_CREATED', "Info: you have successfully created a new advert!");
define("PRIMARY_BANNER_NOT_SELECTED", "Error: primary banner not selected!");
define("DISPLAY_BANNER_YES_NO", "Error: please select whether you have a banner to upload");

// --- >> profile << --- //
define("PROFILE_LOADING_ERROR", "Failed to load your profile; please try again later!");
define("PROFILE_UPDATED_SUCCESSFULLY", "Info: your profile was successfully updated!");
define("FAILED_TO_UPDATE_PROFILE", "Error: failed to update your profile!");
define("ENTER_YOUR_FULL_NAMES", "Error: please enter your full names!");
define("PLEASE_SELECT_GENDER", "Error: please select your gender!");
define("PLEASE_SELECT_NATIONALITY", "Error: please select your nationality!");
define("PLEASE_ENTER_TEL", "Error: please enter your telephone contact!");
define("PLEASE_ENTER_PROFESSION", "Error: please enter your profession!");
define("LONG_FULL_NAMES", "Error: your names should not exceed 25 characters!");
define("EMPTY_CURRENT_LOCATION", "Error: please enter your current location!");
define("LONG_PHONE_NUMBER", "Error: phone number should not exceed 13 characters!");
define("SHORT_PHONE_NUMBER", "Error: phone number should not be less than 10 characters!");
define("INVALID_CURRENT_PASSWORD", "Error: wrong current password entered!");
define("PASSSWORD_SUCCESSFUL_UPDATED", "Info: your password was successfully updated.");
define("FAILED_TO_UPDATE_PASSWORD", "Error: failed to update your current password!");
define("ACCEPT_AGREEMENT", "Error: please tick on radio button to agree to our Terms and Conditions before deletion");
define("JOB_DELETED", "Info: your job was successfully deleted!");
define("FAILED_DELETE_JOB","Error: failed to delete job!");
define("JOB_INFORMATION_UPDATED", "Info: your job details was successfully updated!");
define("FAILED_JOB_UPDATE", "Error: failed to updated your job details!");
define("NO_LIKES_YET", "Info: you are yet to like any advert");
define("ADVERT_DELETED", "Info: your advert was deleted");
define("ADVERT_DELETION_FAILED", "Error: failed to delete advert; please try again later");
define("NO_ADVERTS_CREATED_YET", "Info: no adverts created yet!");
define("NO_JOBS_CREATED_YET", "Info: you havent created any job yet!");
define("FAILED_TO_LOAD_ADVERT_FOR_EDITING", "Error: failed to load advert for editing; please try again!");
define("ADVERT_DETAILS_UPDATED", "Info: your advert details was successfully updated!");
define("FAILED_TO_UPDATE_ADVERT_DETAILS", "Error: failed to update advert details");
define("NO_FAVORITES_ADVERTS_AVAILABLE", "Info: no favorite adverts available");

// --- >> general << --- //
define('FILE_MAX_SIZE', "Error: maximum file size should not exceed 1 Mb");
define('IMAGES_ONLY', 'Error: invalid file format selected; only jpg, jpeg, gif and png allowed');
define("FILE_ALREADY_EXISTS", "Error: file name already exists; please consider renaming it!");
define("INVALID_FILE_MIME", "Error: File is not an image!");
define("EMPTY_CURRENT_PASSWORD", "Error: please enter current password");
define("EMPTY_NEW_PASSWORD", "Error: please enter your new password");
define("EMPTY_CONFIRM_PASSWORD", "Error: please confirm your new password");
define("INTERNAL_ERROR", "Internal server error; please try again later!");
define('PROFILE_MAX_SIZE', "Error: maximum file size should not exceed 500 kb");
define('PROFILE_INVALID_FORMAT', "Error: invalid file format selected; only jpg, jpeg and png allowed");

// --- >> admin << ---- //

// login
define("EMPTY_PRIVILEDGE", "Error: please select your priviledge");
define("EMPTY_ADMIN_PWD", "Error: please enter your password");
define("WRONG_PRIVILEDGE", "Error: priviledge selected does not match credentials entered!");






