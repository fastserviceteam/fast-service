<?php
    /* -------------------------------------------------- */
    /* require file for directory files and method calls  & ~E_WARNING
    /* -------------------------------------------------- */
   

    error_reporting(E_ALL & ~E_NOTICE);
    
    require "_modules/config/_config.php";      // --- >> database globals

    require "_modules/libraries/password_compatibility_library.php"; // --- >> password library for encrypting passwords

    use PHPMailer\PHPMailer\Exception; // --- >> phpmailer exception handling

	require_once '_modules/classes/PHPMailer/vendor/autoload.php'; // --- >> Load Composer's autoloader

    require "_modules/translations/_en.php";    // --- >> feedback definitions

    require "_modules/classes/_globalClass.php"; // --- >> main class

   // include "_modules/classes/_paginationClass.php"; // --- pagination class

  