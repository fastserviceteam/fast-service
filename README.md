#   fast-service-project

## Changelog ##

### Update - 26th Thursday September, 2019

### General
* Alterations done to database; Please check 'database\' directory for **fix.sql** file and import.

* Small styling done to welcome page.

### Client Section
* Added **Job Category** option when creating a new job.

* Push notification permission added. Implementation will be added in next update.


### Update - 13th Friday September, 2019

### General

* Alterations done to database; Please check 'database\' directory for **Q4.sql** file and import.

* Home page updated;
	- Features, home section removed.
	- Contacts updated
	- Login form made to appear first.
	- Password reset and contact us server codes added. If system is uploaded online, please update ***config.php*** under ***"_modules/config/"***
```php
// replace https://fastservice.co.ug with right website url
define("FF_URL","https://fastservice.co.ug");
```

* Use ***"_assets\media\"*** directory for media files. Media files are supposed to be in .mp4 or .ogg format. You can also link from ***youtube***.



### Update - 10th September, 2019

### General

* Alterations done to database; Please check 'database\' directory for **Q2.sql** file and import.

* Home page redesigned using bootstrap 4.3. Please refer to [getbootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction) 
  for documentation.

* New images / banners will be required for the header, features and footer sections.


### update - 7th September, 2019

### General

* Alteration(s) done to database. Please check 'database/' directory for **Q1.sql** file and import.

### Client side

* Profile page update; Editing of adverts / jobs created by client removed (This will be added to admin side such that he/she can be performing the editing on client request).

* Client will only edit his/her basic profile.

* Buggy "clicking twice to logout" issue fixed.

* Database scheduling added to check whether adverts or jobs have expired (reached their running period) or their running period has started 
  after having already been approved by the admin.


### update - 1st September, 2019

### Admin Side

* Fixed login issue from previous update. This was due to using old datatables plugin.


### update - 31st August, 2019

### General

**IMPORTANT**: If your database is up-to-date / ok, please check "database/" directory for **partial_changes.sql** file which contains new updates/changes and import; Do not import **full_updated_database.sql**.

**IMPORTANT**: Else if you're having issues with your database, please drop your current database and import **full_updated_database.sql**. Do not import **partial_changes.sql**.

### Admin Side

* Job Management page completed
* Panel jquery sliding effect removed for panel-footer class. This effect was removed to accommodate job list display effect under Job Management where the ```panel-body``` can be hidden or shown using jQuery slideToggle except the ```panel-footer```. If you want to use it in any other panels, just add ```panel-footerc``` like so.
```html
<div class='panel-footer panel-footerc'>
	<!-- your code --->
</div>
```
* Reports page added; (check a_reports.php). Also navigate to "Reports" link on the left navigation-bar.
  - Jobs reports tab added with excel export; Parameters are:- **date range** and **all/selected business**.
  - Adverts reports tab added with excel export; Parameters are:- **date range** and **all/selected business**.
  - Jobs / Adverts with or without any business can be classified under **All businesses**.
* Comments added to **_adminClass.php** for easy understanding of methods.

### Client Side

* WYSIWYG added for job qualifications
* Comments added to **_globalClass.php** for easy understanding of methods
* Removed unclosed ```font``` tag on left navigation menu which was causing texts to be invisible (white color).
* Transfered some inline css properties to an external css file, (custom.css) and done some design updates.


### update - 27th August, 2019

### General
* Alterations done to database; please check "database/" directory for **q2.sql** file and import.

### Admin Side

* Removed onclick feature to load / navigate to pages; Pages are loaded via href as shown below.
```php
// removed this
<a href="javascript:void(0)" onclick="loadTemplateView('page-to-load')">Page-Name</a>
// replaced with this
<a href="page-to-load.php">Page-Name</a>
```
This was to accommodate jquery plugins **(re)initialization** on page load.

* Advert Management page completed with:- search, filter and order, trends.

### Client Side

* Small PHP mySql fixes.


## update - 15th August, 2019

### General

* Alterations done to database; please check "database/" directory for "q1.sql" file and import.
* Please avoid using the <font> tag as it is deprecated in Html5; Declare a class or id attribute and make use of the "fastStyles.css" in admin or "style.css" in client.

### Client Side

* Fixed missing words/text issue especially under "create job, create advert, etc" pages;
* Update done on messages page.
* Fixed right-side adverts and jobs display tab issue.

### Admin Side

* Advert cancelling option added on Alerts notification navbar
* Messages notification navbar updated and Messages/Inbox page updated
* Little css styling done on left navigation menu.
* Fixed tags with missing closing tags or opening tags eg. <h3>Hello world</b></h3> 
* Notification counter added for jobs, adverts and messages on left navigation menu.


#	update - 9th August, 2019
	## Change-log
	1.	Alter to some tables; please check "database/" for newQuery.sql file and import.
	
	2.	Advert approval option added in admin side; A notification email is sent locally to client side.
	
	3.	Viewing of advert details added; Canceling of an advert code not yet added;
	

# 	update - 1st August, 2019
	-	Added admin section based on original template; to access, use 
```
	Url: 		localhost/fast-service-folder-name/admin/
	Email: 		admin25@gmail.com
	Password: 	12345678
```
	-	Updated database with new table. Please import "admin.sql" file under "database/" directory.

	- 	$_SESSION['fastService_fadminEmail_Session'] - full email address ie. admin25@gmail.com

	- 	$_SESSION['fastService_uadminEmail_Session'] - truncated email address ie. admin25

	-	$_SESSION["fastService_adminPriviledge"] - priviledge; either Admin or Guest - Limiting access to some pages / sections to be added in future.

	## Directory Structure

		1.	_assets - javascript, css, images and plugins.

		2.	_includes - small html/php files which can be included in larger files.

		3. 	_uploads - admin uploads.

	## Important File(s)

		1. 	_admin-config.php - custom definitions, require database, functions and classes files.

		2. 	_admin-requests.php - POST / GET requests.

		3. 	_adminClass.php - this is found outside the admin directory under "_modules/classes/". Create methods here and use "	_admin-requests.php" file to perform POST OR GET requests

		4. 	index.php - checks login session status.

		5. 	login.php - login page. login and remember me works; "forpassword" part not added yet.

		6. 	a_dashboard.php - this is the main page having a default view for "a_home.php"; other pages are loaded based on "onClick" events (refer to "fastservice-function-calls.js" ).
```
onclick='loadTemplateView("page_to_load")'

```

		7. 	fastStyles.css - declare your custom css here.

		8. 	admin-theme.css and default.css - original template css files; you can make modifications to already existing template css.

		9. 	fastservice-function-calls.js - please use this for ONLY function calls unless otherwise. All original and new functions are found in this file.
```
function myFunction()
{
	// perform task
}
```
		10. fastservice-script.js - you can perform $.jquery calls here	
```
$("#somId .someClass").on("click", function(e)
{
	// perform task
});
```

#   Update  - 12 July, 2019
	
	- Added "profile" section; check navigation menu to your left.

	- Updated adverts and jobs pages

	- Fixed "clicking logout twice in order to signout" issue.

	- Updated some tables; Refer to "database/" directory for update.sql file; Please run the following query before importing;
```

DROP TABLE `advert_likes`, `advert_posts`, `advert_views`, `jobs`, `job_responsibility`, `likes`, `user_profile`;

```

#	Inside Client Directory			

## 	--- Files in the "_partials/" directory include:-

	1. _bottomScripts.php - has javascript plugins and other custom js files; if there is a plugin which appears in all pages; place them here.

	2. _leftColumnNavigationMenu.php - this is the menu appearing on your left containing the links;

	3. _modalViews.php - use this file to include your modals elements

	4. _notification.php - stevo's message4 file renamed to this

	5. _rightColumnAdvertsJobs.php - the right adverts and jobs tabs

	6. _topCss.php - has css files for plugins and other custom css files; if there is a css which appears in all pages; place them here.

### --- Creating New pages

	1. If you intend to create a new page; please place them in the "_pages/" directory;

	2. Then give it an appropriate call in the "_leftColumnNavigationMenu.php" file found in "_partials/" directory.

	3. After which open the "_navigation.php" file in "client/" directory and define it in the switch statement

### --- Do not include css template files from other website templates as they will affect the UI of the current one.

### --- Please define plugins configurations appropriately; 

	--- If you intend to use a plugin in one page, then define its css / js there, then perform its initialization on that same page.

	--- Plugins are placed in the "_assets/plugins/" directory.

	--- custom css and js files are placed in the "_assets/css/" and "_assets/js/" directory respectively.

	--- while images are placed in "_assets/images/" directory

	--- stevo's ajax folder was transfered to "_assets/" directory; NB: i don't know its' purpose

	--- The "clientUploads" folder under "_assets/" is for storing advert banners uploaded by client; so please don't delete it.

	--- If you create a new file for the purpose of writing only PHP codes; include this at the top:-
	require "../_require-file.php"; // database config, server feedbacks, phpmailer class and other global constants
	require "../_server-functions.php"; // custom functions

	-- the first file has my definitions
	-- second file has stevo's function calls; it would be great if you could make use of classes
```
eg	
class MyClassName {

// your function
public / private function functionName(some_arguments) {
// your code

}		
}
```

	--- there is a class under "_modules/classes/" directory outside the client directory called "_globalClass.php"; you can make use of it.

	--- Please consider naming your while starting with an under-score (if it handles ONLY PHP codes), eg. _myphpfile; 
	such that they can be differentiated from html+php files.

	--- stevo created the following files which if possible, he should please merge in to at least 1 or 2 and rename them starting with under-score
	1. final.php
	2. final_step.php
	3. livesearch.php
	4. real_time.php
	5. real_time2.php
	6. real_time3.php
	7. real_time4.php
	8. real_time5.php
	6. real_time_all.php

	-- You can also if you want to, use the "_server_requests.php" file under "client/" to perform any other $_POST requests and "page_script.js" files 
	under "client/_assets/js" to perform javascript or jquery requests.

	--- If you also want to use method calls to a class; then put your;
	if(isset($_POST['some_value'])) {} in "_server_requests.php" and use the "_globalClass.php" file in "_modules/classes/" outside the client directory
	to write your method code.	

# 	Outside Client Directory

	--- I renamed stevo's "connect.php" file to "_server-functions.php files" and edited it a little;

	--- the database file is inside the "_modules/config/" directory.

	--- you can define feedbacks to the user under the "_modules/translations/" directory in the "en.php" file.

	--- create your own class in "_modules/classes/" directory; then require it in "_require-file.php" file.

	--- the "_method-file.php" file is for your $_POST requests outside of the client folder