-- ALTER TABLE `advert_posts` ADD `Business_Name` VARCHAR(180) NULL DEFAULT 'N/A' COMMENT 'Business name associ with advert' AFTER `cancelled`;
ALTER TABLE `jobs` ADD `job_serial` VARCHAR(120) NULL DEFAULT NULL COMMENT 'job serial' AFTER `approved`;
ALTER TABLE `job_responsibility` ADD `job_serial` VARCHAR(120) NULL DEFAULT NULL COMMENT 'job serial' AFTER `responsibility`;
ALTER TABLE `jobs` ADD `Business_Name` VARCHAR(150) NOT NULL DEFAULT 'na' COMMENT 'business name' AFTER `job_serial`;
ALTER TABLE `jobs` CHANGE `Business_Name` `Business_Name` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'n\\a' COMMENT 'business name';
ALTER TABLE `jobs` CHANGE `deadline` `deadline` DATE NOT NULL COMMENT 'deadline';
ALTER TABLE `advert_posts` CHANGE `status` `status` TINYINT(2) NULL DEFAULT NULL COMMENT 'running-1; ended/not yet-0';
