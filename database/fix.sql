ALTER TABLE `ur_login` CHANGE `reset_password_timestamp` `reset_password_timestamp` DATETIME NOT NULL COMMENT 'the reset password link period';

ALTER TABLE `ur_login` CHANGE `reset_password_timestamp` `reset_password_timestamp` DATETIME NULL DEFAULT NULL COMMENT 'the reset password link period';

DROP TABLE IF EXISTS `job_category`;
CREATE TABLE IF NOT EXISTS `job_category` (
  `jcId` int(11) NOT NULL AUTO_INCREMENT,
  `jcName` varchar(120) NOT NULL,
  PRIMARY KEY (`jcId`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COMMENT='stores job categories';

--
-- Dumping data for table `job_category`
--

INSERT INTO `job_category` (`jcId`, `jcName`) VALUES
(1, 'Accounting, Auditing &amp; Finance'),
(2, 'Administrative &amp; Office'),
(3, 'Agriculture &amp; Farming'),
(4, 'Building &amp; Architecture'),
(5, 'Community &amp; Social Services'),
(6, 'Consulting &amp; Strategy'),
(7, 'Creative &amp; Design'),
(8, 'Customer Service &amp; Support'),
(9, 'Engineering'),
(10, 'Food Services &amp; Catering'),
(11, 'Health &amp; Safety'),
(12, 'Hospitality/Leisure/Travel'),
(13, 'Human Resources'),
(14, 'IT &amp; Software'),
(15, 'Legal Services'),
(16, 'Management &amp; Business Development'),
(17, 'Marketing &amp; Communications'),
(18, 'Medical &amp; Pharmaceutical'),
(19, 'Project &amp; Product Management'),
(20, 'Quality Control &amp; Assurance'),
(21, 'Real Estate &amp; Property Management'),
(22, 'Research, Teaching &amp; Training'),
(23, 'Sales &amp; Marketing'),
(24, 'Supply Chain &amp; Procurement'),
(25, 'Transport &amp; Logistics'),
(26, 'Security');

ALTER TABLE `jobs` ADD `job_category` VARCHAR(45) NOT NULL COMMENT 'job category string token' AFTER `business_profile_id`;

ALTER TABLE `job_category` ADD `jcToken` VARCHAR(45) NOT NULL COMMENT 'job category string token' AFTER `jcName`;

UPDATE `job_category` SET `jcToken` = '744705c6884269745d6567fa018e2da2ddf2fcce' WHERE `job_category`.`jcId` = 1;
UPDATE `job_category` SET `jcToken` = '79a961aadb2b26fb25de8004546e81572f276a1d' WHERE `job_category`.`jcId` = 2;
UPDATE `job_category` SET `jcToken` = 'e24ea89fed6cbb10e59c6948882937859135143b' WHERE `job_category`.`jcId` = 3;
UPDATE `job_category` SET `jcToken` = 'd88a9b12fce478ed4e2f100c7cb1e2a17f4b8cd9' WHERE `job_category`.`jcId` = 4;
UPDATE `job_category` SET `jcToken` = '0cf3a3998562677268dce1699f464cf0100e44af' WHERE `job_category`.`jcId` = 5;
UPDATE `job_category` SET `jcToken` = 'd277b9847952f9fe5a8a5b9b73eb7c3c2e53971f' WHERE `job_category`.`jcId` = 6;
UPDATE `job_category` SET `jcToken` = 'd2e6a1598fb825857a417ca410dd6aa82716a153' WHERE `job_category`.`jcId` = 7;
UPDATE `job_category` SET `jcToken` = 'a83703eb464652eafc35a248fdb91756f545055d' WHERE `job_category`.`jcId` = 8;
UPDATE `job_category` SET `jcToken` = '4e6a72c58de3b251f8887e363778484ceb3bbb5a' WHERE `job_category`.`jcId` = 9;
UPDATE `job_category` SET `jcToken` = '0dd2cc04beb7915487728e7fc6bae169e88fdfb7' WHERE `job_category`.`jcId` = 10;
UPDATE `job_category` SET `jcToken` = 'fbefb5d58445f64a5f16e6880af9a914770fddd4' WHERE `job_category`.`jcId` = 11;
UPDATE `job_category` SET `jcToken` = '7b42771b82e127fff9d28c28f7b9df01e4cc8a5b' WHERE `job_category`.`jcId` = 12;
UPDATE `job_category` SET `jcToken` = 'ae5efd1fbf33a68e0d1a0ff9bdeb029a642604fa' WHERE `job_category`.`jcId` = 13;
UPDATE `job_category` SET `jcToken` = 'ae40688b48ee61001df4b4700cd224dcdea6bea7' WHERE `job_category`.`jcId` = 14;
UPDATE `job_category` SET `jcToken` = '71b1372c25667d8bc08233545224e54be10dded6' WHERE `job_category`.`jcId` = 15;
UPDATE `job_category` SET `jcToken` = '7dcba6558045af99a66a0305e25cfbb15d567213' WHERE `job_category`.`jcId` = 16;
UPDATE `job_category` SET `jcToken` = '0989e4e6e64fd2fce8f536e089c514c70c2004cc' WHERE `job_category`.`jcId` = 17;
UPDATE `job_category` SET `jcToken` = 'f74ccb7315cc7c4f4955102ac4dc3f6b0878715e' WHERE `job_category`.`jcId` = 18;
UPDATE `job_category` SET `jcToken` = '6d1b042e0edfc43b63dd433f967c14482f619f2d' WHERE `job_category`.`jcId` = 19;
UPDATE `job_category` SET `jcToken` = 'ce9fa5b137cf080e7563933c0ae5bbdcd0dd49a3' WHERE `job_category`.`jcId` = 20;
UPDATE `job_category` SET `jcToken` = 'fa3a597e39f89a739a415fcfe54e88a0ef10067a' WHERE `job_category`.`jcId` = 21;
UPDATE `job_category` SET `jcToken` = '53cc462a48726058b5a18bb3c4ab11f65b4e4bb4' WHERE `job_category`.`jcId` = 22;
UPDATE `job_category` SET `jcToken` = 'c602bf4e8702a0c4223c14b4c20a62c713bd58e2' WHERE `job_category`.`jcId` = 23;
UPDATE `job_category` SET `jcToken` = '5975a54b0deb0e208a80c833d1f9ab810ff65e2d' WHERE `job_category`.`jcId` = 24;
UPDATE `job_category` SET `jcToken` = 'e99d91b8104ca914ca7fa89fc9831a71de89462f' WHERE `job_category`.`jcId` = 25;
UPDATE `job_category` SET `jcToken` = 'e283aeed3e893ba786dca2f4f1afe550a0aea6f0' WHERE `job_category`.`jcId` = 26;

ALTER TABLE `jobs` CHANGE `jc` `jcToken` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'job category string token';

ALTER TABLE `jobs` ADD `jcName` VARCHAR(120) NOT NULL COMMENT 'job category name' AFTER `jcToken`;