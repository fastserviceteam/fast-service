-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 20, 2019 at 10:55 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bcd`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

DROP TABLE IF EXISTS `admin_login`;
CREATE TABLE IF NOT EXISTS `admin_login` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'login tbl primary key',
  `admin_email` varchar(64) NOT NULL COMMENT 'admin login email',
  `admin_priviledge` varchar(25) NOT NULL COMMENT 'admin login access level',
  `admin_password` varchar(150) NOT NULL COMMENT 'admin login password',
  `remember_token` varchar(120) DEFAULT NULL COMMENT 'cookie token',
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `admin_email` (`admin_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='fast service admin login credentials';

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `admin_email`, `admin_priviledge`, `admin_password`, `remember_token`) VALUES
(1, 'admin25@gmail.com', 'Admin', '$2y$10$6NSuXqiKw/uPUglAIsZdRObVVDK4rjXHrtkviGLsqWfKKMJYC9Bt6', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `advert_comments`
--

DROP TABLE IF EXISTS `advert_comments`;
CREATE TABLE IF NOT EXISTS `advert_comments` (
  `commentid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'advert comments primary key',
  `post_id` int(11) NOT NULL COMMENT 'foreign key from advert_post tbl',
  `client_id` varchar(25) NOT NULL COMMENT 'commentor identification',
  `comments` varchar(150) NOT NULL COMMENT 'comments being posted; max 150 characters',
  PRIMARY KEY (`commentid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='manages comments between parties';

-- --------------------------------------------------------

--
-- Table structure for table `advert_likes`
--

DROP TABLE IF EXISTS `advert_likes`;
CREATE TABLE IF NOT EXISTS `advert_likes` (
  `advert_like_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'advert stats tbl primary',
  `advert_post_id` int(11) NOT NULL COMMENT 'posted advert foreign key',
  `number_likes` int(11) NOT NULL DEFAULT '0' COMMENT 'number of likes',
  `liked_by_email` varchar(64) NOT NULL COMMENT 'email of person who liked advert',
  PRIMARY KEY (`advert_like_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='stores advert likes stats';

-- --------------------------------------------------------

--
-- Table structure for table `advert_posts`
--

DROP TABLE IF EXISTS `advert_posts`;
CREATE TABLE IF NOT EXISTS `advert_posts` (
  `advert_post_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'adverts primary key',
  `business_profile_id` int(11) DEFAULT NULL COMMENT 'business foreign key',
  `advert_title` varchar(100) NOT NULL COMMENT 'advert title',
  `date_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'current date time of creating banner',
  `start_period` date NOT NULL COMMENT 'advert start period',
  `end_period` date NOT NULL COMMENT 'advert end period',
  `allow_comments` varchar(3) DEFAULT NULL COMMENT 'yes or no allow comments',
  `publish` varchar(3) DEFAULT NULL COMMENT 'publish or not',
  `description` longtext NOT NULL COMMENT 'advert descriptions',
  `primary_banner_1` varchar(100) DEFAULT NULL COMMENT 'primary advert banner',
  `s_banner_2` varchar(100) DEFAULT NULL COMMENT 'secondary banner 2',
  `s_banner_3` varchar(100) DEFAULT NULL COMMENT 'secondary banner 3',
  `loggedEmail` varchar(64) NOT NULL COMMENT 'currently logged in user',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'not yet - 0; running-1; 2 - expired',
  `approved` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'approval from admin',
  `advert_serial` varchar(50) NOT NULL COMMENT 'advert serial / identifier',
  `cancelled` varchar(4) NOT NULL DEFAULT 'no' COMMENT 'advert cancelled or not',
  `Business_Name` varchar(180) DEFAULT 'N/A' COMMENT 'Business name associ with advert',
  PRIMARY KEY (`advert_post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='stores adverts information';

--
-- Dumping data for table `advert_posts`
--

INSERT INTO `advert_posts` (`advert_post_id`, `business_profile_id`, `advert_title`, `date_post`, `start_period`, `end_period`, `allow_comments`, `publish`, `description`, `primary_banner_1`, `s_banner_2`, `s_banner_3`, `loggedEmail`, `status`, `approved`, `advert_serial`, `cancelled`, `Business_Name`) VALUES
(1, 3, 'Used Computers for Sale', '2019-08-12 06:25:57', '2019-09-06', '2019-09-30', NULL, 'yes', '&lt;p&gt;\r\nUt enim ad minima veniam, quis nostrum &lt;b&gt;exercitationem&lt;/b&gt; ullam corporis \r\n							suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur\r\n							quis nostrum exercitationem ullam corporis suscipit laboriosam.\r\n\r\n&lt;br&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;\r\nAenean ac leo eget nunc fringilla fringilla a non nulla!&lt;/li&gt;&lt;li&gt;Nunc orci mi, \r\nvenenatis quis ultrices vitae, congue non nibh.&lt;/li&gt;&lt;li&gt;Nulla bibendum justo \r\neget. &lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;\r\nTemporibus autem quibusdam et aut officiis \r\n		debitis aut rerum necessitatibus saepe eveniet\r\n\r\n.&lt;br&gt;&lt;/p&gt;&lt;br&gt;', 'project.jpg', 'project-1.jpg', 'project-2.jpg', 'ruperic25@gmail.com', 1, 'yes', 'addb2a1632c432593c146e9a123ffbcf2811554e3c', 'no', 'Real tech co ltd'),
(2, 2, 'Hard Drives 160 Gb', '2019-08-14 08:10:49', '2019-08-17', '2019-08-26', NULL, 'no', '&lt;p&gt;\r\nRaw denim you probably haven\'t heard of them jean shorts Austin. \r\nNesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache \r\ncliche tempor, williamsburg carles vegan helvetica. &lt;br&gt;&lt;/p&gt;&lt;p&gt;Reprehenderit \r\nbutcher retro keffiyeh dreamcatcher\r\n                            synth. Cosby sweater eu banh mi, qui irure \r\nterr.&lt;br&gt;&lt;/p&gt;', NULL, NULL, NULL, 'ruperic25@gmail.com', 2, 'yes', 'ad751f654b047c44bede34b4d1895fb5179eaed8c7', 'no', 'Ntungamo Computer Center'),
(3, 3, 'Softwares and office applications', '2019-07-10 06:25:57', '2019-08-01', '2019-08-25', NULL, 'no', '&lt;p&gt;\r\nUt enim ad minima veniam, quis nostrum &lt;b&gt;exercitationem&lt;/b&gt; ullam corporis \r\n							suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur\r\n							quis nostrum exercitationem ullam corporis suscipit laboriosam.\r\n\r\n&lt;br&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;\r\nAenean ac leo eget nunc fringilla fringilla a non nulla!&lt;/li&gt;&lt;li&gt;Nunc orci mi, \r\nvenenatis quis ultrices vitae, congue non nibh.&lt;/li&gt;&lt;li&gt;Nulla bibendum justo \r\neget. &lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;\r\nTemporibus autem quibusdam et aut officiis \r\n		debitis aut rerum necessitatibus saepe eveniet\r\n\r\n.&lt;br&gt;&lt;/p&gt;&lt;br&gt;', 'project-2.jpg', NULL, NULL, 'ruperic25@gmail.com', 2, 'no', '2affcd2e6321c75ef4de74574de23893c43fec4f ', 'no', 'Real tech co ltd'),
(4, 2, 'Computer Maintenance', '2019-05-10 08:10:49', '2019-05-17', '2019-05-26', NULL, 'no', '&lt;p&gt;\r\nRaw denim you probably haven\'t heard of them jean shorts Austin. \r\nNesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache \r\ncliche tempor, williamsburg carles vegan helvetica. &lt;br&gt;&lt;/p&gt;&lt;p&gt;Reprehenderit \r\nbutcher retro keffiyeh dreamcatcher\r\n                            synth. Cosby sweater eu banh mi, qui irure \r\nterr.&lt;br&gt;&lt;/p&gt;', NULL, NULL, NULL, 'ruperic25@gmail.com', 2, 'no', '781d859c4d3251aafb0f2c7a38b4f4006df34a23 ', 'no', 'Ntungamo Computer Center'),
(5, 3, 'Stationary', '2019-04-10 06:25:57', '2019-05-01', '2019-05-25', NULL, 'no', '&lt;p&gt;\r\nUt enim ad minima veniam, quis nostrum &lt;b&gt;exercitationem&lt;/b&gt; ullam corporis \r\n							suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur\r\n							quis nostrum exercitationem ullam corporis suscipit laboriosam.\r\n\r\n&lt;br&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;\r\nAenean ac leo eget nunc fringilla fringilla a non nulla!&lt;/li&gt;&lt;li&gt;Nunc orci mi, \r\nvenenatis quis ultrices vitae, congue non nibh.&lt;/li&gt;&lt;li&gt;Nulla bibendum justo \r\neget. &lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;\r\nTemporibus autem quibusdam et aut officiis \r\n		debitis aut rerum necessitatibus saepe eveniet\r\n\r\n.&lt;br&gt;&lt;/p&gt;&lt;br&gt;', NULL, NULL, NULL, 'ruperic25@gmail.com', 2, 'no', '49dcf8f75ed766c4d6b4fbc72ff5bfdc1275e52d ', 'no', 'Real tech co ltd'),
(6, 1, 'Consultation', '2019-04-10 08:10:49', '2019-05-02', '2019-05-21', NULL, 'no', '&lt;p&gt;\r\nRaw denim you probably haven\'t heard of them jean shorts Austin. \r\nNesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache \r\ncliche tempor, williamsburg carles vegan helvetica. &lt;br&gt;&lt;/p&gt;&lt;p&gt;Reprehenderit \r\nbutcher retro keffiyeh dreamcatcher\r\n                            synth. Cosby sweater eu banh mi, qui irure \r\nterr.&lt;br&gt;&lt;/p&gt;', NULL, NULL, NULL, 'ruperic25@gmail.com', 2, 'no', '8c73c33fb4e0baa78258bf1056ee2bc4b87ec762 ', 'no', 'N/A');

-- --------------------------------------------------------

--
-- Table structure for table `advert_views`
--

DROP TABLE IF EXISTS `advert_views`;
CREATE TABLE IF NOT EXISTS `advert_views` (
  `advert_view_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'advert views primary key',
  `advert_post_id` int(11) NOT NULL COMMENT 'advert_post foreign key',
  `advert_view_numbers` int(11) NOT NULL COMMENT 'number of views',
  PRIMARY KEY (`advert_view_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='stores advert views stats';

--
-- Dumping data for table `advert_views`
--

INSERT INTO `advert_views` (`advert_view_id`, `advert_post_id`, `advert_view_numbers`) VALUES
(1, 1, 5),
(2, 2, 2),
(3, 5, 5),
(4, 4, 6);

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

DROP TABLE IF EXISTS `billing`;
CREATE TABLE IF NOT EXISTS `billing` (
  `bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(30) NOT NULL,
  `billing_type` varchar(25) NOT NULL,
  `min_value` varchar(10) NOT NULL,
  `max_value` varchar(25) NOT NULL,
  `last_edit` varchar(20) NOT NULL,
  `amount` varchar(10) NOT NULL,
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`bill_id`, `item_name`, `billing_type`, `min_value`, `max_value`, `last_edit`, `amount`) VALUES
(4, 'business_subscription', 'Monthly Income', '60000', '120000', '2019-01-01', '2000'),
(5, 'business_subscription', 'Monthly Income', '120001', '240000', '2019-01-01', '4000'),
(6, 'business_subscription', 'Monthly Income', '240001', '360000', '2019-01-01', '6000'),
(7, 'advert', 'duration', '0', '30', '2019-01-01', '10000'),
(8, 'advert', 'duration', '31', '60', '2019-01-01', '20000'),
(9, 'job', 'duration', '0', '14', '2019-01-01', '2000'),
(10, 'job', 'duration', '15', '21', '2019-01-01', '5000'),
(11, 'business_subscription', 'viewers', '0', '1200', '2019-01-03', '10000');

-- --------------------------------------------------------

--
-- Table structure for table `business_profile`
--

DROP TABLE IF EXISTS `business_profile`;
CREATE TABLE IF NOT EXISTS `business_profile` (
  `business_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `Business_Name` varchar(150) NOT NULL,
  `Business_Address` varchar(50) NOT NULL,
  `Business_Phone` varchar(13) NOT NULL,
  `Business_Email` varchar(64) NOT NULL,
  `Business_Category` varchar(200) NOT NULL,
  `Services_Offered` varchar(500) NOT NULL,
  `Geographical_Area` varchar(200) NOT NULL,
  `vander_latitude` varchar(100) DEFAULT NULL,
  `vander_longitude` varchar(100) DEFAULT NULL,
  `loggedin_email` varchar(64) NOT NULL,
  `date_time_creation` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`business_profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_profile`
--

INSERT INTO `business_profile` (`business_profile_id`, `Business_Name`, `Business_Address`, `Business_Phone`, `Business_Email`, `Business_Category`, `Services_Offered`, `Geographical_Area`, `vander_latitude`, `vander_longitude`, `loggedin_email`, `date_time_creation`) VALUES
(2, 'Ntungamo Computer Center', 'Uganda', '077787843', 'ruperic25@gmail.com', 'Information Technology', 'computer repair, accesories and computer sales', 'Nyungamo', NULL, NULL, '', '2019-04-27 10:04:32'),
(3, 'Real tech co ltd', 'Uganda', '077783434', 'ruperic25@gmail.com', 'Research and Project Analysis', 'research projects and mangement', 'Kampala banda', NULL, NULL, '', '2019-04-27 10:04:32'),
(4, 'wide wild uganda safaris', 'Uganda', '077898989', 'sam@gmail.com', 'Information Technology', 'Tourism, Bird watching, and Zoe touring', 'Kira Town', '0.3972', '32.6387', '', '2019-04-27 10:04:32'),
(5, 'FutureInfo Soultions', 'Uganda', '0785574424', 'timothyaine@gmail.com', 'Farming and agriculture', 'business planning, proposal writingb', 'Off University Road,Kyambogo, Kampala, Uganda', '0.30972', '32.6387', '', '2019-04-27 10:04:32'),
(6, 'At vero eos et accusamus', 'Uganda', '0778996989', 'timothyaine@gmail.com', 'boda', 'mobile money', 'd44', '0.3424525', '32.635459', '', '2019-04-27 10:04:32');

-- --------------------------------------------------------

--
-- Table structure for table `cancelled_adverts`
--

DROP TABLE IF EXISTS `cancelled_adverts`;
CREATE TABLE IF NOT EXISTS `cancelled_adverts` (
  `cad_pkey` int(11) NOT NULL AUTO_INCREMENT COMMENT 'cancelled adverts primary key',
  `advert_post_id` int(11) NOT NULL COMMENT 'foreign key from adverts posted',
  `advert_serial` varchar(60) NOT NULL COMMENT 'foreign key from adverts posted',
  `cad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date and time of cancelling',
  `reason` varchar(150) NOT NULL COMMENT 'reason for advert cancelling',
  PRIMARY KEY (`cad_pkey`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='cancelled adverts';

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(60) NOT NULL,
  `industry_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `industry_id`) VALUES
(2, 'Computer Repair', 2),
(3, 'computer accesssories', 2),
(4, 'Computer training ', 2),
(5, 'Cabbages ', 3),
(6, 'Broilers ', 4),
(7, 'tax', 5),
(8, 'boda', 5);

-- --------------------------------------------------------

--
-- Table structure for table `client_quotation`
--

DROP TABLE IF EXISTS `client_quotation`;
CREATE TABLE IF NOT EXISTS `client_quotation` (
  `client_quotation_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_email` varchar(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `service_address` varchar(200) NOT NULL,
  `service_needed` varchar(200) NOT NULL,
  `area_selected` varchar(200) DEFAULT NULL,
  `quotation_details` varchar(200) NOT NULL,
  `vander_appointed` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=default, 1= pending, 2= accepted, 3= cancelled',
  `vander_quotation` varchar(500) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `vander_latitude` varchar(100) DEFAULT NULL,
  `vander_longitude` varchar(100) DEFAULT NULL,
  `time_taken` varchar(100) DEFAULT NULL,
  `date_delivery` date DEFAULT NULL COMMENT 'delivery date',
  `time_delivery` varchar(20) DEFAULT NULL COMMENT 'delivery time',
  PRIMARY KEY (`client_quotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_quotation`
--

INSERT INTO `client_quotation` (`client_quotation_id`, `client_email`, `location`, `service_address`, `service_needed`, `area_selected`, `quotation_details`, `vander_appointed`, `status`, `vander_quotation`, `longitude`, `latitude`, `vander_latitude`, `vander_longitude`, `time_taken`, `date_delivery`, `time_delivery`) VALUES
(1, 'steven@gmail.com', 'Kampala Uganda', 'Entebe', 'Tourism, Bird watching, and Zoe touring', 'Bwindi', 'i want to tour in entebe as weeknd with my family', 'sam@gmail.com', 1, 'sd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'tom@gmail.com', 'Kampala Uganda', 'kireka', 'research projects and mangement', 'Kampala banda', 'hello sir,\r\ni want you to discuss for me research tools,\r\nwhat is the price range', 'sam@gmail.com', 1, 'full researcch report costs 400,000 UGX and proposal alone costs 250,000 \r\ncall me on 0777872 and we talk more\r\ntthx', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'sam@gmail.com', 'Kampala Uganda', 'gh', 'cooking beans and town katanga', 'katanga hoods', 'ghjghvghvfhghgh', 'tom@gmail.com', 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'sam@gmail.com', 'Kampala Uganda', 'g', 'stealing potatoes', 'Ntungamo district', 'gh', 'tom@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'tom@gmail.com', 'Kampala Uganda', 'dsd', 'computer repair, accesories and computer sales', 'Nyungamo', 'jkj', 'sam@gmail.com', 2, '100 ugx per sark', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'tom@gmail.com', 'Kampala Uganda', 'asa', 'computer repair, accesories and computer sales', 'Nyungamo', 'as', 'sam@gmail.com', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'tom@gmail.com', 'Kampala Uganda', 'banda', 'supplying potatoes and cabbages', 'Ntungamo district', 'One suck of cabbages 100k', 'sam@gmail.com', 2, '100000 per sack', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'tom@gmail.com', 'Kampala Uganda', 'masaka', 'research projects and mangement', 'Kampala banda', 'please ser send mi reasearch', 'sam@gmail.com', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'tom@gmail.com', 'Kampala Uganda', 'Kireka Taxi stage, Kampala, Uganda', 'research projects and mangement', 'Kampala banda', 'sdddfsdfds', 'sam@gmail.com', 2, 'sent  money now on MM', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'tom@gmail.com', 'Kampala Uganda', 'Legacy Towers, Kampala, Uganda', 'Tourism, Bird watching, and Zoe touring', 'Kira Town', 'i want to tour there please', 'sam@gmail.com', 2, 'okey ley us meet tmrw at 7 pm sharp', '32.5781302', '0.3303425', '0.3972', '32.6387', NULL, NULL, NULL),
(12, 'banxie@gmail.com', 'Kampala Uganda', 'Banda Cl, Kampala, Uganda', 'business planning, proposal writing', 'Off University Road,Kyambogo, Kampala, Uganda', 'jfdhgfjhgjh', 'timothyaine@gmail.com', 2, 'ghdfhsgfdjhgjnbkmv nnnnn', '32.6356877', '0.3439505', '0.4972', '32.6387', NULL, NULL, NULL),
(13, 'timothyaine@gmail.com', '11a K.A.R. Rd, Kampala, Uganda', 'Banda Computer Centre, Kampala, Uganda', 'computer repair, accesories and computer sales', 'Nyungamo', 'lorem ipsum', 'sam@gmail.com', 3, NULL, '32.63566019999996', '0.3444754', '0.3972', '32.6387', NULL, NULL, NULL),
(14, 'ruperic25@gmail.com', 'Kampala Uganda', 'ergerger', '', '', 'egergergergergreh', '', 0, NULL, '', '', '', '', NULL, '2019-04-30', '05:00 PM'),
(15, 'ruperic25@gmail.com', 'Namugongo-Jjanda', 'Namugongo-Jjanda', 'mobile money', NULL, 'Fusce semper, nibh eu sollicitudin imperdiet, dolor magna vestibulum mi, vel tincidunt augue ipsum nec erat. Vestibulum congue leo elementum ullamcorper commodo. Class aptent taciti sociosqu ad litora', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-30', '02:21 PM'),
(16, 'ruperic25@gmail.com', 'Banda Kyambogo', 'Banda Kyambogo', 'shoe services', NULL, 'Fusce semper, nibh eu sollicitudin imperdiet, dolor magna vestibulum mi, vel tincidunt augue ipsum nec erat. Vestibulum congue leo elementum ullamcorper commodo. Class aptent taciti sociosqu ad litora', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-30', '02:55 PM');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

DROP TABLE IF EXISTS `districts`;
CREATE TABLE IF NOT EXISTS `districts` (
  `district_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `district_name` varchar(100) NOT NULL COMMENT 'district''s name',
  PRIMARY KEY (`district_id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COMMENT='uganda districts';

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`district_id`, `district_name`) VALUES
(1, 'Abim'),
(2, 'Adjumani'),
(3, 'Agago'),
(4, 'Alebtong'),
(5, 'Amolatar'),
(6, 'Amudat'),
(7, 'Amuria'),
(8, 'Amuru'),
(9, 'Apac'),
(10, 'Arua'),
(11, 'Budaka'),
(12, 'Bududa'),
(13, 'Bugiri'),
(14, 'Buhweju'),
(15, 'Buikwe'),
(16, 'Bukedea'),
(17, 'Bukomansimbi'),
(18, 'Bukwo'),
(19, 'Bulambuli'),
(20, 'Buliisa'),
(21, 'Bundibugyo'),
(22, 'Bushenyi'),
(23, 'Busia'),
(24, 'Butaleja'),
(25, 'Butambala'),
(26, 'Buvuma'),
(27, 'Buyende'),
(28, 'Dokolo'),
(29, 'Gomba'),
(30, 'Gulu'),
(31, 'Hoima'),
(32, 'Ibanda'),
(33, 'Iganga'),
(34, 'Isingiro'),
(35, 'Jinja'),
(36, 'Kaabong'),
(37, 'Kabale'),
(38, 'Kabarole'),
(39, 'Kaberamaido'),
(40, 'Kalangala'),
(41, 'Kaliro'),
(42, 'Kalungu'),
(43, 'Kampala'),
(44, 'Kamuli'),
(45, 'Kamwenge'),
(46, 'Kanungu'),
(47, 'Kapchorwa'),
(48, 'Kasese'),
(49, 'Katakwi'),
(50, 'Kayunga'),
(51, 'Kibaale'),
(52, 'Kiboga'),
(53, 'Kibuku'),
(54, 'Kiruhura'),
(55, 'Kiryandongo'),
(56, 'Kisoro'),
(57, 'Kitgum'),
(58, 'Koboko'),
(59, 'Kole'),
(60, 'Kotido'),
(61, 'Kumi'),
(62, 'Kween'),
(63, 'Kyakwanzi'),
(64, 'Kyegegwa'),
(65, 'Kyenjojo'),
(66, 'Lamwo'),
(67, 'Lira'),
(68, 'Luuka'),
(69, 'Luwero'),
(70, 'Lwengo'),
(71, 'Lyantonde '),
(72, 'Manafwa'),
(73, 'Maracha'),
(74, 'Masaka'),
(75, 'Masindi'),
(76, 'Mayuge'),
(77, 'Mbale'),
(78, 'Mbarara'),
(79, 'Mitooma'),
(80, 'Mityana'),
(81, 'Moroto'),
(82, 'Moyo'),
(83, 'Mpigi'),
(84, 'Mubende'),
(85, 'Mukono'),
(86, 'Nakapiripirit'),
(87, 'Nakaseke'),
(88, 'Nakasongola'),
(89, 'Namayingo'),
(90, 'Namutumba'),
(91, 'Napak'),
(92, 'Nebbi'),
(93, 'Ngora'),
(94, 'Ntoroko'),
(95, 'Ntungamo'),
(96, 'Nwoya'),
(97, 'Otuke'),
(98, 'Oyam'),
(99, 'Pader'),
(100, 'Pallisa'),
(101, 'Rakai'),
(102, 'Rubirizi'),
(103, 'Rukungiri'),
(104, 'Serere'),
(105, 'Sheema'),
(106, 'Sironko'),
(107, 'Soroti'),
(108, 'Ssembabule'),
(109, 'Tororo'),
(110, 'Wakiso'),
(111, 'Yumbe'),
(112, 'Zombo');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
CREATE TABLE IF NOT EXISTS `favorites` (
  `fav_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'favorites primary key',
  `advert_post_id` int(11) DEFAULT '0' COMMENT 'advert post foreign key',
  `job_id` int(11) DEFAULT '0' COMMENT 'job post foreign key',
  `email_address` varchar(64) NOT NULL COMMENT 'email of current session',
  `fav_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 for unfav; 1 for fav',
  PRIMARY KEY (`fav_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='monitors jobs or adverts added to favorites';

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`fav_id`, `advert_post_id`, `job_id`, `email_address`, `fav_status`) VALUES
(10, 0, 3, 'ruperic25@gmail.com', 1),
(8, 6, 0, 'ruperic25@gmail.com', 1),
(11, 0, 2, 'ruperic25@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fs_staff`
--

DROP TABLE IF EXISTS `fs_staff`;
CREATE TABLE IF NOT EXISTS `fs_staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `current_address` varchar(100) DEFAULT NULL,
  `permanent_address` varchar(100) DEFAULT NULL,
  `id_no` varchar(40) DEFAULT NULL,
  `position` varchar(40) DEFAULT NULL,
  `workstation` varchar(50) NOT NULL,
  `ur_email` varchar(50) DEFAULT NULL,
  `image_name` varchar(40) DEFAULT NULL,
  `image` blob NOT NULL,
  `date_reg` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fs_staff`
--

INSERT INTO `fs_staff` (`staff_id`, `fname`, `lname`, `dob`, `phone_no`, `current_address`, `permanent_address`, `id_no`, `position`, `workstation`, `ur_email`, `image_name`, `image`, `date_reg`) VALUES
(4, 'Anthon', 'Turyahikayo', '29-april-1992', '0752334382', 'kireka', 'kabale', 'CXV9008K', 'Multimedi Architect', 'Banda', 'anthonturyahikayo@gmail.com', 'banxie.jpg', 0x75706c6f6164732f31322d30312d323031392d313534373238373935352e6a7067, '2019-01-12'),
(5, 'Stephen', 'Kabeiraho', '21-May-1991', '0704578518', 'kyaliwajala', 'kabale', 'YF005angt', 'Web developer', 'Banda', 'stvo@gmail.com', 'jesus.jpg', 0x75706c6f6164732f31322d30312d323031392d313534373238383038312e6a7067, '2019-01-12');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

DROP TABLE IF EXISTS `industries`;
CREATE TABLE IF NOT EXISTS `industries` (
  `industry_id` int(11) NOT NULL AUTO_INCREMENT,
  `industry_name` varchar(40) NOT NULL,
  PRIMARY KEY (`industry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`industry_id`, `industry_name`) VALUES
(2, 'Information Technology'),
(3, 'Agriculture'),
(4, 'Poultry'),
(5, 'transport'),
(6, 'engineering'),
(7, 'Arts and Design'),
(8, 'Construction');

-- --------------------------------------------------------

--
-- Table structure for table `item_rating`
--

DROP TABLE IF EXISTS `item_rating`;
CREATE TABLE IF NOT EXISTS `item_rating` (
  `ratingId` int(11) NOT NULL AUTO_INCREMENT,
  `itemId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `ratingNumber` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Block, 0 = Unblock',
  PRIMARY KEY (`ratingId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_rating`
--

INSERT INTO `item_rating` (`ratingId`, `itemId`, `userId`, `ratingNumber`, `title`, `comments`, `created`, `modified`, `status`) VALUES
(1, 12345678, 1234567, 1, 'Hello', 'rgregerg', '2019-08-21 10:21:48', '2019-08-21 10:21:48', 1),
(2, 12345678, 1234567, 1, 'ewff', 'ewfwefwe', '2019-08-21 10:23:44', '2019-08-21 10:23:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `have_business` varchar(3) NOT NULL COMMENT 'does user have a business; yes or no',
  `business_profile_id` varchar(11) DEFAULT NULL,
  `job_vacancy` varchar(70) NOT NULL COMMENT 'job title',
  `job_district` varchar(100) NOT NULL COMMENT 'district where job is',
  `location` varchar(100) NOT NULL COMMENT 'duty station',
  `job_type` varchar(15) NOT NULL COMMENT 'full time, part time etc',
  `job_qualification` longtext NOT NULL COMMENT 'qualifications of job',
  `salary` varchar(20) NOT NULL COMMENT 'job salary',
  `application_mode` varchar(200) NOT NULL COMMENT 'how to apply',
  `date_posted` date NOT NULL COMMENT 'date posted',
  `deadline` date NOT NULL COMMENT 'deadline',
  `duration` varchar(75) NOT NULL DEFAULT 'Not Defined' COMMENT 'job duration',
  `loggedin_email` varchar(64) NOT NULL COMMENT 'logged in email entering data',
  `approved` varchar(3) NOT NULL DEFAULT 'no' COMMENT 'admin approval',
  `job_serial` varchar(120) DEFAULT NULL COMMENT 'job serial',
  `Business_Name` varchar(150) NOT NULL DEFAULT 'n\\a' COMMENT 'business name',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 ended; 1 - running; 2 - expired',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `have_business`, `business_profile_id`, `job_vacancy`, `job_district`, `location`, `job_type`, `job_qualification`, `salary`, `application_mode`, `date_posted`, `deadline`, `duration`, `loggedin_email`, `approved`, `job_serial`, `Business_Name`, `status`) VALUES
(1, 'yes', '2', 'Cafe attendant', 'Kampala', 'Kampala', 'Full time', '<ul><li>\r\nLorem ipsum dolor sit amet, <b>consectetur </b>adipiscing elit, sed do eiusmod \r\ntempor incididunt ut labore et dolore magna aliqua.</li><li>Ut enim ad minim \r\nveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\ncommodo consequat. </li><li>                              Duis aute irure dolor in reprehenderit in \r\nvoluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur \r\nsint occaecat cupidatat non proident, sunt in culpa qui officia deserunt\r\n mollit anim id est laborum.</li></ul><br>', 'Confidential', 'If you meet the required qualifications, please send your cover letter, CV with at most 3 referees and academic documents to ntungamucomputers@ymail.com', '2019-09-06', '2019-09-20', 'N/A', 'ruperic25@gmail.com', 'yes', 'job-3cacc4fc52e31b68e7528705428b71456bb5c40f104808', 'Ntungamo Computer Center', 1),
(2, 'yes', '3', 'Marketeer', 'Jinja', 'Lorenzo Plaza', 'Full time', '<ul><li>\r\nRaw denim you probably haven\'t heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master <b>cleanse</b>.</li><li>\r\nMustache cliche tempor, <span class=\"wysiwyg-color-maroon\">williamsburg </span>carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher\r\n                            synth. Cosby sweater eu banh mi, qui irure terr. </li><li>\r\nDuis ante ipsum, malesuada eu risus vitae, mattis dapibus arcu. Nullam \r\nmetus dui, fermentum dictum nulla id, gravida dignissim eros.\r\n\r\n\r\n Nulla suscipit mollis dui vitae porttitor. Nulla faucibus neque leo. </li><li>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer \r\nvolutpat nulla et sollicitudin volutpat. Suspendisse consequat massa et \r\nvarius tincidunt.</li></ul>', '320000', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer volutpat nulla et sollicitudin volutpat. Suspendisse consequat massa et varius tincidunt. In quis velit et enim posuere consectetur.', '2019-08-29', '2019-09-18', 'N/A', 'ruperic25@gmail.com', 'yes', 'job-51f0d5502aed7047dfaf2f475a26a055b9a5cbef105824', 'Real tech co ltd', 2),
(3, 'yes', '3', 'IT Specialist', 'Kampala', 'Kampala Road', 'Part time', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non \r\nimperdiet nisi. Quisque cursus leo et lacus tempus porttitor. Sed \r\negestas laoreet justo non feugiat.</p><p>\r\nLorem ipsum dolor sit amet, <i>consectetur</i> adipiscing elit. Curabitur non \r\nimperdiet nisi. Quisque cursus leo et lacus tempus porttitor. Sed \r\negestas laoreet justo non feugiat.<br></p><br>', '250000', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti at que corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provide', '2019-08-09', '2019-08-31', 'N/A', 'ruperic25@gmail.com', 'no', 'job-1c350af4d626f61aef9ee3357a95080b0209ab35093308', 'Real tech co ltd', 2);

-- --------------------------------------------------------

--
-- Table structure for table `job_responsibility`
--

DROP TABLE IF EXISTS `job_responsibility`;
CREATE TABLE IF NOT EXISTS `job_responsibility` (
  `job_resp_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_profile_id` varchar(11) DEFAULT NULL,
  `job_id` int(11) NOT NULL,
  `responsibility` varchar(150) NOT NULL,
  `job_serial` varchar(120) DEFAULT NULL COMMENT 'job serial',
  PRIMARY KEY (`job_resp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COMMENT='job responsibility table';

--
-- Dumping data for table `job_responsibility`
--

INSERT INTO `job_responsibility` (`job_resp_id`, `business_profile_id`, `job_id`, `responsibility`, `job_serial`) VALUES
(1, '2', 1, 'Riusmod tempor incididunt ut labor erem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.', 'job-3cacc4fc52e31b68e7528705428b71456bb5c40f104808'),
(2, '2', 1, 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.', 'job-3cacc4fc52e31b68e7528705428b71456bb5c40f104808'),
(3, '3', 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer volutpat nulla et sollicitudin volutpat. Suspendisse consequat massa et varius tincid', 'job-51f0d5502aed7047dfaf2f475a26a055b9a5cbef105824'),
(4, '3', 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer volutpat nulla et sollicitudin volutpat. Suspendisse consequat massa et varius.', 'job-51f0d5502aed7047dfaf2f475a26a055b9a5cbef105824'),
(5, '3', 2, 'Nulla suscipit mollis dui vitae porttitor. Nulla faucibus neque leo. Sed tincidunt enim sit amet tellus bibendum consectetur. Nunc lobortis metus', 'job-51f0d5502aed7047dfaf2f475a26a055b9a5cbef105824'),
(6, '3', 2, 'Nulla suscipit mollis dui vitae porttitor. Nulla faucibus neque leo. Sed tincidunt enim sit amet tellus bibendum consectetur. Nunc lobortis metus', 'job-51f0d5502aed7047dfaf2f475a26a055b9a5cbef105824'),
(7, '3', 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'job-1c350af4d626f61aef9ee3357a95080b0209ab35093308'),
(8, '3', 3, 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti at que corrupti quos dolores et quas.', 'job-1c350af4d626f61aef9ee3357a95080b0209ab35093308'),
(9, '3', 3, 'Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'job-1c350af4d626f61aef9ee3357a95080b0209ab35093308'),
(10, '3', 3, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit', 'job-1c350af4d626f61aef9ee3357a95080b0209ab35093308');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(40) NOT NULL,
  `read_efficiency` varchar(40) NOT NULL,
  `write_efficiency` varchar(40) NOT NULL,
  `ur_email` varchar(30) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`language_id`, `language`, `read_efficiency`, `write_efficiency`, `ur_email`) VALUES
(1, 'English', 'Good', 'Good', 'banxie@gmail.com'),
(2, 'English', 'Excellent', 'Excellent', 'timothyaine@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `like_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `ur_email` varchar(50) NOT NULL,
  PRIMARY KEY (`like_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`like_id`, `item_id`, `ur_email`) VALUES
(1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `msg_bin`
--

DROP TABLE IF EXISTS `msg_bin`;
CREATE TABLE IF NOT EXISTS `msg_bin` (
  `bin_id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL COMMENT 'message foreign key',
  `bin_email` varchar(64) NOT NULL,
  `bin_subject` varchar(100) NOT NULL,
  `bin_message` longtext NOT NULL,
  `bin_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `msg_box` varchar(11) NOT NULL,
  `bin_status` varchar(10) NOT NULL DEFAULT 'Deleted',
  `msg_date_time` timestamp NOT NULL,
  `sender_names` varchar(20) DEFAULT NULL,
  `sender_email` varchar(64) NOT NULL,
  `msg_read` int(11) NOT NULL DEFAULT '0' COMMENT '0-unread; 1-read',
  PRIMARY KEY (`bin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msg_inbox`
--

DROP TABLE IF EXISTS `msg_inbox`;
CREATE TABLE IF NOT EXISTS `msg_inbox` (
  `inbox_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'inbox primary key',
  `inbox_email` varchar(64) DEFAULT NULL COMMENT 'to email address',
  `inbox_subject` varchar(100) NOT NULL COMMENT 'message subject',
  `inbox_message` longtext NOT NULL COMMENT 'message message',
  `inbox_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'current date time',
  `inbox_status` varchar(10) NOT NULL DEFAULT 'Received' COMMENT 'message status',
  `sender_names` varchar(50) DEFAULT NULL COMMENT 'from names',
  `sender_email` varchar(64) NOT NULL COMMENT 'from email address',
  `msg_read` int(11) NOT NULL DEFAULT '0' COMMENT '0-unread; 1-read',
  PRIMARY KEY (`inbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msg_outbox`
--

DROP TABLE IF EXISTS `msg_outbox`;
CREATE TABLE IF NOT EXISTS `msg_outbox` (
  `outbox_id` int(11) NOT NULL AUTO_INCREMENT,
  `outbox_email` varchar(64) NOT NULL,
  `outbox_subject` varchar(100) NOT NULL,
  `outbox_message` longtext NOT NULL COMMENT 'outbox message',
  `outbox_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `outbox_status` varchar(4) NOT NULL DEFAULT 'Sent',
  `sender_names` varchar(20) DEFAULT NULL,
  `sender_email` varchar(64) NOT NULL,
  PRIMARY KEY (`outbox_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

DROP TABLE IF EXISTS `nationality`;
CREATE TABLE IF NOT EXISTS `nationality` (
  `code` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='list of countries in the world with initials';

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`code`, `name`) VALUES
('AD', 'Andorra'),
('AE', 'United Arab Emirates'),
('AF', 'Afghanistan'),
('AG', 'Antigua and Barbuda'),
('AI', 'Anguilla'),
('AL', 'Albania'),
('AM', 'Armenia'),
('AO', 'Angola'),
('AQ', 'Antarctica'),
('AR', 'Argentina'),
('AS', 'American Samoa'),
('AT', 'Austria'),
('AU', 'Australia'),
('AW', 'Aruba'),
('AX', 'Åland Islands'),
('AZ', 'Azerbaijan'),
('BA', 'Bosnia and Herzegovina'),
('BB', 'Barbados'),
('BD', 'Bangladesh'),
('BE', 'Belgium'),
('BF', 'Burkina Faso'),
('BG', 'Bulgaria'),
('BH', 'Bahrain'),
('BI', 'Burundi'),
('BJ', 'Benin'),
('BL', 'Saint Barthélemy'),
('BM', 'Bermuda'),
('BN', 'Brunei Darussalam'),
('BO', 'Bolivia'),
('BQ', 'Caribbean Netherlands '),
('BR', 'Brazil'),
('BS', 'Bahamas'),
('BT', 'Bhutan'),
('BV', 'Bouvet Island'),
('BW', 'Botswana'),
('BY', 'Belarus'),
('BZ', 'Belize'),
('CA', 'Canada'),
('CC', 'Cocos (Keeling) Islands'),
('CD', 'Congo, Democratic Republic of'),
('CF', 'Central African Republic'),
('CG', 'Congo'),
('CH', 'Switzerland'),
('CI', 'Côte D’Ivoire'),
('CK', 'Cook Islands'),
('CL', 'Chile'),
('CM', 'Cameroon'),
('CN', 'China'),
('CO', 'Colombia'),
('CR', 'Costa Rica'),
('CU', 'Cuba'),
('CV', 'Cape Verde'),
('CW', 'Curaçao'),
('CX', 'Christmas Island'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DE', 'Germany'),
('DJ', 'Djibouti'),
('DK', 'Denmark'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('DZ', 'Algeria'),
('EC', 'Ecuador'),
('EE', 'Estonia'),
('EG', 'Egypt'),
('EH', 'Western Sahara'),
('ER', 'Eritrea'),
('ES', 'Spain'),
('ET', 'Ethiopia'),
('FI', 'Finland'),
('FJ', 'Fiji'),
('FK', 'Falkland Islands'),
('FM', 'Micronesia, Federated States of'),
('FO', 'Faroe Islands'),
('FR', 'France'),
('GA', 'Gabon'),
('GB', 'United Kingdom'),
('GD', 'Grenada'),
('GE', 'Georgia'),
('GF', 'French Guiana'),
('GG', 'Guernsey'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GL', 'Greenland'),
('GM', 'Gambia'),
('GN', 'Guinea'),
('GP', 'Guadeloupe'),
('GQ', 'Equatorial Guinea'),
('GR', 'Greece'),
('GS', 'South Georgia and the South Sandwich Islands'),
('GT', 'Guatemala'),
('GU', 'Guam'),
('GW', 'Guinea-Bissau'),
('GY', 'Guyana'),
('HK', 'Hong Kong'),
('HM', 'Heard and McDonald Islands'),
('HN', 'Honduras'),
('HR', 'Croatia'),
('HT', 'Haiti'),
('HU', 'Hungary'),
('ID', 'Indonesia'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IM', 'Isle of Man'),
('IN', 'India'),
('IO', 'British Indian Ocean Territory'),
('IQ', 'Iraq'),
('IR', 'Iran'),
('IS', 'Iceland'),
('IT', 'Italy'),
('JE', 'Jersey'),
('JM', 'Jamaica'),
('JO', 'Jordan'),
('JP', 'Japan'),
('KE', 'Kenya'),
('KG', 'Kyrgyzstan'),
('KH', 'Cambodia'),
('KI', 'Kiribati'),
('KM', 'Comoros'),
('KN', 'Saint Kitts and Nevis'),
('KP', 'North Korea'),
('KR', 'South Korea'),
('KW', 'Kuwait'),
('KY', 'Cayman Islands'),
('KZ', 'Kazakhstan'),
('LA', 'Lao People’s Democratic Republic'),
('LB', 'Lebanon'),
('LC', 'Saint Lucia'),
('LI', 'Liechtenstein'),
('LK', 'Sri Lanka'),
('LR', 'Liberia'),
('LS', 'Lesotho'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('LV', 'Latvia'),
('LY', 'Libya'),
('MA', 'Morocco'),
('MC', 'Monaco'),
('MD', 'Moldova'),
('ME', 'Montenegro'),
('MF', 'Saint-Martin (France)'),
('MG', 'Madagascar'),
('MH', 'Marshall Islands'),
('MK', 'Macedonia'),
('ML', 'Mali'),
('MM', 'Myanmar'),
('MN', 'Mongolia'),
('MO', 'Macau'),
('MP', 'Northern Mariana Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MS', 'Montserrat'),
('MT', 'Malta'),
('MU', 'Mauritius'),
('MV', 'Maldives'),
('MW', 'Malawi'),
('MX', 'Mexico'),
('MY', 'Malaysia'),
('MZ', 'Mozambique'),
('NA', 'Namibia'),
('NC', 'New Caledonia'),
('NE', 'Niger'),
('NF', 'Norfolk Island'),
('NG', 'Nigeria'),
('NI', 'Nicaragua'),
('NL', 'The Netherlands'),
('NO', 'Norway'),
('NP', 'Nepal'),
('NR', 'Nauru'),
('NU', 'Niue'),
('NZ', 'New Zealand'),
('OM', 'Oman'),
('PA', 'Panama'),
('PE', 'Peru'),
('PF', 'French Polynesia'),
('PG', 'Papua New Guinea'),
('PH', 'Philippines'),
('PK', 'Pakistan'),
('PL', 'Poland'),
('PM', 'St. Pierre and Miquelon'),
('PN', 'Pitcairn'),
('PR', 'Puerto Rico'),
('PS', 'Palestinian Territory, Occupied'),
('PT', 'Portugal'),
('PW', 'Palau'),
('PY', 'Paraguay'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RS', 'Serbia'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('SA', 'Saudi Arabia'),
('SB', 'Solomon Islands'),
('SC', 'Seychelles'),
('SD', 'Sudan'),
('SE', 'Sweden'),
('SG', 'Singapore'),
('SH', 'Saint Helena'),
('SI', 'Slovenia'),
('SJ', 'Svalbard and Jan Mayen Islands'),
('SK', 'Slovakia (Slovak Republic)'),
('SL', 'Sierra Leone'),
('SM', 'San Marino'),
('SN', 'Senegal'),
('SO', 'Somalia'),
('SR', 'Suriname'),
('SS', 'South Sudan'),
('ST', 'Sao Tome and Principe'),
('SV', 'El Salvador'),
('SX', 'Saint-Martin (Pays-Bas)'),
('SY', 'Syria'),
('SZ', 'Swaziland'),
('TC', 'Turks and Caicos Islands'),
('TD', 'Chad'),
('TF', 'French Southern Territories'),
('TG', 'Togo'),
('TH', 'Thailand'),
('TJ', 'Tajikistan'),
('TK', 'Tokelau'),
('TL', 'Timor-Leste'),
('TM', 'Turkmenistan'),
('TN', 'Tunisia'),
('TO', 'Tonga'),
('TR', 'Turkey'),
('TT', 'Trinidad and Tobago'),
('TV', 'Tuvalu'),
('TW', 'Taiwan'),
('TZ', 'Tanzania'),
('UA', 'Ukraine'),
('UG', 'Uganda'),
('UM', 'United States Minor Outlying Islands'),
('US', 'United States'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VA', 'Vatican'),
('VC', 'Saint Vincent and the Grenadines'),
('VE', 'Venezuela'),
('VG', 'Virgin Islands (British)'),
('VI', 'Virgin Islands (U.S.)'),
('VN', 'Vietnam'),
('VU', 'Vanuatu'),
('WF', 'Wallis and Futuna Islands'),
('WS', 'Samoa'),
('YE', 'Yemen'),
('YT', 'Mayotte'),
('ZA', 'South Africa'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `notif`
--

DROP TABLE IF EXISTS `notif`;
CREATE TABLE IF NOT EXISTS `notif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notif_msg` text,
  `notif_time` datetime DEFAULT NULL,
  `notif_repeat` int(11) DEFAULT '1' COMMENT 'schedule in minute',
  `notif_loop` int(11) DEFAULT '1',
  `publish_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `username` varchar(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notif`
--

INSERT INTO `notif` (`id`, `notif_msg`, `notif_time`, `notif_repeat`, `notif_loop`, `publish_date`, `username`) VALUES
(2, 'Hello world', '2019-08-31 11:01:12', 1, 0, '2019-08-31 08:00:12', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `industry_id` varchar(11) NOT NULL,
  `category_id` varchar(11) NOT NULL,
  `service_name` varchar(50) NOT NULL,
  `serviceEntrantEmail` varchar(64) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `industry_id`, `category_id`, `service_name`, `serviceEntrantEmail`) VALUES
(1, '2', '1', 'Agriculture', 'admin25@gmail.com'),
(2, '2', '3', 'Printing', 'admin25@gmail.com'),
(3, '2', '1', 'Secretarial', 'admin25@gmail.com'),
(4, '2', '3', 'Welding', 'admin25@gmail.com'),
(5, '2', '1', 'Machinery', 'admin25@gmail.com'),
(6, '2', '3', 'Doctor', 'admin25@gmail.com'),
(7, '2', '1', 'Labour', 'admin25@gmail.com'),
(8, '2', '3', 'Tourism, Bird watching, and Zoe touring', 'admin25@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `ur_login`
--

DROP TABLE IF EXISTS `ur_login`;
CREATE TABLE IF NOT EXISTS `ur_login` (
  `ur_login_id` int(11) NOT NULL AUTO_INCREMENT,
  `ur_email` varchar(30) NOT NULL,
  `ur_pas` varchar(100) NOT NULL,
  `ur_type` varchar(20) NOT NULL,
  `d` varchar(100) DEFAULT NULL,
  `remember_me_token` varchar(100) DEFAULT NULL,
  `reset_password_token` varchar(130) DEFAULT NULL COMMENT 'reset password token',
  `reset_password_timestamp` datetime DEFAULT NULL COMMENT 'the reset password link period',
  `reset_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0; link active. 1; link expired',
  PRIMARY KEY (`ur_login_id`),
  KEY `ur_email` (`ur_email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ur_login`
--

INSERT INTO `ur_login` (`ur_login_id`, `ur_email`, `ur_pas`, `ur_type`, `d`, `remember_me_token`, `reset_password_token`, `reset_password_timestamp`, `reset_status`) VALUES
(1, 'ruperic25@gmail.com', '$2y$10$txp2oJobXBm/ZfSHeAfITOevPKJCafIUjNgK8PZMu.3s9xAL4/ace', 'user', NULL, NULL, NULL, NULL, 1),
(2, 'noreply.acad@gmail.com', '$2y$10$6NSuXqiKw/uPUglAIsZdRObVVDK4rjXHrtkviGLsqWfKKMJYC9Bt6', 'user', NULL, NULL, NULL, '2019-09-11 18:38:01', 0),
(3, 'ruperic25@outlook.com', '$2y$10$nwNCyvrcPqi4TlzKZzgcdOvIzSquV4Ca8Kul/bInrpMKb32w5B48C', 'user', NULL, NULL, '471e0b5a6dc1dab218a97567a9321616ee2f46a820190913110938aa193b33f02b6c09ce11306764d04aa3048e7488', '2019-09-14 11:09:38', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`) VALUES
('admin', '123'),
('donald', '123'),
('ronaldo', '123'),
('messi', '123');

-- --------------------------------------------------------

--
-- Table structure for table `user_education`
--

DROP TABLE IF EXISTS `user_education`;
CREATE TABLE IF NOT EXISTS `user_education` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `educ_level` varchar(20) NOT NULL,
  `institution` varchar(60) NOT NULL,
  `award` varchar(70) NOT NULL,
  `grade` varchar(30) NOT NULL,
  `year` varchar(5) NOT NULL,
  `ur_email` varchar(50) NOT NULL,
  PRIMARY KEY (`education_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_education`
--

INSERT INTO `user_education` (`education_id`, `educ_level`, `institution`, `award`, `grade`, `year`, `ur_email`) VALUES
(3, 'Primary', 'Kikungiri Primary School', 'pass slip', 'First', '2004', 'banxie@gmail.com'),
(4, 'Secondary', 'St Marys College Rushoroza ', 'UCE Ceritificate', 'First ', '2008', 'banxie@gmail.com'),
(5, 'Secondary', 'St Marys College Rushoroza', 'UACE Certificate', '19 points', '2010', 'banxie@gmail.com'),
(6, 'Tertiary', 'Kyambogo University ', 'Bachelors in IT and Computing ', '2 class upper ', '2015', 'banxie@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE IF NOT EXISTS `user_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `names` varchar(70) DEFAULT NULL COMMENT 'full names',
  `phone_no` varchar(20) DEFAULT NULL COMMENT 'phone number',
  `ur_email` varchar(50) NOT NULL COMMENT 'valid email address',
  `gender` varchar(7) DEFAULT NULL COMMENT 'gender; male or female',
  `date_of_birth` varchar(20) DEFAULT NULL COMMENT 'date of birth',
  `nationality` varchar(20) DEFAULT NULL COMMENT 'nationality',
  `id_type` varchar(20) DEFAULT NULL COMMENT 'type of identification',
  `id_no` varchar(40) DEFAULT NULL COMMENT 'identification number',
  `current_address` varchar(100) DEFAULT NULL COMMENT 'current location of person',
  `permanent_address` varchar(100) DEFAULT NULL COMMENT 'permanent location',
  `referee` varchar(50) DEFAULT NULL COMMENT 'referees',
  `profession` varchar(90) DEFAULT NULL COMMENT 'profession of person',
  `profile_picture` varchar(100) DEFAULT NULL COMMENT 'profile picture',
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='stores information regarding client';

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`profile_id`, `names`, `phone_no`, `ur_email`, `gender`, `date_of_birth`, `nationality`, `id_type`, `id_no`, `current_address`, `permanent_address`, `referee`, `profession`, `profile_picture`) VALUES
(1, 'Oliver Newton', '0787916686', 'ruperic25@gmail.com', 'Female', NULL, 'Ugandan', NULL, NULL, 'Namugongo', NULL, NULL, 'IT Specialist', 'team3.jpg'),
(2, 'Newt P', '0758219490', 'noreply.acad@gmail.com', 'Male', NULL, 'Ugandan', NULL, NULL, 'Namugongo', NULL, NULL, 'Welder', '2.png'),
(3, 'Joe Doe', '0702100202', 'joe.doe@ymail.com', 'Male', NULL, 'Ugandan', NULL, NULL, 'Jinja', NULL, NULL, 'Marketeer', '1.jpg'),
(4, 'Rupiny Eric', '0787916686', 'ruperic25@outlook.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_skills`
--

DROP TABLE IF EXISTS `user_skills`;
CREATE TABLE IF NOT EXISTS `user_skills` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(50) NOT NULL,
  `ur_email` varchar(50) NOT NULL,
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_skills`
--

INSERT INTO `user_skills` (`skill_id`, `skill`, `ur_email`) VALUES
(1, 'Programming', 'banxie@gmail.com'),
(2, 'MC', 'banxie@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `working_experience`
--

DROP TABLE IF EXISTS `working_experience`;
CREATE TABLE IF NOT EXISTS `working_experience` (
  `experience_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `location` varchar(100) NOT NULL,
  `from_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `ur_email` varchar(50) NOT NULL,
  PRIMARY KEY (`experience_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `working_experience`
--

INSERT INTO `working_experience` (`experience_id`, `title`, `company`, `location`, `from_date`, `end_date`, `ur_email`) VALUES
(3, 'Research Associate', 'Eden Resource Center', 'Banda', '2015-01-13', '2016-01-03', 'banxie@gmail.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
