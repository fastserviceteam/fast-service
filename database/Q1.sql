ALTER TABLE `jobs` ADD `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '1 - running; 0 ended' AFTER `Business_Name`;
ALTER TABLE `jobs` CHANGE `status` `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '0 ended; 1 - running; 2 - expired';
ALTER TABLE `advert_posts` CHANGE `status` `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT 'ended - 0; running-1; 2 - expired';
ALTER TABLE `advert_posts` CHANGE `status` `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT 'not yet - 0; running-1; 2 - expired';