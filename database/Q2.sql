ALTER TABLE `ur_login` ADD `reset_password_token` VARCHAR(130) NULL DEFAULT NULL COMMENT 'reset password token' AFTER `remember_me_token`;
ALTER TABLE `ur_login` DROP INDEX `ur_pas`;
ALTER TABLE `ur_login` ADD `reset_password_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'the reset password link period' AFTER `reset_password_token`;