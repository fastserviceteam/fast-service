<?php
    session_start();

    require "../_require-file.php"; 
      // --- >> instantiate object
    $_globalObj = new globalClass();

    if ($_globalObj->_isLoggedIn() == false) 
    {
       header("Location: ../index.php");        
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fast Service | Client</title>

    <!-- Bootstrap -->
    <link href="_assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="_assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">  
    <!-- bootstrap-progressbar -->
    <!--<link href="_assets/plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">-->
    <!-- NProgress -->
    <link href="_assets/plugins/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="_assets/plugins/google-code-prettify/bin/prettify.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="_assets/css/custom.css" rel="stylesheet">
        <!-- /. Favicon --> 
    <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
  </head>

  <body class="nav-md fixed_nav">

    <div class="container body" id="main_wrapper">
      
      <div class="main_container">

    <!-- include left sidebar main navigation menu -->
    <?php require "_partials/_leftColumnNavigationMenu.php"; ?>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="_assets/images/img.png" alt=""><?php echo $_SESSION['emailUname']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="?logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green"><?php $_globalObj->_receivedMessagesCount($_SESSION['ur_email']); ?></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
          <?php $_globalObj->_retrieveMessages($_SESSION['ur_email']);?>
                    <li>
                      <div class="text-center">
                        <a href="messages.php">
                          <strong>See All Messages</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col page_content" role="main">         

            <div class="page-title">              
              <div class="title_left">
                <h3>Messages</h3>
              </div>
            </div>           

			<div id="message_wrapper">

            <div class="row">			
              <div class="col-md-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Inbox</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">
                      <div class="col-sm-3 mail_list_column">
                        <button id="compose" class="btn btn-sm btn-success btn-block" type="button">COMPOSE</button>
                        
                        <select name="input_filter_messages_selection" id="input_filter_messages_selection" class="form-control">
                          <option value="inbox" selected>Inbox</option>
                          <option value="sent">Sent</option>
                          <option value="trash">Trash</option>
                        </select>
                        
                        <h4 class="text-center text-success font-weight-400" id="change_title_box"><i class="fa fa-inbox fa-1x"></i> Inbox</h4>
                        <hr>

                            <div id="messages_wrapper">
                              
                                <span id="load_inbox_messages_inbox">
                                  <?php $_globalObj->_messagesInbox($_SESSION['ur_email']); ?>
                                </span>
                                <span id="load_inbox_messages_sentbox" class="item_display_none">
                                  <?php $_globalObj->_messagesSentBox($_SESSION['ur_email']); ?>
                                </span>
                                <span id="load_inbox_messages_bin" class="item_display_none">
                                  <?php $_globalObj->_messagesBinBox($_SESSION['ur_email']); ?>
                                </span>

                            </div>

                      </div>
                      <!-- /MAIL LIST -->

                      <!-- CONTENT MAIL -->
                      <div class="col-sm-9 mail_view">

                        <div class="inbox-body">
                          <div class="mail_heading row">
                            <div class="col-md-8">
                              <div class="btn-group">
                            <!--
                                <button class="btn btn-sm btn-flat btn-primary" onclick="replyToThisMessage()" type="button" id="reply_message_btn"><i class="fa fa-reply"></i> Reply</button>
                                <button class="btn btn-sm btn-flat btn-info" onclick="forwardThisMessage()" type="button" id="forward_message_btn"><i class="fa fa-mail-forward"></i> Forward</button>
                             -->

                                <button class="btn btn-sm btn-flat bg-maroon text-white" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash" id="delete_message_btn" onclick="deleteThisMessage()"><i class="fa fa-trash-o"></i> Delete</button>


                              </div>
                            </div>
                            <div class="col-md-4 text-right">
                              <p class="date"> 
                                <span id="show_mail_date_time" class="reset_fields"></span>
                                <!--<span id="show_mail_date" class="reset_fields"></span>-->
                              </p>
                            </div>
                            <div class="col-md-12">
                              <h4 id="show_mail_subject" class="reset_fields"> </h4>
                            </div>
                          </div>
                          <div class="sender-info">
                            <div class="row">
                              <div class="col-md-12">
                                <span id="show_from" class="reset_fields"></span>

                                <strong id="show_mail_names" class="reset_fields"></strong>

                                <span id="show_to" class="reset_fields"></span>

                                <span id="show_mail_email_address" class="reset_fields"></span>
                                
                                <span id="show_mail_deleted" class="reset_fields text-primary"></span>

                               <h3 class="text-muted text-center" id="no_preview_p">No preview</h3>

                                <a class="sender-dropdown item_display_none" id="toggle_info"><i class="fa fa-chevron-down"></i></a>
                                <div id="toggle_more_message_info" class="item_display_none">
                                  <span id="load_more_message_details">
                                    <ul class="ul_load_more_mail_details">
                                      <li id="more_details_to_from_names"></li>
                                      <li id="more_details_to_from_email"></li>
                                    </ul>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="view-mail">
                            <p id="show_mail_messages" class="reset_fields"></p>

                            <input type="hidden" id="showing_message_id">
                            <input type="hidden" id="showing_message_status">
                            <span id="message_alerts"></span>
                          </div>
                        </div>


                      </div>
                      <!-- /CONTENT MAIL -->
                    </div>
                  </div>
                </div>
              </div>
			  
            </div>
			<!-- row -->
            </div>
			<!-- message_wrapper -->
			
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)">Fast Service</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
      <!-- main_container -->

    </div>
   <!-- .container .body #main_wrapper -->

    <!-- compose -->
    <div class="compose col-md-6 col-xs-12">
      <div class="compose-header">
        New Message
        <button type="button" class="close compose-close">
          <span>×</span>
        </button>
      </div>

<form class="clear_form_after_submission">
  
      <div class="compose-body">
        <span id="message_alerts_span"></span>

<br>
          <div class="form-group">
              <input type="email" name="input_receivingEmailAddress" id="input_receivingEmailAddress" class="form-control" placeholder="To: email address">
          </div>

        <div class="form-group">
            <input type="text" name="input_receivingSubject" id="input_receivingSubject" class="form-control" placeholder="Subject:">
        </div>

        <div class="form-group">
          <textarea class="editor-wrapper form-control" id="input_receivingMesage" placeholder="Message:" maxlength="150"></textarea>
        </div>

        <!--<input type="hidden" id="input_receivingDate" value="<?php #echo date('Y-m-d'); ?>">-->
        <input type="hidden" id="input_sendingEmail" value="<?php echo $_SESSION['ur_email']; ?>">

      </div>

      <div class="compose-footer">
        <button id="sendMessageBtn" class="btn btn-flat" type="button">Send</button>
      </div>
</form>

    </div>
    <!-- /compose -->

        <!-- >> include bottom scripts << -->
    <?php require_once "_partials/_bottomScripts.php"; ?>



  </body>
</html>