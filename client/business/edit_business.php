<!DOCTYPE html> 
<?php
	require "../../_require-file.php"; // database config, server feedbacks, phpmailer class and other global constants
	require "../../_server-functions.php"; // custom functions
 
 	
	$to = GetRow("SELECT * FROM business_profile WHERE business_profile_id = '".$_POST['edit_id']."'");
			 
	 ?> 
<style>
.blu{color:#120f90; font-weight:bold;}
</style>
<form method="post" action="business/submit.php">
							<section class="panel panel-default" >
								<header class="panel-heading" >
									<div class="panel-actions">
									 </div>

									<h4 class="panel-title"><b>Update Your Business </b></h4>
									
								</header>
								<div class="panel-body" style="background:#f9fdfd; ">
							
		<div class="form-group">
				 <div class="col-sm-12"> 
					<input type="text" name="business_profile_id" value="<?=$to->business_profile_id;?>" class="blu form-control hidden"  required >
					<input type="text" name="Business_Name"  onkeyup="s_business();" value="<?=$to->Business_Name;?>" class="blu form-control" placeholder="Business Name: " required >
											
						</div>
							
							</div>
							 
							
							 <div class="form-group row">
							 
			<div class="col-sm-12">
			  
					<div class="col-sm-5"> 
						<?php
						$categories = GetAll("categories","ORDER BY category_name ASC");
						?>
						<label class="col-sm-12 control-label"><b>Category </b></label> 
						<select type="text"  onkeyup="s_business();" name="Business_Category" class="form-control blu" required>
						<option VALUE="<?=$to->Business_Category;?>"><?=$to->Business_Category;?></option> 
							<?php
							foreach($categories as $r){
							echo '<option>'.$r->category_name.'</option>';
							}
							?>							
							</select>
							</div>
							<div class="col-sm-7"> 
						<div class="form-group"> 
				<label class="col-sm-12 control-label"><b>Select Services Offered </b></label> 
					<div class="col-sm-12"> 
					<div class="form-group">
					<?php
					$services = GetAll("services");
 
					?>
					 
					<select type="text"  onchange="showSelectsd(), s_business();" name="multiple_select" class="blu form-control" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }'>
					<?php
							foreach($services as $svc){
								echo '<option value="'.$svc->service_id.'">'.$svc->service_name.'</option>';						
							}
							?>  
					</select>
					<input type="text" class="hidden form-control" id="send" value="<?=$to->Services_Offered;?>" name="Services_Offered" /> 
					
			</div> 
<script>

function showSelectsd(){
    var selO = document.getElementsByName('multiple_select')[0];
    var selValues = [];
    for(i=0; i < selO.length; i++){
        if(selO.options[i].selected){
            selValues.push(selO.options[i].value);
        }
		document.getElementById('send').value = selValues; 
    }
}
</script>
				</div>	 
				</div>
				</div>
				
				 		
							
								</div>
								</div>  
						 
						
		<div class="form-group row">								
				<div class="col-sm-12">	
							<div class="col-sm-5">	
 <label class="control-label"><b><i class="fa fa-phone"></i> Phone Contact</b> </label>
															
					<input type="number" name="Business_Phone" value="<?=$to->Business_Phone;?>"  onkeyup="s_business();" placeholder="Business Phone No." class="blu form-control" required >
						</div>
						<div class="col-sm-7">
									<input type="email" name="Business_Email" class="hidden form-control" value="<?=$mail;?>" readonly placeholder="Email Address" required></input>
									<input type="text" class="form-control hidden input-sm" id="location" name="location" value="Kampala Uganda" readonly >
								 <label class="control-label"><b><i class="fa fa-map-marker"></i> Location / Area of Operation</label>
									 <button id="submit" type="button" class="btn-xs  btn-warning pull-right btn" >Get my Current  location  </b>&nbsp; &nbsp; <i class="fa fa-map-marker"></i></button></label>
									 
			<input type="text" class="form-control blu "   id="address" name="Geographical_Area"  onkeyup="s_business();" value="<?=$to->Geographical_Area;?>" placeholder="Enter a location" required>
													<span  id="service_address"></span>
													 
													 
														<input id="latlng"  name="latlng" class="hidden" type="text" ></input>
														<input id="longitude"  name="longitude" type="text" class="hidden" value="<?=$to->vander_latitude;?>"></input>
														<input id="latitude" name="latitude" type="text" class="hidden"value="<?=$to->vander_latitude;?>" ></input>
			  
							</div> 
							</div>  
						
<script>
/* This showResult function is used as the callback function*/
function showResult(result) {
    document.getElementById('latitude').value = result.geometry.location.lat();
    document.getElementById('longitude').value = result.geometry.location.lng();
}

function getLatitudeLongitude(callback, address) {   
    geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }
}

	var button = document.getElementById('submit');
	button.addEventListener("click", function () { 
		s_business();
		var address = document.getElementById('location').value;
		getLatitudeLongitude(showResult, address)
		
	});
</script> 						
												<script language=javascript src='http://maps.google.com/maps/api/js?sensor=false'></script>
 
 
    <div id="map"></div>
	 
 
   <script>  
    if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(position){
            //    var positionInfo = "Your current position is (" + "Latitude: " + position.coords.latitude + ", " + "Longitude: " + position.coords.longitude + ")";
             //   document.getElementById("result").innerHTML = positionInfo;
                var crnt =document.getElementById("latlng").value = position.coords.latitude + ", " + position.coords.longitude;
                document.getElementById("longitude").value = position.coords.longitude;
                document.getElementById("latitude").value = position.coords.latitude;
           });
        } else{
            alert("Sorry, your browser does not support HTML5 geolocation.");
        }
      //===========location of auto complete serarch =============
      function initMap() { 
		var map = new google.maps.Map(document.getElementById('map'), {
			//center: {lat: position.coords.latitude, lng: position.coords.longitude}
			  center: {lat: 0.3733, lng: 32.5825},
          zoom: 13
        });
		  var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;

    //====================on ready also get my location============	
	 	$( document ).ready(function() { 
			 geocodeLatLngReady(geocoder, map, infowindow);
		});
	//====================on click get my location============	
	   document.getElementById('submit').addEventListener('click', function() {
          geocodeLatLng(geocoder, map, infowindow); 
		});
	
//====================end============	
	 
        var card = document.getElementById('pac-card');
        var input = document.getElementById('address');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });
		
		 	
		autocomplete.addListener('place_changed', function() {
			//&&&&&&&&&&&&&&&&&&s show long and lat for the new input address in click place &&&&&&&&&&&&&&&&&&&&&&&&&&&
        	var address = document.getElementById('address').value;
		getLatitudeLongitude(showResult, address) 
		
			//&&&&&&&&&&&&&&&&&&  end  &&&&&&&&&&&&&&&&&&&&&&&&&&
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          
		  if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
		  
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(7);  // Why 17? Because it looks good.
			}
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
         } 
          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);		  	 
        });

         
} 
 
 
	  //================================ on ready page, input my location in text field============================
      function geocodeLatLngReady(geocoder, map, infowindow) {
       navigator.geolocation.getCurrentPosition(function(position){
            //    var positionInfo = "Your current position is (" + "Latitude: " + position.coords.latitude + ", " + "Longitude: " + position.coords.longitude + ")";
             //   document.getElementById("result").innerHTML = positionInfo;
                var crnt =document.getElementById("latlng").value = position.coords.latitude + ", " + position.coords.longitude;
                document.getElementById("longitude").value = position.coords.longitude;
                document.getElementById("latitude").value = position.coords.latitude;
            
		   var input = crnt;
		//   var input = document.getElementById('latlng').value;
        var latlngStr = input.split(',', 2);
         var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
       
		  
		
	   geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
              map.setZoom(14);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
			var adress_name =results[0].formatted_address;  
			document.getElementById('location').value = adress_name;	 
		} else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
		  
           });
           });
      }
	  //=============================================end============================
	  function geocodeLatLng(geocoder, map, infowindow) {
        var input = document.getElementById('latlng').value;
        var latlngStr = input.split(',', 2);
         var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
        
		 
	   geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
				
              map.setZoom(14);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
			var adress_name =results[0].formatted_address;  
			document.getElementById('address').value = adress_name;	 
		 
		} else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
		  
           });
      } 
      </script>
         

		 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeWLbDlNxutMqYPmgXHETvG-PXuJT9_Q8&libraries=places&callback=initMap"
			async defer></script>
		   <div class="col-sm-12"> 
					 		<div class="col-sm-5">
								<br >
								<input type="email" name="Business_Email"  onkeyup="s_business();" value="<?=$to->Business_Email;?>"  placeholder="Business Email Address" class="blu hidden form-control"required disabled>
							
						<select type="text"  name="Business_Address"  onchange="s_business();" class="blu form-control" >
						<option><?=$to->Business_Address;?></option>  
							<option>Kabale</option> 	
							<option>Kampala</option>  
							</select>
											
							</div>
							
								<div class="col-sm-4">
							<br>
						<input type="submit" style="display:none; width:100%" id="submit_business" name="submit_business" class="btn btn-primary" value="Update Business" />
						</div>  
									 
								</div>
							</section>
							</div>
						</form>
				
<script>  		
			
	 	function s_business(){ 
			$("#submit_business").show();
		} 		 		
	</script> 