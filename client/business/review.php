
<?php
	require "../../_require-file.php"; // database config, server feedbacks, phpmailer class and other global constants
	require "../../_server-functions.php"; // custom functions	
	$pro = GetRow("SELECT * FROM business_profile WHERE business_profile_id = '".$_POST['edit_id']."'");
	$serv = GetRow("SELECT * FROM services WHERE service_id = '%".$pro->Services_Offered."%'");
?>
<script>
function printDiv(){ 
			var divToPrint=document.getElementById('print_div'); 
			 var newWin=window.open('','Print-Window');
			newWin.document.open(); 
			newWin.document.write('<link href="assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">');
			 newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
			newWin.document.close(); 
			setTimeout(function(){newWin.close();},10000); 
		} 
</script>
 <div class="panel-body" id="profile_form" > 
	 	<div class="col-sm-12" style="border:2px solid #ccc; border-radius:5px;background:#eee;"> 
			<div id="add_new_form" class="row" style="margin:6px; margin-left:-6px; margin-right:-6px; border-radius:5px; border:1px dashed #ccc; background:#fff; padding :30">
				<button type="button" id="close_tbn4" class="btn btn-xs btn-danger active bold pull-right"><i class="fa">close</i></button> 
				<h4 class="hh"><button type="button" class="btn btn-default  pull-left btn-xs" onclick="printDiv();" title="Print" value="print"><i class="fa-2x fa fa-print"></i></button>
				<center>FAST SERVICE APP </center></h4>
				<div class="col-sm-12">
					<div class="row"> 
					<div id="print_div">
						<table class="table table-bordered table-condensed">	
							<div class="col-xs-2">
								<span class="pull-right"><img src="../images/logo.jpg" style="width:40px; height:45px"></img></span> 							
							</div>
							<div class="col-xs-9 bold">
								<center> 
								<em> Get your service anywhere, at any time</em>
								</center>
							</div>
										 
						<tr>
							<td><b>Business Name:</b></td><td> <?=$pro->Business_Name;?> </td>										
						</tr>						 
						<tr>
							<td><b>Category:</b></td><td> <?=$pro->Business_Category;?> </td>										
						</tr>						 
						<tr>
							<td><b>Address </b></td><td><?=$pro->Business_Address;?></td>							
						</tr>
						<tr>	
							<td><b>Phone </b></td><td><?=$pro->Business_Phone;?></td>
						</tr>
						<tr>	
							<td><b>Services_Offered </b></td><td><?=$serv->service_name;?></td>
						</tr>
						<tr>	
							<td><b>Email Address:</b></td><td> <?=$pro->Business_Email;?></td>
						</tr> 
						 
						<tr>	
							<td><b>Geographical Area </b></td><td><?=$pro->Geographical_Area;?></td>
						</tr>
						 
						</table> 
					</div>
				</div> 					  
			</div>	
		</div>	<br><br>
</div>	
	 <script> 
		 	$(document).ready(function(){ 
				$("#close_tbn4").click(function(){
					$("#add_new_form").hide();
					$("#profile_form").hide();
					$("#bzn_table_form").fadeIn();
				});				
		 	}); 
			
			</script>