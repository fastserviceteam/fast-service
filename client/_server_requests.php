<?php
	/* ----------------------------- */
	/* server requests - objects
	/* ----------------------------- */
	
	require "../_require-file.php"; 
      // --- >> instantiate object
    $_globalObj = new globalClass();

?>

<?php

// --- >> filter messages dropdown
if (isset($_POST['mailListfilter']))
{
	$mailListfilter 	= $_POST['mailListfilter'];

	$_globalObj->_mailListfilter($mailListfilter);
} // end


// --- >> delete message
if (isset($_POST['delMessageId']) && isset($_POST['delMessageStatus'])) 
{
	$delMessageId 		= $_POST['delMessageId'];
	$delMessageStatus 	= $_POST['delMessageStatus'];

	$_globalObj->_deleteMessage($delMessageId, $delMessageStatus);
}

// --- >> send message
if (isset($_POST['receivingEmailAddress']) && 
	isset($_POST['receivingSubject']) && 
	isset($_POST['receivingMessage']) && 
	isset($_POST['sendingEmail'])) 
{
	$receivingEmailAddress 	= $_POST['receivingEmailAddress'];
	$receivingSubject 		= $_POST['receivingSubject'];
	$receivingMessage 		= $_POST['receivingMessage'];
	$sendingEmail 		    = $_POST['sendingEmail'];

	$_globalObj->_clientSendEmail($receivingEmailAddress, $receivingSubject, $receivingMessage, $sendingEmail);
}

// --- >> show clicked message to preview
if (isset($_POST['messageId']) && isset($_POST['messageStatus'])) 
{
	$messagePkeyId = $_POST['messageId'];
	$messageStatus = $_POST['messageStatus'];

	$_globalObj->_showSelectedMessagePreview($messagePkeyId, $messageStatus);
	
}

// --- >> create new business
if (isset($_POST['dataInput_business_name']) && 
	isset($_POST['dataInput_business_category']) && 
	isset($_POST['dataInput_business_pcontact']) && 
	isset($_POST['dataInput_business_emailaddress']) && 
	isset($_POST['dataInput_business_address']) && 
	isset($_POST['dataInput_business_servicesoffered']) && 
	isset($_POST['dataInput_Geographical_Area']) && 
	isset($_POST['dataInput_latlng']) && 
	isset($_POST['dataInput_emailAddress']) && 
	isset($_POST['dataInput_longitude']) && 
	isset($_POST['dataInput_latitude'])) 
{
	$business_name 				= $_POST['dataInput_business_name'];
	$business_category 			= $_POST['dataInput_business_category'];
	$business_pcontact 			= $_POST['dataInput_business_pcontact'];
	$business_emailaddress 		= $_POST['dataInput_business_emailaddress'];
	$business_address 			= $_POST['dataInput_business_address'];
	$business_servicesoffered 	= $_POST['dataInput_business_servicesoffered'];
	$geog_Area 					= $_POST['dataInput_Geographical_Area'];
	$geog_latlng 		    	= $_POST['dataInput_latlng'];
	$loggedInemailAddress 		= $_POST['dataInput_emailAddress'];
	$geog_longitude 		    = $_POST['dataInput_longitude'];
	$geog_latitude 		    	= $_POST['dataInput_latitude'];

	$_globalObj->_createNewBusiness($business_name, $business_category, $business_pcontact, $business_emailaddress, $business_address, $business_servicesoffered, $geog_Area, $geog_latlng, $geog_longitude, $geog_latitude, $loggedInemailAddress);
	
	$_globalObj->_feedbacks();
}

// --- >> my business list    
if (isset($_POST['myBusinessListRandomValue'])) 
{
	$myBusinessListRandomValue 				= $_POST['myBusinessListRandomValue'];
	$_globalObj->_myBusinessProfileList();
	//echo $myBusinessListRandomValue;
}

// --- >> delete business profile
if (isset($_POST['businessProfilePkey'])) 
{
	$businessProfilePkey 	= $_POST['businessProfilePkey'];
	$_globalObj->_deleteMyBusiness($businessProfilePkey);

}

// --- >> create new job
if (
	isset($_POST['dataInput_job_have_a_business']) &&
	//isset($_POST['dataInput_job_businessname']) && 
	isset($_POST['dataInput_job_categorytokenname']) &&
	isset($_POST['dataInput_job_categoryname']) &&
	isset($_POST['dataInput_job_title']) && 
	isset($_POST['dataInput_job_district']) && 
	isset($_POST['dataInput_job_location']) && 
	isset($_POST['dataInput_job_type']) && 
	isset($_POST['dataInput_job_salary']) && 
	isset($_POST['dataInput_job_posted_on']) && 
	isset($_POST['dataInput_job_deadline']) && 
	isset($_POST['dataInput_job_qualification']) && 
	isset($_POST['dataInput_job_responsibility']) && 
	isset($_POST['dataInput_job_duration']) && 
	isset($_POST['dataInput_job_how_to_apply'])) 
{
	$job_have_a_business 	= $_POST['dataInput_job_have_a_business'];
	$job_businessid 		= $_POST['dataInput_job_businessname'];
	$job_categorytokenname 	= $_POST['dataInput_job_categorytokenname'];
	$job_categoryname 		= $_POST['dataInput_job_categoryname'];
	$job_title 				= $_POST['dataInput_job_title'];
	$job_district 			= $_POST['dataInput_job_district'];
	$job_location 			= $_POST['dataInput_job_location'];
	$job_type 				= $_POST['dataInput_job_type'];
	$job_salary 			= $_POST['dataInput_job_salary'];
	$job_posted_on 			= $_POST['dataInput_job_posted_on'];
	$job_deadline 			= $_POST['dataInput_job_deadline'];
	$job_qualification 		= $_POST['dataInput_job_qualification'];
	$job_responsibility 	= $_POST['dataInput_job_responsibility'];
	$job_how_to_apply 		= $_POST['dataInput_job_how_to_apply'];
	$job_duration 			= $_POST['dataInput_job_duration'];


	$_globalObj->createNewJob($job_categorytokenname, $job_categoryname, $job_have_a_business, $job_businessid, $job_title, $job_district, $job_location, $job_type, $job_salary, $job_posted_on, $job_deadline, $job_qualification, $job_responsibility, $job_how_to_apply, $job_duration);
	
	$_globalObj->_feedbacks();
} // end

// load job name associated with selected job token
if (isset($_POST['jcTokenAssocName'])) {
	$_globalObj->loadAssocJobName($_POST['jcTokenAssocName']);
}


// --- >> load selected job details
if (isset($_POST['selectedJobId'])) 
{
	$selectedJobId 	= $_POST['selectedJobId'];
	$_globalObj->_loadSelectedJobDetails($selectedJobId);
}


// --- >> return services offered in json format for searching
if (isset($_POST['servicesOffered'])) 
{

	$keyword = strval($_POST['servicesOffered']);

	$_globalObj->_returnBusinessServicesOffered($keyword);
}



// --- >> request for a service
if (isset($_POST['dataForm_my_location']) && 
	isset($_POST['dataForm_where_to_deliver_service']) && 
	isset($_POST['dataInput_sr_latlng']) && 
	isset($_POST['dataInput_sr_longitude']) && 
	isset($_POST['dataInput_sr_latitude']) && 
	isset($_POST['dataForm_servicerequired']) && 
	isset($_POST['dataInput_loggedin_email']) && 
	isset($_POST['quotation_details']) && 
	isset($_POST['dataInput_sr_date_of_servicedelivery']) && 
	isset($_POST['dataInput_sr_time_of_servicedelivery'])) 
{
	$my_location 			  = $_POST['dataForm_my_location'];
	$where_to_deliver_service = $_POST['dataForm_where_to_deliver_service'];
	$sr_latlng 		          = $_POST['dataInput_sr_latlng'];
	$sr_longitude 			  = $_POST['dataInput_sr_longitude'];
	$sr_latitude 		      = $_POST['dataInput_sr_latitude'];
	$servicerequired 		  = $_POST['dataForm_servicerequired'];
	$loggedin_email 		  = $_POST['dataInput_loggedin_email'];
	$quotation_details 	      = $_POST['quotation_details'];
	$date_of_servicedelivery  = $_POST['dataInput_sr_date_of_servicedelivery'];
	$time_of_servicedelivery  = $_POST['dataInput_sr_time_of_servicedelivery'];


	$_globalObj->_requestService($my_location, $where_to_deliver_service, $sr_latlng, $sr_longitude, $sr_latitude, $servicerequired, $loggedin_email, $quotation_details, $date_of_servicedelivery, $time_of_servicedelivery);
	
	$_globalObj->_feedbacks();
}

// --- >> add new advert
if (
	isset($_POST['dataFormAdvert_have_a_business']) && 
	isset($_POST['dataFormAdvert_title']) && 
	//isset($_POST['dataFormAdvert_allowComments']) && 
	isset($_POST['dataFormAdvert_publish']) && 
	isset($_POST['dataFormAdvert_period']) && 
	isset($_POST['dataFormAdvert_details']) &&
	isset($_POST['dataFormAdvert_bannersYesNo']) &&
	isset($_POST['dataFormAdvert_loggedInEmail'])
	/* && 
	isset($_POST['advert_bannerOne']) &&
	isset($_POST['advert_bannerTwo']) && 
	isset($_POST['advert_bannerThree'])*/
) 
{
	$advert_have_a_business = $_POST['dataFormAdvert_have_a_business']; // yes or no
	$advert_businessName 	= $_POST['dataFormAdvert_businessName'];
	//$advert_allowComments 	= $_POST['dataFormAdvert_allowComments'];

	$advert_bannersYesNo 	= $_POST['dataFormAdvert_bannersYesNo'];

	$advert_title 			= $_POST['dataFormAdvert_title'];
	$advert_publish 		= $_POST['dataFormAdvert_publish'];
	$advert_period 		    = $_POST['dataFormAdvert_period'];
	$advert_loggedInEmail 	= $_POST['dataFormAdvert_loggedInEmail'];
	$advert_details 		= $_POST['dataFormAdvert_details'];

	$advert_bannerName 		= $_FILES['advert_bannerOne']['name'];
	$advert_bannerTmp 		= $_FILES["advert_bannerOne"]["tmp_name"];
	$advert_bannerSize 		= $_FILES["advert_bannerOne"]["size"];

	$advert_bannerTwoName 	= $_FILES['advert_bannerTwo']['name'];
	$advert_bannerTwoTmp 	= $_FILES["advert_bannerTwo"]["tmp_name"];
	$advert_bannerTwoSize 	= $_FILES["advert_bannerTwo"]["size"];

	$advert_bannerThreeName = $_FILES['advert_bannerThree']['name'];
	$advert_bannerThreeTmp 	= $_FILES["advert_bannerThree"]["tmp_name"];
	$advert_bannerThreeSize = $_FILES["advert_bannerThree"]["size"];


	$_globalObj->_addNewAdvert(
		$advert_businessName,
		$advert_have_a_business,
		$advert_title, 
		//$advert_allowComments, 
		$advert_publish, 
		$advert_period, 
		$advert_loggedInEmail,
		$advert_details,
		$advert_bannerName, 
		$advert_bannerTmp,
		$advert_bannerSize,
		$advert_bannerTwoName,
		$advert_bannerTwoTmp,
		$advert_bannerTwoSize, 
		$advert_bannerThreeName, 
		$advert_bannerThreeTmp, 
		$advert_bannerThreeSize,
		$advert_bannersYesNo);

	$_globalObj->_feedbacks();
}

// --- >> load selected advert information   
if (isset($_POST['selectedadvertId'])) 
{
	$selectedadvertId 	= $_POST['selectedadvertId'];

	$_globalObj->_loadSelectedAdvertDetails($selectedadvertId);
}	

// --- >> like an advert  
if(isset($_POST['advertPkeyCounter']) && isset($_POST['likedAdvertLoggedInEmail']))
{
	$advertPkeyCounter 			= $_POST['advertPkeyCounter'];
	$likedAdvertLoggedInEmail 	= $_POST['likedAdvertLoggedInEmail'];

	$_globalObj->_likeAdvertMethod($advertPkeyCounter, $likedAdvertLoggedInEmail);
}  
if(isset($_POST['profileSideCounter']))
{
	$advertPkeyCounter 			= $_POST['profileSideCounter'];
	$likedAdvertLoggedInEmail 	= $_SESSION['ur_email'];

	$_globalObj->_likeAdvertMethod($advertPkeyCounter, $likedAdvertLoggedInEmail);
}

// --- >> load more adverts in advertsList.php
if (isset($_POST['advertLastId'])) 
{
	$advertLastId 	= $_POST['advertLastId'];

	$_globalObj->_loadMoreAdvertsMethod($advertLastId);
}

// --- >> search for adverts autocomplete
if (isset($_POST['query'])) 
{
	$advertTerm = $_POST['query'];
	//$advertTerm = trim(strip_tags($advertTerm));
	//$advertTerm = mysqli_real_escape_string($advertTerm);

	$_globalObj->_returnAdvertsJson($advertTerm);
} //

// --- >> adverts filter pagination
if(isset($_POST['page'])){

	$_globalObj->_advertListPagination($_POST['page'], $_POST['keywords'], $_POST['sortBy']);
} //

// ---- >> load job search results
if(isset($_POST['jobSearchKeyWord']))
{
	$jobSearchKeyWord = trim(strip_tags($_POST['jobSearchKeyWord']));

	$_globalObj->_jobSearchFilter($jobSearchKeyWord);
}

// ---- >> order / filter jobs and adverts  << --- //

// ascending or descending order adverts
if(isset($_POST['orderingAdvertsValue']))
{
	$orderingAdvertsValue = $_POST['orderingAdvertsValue'];

	$_globalObj->_advertsOrderFilter($orderingAdvertsValue);
}

// --- >> load more adverts in advertsList.php for ordered / sorted
if (isset($_POST['advertLastOrderDate']) && isset($_POST['dataOrderValue'])) 
{
	$advertLastOrderDate 	= $_POST['advertLastOrderDate'];
	$dataOrderValue 	= $_POST['dataOrderValue'];

	$_globalObj->_loadMoreAdvertsOrderedMethod($advertLastOrderDate, $dataOrderValue);
}

// ascending or descending order jobs
if(isset($_POST['orderingValue']))
{
	$orderingValue = $_POST['orderingValue'];

	$_globalObj->_jobOrderFilter($orderingValue);
}

// --- >> added jobs to or remove from favorites
if(isset($_POST['jobIdToAddToFavorites']))
{
	$jobIdToAddToFavorites = $_POST['jobIdToAddToFavorites'];

	$_globalObj->_addingJobTofavorites($jobIdToAddToFavorites);
}
// remove already fav jobs
if(isset($_POST['jobIDRemovefromFavorites']))
{
	$jobIDRemovefromFavorites = $_POST['jobIDRemovefromFavorites'];

	$_globalObj->_addingJobTofavorites($jobIDRemovefromFavorites);
}

// --- >> add adverts or remove adverts from favorites
if(isset($_POST['advertIdToAddToFavorites']))
{
	$advertIdToAddToFavorites = $_POST['advertIdToAddToFavorites'];

	$_globalObj->_addingAdvertTofavorites($advertIdToAddToFavorites);
}



// --- >> profile << --- //

// --- >> load favorite jobs  
if (isset($_POST['favoriteJobsValue'])) 
{
	$_globalObj->_favoritejobsList();
}	

// --- >> load favorite adverts
if (isset($_POST['favoriteAdvertsValue'])) 
{
	$_globalObj->_favoriteadvertsList();
}

// profile picture upload
if (isset($_FILES['newClientPhotoValue']['name'])) 
{
	$_globalObj->clientChangingProfileImage(
		$_FILES['newClientPhotoValue']['name'], 
		$_FILES['newClientPhotoValue']['size'], 
		$_FILES["newClientPhotoValue"]["tmp_name"]);	
} //

// reload profile pic
if (isset($_POST['clientProfileImageReload'])) 
{
	echo '_assets/clientUploads/profile/'.$_globalObj->sessionProfilePicture();
}

// --- >> filter favorite items	


// --- >> request current active user session
if (isset($_POST['currentSessionEmailAddress'])) 
{
	$currentSessionEmailAddress 	= $_POST['currentSessionEmailAddress'];
	$_globalObj->_queryProfileOfActiveSession($currentSessionEmailAddress);
}

// --- >> basic
if (
	isset($_POST['editing_fullnames']) && 
	isset($_POST['editing_gender']) && 
	isset($_POST['editing_profession']) &&
	isset($_POST['editing_nationality']) && 
	isset($_POST['editing_currentLocation']) && 
	isset($_POST['editing_phoneNumber'])
) 
{

	$editing_fullnames 	= $_POST['editing_fullnames'];
	$editing_gender 	= $_POST['editing_gender'];
	$editing_profession = $_POST['editing_profession'];
	$editing_nationality 	= $_POST['editing_nationality'];
	$editing_currentLocation 	= $_POST['editing_currentLocation'];
	$editing_phoneNumber = $_POST['editing_phoneNumber'];

	//$profile_pic_name 	= $_FILES["editing_change_profile_pic"]["name"];
	//$profile_pic_size 	= $_FILES["editing_change_profile_pic"]["size"];
	//$profile_pic_tmp 	= $_FILES["editing_change_profile_pic"]["tmp_name"];

	$_globalObj->updateBasicProfile($editing_fullnames,$editing_gender,$editing_profession,$editing_nationality,$editing_currentLocation,$editing_phoneNumber);
	$_globalObj->_feedbacks();

}

// --- >> location
/*
if (isset($_POST['editing_nationality']) && isset($_POST['editing_currentLocation']) && isset($_POST['editing_phoneNumber'])) 
{

	$editing_nationality 		= $_POST['editing_nationality'];
	$editing_currentLocation 	= $_POST['editing_currentLocation'];
	$editing_phoneNumber 		= $_POST['editing_phoneNumber'];

	$_globalObj->_updateLocationProfile($editing_nationality,$editing_currentLocation,$editing_phoneNumber);
	$_globalObj->_feedbacks();
}
*/

// --- >> update password
if (isset($_POST['dataCurrentPassword']) && isset($_POST['dataNewPassword']) && isset($_POST['dataConfirmNewPassword']) && isset($_POST['dataInputEmailSession'])) 
{

	$dataCurrentPassword 		= $_POST['dataCurrentPassword'];
	$dataNewPassword 			= $_POST['dataNewPassword'];
	$dataConfirmNewPassword 	= $_POST['dataConfirmNewPassword'];
	$dataInputEmailSession	 	= $_POST['dataInputEmailSession'];

	$_globalObj->_updatecurrentPassword($dataCurrentPassword,$dataNewPassword,$dataConfirmNewPassword, $dataInputEmailSession);
	$_globalObj->_feedbacks();
}

// --- >> permanently delete account
if (isset($_POST['dataConfirmAgreement']) && isset($_POST['confirmDeletionPassword']) && isset($_POST['dataDeleteEmailSession'])) 
{
	$dataConfirmAgreement 		= $_POST['dataConfirmAgreement'];
	$confirmDeletionPassword 	= $_POST['confirmDeletionPassword'];
	$dataDeleteEmailSession 	= $_POST['dataDeleteEmailSession'];

	$_globalObj->_deleteUserAccount($dataConfirmAgreement,$confirmDeletionPassword,$dataDeleteEmailSession);
	$_globalObj->_feedbacks();
}

// --- >> return user created activities
// fetch jobs -- default list view
if (isset($_GET['sessionJobsValue']))
{
	$jobEmailSession     = $_GET['sessionJobsValue'];
	$_globalObj->sessionJobsList($jobEmailSession);
}
// fetch jobs -- grid view
if (isset($_GET['sessionJobsValueGrid'])) 
{
	$jobEmailSession = $_GET['sessionJobsValueGrid'];
	$_globalObj->sessionJobsGrid($jobEmailSession);
}
 // load job details for editing
if (isset($_POST['sessionEditJobArgs'])) 
{
	$sessionEditJobArgs = $_POST['sessionEditJobArgs'];

	$_globalObj->_sessionEditJobs($sessionEditJobArgs);
}

// --- >> edit created job
if (
	isset($_POST['dataEdit_job_have_a_business']) &&
	isset($_POST['dataEdit_job_id']) &&
	isset($_POST['dataEdit_job_resp_id']) &&
	//isset($_POST['dataEdit_job_businessname']) && 
	isset($_POST['dataEdit_job_title']) && 
	isset($_POST['dataEdit_job_district']) && 
	isset($_POST['dataEdit_job_location']) && 
	isset($_POST['dataEdit_job_type']) && 
	isset($_POST['dataEdit_job_salary']) && 
	isset($_POST['dataEdit_job_posted_on']) && 
	//isset($_POST['dataEdit_job_deadline']) && 
	isset($_POST['dataEdit_job_qualification']) && 
	isset($_POST['dataEdit_job_responsibility']) && 
	isset($_POST['dataEdit_job_duration']) && 
	isset($_POST['dataEdit_job_how_to_apply'])) 
{
	$job_have_a_business 	= $_POST['dataEdit_job_have_a_business'];
	$job_businessid 		= $_POST['dataEdit_job_businessname'];
	$job_title 				= $_POST['dataEdit_job_title'];
	$job_district 			= $_POST['dataEdit_job_district'];
	$job_location 			= $_POST['dataEdit_job_location'];
	$job_type 				= $_POST['dataEdit_job_type'];
	$job_salary 			= $_POST['dataEdit_job_salary'];
	$job_posted_on 			= $_POST['dataEdit_job_posted_on'];
	$job_deadline 			= $_POST['dataEdit_job_deadline'];
	$job_qualification 		= $_POST['dataEdit_job_qualification'];
	$job_responsibility 	= $_POST['dataEdit_job_responsibility'];
	$job_how_to_apply 		= $_POST['dataEdit_job_how_to_apply'];
	$job_duration 			= $_POST['dataEdit_job_duration'];
	$data_job_id 			= $_POST['dataEdit_job_id'];
	$data_job_resp_id 		= $_POST['dataEdit_job_resp_id'];


	$_globalObj->_updateJob($job_have_a_business, $job_businessid, $job_title, $job_district, $job_location, $job_type, $job_salary, $job_posted_on, $job_deadline, $job_qualification, $job_responsibility, $job_how_to_apply, $job_duration, $data_job_id, $data_job_resp_id);
	
	$_globalObj->_feedbacks();
}
// delete jobs
if (isset($_POST['sessionJobDeleteArgs'])) 
{
	$sessionJobDeleteArgs = $_POST['sessionJobDeleteArgs'];

	$_globalObj->_deleteSessionJobs($sessionJobDeleteArgs);
}


// ---- >> likes << -----
// --- fetch likes
if (isset($_GET['sessionLikesValue'])) 
{
	$likesEmailSession = $_GET['sessionLikesValue'];

	$_globalObj->_sessionLikes($likesEmailSession);
}


// --- >>  adverts
// fetch created adverts -- list view
if (isset($_GET['sessionAdvertsValue'])) 
{
	$advertsEmailSession = trim(strip_tags($_GET['sessionAdvertsValue']));

	$_globalObj->sessionCreatedAdvertsList($advertsEmailSession);
}

// fetch created adverts -- grid view
if (isset($_GET['sessionAdvertsValueGrid'])) 
{
	$advertsEmailSession = $_GET['sessionAdvertsValueGrid'];

	$_globalObj->sessionCreatedAdvertsGrid($advertsEmailSession);
}

// showing edit view for advert
if (isset($_POST['sessionAdvertEditArgs'])) 
{
	$sessionAdvertEditArgs = $_POST['sessionAdvertEditArgs'];

	$_globalObj->_editingSessionAdvert($sessionAdvertEditArgs);
}

// editing advert
if (
	isset($_POST['editAdvert_have_a_business']) && 
	isset($_POST['editAdvert_title']) && 
	//isset($_POST['dataFormAdvert_allowComments']) && 
	isset($_POST['editAdvert_publish']) && 
	isset($_POST['editAdvert_period']) && 
	isset($_POST['editAdvert_details']) &&
	//isset($_POST['editAdvert_bannersYesNo']) &&
	isset($_POST['editAdvert_loggedInEmail']) &&
	isset($_POST['editAdvert_currentAdvert'])
	/* && 
	isset($_POST['advert_bannerOne']) &&
	isset($_POST['advert_bannerTwo']) && 
	isset($_POST['advert_bannerThree'])*/
) 
{
	//$advert_businessName 	= $_POST['dataFormAdvert_businessName'];
	//$advert_allowComments 	= $_POST['dataFormAdvert_allowComments'];

	$advert_have_a_business = $_POST['editAdvert_have_a_business']; // yes or no
	$advert_businessName 	= $_POST['editAdvert_businessName'];

	$advert_title 			= $_POST['editAdvert_title'];
	$advert_publish 		= $_POST['editAdvert_publish'];
	$advert_period 		    = $_POST['editAdvert_period'];
	$advert_details 		= $_POST['editAdvert_details'];	
	$advert_loggedInEmail 	= $_POST['editAdvert_loggedInEmail'];
	$advert_currentAdvert 	= $_POST['editAdvert_currentAdvert'];
/*
	$advert_bannersYesNo 	= $_POST['editAdvert_bannersYesNo'];

	$advert_bannerName 		= $_FILES['advert_bannerOne']['name'];
	$advert_bannerTmp 		= $_FILES["advert_bannerOne"]["tmp_name"];
	$advert_bannerSize 		= $_FILES["advert_bannerOne"]["size"];

	$advert_bannerTwoName 	= $_FILES['advert_bannerTwo']['name'];
	$advert_bannerTwoTmp 	= $_FILES["advert_bannerTwo"]["tmp_name"];
	$advert_bannerTwoSize 	= $_FILES["advert_bannerTwo"]["size"];

	$advert_bannerThreeName = $_FILES['advert_bannerThree']['name'];
	$advert_bannerThreeTmp 	= $_FILES["advert_bannerThree"]["tmp_name"];
	$advert_bannerThreeSize = $_FILES["advert_bannerThree"]["size"];
	*/

	$_globalObj->_updateSessionAdvert(
		$advert_have_a_business,
		$advert_businessName,
		$advert_currentAdvert,
		$advert_title,  
		$advert_publish, 
		$advert_period, 
		$advert_loggedInEmail,
		$advert_details
		/*$advert_bannerName, 
		$advert_bannerTmp,
		$advert_bannerSize,
		$advert_bannerTwoName,
		$advert_bannerTwoTmp,
		$advert_bannerTwoSize, 
		$advert_bannerThreeName, 
		$advert_bannerThreeTmp, 
		$advert_bannerThreeSize,
		$advert_bannersYesNo*/);

	$_globalObj->_feedbacks();
}


// delete your advert
if (isset($_POST['sessionAdvertDeleteArgs'])) 
{
	$sessionAdvertDeleteArgs = $_POST['sessionAdvertDeleteArgs'];

	$_globalObj->_deleteSessionAdvert($sessionAdvertDeleteArgs);
}

// logout
/*
if (isset($_GET['logout'])) {
	$_globalObj->_logout();
}
*/