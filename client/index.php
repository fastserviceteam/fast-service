<?php
//session_start();
require "../_require-file.php"; // database config, server feedbacks, phpmailer class and other global constants
/**
* instantiate class
*
*/
$_globalObj = new globalClass();
/**
* check login status
*/
if ($_globalObj->_isLoggedIn() == false)
{
	header("Location: ../index.php");
}
else {
	include "dashboard.php";
}