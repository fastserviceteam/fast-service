
<?php 
//==========STEP TWO FOR PROCESS ONE=================

	global $mail;
			
	$i=0;$i++;
	//----------- getting the active / unviewed requests in notifications table------------------------------/ 
	$alert = GetAll("notifications","INNER JOIN services ON notifications.service_id = services.service_id AND notifications.viewed !='yes'");
	
	foreach($alert as $rw){
	
	//----------- getting vander ID, offering that service requested for ------------------------------/ 
	$cs = GetRow("SELECT * FROM business_profile WHERE Services_Offered LIKE '%".$rw->service_id."%' AND Business_Email = '".$mail."'");
	
	//----------- getting client ID, that requested for service  ------------------------------/ 
	$client = GetRow("SELECT * FROM client_quotation WHERE client_quotation_id = '$rw->client_id'");
	
	if(!empty($cs)){
	?>  						
          <form method="post" id="final_request<?=$i;?>">
            <div id="accordion">
              <div  style="padding:3px">
                <a data-toggle="collapse" data-parent="#accordion" href="#inbox<?=$i;?>">
                  <p>
                    <?=$i;?>.  Service Requested: 
                    <?=$rw->service_name; ?>  &nbsp; &nbsp; &nbsp; &nbsp; <small style="color:red">Read more..</small>
                    <span class="small pull-right">
                      <i class="fa ">  
                        <?= date('d-M, Y', (strtotime($rw->on_date)));?> 
                      </i>
                      </p>
                </a>			
              </div>		 
              <div id="inbox<?=$i;?>" class="alert well panel-collapse collapse">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="text" class="hidden form-control input-sm" name="vander_id" value="<?=$cs->business_profile_id;?>" />
                    <input type="text" class="hidden form-control input-sm" name="notifications_id" value="<?=$rw->notifications_id;?>" />
                    <input type="text" class="hidden form-control input-sm" name="send_client_id" value="<?=$rw->client_id;?>" />
                    <p> 
                      <?=$client->quotation_details; ?> 
                    </p>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-md-4">
						<label>Estimated Price  </label>
					   
						<select type="text" onchange="get_charge<?=$i;?>();" name="price_range" id="price_range<?=$i;?>" class="form-control" style="height:30px" required>
						  <option>
						  </option> 
						  <option value="5000">0 &nbsp; &nbsp; &nbsp; - &nbsp; &nbsp; 5,000
						  </option> 	
						  <option value="10000">5,001 &nbsp; &nbsp; &nbsp; - &nbsp; &nbsp; 10,000
						  </option> 
						  <option value="50000">10,001 &nbsp; &nbsp; - &nbsp; &nbsp; 50,000
						  </option> 
						  <option value="100000">50,001 &nbsp; &nbsp; - &nbsp; &nbsp; 100,000
						  </option> 
						</select>
					   <i class="fa">Service Charge : 
						  <span id="vat<?=$i;?>">0
						  </span> UGX
						</i>
					</div>
					
                    <div class="col-xs-12 col-sm-6 col-md-6">
						<input type="text" readonly class="hidden form-control" id="fs_charge<?=$i;?>" name="fs_charge" value=""></input>
						description
						<textarea class="form-control"  rows="2" name="vander_quotation" type="text" required></textarea>
                    </div>
					<div class="col-xs-12 col-sm-2 col-md-2">						
						<br>
						<button type="button"  onclick="final_send<?=$i;?>();" class="btn c_btn c_btnOn e btn-flat btn-sm  btn-primary" style="font-weight:bold;"> Send 
						</button>
                    </div>
                </div>
              </div>
              <script>
                function get_charge<?=$i; ?>() {
                  var price_range = document.getElementById('price_range<?=$i;?>').value;
                  var price_range2 =  document.getElementById('fs_charge<?=$i;?>').value = price_range * 0.03;
                  document.getElementById('vat<?=$i;?>').innerHTML = price_range * 0.03;
                }
                function final_request<?=$i;
                ?>(type, msg){
                  $("#xx").html('<center><i class="fa fa-spinner fa-spin"></i> Sending ... </center>');
                  setTimeout('window.location.href = "../client/dashboard.php"; ',4000);
                }
                function final_send<?=$i;
                ?>(){
                  if($("#fs_charge<?=$i;?>").val() ==''){
                    alert("Please Select your Price Range for this Service ! ")
                  }
                  else{
                    var accept_data = $("#final_request<?=$i;?>").serialize();
                    $.ajax({
                      type: "POST",
                      url: "final.php",
                      data: accept_data,
                      async: true,
                      cache: false,
                      beforeSend: function(){
                        $("#xx").html('<center><i class="fa fa-spinner fa-spin"></i> please wait ...</center>');
                      }
                      ,
                      success: function(data){
                        final_request<?=$i;
                        ?>("new", data);
                        notification_1();
                        notification_2();
                        notification_3();
                        notification_4();
                      }
                    }
                          );
                  }
                }
              </script>
              <script>  
                function denay_request_note<?=$i;
                ?>(type, msg){
                  $("#xx").html('<center><i class="fa fa-spinner fa-spin"></i> Cancelling</center>');
                  setTimeout('window.location.href = "../client/dashboard.php"; ',4000);
                }
                function denay_request<?=$i;
                ?>(){
                  var request_data = $("#final_request<?=$i;?>").serialize();
                  $.ajax({
                    type: "POST",
                    url: "vander_cancell_request.php",
                    data: request_data,
                    async: true,
                    cache: false,
                    beforeSend: function(){
                      $("#xx").html('<center><i class="fa fa-spinner fa-spin"></i> please wait ...</center>');
                    }
                    ,
                    success: function(data){
                      denay_request_note<?=$i;
                      ?>("new", data);
                      notification_1();
                      notification_2();
                      notification_3();
                      notification_4();
                    }
                  }
                        );
                }
              </script>
            </div>
          </form>
          <?php
		}
		$i++;
	}
	
	?> 