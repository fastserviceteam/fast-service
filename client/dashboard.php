<?php
//session_start();
require "../_require-file.php"; // database config, server feedbacks, phpmailer class and other global constants
require "../_server-functions.php"; // custom functions
/**
* instantiate class
*
*/
$_globalObj = new globalClass();
/**
* user email sesion; i.e. blahblah@yayay.com :
* for truncated version (without domain); use $_SESSION['emailUname']
*
*/
$mail = $_SESSION['ur_email'];

/**
* check login status
*/
if ($_globalObj->_isLoggedIn() == false)
{
header("Location: ../index.php");
}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Fast Service
        </title>
        <!-- Custom Theme Style -->
        <link href="_assets/css/style3.css" rel="stylesheet">
        <!-- /. bootstrap multiselect plugin -->
        <link rel="stylesheet" href="_assets/plugins/bootstrap-multiselect/bootstrap-multiselect.css" />
        <!-- /. jquery-ui -->
        <link rel="stylesheet" href="_assets/plugins/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
        <!-- /. Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />

        <!-- /. for css which should appear on every page; include them in this file -->
        <?php include "_partials/_topCss.php"; ?>
            <style>
                #infowindow-content .title {
                    font-weight: bold;
                }
                
                #infowindow-content {
                    display: none;
                }
                
                #map #infowindow-content {
                    display: inline;
                }
                
                div.stepContainer {
                    height: 20px !important;
                }
                
                #map {
                    height: 170px !important;
                    border: 1px solid #ccc;
                    width: 100% !important;
                }
                
                #description {
                    font-family: Roboto;
                    font-size: 15px;
                    font-weight: 300;
                }
                
                #infowindow-content .title {
                    font-weight: bold;
                }
                
                #infowindow-content {
                    display: none;
                }
                
                #map #infowindow-content {
                    display: inline;
                }
            </style>
    </head>

    <body class="nav-md fixed_nav">
        <div class="container body">
            <div class="main_container">

                <?php 
          # include left sidebar main navigation menu
          require "_partials/_leftColumnNavigationMenu.php"; 

          # include top navigation
          require "_partials/_topNavigation.php"; 
        ?>

                    <!-- page content -->
                    <div class="right_col page_content" role="main" id="dashboard_content" style="color:#555 ! important;">
                        <div id="wizard_custom_wrapper">
                            <!-- <<<<<<< HEAD -->
                            <div class="row">
                                <div class="col-md-8 col-sm-12 col-lg-8 col-xs-12">
                                    <?php
                global $mail;
                $reqsts = GetRow("SELECT * FROM client_quotation WHERE client_email = '$mail' AND status ='0' ORDER BY client_quotation_id DESC");
                //===========show process one for searching service ===============
                ?>
                                        <section class="panel form-wizard" id="w1">
                                            <div class="panel-header panel-header-custom">
                                                <center><span class="text-center" style="font-size:1.5vw; font-weight:600;color:#4FC3F7;">Find a Service</span></center>
                                            </div>
                                            <div class="panel-body">
                                                <div class="wizard-tabs">
                                                    <ul class="wizard-steps" style="margin-top:-3.5%">
                                                        <li class="active">
                                                            <a href="#locate_a_service_panel" data-toggle="tab" class="text-center">
                                                                <span class="badge hidden-xs">1
                            </span>
                                                                <b>Location</b>

                                                                <!-- ======= if this was the last changes you made, please uncomment out this up to comment marked ">> Ends here <<"
          <div class="row">  
            <div class="col-md-8 col-sm-8 col-xs-12">

                    <?php
/* global $mail;
$reqsts = GetRow("SELECT * FROM client_quotation WHERE client_email = '$mail' AND status ='0' ORDER BY client_quotation_id DESC");
//===========show process one for searching service ===============
?>
        <section class="panel form-wizard" id="w1">
            <div class="panel-header panel-header-custom">
              <center><span class="text-center" style="font-size:1.5vw;  font-weight:600;color:#4FC3F7;">Find a Service</span></center>
            </div>
                      <div class="panel-body">
                        <div class="wizard-tabs">
                          <ul class="wizard-steps" style="margin-top:-3.5%">
                            <li class="active">
                              <a href="#locate_a_service_panel" data-toggle="tab" class="text-center">
                                <span class="badge hidden-xs">1
                                </span>
                                <b>Location</b>
                              </a>
                            </li>
                            <li>
                              <a href="#request_for_a_service_panel" data-toggle="tab" class="text-center">
                                <span class="badge hidden-xs">2
                                </span>
                                <b>Request for Service </b>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <form class="form-horizontal" novalidate="novalidate" id="client_process_request" role="form" method="POST" action="">
                          <?php
  if(!empty($reqsts)){
  $n_id = $reqsts->client_quotation_id + 1;
  echo '<input type="text" class="hidden form-control input-sm" name="client_request_id" value="'.$n_id.'" />';
  }else{
  echo '<input type="text" class="hidden form-control input-sm" name="client_request_id" value="1"  />';
  }
  ?>

      <div class="tab-content">

          <div id="locate_a_service_panel" class="tab-pane active">

                <div class="form-group">
                  <label for="address">Where do you need Services?</label>
                  <div class="input-group">
                    <input type="text" class="form-control" autofocus id="address" name="service_address" placeholder="please enter location" required>                            
                    <span id="service_address"></span>
                    <span class="input-group-btn">
                       <button type="button" class="btn btn-success" id="submit">
                       <i class="fa fa-map-marker">  My location</i></button>
                    </span>
                  </div>
                  <span class="help-block error-notice"></span>
                </div>
                <!-- / hidden inputs -->
                                                                <input type="hidden" id="location" name="location" value="Kampala Uganda">
                                                                <input type="hidden" id="latlng" name="latlng">
                                                                <input type="hidden" id="longitude" name="longitude">
                                                                <input type="hidden" id="latitude" name="latitude">
                                                                <input type="hidden" id="get_client_email" name="client_email" value="<?=$mail;?>">
                                                                <!-- / hidden inputs -->

                                                                <!-- /. map view -->
                                                                <div class="form-group">
                                                                    <div id="map"></div>
                                                                </div>

                                                </div>
                                                <!-- /. locate_a_service_panel -->

                                                <!-- /. request_for_a_service_panel -->
                                                <div id="request_for_a_service_panel" class="tab-pane">

                                                    <div class="form-group">
                                                        <label for="search-box">What Service do your need ?</label>
                                                        <input type="text" onkeyup="psearch();" name="keyword" class="form-control" id="search-box" required placeholder="Search for service..." autocomplete="off">
                                                        <div id="suggesstion-box"></div>
                                                        <span id="return_available_services"></span>
                                                    </div>

                                                    <!-- / hidden inputs -->
                                                    <input type="hidden" name="is_logged_in" id="is_logged_in" value="<?=$mail;?>" />
                                                    <!-- / hidden inputs -->

                                                    <span id="to_sending"></span>
                                                    <br />

                                                </div>
                                                <!-- /. request_for_a_service_panel -->

                                            </div>
                                            </form>
                                </div>

                                <div class="panel-footer">
                                    <ul class="pager" style="margin-bottom:-1px; margin-top:-1px; font-weight:bold;">
                                        <li class="previous disabled">
                                            <a>
                                                <i class="fa fa-angle-left">
                            </i> &nbsp; Previous
                                                <?php */ ?>
                                                    >>>>>>> f73f6134e4b8bdf462109110e086da5028d342c5 >> Ends here
                                                    << -->

                                            </a>
                                        </li>
                                        <li>
                                            <a href="#request_for_a_service_panel" data-toggle="tab" class="text-center">
                                                <span class="badge hidden-xs">2
                            </span>
                                                <b>Request for Service </b>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <form class="form-horizontal" novalidate="novalidate" id="client_process_request" role="form" method="POST" action="">
                                    <?php
                      if(!empty($reqsts)){
                      $n_id = $reqsts->client_quotation_id + 1;
                      echo '<input type="text" class="hidden form-control input-sm" name="client_request_id" value="'.$n_id.'" />';
                      }else{
                      echo '<input type="text" class="hidden form-control input-sm" name="client_request_id" value="1"  />';
                      }
                      ?>
                                        <div class="tab-content">
                                            <div id="locate_a_service_panel" class="tab-pane active">

                                                <div class="form-group">
                                                    <label for="dataServiceRequest-myLocationId"><i class="fa fa-map-marker"></i> My Location</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="dataServiceRequest-myLocationId" name="dataServiceRequest_myLocation" placeholder="your location" required>
                                                        <span id="service_address"></span>
                                                        <span class="input-group-btn">
                                <button type="button" class="btn btn-flat btn-success" id="getCurrentLocation-btn">
                                <i class="fa fa-search"></i> Go!</button>
                              </span>
                                                    </div>
                                                    <span class="help-block error-notice"></span>
                                                </div>

                                                <div class="form-group">
                                                    <label for="address"><i class="fa fa-map-marker"></i> Where do you need Services?</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="address" name="service_address" placeholder="please enter location" required>
                                                        <span id="service_address"></span>
                                                        <span class="input-group-btn">
                                <button type="button" class="btn btn-flat btn-success" id="submit">
                                <i class="fa fa-search"></i> Go!</button>
                              </span>
                                                    </div>
                                                    <span class="help-block error-notice"></span>
                                                </div>

                                                <!-- / hidden inputs -->
                                                <input type="hidden" id="location" name="location" value="Kampala Uganda">
                                                <input type="hidden" id="latlng" name="latlng">
                                                <input type="hidden" id="longitude" name="longitude">
                                                <input type="hidden" id="latitude" name="latitude">
                                                <input type="hidden" id="get_client_email" name="client_email" value="<?=$mail;?>">
                                                <!-- / hidden inputs -->

                                                <!-- /. map view -->
                                                <div class="form-group">
                                                    <div id="map"></div>
                                                </div>
                                            </div>
                                            <!-- /. locate_a_service_panel -->

                                            <!-- /. request_for_a_service_panel -->
                                            <div id="request_for_a_service_panel" class="tab-pane">
                                                <div class="form-group">
                                                    <label for="search-box">What Service do your need ?</label>
                                                    <input type="text" onkeyup="psearch();" name="keyword" class="form-control" id="search-box" required placeholder="Search for service..." autocomplete="off">
                                                    <div id="suggesstion-box"></div>
                                                    <span id="return_available_services"></span>
                                                </div>
                                                <!-- / hidden inputs -->
                                                <input type="hidden" name="is_logged_in" id="is_logged_in" value="<?=$mail;?>" />
                                                <!-- / hidden inputs -->
                                                <span id="to_sending"></span>
                                                <br />
                                            </div>
                                            <!-- /. request_for_a_service_panel -->
                                        </div>
                                </form>
                            </div>

                            <div class="panel-footer">
                                <ul class="pager" style="margin-bottom:-1px; margin-top:-1px; font-weight:bold;">
                                    <li class="previous disabled">
                                        <a>
                                            <i class="fa fa-angle-left">
                          </i> &nbsp; Previous
                                        </a>
                                    </li>
                                    <li id="finishs" class="finishs hiddens pull-right" style="display:none">
                                        <button type="button" onclick="send_request();" class="c_btn c_btnOne btn-flat pull-right btn btn-md">
                                            <b>Send Request
                        </b>
                                        </button>
                                    </li>
                                    <li class="next">
                                        <a>Next &nbsp;
                          <i class="fa fa-angle-right">
                          </i>
                        </a>
                                    </li>
                                </ul>
                            </div>
                            </section>

                            <!-- /. row : notifications -->
                            <?php
                // --- >> messages
                require "_partials/_notification.php";
                ?>
                        </div>
                        <!-- /. col-md-4 col-sm-42 col-xs-12 -->
                        <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12">
                            <?php include '_partials/_rightColumnAdvertsJobs.php'; ?>
                        </div>
                        <!-- /. col-md-4 col-sm-4 col-xs-12 -->
                    </div>
                    <!-- /. row -->
            </div>
            <!-- /. wizard_custom_wrapper -->

            <!-- /. to include the notification with 100% width uncomment this -->
            <!--
              <div class="row">
                  <div class="col-xs-12 col-md-12">
              <?php
                // --- >> messages
                // require "_partials/_notification.php";
              ?>
            </div>
            -->
            <!--
            / col-xs-12 col-md-12 -->
            <!--
              </div>
          -->
            <!-- / row -->

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                All rights reserved &copy;
                <?php echo date('Y'); ?>
                    <a href="javascript:void(0)">Fast Service
            </a>
            </div>
            <div class="clearfix">
            </div>
        </footer>
        <!-- /footer content -->
        </div>
        <!-- main_container -->
        </div>
        <!-- .container .body #main_wrapper -->
        <!-- >> include bottom scripts << -->
        <?php require_once "_partials/_bottomScripts.php"; ?>
            <!-- Specific Page Vendor -->
            <script src="_assets/plugins/jquery-validation/jquery.validate.js">
            </script>
            <script src="_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js">
            </script>
            <!-- Examples -->
            <script src="_assets/js/forms/examples.wizard.js">
            </script>
            <!-- >> page specific scripts << -->
            <script>
                $(function() {
                    $('#dateofservicedelivery').datetimepicker({
                        format: 'YYYY-MM-DD'
                    });
                    $('#timeofservicedelivery').datetimepicker({
                        format: 'hh:mm A'
                    });
                    // --- >> typeahead autocomplete
                    var $inputSearchValue = $("#dataForm_servicerequired");
                    $inputSearchValue.typeahead({
                        source: function(query, result) {
                            $.ajax({
                                url: "_server_requests.php",
                                data: {
                                    servicesOffered: query
                                },
                                dataType: "json",
                                type: "POST",
                                success: function(data) {
                                    result($.map(data, function(item) {
                                        return item;
                                    }));
                                }
                            });
                        }
                    });
                });

                function psearch() {
                    var mydata = $("#client_process_request").serialize();
                    $.ajax({
                        type: "POST",
                        url: "livesearch.php",
                        //data:'keyword='+$(this).val(),
                        data: mydata,
                        beforeSend: function() {
                            // $("#search-box").css("background","#FFF url(_assets/images/LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data) {
                            $("#return_available_services").show();
                            $("#suggesstion-box").html(data).show();
                            // assign width of search-box to search results
                            var getSearchboxWidth = $("#search-box").innerWidth();
                            console.log(getSearchboxWidth)
                                //$("#search-list li").css("width", getSearchboxWidth + "px");
                        }
                    });
                }

                function finishs() {
                    $("#finishs").show();
                }

                function message_note(type, msg) {
                    $("#to_sending").html('<span style="color:red"><i class="fa fa-spinner fa-spin"></i> Processing your request, Please wait ...</span>');
                    setTimeout('window.location.href = "dashboard.php"; ', 4000);
                }

                function send_request() {
                    $("#to_sending").show();
                    var request_data = $("#client_process_request").serialize();
                    $.ajax({
                        type: "POST",
                        url: "submit_request.php",
                        data: request_data,
                        async: true,
                        cache: false,
                        beforeSend: function() {
                            $("#to_sending").html('<i class="fa fa-spinner fa-spin"></i> please wait ...');
                        },
                        success: function(data) {
                            message_note("new", data);
                        }
                    });
                }
            </script>
            <!-- >> google geolocation << -->
            <script src="_assets/js/maps.js">
            </script>
            <!-- >> api key << -->
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeWLbDlNxutMqYPmgXHETvG-PXuJT9_Q8&libraries=places&callback=initMap" async defer></script>
            <!-- /. messages / notification -->
            <script src="_assets/js/_notification.js"></script>
    </body>

    </html>