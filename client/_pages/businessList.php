<?php
session_start();
require "../_require-file.php"; // database config, server feedbacks, phpmailer class and other global constants
require "../_server-functions.php"; // custom functions
/**
* user email sesion; i.e. blahblah@yayay.com : 
* for truncated version (without domain); use $_SESSION['emailUname']
*
*/
$mail = $_SESSION['ur_email']; 
global $mail;
/**
* instantiate object
*
*/
$_globalObj = new globalClass();
/**
* check login status
*/
if ($_globalObj->_isLoggedIn() == false) 
{
header("Location: ../index.php");        
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fast Service | Job List
    </title>
    <!-- /. Favicon --> 
    <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
    <!-- /. include general css -->
    <?php include "_partials/_topCss.php"; ?>
  </head>
  <body class="nav-md fixed_nav">
    <div class="container body">
      <div class="main_container">

        <?php 
          # include left sidebar main navigation menu
          require "_partials/_leftColumnNavigationMenu.php"; 

          # include top navigation
          require "_partials/_topNavigation.php"; 
        ?>
        <!-- page content -->
        <div class="right_col page_content" role="main">
          <div class="row" id="businessList_id">  
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h3>My Businesses
                  </h3>
                </div>
                <div class="x_content">
                  <span id="show_dataForm_Business_replies">
                  </span>
                  <table class="table table-bordered table-condensed table-hover dataTableGeneral">
                    <thead class="bg-olive text-white">
                      <tr>
                        <th>Date
                        </th>
                        <th>Category
                        </th>
                        <th>Name
                        </th>
                        <th>Address
                        </th> 
                        <th>Tel
                        </th>
                        <th>Services
                        </th>
                        <th>Location
                        </th>
                        <th>Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
$my_bzns = GetAll("business_profile","WHERE Business_Email = '$mail' ORDER BY business_profile_id DESC");
$i=1;
foreach($my_bzns as $row){
?>      
                      <script>     
                        function return_editing<?=$i;
                        ?>(){
                          $("#bzn_table_form").hide();
                          var bzn_id<?=$i;
                          ?> ='edit_id='+ document.getElementById('edit_id<?=$i;?>').value;
                          $.ajax({
                            type: "POST",
                            url: "business/edit_business.php",
                            async: true,
                            data: bzn_id<?=$i;
                            ?>, 
                            cache: false, 
                            success: function(data){
                            show_edit("new", data);
                          }
                                 }
                                );
                        }
                        function show_edit(type, msg){
                          $('#show_edit').html(msg);
                        }
                        function return_review<?=$i;
                        ?>(){
                          $("#bzn_table_form").hide();
                          var bzn_review<?=$i;
                          ?> ='edit_id='+ document.getElementById('edit_id<?=$i;?>').value;
                          $.ajax({
                            type: "POST",
                            url: "business/review.php",
                            async: true,
                            data: bzn_review<?=$i;
                            ?>, 
                            cache: false, 
                            success: function(data){
                            show_edit("new", data);
                          }
                                 }
                                );
                        }
                        function show_edit(type, msg){
                          $('#show_edit').html(msg);
                        }
                      </script>
                      <input name="edit_id" type="text" class="hidden" id="edit_id<?=$i;?>" value="<?=$row->business_profile_id;?>">
                    </input>
                  <tr>
                    <td>
                      <?=date('d-M, Y', (strtotime($row->on_date)));?>
                    </td>
                    <td>
                      <?=$row->Business_Category;?>
                    </td>
                    <td>
                      <?=$row->Business_Name;?> 
                    </td>
                    <td>
                      <?=$row->Business_Address;?>
                    </td>
                    <td>
                      <?=$row->Business_Phone;?>
                    </td>
                    <td>
                      <?=$row->Services_Offered;?> 
                    </td>
                    <td>
                      <?=$row->Geographical_Area;?> 
                    </td>
                    <td>
                      <button onclick="return_editing<?=$i;?>();" type="button"  class="btn btn-xs btn-default text-primary" title="Update / Edit">
                        <i class="fa fa-pencil text-primary">
                        </i> 
                      </button>
                      <button onclick="return_review<?=$i;?>();" type="button"  class="btn btn-xs btn-default text-primary" title="Review">
                        <i class="fa fa-eye text-warning">
                        </i> 
                      </button>
                      <form method="post" action="business/submit.php">
                        <input type="text" name="business_get_id" value="<?=$row->business_profile_id;?>" class="blu form-control hidden"  required >
                        <button  type="submit" name="delete_business" class="btn btn-xs btn-default" title="delete">
                          <i class="fa fa-trash text-danger">
                          </i> 
                        </button>
                      </form>
                    </td>
                  </tr> 
                  <?php
$i++;         
}
?>  
                  </tbody>
                </table>
            </div>
            <!-- /x_content-->
          </div>
          <!-- /x_panel-->
        </div>
        <!-- /.col-md-12 col-sm-12 col-xs-12--> 
      </div>
      <!--/ row -->
    </div>
    <!-- /page_content-->
    <!-- footer content -->
    <footer>
      <div class="pull-right">
        All rights reserved &copy; 
        <?php echo date('Y'); ?> 
        <a href="javascript:void(0)">Fast Service
        </a>
      </div>
      <div class="clearfix">
      </div>
    </footer>
    <!-- /footer content -->
    </div>
  <!-- main_container -->
  </div>
<!-- .container .body #main_wrapper -->
<!-- >> include bottom scripts << -->
<?php require_once "_partials/_bottomScripts.php"; ?>
</body>
</html>
