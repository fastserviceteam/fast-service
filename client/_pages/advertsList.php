<?php
    session_start();

    require "../_require-file.php"; 
    // --- >> instantiate object
    $_globalObj = new globalClass();

    if ($_globalObj->_isLoggedIn() == false) 
    {
       header("Location: ../index.php");        
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Fast Service | Client</title>
  <!-- /. Favicon --> 
  <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
  <!-- bootstrap-daterangepicker -->
  <link href="_assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

  <!-- /. for css which should appear on every page; include them in this file -->
  <?php include "_partials/_topCss.php"; ?>

  <style>
  div.pagination {
    font-family: "Lucida Sans", Geneva, Verdana, sans-serif;
    padding:20px;
    margin: 5px 2px 5px 2px;
    font-size: 17px;
    color: #3d9970;
    float: right;
  }
  div.pagination a {
    margin: 2px;
    padding: 10px 16px;
    font-size: 16px;
    background-color: #d81b60;
    text-decoration: none;
    color: #fff;
    border: 1px solid #ddd;
  }
  div.pagination a:hover/*, div.pagination a:active*/ {
    border-radius: 50%;
    background-color: #fff;
    border-color: #d81b60;
    color: #d81b60;
    -webkit-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    transition: all 0.3s ease;
  }
  /*
  div.pagination span.current, div.pagination a.active {
    padding: 0.5em 0.64em 0.43em 0.64em;
    margin: 2px;
    background-color: #f6efcc;
    color: #d81b60 !important;
    text-decoration: underline;
  }
  */
  div.pagination span.disabled {
    display:none;
  }
   div.pagination a:active{
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #337ab7;
    border-color: #337ab7;
  }
  </style>

</head>
  <body class="nav-md fixed_nav">

   <div class="container body">
      <div class="main_container">

        <?php 
          # include left sidebar main navigation menu
          require "_partials/_leftColumnNavigationMenu.php"; 

          # include top navigation
          require "_partials/_topNavigation.php"; 
        ?>

        <!-- page content -->
        <div class="right_col page_content" role="main">
          
          <div class="pageTitle-Wrapper-Class">

            <div class="page-title">
              <div class="title_left">
                <h3><i class="fa fa-info-circle"></i> Adverts <small></small></h3>
              </div>

                <!-- search advert text box -->
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <input type="text" class="form-control" placeholder="Search for adverts..." id="dataInput_advertFilter" onkeyup="searchFilter()">
                </div>
              </div>
            </div>

          </div>
<div class="clearfix"></div>
  <div class="row">


    <div class="col-md-12">
      <div class="x_panel">

          <div class="x_title">
              <h2 class="text-navy">Active Adverts</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <select id="sort_Adverts_select" onchange="searchFilter()" class="form-control">
                          <option value=""> --- Order By --- </option>
                          <option value="asc">Old Adverts first</option>
                          <option value="desc">New Adverts first</option>
                        </select>                      
                    </li>
                    <!--
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle order-action-span" title="Sort" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-sort fa-1x text-info"></i> <span class="text-info">Order by</span></a>
                      <ul class="dropdown-menu" role="menu">
                        
                        <li><a href="javascript:void(0)" onclick="sortAvertsDescfunc()">New Adverts First</a>
                        </li>
                        <li><a href="javascript:void(0)" onclick="sortAdvertsAscfunc()">Old Adverts First</a>
                        </li>
                      </ul>
                    -->
                    </li>
                    <li><a href="javascript:void(0)" id="order-action-span-value" class="order-action-span" id="order-action-span-value"></a></li>
                  </ul>

              <div class="clearfix"></div>
          </div>
    
        <div class="x_content">

            <div class="row">

              <div class="col-md-12">


<div class="loadingAdverts_wrapper element-hidden">
    <img src="_assets/images/loading.gif" alt="Loading adverts..." class="">
</div>

                <div id="adverts-wrapper-server-filter">
<?php 
    // load adverts list
    $_globalObj->_returnAdvertList();
?>
                </div>

                </div>  
                <!-- / col-md-12 -->     
            </div>
            <!-- / row -->

        </div>
      </div>
      
    </div>

  </div>
  <!-- /. row -->

</div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)">Fast Service</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
      <!-- main_container -->

    </div>
   <!-- .container .body #main_wrapper -->




    <!-- ================ >> page specific scripts << ================ -->

    <script>

    /*
      // --- >> default
      function advertWindowOnScroll() {
           $(window).on("scroll", function(e){
            if ($(window).scrollTop() == $(document).height() - $(window).height()){
                if($(".adverts_scroll_wrapper").length < $("#dataAdverts_totalCount").val()) {
                    
                    var lastId = $(".adverts_scroll_wrapper:last").attr("id");
                    
                    loadMoreAdverts(lastId);
                }
            }
        });
      };

      function loadMoreAdverts(lastId) {
          $(window).off("scroll");
          $.ajax({
              url: '_server_requests.php',
              type: "post",
              data: {advertLastId:lastId},
              beforeSend: function ()
              {
                  $('.loadingAdverts_wrapper').fadeIn();
              },
              success: function (data) {
                  setTimeout(function() {
                      $('.loadingAdverts_wrapper').fadeOut();
                      $("#advertsListContent").append(data);
                  advertWindowOnScroll();
                   }, 1000);
              }
         });
      };

      // --- >> order sort << ---- //
      function advertWindowFilterScroll() {
           $(window).on("scroll", function(e){
            if ($(window).scrollTop() == $(document).height() - $(window).height()){
                if($(".adverts_filter_scroll_wrapper").length < $("#dataAdverts_totalSortedCount").val()) {
                    
                    var lastDateOrder = $(".adverts_filter_scroll_wrapper:last").attr("id");
                    var dataOrderValue = $("#dataOrderValue").val();
                    
                    loadMoreAdvertsSorted(lastDateOrder, dataOrderValue);
                }
            }
        });
      };

      // --- >> load more sorted values << --- //
      function loadMoreAdvertsSorted(lastDateOrder, dataOrderValue) {
        console.log(lastDateOrder)
          $(window).off("scroll");
          $.ajax({
              url: '_server_requests.php',
              type: "post",
              data: {advertLastOrderDate:lastDateOrder, dataOrderValue:dataOrderValue},              
              success: function (data) {
                  setTimeout(function() {
                    $('.loadingAdverts_wrapper_order').fadeOut();
                    $("#advertsListContentFilter").append(data);
                  advertWindowFilterScroll();
                   }, 1000);
              }
         });
      };
      

    // --- >> order jobs << --- //

      //  descending order
      function sortAvertsDescfunc()
      {
        var orderDescending = "desc";

        $.ajax({
            type: 'post',
            url: '_server_requests.php',
            data:{orderingAdvertsValue: orderDescending},
            success: function (data) {
                $("#order-action-span-value").html('<i class="fa fa-angle-right"></i> <span>New Adverts First</span>');
                $('#adverts-wrapper-server-filter').html(data);
            }
        });
      };
      //  ascending order
      function sortAdvertsAscfunc()
      {
        var orderAscending = "asc";

        $.ajax({
            type: 'post',
            url: '_server_requests.php',
            data:{orderingAdvertsValue: orderAscending},
            success: function (data) {
                $("#order-action-span-value").html('<i class="fa fa-angle-right"></i> <span>Old Adverts First</span>');
                $('#adverts-wrapper-server-filter').html(data);
            }
        });
      };
      */

      // --- >> search for adverts << --- /
      /*
      function adverts_search_func() 
      {
          // initialize autocomplete with custom appendTo
          $('#dataInput_advertFilter').autocomplete({
            serviceUrl: "_server_requests.php",
            type: "POST",
            onSelect: function (suggestion) {
              console.log('You selected: ' + suggestion.key + ', ' + suggestion.value);
              //$('#dataInput_advertFilter').val('');
              loadAdvertDetailsfunction(suggestion.key);
              $("#dataModalAdverts").modal("show");
              }
          });      
      };
      */
    function searchFilter(page_num) {

        page_num = page_num ? page_num : 0;

        var keywords = $('#dataInput_advertFilter').val();
        var sortBy   = $('#sort_Adverts_select').val();
        $.ajax({
            type: 'POST',
            url: '_server_requests.php',
            data:'page='+page_num+'&keywords='+keywords.toLowerCase()+'&sortBy='+sortBy,
            beforeSend: function () {
                $('.loadingAdverts_wrapper').show();
            },
            success: function (html) {
                $('#adverts-wrapper-server-filter').html(html);
                $('.loadingAdverts_wrapper').fadeOut("slow");
            }
        });
    }
          
    </script>

    <!-- >> include bottom scripts << -->
    <?php require_once "_partials/_bottomScripts.php"; ?>
    
    <!-- jQuery autocomplete -->
    <script src="_assets/plugins/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    
    <script>      
      $(function()
      {          
          // adverts search
          //adverts_search_func();        

          // infinite scrolling init for default view
          //advertWindowOnScroll();

          // infinite scroll for filter view
          //advertWindowFilterScroll();
      });
    </script>


</script>
</body>
</html>