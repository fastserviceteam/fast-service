<?php
    session_start();

    require "../_require-file.php"; 
      // --- >> instantiate object
    $_globalObj = new globalClass();

    if ($_globalObj->_isLoggedIn() == false) 
    {
       header("Location: ../index.php");        
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Fast Service | Client</title>
  <!-- /. Favicon --> 
  <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
  <!-- bootstrap-daterangepicker -->
  <link href="_assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <!-- /. include general css -->
  <?php include "_partials/_topCss.php"; ?>


    <style>
    	#span_contents_likes,
    	#span_contents_views {
		  color: #337ab7;
		  background-color: #fff;
		}
        /* override bootstrap default form-group style*/
        #dataForm_createNewAdvert .form-group {
            margin-bottom: 18px;
        }

        .imagePreviewClass {
            width: 200px !important;
            height: 200px !important;
            background-position: center center;
            background:url(_assets/images/pic_blank.jpg);
            background-color:#fff;
            background-size: cover;
            background-repeat:no-repeat;
            display: inline-block;
        }
        .selectFileToUploadClass
        {
          display:block;
          width: 200px !important;
          border-radius:0px;
          box-shadow:0px 2px 6px 2px rgba(0,0,0,0.25);
          margin-top:-5px;
          color: #fff !important;
        }
        .iclassBanners
        {
          position:relative;
           bottom: 225px;
          left: 167px;
          width:30px;
          height:30px;
          border-radius: 50%;
          text-align:center;
          line-height:30px;
          background-color:rgba(0, 0, 0,0.6);
          cursor:pointer;
          color: #ffffff;
        }
        .displayClass {
          visibility: hidden;            
        }
        #toggleBannerUpload_formGroup{
            display: none;
        }
    </style>
</head>
  <body class="nav-md fixed_nav">

   <div class="container body">
      <div class="main_container">

        <?php 
          # include left sidebar main navigation menu
          require "_partials/_leftColumnNavigationMenu.php"; 

          # include top navigation
          require "_partials/_topNavigation.php"; 
        ?>

        <!-- page content -->
        <div class="right_col page_content" role="main">
          
          <div class="">

            <div class="page-title">
              <div class="title_left">
                <h3><i class="fa fa-info-circle"></i> Adverts <small>create advert</small></h3>
              </div>

              <div class="title_right">
                <!-- other content -->
              </div>
            </div>

          </div>

  <div class="row">


    <div class="col-md-8 col-sm-8 col-xs-12">
      <div class="x_panel tile">

        <div class="x_title">
          <h3>Create New Advert</h3>
        </div>

        <div class="x_content">

            <form id="dataForm_createNewAdvert" class="form-vertical  form-label-left" role="form" enctype="multipart/form-data">

                <!-- hidden - fields -->
                <input type="hidden" value="<?php echo $_SESSION["ur_email"]; ?>" name="dataFormAdvert_loggedInEmail">
                <input type="hidden" value="no" name="dataFormAdvert_publish">
                <!-- hidden - fields -->
 
              <div class="form-group">
                  <label for="dataFormAdvert_have_a_businessId">Do you have a business?</label>
                    <div id="dataFormAdvert_have_a_businessId">
                      <label class="radio-inline"><input type="radio" class="dataFormAdvert_have_a_businessClass" name="dataFormAdvert_have_a_business" value="yes">Yes</label>
                      <label class="radio-inline"><input type="radio" class="dataFormAdvert_have_a_businessClass" name="dataFormAdvert_have_a_business" value="no">No</label>                      
                    </div>
              </div>
      
              <div class="form-group item_display_none" id="business_name_toggle">
                  <label for="dataFormAdvert_businessName">Business Name</label>
                    <select name="dataFormAdvert_businessName" id="dataFormAdvert_businessName" class="form-control" style="width: 100%" data-placeholder="--- select business ---">
                      <option></option>
                      <?php foreach( $_globalObj->_businessnames() as $row ) { ?>
                          <option value="<?php echo $row['business_profile_id']; ?>"><?php echo $row['Business_Name']; ?></option>
                      <?php } ?>
                    </select>
              </div>

                <div class="form-group">
                  <label for="dataFormAdvert_title">Advert Title</label>
                  <input type="text" id="dataFormAdvert_title" class="form-control" name="dataFormAdvert_title" required placeholder="enter advert title" autocomplete="off" aria-describedby="advertTitle_help" maxlength="120"> 
                  <small class="text-danger" id="advertTitle_help">120 characters ONLY</small>                  
                </div>

            <!--
                <div class="form-group">
                    <label for="dataFormAdvert_allowCommentsid">Allow comments</label>
                    <div id="dataFormAdvert_allowCommentsid">
                        <label class="radio-inline"><input type="radio" value="yes" name="dataFormAdvert_allowComments">Yes</label>
                        <label class="radio-inline"><input type="radio" value="no" name="dataFormAdvert_allowComments">No</label>
                    </div>
                </div>
            -->
            <!--
                <div class="form-group">
                    <label for="dataFormAdvert_publishid">Publish</label>
                    <div id="dataFormAdvert_publishid">
                        <label class="radio-inline"><input type="radio" value="yes" name="dataFormAdvert_publish">Publish now</label>
                        <label class="radio-inline"><input type="radio" value="no" name="dataFormAdvert_publish">Publish later</label>
                    </div>
                </div>
              -->


                <div class="form-group">
                    <label for="dataFormAdvert_period">Advert Period</label>
                      <div class="controls">
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="dataFormAdvert_period" id="dataFormAdvert_period" class="form-control daterange_picker_one" value="<?php echo date('Y-m-d').' - '.date('Y-m-d'); ?>" />
                        </div>
                      </div>
                </div>

                <div class="form-group">
                    <label for="dataFormAdvert_details">Advert Details</label>

                  <textarea id="dataFormAdvert_details" class="form-control bp-wysihtml5" aria-describedby='helperInfor_characters' name="dataFormAdvert_details"></textarea>
                
                    <small class="text-danger" id="helperInfor_characters">800 characters ONLY</small>

                </div>

                <div class="form-group">
                    <label for="dataFormAdvert_bannersToUploadid">Do you have banners to upload?</label>
                    <div id="dataFormAdvert_bannersToUploadid">
                        <label class="radio-inline"><input type="radio" value="yes" name="dataFormAdvert_bannersYesNo" class="dataclass_banners">Yes</label>
                        <label class="radio-inline"><input type="radio" value="no" name="dataFormAdvert_bannersYesNo" class="dataclass_banners">No</label>
                    </div>
                </div>

                <div class="form-group" id="toggleBannerUpload_formGroup">
                    <label>Upload Banners <span class="fa fa-question-circle text-info fa-2x" style="cursor: pointer;" title="You can upload a maximum of 3 banners. The first banner is REQUIRED and is considered as your primary/main banner which is shown when advertising" data-toggle="tooltip"></span></label>

                    <!-- allow 3 banners only -->

                    <span class="uploadErrors_span"></span>

                    <div class="row">

                        <!-- banner 1 -->
                        <div class="col-md-4">
                          <div class="imgUp">
                            <div class="imagePreview imagePreviewClass"></div>
                            <label class="btn bg-purple btn-flat selectFileToUploadClass" id="selectFileToUpload"><span id="selectTextOne">Select</span>
                                <input type="file" class="uploadFile img" value="" id="advert_bannerOne" name="advert_bannerOne" style="width: 0px;height: 0px;overflow: hidden;">
                            </label>
                            <span class="fa fa-times iclassBanners displayClass" onclick="deleteBanner(1)" id="deleteBanner1" title="delete this banner"></span>
                          </div>  
                          <p class="text-center"><small class="text-red"><i class="fa fa-arrow-circle-up fa-2x"></i> This is your primary banner</small></p>
                        </div>

                        <!-- banner 2 -->
                        <div class="col-md-4">
                          <div class="imgUp2 imgUpClass">
                            <div class="imagePreview2 imagePreviewClass"></div>
                            <label class="btn bg-purple btn-flat selectFileToUploadClass" id="selectFileToUpload2"><span id="selectTextTwo">Select</span>
                                <input type="file" class="uploadFile2 img" value="" id="advert_bannerTwo" name="advert_bannerTwo" style="width: 0px;height: 0px;overflow: hidden;">
                            </label>
                            <span class="fa fa-times iclassBanners displayClass" onclick="deleteBanner(2)" id="deleteBanner2" title="delete this banner"></span>
                          </div>                            
                        </div>

                        <!-- banner 3 -->
                        <div class="col-md-4">
                          <div class="imgUp3 imgUpClass">
                            <div class="imagePreview3 imagePreviewClass"></div>
                            <label class="btn bg-purple btn-flat selectFileToUploadClass" id="selectFileToUpload3"><span id="selectTextThree">Select</span>
                                <input type="file" class="uploadFile3 img" value="" id="advert_bannerThree" name="advert_bannerThree" style="width: 0px;height: 0px;overflow: hidden;">
                            </label>
                            <span class="fa fa-times iclassBanners displayClass" onclick="deleteBanner(3)" id="deleteBanner3" title="delete this banner"></span>
                          </div>                             
                        </div>
                    </div>

                </div>


              <div class="ln_solid"></div>

              <div class="form-group row">
                <div class="col-xs-10">
                  <button type="submit" class="btn c_btn c_btnOne btn-flat" id="dataBtn_createNewAdvert">Submit</button>
                </div>
              </div>

            </form>

              <div class="adverts_briefs_notes">
                  <div class="text-3">
                    <h4 class="text-underline text-info"><strong>Note:</strong></h4>
                    <p><span>1.</span> Adverts should be sent at least <b>1 week</b> before the "Advert Period" begins.</p>
                    <p><span>2.</span> We shall first <b>Review</b> your Advert before authorizing publication and inform you by Mail if review was successful or not.</p>
                    <p><span>3.</span> Publication of <b>Misleading</b>, <b>Inappropriate</b> or <b>Otherwise Illegal</b> information is <b>HIGHLY</b> discouraged. If discovered, your advert shall not be published and you shall be suspended from using any of our services.</p>
                  </div>
              </div>

        </div>
      </div>
      
    </div>
    <!-- /. col-md-8 col-sm-8 col-xs-12 | left -->

    <div class="col-md-4 col-sm-4 col-xs-12">

        <?php 
            include '_partials/_rightColumnAdvertsJobs.php'; 
        ?>        


    </div>
    <!-- /. col-md-4 col-sm-4 col-xs-12 | right -->

  </div>
  <!-- /. row -->

</div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)">Fast Service</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
      <!-- main_container -->

    </div>
   <!-- .container .body #main_wrapper -->

    <!-- >> include bottom scripts << -->
    <?php require_once "_partials/_bottomScripts.php"; ?>

<!-- ================ >> page specific scripts << ================ -->

<script>
  /* $(function(){
 
    $(".imgAdd").click(function(){
          $(this).closest(".row").find('.imgAdd').before(`
                  <div class="imgUp">
                    <div class="imagePreview"></div>
                    <label class="btn btn-primary" id="selectFileToUpload">Select
                        <input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                    </label>
                    <i class="fa fa-times deleteBanner"></i>
                  </div>
            `);
        });
    });
*/
// --- >>> delete banner
function deleteBanner(bannerId) 
{
    // deleting banner 1
    if (bannerId == 1) 
    {
        // $(this).parent().remove();
        $(".imagePreview").css("background-image", "url('_assets/images/pic_blank.jpg')");
        // clear file
        $("#advert_bannerOne").val("");
        // hide delete button
        $("#deleteBanner1").addClass("displayClass");
        // change select text
        $("#selectTextOne").html("Select");
    }

    // deleting banner 2
    else if (bannerId == 2) 
    {
        // $(this).parent().remove();
        $(".imagePreview2").css("background-image", "url('_assets/images/pic_blank.jpg')");
        // clear file
        $("#advert_bannerTwo").val("");
        // hide delete button
        $("#deleteBanner2").addClass("displayClass");
        // change select text
        $("#selectTextTwo").html("Select");
    }

    // deleting banner 3
    else if (bannerId == 3) 
    {
        // $(this).parent().remove();
        $(".imagePreview3").css("background-image", "url('_assets/images/pic_blank.jpg')");
        // clear file
        $("#advert_bannerThree").val("");
        // hide delete button
        $("#deleteBanner3").addClass("displayClass");
        // change select text
        $("#selectTextThree").html("Select");
    }

}

$(function() 
{
    // do you have banners
    $(".dataclass_banners").on("click", function(){
        let clickedRadioBtnValue = $(this).val();

        if (clickedRadioBtnValue == 'yes') 
        {
            $("#toggleBannerUpload_formGroup").slideDown("slow");
        }
        else{
            $("#toggleBannerUpload_formGroup").slideUp("slow");
        }
    });

    // >> ---- accepted file extensions
    let exceptedExtensions = ["jpg", "png", "jpeg", "gif"];


    // 1
    $(document).on("change",".uploadFile", function()
    {

        // --- >> validate
        var bannerOneValue = $("#advert_bannerOne").val();
        // size
        var bannerOneSize = $("#advert_bannerOne")[0].files[0].size;

        if ($.inArray(bannerOneValue.split('.').pop().toLowerCase(), exceptedExtensions) == -1) 
        {                
            _errorNotfify("Error: invalid file format selected; only jpg, jpeg, gif and png allowed!")
        }
        else if (bannerOneSize > 1000000)
        {
            _errorNotfify("Error: maximum file size should not exceed 1 Mb")
        }
        else
        {                

            var uploadFile = $(this);            

            // show delete button
            $("#deleteBanner1").removeClass("displayClass");

            // change select text
            $("#selectTextOne").html("Change");


            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
     
            if (/^image/.test( files[0].type))
            { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
     
                reader.onloadend = function(){ // set image data as background of div
                    //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                    uploadFile.closest(".imgUp").find('.imagePreview').css({"background-image":"url("+this.result+")", "width":"200px", "height":"200px"});
                }
            } 

        } // else     
    });

    // 2
    $(document).on("change",".uploadFile2", function()
    {

        // --- >> validate
        var bannerTwoValue = $("#advert_bannerTwo").val();
        // size
        var bannerTwoSize = $("#advert_bannerTwo")[0].files[0].size;

        if ($.inArray(bannerTwoValue.split('.').pop().toLowerCase(), exceptedExtensions) == -1) 
        {                
            _errorNotfify("Error: invalid file format selected; only jpg, jpeg, gif and png allowed!")
        }
        else if (bannerTwoSize > 1000000)
        {
            _errorNotfify("Error: maximum file size should not exceed 1 Mb")
        }
        else
        { 
            // show delete button
            $("#deleteBanner2").removeClass("displayClass");

            // change select text
            $("#selectTextTwo").html("Change");
                
            var uploadFile = $(this);

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
     
            if (/^image/.test( files[0].type)){ // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
     
                reader.onloadend = function(){ // set image data as background of div
                    //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                    uploadFile.closest(".imgUp2").find('.imagePreview2').css({"background-image":"url("+this.result+")", "width":"200px", "height":"200px"});
                }
            }     
        } // else     
    });

    // 3
    $(document).on("change",".uploadFile3", function()
    {

        // --- >> validate
        var bannerThreeValue = $("#advert_bannerThree").val();
        // size
        var bannerThreeSize = $("#advert_bannerThree")[0].files[0].size;

        if ($.inArray(bannerThreeValue.split('.').pop().toLowerCase(), exceptedExtensions) == -1) 
        {                
            _errorNotfify("Error: invalid file format selected; only jpg, jpeg, gif and png allowed!")
        }
        else if (bannerThreeSize > 1000000)
        {
            _errorNotfify("Error: maximum file size should not exceed 1 Mb")
        }
        else
        { 
            // show delete button
            $("#deleteBanner3").removeClass("displayClass");

            // change select text
            $("#selectTextThree").html("Change");
                
            var uploadFile = $(this);

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
     
            if (/^image/.test( files[0].type)){ // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
     
                reader.onloadend = function(){ // set image data as background of div
                    //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                    uploadFile.closest(".imgUp3").find('.imagePreview3').css({"background-image":"url("+this.result+")", "width":"200px", "height":"200px"});
                }
            }      
        }    
    });


    //bootstrap WYSIHTML5 - text editor
    $(".bp-wysihtml5").wysihtml5({
        toolbar:{
            "image": false
            /*
            "font-styles": true,
            "emphasis": true,
            "image": false,
            "link": true,
            "html": true,
            "color": false,
            "lists": true,
            "blockquote": true
            */
        }
    });

        // --- >> select2 for selecting businesses available
      $("#dataFormAdvert_businessName").select2({
        placeholder: '--- select Business ---',
        tags: true, 
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        language: {
              noResults: function () {
                   return "No Business Created Yet. <a href='_navigation.php?pageNavigation=createNewBusiness'>Create one?</a>";
          }
        }
      });
      // --- >> does user have a business

      // $("input[name='optradio']:checked").val();      

        $(".dataFormAdvert_have_a_businessClass").on('click', function() {
            var businessYesNoValue = $("input[name='dataFormAdvert_have_a_business']:checked").val();
            if (businessYesNoValue == "yes") 
            {
              $("#business_name_toggle").slideDown();
            } else {
              $("#business_name_toggle").slideUp();
            }
        });


});


    // pnotify error function
    function _errorNotfify(errorMsg) {

        // notify
        new PNotify({
          title: "Upload Notice",
          type: "error",
          text: errorMsg,
          nonblock: {
            nonblock: false
          },
          delay: 6000,
          buttons: {
            closer: true,
            closer_hover: true,
            show_on_nonblock: false
          },
          styling: 'bootstrap3'
        });
    }

    /*
    $(function(){  

        $("#btn_action_id_like").on('click touchstart', function(){        


            let likeCount = 0

            $("#btn_action_id_like").html('<i class="fa fa-spin fa-spinner"></i>')          

            setTimeout(function(){
                $("#btn_action_id_like").html('<i class="fa fa-check-circle text-white"></i> Liked').fadeIn("slow")
                $("#span_contents_likes").html(likeCount+=1)
             }, 3000) 
            
            
        });    

        $("#btn_action_id_fav").on('click touchstart', function(){

            let favCount = 0

            $("#btn_action_id_fav").html('<i class="fa fa-spin fa-spinner"></i>')

            setTimeout(function(){
                $("#btn_action_id_fav").html('<i class="fa fa-check-circle"></i> Favorite').fadeIn("slow")
                //$("#span_contents_fav").html(favCount+=1)
             }, 3000) 
            
        });       
    });
*/
</script>
</body>
</html>