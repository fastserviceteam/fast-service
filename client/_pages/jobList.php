<?php
    session_start();

    require "../_require-file.php"; 
      // --- >> instantiate object
    $_globalObj = new globalClass();

    if ($_globalObj->_isLoggedIn() == false) 
    {
       header("Location: ../index.php");        
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fast Service | Job List</title>
        <!-- /. Favicon --> 
    <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
    
  <!-- /. for css which should appear on every page; include them in this file -->
  <?php include "_partials/_topCss.php"; ?>

  </head>
  <body class="nav-md fixed_nav">

   <div class="container body">
      <div class="main_container">

        <?php 
          # include left sidebar main navigation menu
          require "_partials/_leftColumnNavigationMenu.php"; 

          # include top navigation
          require "_partials/_topNavigation.php"; 
        ?>

        <!-- page content -->
        <div class="right_col page_content" role="main">
          
          <div class="pageTitle-Wrapper-Class">

            <div class="page-title">
              <div class="title_left">
                <h3 class="text-info"><i class="fa fa-briefcase"></i> Jobs <small>List</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <input type="text" class="form-control" placeholder="Search for job..." id="dataSearch_jobs" autocomplete="off" onkeyup="jobSearchFilter()">
                </div>
              </div>
            </div>

          <div class="clearfix"></div>

          <div class="row">

              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                  <div class="x_title">
                    <h2 class="text-navy">Jobs Available</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" title="Sort" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-sort fa-1x text-info"></i> <span class="text-info">Order by</span></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="javascript:void(0)" onclick="sortJobsDescfunc()">New Jobs First</a>
                          </li>
                          <li><a href="javascript:void(0)" onclick="sortJobsAscfunc()">Old Jobs First</a>
                          </li>
                        </ul>
                      </li>
                       <li><a href="javascript:void(0)" id="order-action-span-value" class="order-action-span" id="order-action-span-value"></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                          <div class="row">
                              <div class="col-lg-12">  

                                <div class="loading-overlay">
                                  <i class="fa fa-spinner fa-spin fa-3x text-maroon"></i>
                                </div>

                                <div id="jobs_content_wrapper_class">
                          <?php  
                              // load job list from class
                              $_globalObj->_returnJobList();
                          ?>
                                  
                                  </div>
                                  <!-- /. jobs_content_wrapper_class -->

                              </div>
                              <!--  /. col-md-12 -->
                          </div>
                          <!-- /. row -->


                      </div>
                      <!-- /. x_content -->
                    </div>
                    <!-- /. x_panel -->
                  </div>
                  <!-- col-md-12, sm-12, xs-12 -->
              </div>
              <!-- / row -->


            </div>
            <!-- /class ='' -->        
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)">Fast Service</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


<!-- ================ >> page specific scripts << ================ -->

<script>
  
// search for jobs function
function jobSearchFilter()
{

  var jobSearchKeyWord = $("#dataSearch_jobs").val().toLowerCase()

  $.ajax({
      type: 'POST',
      url: '_server_requests.php',
      data:'jobSearchKeyWord='+jobSearchKeyWord,
      beforeSend: function () {
          $('.loading-overlay').show();
      },
      success: function (data) {
          $('#jobs_content_wrapper_class').html(data);
          $('.loading-overlay').fadeOut("slow");
      }
  });

} // end of function

// --- >> order jobs << ---
//  descending order
function sortJobsDescfunc()
{
  var orderDescending = "desc";
  $.ajax({
      type: 'POST',
      url: '_server_requests.php',
      data:{orderingValue: orderDescending},
      beforeSend: function () {
          $('.loading-overlay').show();
      },
      success: function (data) {
          $("#order-action-span-value").html('<i class="fa fa-angle-right"></i> <span>New Adverts First</span>');
          $('#jobs_content_wrapper_class').html(data);
          $('.loading-overlay').fadeOut("slow");
      }
  });
};
//  ascending order
function sortJobsAscfunc()
{
  var orderAscending = "asc";
  $.ajax({
      type: 'POST',
      url: '_server_requests.php',
      data:{orderingValue: orderAscending},
      beforeSend: function () {
          $('.loading-overlay').show();
      },
      success: function (data) {
          $("#order-action-span-value").html('<i class="fa fa-angle-right"></i> <span>Old Jobs First</span>');
          $('#jobs_content_wrapper_class').html(data);
          $('.loading-overlay').fadeOut("slow");
      }
  });
};

// add job to favorites
function addJobtofavfun(jobToken)
{
  $.ajax({
      type: 'POST',
      url: '_server_requests.php',
      data: "jobIdToAddToFavorites="+jobToken,
      success: function (data) {
          if ($.trim(data) == 3) 
          {
            $("#data-favorite"+jobToken).html("Remove from favorites");
              _genNotify("Info: job added to favorites")
          }
          else if ($.trim(data) == 1) 
          {
            $("#data-favorite"+jobToken).html("Add to favorites");
              _infoNotify("Info: job removed to favorites")
          }
          else if ($.trim(data) == 2) 
          {
             _erNotfify("Error: failed to remove job favorites; please try again later");
          }
          else
          {
              _erNotfify("Error: failed to add job to favorites; please try again later");
          }
      }
  }); 

} // 
</script>

<!-- >> include bottom plugins << -->
<?php require_once "_partials/_bottomScripts.php"; ?>
  
  </body>
</html>
