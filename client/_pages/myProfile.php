<?php
session_start();
require "../_require-file.php";
// --- >> instantiate object
$_globalObj = new globalClass();
if ($_globalObj->_isLoggedIn() == false)
{
header("Location: ../index.php");
}
// client profile json information
$userProfile = json_decode($_globalObj->_userProfile());
// client activity stats
$activityStats = json_decode($_globalObj->clientActivityStats());
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fast Service | Profile</title>
    <!-- /. Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
    <!-- /. include general css -->
    <?php include "_partials/_topCss.php"; ?>
    <!-- bootstrap-daterangepicker -->
    <link href="_assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <style>
    #span_contents_likes,
    #span_contents_views {
    color: #337ab7;
    background-color: #fff;
    }
    /* override bootstrap default form-group style*/
    #dataForm_createNewAdvert .form-group {
    margin-bottom: 18px;
    }
    .imagePreviewClass {
    width: 200px !important;
    height: 200px !important;
    background-position: center center;
    background:url(_assets/images/pic_blank.jpg);
    background-color:#fff;
    background-size: cover;
    background-repeat:no-repeat;
    display: inline-block;
    }
    .selectFileToUploadClass
    {
    display:block;
    width: 200px !important;
    border-radius:0px;
    box-shadow:0px 2px 6px 2px rgba(0,0,0,0.25);
    margin-top:-5px;
    color: #fff !important;
    }
    .iclassBanners
    {
    position:relative;
    bottom: 225px;
    left: 167px;
    width:30px;
    height:30px;
    border-radius: 50%;
    text-align:center;
    line-height:30px;
    background-color:rgba(0, 0, 0,0.6);
    cursor:pointer;
    color: #ffffff;
    }
    .displayClass {
    visibility: hidden;
    }
    #toggleBannerUpload_formGroup{
    display: none;
    }

    </style>
  </head>
  <body class="nav-md fixed_nav">
    <div class="container body">
      <div class="main_container">
        <?php
        # include left sidebar main navigation menu
        require "_partials/_leftColumnNavigationMenu.php";
        # include top navigation
        require "_partials/_topNavigation.php";
        ?>
        <!-- page content -->
        <div class="right_col page_content" role="main">
          <div class="">
		  
            <div class="page-title">
              <div class="title_left">
                <h3><i class="fa fa-user"></i> Profile &amp; Activities</h3>
              </div>
              <div class="title_right">
                <!-- search bar for later use --
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              -->
              </div>
            </div>
            
            <div class="clearfix"></div>
			
			
            <div class="row">	
			
				<div class="col-md-3 col-sm-12 col-xs-12">				
					<div class="x_panel x_panel_primary">
						<div class="x_content mdb-color-text">
			
						  <form enctype="multipart/form-data" id="change-profile-form">
							  <div id="profile_img">
								<div id="crop-avatar">
								  <!-- Current profile pic avatar -->
								  <img class="avatar-view img-responsive" src="<?php echo '_assets/clientUploads/profile/'.$userProfile->profile_picture; ?>" alt="Client" id='avatar-client-profile'>
									<div id="change-profile-div">
										<div id="change-profile-button">
											<input type="file" id="change-profile-btn" class="avatar-client-photo-btn" name="newClientPhotoValue">
											<label for="change-profile-btn">Change</label>
										</div>
									</div>
								</div>
							  </div>
							  <input type="hidden" value="<?php echo '_assets/clientUploads/profile/'.$userProfile->profile_picture; ?>" id="currentClientImage">
							</form>


							  <h3 class="text-center"><?php echo $userProfile->full_names; ?></h3>
							  <ul class="list-unstyled user_data">
								
								<li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo $userProfile->current_address; ?>
								</li>
								<li>
								  <i class="fa fa-briefcase user-profile-icon"></i> <?php echo $userProfile->profession; ?>
								</li>
								<li>
								  <i class="fa fa-envelope user-profile-icon"></i> <?php echo $userProfile->email_address; ?>
								</li>
								<li>
								  <i class="fa fa-phone user-profile-icon"></i> <?php echo $userProfile->phone_number; ?>
								</li>
							  </ul>
							  <a href="#editProfileModal" data-toggle="modal" class="btn btn-flat bg-olive"><i class="fa fa-edit m-right-xs"></i> Edit Profile</a>
							  <br />
							  <!-- start skills -->
							  <h4>Stats</h4>
							  <ul class="list-unstyled user_data">
								<li>
								  <p>Adverts Created</p>
								  <div class="progress progress_md">
									<div class="progress-bar bg-purple progress-bar-striped active" role="progressbar" data-transitiongoal="<?php echo $activityStats->advertsNumber; ?>" aria-valuenow="<?php echo $activityStats->advertsNumber; ?>" aria-valuemin="0" aria-valuemax="100">
									  <?php echo $activityStats->advertsNumber; ?>
									</div>
								  </div>
								</li>
								<li>
								  <p>Jobs Created</p>
								  <div class="progress progress_md">
									<div class="progress-bar bg-orange progress-bar-striped active" role="progressbar" data-transitiongoal="<?php echo $activityStats->jobsNumber; ?>" aria-valuenow="<?php echo $activityStats->jobsNumber; ?>" aria-valuemin="0" aria-valuemax="100">
									  <?php echo $activityStats->jobsNumber; ?>
									</div>
								  </div>
								</li>
								<li>
								  <p>Likes</p>
								  <div class="progress progress_md">
									<div class="progress-bar bg-teal progress-bar-striped active" role="progressbar" data-transitiongoal="<?php echo $activityStats->advertsLikesNumber; ?>" aria-valuenow="<?php echo $activityStats->advertsLikesNumber; ?>" aria-valuemin="0" aria-valuemax="100">
									  <?php echo $activityStats->advertsLikesNumber; ?>
									</div>
								  </div>
								</li>
								<li>
								  <p>Favorites</p>
								  <div class="progress progress_md">
									<div class="progress-bar bg-maroon progress-bar-striped active" role="progressbar" data-transitiongoal="<?php echo $activityStats->favNumber; ?>" aria-valuenow="<?php echo $activityStats->favNumber; ?>" aria-valuemin="0" aria-valuemax="100">
									  <?php echo $activityStats->favNumber; ?>
									</div>
								  </div>
								</li>
							  </ul>
							  <!-- end of skills -->
						
						</div>
						
					</div>
					<!-- x_panel x_panel_primary -->
				</div>
				<!-- col-md-9 -->
			
              <div class="col-md-9 col-sm-12 col-xs-12">	  
			  
			  
                <div class="x_panel x_panel_primary">

          <div class="x_content mdb-color-text">
			
            <div class="col-md-12">

              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#tab_content1" id="jobs-activity-tab" role="tab" data-toggle="tab" aria-expanded="true">Jobs Activity</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="adverts-activity-tab" data-toggle="tab" aria-expanded="false">Adverts Activity</a>
              </li>
              <li role="presentation" class=""><a href="#tab_content3" role="tab" id="favorite-tab" data-toggle="tab" aria-expanded="false">Favorites</a>
            </li>
              <li role="presentation" class=""><a href="#tab_content4" role="tab" id="likes-tab" data-toggle="tab" aria-expanded="false">Likes</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">

            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="jobs-activity-tab">
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="btn-group pull-right">
                    <a href="javascript:void(0);" class="btn bg-purple btn-sm btn-flat" title="List view" id="btnJobsList">
                      <i class="fa fa-list"></i>
                    </a>
                    <a href="javascript:void(0);" class="btn bg-teal btn-sm btn-flat" title="Grid View" id="btnJobsGrid">
                      <i class="fa fa-th"></i>
                    </a>
                  </div>
                </div>                
              </div>
              <br>

              <div id="loadCurrentSessionJobs"></div>
              <div id="loadCurrentSessionJobsGrid" class="item_display_none"></div>
            </div>

            <!-- /. jobs-activity-tab -->
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="adverts-activity-tab">

              <div class="row">
                <div class="col-sm-12">
                  <div class="btn-group pull-right">
                    <a href="javascript:void(0);" class="btn bg-purple btn-sm btn-flat" title="List view" id="btnAdvertsList">
                      <i class="fa fa-list"></i>
                    </a>
                    <a href="javascript:void(0);" class="btn bg-teal btn-sm btn-flat" title="Grid View" id="btnAdvertsGrid">
                      <i class="fa fa-th"></i>
                    </a>
                  </div>
                </div>                
              </div>
              <br>

              <div id="loadCurrentSessionAdverts"></div>
              <div id="loadCurrentSessionAdvertsGrid" class="item_display_none"></div>
            </div>

            <!-- /. adverts-activity-tab -->
            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="favorite-tab">
                <form>
                  <div class="form-group">
                    <select name="dataSelect-fav-name" id="dataSelect-fav-id" class="form-control" onchange="filterFavoritesfunction(this.value)">
                      <option selected disabled> --- select favorites to view --- </option>
                      <option value="favjobs" selected>Favorite Jobs</option>
                      <option value="favadverts">Favorite Adverts</option>
                    </select>
                  </div>                
                </form>

                <div id="favorite-div-view" class="favorite_container">

                    <div id="loadFavoriteJobs">
                      <h3 class="text-center hr_font_weight_600">Showing Favorite Jobs</h3>
                <hr>
                        <table class="table table-hover text-center" id="loadFavoriteJobsDataTable">
                          <thead>
                            <tr class="bg-blue">
                              <th class="text-center">Date Posted</th>
                              <th class="text-center">Job Title</th>
                              <th class="text-center">District</th>
                              <th class="text-center">Location</th>
                              <th class="text-center">Job Type</th>
                              <th class="text-center">Deadline</th>
                              <th class="text-center">Action</th>
                            </tr>
                          </thead>
                        </table>
                    </div>

                    <div id="favorites-adverts-container" class="item_display_none">
                      <h3 class="text-center hr_font_weight_600">Showing Favorite Adverts</h3>
                <hr>
                          <div id="loadFavoriteAdverts"></div>
                    </div>    

                </div>
            </div>
            <!-- /. favorite-tab -->

            <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="likes-tab">
              <div id="loadCurrentSessionLikes"></div>
            </div>
            <!-- /. likes-tab -->
          </div>
          <!-- myTabContent -->
        </div>
      </div>
    </div>
  </div>
  
</div>
<!-- col-md-9 - right -->

</div>
</div>
</div>
<!-- /page content -->
<!-- footer content -->
<footer>
<div class="pull-right">
All rights reserved &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)">Fast Service</a>
</div>
<div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
<!-- main_container -->
</div>
<!-- container body -->
<!-- >> include bottom scripts << -->
<?php require_once "_partials/_bottomScripts.php"; ?>
<!-- page specific scripts -->
<script>
(function( $ ) {
  "use strict";
  var currentUserSession = "<?php echo $_SESSION['ur_email']; ?>";
  var constNumber = 1;
 // var changeViewInput = $(".changeView_input").val();

  // --- created jobs ; show a preloader
  $("#loadCurrentSessionJobs").html("<p class='text-center text-maroon'><i class='fa fa-spinner fa-spin fa-2x'></i></p>");
  // --- created adverts
  $("#loadCurrentSessionAdverts").html("<p class='text-center text-maroon'><i class='fa fa-spinner fa-spin fa-2x'></i></p>");
  // --- likes
  $("#loadCurrentSessionLikes").html("<p class='text-center text-maroon'><i class='fa fa-spinner fa-spin fa-2x'></i></p>");          
  // --- favorite adverts
  $("#loadFavoriteAdverts").html("<p class='text-center text-info'><i class='fa fa-spinner fa-spin fa-2x'></i></p>");

  // --- >> load session jobs
  // , changeViewInputJobs: changeViewInput
  function loadSessionJobsList()
  {
    $.get("_server_requests.php", {sessionJobsValue: currentUserSession}, function(data){
    $("#loadCurrentSessionJobs").html(data)
    }, "html");
  };
  function loadSessionJobsGrid()
  {
    $.get("_server_requests.php", {sessionJobsValueGrid: currentUserSession}, function(data){
    $("#loadCurrentSessionJobsGrid").html(data)
    }, "html");
  };
  // --- >> load session adverts
  // list
  function loadSessionAdvertsList()
  {
    $.get("_server_requests.php", {sessionAdvertsValue: currentUserSession}, function(data){
    $("#loadCurrentSessionAdverts").html(data)
    }, "html");
  };
  // grid
  function loadSessionAdvertsGrid()
  {
    $.get("_server_requests.php", {sessionAdvertsValueGrid: currentUserSession}, function(data){
    $("#loadCurrentSessionAdvertsGrid").html(data)
    }, "html");
  };
  // --- >> load session likes
  function loadSessionlikes()
  {
    $.get("_server_requests.php", {sessionLikesValue: currentUserSession}, function(likesdataResp){
        $("#loadCurrentSessionLikes").html(likesdataResp)
    }, "html");
  };

  // --- >> load favorite adverts
  function loadFavoriteAdverts()
  {
    $.post("_server_requests.php", {favoriteAdvertsValue: currentUserSession}, function(data){
        $("#loadFavoriteAdverts").html(data)
    }, "html");
  };

  // --- >> arrange jobs view function
  // grid
  $("#btnJobsGrid").on("click", function(){

    loadSessionJobsGrid();

    $("#loadCurrentSessionJobsGrid").removeClass("item_display_none")
    $("#loadCurrentSessionJobs").addClass("item_display_none")
    $("#btnJobsGrid")
      .removeClass("bg-teal")
      .addClass("bg-purple");
    $("#btnJobsList")
      .removeClass("bg-purple")
      .addClass("bg-teal");

  });

   // list
  $("#btnJobsList").on("click", function(){

    loadSessionJobsList();

    $("#loadCurrentSessionJobs").removeClass("item_display_none")
    $("#loadCurrentSessionJobsGrid").addClass("item_display_none")
    
    $("#btnJobsGrid")
      .removeClass("bg-purple")
      .addClass("bg-teal");
    $("#btnJobsList")
      .removeClass("bg-teal")
      .addClass("bg-purple");

  });

  // --- >> arrange adverts view function
  // grid
  $("#btnAdvertsGrid").on("click", function(){

    loadSessionAdvertsGrid();

    $("#loadCurrentSessionAdvertsGrid").removeClass("item_display_none")
    $("#loadCurrentSessionAdverts").addClass("item_display_none")
    $("#btnAdvertsGrid")
      .removeClass("bg-teal")
      .addClass("bg-purple");
    $("#btnAdvertsList")
      .removeClass("bg-purple")
      .addClass("bg-teal");
  });

   // list
  $("#btnAdvertsList").on("click", function(){

    loadSessionAdvertsList();

    $("#loadCurrentSessionAdverts").removeClass("item_display_none")
    $("#loadCurrentSessionAdvertsGrid").addClass("item_display_none")
    
    $("#btnAdvertsGrid")
      .removeClass("bg-purple")
      .addClass("bg-teal");
    $("#btnAdvertsList")
      .removeClass("bg-teal")
      .addClass("bg-purple");
  });

    // favorite jobs datatable
  var yourJobsListdataTable = $('#loadFavoriteJobsDataTable').DataTable({
     "processing": false,
     "serverSide": true,
     "info": false,
     "autoWidth": false,
     "dom": "<'row'<'col-xs-6'l><'col-xs-6'f>><'row'<'col-xs-12 col-sm-12 col-md-12't>><'row'<'col-md-12 col-xs-12 col-sm-12'p>>",
     "ajax":{
        url :"_server_requests.php",
        type: "post",
        data: {favoriteJobsValue:constNumber},
        error: function(){
           $(".jl_gridClass").html("");
           $("#jl-grid").append('<tbody class="jl_gridClass"><tr><th colspan="6">No records found!</th></tr></tbody>');
           $("#dataTable-grid_processing").css("display","none");                     
        }
     }
  });

  // setinterval refresh
  setInterval( ()=> {
    loadSessionJobsList();
    loadSessionJobsGrid();
    loadSessionAdvertsList();
    loadSessionAdvertsGrid();
    loadSessionlikes();
    loadFavoriteAdverts();
    yourJobsListdataTable.ajax.reload( null, false); 
  }, 3000);



  var exceptedExtensions = ["jpg", "png", "jpeg"];
  var maxHeight = 500;
  var maxWidth = 500;
  var _URL = window.URL || window.webkitURL;

  $(document).on("change",".avatar-client-photo-btn", function()
    {

        // new profile pic value
        var clientPhotoValue = $("#change-profile-btn").val();
        // new profile size
        var clientPhotoSize = $("#change-profile-btn")[0].files[0].size;

        // current profile image
        var currentClientImage = $("#currentClientImage").val();

        // validate
        if ($.inArray(clientPhotoValue.split('.').pop().toLowerCase(), exceptedExtensions) == -1) 
        {                
            _erNotfify("Error: invalid file format selected; only jpg, jpeg and png allowed!")
        }
        else if (clientPhotoSize > 500000)
        {
            _erNotfify("Error: maximum file size should not exceed 1 Mb")
        }
        else
        {               

            var uploadFile = $(this);            

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
     
            if (/^image/.test( files[0].type))
            { // only image file

                
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file

                // show loading gif
                $('#avatar-client-profile').attr("src",  "_assets/images/loading.gif").fadeIn();

              // set image data src     
              reader.onloadend = function(e){

                  var clientImageResult =  this.result;
                  
                  var imgObj = new Image(); // instance of image

                 // imgObj.src = _URL.createObjectURL(files);

                  imgObj.src = e.target.result;

              imgObj.onload = function() {  

                  var newClientPhotoValue = clientPhotoValue.split('\\').pop(); // removing the fakepath url

                  var selectedImageWidth  = this.width;
                  var selectedImageHeight = this.height;

                  console.log(this.width + '\n' + this.height)

                if (selectedImageWidth == maxWidth && selectedImageHeight == maxHeight) 
                {
                  
                      // preparing file for upload
                      var fd = new FormData();
                      var cpFile = $("#change-profile-btn")[0].files[0];
                      fd.append('newClientPhotoValue', cpFile);

                      $.ajax({
                        url: "_server_requests.php", 
                        type: "POST",       
                        data: fd,
                        contentType: false,   
                        cache: false,         
                        processData:false, 
                        success: function(data)   // A function to be called if request succeeds
                        {
                          if (data == 1) {
                            _erNotfify("Error: invalid file format selected; only jpg, jpeg and png allowed!");

                            $('#avatar-client-profile').attr("src", currentClientImage).fadeIn();
                          } 
                          else if(data == 2) {
                            _erNotfify("Error: maximum file size should not exceed 1 Mb");

                            $('#avatar-client-profile').attr("src", currentClientImage).fadeIn();
                          } 
                          else if(data == 3) {
                            _erNotfify("Error: please give your profile image a proper name and then upload again!");

                            $('#avatar-client-profile').attr("src", currentClientImage).fadeIn();
                          } 
                          else if(data == 4) {
                            _erNotfify("Unkown error occured; please try again later!");

                            $('#avatar-client-profile').attr("src", currentClientImage).fadeIn();
                          } 
                          else if(data == 5) {
                            _erNotfify("Error: please select an image with dimensions of exactly 500 x 500");;

                            $('#avatar-client-profile').attr("src", currentClientImage).fadeIn();
                          } 
                          else {
                            $('#avatar-client-profile').attr("src", clientImageResult).fadeIn();
                            _genNotify("Profile Image Successfully changed");
                          }
                        }
                      });                  

                    } // image dimensions is ok
                    else {
                      _erNotfify("Error: please select an image with dimensions of exactly 500 x 500");;

                      $('#avatar-client-profile').attr("src", currentClientImage).fadeIn();

                    } // invalid image dimensions

                  } // imgObj instance

              } // reader.onloadend

            } // checking if its image

        } // else validation
        
    });


}).apply( this, [ jQuery ]);


  // ---- >> onchange filter of favorite jobs and adverts
  function filterFavoritesfunction(favArgs) {
    /*
      $.post("_server_requests.php", {favItemsFilter: favArgs}, function(data){
          $("#favorite-div-view").html(data)
      });favjobs
    */
    if (favArgs == "favadverts") {
      $("#favorites-adverts-container").removeClass("item_display_none");
      $("#loadFavoriteJobs").addClass("item_display_none");
    }
    else {
      $("#favorites-adverts-container").addClass("item_display_none");
      $("#loadFavoriteJobs").removeClass("item_display_none");      
    }
  };


</script>
</body>
</html>