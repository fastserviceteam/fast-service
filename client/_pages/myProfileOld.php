<?php
    session_start();

    require "../_require-file.php"; 
      // --- >> instantiate object
    $_globalObj = new globalClass();

    if ($_globalObj->_isLoggedIn() == false) 
    {
       header("Location: ../index.php");        
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fast Service | Profile</title>
        <!-- /. Favicon --> 
    <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
    <!-- /. include general css -->
    <?php include "_partials/_topCss.php"; ?>
    <!-- bootstrap-daterangepicker -->
    <link href="_assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <style>
      #span_contents_likes,
      #span_contents_views {
      color: #337ab7;
      background-color: #fff;
    }

        /* override bootstrap default form-group style*/
        #dataForm_createNewAdvert .form-group {
            margin-bottom: 18px;
        }

        .imagePreviewClass {
            width: 200px !important;
            height: 200px !important;
            background-position: center center;
            background:url(_assets/images/pic_blank.jpg);
            background-color:#fff;
            background-size: cover;
            background-repeat:no-repeat;
            display: inline-block;
        }
        .selectFileToUploadClass
        {
          display:block;
          width: 200px !important;
          border-radius:0px;
          box-shadow:0px 2px 6px 2px rgba(0,0,0,0.25);
          margin-top:-5px;
          color: #fff !important;
        }
        .iclassBanners
        {
          position:relative;
           bottom: 225px;
          left: 167px;
          width:30px;
          height:30px;
          border-radius: 50%;
          text-align:center;
          line-height:30px;
          background-color:rgba(0, 0, 0,0.6);
          cursor:pointer;
          color: #ffffff;
        }
        .displayClass {
          visibility: hidden;            
        }
        #toggleBannerUpload_formGroup{
            display: none;
        }
    </style>
  </head>

   <body class="nav-md fixed_nav">

   <div class="container body">
      <div class="main_container">

        <?php 
          # include left sidebar main navigation menu
          require "_partials/_leftColumnNavigationMenu.php"; 

          # include top navigation
          require "_partials/_topNavigation.php"; 
        ?>

        <!-- page content -->
        <div class="right_col page_content" role="main">
          
          <div class="">

            <div class="page-title">
              <div class="title_left">
                <h3><i class="fa fa-user"></i> Profile &amp; Activities</h3>
              </div>

              <div class="title_right">
                <!-- other content -->
              </div>
            </div>

          </div>

  <div class="row">


    <div class="col-md-8 col-sm-8 col-xs-12">

      <div class="x_panel tile">

          <div id="secondary-View-editing-advert" class="element-hidden view-editing-toggle">
            
            <div class="x_title">
              <div class="row">
                <div class="col-xs-8">
                  <h3>Editing Advert </h3>
                </div>
                <div class="col-xs-4">
                  <h3 class="text-right"><a href="#" onclick="dataChangeView()" data-title="back to profile view" data-toggle="tooltip" data-placement="bottom" class="text-teal"><i class="fa fa-arrow-circle-right"></i></a></h3>
                </div>
              </div>              
                  <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <span id="advert-details-editing-span"></span>
            </div>

          </div>
          <!-- secondary-View-editing-advert -->

          <div id="secondary-View-editing-job" class="element-hidden view-editing-toggle">

            <div class="x_title">
              <div class="row">
                <div class="col-xs-8">
                  <h3>Editing Job </h3>
                </div>
                <div class="col-xs-4">
                  <h3 class="text-right"><a href="#" onclick="dataChangeView()" data-title="back to profile view" data-toggle="tooltip" data-placement="bottom" class="text-teal"><i class="fa fa-arrow-circle-right"></i></a></h3>
                </div>
              </div>
              
                  <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <span id="job-details-editing-span"></span>
            </div>

          </div>
          <!-- secondary-View-editing-job -->

        <div id="primary-view-profile" class="view-editing-toggle">

        <div class="x_title">
          <h3>Profile</h3>
        </div>

        <div class="x_content">

                    <?php
                      $userProfile = json_decode($_globalObj->_userProfile());
                    ?>
                    
                    <ul class="list-unstyled timeline">
                    <li>
                      <div class="block">
                        <div class="tags">
                          <a href="javascript:void(0)" class="tag bg-blue tag-basic">
                            <span>Basic</span>
                          </a>
                        </div>
                        <div class="block_content">
                          <h2 class="title basic-title">
                              <a href="javascript:void(0)" class="no-cursor-link">Your Basic Profile &nbsp;<label class="text-red text-edit-i" title="click to edit" data-toggle="tooltip" onclick="showEditingInformationForm(1)"><i class="fa fa-pencil"></i></label></a>
                          </h2>


                          <div class="excerpt account-content">

                            <!-- naked elements display -->
                              <div id="sub_account_details_p" class="toggle_account_details">
                                <p>
                                  <strong>Names:</strong> <span id="dataFullNamesSpan"><?php echo $userProfile->full_names; ?></span>
                                </p>
                                <p>
                                  <strong>Gender:</strong> <span id="dataGenderSpan"><?php echo $userProfile->gender; ?></span>
                                </p>  
                                <p>
                                  <strong>Profession:</strong> <span id="dataProfessionSpan"><?php echo $userProfile->profession; ?></span>
                                </p>                                 
                              </div> 
                              <!-- sub_account_details_p -->
                              
                              <!-- editing elements form -->
                              <div id="sub_account_details_form" class="form-div-hidden toggle_account_details">
                                
                                <form role="form" id="UpdatingProfileFormId" enctype="multipart/form-data">

                                  <div class="form-group">
                                    <label for="editing_change_profile_pic">Upload Profile Picture (Optional)</label>
                                    <input type="file" name="editing_change_profile_pic" id="editing_change_profile_pic" class="btn-xs" aria-describedy="picture_help">
                                    <small id="picture_help" class="text-info">png, jpeg or jpg ONLY; Less then 500kb</small>
                                  </div>
                                    
                                    <div class="form-group">
                                      <label for="editing_fullnames">Full Names</label>
                                      <input type="text" class="form-control" value="<?php echo $userProfile->full_names; ?>" autocomplete="off" id="editing_fullnames" name="editing_fullnames" placeholder="your full names" required>
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="editing_gender">Gender</label>
                                      <div id="editing_gender">
                                        <label class="radio-inline"><input type="radio" value="Male" name="editing_gender">Male</label>
                                        <label class="radio-inline"><input type="radio" value="Female" name="editing_gender" required>Female</label>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label for="editing_profession">Profession</label>
                                      <input type="text" class="form-control" value="<?php echo $userProfile->profession; ?>" autocomplete="off" id="editing_profession" name="editing_profession" placeholder="your profession" required>
                                    </div>

                                    <button type="submit" class="btn btn-flat c_btnSix btn-sm" id="updateProfileBtn">Done</button>

                                </form>
                      

                              </div>
                              <!-- sub_account_details_form -->                           

                          </div>

                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="block">
                        <div class="tags">
                          <a href="javascript:void(0)" class="tag bg-purple tag-location">
                            <span>Location</span>
                          </a>
                        </div>
                        <div class="block_content">
                          <h2 class="title location-title">
                              <a  href="javascript:void(0)" class="no-cursor-link">Your Contacts &amp; Location &nbsp;<label class="text-red text-edit-i" title="click to edit" data-toggle="tooltip" onclick="showEditingInformationForm(2)"><i class="fa fa-pencil"></i></label></a>
                          </h2>

                          <div class="excerpt account-content">
  
                            <!-- naked elements display -->
                            <div id="sub_location_details_p" class="toggle_location_details">
                              <p>
                                <strong>Nationality:</strong> <span id="dataNationality"><?php echo $userProfile->nationality; ?></span>
                              </p>
                              <p>
                                <strong>Current Location:</strong> <span id="dataCurrentLocation"><?php echo $userProfile->current_address; ?></span>
                              </p>
                              <p>
                                <strong>Email Address:</strong> <span id="dataEmailAddress"><?php echo $userProfile->email_address; ?></span>
                              </p>
                              <p>
                                <strong>Phone Number:</strong> <span id="dataPhoneNumber"><?php echo $userProfile->phone_number; ?></span>
                              </p>
                            </div>
                              <!-- sub_location_details_p -->
                              
                              <!-- editing elements form -->
                              <div id="sub_location_details_form" class="form-div-hidden toggle_location_details">
                                
                                <form role="form" id="UpdatingLocationFormId">
                                    
                                    <div class="form-group">
                                      <label for="editing_nationality">Nationality</label>
                                      <select name="editing_nationality" id="editing_nationality" class="form-control">
                                        <option> --- select nationality --- </option>
                                        <?php foreach($_globalObj->_nationality() as $row) { ?>
                                            <option><?php echo $row['name']; ?></option>
                                        <?php } ?>
                                      </select>
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="editing_currentLocation">Current Location</label>
                                      <input type="text" name="editing_currentLocation" id="editing_currentLocation" class="form-control" value="<?php echo $userProfile->current_address; ?>" autocomplete="off" required placeholder="your current location">
                                    </div>

                                    <div class="form-group">
                                      <label for="editing_phoneNumber">Phone Number</label>
                                      <input type="text" class="form-control" value="<?php echo $userProfile->phone_number; ?>" autocomplete="off" name="editing_phoneNumber" id="editing_phoneNumber" placeholder="your phone number" required>
                                    </div>

                                    <button type="button" onclick="onClickUpdateLocationfunc()" class="btn btn-flat c_btnSeven btn-sm" id="updateLocationBtn">Done</button>

                                </form>                      

                              </div>
                              <!-- sub_location_details_form -->   

                          </div>

                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="block">
                        <div class="tags">
                          <a href="javascript:void(0)" class="tag bg-olive tag-settings">
                            <span>Settings</span>
                          </a>
                        </div>
                        <div class="block_content">
                          <h2 class="title settings-title">
                              <a href="javascript:void(0)">Your settings and account</a>
                          </h2>

                          <div class="excerpt account-content">
                              
                              <p>
                                <a href="javascript:void(0)" class="text-maroon sub-title-l"><i class="fa fa-angle-double-right"></i> Change / Update your password?</a>
                                <!-- 
data-parsley-validate
 data-parsley-trigger="keyup" data-parsley-minlength="8"
                                -->
                                <form id="data-form-update-password" class="form-horizontal form-label-left">

                                    <div class="form-group">
                                      <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="dataCurrentPassword">Current Password</label>
                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="password" class="form-control" placeholder="enter current password" autocomplete="off" id="dataCurrentPassword" name="dataCurrentPassword" required>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="dataNewPassword">New Password</label>
                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="password" class="form-control" placeholder="enter new password" autocomplete="off" id="dataNewPassword" name="dataNewPassword" required>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="dataConfirmNewPassword">Confirm Password</label>
                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="password" class="form-control" placeholder="confirm new password" autocomplete="off" id="dataConfirmNewPassword" name="dataConfirmNewPassword" required>
                                      </div>
                                    </div>
                                    <input type="hidden" id="dataInputEmailSession" name="dataInputEmailSession" value="<?php echo $_SESSION['ur_email']; ?>">

                                    <div class="form-group">
                                      <button type="button" class="btn btn-flat btn-warning btn-sm" id="InputUpdatePasswordBtn">Update</button>
                                    </div>

                                </form>
                                
                              </p>
                              <div class="deleteAccountSettings">
                                <p>
                                  <a href="javascript:void(0)" class="text-maroon sub-title-l"><i class="fa fa-angle-double-right"></i> Delete your account?</a>
                                </p>

                                <p>
                                  <strong>NB:</strong> Deleting your account will not only permanently remove your personal information but also any adverts, jobs, businesses and any other activity you might have added or carried out.
                                </p>

                                <form role="form" id="data-form-delete-account">
                                  <div class="form-group">
                                    <label class="radio-inline"><input type="radio" name="dataConfirmAgreement" value="agreed" required> By deleting my account, i fully agree to the <a href="#" class="text-underline">Terms and Conditions</a> stated under the Fast Service</label>
                                  </div>

                                  <div class="form-group">
                                    <label for="confirmDeletionPassword">Enter your password</label>
                                    <input type="password" class="form-control" placeholder="enter your password" autocomplete="off" id="confirmDeletionPassword" required name="confirmDeletionPassword" required>
                                  </div>

                                  <input type="hidden" id="dataDeleteEmailSession" name="dataDeleteEmailSession" value="<?php echo $_SESSION['ur_email']; ?>">

                                  <div class="form-group">
                                    <button type="submit" class="btn btn-flat bg-red btn-sm" id="dataConfirmDeletionBtn">Delete my Account</button>
                                  </div>

                                  <div class="form-group">
                                    <img src="_assets/images/serverLoader.gif" alt="" class="center-block element-hidden">
                                  </div>

                                </form>
                                
                              </div>

                          </div>

                        </div>
                      </div>
                    </li>
                  </ul>

        </div>
        <!-- /. x_content -->
      </div>
      <!-- /. primary-view-profile -->

      </div>
    </div>
    <!-- /. col-md-8 col-sm-8 col-xs-12 | left -->

    <div class="col-md-4 col-sm-4 col-xs-12">

      <div class="x_panel tile">

        <div class="x_title">
          <h3>Activities</h3>
        </div>

        <div class="x_content activities-view-wrapper-content">
        
          <div id="activities-view-wrapper">

           <div class="activities_tab" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#your_jobs_tab" role="tab" id="jobs-tab" data-toggle="tab" aria-expanded="false">Jobs</a>
                  </li>
                  <li role="presentation" class=""><a href="#your_adverts_tab" role="tab" id="adverts-tab" data-toggle="tab" aria-expanded="false">Adverts</a>
                  </li>
                </ul>
                <div id="myTabContent" class="tab-content">

                  <div role="tabpanel" class="tab-pane active fade in" id="your_jobs_tab" aria-labelledby="jobs-tab">                    
                       <div class="activities_container">
                          <span id="loadCurrentSessionJobs"></span>
                       </div>                    
                  </div>

                  <div role="tabpanel" class="tab-pane fade in" id="your_adverts_tab" aria-labelledby="adverts-tab">
                       <div class="activities_container">
                         <p id="loadCurrentSessionAdverts"></p>
                       </div>
                  </div>

                </div>
            </div>
            <!-- / tabs -->  

          </div>
          <!-- /. activities-view-wrapper -->           

        </div>
          <!-- x_content -->
      </div>
        <!-- x_panel tile -->

    </div>
    <!-- /. col-md-4 col-sm-4 col-xs-12 | right -->

  </div>
  <!-- /. row -->

  <!-- /. favorites -->
  <div class="row">

    <div class="col-md-12 col-xs-12 col-sm-12">

        <div class="favorites_wrapper">    
          <div class="x_panel tile">            
              <div class="x_title">
                  <h2>Favorites</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li> 
                    </ul>            
                  <div class="clearfix"></div>
              </div>

              <div class="x_content">

               <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#your_fav_jobs_tab" role="tab" id="fav-jobs-tab" data-toggle="tab" aria-expanded="false">Jobs</a>
                      </li>
                      <li role="presentation" class=""><a href="#your_fav_adverts_tab" role="tab" id="fav-adverts-tab" data-toggle="tab" aria-expanded="false">Adverts</a>
                      </li>
                      <li role="presentation" class=""><a href="#your_likes_tab" role="tab" id="likes-tab" data-toggle="tab" aria-expanded="false">Likes</a>
                      </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">

                      <div role="tabpanel" class="tab-pane active fade in" id="your_fav_jobs_tab" aria-labelledby="fav-jobs-tab">                    
                           <div class="favorite_container">
                              <span id="loadFavoriteJobs"></span>
                              <table class="table table-hover table-condensed text-center" id="loadFavoriteJobsDataTable">
                                <thead>
                                  <tr class="bg-black">
                                    <th class="text-center">Date Posted</th>
                                    <th class="text-center">Job Title</th>
                                    <th class="text-center">District</th>
                                    <th class="text-center">Location</th>
                                    <th class="text-center">Job Type</th>
                                    <th class="text-center">Deadline</th>
                                    <th class="text-center">Action</th>
                                  </tr>
                                </thead>
                              </table>
                           </div>                    
                      </div>

                      <div role="tabpanel" class="tab-pane fade in" id="your_fav_adverts_tab" aria-labelledby="fav-adverts-tab">                    
                           <div class="favorite_container" id="favorites-adverts-container">
                              <span id="loadFavoriteAdverts"></span>
                           </div>        

                      </div>

                      <div role="tabpanel" class="tab-pane fade in" id="your_likes_tab" aria-labelledby="likes-tab">
                           <div class="favorite_container">
                             <span id="loadCurrentSessionLikes"></span>
                           </div>
                      </div>

                    </div>
                </div>
                <!-- / tabs -->  
                
              </div>

          </div>
          <!-- /. x_panel -->    
        </div>
        <!-- /. favorites_wrapper -->
      
    </div>
    <!-- col-md-12 -->
    
  </div>
  <!-- /. row -->


        
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)">Fast Service</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- >> include bottom scripts << -->
    <?php require_once "_partials/_bottomScripts.php"; ?>
    

  <!-- =================== >> page specific scripts << =================== -->

    <script>
      
      var currentUserSession = "<?php echo $_SESSION['ur_email']; ?>";
      var constNumber = 1;

      $(function(){

        // favorite jobs datatable
        var yourJobsListdataTable = $('#loadFavoriteJobsDataTable').DataTable({
           "processing": false,
           "serverSide": true,
           "info": false,
           "autoWidth": false,
           "ajax":{
              url :"_server_requests.php",
              type: "post",
              data: {favoriteJobsValue:constNumber},
              error: function(){
                 $(".jl_gridClass").html("");
                 $("#jl-grid").append('<tbody class="jl_gridClass"><tr><th colspan="6">No records found!</th></tr></tbody>');
                 $("#dataTable-grid_processing").css("display","none");                     
              }
           }
        });

        // favorite adverts datatable
        /*
        var yourFavAdvertsDatatable = $("#loadFavoriteAdvertsDataTable").DataTable({
           "processing": false,
           "serverSide": true,
           "info": false,
           "autoWidth": false,
           "ajax":{
              url :"_server_requests.php",
              type: "post",
              data: {favoriteAdvertsValue:constNumber},
              error: function(){
                 $(".jl_gridClass").html("");
                 $("#jl-grid").append('<tbody class="jl_gridClass"><tr><th colspan="6">No records found!</th></tr></tbody>');
                 $("#dataTable-grid_processing").css("display","none");                     
              }
           }
        });
        */

      //  --- >> activities << --- //

        // --- created jobs ; show a preloader
        $("#loadCurrentSessionJobs").html("<p class='text-center text-navy'><i class='fa fa-spinner fa-spin'></i></p>");

        // --- created adverts
        $("#loadCurrentSessionAdverts").html("<p class='text-center text-navy'><i class='fa fa-spinner fa-spin'></i></p>");

        // --- likes
        $("#loadCurrentSessionLikes").html("<p class='text-center text-navy'><i class='fa fa-spinner fa-spin'></i></p>");

        // --- favorite jobs
        //$("#loadFavoriteJobs").html("<p class='text-center text-navy'><i class='fa fa-spinner fa-spin'></i></p>");
          
        // --- favorite adverts
        $("#loadFavoriteAdverts").html("<p class='text-center text-navy'><i class='fa fa-spinner fa-spin'></i></p>");

          // --- >> we're refreshing the function call every 3s
           setInterval( ()=> {
            _loadSessionJobs();
            _loadSessionAdverts();
            _loadSessionlikes(); 
            yourJobsListdataTable.ajax.reload( null, false); 
            /*
            _loadFavoriteJobs();
            */ 
            _loadFavoriteAdverts();           
          }, 3000); 
            

          // --- >> parsely validate

        /**
          $('parsley:field:validate', function() {
            validateFront();
          });

          // validate password update form
          $('#data-form-update-password #InputUpdatePasswordBtn').on('click', function() {
            $('#data-form-update-password').parsley().validate();
            validateFront();
          });

          var validateFront = function() {
            if (true === $('#data-form-update-password').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
            } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
            }
          };
      
        try {
        hljs.initHighlightingOnLoad();
        } catch (err) {}
        */


        // ---- >> delete account        
        $("#data-form-delete-account").on("submit", function(e)
        {
          /*
            $("#dataConfirmDeletionBtn").prop("disabled", true);
            $("#dataConfirmDeletionBtn").html("please wait");

            $("#dataConfirmDeletionBtn").prop("disabled", false);
            $("#dataConfirmDeletionBtn").html("Update");  

            element-hidden
          */
          e.preventDefault();

          $("#dataConfirmDeletionBtn").css("display", "none")

          $.ajax({
            url: "_server_requests.php",
            method: "post",
            data: $("#data-form-delete-account").serialize(),
            contentType: false, 
            cache: false,        
            processData:false, 
            success: function(data)
            {
              bootbox.alert(data);
                $(".element-hidden").css("display", "none")
                $("#dataConfirmDeletionBtn").css("display", "block")
            } 
          });          
        });

      // update profile
      $("#UpdatingProfileFormId").on("submit", function(e)
      {
          e.preventDefault();
        
        $("#updateProfileBtn").prop("disabled", true);
        $("#updateProfileBtn").html("Please wait...")
          $.ajax({
            url: "_server_requests.php",
            method: "post", 
            data: new FormData(this),
            contentType: false, 
            cache: false,        
            processData:false, 
            success: function(data){
              bootbox.alert(data)
              $("#updateProfileBtn").prop("disabled", false);
              $("#updateProfileBtn").html("Done")
            }
          });
      });


        // ----- >> update password submit
        $("#InputUpdatePasswordBtn").on("click", function(){
            
            $("#InputUpdatePasswordBtn").prop("disabled", true);
            $("#InputUpdatePasswordBtn").html("please wait");

            $.ajax({
              url: "_server_requests.php",
              method: "post",
              cache: false,
              data: $("#data-form-update-password").serialize(),
              success: function(data){
                bootbox.alert(data)
                $("#InputUpdatePasswordBtn").prop("disabled", false);
                $("#InputUpdatePasswordBtn").html("Update");
              } 
            });
        });
    });


     
    // --- >> load favorite jobs
    function _loadFavoriteJobs()
    {
      $.get("_server_requests.php", {favoriteJobsValue: currentUserSession}, function(data){
          $("#loadFavoriteJobs").html(data)
      }, "html");
    };

    // --- >> load favorite adverts
    function _loadFavoriteAdverts()
    {
      $.post("_server_requests.php", {favoriteAdvertsValue: currentUserSession}, function(data){
          $("#loadFavoriteAdverts").html(data)
      }, "html");
    };

    // --- >> load session adverts
    function _loadSessionAdverts()
    {
      $.get("_server_requests.php", {sessionAdvertsValue: currentUserSession}, function(data){
          $("#loadCurrentSessionAdverts").html(data)
      }, "html");
    };

    // --- >> load session jobs
    function _loadSessionJobs()
    {
      $.get("_server_requests.php", {sessionJobsValue: currentUserSession}, function(data){

          $("#loadCurrentSessionJobs").html(data)

      }, "html");        
    };

    // --- >> load session likes
    function _loadSessionlikes()
    {
      $.get("_server_requests.php", {sessionLikesValue: currentUserSession}, function(likesdataResp){

          $("#loadCurrentSessionLikes").html(likesdataResp)

      }, "html");
    };

    // --- >> change view
    function dataChangeView()
    {
      $("#primary-view-profile").fadeIn() // show profile view
      $("#secondary-View-editing-job").hide() // hide job editing view
      $("#secondary-View-editing-advert").hide() // hide advert editing view

      $(".deleteSessionInputEditClass").val('') // clear field
    }


    // update location
    function onClickUpdateLocationfunc()
    {
      $("#updateLocationBtn").prop("disabled", true);
      $("#updateLocationBtn").html("Please wait...");

        $.ajax({
          url: "_server_requests.php",
          method: "post", 
          data: $("#UpdatingLocationFormId").serialize(),
          success: function(data){
            bootbox.alert(data);
            $("#updateLocationBtn").prop("disabled", false);
            $("#updateLocationBtn").html("Done")
          }
        });
    }
    
    // switch between  forms function
    function showEditingInformationForm(args)
    {
      switch(args)
      {
        case 1:  
          $(".toggle_account_details").slideToggle(300, function(){

                $.ajax({
                  url: "_server_requests.php",
                  method: "post", 
                  data: {currentSessionEmailAddress:currentUserSession},
                  success: function(data)
                  {     
                    var userBasicInfo = JSON.parse(data);

                    $("#dataFullNamesSpan").html(userBasicInfo['full_names']);
                    $("#dataGenderSpan").html(userBasicInfo['gender']);
                    $("#dataProfessionSpan").html(userBasicInfo['profession']);
                  }
                });

          });        
        break; 

        case 2:
          $(".toggle_location_details").slideToggle(300, function(){

                $.ajax({
                  url: "_server_requests.php",
                  method: "post", 
                  data: {currentSessionEmailAddress:currentUserSession},
                  success: function(data)
                  {     
                    var userLocationInfo = JSON.parse(data);

                      $("#dataNationality").html(userLocationInfo['nationality']);
                      $("#dataCurrentLocation").html(userLocationInfo['current_address']);
                      $("#dataEmailAddress").html(userLocationInfo['email_address']);
                      $("#dataPhoneNumber").html(userLocationInfo['phone_number']);
                  }
                });

          }); 
        break;
      }
    }; //

     // remove already fav jobs from favorites
      function removeFromFavFunction(jobToken)
      {
        $.ajax({
            type: 'POST',
            url: '_server_requests.php',
            data: "jobIDRemovefromFavorites="+jobToken,
            success: function (data) {
                if ($.trim(data) == 3) 
                {
                    _genNotify("Info: job added to favorites")
                }
                else if ($.trim(data) == 1) 
                {
                    _infoNotify("Info: job removed to favorites")
                }
                else if ($.trim(data) == 2) 
                {
                   _erNotfify("Error: failed to remove job favorites; please try again later");
                }
                else
                {
                    _erNotfify("Error: failed to add job to favorites; please try again later");
                }
            }
        });  
      
      }; // 


    // pnotify info function
    function _infoNotify(msg) {

        // notify
        new PNotify({
          title: "Notice",
          type: "info",
          text: msg,
          nonblock: {
            nonblock: false
          },
          delay: 4500,
          buttons: {
            closer: true,
            closer_hover: true,
            show_on_nonblock: false
          },
          styling: 'bootstrap3'
        });
    };

  </script>

  
  </body>
</html>
