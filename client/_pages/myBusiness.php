<?php
session_start();
require "../_require-file.php"; // database config, server feedbacks, phpmailer class and other global constants
require "../_server-functions.php"; // custom functions
/**
* user email sesion; i.e. blahblah@yayay.com :
* for truncated version (without domain); use $_SESSION['emailUname']
*
*/
$mail = $_SESSION['ur_email'];
/**
* instantiate object
*
*/
$_globalObj = new globalClass();
/**
* check login status
*/
if ($_globalObj->_isLoggedIn() == false)
{
header("Location: ../index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fast Service | Client
    </title>
    <!-- Custom Theme Style -->
    <link href="_assets/css/style3.css" rel="stylesheet">
    <!-- /. jquery-ui -->
    <link rel="stylesheet" href="_assets/plugins/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
    <!-- /. Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
    <!-- /. for css which should appear on every page; include them in this file -->
    <?php include "_partials/_topCss.php"; ?>
    <style>
    #infowindow-content .title
    {
    font-weight: bold;
    }
    #infowindow-content
    {
    display: none;
    }
    #map #infowindow-content
    {
    display: inline;
    }
    div.stepContainer{
    height: 20px !important;
    }
    #map {
    height: 130px !important;
    border:1px solid #ccc;
    }
    #description {
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    }
    #infowindow-content .title {
    font-weight: bold;
    }
    #infowindow-content {
    display: none;
    }
    #map #infowindow-content {
    display: inline;
    }
    </style>
  </head>
  <body class="nav-md fixed_nav">
    <div class="container body">
      <div class="main_container">
        <?php
        # include left sidebar main navigation menu
        require "_partials/_leftColumnNavigationMenu.php";
        # include top navigation
        require "_partials/_topNavigation.php";
        ?>
        <!-- page content -->
        <div class="right_col page_content" role="main">
          <div class="row" id="create_new_business">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile">
                <div class="col-sm-12">
                  <div id="show_edit">
                  </div>
                  <div id="bzn_table_form">
                    <form method="post" action="business/submit.php">
                      <section class="panel panel-default">
                        <div class="panel-header">
                          <h3>Create New Business
                          </h3>
                        </div>
                        <div class="panel-body"  style="background:#ececec">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="Business_Name">Business Name</label>
                                <input type="text" id="Business_Name" name="Business_Name" class="form-control" autofocus placeholder="Business Name: " required >
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <?php
                                $categories = GetAll("categories","ORDER BY category_name ASC");
                                ?>
                                <label for="Business_Category">
                                  Category
                                </label>
                                <select type="text" id="Business_Category" name="Business_Category" class="form-control" required>
                                  <option selected disabled> --- select category --- </option>
                                  <?php
                                  foreach($categories as $r){
                                  echo '<option>'.$r->category_name.'</option>';
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="select_services_offered">Select Services Offered</label>
                                <?php
                                $services = GetAll("services");
                                ?>
                                <select id="select_services_offered" onchange="showSelectsd();" name="multiple_select" class="form-control" multiple="multiple">
                                  <?php
                                  foreach($services as $svc){
                                              echo '<option value="'.$svc->service_id.'">'.$svc->service_name.'</option>';
                                  }
                                  ?>
                                </select>
                                <input type="hidden" class="hidden form-control" id="send" value="" name="Services_Offered" />
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="Business_Phone">Phone Contact</label>
                                <input type="number" id="Business_Phone" name="Business_Phone" class="form-control" placeholder="Tel." autocomplete="off" required>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="control-label">
                                  <i class="fa fa-map-marker">
                                  </i>   Area of Operation
                                </label>
                                <button id="submit" type="button" class="btn btn-xs btn-warning btn-flat pull-right" >My location
                                <i class="fa fa-map-marker"></i>
                                </button>
                                <input type="text" class="form-control input-sm"   id="address" name="Geographical_Area"  placeholder="location" required>
                                <span  id="service_address">
                                </span>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <input type="submit" name="submit_business" class="btn btn-primary pull-right  btn-xs c_btn c_btnOne btn-flat" value="Create Business">
                            </div>
                          </div>
                          <!-- hidden -->
                          <input type="hidden" class="hidden" id="location" name="location" value="Kampala Uganda">
                          <input type="hidden" name="Business_Email" class="hidden form-control" value="<?=$mail;?>">
                          <input type="hidden"  id="latlng" name="latlng" class="hidden">
                          <input type="hidden"  id="longitude" name="longitude" class="hidden">
                          <input type="hidden"  id="latitude" name="latitude" class="hidden">
                        </div>
                        <!-- / panel-body -->
                      </section>
                      <!-- /panel panel-default-->
                    </div>
                  </form>
                </div>
                <!--/bzn_table_form-->
              </div>
              <!-- / x_panel -->
            </div>
            <!-- / col-md-12 col-sm-12 col-xs-12 -->
          </div>
          <!-- /row : create business -->
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h3>My Businesses
                  </h3>
                </div>
                <div class="x_content">
                  <span id="show_dataForm_Business_replies">
                  </span>
                  <table class="table table-bordered table-condensed table-hover">
                    <thead class="bg-olive text-white">
                      <tr>
                        <th>Date
                        </th>
                        <th>Name
                        </th>
                        <th>Services
                        </th>
                        <th>Location
                        </th>
                        <th>Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $my_bzns = GetAll("business_profile","WHERE Business_Email = '$mail' ORDER BY business_profile_id DESC");
                      $i=1;
                      foreach($my_bzns as $row){
                      ?>
                      <script>
                      function return_editing<?=$i;
                      ?>(){
                      $("#bzn_table_form").hide();
                      var bzn_id<?=$i;
                      ?> ='edit_id='+ document.getElementById('edit_id<?=$i;?>').value;
                      $.ajax({
                      type: "POST",
                      url: "../client/business/edit_business.php",
                      async: true,
                      data: bzn_id<?=$i;
                      ?>,
                      cache: false,
                      success: function(data){
                      show_edit("new", data);
                      }
                      }
                      );
                      }
                      function show_edit(type, msg){
                      $('#show_edit').html(msg);
                      }
                      function return_review<?=$i;
                      ?>(){
                      $("#bzn_table_form").hide();
                      var bzn_review<?=$i;
                      ?> ='edit_id='+ document.getElementById('edit_id<?=$i;?>').value;
                      $.ajax({
                      type: "POST",
                      url: "../client/business/review.php",
                      async: true,
                      data: bzn_review<?=$i;
                      ?>,
                      cache: false,
                      success: function(data){
                      show_edit("new", data);
                      }
                      }
                      );
                      }
                      function show_edit(type, msg){
                      $('#show_edit').html(msg);
                      }
                      </script>
                      <input name="edit_id" type="text" class="hidden" id="edit_id<?=$i;?>" value="<?=$row->business_profile_id;?>">
                      </input>
                      <tr>
                        <td>
                          <?=date('d-M, Y', (strtotime($row->on_date)));?>
                        </td>
                        <td>
                          <?=$row->Business_Name;?>
                        </td>
                        <td>
                          <?=$row->Services_Offered;?>
                        </td>
                        <td>
                          <?=$row->Geographical_Area;?>
                        </td>
                        <td>
                          <button onclick="return_editing<?=$i;?>();" type="button"  class="btn btn-xs btn-default text-primary" title="Update / Edit">
                          <i class="fa fa-pencil text-primary">
                          </i>
                          </button>
                          <button onclick="return_review<?=$i;?>();" type="button"  class="btn btn-xs btn-default text-primary" title="Review">
                          <i class="fa fa-eye text-warning">
                          </i>
                          </button>
                          <form method="post" action="business/submit.php">
                            <input type="text" name="business_get_id" value="<?=$row->business_profile_id;?>" class="blu form-control hidden"  required >
                            <button  type="submit" name="delete_business" class="btn btn-xs btn-default" title="delete">
                            <i class="fa fa-trash text-danger">
                            </i>
                            </button>
                          </form>
                        </td>
                      </tr>
                      <?php
                                $i++;
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- /row : your businesses -->
          <div class="col-sm-12">
            <div id="map">
            </div>
            <input type="text" name="Business_Address" value="Uganda" class="hidden form-control" />
          </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved &copy;
            <?php echo date('Y'); ?>
            <a href="javascript:void(0)">Fast Service
            </a>
          </div>
          <div class="clearfix">
          </div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- main_container -->
    </div>
    <!-- .container .body #main_wrapper -->
    <!-- >> include bottom scripts << -->
    <?php require_once "_partials/_bottomScripts.php"; ?>
    <!-- Specific Page Vendor -->
    <script src="_assets/plugins/jquery-browser-mobile/jquery.browser.mobile.js">
    </script>
    <script src="_assets/plugins/jquery-ui/js/jquery-ui-1.10.4.custom.js">
    </script>
    <script src="_assets/plugins/jquery-appear/jquery.appear.js">
    </script>
    <script>
    function showSelectsd(){
    var selO = document.getElementsByName('multiple_select')[0];
    var selValues = [];
    for(i=0; i < selO.length; i++){
    if(selO.options[i].selected){
    selValues.push(selO.options[i].value);
    }
    document.getElementById('send').value = selValues;
    }
    }
    // MultiSelect
    $('#select_services_offered').multiselect({
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    disableIfEmpty: true,
    disabledText: "No services available",
    buttonClass: 'btn bg-blue btn-flat',
    buttonWidth: "100%"
    });
    </script>
    <!-- >> google geolocation << -->
    <script src="_assets/js/maps.js">
    </script>
    <!-- >> api key << -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeWLbDlNxutMqYPmgXHETvG-PXuJT9_Q8&libraries=places&callback=initMap" async defer></script>
    <!-- /. messages / notification -->
    <script src="_assets/js/_notification.js"></script>
  </body>
</html>