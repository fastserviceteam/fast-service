<?php
    session_start();

    require "../_require-file.php"; 
      // --- >> instantiate object
    $_globalObj = new globalClass();

    if ($_globalObj->_isLoggedIn() == false) 
    {
       header("Location: ../index.php");        
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fast Service | New Job</title>
        <!-- /. Favicon --> 
    <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
    <!-- /. include general css -->
    <?php include "_partials/_topCss.php"; ?>
  </head>

   <body class="nav-md fixed_nav">

   <div class="container body">
      <div class="main_container">

        <?php 
          # include left sidebar main navigation menu
          require "_partials/_leftColumnNavigationMenu.php"; 

          # include top navigation
          require "_partials/_topNavigation.php"; 
        ?>

        <!-- page content -->
        <div class="right_col page_content" role="main">
          
          <div class="">

            <div class="page-title">
              <div class="title_left">
                <h3><i class="fa fa-briefcase"></i> Jobs <small>create job</small></h3>
              </div>

              <div class="title_right">
                <!-- other content -->
              </div>
            </div>

          </div>

  <div class="row">


    <div class="col-md-8 col-sm-8 col-xs-12">
      <div class="x_panel tile">

        <div class="x_title">
          <h3>Create New Job</h3>
        </div>

        <div class="x_content">

          <span id="show_dataForm_Business_replies"></span>

            <form id="dataForm_createNewJob" role="form">
 
              <div class="form-group row">
                <div class="col-xs-12">
                  <label for="dataInput_job_do_you_have_a_business">Do you have a business?</label>
                    <div id="dataInput_job_do_you_have_a_business">
                      <label class="radio-inline"><input type="radio" class="dataInput_job_have_a_businessClass" name="dataInput_job_have_a_business" value="yes">Yes</label>
                      <label class="radio-inline"><input type="radio" class="dataInput_job_have_a_businessClass" name="dataInput_job_have_a_business" value="no">No</label>                      
                    </div>
                </div>
              </div>
      
              <div class="form-group row item_display_none" id="business_name_toggle">
                <div class="col-xs-12">
                  <label for="dataInput_job_businessname">Business Name</label>
                    <select name="dataInput_job_businessname" id="dataInput_job_businessname" class="form-control" style="width: 100%" data-placeholder="--- select business ---">
                      <option></option>
                      <?php foreach( $_globalObj->_businessnames() as $row ) { ?>
                          <option value="<?php echo $row['business_profile_id']; ?>"><?php echo $row['Business_Name']; ?></option>
                      <?php } ?>
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-12">
                  <label for="dataInput_job_categoryToken">Job Category</label>
                  <!-- 
                  <select class="form-control" multiple="multiple" id="dataInput_job_category" name="dataInput_job_categoryname" required style="width: 100%" data-placeholder="--- select job category ---">-->
                  <select class="form-control" id="dataInput_job_categoryToken" name="dataInput_job_categorytokenname" required onchange="loadAssocJobCategoryName(this.value)">
                      <option selected disabled> --- select job category --- </option>
                      <?php foreach($_globalObj->jobCategories() as $jcRow) { ?>
                      <option value="<?php echo $jcRow['jcToken']; ?>"><?php echo $jcRow['jcName']; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>

              <input type="hidden" value="" name="dataInput_job_categoryname" id="dataInput_job_categoryid">

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_title">Job Title</label>
                    <input type="text" id="dataInput_job_title" class="form-control" name="dataInput_job_title" placeholder="enter job title" maxlength="70" autocomplete="off">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_district">Job District</label>
                    <select name="dataInput_job_district" id="dataInput_job_district" class="form-control" style="width: 100%" data-placeholder="--- select district ---">
                      <option></option>
                      <?php foreach( $_globalObj->_districts() as $row ) { ?>
                          <option><?php echo $row['district_name']; ?></option>
                      <?php } ?>
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_location">Job Location / Duty Station</label>
                    <input type="text" id="dataInput_job_location" class="form-control" name="dataInput_job_location" placeholder="enter job location" maxlength="100" autocomplete="off" required>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-12">
                  <label for="dataInput_job_type">Job Type</label>
                   <select id="dataInput_job_type" name="dataInput_job_type" class="form-control">
                      <option selected disabled> --- select job type --- </option> 
                      <option>Full time</option>   
                      <option>Part time</option>
                      <option>Contract</option>
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_salary">Job Salary</label>
                    <input type="text" id="dataInput_job_salary" class="form-control" name="dataInput_job_salary" placeholder="enter job salary" maxlength="100" autocomplete="off" aria-describedby="jobsalaryHelp" list="job_salary_list" required>
                    <small id="jobsalaryHelp" class="text-maroon">eg. Confidential or 150000 etc</small>
                </div>
              </div>
              <datalist id="job_salary_list">
                <option value="Confidential">Confidential</option>
              </datalist>

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_posted_on">To be posted on</label>
                    <input type="text" id="dataInput_job_posted_on" class="form-control datepicker" name="dataInput_job_posted_on" placeholder="select post date" autocomplete="off">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_deadline">Deadline</label>
                    <input type="text" id="dataInput_job_deadline" class="form-control datepicker" name="dataInput_job_deadline" placeholder="select deadline" autocomplete="off" required>
                </div>
              </div>
              
              <!-- Not required right now -->
              <div class="form-group row" style="display: none;">
                <div class="col-xs-12">
                    <label for="dataInput_job_duration">Duration (Optional)</label>
                    <input type="text" id="dataInput_job_duration" class="form-control" name="dataInput_job_duration" placeholder="enter duration" autocomplete="off" aria-describedby='helpDuration'>
                    <small id="helpDuration" class="text-warning">eg. 3 months, 1 week</small>
                </div>
              </div>
              <!--- * --->

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_qualification">Qualifications</label>
                    <textarea id="dataInput_job_qualification" class="form-control bp-wysihtml5" name="dataInput_job_qualification" placeholder="enter qualifications" aria-describedby="qualificationsHelp"></textarea>
                    <small id="qualificationsHelp" class="text-maroon">Please be precise. 800 characters ONLY</small>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_responsibility">Responsibilities (150 characters for each responsibility)</label>
                    <input type="text" id="dataInput_job_responsibility1" class="form-control" name="dataInput_job_responsibility[]" placeholder="enter responsibility" autocomplete="off" maxlength="150">
                    
                    <div id="dataDiv-appendResponsibility"></div> 
                    
                    <div class="btn-group">
                        <a href="javascript:void;" class="btn btn-xs btn-flat btn-primary" id="addResponsibility">add</a> 
                        <a href="javascript:void;" class="btn btn-xs btn-flat btn-danger" id="removeResponsibility">remove</a>
                    </div> 
     
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-12">
                    <label for="dataInput_job_how_to_apply">How to apply</label>
                    <textarea id="dataInput_job_how_to_apply" class="form-control" name="dataInput_job_how_to_apply" placeholder="enter how to apply" maxlength="200" aria-describedby="howtoapplyHelp"></textarea>
                    <small id="howtoapplyHelp" class="text-maroon">200 characters ONLY</small>
                </div>
              </div>

              <div class="ln_solid"></div>

              <div class="form-group row">
                <div class="col-xs-12">
                  <button type="submit" class="btn c_btn c_btnTwo btn-flat" id="dataBtn_createNewJob">Submit</button>
                </div>
              </div>

            </form>

        </div>
      </div>
    </div>
    <!-- /. col-md-8 col-sm-8 col-xs-12 | left -->

    <div class="col-md-4 col-sm-4 col-xs-12">

        <?php include '_partials/_rightColumnAdvertsJobs.php'; ?>

    </div>
    <!-- /. col-md-4 col-sm-4 col-xs-12 | right -->

  </div>
  <!-- /. row -->


        
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)">Fast Service</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- >> include bottom scripts << -->
    <?php require_once "_partials/_bottomScripts.php"; ?>

    <!-- >> page specific scripts << -->
    <script>
      $(function()
      {   
        // --- >> select2 for selecting services offered
        $(".select2services").select2({
          placeholder: '--- select services offered ---',
          maximumSelectionLength: 5,
          allowClear: true
        });
        // --- >> select2 for selecting businesses available
      $("#dataInput_job_businessname").select2({
        placeholder: '--- select Business ---',
        tags: true, 
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        language: {
              noResults: function () {
                   return "No Business Created Yet. <a href='_navigation.php?pageNavigation=createNewBusiness'>Create one?</a>";
          }
        }
      });

      // --- >> select2 for selecting districts
      $("#dataInput_job_district").select2({
        placeholder: '--- select district ---',
        tags: true, 
        allowClear: true        
      });

      // --- >> select2 for job category
     /* $("#dataInput_job_category").select2({
        placeholder: '--- select job category ---',
        tags: false, 
        allowClear: false,
        maximumSelectionLength: 3
      });
      */
      /*

      $('#dataInput_job_category').multiselect({
          enableCaseInsensitiveFiltering: true,
          enableFiltering: true,
          disableIfEmpty: true,
          disabledText: "Select job category / categories",
          buttonClass: 'btn btn-primary btn-flat',
          buttonWidth: "100%"
      });
       
      */

      // --- >> does user have a business

      // $("input[name='optradio']:checked").val();      

        $(".dataInput_job_have_a_businessClass").on('click', function() {
            var businessYesNoValue = $("input[name='dataInput_job_have_a_business']:checked").val();
            if (businessYesNoValue == "yes") 
            {
              $("#business_name_toggle").slideDown();
            } else {
              $("#business_name_toggle").slideUp();
            }
        });              

          //bootstrap WYSIHTML5 - text editor
          $(".bp-wysihtml5").wysihtml5({
              toolbar:{
                  "image": false,
                  "link": false,
                  "blockquote": false,
                  "color": true
              }
          });
         
            // prettly print
            window.prettyPrint;
            prettyPrint();

      });

      // load job category name to input hidden
      function loadAssocJobCategoryName(jcTokenArgs){
        $.post("_server_requests.php",{
            jcTokenAssocName:jcTokenArgs
        }, function(data){
            $("#dataInput_job_categoryid").val(data)
        });

      }
       
    </script>  
  
  </body>
</html>
