<?php
    session_start();

    require "../_require-file.php"; 
      // --- >> instantiate object
    $_globalObj = new globalClass();

    if ($_globalObj->_isLoggedIn() == false) 
    {
       header("Location: ../index.php");        
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fast Service | Messages</title>

    <link rel="shortcut icon" type="image/x-icon" href="../_assets/img/logo.png" />
    <!-- /. for css which should appear on every page; include them in this file -->
    <?php include "_partials/_topCss.php"; ?>

    <style>
      @media print{
          @page {
            margin: 0;
            padding: 0;
            background: white;
            size: A4 portrait;
          }
          body,
          .mail_view {
            width: 100%;
            height: auto;            
          }
          * {
            -webkit-print-color-adjust:exact;
          }
          .noprint {
              display: none !important;
          }
          .view-mail,
          .view-mail p {
            text-align: justify;
          }
          .view-mail p {
            font-weight: normal;
          }
          .compose,
          .compose .compose-header,
          .x_panel,
          .x_title,
          .mail_list_column,
          .mail_view {
            border: none !important; 
          }
      }
    </style>
  </head>

  <body class="nav-md fixed_nav">

    <div class="container body" id="main_wrapper">
      
      <div class="main_container">

    <!-- include left sidebar main navigation menu -->
    <?php require "_partials/_leftColumnNavigationMenu.php"; ?>

        <!-- top navigation -->
        <div class="top_nav noprint">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo '_assets/clientUploads/profile/'.$_globalObj->_sessionProfilePicture(); ?>" alt="user-pic">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="?logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green"><?php $_globalObj->_receivedMessagesCount($_SESSION['ur_email']); ?></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
          <?php $_globalObj->_retrieveMessages($_SESSION['ur_email']);?>
                    <li>
                      <div class="text-center">
                        <a href="messages.php">
                          <strong>See All Messages</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col page_content" role="main">  
          <div class="">       

            <div class="page-title noprint">
              <div class="title_left">
                <h3 class="text-navy"><i class="fa fa-envelope-o"></i> Messages<small> your mail</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for mail...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix noprint"></div>    

<div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title noprint">
                    <h2>Inbox<small>User Mail</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content inbox-blue-dark">
                    <div class="row">
                      <div class="col-sm-12 col-xs-12 col-md-3 mail_list_column noprint">
                        <!-- compose new message -->
                        <button id="compose" class="btn btn-sm btn-success btn-block" type="button">COMPOSE</button>
                          
                        <!-- filter mail list -->
                        <div class="form-group">
                          <select name="input_filter_messages_selection" id="input_filter_messages_selection" class="form-control">
                            <option value="inbox" selected>Inbox</option>
                            <option value="sent">Sent</option>
                            <option value="trash">Trash</option>
                            <!--<option value="starred">Starred</option>-->
                          </select>                          
                        </div>
                        
                        <!-- mailing list -->
                        <div id="messages_wrapper">

                            <?php 
                              // default view
                              $_globalObj->_messagesInbox(); 
                            ?>                       
                                            
                        </div>
                        <!-- mail_list_details -->

                      </div>
                      <!-- /MAIL LIST -->

                      <!-- CONTENT MAIL -->
                      <div class="col-sm-12 col-xs-12 col-md-9 mail_view">
                        <div class="inbox-body" id="load_message_body_div">
                            
                            <h3 class="text-maroon text-center" id="no_preview_p">Select any message on your left to view</h3>

                        </div>
                        <!-- inbox-body ~ load_message_body_div -->

                      </div>
                      <!-- /CONTENT MAIL -->
                    </div>
                  </div>
                </div>
              </div>
            </div>

			     </div>
           <!-- /. -->
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer class="noprint">
          <div class="pull-right">
            All rights reserved &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)">Fast Service</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
      <!-- main_container -->

    </div>
   <!-- .container .body #main_wrapper -->


   <!-- =========== >> compose message content << =========== -->
    <div class="compose col-md-6 col-xs-12 noprint">
      <div class="compose-header">
        New Message
        <button type="button" class="close compose-close">
          <span>×</span>
        </button>
      </div>

      <form role="form" id="sendMessageData_form" class="clear_form_after_submission">

        <div class="compose-body p-5">
          <div id="alerts"></div>

          <div class="form-group">
            <label for="input_receivingEmailAddress" class="message_compose">To Email Address</label>
              <input type="email" name="input_receivingEmailAddress" id="input_receivingEmailAddress" class="form-control" placeholder="To: email address" required autocomplete="off">
          </div>

          <div class="form-group">
            <label for="input_receivingSubject" class="message_compose">Subject - <small class="text-maroon">100 characters Only</small></label>
              <input type="text" name="input_receivingSubject" id="input_receivingSubject" class="form-control" placeholder="Subject:" maxlength="100" required autocomplete="off">
          </div>

          <div class="form-group">
            <label for="editor" class="message_compose">Message</label>

          <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
            <div class="btn-group">
              <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
              <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
              <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
            </div>

            <div class="btn-group">
              <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
              <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
              <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
              <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
            </div>

            <div class="btn-group">
              <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
              <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
              <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
              <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
            </div>

            <div class="btn-group">
              <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
              <div class="dropdown-menu input-append">
                <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                <button class="btn" type="button">Add</button>
              </div>
              <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
            </div>

            <div class="btn-group">
              <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
              <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
            </div>
          </div>

          <div id="editor" class="editor-wrapper"></div>
          </div>
   
        <!-- hidden fields -->
        <!--<textarea name="input_receivingMesage" id="input_receivingMesage" class="element-hidden"></textarea>-->
        <input type="hidden" id="input_sendingEmail" value="<?php echo $_SESSION['ur_email']; ?>">
        <!-- /. -->
        
        </div>
        <!-- compose-body -->

        <div class="compose-footer">
          <button id="sendMessageBtn" class="btn btn-sm btn-success" type="button">Send</button>
        </div>

      </form>
      <!-- /. sendMessageData_form -->

    </div>
    <!-- /compose -->

    <!-- >> include bottom scripts << -->
    <?php require_once "_partials/_bottomScripts.php"; ?>


    <!-- >> /. page specific scripts << -->

    <script>
      $(function(){
          
          /* WYSIWYG EDITOR */
              
            if( typeof ($.fn.wysiwyg) === 'undefined'){ return; }
            console.log('init_wysiwyg');  
              
                function init_ToolbarBootstrapBindings() {
                  var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                      'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                      'Times New Roman', 'Verdana'
                    ],
                    fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                  $.each(fonts, function(idx, fontName) {
                    fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                  });
                  $('a[title]').tooltip({
                    container: 'body'
                  });
                  $('.dropdown-menu input').click(function() {
                      return false;
                    })
                    .change(function() {
                      $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                    })
                    .keydown('esc', function() {
                      this.value = '';
                      $(this).change();
                    });

                  $('[data-role=magic-overlay]').each(function() {
                    var overlay = $(this),
                      target = $(overlay.data('target'));
                    overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                  });

                  if ("onwebkitspeechchange" in document.createElement("input")) {
                    var editorOffset = $('#editor').offset();

                    $('.voiceBtn').css('position', 'absolute').offset({
                      top: editorOffset.top,
                      left: editorOffset.left + $('#editor').innerWidth() - 35
                    });
                  } else {
                    $('.voiceBtn').hide();
                  }
                }

                function showErrorAlert(reason, detail) {
                  var msg = '';
                  if (reason === 'unsupported-file-type') {
                    msg = "Unsupported format " + detail;
                  } else {
                    console.log("error uploading file", reason, detail);
                  }
                  $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                }

               $('.editor-wrapper').each(function(){
              var id = $(this).attr('id');  //editor-one
              
              $(this).wysiwyg({
                toolbarSelector: '[data-target="#' + id + '"]',
                fileUploadError: showErrorAlert
              }); 
            });
         
            // prettly print
            window.prettyPrint;
            prettyPrint();

          /* ------- compose toggle ------- */        
          $('#compose, .compose-close').click(function(){
            $('.compose').css("max-height","480px")
            $('.compose').slideToggle();
          });
      });
    </script>

  </body>
</html>