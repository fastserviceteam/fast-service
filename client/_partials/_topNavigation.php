<!-- /. top navigation -->
<div class="top_nav" style="background-color:#00BCD4; height:9.2%;">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle">
				<a id="menu_toggle">
					<i class="fa fa-bars text-white">
					</i>
				</a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li class="">

					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<img src="_assets/images/pp.gif" alt="user-pic" class="client_avatar_class">
						<span class="fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li>
							<a href="_navigation.php?pageNavigation=myProfile"> Profile
							</a>
						</li>
						<li>
							<a href="index.php?logout">
								<i class="fa fa-sign-out pull-right">
								</i> Log Out
							</a>
						</li>
					</ul>
				</li>
				<li role="presentation" class="dropdown">
					<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
						<span class="text-white"><span class="notify-text-span">Messages </span><i class="fa fa-envelope"></i></span>
						<span class="badge bg-maroon">
							<?php $_globalObj->_receivedMessagesCount($_SESSION['ur_email']); ?>
						</span>
					</a>
					<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
						<?php $_globalObj->_retrieveMessages($_SESSION['ur_email']);?>
						<li>
							<div class="text-center">
								<a href="messages.php">
									<strong>See All Messages
									</strong>
									<i class="fa fa-angle-right">
									</i>
								</a>
							</div>
						</li>
					</ul>
				</li>
				<!--=================end =================-->
				<!--================= Notifications messages for client =================-->
				<li role="presentation" class="dropdown">
					<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
						<span class="text-white"><span class="notify-text-span">Business </span><i class="fa fa-bell"></i></span>
						<span class="badge bg-orange real_time3"> </span>
					</a>
					<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
						<li><a data-target="#viewSentRequests_2" data-toggle="modal"> Received Requests   <span class="badge pull-right bg-orange real_time3"> </span></a></li>
						<li><a data-target="#viewNotification_5"  data-toggle="modal">  Sent Quotations  <span class="badge pull-right bg-orange real_time4"> </span></a></li>
						<li><a href="">  Accepted Quotations   <span class="badge pull-right bg-orange "> </span></a></li>
						<li><a href=""> Started Transactionss  <span class="badge pull-right bg-orange "> </span></a></li>
						<li>
							<div class="text-center">
								<a href="">
									<strong>View All
									</strong>
									<i class="fa fa-angle-right">
									</i>
								</a>
							</div>
						</li>
					</ul>
				</li>
				<!--=================end =================-->
				<!--=================Requests Notifications messages for client =================-->
				<li role="presentation" class="dropdown">
					<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
						<span class="text-white"><span class="notify-text-span">Client </span><i class="fa fa-user"></i></span>
						<span class="badge bg-red real_time2"></span>
					</a>
					<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
						<li><a data-target="#viewSentRequests" data-toggle="modal"> Sent Requests  <span class="badge pull-right bg-red real_time2"></span> </a></li>
						<li><a data-target="#viewNotification_4" data-toggle="modal">  Received Quotations  <span class="badge pull-right real_time5 bg-red "></span> </a></li>
						<li><a href="">  Confirmed Quotations   <span class="badge bg-red pull-right "></span></a></li>
						<li><a href=""> Ongoing Transactionss  <span class="badge pull-right bg-red "></span></a></li>
						<li>
							<div class="text-center">
								<a href="">
									<strong>view all
									</strong>
									<i class="fa fa-angle-right">
									</i>
								</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</div>