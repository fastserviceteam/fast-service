<!--
- main navigation menu Partial appearing on left side
-->
<div class="col-md-3 left_col menu_fixed" id="main-sidebar">
	<div class="left_col scroll-view">
		<!-- /. logo and title -->
		<div class="navbar nav_title" style="background-color:#009688;">
			<a href="dashboard.php" class="site_title">
				<img src="_assets/images/fs-logo.png" alt="Logo" class="img-circle">
				<span class="text-white">Fast Service!</span>
			</a>
		</div>
		<div class="clearfix"></div>

		<!-- menu profile quick info -->
		<!--
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="<?php /* echo '_assets/clientUploads/profile/'.$_SESSION["user_profile_pic"]; ?>" alt="..." class="img-circle profile_img client_avatar_class">
			</div>
			<div class="profile_info">
				<span>Welcome,</span>
				<h2><?php echo $_SESSION['emailUname']; */ ?></h2>
			</div>
		</div>
	-->
		<!-- /menu profile quick info -->

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<ul class="nav side-menu">
					<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashoard</a></li>
					<li class="business_li"><a><i class="fa fa-money"></i> Business <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="_navigation.php?pageNavigation=createNewBusiness">Create New Business</a></li>
						<li><a href="_navigation.php?pageNavigation=myBusinessList">My Business List</a></li>
					</ul>
				</li>
				<li class="jobs_li"><a><i class="fa fa-briefcase"></i> Jobs<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="_navigation.php?pageNavigation=createNewJob">Create New Job</a></li>
					<li><a href="_navigation.php?pageNavigation=myJobList">Job Lists</a></li>
				</ul>
			</li>
			<li class="adverts_li"><a><i class="fa fa-info-circle"></i> Adverts<span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li class="adverts_li_add"><a href="_navigation.php?pageNavigation=myAdvert" data-toggle="modal">Create Advert</a></li>
				<li class="adverts_li_list"><a href="_navigation.php?pageNavigation=myAdvertList">View All adverts</a></li>
			</ul>
		</li>
		<li><a href="javascript:void(0)"><i class="fa fa-exchange"></i> Market and Economy</a></li>
		<li><a href="javascript:void(0)"><i class="fa fa-newspaper-o"></i> News and Events</a></li>
	</ul>
</div>
<div class="menu_section_custom">
	<h3>OTHERS</h3>
	<ul class="nav side-menu">
		<li><a href="_navigation.php?pageNavigation=myProfile"><i class="fa fa-user-secret"></i> Profile</a></li>
		<li><a href="messages.php"><i class="fa fa-envelope-o"></i> Messages</a></li>
		<li><a href="javascript:void(0)"><i class="fa fa-question-circle"></i> Help Center</a></li>
		<li><a href="index.php?logout"><i class="fa fa-sign-out"></i> Logout</a></li>
	</ul>
</div>
</div>
<!-- /sidebar menu -->
</div>
</div>
<!-- /. start of fixed sidebar menu and footer |  col-md-3 left_col -->