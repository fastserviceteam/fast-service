<!-- Bootstrap -->
<link href="_assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="_assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- bootstrap-progressbar -->
<link href="_assets/plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
<!-- datepicker css -->
<link rel="stylesheet" href="_assets/plugins/datepicker/datepicker3.css">
<!-- /. bootstrap multiselect plugin -->
<link rel="stylesheet" href="_assets/plugins/bootstrap-multiselect/bootstrap-multiselect.css" />
<!-- NProgress -->
<link href="_assets/plugins/nprogress/nprogress.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="_assets/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
<!-- select2 -->
<link rel="stylesheet" href="_assets/plugins/select2/dist/css/select2.min.css">
<!-- custom css -->
<link href="_assets/css/custom.css" rel="stylesheet">
<!-- PNotify -->
<link href="_assets/plugins/pnotify/dist/pnotify.css" rel="stylesheet">
<link href="_assets/plugins/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
<link href="_assets/plugins/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<!-- easing scroll -->
<link rel="stylesheet" href="_assets/plugins/scroll/easing.css">
<!-- bootstrap-wysiwyg -->
<link href="_assets/plugins/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- Datatables -->
<link href="_assets/plugins/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="_assets/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css" rel="stylesheet">
<link href="_assets/plugins/datatables/extensions/Scroller/css/dataTables.scroller.css" rel="stylesheet">
<!-- Head Libs -->
<script src="_assets/plugins/modernizr/modernizr.js"></script>