<?php // include "_partials/_chatBox.php"; ?>
<!-- 
/* -----------------------------
/* including bottom scripts
/* -----------------------------
-->

<!-- scroll to top link -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

<!-- jQuery -->
<script src="_assets/plugins/jquery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap -->
<script src="_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- bootstrap multiselect -->
<script src="_assets/plugins/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<!-- /. system script -->
<script src="_assets/js/page_script.js"></script>
<!-- FastClick -->
<script src="_assets/plugins/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="_assets/plugins/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script src="_assets/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="_assets/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!--  /. moment -->
<script src="_assets/js/moment/moment.min.js"></script>
<!-- bootstrap-datetimepicker -->    
<script src="_assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="_assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap typeahead autocomplete -->
<script src="_assets/plugins/typehead/bootstrap3-typeahead.js"></script>
<!-- datepicker -->
<script src="_assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- DateJS -->
<script src="_assets/plugins/DateJS/build/date.js"></script>
<!-- select2 -->
<script src="_assets/plugins/select2/dist/js/select2.full.js"></script>
<!-- /. bootbox alert -->
<script src="_assets/js/bootbox.min.js"></script>
<!-- bootstrap dataTables -->
<script src="_assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="_assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- PNotify -->
<script src="_assets/plugins/pnotify/dist/pnotify.js"></script>
<script src="_assets/plugins/pnotify/dist/pnotify.buttons.js"></script>
<script src="_assets/plugins/pnotify/dist/pnotify.nonblock.js"></script>
<!-- move to top -->
<script src="_assets/plugins/scroll/easing.js"></script>
<script src="_assets/plugins/scroll/move-top.js"></script>
<!-- Parsley -->
<script src="_assets/plugins/parsleyjs/dist/parsley.min.js"></script>
<!-- /. piczoomer -->
<script src="_assets/plugins/imageZoom/imagezoom.js"></script>
<!-- bootstrap-wysiwyg -->
<!--<script src="_assets/plugins/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>-->
<script src="_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="_assets/plugins/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="_assets/plugins/google-code-prettify/src/prettify.js"></script>
<!-- bootstrap max-length -->
<script src="_assets/plugins/bootstrap-maxlength/bootstrap-maxlength.js"></script>
<!-- init Script -->
<script src="_assets/js/custom.js"></script>

<script>
  $(function(){
    $('#dateofservicedelivery').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    
    $('#timeofservicedelivery').datetimepicker({
        format: 'hh:mm A'
    });

    // --- >> typeahead autocomplete
    var $inputSearchValue = $("#dataForm_servicerequired");

      $inputSearchValue.typeahead({
          source: function (query, result) {
              $.ajax({
                  url: "_server_requests.php",
        data: {servicesOffered: query},            
                  dataType: "json",
                  type: "POST",
                  success: function (data) {
          result($.map(data, function (item) {
            return item;
                      }));
                  }
              });
          }
      });
	/*
    cursorcolor:'255,255,255',
    opacity:0.5,
    cursor:'crosshair',
    zindex:2147483647,
    zoomviewsize:[480,395],
    zoomviewposition:'right',
    zoomviewmargin:10,
    zoomviewborder:'none',
    magnification:1.925
    */
});  
  
</script>
<!-- 
/* ------------------------------------
/* including bottom html modal partials
/* ------------------------------------
-->
<?php require "_partials/_modalViews.php"; ?>
