<!-- >> modal container << -->
<div class="container">

  <!-- Subscribe to notifications Modal -->
  <div class="modal fade" id="notificationSubscriptionModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header bg-purple text-white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-uppercase text-center">Push Notifications</h4>
        </div>
        <div class="modal-body">
          <div class="push-notification-body">

              <h5>Would you like to receive notifications?</h5>

              <label class="radio-inline"><input type="radio" name="dataFormAdvert_have_a_business" value="yes">Yes</label>
              <label class="radio-inline"><input type="radio" name="dataFormAdvert_have_a_business" value="no">No</label> 

              <select class="form-control">
                  <option value="">All Job Functions</option>
                  <option value="accounting-auditing">Accounting, Auditing &amp; 
                  Finance</option>
                  <option value="administrative">Administrative &amp; Office</option>
                  <option 
                  value="farming-agriculture">Agriculture &amp; Farming</option>
                  <option value="building-architecture">Building &amp; 
                  Architecture</option>
                  <option value="social-services">Community &amp; Social Services</option>
                  <option 
                  value="consulting">Consulting &amp; Strategy</option>
                  <option value="creative">Creative &amp; Design</option>
                  <option 
                  value="customer-service">Customer Service &amp; Support</option>
                  <option value="engineering">Engineering</option>
                  <option 
                  value="food-services-catering">Food Services &amp; Catering</option>
                  <option value="health-safety">Health &amp; 
                  Safety</option>
                  <option value="hospitality">Hospitality/Leisure/Travel</option>
                  <option value="human-resources">Human 
                  Resources</option>
                  <option value="it-software">IT &amp; Software</option>
                  <option value="legal">Legal Services</option>
                  <option 
                  value="management-business-development">Management &amp; Business Development</option>
                  <option 
                  value="marketing-communications">Marketing &amp; Communications</option>
                  <option value="medical-pharmaceutical">Medical &amp; 
                  Pharmaceutical</option>
                  <option value="other">Other</option>
                  <option value="project-management">Project &amp; Product 
                  Management</option>
                  <option value="quality-control">Quality Control &amp; Assurance </option>
                  <option 
                  value="property-management">Real Estate &amp; Property Management</option>
                  <option value="teaching-training">Research, Teaching 
                  &amp; Training</option>
                  <option value="sales">Sales</option>
                  <option value="security">Security</option>
                  <option 
                  value="supply-chain-procurement">Supply Chain &amp; Procurement</option>
                  <option value="trades-services">Trades &amp; 
                  Services</option>
                  <option value="transport-logistics">Transport &amp; Logistics</option>
</select>
                      

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-warning text-white" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Job details Modal -->
  <div class="modal fade" id="editProfileModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-purple text-white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-uppercase text-center">Editing Profile</h4>
        </div>
        <div class="modal-body">
          <?php $_globalObj->clientEditingform(); ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-warning text-white" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Job details Modal -->
  <div class="modal fade" id="dataModalJobs" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header bg-purple text-white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-uppercase text-center">Jobs</h4>
        </div>
        <div class="modal-body">
          <span id="load-Job-Details-Span"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-warning text-white" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Advert details Modal -->
  <div class="modal fade" id="dataModalAdverts" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-purple text-white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-uppercase text-center">Advert Details</h4>
        </div>
        <div class="modal-body">
          <span id="load-Advert-Details-Span"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-warning text-white" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!--STEP 1: Clients Sent request notifications Modal -->
<?php /* ?>   
<div class="modal fade" id="viewSentRequests" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="  modal-header bg-purple text-white">
        <h3 class="notify"> <button type="button" class="pull-right  btn-danger close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button><center>Sent Requests<span id="xx" style="color:red"></span></center>
        </h3>
      </div>
      <div class="modal-body"> 
	  
          <?php 
			require "_notifications/_sent_requests.php";
		  ?>
		   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
		
	
  <!-- STEP 2: vander received requests notifications Modal -->
   
<div class="modal fade" id="viewSentRequests_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="  modal-header bg-purple text-white">
        <h3 class="notify"> <button type="button" class="pull-right  btn-danger close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button><center>Clients Requests<span id="xx" style="color:red"></span></center>
        </h3>
      </div>
      <div class="modal-body"> 
          <?php
			require "_notifications/_client_sent_requests.php";
		  ?>
		   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>

<!-- STEP 3: vander received requests notifications Modal -->   
<div class="modal fade" id="viewSentRequests_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="  modal-header bg-purple text-white">
        <h3 class="notify"> <button type="button" class="pull-right  btn-danger close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button><center>Clients Requests</center>
        </h3>
      </div>
      <div class="modal-body"> 
          <?php
			require "_notifications/_client_sent_requests.php";
		  ?>
		   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
		
	 <!-- STEP 4: client received requests notifications Modal -->
   
<div class="modal fade" id="viewNotification_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="  modal-header bg-purple text-white">
        <h3 class="notify"> <button type="button" class="pull-right  btn-danger close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button><center>Received Quotation</center>
        </h3>
      </div>
      <div class="modal-body"> 
          <?php
			require "_notifications/_client_received_requests.php";
		  ?>
		   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
		

		<!-- STEP 5: vander sent requests  Modal -->
   
<div class="modal fade" id="viewNotification_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="  modal-header bg-purple text-white">
        <h3 class="notify"> <button type="button" class="pull-right  btn-danger close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button><center> Quotations Sent</center>
        </h3>
      </div>
      <div class="modal-body"> 
          <?php
			require "_notifications/_vander_sent_quotation.php";
		  ?>
		   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
		
		
<?php */ ?>


<!-- >> modal container << -->
</div>