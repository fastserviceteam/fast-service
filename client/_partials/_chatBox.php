<!-- /. fixed bottom right chatbox ./ -->

<div class="container">
	<div class="row">

		<div class="col-md-6 col-xs-12" id="fast-service-popup-chatbox">
			<div class="chat-header">
				<!--<span class="badge-notifi text-olive">0</span>--> <i class="fa fa-comments"></i> Live Chat
				<a href="javascript:void(0)" class="close" id="close-chatbox" title="Open chatbox">
					<i class="fa fa-angle-up text-white" id="chat-close-icon"></i>
				</a>
			</div>
			<div class="chat-body-wrapper">
				<div class="chat-body">
					
				</div>
				  <div class="chat-actions">
				  	<form role="form" id="chat-form">

				  		<textarea id="chat_message" placeholder="type your message" class="form-control" maxlength="150"  aria-describedby="chatHelp"></textarea>

				  		<span id="chatHelp" class="text-info">150 Characters Remaining</span>

				  	</form>
				    
		    	</div>
			</div>
		</div>
		
	</div>
</div>
