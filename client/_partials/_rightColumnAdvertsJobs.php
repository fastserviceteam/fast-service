<!--
  - Job Adverts Partial appearing on right side
-->
<div class="x_panel">
  <!-- uncomment to show tab title
  <div class="x_title">
    <h2><i class="fa fa-bars"></i> Jobs &amp; Adverts</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
-->
  <div class="x_content">

    <div class="" role="tabpanel" data-example-id="togglable-tabs">
      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab_content1" id="adverts-tab" role="tab" data-toggle="tab" aria-expanded="true">Adverts</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="jobs-tab" data-toggle="tab" aria-expanded="false">Jobs</a>
        </li>
      </ul>
      <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="adverts-tab">
          <div class="adverts_jobs_content">
            <?php $_globalObj->_retrieveAdverts(); ?>
          </div>
        </div>
        <!-- /. adverts-tab -->

        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="jobs-tab">
          <div class="adverts_jobs_content">
            <?php  $_globalObj->_retrieveJobs(); ?> 
          </div>
        </div>
        <!-- /. jobs-tab -->
      </div>
    </div>

  </div>
</div>