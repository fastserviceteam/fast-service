 <?php
	require "../_require-file.php"; // database config, server feedbacks, phpmailer class and other global constants
	require "../_server-functions.php"; // custom functions
	
	if(!empty($_POST["keyword"])) {
	
	$query = GetAll("services","WHERE service_name like '%".$_POST["keyword"]."%' ORDER BY service_name LIMIT 1000");
	if(!empty($query)) {
	?>
		<ul id="search-list">
		<?php 
		$ix = 0; $ix++; 
		
		foreach($query as $row)
		{	 
		?>
		<li onClick="selectsearch<?=$ix;?>('<?=$row->service_name;?>');" title="Service Location : <?=$row->Business_Address;?>">
			<div class="row">
				<div class="col-xs-10">
					<p>
						<?=$row->service_name;?>
					</p>
				</div>
				<div class="col-xs-2">
					<p class="text-right start_category">
						<i class="fa fa-star"></i> <?=$row->Business_Category;?>
					</p>
				</div>
			</div>		
		</li>
		
		<input type="text" id="coverage_area<?=$row->service_id;?>" name="pic_service_id" class="hidden" value="<?=$row->service_id;?>">
		<script>
		 function selectsearch<?=$ix;?>(val) {
			$("#search-box").val(val); 
			$("#suggesstion-box").hide(); 
					$.ajax({
					type: "POST",
					url: "return_services.php",
					data:'searched_service='+$("#search-box").val()+ '&client_email='+$("#get_client_email").val(),
					success: function(msg){
						$("#r_services").slideDown();
						$("#return_available_services").html(msg);
					} 
				});
		}
		</script>
		<?php 
		$ix++; 
 
		}
		?>
		</ul>
		<?php 
		}else{
		echo '<div class="alert-custom alert-danger fade in">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  No Service Currently available for your search, please try again!
		</div>';
	} 
	}
	?>
	
	 
	