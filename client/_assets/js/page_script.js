/* 
----------------------- -----------------------
					$.jQuery()	
----------------------- -----------------------
*/

/* ------------------------------------ */
/* variables
/* ------------------------------------ */
var reLoad = "reload";

$(function() {



/* ------------------------------------ */
/* general
/* ------------------------------------ */

// profile image reload
function reloadProfileImage() {
	$.post("_server_requests.php",{
		clientProfileImageReload: reLoad
	}, function(data){
		$(".client_avatar_class").attr("src", data);
	});
}



/* ------------------------------------ */
/* profile
/* ------------------------------------ */





/* ------------------------------------ */
/* messages
/* ------------------------------------ */

// --- load sent , received, trash messages
$("#input_filter_messages_selection").on("change", function(){

	$("#messages_wrapper").html("<p class='text-center text-maroon'><i class='fa fa-spinner fa-spin'></i></p>");

	var filterValue = $("#input_filter_messages_selection").val();

	$.post("_server_requests.php",{
		mailListfilter:filterValue
	}, function(data){
		$("#messages_wrapper").html(data).fadeIn();
	});
	/*
	if (filterValue == 'inbox') 
	{
		$("#load_inbox_messages_inbox").fadeIn();
		$("#load_inbox_messages_bin").hide();
		$("#load_inbox_messages_sentbox").hide();
		//$("#load_inbox_messages_inbox").removeClass("item_display_none");
		//$("#load_inbox_messages_bin, #load_inbox_messages_sentbox").addClass("item_display_none");

		//$("#change_title_box").html('<i class="fa fa-inbox fa-1x"></i> Inbox');
	}
	else if (filterValue == 'sent') 
	{
		$("#load_inbox_messages_sentbox").fadeIn();
		$("#load_inbox_messages_inbox").hide();
		$("#load_inbox_messages_bin").hide();
		//$("#load_inbox_messages_sentbox").removeClass("item_display_none");
		//$("#load_inbox_messages_bin, #load_inbox_messages_inbox").addClass("item_display_none");

		//$("#change_title_box").html('<i class="fa fa-send fa-1x"></i> Sent');
	}
	else if (filterValue == 'trash') 
	{
		$("#load_inbox_messages_bin").fadeIn();
		$("#load_inbox_messages_inbox").hide();
		$("#load_inbox_messages_sentbox").hide();
		//$("#load_inbox_messages_bin").removeClass("item_display_none");
		//$("#load_inbox_messages_sentbox, #load_inbox_messages_inbox").addClass("item_display_none");

		//$("#change_title_box").html('<i class="fa fa-trash-o fa-1x"></i> Trash');
	}

	*/

});



// --- >> send email
$("#sendMessageBtn").on("click", function(){
	$('.compose').slideUp();

	$("#sendMessageBtn").prop("disabled", true);
	$("#sendMessageBtn").html("Sending...");

	var receivingEmailAddress = $("#input_receivingEmailAddress").val();
	var receivingSubject      = $("#input_receivingSubject").val();
	var sendingEmail          = $('#input_sendingEmail').val();
	var receivingMessage      = $("#editor").html();

	if (receivingEmailAddress == '' || receivingSubject == '' || receivingMessage == '' ) 
	{
		_erNotfify("please check for empty fields!");
			$("#sendMessageBtn").prop("disabled", false);
			$("#sendMessageBtn").html("Send");
	}
	else
	{
		$.ajax({
	      url: "_server_requests.php", 
		  type: "POST",       
		  data: {sendingEmail:sendingEmail, receivingEmailAddress:receivingEmailAddress, receivingSubject:receivingSubject, receivingMessage:receivingMessage},  
		  cache: false,
		  success: function(data)
		  {
			if ($.trim(data) == 1) {
				_genNotify("Message Sent!")
					$("#sendMessageData_form")[0].reset();
					$("#editor").html('')
					$('.compose').slideUp();
			} else {
				_erNotfify("Failed to send Message!")
			}
			$("#sendMessageBtn").prop("disabled", false);
			$("#sendMessageBtn").html("Send");
		  }
		});	
	} // else

});


	// ---- show more messages info
	$("#toggle_info").click(function(){
		$("#toggle_more_message_info").slideToggle("slow");
	});


	// --- >> create new business
	$("#dataForm_createNewBusiness").on("submit", function(e)
	{		
		e.preventDefault();

		$("#dataBtn_createNewBusiness").prop("disabled", true);
		$("#dataBtn_createNewBusiness").html("please wait...");	

		var business_name 			 = $("#dataInput_business_name").val();
		var business_category 		 = $("#dataInput_business_category").val();
		var business_pcontact 		 = $("#dataInput_business_pcontact").val();
		var business_emailaddress 	 = $("#dataInput_business_emailaddress").val();
		var business_address 		 = $("#dataInput_business_address").val();
		var business_servicesoffered = $("#dataInput_business_servicesoffered").val();
		var address 				 = $("#address").val();

		if (business_name == '') 
		{
			bootbox.alert('<p class="text-center text-danger">Error: please enter business name</p>');
	
				$("#dataBtn_createNewBusiness").prop("disabled", false);
				$("#dataBtn_createNewBusiness").html("Submit");	
		}
		else if(business_category == '')
		{
			bootbox.alert('<p class="text-center text-danger">Error: please enter business category</p>');
	
				$("#dataBtn_createNewBusiness").prop("disabled", false);
				$("#dataBtn_createNewBusiness").html("Submit");	
		}	
		else if(business_pcontact == '')
		{
			bootbox.alert('<p class="text-center text-danger">Error: please enter business phone contact</p>');
	
				$("#dataBtn_createNewBusiness").prop("disabled", false);
				$("#dataBtn_createNewBusiness").html("Submit");	
		}	
		else if(business_emailaddress == '')
		{
			bootbox.alert('<p class="text-center text-danger">Error: please enter business email address</p>');
	
				$("#dataBtn_createNewBusiness").prop("disabled", false);
				$("#dataBtn_createNewBusiness").html("Submit");	
		}	
		else if(business_address == '')
		{
			bootbox.alert('<p class="text-center text-danger">Error: please select business business country</p>');
	
				$("#dataBtn_createNewBusiness").prop("disabled", false);
				$("#dataBtn_createNewBusiness").html("Submit");	
		}	
		else if(business_servicesoffered == '')
		{
			bootbox.alert('<p class="text-center text-danger">Error: please enter select service(s) offered</p>');
	
				$("#dataBtn_createNewBusiness").prop("disabled", false);
				$("#dataBtn_createNewBusiness").html("Submit");	
		}
		else if(address == '')
		{
			bootbox.alert('<p class="text-center text-danger">Error: please enter business area of operation</p>');
	
				$("#dataBtn_createNewBusiness").prop("disabled", false);
				$("#dataBtn_createNewBusiness").html("Submit");	
		}		
		else
		{

			$.ajax({
		      url: "_server_requests.php", 
			  type: "POST",       
			  data: new FormData(this),
			  contentType: false,   
			  cache: false,         
			  processData:false, 
			  success: function(data)
			  {
				bootbox.alert(data);
					$("#dataBtn_createNewBusiness").prop("disabled", false);
					$("#dataBtn_createNewBusiness").html("Submit");	
			  }
			});	
		}

	});


/* ------------------------------------ */
/* myJob.php
/* ------------------------------------ */
// --- >> create new job
$("#dataForm_createNewJob").on("submit", function(e)
{		
	e.preventDefault();

	$("#dataBtn_createNewJob").prop("disabled", true);
	$("#dataBtn_createNewJob").html("please wait...");	

	$.ajax({
      url: "_server_requests.php", 
	  type: "POST",       
	  data: new FormData(this),
	  contentType: false,   
	  cache: false,         
	  processData:false, 
	  success: function(data)
	  {
		bootbox.alert(data);
			$("#dataBtn_createNewJob").prop("disabled", false);
			$("#dataBtn_createNewJob").html("Submit");	
	  }
	});	
});


// --- >> add or remove job responsibilities
var respCounter = 2;
$("#addResponsibility").on("click", function() {  
    $("#dataDiv-appendResponsibility").append('<div><br><input type="text" id="dataInput_job_responsibility'+respCounter+'" class="form-control" name="dataInput_job_responsibility[]" placeholder="enter responsibility" autocomplete="off" maxlength="150"></div>');  
	respCounter++;
});  

$("#removeResponsibility").on("click", function() {  
    $("#dataDiv-appendResponsibility").children().last().remove();  
}); 
/* --------------------------------------- */


/* ------------------------------------ */
/* adverts.php
/* ------------------------------------ */

// --- >> create new advert
	//$("#dataBtn_createNewAdvert").on("click", function ()
	$("#dataForm_createNewAdvert").on("submit", function(e)
	{	
		e.preventDefault();

		$("#dataBtn_createNewAdvert").prop("disabled", true);
		$("#dataBtn_createNewAdvert").html("please wait...");	

		$.ajax({
	      url: "_server_requests.php", 
		  type: "POST",       
		  data: new FormData(this),
		  contentType: false,   
		  cache: false,         
		  processData:false, 
		  success: function(data)
		  {
			bootbox.alert(data);
				$("#dataBtn_createNewAdvert").prop("disabled", false);
				$("#dataBtn_createNewAdvert").html("Submit");	
		  }
		});	
		
	});

	/* ------------------------------------ */
	/* datatables - server-side processing
	/* ------------------------------------ */

	// --- >> business list
	var constValue = 1; // return random number

	var myBusinessListDataTable = $('#myBusinessListDataTable').DataTable({
     "processing": false,
     "serverSide": true,
     "info": false,
     "autoWidth": false,
     "ajax":{
        url :"_server_requests.php",
        type: "post",
        data: {myBusinessListRandomValue:constValue},
        error: function(){
           $(".bl_gridClass").html("");
           $("#bl-grid").append('<tbody class="bl_gridClass"><tr><th colspan="6">No records found!</th></tr></tbody>');
           $("#dataTable-grid_processing").css("display","none");                     
        }
     }
  });


  // ---- refresh table after 3s
  setInterval( ()=> {
     myBusinessListDataTable.ajax.reload( null, false); 
     reloadProfileImage()
    // yourJobsListdataTable.ajax.reload( null, false); 
  }, 3000);

  
  // --- Chatbox
  $("#close-chatbox").on("click", function(e){
  		e.preventDefault();

  		// by default, we first hide the body of the chatbox; so we get its css property here
  		var toggleState = $('.chat-body-wrapper').css('display');

		// use the jquery slidetoggle animation effect fo sliding hide and show
		$('.chat-body-wrapper').slideToggle();

		// check the css property value; if its none (means the body is hidden) we change font icon
		// to angle-up else angle-down
		if(toggleState == 'none')
		{
			$("#chat-close-icon")
				.removeClass( 'fa-angle-up' )
				.addClass( 'fa-angle-down' );

			$("#close-chatbox").attr("title", "Close chatbox");
		}else{
			$("#chat-close-icon")
				.removeClass( 'fa-angle-down' )
				.addClass( 'fa-angle-up' );

			$("#close-chatbox").attr("title", "Open chatbox");
		}
  });

  // chat character count
  $("#chat_message").on("keyup", function(){

  		var maxNumberOfCharactersAllowed = 150;
  		var chatCharacters = $("#chat_message").val().length;
  		var remainingCharacters = maxNumberOfCharactersAllowed - chatCharacters;

  		$("#chatHelp").html(remainingCharacters+" Characters Remaining");

  		if (chatCharacters > maxNumberOfCharactersAllowed) {
  			$("#chatHelp")
	  			.removeClass( 'text-info' )
	  			.addClass( 'text-danger' );
  		} else {
  			$("#chatHelp")
  				.removeClass( 'text-danger' )
	  			.addClass( 'text-info' );
  		}
  });

});


/* 
----------------------- -----------------------
					function calls	
-----------------------  -----------------------
*/


// add job to favorites
function addJobtofavfun(jobToken)
{
	$.ajax({
	    type: 'POST',
	    url: '_server_requests.php',
	    data: "jobIdToAddToFavorites="+jobToken,
	    success: function (data) {
	        if ($.trim(data) == 3) 
	        {
	          $("#data-favorite"+jobToken).html("Remove from favorites");
	            _genNotify("Info: job added to favorites")
	        }
	        else if ($.trim(data) == 1) 
	        {
	          $("#data-favorite"+jobToken).html("Add to favorites");
	            _infoNotify("Info: job removed from favorites")
	        }
	        else if ($.trim(data) == 2) 
	        {
	           _erNotfify("Error: failed to remove job from favorites; please try again later");
	        }
	        else
	        {
	            _erNotfify("Error: failed to add job to favorites; please try again later");
	        }
	    }
	}); 

} // 

// loading function
function loadingfunc()
{
	return '<p class="text-center text-olive"><i class="fa fa-spinner fa-spin fa-2x"></i></p>';
}

/* --------------- */

// --- >> load selected advert details in modal ; this also captures view events
function loadAdvertDetailsfunction(advertId)
{
	$("#load-Advert-Details-Span").html(loadingfunc());

	$.post("_server_requests.php",{
		selectedadvertId:advertId
	}, function(data){
		$("#load-Advert-Details-Span").html(data);
	});
}

// --- >> like advert 1
function likethisAdvertfunction(advertCounter, loggedInEmail) 
{
	/*
	 advertCounter = advert post primary key
	 loggedInEmail = email of person liking the advert
	*/

	$.post("_server_requests.php", {
		advertPkeyCounter:advertCounter,
		likedAdvertLoggedInEmail:loggedInEmail
	}, function(data){
		if ($.trim(data) == 1) 
		{
			$("#advertLikeLink"+advertCounter).html('<i class="fa fa-thumbs-up text-info"></i>');
			$("#advertLikeLink"+advertCounter).attr("title", "Unlike");
			_genNotify("You have liked this advert!");
		}
		else if ($.trim(data) == 2) 
		{
			$("#advertLikeLink"+advertCounter).html('<i class="fa fa-thumbs-o-up text-info"></i>');
			$("#advertLikeLink"+advertCounter).attr("title", "Like");
			_genNotify("You have Unliked this advert!");
		}
		else{
			alert("Please try again later!")
		}
	});
}
// --- >> like advert 2
function likethisAdvertfunction2(advertCounter) 
{
	$.post("_server_requests.php", {
		profileSideCounter:advertCounter
	}, function(data){
		if ($.trim(data) == 1) 
		{
			//$("#advertLikeLink"+advertCounter).html('Unlike');
			_genNotify("You have liked this advert!");
		}
		else if ($.trim(data) == 2) 
		{
			//$("#advertLikeLink"+advertCounter).html('Like');
			_genNotify("You have Unliked this advert!");
		}
		else{
			alert("Please try again later!")
		}
	});
};

// add advert to favorites
function favoriteAdvertfunction(advertFavId)
{
    $.ajax({
        type: 'POST',
        url: '_server_requests.php',
        data: "advertIdToAddToFavorites="+advertFavId,
        success: function (data) {
            if ($.trim(data) == 3) 
            {
              $("#advertFavLink"+advertFavId).html('<i class="fa fa-heart text-maroon"></i>');
                _genNotify("Info: advert added to favorites")
            }
            else if ($.trim(data) == 1) 
            {
              $("#advertFavLink"+advertFavId).html('<i class="fa fa-heart-o text-maroon"></i>');
                _infoNotify("Info: advert removed from favorites")
            }
            else if ($.trim(data) == 2) 
            {
               _erNotfify("Error: failed to remove advert from favorites; please try again later");
            }
            else
            {
                _erNotfify("Error: failed to add advert to favorites; please try again later");
            }
        }
    }); 	
};

// pnotify success function
function _genNotify(msg) 
{
    // notify
    new PNotify({
      title: "Notice",
      type: "success",
      text: msg,
      nonblock: {
        nonblock: false
      },
      delay: 4500,
      buttons: {
        closer: true,
        closer_hover: true,
        show_on_nonblock: false
      },
      styling: 'bootstrap3'
    });
};

// pnotify info function
function _infoNotify(msg) 
{
    // notify
    new PNotify({
      title: "Notice",
      type: "info",
      text: msg,
      nonblock: {
        nonblock: false
      },
      delay: 4500,
      buttons: {
        closer: true,
        closer_hover: true,
        show_on_nonblock: false
      },
      styling: 'bootstrap3'
    });
};

// pnotify error function
function _erNotfify(errorMsg) 
{
    // notify
    new PNotify({
      title: "Error Notice",
      type: "error",
      text: errorMsg,
      nonblock: {
        nonblock: false
      },
      delay: 6000,
      buttons: {
        closer: true,
        closer_hover: true,
        show_on_nonblock: false
      },
      styling: 'bootstrap3'
    });
};


// --- >> more job details
function jobDetailsfunc(jobId)
{
	$("#load-Job-Details-Span").html(loadingfunc())

	$.post("_server_requests.php",{
		selectedJobId:jobId
	}, function(data){
		$("#load-Job-Details-Span").html(data);
	});

}

// --- >> delete your job
function deletejobFunction(sessionJobDeleteArgs)
{
		// is the advert currently in view
	var currentviewValue = $("#deleteJobSessionInputEdit"+sessionJobDeleteArgs).val()

	if (currentviewValue == sessionJobDeleteArgs) 
	{
		_erNotfify("Error: Can not delete job when currently being edited; please close and try again");
	}
	else{

		if (confirm("Delete this job?") == true)
		{
			$.ajax({
				url: "_server_requests.php",
				method: "post",
				data: {sessionJobDeleteArgs:sessionJobDeleteArgs},
				success: function(data)
				{
					//$("#jobsSpanFeedback").html(data)
					 _genNotify(data)
				}
			});
		}
	}
}

// edit job
function jobEditfunc(sessionJobEditArgs)
{

	$("#job-details-editing-span").html("<img src='_assets/images/serverLoader.gif' class='center-block img-responsive' alt='Loading..'>")

	$.ajax({
		url: "_server_requests.php",
		method: "post",
		data: {sessionEditJobArgs:sessionJobEditArgs},
		success: function(data){
			//$("#load-job-Details-for-editing-Span").html(data)
			$("#primary-view-profile").hide() // hide profile view
			$("#secondary-View-editing-advert").hide() // hide advert editing view
			$("#secondary-View-editing-job").fadeIn() // show job editing view
			$("#job-details-editing-span").html(data)//.fadeIn() // show editing data
			//$(".view-editing-toggle").fadeToggle()
		}
	});
}


// edit advert
function advertEditfunc(sessionAdvertEditArgs)
{
	$("#advert-details-editing-span").html("<img src='_assets/images/serverLoader.gif' class='center-block img-responsive' alt='Loading..'>")

	$.ajax({
		url: "_server_requests.php",
		method: "post",
		data: {sessionAdvertEditArgs:sessionAdvertEditArgs},
		success: function(data){
			$("#primary-view-profile").hide() // hide profile view
			$("#secondary-View-editing-job").hide() // hide job editing view
			$("#secondary-View-editing-advert").fadeIn() // show advert editing view
			$("#advert-details-editing-span").html(data)//.fadeIn() // show editing data
		}
	});
}
// --- >> delete your advert
function deleteAdvertFunction(sessionAdvertDeleteArgs)
{
	// is the advert currently in view
	var currentViewValue = $("#deleteSessionInputEdit"+sessionAdvertDeleteArgs).val()

	if (currentViewValue == sessionAdvertDeleteArgs) 
	{
		_erNotfify("Error: Can not delete advert when currently being edited; please close and try again");
	}
	else{

		if (confirm("Delete this job?") == true)
		{
			$.ajax({
				url: "_server_requests.php",
				method: "post",
				data: {sessionAdvertDeleteArgs:sessionAdvertDeleteArgs},
				success: function(data)
				{
					_genNotify(data)
				}
			});
		}

	}
}



// --- >> delete business
function deletebusinessFunction(businessProfilePkey)
{
	if (confirm("Delete this business?") == true)
	{
		$.ajax({
			url: "_server_requests.php",
			method: "post",
			dataType: "json",
			data: {businessProfilePkey:businessProfilePkey},
			success: function(data)
			{
				alert(data)
			}
		});
	}
}

// --- >> show clicked messages to preview
//showMessageBody
function showMessageBody(messageId, messageStatus)
{

	$.ajax({
		url: "_server_requests.php",
		method: "post",
		//dataType: "json",
		cache: false,
		data: {messageId:messageId,messageStatus:messageStatus},
		success: function(data){
			$("#load_message_body_div").html(data);
			if (messageStatus == "Received") {
				$("#update_i_top"+messageId).html('<i class="fa fa-circle-o text-primary"></i>')
			}
		}
	});
}

function mailStatusfunc(mailStatus, mailEmail, mailDeleted)
{

	if (mailStatus == 'Received') 
	{
		$("#more_details_to_from_email").html("<b>To:</b> " + mailEmail);
		$("#show_from").html('<b class="text-olive">From: </b>');

		  mailDeleted == 'Deleted' ? $("#show_mail_deleted").html(" - (Inbox)") : '';
	}
	else if (mailStatus == 'Sent') 
	{
		$("#more_details_to_from_email").html("<b>From:</b> " + mailEmail);
		$("#show_from").html('<b class="text-olive">To: </b>');
		  
		  mailDeleted == 'Deleted' ? $("#show_mail_deleted").html(" - (Sent)") : '';
	}	

}

// --- >> delete message
function deleteMessage(delMessageStatus, delMessageId)
{
	//var delMessageId 	 = $("#showing_message_id").val();
	//var delMessageStatus = $("#showing_message_status").val();

	//if (delMessageStatus == 'Deleted' ? confirm("Are you sure you want to delete this message forever?") == true : confirm("Are you sure you want to move this message to trash?") == true) 
	//{
		$.post("_server_requests.php",{
			delMessageId:delMessageId,
			delMessageStatus:delMessageStatus
		}, function(data){	
			
			if (data == 1) {
				_genNotify("Message has been Permanently Deleted!")
					$("#load_message_body_div").html('<h3 class="text-maroon text-center" id="no_preview_p">Select any message on your left to view</h3>').fadeIn();
					$("#bin_message_link"+delMessageId).animate({backgroundColor: "#34404b"}, "slow").animate({opacity: "hide"}, "slow");
			} 
			else if (data == 2) {
				_genNotify("Message has been moved to Trash!")
					$("#load_message_body_div").html('<h3 class="text-maroon text-center" id="no_preview_p">Select any message on your left to view</h3>').fadeIn();
					
					$("#sent_message_link"+delMessageId).animate({backgroundColor: "#34404b"}, "slow").animate({opacity: "hide"}, "slow");
			}
			else if (data == 5) {
				_genNotify("Message has been moved to Trash!")
					$("#load_message_body_div").html('<h3 class="text-maroon text-center" id="no_preview_p">Select any message on your left to view</h3>').fadeIn();
					
					$("#inbox_message_link"+delMessageId).animate({backgroundColor: "#34404b"}, "slow").animate({opacity: "hide"}, "slow");
			}
			else if (data == 4) {
				_erNotfify("Failed to move message to trash!")
			}
			else if (data == 3) {
				_erNotfify("Failed to Permanently delete Message!")
			}
			else {
				_erNotfify("Unknown error occured!")
			}
		
		});
	//}	
};

// load mail details chevron
function loadMailDetails() {
	$("#mail-brief-details").slideToggle();
}