/**
 * Resize function without multiple trigger
 * 
 * Usage:
 * $(window).smartresize(function(){  
 *     // code here
 * });
 */
(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
      var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args); 
                timeout = null; 
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100); 
        };
    };

    // smartresize 
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href;//.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');

	
	
// Sidebar
function init_sidebar() {
// TODO: This is some kind of easy fix, maybe we can improve this
var setContentHeight = function () {
	// reset height
	$RIGHT_COL.css('min-height', $(window).height());

	var bodyHeight = $BODY.outerHeight(),
		footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
		leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
		contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

	// normalize content
	contentHeight -= $NAV_MENU.height() + footerHeight;

	$RIGHT_COL.css('min-height', contentHeight);

	//$(".page_content").css("padding-top", "50px");

	$(".top_nav").css("z-index", "20");
};

  $SIDEBAR_MENU.find('a').on('click', function(ev) {
	  console.log('clicked - sidebar_menu');
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }else
            {
				if ( $BODY.is( ".nav-sm" ) )
				{
					$SIDEBAR_MENU.find( "li" ).removeClass( "active active-sm" );
					$SIDEBAR_MENU.find( "li ul" ).slideUp();
				}
			}
            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

// toggle small or large menu 
$MENU_TOGGLE.on('click', function() {
		console.log('clicked - menu toggle');		
		
		if ($BODY.hasClass('nav-md')) {
			$SIDEBAR_MENU.find('li.active ul').hide();
			$SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
		} else {
			$SIDEBAR_MENU.find('li.active-sm ul').show();
			$SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
		}

	$BODY.toggleClass('nav-md nav-sm');

	setContentHeight();

	$('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
});

	// check active menu
	$SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

	$SIDEBAR_MENU.find('a').filter(function () {
		return this.href == CURRENT_URL;
	}).parent('li').addClass('current-page').parents('ul').slideDown(function() {
		setContentHeight();
	}).parent().addClass('active');

	// recompute content when resizing
	$(window).smartresize(function(){  
		setContentHeight();
	});

	setContentHeight();

	// fixed sidebar
	if ($.fn.mCustomScrollbar) {
		$('.menu_fixed').mCustomScrollbar({
			autoHideScrollbar: true,
			theme: 'minimal',
			mouseWheel:{ preventDefault: true }
		});
	}
};
// /Sidebar

	var randNum = function() {
	  return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
	};


// Panel toolbox
$(document).ready(function() {
    $('.collapse-link').on('click', function() {
        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');
        
        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200); 
            $BOX_PANEL.css('height', 'auto');  
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');

        $BOX_PANEL.remove();
    });
});
// /Panel toolbox

// Tooltip
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
});
// /Tooltip

// Progressbar
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar();
}
// /Progressbar



// Accordion
$(document).ready(function() {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

// NProgress
if (typeof NProgress != 'undefined') {
    $(document).ready(function () {
        NProgress.start();
    });

    $(window).load(function () {
        NProgress.done();
    });
}

	
	  //hover and retain popover when on popover content
        var originalLeave = $.fn.popover.Constructor.prototype.leave;
        $.fn.popover.Constructor.prototype.leave = function(obj) {
          var self = obj instanceof this.constructor ?
            obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
          var container, timeout;

          originalLeave.call(this, obj);

          if (obj.currentTarget) {
            container = $(obj.currentTarget).siblings('.popover');
            timeout = self.timeout;
            container.one('mouseenter', function() {
              //We entered the actual popover – call off the dogs
              clearTimeout(timeout);
              //Let's monitor popover content instead
              container.one('mouseleave', function() {
                $.fn.popover.Constructor.prototype.leave.call(self, self);
              });
            });
          }
        };

        $('body').popover({
          selector: '[data-popover]',
          trigger: 'click hover',
          delay: {
            show: 50,
            hide: 400
          }
        });


	function gd(year, month, day) {
		return new Date(year, month - 1, day).getTime();
	}
	  
	   

	 /* AUTOSIZE */
			
		function init_autosize() {
			
			if(typeof $.fn.autosize !== 'undefined'){
			
			autosize($('.resizable_textarea'));
			
			}
			
		};  
	   
	   /* PARSLEY */
			
		function init_parsley() {
			
			if( typeof (parsley) === 'undefined'){ return; }
			console.log('init_parsley');
			
		};
	   
		

	   
		/* SELECT2 */
	  
		function init_select2() {
			 
			if( typeof (select2) === 'undefined'){ return; }
			console.log('init_toolbox');
			 
			$(".select2_single").select2({
			  placeholder: "Select a state",
			  allowClear: true
			});
			$(".select2_group").select2({});
			$(".select2_multiple").select2({
			  maximumSelectionLength: 4,
			  placeholder: "With Max Selection limit 4",
			  allowClear: true
			});
			
		};



			// Tooltip
			$('[data-toggle="tooltip"]').tooltip();




	  

	   /* SMART WIZARD */
		
		function init_SmartWizard() {
			
			if( typeof ($.fn.smartWizard) === 'undefined'){ return; }
			console.log('init_SmartWizard');
			
			$('#wizard').smartWizard({
				transitionEffect:'slideleft',
				onLeaveStep:leaveAStepCallback,
				onFinish:onFinishCallback,
				enableFinishButton:false,
				hideButtonsOnDisabled: true,
				labelFinish:'Submit',
			});

			$('.buttonNext').addClass('btn btn-sm btn-flat bg-green pagerButtons');
			$('.buttonPrevious').addClass('btn btn-sm btn-flat bg-blue pagerButtons');
			$('.buttonFinish').addClass('btn btn-sm btn-flat bg-purple pagerButtons srfinishBtn');
			
		};
		 function leaveAStepCallback(obj){
	        var step_num= obj.attr('rel');
	        return validateSteps(step_num);
	      }
	      
	      function onFinishCallback(){
	       if(validateAllSteps()){
	        //$('form').submit();
	        $(".srfinishBtn").prop("disabled", true);
	        $(".srfinishBtn").html("please wait...");

		        $.ajax({
		        	url: "_server_requests.php",
		        	method: "post",
		        	data: $("#dataForm_ServiceQuotation").serialize(),
		        	success: function(data){
		        		bootbox.alert(data);
				        $(".srfinishBtn").prop("disabled", false);
				        $(".srfinishBtn").html("Submit");
		        	}
		        });
	       	}
	      }
	   
	  /* VALIDATOR */

	    function validateAllSteps(){
	       var isStepValid = true;
	       
	       if(validateStep1() == false){
	         isStepValid = false;
	         $('#wizard').smartWizard('setError',{stepnum:1,iserror:true});         
	       }else{
	         $('#wizard').smartWizard('setError',{stepnum:1,iserror:false});
	       }
	       
	       if(validateStep2() == false){
	         isStepValid = false;
	         $('#wizard').smartWizard('setError',{stepnum:2,iserror:true});         
	       }else{
	         $('#wizard').smartWizard('setError',{stepnum:2,iserror:false});
	       }
	       
	       if(validateStep3() == false){
	         isStepValid = false;
	         $('#wizard').smartWizard('setError',{stepnum:3,iserror:true});         
	       }else{
	         $('#wizard').smartWizard('setError',{stepnum:3,iserror:false});
	       }
	       
	       if(!isStepValid){
	          //$('#wizard').smartWizard('showMessage','Please correct the errors in the steps and continue');
	          console.log('Please correct the errors in the steps and continue');
	       }
	              
	       return isStepValid;
	    } 	


		function validateSteps(step){
			  var isStepValid = true;
	      // validate step 1
	      if(step == 1){
	        if(validateStep1() == false ){
	          isStepValid = false; 
	          //$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
	          console.log('Please correct the errors in step'+step+ ' and click next.');
	          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
	        }else{
	          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
	        }
	      }
	      
	      // validate step2
	      if(step == 2){
	        if(validateStep2() == false ){
	          isStepValid = false; 
	          //$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
	          console.log('Please correct the errors in step'+step+ ' and click next.');
	          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
	        }else{
	          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
	        }
	      }
	      
	      // validate step3
	      if(step == 3){
	        if(validateStep3() == false )
	        {
	          isStepValid = false; 
	          //$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
	          console.log('Please correct the errors in step'+step+ ' and click next.');
	          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
	        }
	        else
	        {
	          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
	        }
	      }
	      
	      return isStepValid;
	    }

		// --- >> steps validation << --- //

		 // --- >> step 1
		function validateStep1(){
			var isValid = true; 
			// Validate current location
			var loc = $('#location').val();
			if(!loc && loc.length <= 0)
			{
			 isValid = false;
			 $('#msg_current_location').html('Error: enter your location').show();
			}
			else
			{
			 $('#msg_current_location').html('').hide();
			}

			// validate delivery address
			var deloc = $('#address').val();
			if(!deloc && deloc.length <= 0)
			{
			 isValid = false;
			 $('#msg_delivery_location').html('Error: enter where you want service delivered!').show();         
			}
			else
			{
			 $('#msg_delivery_location').html('').hide();
			}
			return isValid;
		}

		// --- >> step 2
		function validateStep2(){
			var isValid = true; 
			// Validate service required
			var sreq = $('#dataForm_servicerequired').val();
			if(!sreq && sreq.length <= 0)
			{
			 isValid = false;
			 $('#msg_service_required').html('Error: enter service required').show();
			}
			else
			{
			 $('#msg_service_required').html('').hide();
			}

			var srdate = $('#dataInput_sr_date_of_servicedelivery').val();
			if(!srdate && srdate.length <= 0)
			{
			 isValid = false;
			 $('#msg_date_delivery_required').html('Error: date of delivery is required').show();
			}
			else
			{
			 $('#msg_date_delivery_required').html('').hide();
			}

			var srtime = $('#dataInput_sr_time_of_servicedelivery').val();
			if(!srtime && srtime.length <= 0)
			{
			 isValid = false;
			 $('#msg_time_delivery_required').html('Error: time of delivery is required').show();
			}
			else
			{
			 $('#msg_time_delivery_required').html('').hide();
			}

			return isValid;
		}

	// --- >> step 3
	function validateStep3(){
		var isValid = true; 
		// Validate quotation
		var squot = $('#quotation_details').val();
		var tcchk = document.getElementById('agreeTermsConditionsfs');

		if(!squot && squot.length <= 0)
		{
		 	isValid = false;
		 	$('#msg_quotation').html('Error: service quotation field is required').show();
		}
		else if (squot.length > 200)
		{
			isValid = false;
			$('#msg_quotation').html('Error: service quotation should not exceed 200 characters!').show();
		}
		else if(tcchk.checked == false)
		{
			isValid = false;
			$('#msg_tc_check').html('Error: please agree to our terms and conditions!').show();
		}
		else
		{
			$('#msg_quotation').html('').hide();
		}
		return isValid;
	}


	   
		/* DATA TABLES */
			
			function init_DataTables() {
				
				console.log('run_datatables');
				
				if( typeof ($.fn.DataTable) === 'undefined'){ return; }
				console.log('init_DataTables');
				
				var handleDataTableButtons = function() {
				  if ($("#datatable-buttons").length) {
					$("#datatable-buttons").DataTable({
					  dom: "Bfrtip",
					  buttons: [
						{
						  extend: "copy",
						  className: "btn-sm"
						},
						{
						  extend: "csv",
						  className: "btn-sm"
						},
						{
						  extend: "excel",
						  className: "btn-sm"
						},
						{
						  extend: "pdfHtml5",
						  className: "btn-sm"
						},
						{
						  extend: "print",
						  className: "btn-sm"
						},
					  ],
					  responsive: true
					});
				  }
				};

				TableManageButtons = function() {
				  "use strict";
				  return {
					init: function() {
					  handleDataTableButtons();
					}
				  };
				}();

				$(".dataTableGeneral").DataTable({
				  "paging": true,
				  "lengthChange": true,
				  "searching": true,
				  "ordering": true,
				  "info": false,
				  "autoWidth": false
				});

				

				$('#datatable-keytable').DataTable({
				  keys: true
				});

				$('#datatable-responsive').DataTable();

				$('#datatable-scroller').DataTable({
				  ajax: "js/datatables/json/scroller-demo.json",
				  deferRender: true,
				  scrollY: 380,
				  scrollCollapse: true,
				  scroller: true
				});

				$('#datatable-fixed-header').DataTable({
				  fixedHeader: true
				});

				var $datatable = $('#datatable-checkbox');

				$datatable.dataTable({
				  'order': [[ 1, 'asc' ]],
				  'columnDefs': [
					{ orderable: false, targets: [0] }
				  ]
				});
				$datatable.on('draw.dt', function() {
				  $('checkbox input').iCheck({
					checkboxClass: 'icheckbox_flat-green'
				  });
				});

				TableManageButtons.init();
				
			};
	   

		
	
	   
	/* --- >> datepicker init << --- */
	function init_datepicker()
	{
		$('.datepicker').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight:true
		});
	}

	   /* DATERANGEPICKER */
	   
		function init_daterangepicker() {

			if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
			console.log('init_daterangepicker');
		
			var cb = function(start, end, label) {
			  console.log(start.toISOString(), end.toISOString(), label);
			  $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			};

			var optionSet1 = {
			  startDate: moment().subtract(29, 'days'),
			  endDate: moment(),
			  minDate: '01/01/2012',
			  maxDate: '12/31/2015',
			  dateLimit: {
				days: 60
			  },
			  showDropdowns: true,
			  showWeekNumbers: true,
			  timePicker: false,
			  timePickerIncrement: 1,
			  timePicker12Hour: true,
			  ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			  },
			  opens: 'left',
			  buttonClasses: ['btn btn-default'],
			  applyClass: 'btn-small btn-primary',
			  cancelClass: 'btn-small',
			  format: 'MM/DD/YYYY',
			  separator: ' to ',
			  locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Clear',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			  }
			};
			
			$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
			$('#reportrange').daterangepicker(optionSet1, cb);
			$('#reportrange').on('show.daterangepicker', function() {
			  console.log("show event fired");
			});
			$('#reportrange').on('hide.daterangepicker', function() {
			  console.log("hide event fired");
			});
			$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			  console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
			});
			$('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
			  console.log("cancel event fired");
			});
			$('#options1').click(function() {
			  $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
			});
			$('#options2').click(function() {
			  $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
			});
			$('#destroy').click(function() {
			  $('#reportrange').data('daterangepicker').remove();
			});
   
		}
	
		function init_daterangepicker_period() {
	      
			if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
		 
			/*$('.daterange_picker_one').daterangepicker(null, function(start, end, label) {
			  console.log(start.toISOString(), end.toISOString(), label);
			});
			*/
			$('.daterange_picker_one').daterangepicker({

			  locale: {
				format: 'YYYY-MM-DD'
			  }
			  
			});

			$('#reservation-time').daterangepicker({
			  timePicker: true,
			  timePickerIncrement: 30,
			  locale: {
				format: 'YYYY-MM-DD h:mm A'
			  }
			});
	
		};

		/* PNotify */			
		function init_PNotify() {
			
			if( typeof (PNotify) === 'undefined'){ return; }
			console.log('init_PNotify');

			/**			
			new PNotify({
			  title: "PNotify",
			  type: "info",
			  text: "Welcome. Try hovering over me. You can click things behind me, because I'm non-blocking.",
			  nonblock: {
				  nonblock: true
			  },
			  addclass: 'dark',
			  styling: 'bootstrap3',
			  hide: false,
			  before_close: function(PNotify) {
				PNotify.update({
				  title: PNotify.options.title + " - Enjoy your Stay",
				  before_close: null
				});

				PNotify.queueRemove();

				return false;
			  }
			});
			*/

		}; 
		
		// scroll to top
		function init_scrollTotop() 
		{
		  $().UItoTop({
		  easingType: 'easeOutQuart' }
		  );
		};

		// bootstrap maxlength
		function init_maxlength() {
			$(".maxlength-init").maxlength();
		};

		// grant push notification permission
		function pushNotifications() {
			if (!Notification) {
				$('body').append('<h4 style="color:red">*Browser does not support Web Notification</h4>');
				return;
			}
			if (Notification.permission !== "granted")
				Notification.requestPermission();
			else {
				$.ajax(
				{
					url : "_server_requests.php",
					type: "POST",
					success: function(data, textStatus, jqXHR)
					{
						/*
						var data = jQuery.parseJSON(data);
						if(data.result == true){
							var data_notif = data.notif;
							
							for (var i = data_notif.length - 1; i >= 0; i--) {
								var theurl = data_notif[i]['url'];
								var notifikasi = new Notification(data_notif[i]['title'], {
									icon: data_notif[i]['icon'],
									body: data_notif[i]['msg'],
								});
								notifikasi.onclick = function () {
									window.open(theurl); 
									notifikasi.close();     
								};
								setTimeout(function(){
									notifikasi.close();
								}, 5000);
							};
						}else{

						}
						*/
					},
					error: function(jqXHR, textStatus, errorThrown)
					{

					}
				});	

			}
		};
	   

	$(document).ready(function() {

		init_sidebar(); // side bar configuration
		init_SmartWizard(); // step form field
		init_daterangepicker(); // date range with time picker
		init_daterangepicker_period(); // date range picker
		init_select2(); // select2 plugin
		init_autosize(); // auto size window
		init_datepicker(); // normal date picker
		init_DataTables(); // date tables
		init_PNotify(); // toast-like notification
		init_scrollTotop(); // scroll to top
		init_parsley(); // parsley validate
		init_maxlength(); // bootstrap maxlength plugin
		pushNotifications(); // permission to allow notification				
	});	
	

