/* This showResult function is used as the callback function*/
function showResult(result) {
    document.getElementById('latitude').value = result.geometry.location.lat();
    document.getElementById('longitude').value = result.geometry.location.lng();
}

function getLatitudeLongitude(callback, address) {   
geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }
}

var button = document.getElementById('submit');
    button.addEventListener("click", function () { 
    var address = document.getElementById('location').value;
    getLatitudeLongitude(showResult, address)
});


     if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(position){
            //    var positionInfo = "Your current position is (" + "Latitude: " + position.coords.latitude + ", " + "Longitude: " + position.coords.longitude + ")";
             //   document.getElementById("result").innerHTML = positionInfo;
                var crnt =document.getElementById("latlng").value = position.coords.latitude + ", " + position.coords.longitude;
                document.getElementById("longitude").value = position.coords.longitude;
                document.getElementById("latitude").value = position.coords.latitude;
           });
        } else{
            alert("Sorry, your browser does not support HTML5 geolocation.");
        }
      //===========location of auto complete serarch =============
      function initMap() { 
        var map = new google.maps.Map(document.getElementById('map'), {
            //center: {lat: position.coords.latitude, lng: position.coords.longitude}
              center: {lat: 0.3733, lng: 32.5825},
          zoom: 13
        });
          var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;

    //====================on ready also get my location============    
         $( document ).ready(function() { 
             geocodeLatLngReady(geocoder, map, infowindow);
        });
    //====================on click get my location============    
       document.getElementById('submit').addEventListener('click', function() {
          geocodeLatLng(geocoder, map, infowindow); 
        });
    
//====================end============    
     
        var card = document.getElementById('pac-card');
        var input = document.getElementById('address');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });
        
             
        autocomplete.addListener('place_changed', function() {
            //&&&&&&&&&&&&&&&&&&s show long and lat for the new input address in click place &&&&&&&&&&&&&&&&&&&&&&&&&&&
            var address = document.getElementById('address').value;
        getLatitudeLongitude(showResult, address) 
        
            //&&&&&&&&&&&&&&&&&&  end  &&&&&&&&&&&&&&&&&&&&&&&&&&
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(7);  // Why 17? Because it looks good.
            }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
         } 
          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);               
        });

         
} 
 
 
      //================================ on ready page, input my location in text field============================
      function geocodeLatLngReady(geocoder, map, infowindow) {
       navigator.geolocation.getCurrentPosition(function(position){
            //    var positionInfo = "Your current position is (" + "Latitude: " + position.coords.latitude + ", " + "Longitude: " + position.coords.longitude + ")";
             //   document.getElementById("result").innerHTML = positionInfo;
                var crnt =document.getElementById("latlng").value = position.coords.latitude + ", " + position.coords.longitude;
                document.getElementById("longitude").value = position.coords.longitude;
                document.getElementById("latitude").value = position.coords.latitude;
            
           var input = crnt;
        //   var input = document.getElementById('latlng').value;
        var latlngStr = input.split(',', 2);
         var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
       
          
        
       geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
              map.setZoom(14);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
            var adress_name =results[0].formatted_address;  
            document.getElementById('location').value = adress_name;     
        } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
          
           });
           });
      }
      //=============================================end============================
      function geocodeLatLng(geocoder, map, infowindow) {
        var input = document.getElementById('latlng').value;
        var latlngStr = input.split(',', 2);
         var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
        
         
       geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
                
              map.setZoom(14);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
            var adress_name =results[0].formatted_address;  
            document.getElementById('address').value = adress_name;     
         
        } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
          
           });
      } 