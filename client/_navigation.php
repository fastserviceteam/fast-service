<?php

  /* --------------------------------- */
  /* page navigation
  /* --------------------------------- */
  if (isset($_GET['pageNavigation'])) 
  {

    $pageNavigationVariable = $_GET['pageNavigation'];
    
    switch ($pageNavigationVariable) 
    {
      case 'createNewBusiness':
        include "_pages/myBusiness.php";
        break;
        
      case 'myBusinessList':
        include "_pages/businessList.php";
        break;

      case 'createNewJob':
        include "_pages/myJob.php";
        break;

      case 'myJobList':
         include "_pages/jobList.php";
         break;

      case 'myAdvert':
         include "_pages/myAdverts.php";
         break;

      case 'myAdvertList':
         include "_pages/advertsList.php";
         break;

      case 'myProfile':
         include "_pages/myProfile.php";
         break;

      case 'myProfileUpdate':
         include "_pages/myProfileUpdate.php";
         break;
      
      default:
        //header("Locaton: dashboard.php?logout");


    } // endof switch

  } // --- >> isset << --- get
  else
  {
  //  header("Locaton: dashboard.php");
    ?>
<script>
  setTimeout(' window.location.href = "dashboard.php"; ', 10);
</script>
    <?php

}