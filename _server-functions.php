<?php	
	try { 
		$dbh = new PDO("mysql:host=".DB_HOST."; dbname=".DB_DATABASE, DB_USER, DB_PASSWORD, array(PDO::ATTR_PERSISTENT => true)); 
	} 
	catch(PDOException $e)
	{ 
		echo $e->getMessage(); 
	}

	function GetAll ($tb='',$whr=''){
		global $dbh;
		$sql ="SELECT * FROM $tb $whr ";		
		if (empty($tb)) return FALSE;
		$r = $dbh->prepare($sql);
		$r->execute();
		$results =array();
		while($row = $r->fetch(PDO::FETCH_OBJ)){
			$results[] = $row;
		}
		return $results;
	}
	function GetRow($query=''){
		global $dbh;
		$r = $dbh->prepare($query);
		$r->execute();
		return $r->fetch(PDO::FETCH_OBJ);	
	}
	
	function count_services($tb='',$whr=''){
		global $dbh;
		$fd=("SELECT COUNT(business_profile_id) AS avail FROM $tb $whr");
		if (empty($tb)) return FALSE;
		$r = $dbh->prepare($fd);
		$r->execute();
		$results =array();
		while($row = $r->fetch(PDO::FETCH_OBJ)){
			$results[] = $row;
		}
		return $results;
	}
	
	function getLastseen($datetime){
		$now =date("Y-m-d h:i:s");
		$ts =   (strtotime($now) - strtotime($datetime));
		if ($ts < 10){
			return 'Now &nbsp;';
		}
		$a = array( 365 * 24 * 60 * 60  =>  'yr ago',
		30 * 24 * 60 * 60  =>  'month ago',
		24 * 60 * 60  =>  'day ago',
		60 * 60  =>  'hr ago',
		60  =>  'min ago',
		1  =>  'sec ago'
		);
		$a_plural = array( 'yr'   => 'yrs ago',
		'month'  => 'months ago',
		'day'    => 'days ago',
		'hr'   => 'hrs ago',
		'min' => 'min ago',
		'sec' => 'sec ago'
		);
		foreach ($a as $secs => $str){
			$d = $ts / $secs;
			if ($d >= 1){
				$r = round($d);
				return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) ;
			}
		}
	}	
	