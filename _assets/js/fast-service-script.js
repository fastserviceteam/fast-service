/* --------------------------------- */
/* re-arranged on:- 10 April, '19
/* --------------------------------- */


//$(function(){
(function($) {

    "use strict"; // Start of use strict     

    //PNotify.defaults.styling = 'bootstrap4';

    /* ------------------------------- */
    /* --- >> Misc
    /* ------------------------------- */

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 70)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 100
    });

    // Collapse Navbar
    var navbarCollapse = function() {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);

    // --- >> agree to terms for submitting

    $("#AgreeTerms").on("click", function() {
        if ($(this).is(":checked")) {
            $("#registrationSubmitBtn").removeAttr("disabled");
        } else {
            $("#registrationSubmitBtn").prop("disabled", true);
        }
    });

    // --- >> switch between login and register
    $(function() {
        $('[data-login-register-form]').click(function() {
            var $this = $(this)
            $this.html() == 'Register' ? $this.html('Login') : $this.html('Register');
            $(".access-forms-toggle").toggle()
            //$("#fast-service-login-fieldset").show();
            //$("#fast-service-signup-fieldset").hide();

        });
    });
       

    /* ------------------------------- */
    /* --- >> register account
    /* ------------------------------- */
    $("#dataForm-account-registration-client-form").on("submit", function(e) {
        e.preventDefault();

        var fullNames = $("#input_fullNames").val();
        var phoneNumber = $("#input_phoneNumber").val();
        var valid_emailAddress = $("#input_valid_emailAddress").val();
        var strong_password = $("#input_strong_password").val();
        var confirm_strong_password = $("#input_confirm_strong_password").val();

        $("#registrationSubmitBtn").html('<span class="spinner-grow spinner-grow-lg"></span> Please wait');
        $("#registrationSubmitBtn").prop("disabled", true);

        if (fullNames == '' || phoneNumber == '' || valid_emailAddress == '' || strong_password == '' || confirm_strong_password == '') {
            bootbox.alert("<p class='text-center text-info'>Error: please check for empty fields!</p>");

            $("#registrationSubmitBtn").html("Register");
            $("#registrationSubmitBtn").prop("disabled", false);

            //toggleHtmlDisplay();
        } else {

            $.ajax({
                url: "_method-file.php",
                type: "post",
                contentType: false,
                cache: false,
                processData: false,
                data: new FormData(this),
                success: function(data) {
                    bootbox.alert(data);

                    $("#registrationSubmitBtn").html("Register");
                    $("#registrationSubmitBtn").prop("disabled", false);
                }
            });
        }

    });


    /* ------------------------------- */
    /* --- >> login to account
    /* ------------------------------- */
    $("#dataForm-account-login-client-form").on("submit", function(e) {
        e.preventDefault();

        var dataLoginEmailAddress = $("#input_login_email_address").val();
        var dataLoginPassword     = $("#input_login_password").val();

        $("#submit-login").html('<span class="spinner-grow spinner-grow-lg"></span>');
        $("#submit-login").prop("disabled", true);

        $.ajax({
            url: "_method-file.php",
            type: "post",
            contentType: false,
            cache: false,
            processData: false,
            data: new FormData(this),
            success: function(data) {

                //bootbox.alert(data);
                $(".dataAlert-login").html(data);

                $("#submit-login").html("Login");
                $("#submit-login").prop("disabled", false);
            }
        });
    });


    /* ------------------------------- */
    /* --- >> reset password
    /* ------------------------------- */
    $("#dataForm-account-passwordReset-client-form").on("submit", function(e) {
        e.preventDefault();

        $("#submit-NewPasword-btnId").html('<span class="spinner-grow spinner-grow-lg"></span> Please wait');
        $("#submit-NewPasword-btnId").prop("disabled", true);

        $.ajax({
            url: "_method-file.php",
            type: "post",
            contentType: false,
            cache: false,
            processData: false,
            data: new FormData(this),
            success: function(data) {

                //bootbox.alert(data);
                $(".dataAlert-pwdReset").html(data);

                $("#submit-NewPasword-btnId").html("Reset");
                $("#submit-NewPasword-btnId").prop("disabled", false);
            }
        });
    });

    /* ------------------------------------------------ */
    /* --- >> validate login in input using validate js
    /* ------------------------------------------------ */
    /*
     $("#loginform").validate({
      rules: {
            input_login_password: {
            required: true,
            },
            input_login_email_address: {
            required: true
            },
       },
       messages:  {
            input_login_password:{
                required: "Please enter your password"
                },
                input_login_email_address: "Please enter your email address",
       },
       submitHandler: submitForm    
       });
       */




    /* ------------------------------- */
    /* --- >> send email
    /* ------------------------------- */
    $("#dataFastService-SendEmail").on("submit", function(e) {
        e.preventDefault();

        $("#dataIdButton_sendMessage").html("<span class='spinner-grow spinner-grow-lg'></span> Sending...");
        $("#dataIdButton_sendMessage").prop("disabled", true);

        var fast_service_sender_names  = $("#dataIdText_client_name").val();
        var fast_service_email         = $("#dataIdText_client_emailAddress").val();
        var fast_service_subject       = $("#dataIdText_client_subject").val();
        var fast_service_sent_text     = $("#dataIdTextArea_client_message").val();

        if (fast_service_sender_names == '' || fast_service_email == '' || fast_service_subject == '' || fast_service_sent_text == '') {
            $('#result_msg').html("<p class='text-center text-danger toggle-display'>Error: please check for empty fields!</p>");
            $("#dataIdButton_sendMessage").html("Send");
            $("#dataIdButton_sendMessage").prop("disabled", false);
        } else {
            $.ajax({
                url: "_method-file.php",
                method: "post",
                contentType: false,
                cache: false,
                processData: false,
                data: new FormData(this),
                success: function(data) {
                    bootbox.alert(data);
                    $("#dataIdButton_sendMessage").html("Send");
                    $("#dataIdButton_sendMessage").prop("disabled", false);

                }

            });
        }

    });


    /* ----------------------- */
    /* notification
    /* ----------------------- */
    /*
    function pNoticeMsg(msg) {
        $.notify(
          msg, 
          "info",
          { 
            elementPosition: "bottom",
            globalPosition: "bottom right",
          }
        );
    };
    */

    /* --- init wow --- */
     new WOW().init();


})(jQuery); // End of use strict



/* ----------------------- */
/* login in
/* ----------------------- */
function submitForm() {
    $("#submit-login").html('<i class="fa fa-spinner fa-spin"></i> please wait...');

    var r_data = $("#loginform").serialize();

    $.ajax({
        type: 'POST',
        url: '_method-file.php',
        data: r_data,
        success: function(response) {
            $("#submit-login").html('Login');
            $("#loginFeedback").html(response);
        }
    });
    return false;
}


/* ----------------------- */
/* toggle message feedback
/* ----------------------- */
function toggleHtmlDisplay() {
    setTimeout(() => {
        $(".toggle-display").fadeOut();
    }, 5000);
}


/* ---------------------------- */
/* toggle forgot password modal
/* ---------------------------- */
function forgot() {
    $('#reset_form').fadeIn();
    $('#loginform').hide();
}

function closex() {
    $('#reset_form').hide();
    $('#loginform').slideDown();
}


function requestlink(){

    var registeredEmailAddress = $("#dataForm_ResetPasswordEmailAddress_id").val();

    $("#dataButtonResetPassword").html('<span class="spinner-grow spinner-grow-lg"></span> Please wait')
    $("#dataButtonResetPassword").prop("disabled", true);

    if(registeredEmailAddress == '')
    {
        $(".dataSpan-reset-password").html('<p class="text-center text-danger">"Error: please enter your valid email address!</p>');

        $("#dataButtonResetPassword").html('Get Password Reset Link')
        $("#dataButtonResetPassword").prop("disabled", false);
    } else {

        $.ajax({
            url: "_method-file.php",
            type: "POST",
            data: "registeredEmailAddressUpdatePassword="+registeredEmailAddress, 
            cache: false, 
            success: function(data){                        
                $(".dataSpan-reset-password").html(data);

                $("#dataButtonResetPassword").html('Get Password Reset Link')
                $("#dataButtonResetPassword").prop("disabled", false);
            }
        });          
    }              
}
