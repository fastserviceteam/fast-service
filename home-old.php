<!DOCTYPE html> 

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>FAST SERVICE</title>
    
    <!-- Google Font -->
    <!--
    <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet'>
    -->
    
    <!-- /. Bootstrap -->
    <link href="_assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">    
    <!-- /. font-awesome -->
    <link rel="stylesheet" href="_assets/plugins/fontAwesome/css/font-awesome.min.css">
    <!-- /. animate css -->
    <link rel="stylesheet" href="_assets/plugins/animation/animate.css">     
    <!-- /. Owl Carousel Assets --> 
    <link href="_assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">    
    <link href="_assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">
    <!-- Pixeden Icon Font -->  
    <link rel="stylesheet" href="_assets/plugins/pe-icons/css/pe-icon-7-stroke.css">    
    <!-- /. custom style -->
    <link href="_assets/css/style.css" rel="stylesheet">
    <!-- /. PrettyPhoto -->
    <link href="_assets/css/prettyPhoto.css" rel="stylesheet">       
    <!-- /. responsive CSS -->  
    <link href="_assets/css/responsive.css" rel="stylesheet">
    <!-- /. w3 css -->
    <link rel="stylesheet" href="_assets/css/w3.css">
	
	 <link rel="stylesheet" href="_assets/css/fstyle.css">
	 
    
    <!-- /. Favicon --> 
    <link rel="shortcut icon" type="image/x-icon" href="_assets/img/logo.jpg" />
 
</head>

<body>
    <!-- 
    <div class="spn_hol">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>   -->

 <!-- =========================
     START ABOUT US SECTION
============================== -->
   <!-- parallax home-parallax -->
    <section class="header page" id="HOME">
          <div class="section_overlay">
            <nav class="navbar  navbar-fixed-top" role="navigation" style="background-color:#212121;border-bottom:2px solid khaki;color:cyan;">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="home-section.php">
                            <img src="_assets/img/logo.jpg" style="height:30px;" class="w3-circle img-responsive" alt="fast-service-logo">
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <!-- NAV -->
                            <li><a href="home.php" class="link_scroll">HOME</a> </li>
                            <li><a href="#ABOUT" class="link_scroll">ABOUT US </a> </li>
                            <li><a href="#FEATURES" class="link_scroll">FEATURES</a></li>
                            <li><a href="#GALLERY" class="link_scroll">GALLERY</a> </li>
                            <li><a href="#CONTACT" class="link_scroll">CONTACT US</a> </li>
                             <li><a href="#DOWNLOAD" class="link_scroll">DOWNLOAD</a> </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container- -->
            </nav>

            <div class="container-fluid" style="height:680px;">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-lg-4" style="background-image:url(_assets/img/home.jpg);background-repeat:no-repeat;background-size:100%;border-radius:4px;">
                        
                           
                          <font color="#FFF;" size="7.3em">   <img style="height:50px" class=" w3-circle" src="_assets/img/logo.jpg" alt="fast-service-logo"> 
                            <b>FAST SERVICE </b></font>                   
                          
                       
						</div>
						
					<div class="col-md-8 col-sm-12 col-lg-8" style="background-image:url(_assets/img/home.jpg);background-repeat:no-repeat;background-size:100%;padding-top:1em;">
					
                        <div align="right">
                 
					<a class="tuor btn wow fadeInRight link_scroll" href="#ABOUT"><font color="#FFF;">Take a tour</font><i class="fa fa-angle-down"></i></a>
                      
                            <!-- BUTTON -->
         <a class="btn home-btn wow fadeInLeft" style="background-color:#009688;border:1px solid #009688;color:#FFF;" data-toggle="modal" data-target="#myModal"><i class="fa fa-user"></i> &nbsp; REGISTER FREE</a>
                                
                                     <a class="btn home-btn wow fadeInLeft" style="background-color:#009688;border:1px solid #009688;color:#FFF;"  onclick="document.getElementById('id01').style.display='block'">
									 
									 <i class="fa fa-key"></i> &nbsp; LOGIN</a>
                                     
                                
                            
                       </div>
                    </div>	
					
                </div>
				
			 <div class="row">
			 
		<div class="col-md-8 col-lg-7 col-sm-12" style="background-color:#ECEFF1;;height:591px;">

		

		</div>

		

		<div class="col-md-5 col-lg-5 col-sm-12" style="background-image:url(_assets/img/phone2.png);background-repeat:no-repeat;height:600px;">
		
		

		</div>
				
                
                <!-- <hr> </hr> -->
            </div>
            
        </div>
    </section>

    <!-- END HEADER SECTION -->
     
        
        
 <!-- =========================
     START ABOUT US SECTION
============================== -->


    <section class="about page" id="ABOUT">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- ABOUT US SECTION TITLE-->
                    <div class="section_title">
                        <h2>About Us</h2>
                        <p>
                        Fast Service is a mobile and web application which brings fast access to daily services and products, from a wide range of vendors nearer, to you at your own convenient time and location.                     
                        </p>                
                        
                    </div>
                </div>

            </div>
        </div>
        <div class="inner_about_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                        <!-- PHONE -->
                            <img src="_assets/img/fast-service-app.png" alt="fast-service-app-startup-screen" style="width: 290px; height: 500px">
                        </div>
                    </div>
                    <div class="col-md-8  wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
                        <!-- TITLE -->
                        <div class="inner_about_title">
                            <h2>Why we are the first choice <br> for you</h2>
                            <p>Our Site/App provides a 24/7 update on the available products, vendors in the nearest place</p>
                        </div>
                        <div class="inner_about_desc">

                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay=".8s">
                                <!-- ICON -->
                                <div><i class="pe-7s-timer"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>Services</h3>
                                <p>We have a wide range of service providers ready to cater for all your needs</p>
                            </div>
                            <!-- END SINGLE DESC -->

                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                                <!-- ICON -->
                                <div><i class="pe-7s-headphones"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>Support</h3>
                                <p>We are ready to listen to all your questions and inquiries and assist you in any way possible.</p>
                            </div>
                            <!-- END SINGLE DESC -->

                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1.2s">
                                <!-- ICON -->
                                <div><i class="pe-7s-check"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>Convenience</h3>
                                <p>Order for what you want and when you want it at your own convenience with no hurries and worries.</p>
                            </div>
                            <!-- END SINGLE DESC -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->



<!-- =========================
     START FEATURES
============================== -->
    <section id="FEATURES" class="features page">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- FEATURES SECTION TITLE -->
                    <div class="section_title wow fadeIn" data-wow-duration="1s">
                        <h2>Features</h2>
                        <p>
                            Our application comes with a wide variety of cool features for everybody. In case you need to suggest a new feature, please don't hestitate to drop us a message or review using our contacts or playstore section respectively.
                        </p>
                    </div>
                    <!-- END FEATURES SECTION TITLE -->
                </div>
            </div>
        </div>

        <div class="feature_inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 right_no_padding wow fadeInLeft" data-wow-duration="1s">
                        <!-- FEATURE -->

                        <div class="left_single_feature">
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><i class="fa fa-thumbs-up text-success"></i> <span>/</span> USER FRIENLY DESIGN</h3>
                            <p>
                                No hussle and tussle when navigating. Feel free to check our captivating user interface.
                            </p>
                        </div>
                        <!-- END SINGLE FEATURE -->

                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><i class="fa fa-android text-success"></i> <span>/</span> RUNS ON ANDROID</h3>
                            <p>
                                Our mobile application has been designed to currently support all android devices with API 16 and above.
                            </p>
                        </div>
                        <!-- END SINGLE FEATURE -->

                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><i class="fa fa-tachometer text-success"></i> <span>/</span> MINIMAL DATA USAGE</h3>
                            <p>
                            We have tried all ways possible to limit data usage combined with smooth operation such that you find what you're looking without consuming alot of your internet data.
                            </p>
                        </div>
                        <!-- END SINGLE FEATURE -->

                    </div>
                    
                    
                    <div class="col-md-6 left_no_padding wow fadeInRight" data-wow-duration="1s">

                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><i class="fa fa-clock-o text-success"></i> <span>/</span> OPERATES 24/7</h3>
                            <p>
                                The website and mobile application will be accessed for 24 hours
                            </p>
                        </div>
                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><i class="fa fa-mobile-phone text-success"></i> <span>/</span> FREE ANDROID APP</h3>                    
                            <p>
                                Fast Service App has a mobile application that runs on Android phones and its can downloaded freely on play store.
                            </p>                            
                        </div>
                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><i class="fa fa-headphones text-success"></i> <span>/</span> SUPPORT CENTER</h3>
                            <p>
                                Fast Service App has customer support center that attends to the users questions and other requests
                            </p>
                        </div>
                        <!-- END SINGLE FEATURE -->
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END FEATURES SECTION -->


<!-- =========================
     Start APPS SCREEN SECTION
============================== -->
    <section class="apps_screen page" id="SCREENS">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 wow fadeInBig" data-wow-duration="1s">
                        <!-- APPS SCREEN TITLE -->
                        <div class="section_title">
                            <h2>GALLERY</h2>                         
                        </div>
                        <!-- END APPS SCREEN TITLE -->
                    </div>
                </div>
            </div>

        <div class="screen_slider">
            <div id="demo" class="wow bounceInRight" data-wow-duration="1s">
                <div id="owl-demo" class="owl-carousel">

                    <!-- APPS SCREEN IMAGES -->
                    <div class="item">
                    <a href="_assets/img/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="_assets/img/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="_assets/img/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="_assets/img/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="_assets/img/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="_assets/img/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="_assets/img/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="_assets/img/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="_assets/img/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="_assets/img/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="_assets/img/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="_assets/img/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ENS APPS SCREEN -->



<!-- =========================
     START DOWNLOAD NOW 
============================== -->
    <section class="download page" id="DOWNLOAD">

        <div class="available_store">
            <div class="container  wow bounceInRight" data-wow-duration="1s">
                <div class="col-md-6">
                    <div class="available_title">
                        <h2>Available on</h2>
                        <p>Our mobile application is currently available on Android devices running API 16 and above.</p>
                    </div>
                </div>

                <!-- DOWNLOADABLE STORE -->
                <div class="col-md-6">                   
                    <div class="no_padding">
                        <a href="https://playstore.google.com/fastservice">
                            <div class="single_store">
                                <i class="fa fa-android"></i>
                                <div class="store_inner">
                                    <h2>ANDROID</h2>
                                </div>
                            </div>
                        </a>
                    </div>                        
                </div>
                <!-- END DOWNLOADABLE STORE -->
            </div>
        </div>
    </section>
    <!-- END DOWNLOAD -->
    

<!-- =========================
     START CONTCT FORM AREA
============================== -->
    <section class="contact page" id="CONTACT">
        <div class="section_overlay">
            <div class="container">
                <div class="col-md-10 col-md-offset-1 wow bounceIn">
                    <!-- Start Contact Section Title-->
                    <div class="section_title">
                        <h2>Get in touch</h2>
                        <p>
                            For inquries, assistance, suggestions, scrutinies (yes, we want to hear them), please send us an email using  the contact form; or find us at the address below this form.
                        </p>
                    </div>
                </div>
            </div>

            <div class="contact_form wow bounceIn">
            
                <div class="container">


                <!-- CONTACT FORM starts here, Go to contact.php and add your email ID, thats it.-->    
                      <span id="result_msg"></span>           
          
                        <form role="form" id="fast_service_msg_form" class="clear_form_after_submission">
                        
                        <div class="row">
                            <div class="col-md-4">
                               
                                <input type="text" class="form-control" id="fast_service_sender_names" name="fast_service_sender_names" placeholder="Name" maxlength="15">
                                <input type="email" class="form-control" id="fast_service_email" name="fast_service_email" placeholder="Email" maxlength="64">
                                <input type="text" class="form-control" id="fast_service_subject" name="fast_service_subject" placeholder="Subject" maxlength="25">
                                
                            </div>


                            <div class="col-md-8">
                            
                                <textarea class="form-control" name="fast_service_sent_text" rows="25" cols="10" placeholder="your message (150 characters)" maxlength="150" id="fast_service_sent_text"></textarea>
                                
                                <input type="hidden" class="form-control" id="fast_service_msg_date" name="fast_service_msg_date" value="<?php echo date('Y-m-d') ?>">
                                
                                <button type="submit" class="btn btn-block btn-success" id="dataSendMessagebtn">SEND MESSAGE</button>
                            </div>
                            
                            
                            
                        </div>
                    </form>
                    <!-- END FORM --> 
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInUp">
                        <div class="address_blockline">
                            <ul>
                                <li><i class="fa fa-home"></i> P.O Box 183, Kyambogo
                                </li>
                                <li><i class="fa fa-phone"></i> +256 .........loading
                                </li>
                                <li><i class="fa fa-envelope"></i> fastserviceug@gmail.com
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 wow fadeInUp">
                        <div class="social_icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END CONTACT -->
    

<!-- =========================
     Start location 
============================== -->

    <section class="ourlocation">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">

                        <!-- Start Subscribe Section Title -->
                        <div class="section_title">
                            <h2>OUR LOCATION</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row wow fadeInUp">
                    <div class="col-md-12">
                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.7462085154675!2d32.63347151519876!3d0.
                    34448076407525763!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x177db9790d227fd5%3A0xf9d6646bcaf9e09d!2sBanda+
                    Computer+Centre!5e0!3m2!1sen!2sug!4v1468244061727" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>

                    </div>
                </div>
            </div>
     
    </section>

    <!-- END LOCATION FORM -->


<!-- =========================
     FOOTER 
============================== -->

    <section class="copyright">
        <h2></h2>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copy_right_text">
                    <!-- COPYRIGHT TEXT -->
                        <p>Copyright &copy; <?php echo date('Y'); ?>. All Rights Reserved.</p>        
                        <p>Designed by <a href="#">Fast Service Co Ltd</a></p>
                    </div>
                     
                </div>

                <div class="col-md-6">
                    <div class="scroll_top">
                        <a href="#"><i class="fa fa-angle-up"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END FOOTER -->


<!-- =========================
     SCRIPTS 
============================== -->
<?php require "_partials/_bottom-scripts.php"; ?>

<!-- =========================
     VIEWS 
============================== -->
<?php require "_partials/_modal-views.php"; ?>


</body>
</html>