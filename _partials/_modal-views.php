<!-- /. registration modal -->
<!--
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        -- Modal content--
        <div class="modal-content">
            <div class="modal-header bg-olive text-white">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                <center>
                <img width="85" height="45" style="border:1px solid teal;" class="w3-circle" src="_assets/img/logo.jpg" alt=""> &nbsp; &nbsp;
                <strong>SIGNUP FOR FREE</strong>
                </center>
                </h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <span id="result"> </span>
                    <form id="reg_form" class="clear_form_after_submission" role="form">
                        <div class="form-group mb-lg">
                            <label for="input_fullNames">Name <font color="red">*</font></label>
                            <input name="input_fullNames" id="input_fullNames" type="text" class="form-control input-md" placeholder="enter your full names" required aria-describedby="fullNamesExample" autocomplete="off">
                            <small id="fullNamesExample" class="text-muted">eg. John Doe</small>
                        </div>
                        <div class="form-group mb-lg">
                            <label for="input_phoneNumber">Phone number <font color="red">*</font></label>
                            <input name="input_phoneNumber" id="input_phoneNumber" type="text" class="form-control input-md" placeholder="enter your phonen number" required aria-describedby="phonenumberHelp" autocomplete="off">
                            <small id="phonenumberHelp" class="text-muted">eg. 0777xxxxxx</small>
                        </div>
                        <div class="form-group mb-lg">
                            <label for="input_valid_emailAddress">E-mail Address <font color="red">*</font></label>
                            <input name="input_valid_emailAddress" id="input_valid_emailAddress" type="email" class="form-control input-md" placeholder="enter your valid email address" maxlength="64" required aria-describedby="emailAddressHelp" autocomplete="off">
                            <small id="emailAddressHelp" class="text-muted">eg. johndoe@something.com</small>
                        </div>
                        <div class="form-group mb-none">
                            <div class="row">
                                <div class="col-sm-6 mb-lg">
                                    <label for="input_strong_password">Password <font color="red">*</font></label>
                                    <input name="input_strong_password" type="password" class="form-control input-md" placeholder="enter your password" required aria-describedby='passwordHelp' id="input_strong_password" autocomplete="off">
                                    <small id="passwordHelp" class="text-muted">Mininum of 8 characters</small>
                                </div>
                                <div class="col-sm-6 mb-lg">
                                    <label for="input_confirm_strong_password">Password Confirmation <font color="red">*</font></label>
                                    <input name="input_confirm_strong_password" type="password" class="form-control input-md" placeholder="confirm your password" required id="input_confirm_strong_password" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="checkbox-custom checkbox-default">
                                    <input id="AgreeTerms" name="agreeterms" type="checkbox">
                                    <label for="AgreeTerms">I agree with <a href="javascript:void(0)" class="text-underline text-warning">TERMS OF USE</a></label>
                                </div>
                                <small class="text-muted"><i class="fa fa-hand-o-right"></i> Agree to Terms of use by checking checkbox before submitting</small>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" id="registrationSubmitBtn" class="btn btn-primary btn-flat btn-lg general-button-class" disabled>Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <p class="text-left">
                    <span><font color="red">*</font> These fields are required</span>
                </p>
            </div>
        </div>
    </div>
</div>
-->

<!-- /. login modal -->
<!--
<div id="id01" class="w3-modal">
    <div class="w3-modal-content w3-card-8 w3-animate-zoom w3-round" style="max-width:400px">
        
        <span id="loginFeedback"></span>
        <form class="w3-container" role="form" id="loginform">
            <div class="w3-section">
                <label for="input_login_email_address"><b><span class="fa fa-user"></span>&nbsp;&nbsp;User Email Address</b></label>
                <input class="w3-input w3-border w3-margin-bottom w3-round" type="email" placeholder="enter your email address" name="input_login_email_address" required autofocus id="input_login_email_address">
                <label for="input_login_password"><b><span class="fa fa-lock"></span> Password</b></label>
                <input class="w3-input w3-border w3-round" type="password" placeholder="enter Password" name="input_login_password" required id="input_login_password">
                --<input type="hidden" name="input_login_with_data" value="logIn">--
                <button class="w3-btn-block w3-teal w3-section w3-padding w3-round btn-flat" name="submit-login" id="submit-login" type="submit">
                Login
                </button>
                <input class="w3-check w3-margin-top" type="checkbox" value="1" id="input_remember_me" name="input_remember_me"> Remember me
            </div>
        </form>
        <div class="w3-container w3-border-top w3-padding-16 w3-light-grey w3-round">
            <div id="reset_form" style="display:none">
                <div class=" ">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <button type="button" onclick="closex();" class="btn btn-sm btn-danger  bold pull-right">X</button>
                            <div class="text-center">
                                <span id="show_success">
                                    <h3><i class="fa green fa-lock fa-3x"></i></h3>
                                    <h2 class="text-center">Forgot Password?</h2>
                                </span>
                                <form class="form">
                                    <fieldset>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                <input id="getemail" name="getemail" placeholder="email address" autocomplete="off" class="form-control" type="email" oninvalid="setCustomValidity('Please enter a valid email address!')" onchange="try{setCustomValidity('')}catch(e){}" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button id="resetpass" onclick="requestlink();" name="resetpass" type="button" class="btn btn-lg btn-primary btn-block">Get Password Reset Link</button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="document.getElementById('id01').style.display='none'" type="button" class="btn-xs w3-button w3-round w3-pink">Cancel</button>
            <span class="w3-right w3-padding w3-hide-small w3-round"><b>Forgot <a onclick="forgot();" href="#">password ?</b></a></span>
        </div>
    </div>
</div>
-->
<!-- forgot password -->
<!-- The Modal -->
<div class="modal fade" id="forgotPasswordModal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header bg-dark">
        <h4 class="modal-title text-white text-center">Reset Password</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <span class="dataSpan-reset-password"></span>
        <form role="form" id="dataForm-reset-password">
            <div class="form-group">
                <input id="dataForm_ResetPasswordEmailAddress_id" name="dataForm_ResetPasswordEmailAddress" placeholder="email address" autocomplete="off" class="form-control" type="email" required>
            </div>
            <div class="form-group">
                <button id="dataButtonResetPassword" onclick="requestlink();" type="button" class="btn btn-md btn-outline-primary btn-flat">Get Password Reset Link</button>
            </div>
        </form>
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>