<?php
    /* -------------------------- */
    /* bottom scripts
    /* -------------------------- */
?>
<!-- /. jquery plugin -->
<script src="_assets/plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- /. bootstrap js lib -->
<script src="_assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- /. jqueryui -->
<script src="_assets/plugins/jQueryUI/js/jquery-ui.js"></script>

<!-- /. prettyPhoto -->
<script src="_assets/js/jquery.prettyPhoto.js"></script>

<!-- /. parallax -->
<script src="_assets/js/jquery.parallax-1.1.3.js"></script>

<!-- /. owl-carousel -->
<script src="_assets/plugins/owl-carousel/owl.carousel.js"></script>

<!-- /. wow -->
<script src="_assets/plugins/animation/wow.min.js"></script>

<!-- /. smoothscroll -->
<script src="_assets/plugins/animation/SmoothScroll.min.js"></script>

<!-- /. easing -->
<script src="_assets/plugins/animation/easing.js"></script>
<script src="_assets/plugins/animation/move-top.js"></script>

<!-- /. waypoints -->
<script src="_assets/js/waypoints.min.js"></script>

<!-- /. jquery-counterup -->
<script src="_assets/js/jquery.counterup.min.js"></script>

<!-- /. validation -->
<script src="_assets/js/validation.min.js"></script>

<!-- /. custom scripts -->
<script src="_assets/js/fast-service-script.js"></script>

<!-- /. init script -->
<script src="_assets/js/init.js"></script>