
	
	//js for education background
	
	
	$(document).ready(function() {
		
    $("#educ_btn").click(function() {
 
        // using serialize function of jQuery to get all values of form
        var serializedData = $("#educ_form").serialize();
 
        // Variable to hold request
        var request;
        // Fire off the request to process_registration_form.php
		
		var resultMessage = $('#afterEduc');
		
        request = $.ajax({
            url: "fastProcess.php?userEduc",
            type: "GET",
            data: serializedData
        });
		
		//clearing the form fields 
		
		$('#educ_form')[0].reset();
 
        // Callback handler that will be called on success
		
        request.done(function(jqXHR, textStatus, response) {
			
            // you will get response from your php page (what you echo or print)
             // show successfully for submit message
			 //reload the page
			 
			 location.reload();
			 
            $("#afterEduc").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> Education background updated</div>');
        });
 
        // Callback handler that will be called on failure
		
        request.fail(function(jqXHR, textStatus, errorThrown) {
			
            // Log the error to the console
            // show error
            $("#afterEduc").html('There is some error while submit');
			
            console.error(
			
                "The following error occurred: " +
                
				textStatus, errorThrown
            );
        });
 
        return false;
 
    });
	
	
	
});






//js for creating working experience 
	
	
	$(document).ready(function() {
		
    $("#work_btn").click(function() {
 
        // using serialize function of jQuery to get all values of form
        var serializedData = $("#work_form").serialize();
 
        // Variable to hold request
        var request;
        // Fire off the request to process_registration_form.php
		
		var resultMessage = $('#afterWork');
		
        request = $.ajax({
            url: "fastProcess.php?userWork",
            type: "GET",
            data: serializedData
        });
		
		
		//clearing the form fields 
		
		$('#work_form')[0].reset();
 
        // Callback handler that will be called on success
		
        request.done(function(jqXHR, textStatus, response) {
			
            // you will get response from your php page (what you echo or print)
             // show successfully for submit message
			 
			 location.reload();
			 
            $("#afterWork").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> Working experience has been updated</div>');
        });
 
        // Callback handler that will be called on failure
		
        request.fail(function(jqXHR, textStatus, errorThrown) {
			
            // Log the error to the console
            // show error
            $("#afterWork").html('There is some error while submit');
			
            console.error(
			
                "The following error occurred: " +
                
				textStatus, errorThrown
            );
        });
 
        return false;
 
    });
	
	
	});
	
	
	//js for adding skills
	
	
	$(document).ready(function() {
		
    $("#skill_btn").click(function() {
 
        // using serialize function of jQuery to get all values of form
        var serializedData = $("#skill_form").serialize();
 
        // Variable to hold request
        var request;
        // Fire off the request to process_registration_form.php
		
		var resultMessage = $('#afterSkill');
		
        request = $.ajax({
            url: "fastProcess.php?userSkill",
            type: "GET",
            data: serializedData
        });
		
		
		//clearing the form fields 
		
		$('#skill_form')[0].reset();
 
        // Callback handler that will be called on success
		
        request.done(function(jqXHR, textStatus, response) {
			
            // you will get response from your php page (what you echo or print)
             // show successfully for submit message
			 
			 location.reload();
			 
            $("#afterSkill").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> Your skills list have been updated </div>');
        });
 
        // Callback handler that will be called on failure
		
        request.fail(function(jqXHR, textStatus, errorThrown) {
			
            // Log the error to the console
            // show error
            $("#afterSkill").html('There is some error while submit');
			
            console.error(
			
                "The following error occurred: " +
                
				textStatus, errorThrown
            );
        });
 
        return false;
 
    });
	
	
	
});



//js for adding language
	
	
	$(document).ready(function() {
		
    $("#lang_btn").click(function() {
 
        // using serialize function of jQuery to get all values of form
        var serializedData = $("#lang_form").serialize();
 
        // Variable to hold request
        var request;
        // Fire off the request to process_registration_form.php
		
		var resultMessage = $('#afterLang');
		
        request = $.ajax({
            url: "fastProcess.php?userLang",
            type: "GET",
            data: serializedData
        });
		
		
		//clearing the form fields 
		
		$('#lang_form')[0].reset();
 
        // Callback handler that will be called on success
		
        request.done(function(jqXHR, textStatus, response) {
			
            // you will get response from your php page (what you echo or print)
             // show successfully for submit message
			 
			 location.reload();
			 
            $("#afterLang").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> You added another language </div>');
        });
 
        // Callback handler that will be called on failure
		
        request.fail(function(jqXHR, textStatus, errorThrown) {
			
            // Log the error to the console
            // show error
            $("#afterLang").html('There is some error while submit');
			
            console.error(
			
                "The following error occurred: " +
                
				textStatus, errorThrown
            );
        });
 
        return false;
 
    });
	
	
	
});


//reating and posting a job
	
	
	function saveJob() {
 
        // using serialize function of jQuery to get all values of form
        var serializedData = $("#job_form").serialize();
		
 
        // Variable to hold request
        var request;
        // Fire off the request to process_registration_form.php
		
		var resultMessage = $('#afterJob');
		
        request = $.ajax({
            url: "fastProcess.php?jobSave",
            type: "GET",
            data: serializedData
        });
		
		
		//clearing the form fields 
		
		$('#job_form')[0].reset();
 
        // Callback handler that will be called on success
		
        request.done(function(jqXHR, textStatus, response) {
			
            // you will get response from your php page (what you echo or print)
             // show successfully for submit message
			 
		
			 
            $("#afterJob").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <b>Your job request was sent sucessfully </b></div>');
        });
 
        // Callback handler that will be called on failure
		
        request.fail(function(jqXHR, textStatus, errorThrown) {
			
            // Log the error to the console
            // show error
            $("#afterJob").html('There is some error while submit');
			
            console.error(
			
                "The following error occurred: " +
                
				textStatus, errorThrown
            );
        });
 
        return false;
 
    }
	










