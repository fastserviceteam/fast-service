-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2019 at 11:15 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bandacomputer_fastservice_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `advert_posts`
--

CREATE TABLE `advert_posts` (
  `post_id` int(11) NOT NULL,
  `business_profile_id` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `image_name` varchar(60) DEFAULT NULL,
  `image` blob NOT NULL,
  `date_post` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advert_posts`
--

INSERT INTO `advert_posts` (`post_id`, `business_profile_id`, `description`, `image_name`, `image`, `date_post`) VALUES
(1, 12, 'We hide complexity', 'FutureInfo.png', 0x706f7374732f32302d30312d323031392d313534383030313931322e706e67, '2019-01-20'),
(3, 13, 'Home of IT Solutions ', 'parked.jpg', 0x706f7374732f32302d30312d323031392d313534383030393730302e6a7067, '2019-01-20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advert_posts`
--
ALTER TABLE `advert_posts`
  ADD PRIMARY KEY (`post_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advert_posts`
--
ALTER TABLE `advert_posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
