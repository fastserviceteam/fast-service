-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2019 at 03:59 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bandacomputer_fastservice_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL,
  `business_profile_id` int(11) NOT NULL,
  `job_vacancy` varchar(50) NOT NULL,
  `location` varchar(80) NOT NULL,
  `job_type` varchar(50) NOT NULL,
  `job_function` varchar(50) NOT NULL,
  `salary` varchar(10) NOT NULL,
  `application_mode` varchar(50) NOT NULL,
  `date_posted` varchar(15) NOT NULL,
  `deadline` varchar(15) NOT NULL,
  `description` varchar(300) NOT NULL,
  `approved` varchar(5) NOT NULL,
  `tobe_posted` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `business_profile_id`, `job_vacancy`, `location`, `job_type`, `job_function`, `salary`, `application_mode`, `date_posted`, `deadline`, `description`, `approved`, `tobe_posted`) VALUES
(6, 13, 'Programmer ', 'Kampala', 'Full-time', 'Developing sotwares', '650000', 'Send futurehr@gmail.com', '2019-01-08', '2019-01-29', 'Advanced skills in android programming ', '', '2019-01-15'),
(7, 13, 'Marketier', 'Kampala ', 'Full-time', 'Advertising ', '750000', 'send on futurehr@gmail.com', '2019-01-08', '2019-01-22', 'Masters in Marketting ', '', '2019-01-08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
