-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2019 at 12:42 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bandacomputer_fastservice_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_profile`
--

CREATE TABLE `business_profile` (
  `business_profile_id` int(11) NOT NULL,
  `Business_Name` varchar(200) NOT NULL,
  `Business_Address` varchar(200) NOT NULL,
  `Business_Phone` varchar(50) NOT NULL,
  `Business_Email` varchar(100) NOT NULL,
  `Business_Category` varchar(200) NOT NULL,
  `Services_Offered` varchar(500) NOT NULL,
  `Geographical_Area` varchar(200) NOT NULL,
  `vander_latitude` varchar(100) DEFAULT NULL,
  `vander_longitude` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_profile`
--

INSERT INTO `business_profile` (`business_profile_id`, `Business_Name`, `Business_Address`, `Business_Phone`, `Business_Email`, `Business_Category`, `Services_Offered`, `Geographical_Area`, `vander_latitude`, `vander_longitude`) VALUES
(1, 'Musoke and sons investments tld', 'Uganda', '07878678', 'sam@gmail.com', 'Farming and agriculture', 'supplying potatoes and cabbages', 'Ntungamo district', NULL, NULL),
(2, 'Ntungamo Computer Center', 'Uganda', '077787843', 'sam@gmail.com', 'Information Technology', 'computer repair, accesories and computer sales', 'Nyungamo', NULL, NULL),
(3, 'Real tech co ltd', 'Uganda', '077783434', 'sam@gmail.com', 'Research and Project Analysis', 'research projects and mangement', 'Kampala banda', NULL, NULL),
(4, 'wide wild uganda safaris', 'Uganda', '077898989', 'sam@gmail.com', 'Information Technology', 'Tourism, Bird watching, and Zoe touring', 'Kira Town', '0.3972', '32.6387'),
(5, 'FutureInfo Soultions', 'Uganda', '0785574424', 'tom@gmail.com', 'Farming and agriculture', 'business planning, proposal writing', 'Kampala', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business_profile`
--
ALTER TABLE `business_profile`
  ADD PRIMARY KEY (`business_profile_id`),
  ADD KEY `business_profile_id` (`business_profile_id`),
  ADD KEY `Business_Email` (`Business_Email`),
  ADD KEY `Business_Category` (`Business_Category`),
  ADD KEY `Services_Offered` (`Services_Offered`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_profile`
--
ALTER TABLE `business_profile`
  MODIFY `business_profile_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
