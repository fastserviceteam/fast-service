-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2019 at 11:17 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bandacomputer_fastservice_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `fs_staff`
--

CREATE TABLE `fs_staff` (
  `staff_id` int(11) NOT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `current_address` varchar(100) DEFAULT NULL,
  `permanent_address` varchar(100) DEFAULT NULL,
  `id_no` varchar(40) DEFAULT NULL,
  `position` varchar(40) DEFAULT NULL,
  `workstation` varchar(50) NOT NULL,
  `ur_email` varchar(50) DEFAULT NULL,
  `image_name` varchar(40) DEFAULT NULL,
  `image` blob NOT NULL,
  `date_reg` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fs_staff`
--

INSERT INTO `fs_staff` (`staff_id`, `fname`, `lname`, `dob`, `phone_no`, `current_address`, `permanent_address`, `id_no`, `position`, `workstation`, `ur_email`, `image_name`, `image`, `date_reg`) VALUES
(4, 'Anthon', 'Turyahikayo', '29-april-1992', '0752334382', 'kireka', 'kabale', 'CXV9008K', 'Multimedi Architect', 'Banda', 'anthonturyahikayo@gmail.com', 'banxie.jpg', 0x75706c6f6164732f31322d30312d323031392d313534373238373935352e6a7067, '2019-01-12'),
(5, 'Stephen', 'Kabeiraho', '21-May-1991', '0704578518', 'kyaliwajala', 'kabale', 'YF005angt', 'Web developer', 'Banda', 'stvo@gmail.com', 'jesus.jpg', 0x75706c6f6164732f31322d30312d323031392d313534373238383038312e6a7067, '2019-01-12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fs_staff`
--
ALTER TABLE `fs_staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fs_staff`
--
ALTER TABLE `fs_staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
