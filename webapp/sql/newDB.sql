-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2019 at 07:10 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bandacomputer_fastservice_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `advert_posts`
--

CREATE TABLE `advert_posts` (
  `post_id` int(11) NOT NULL,
  `business_profile_id` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `image_name` varchar(60) DEFAULT NULL,
  `image` blob NOT NULL,
  `date_post` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advert_posts`
--

INSERT INTO `advert_posts` (`post_id`, `business_profile_id`, `description`, `image_name`, `image`, `date_post`) VALUES
(1, 12, 'We hide complexity', 'FutureInfo.png', 0x706f7374732f32302d30312d323031392d313534383030313931322e706e67, '2019-01-20'),
(3, 13, 'Home of IT Solutions ', 'parked.jpg', 0x706f7374732f32302d30312d323031392d313534383030393730302e6a7067, '2019-01-20'),
(4, 6, 'edrfr', '_DSC0299.JPG', 0x706f7374732f32312d30322d323031392d313535303734373833352e6a7067, '2019-02-21');

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `bill_id` int(11) NOT NULL,
  `item_name` varchar(30) NOT NULL,
  `billing_type` varchar(25) NOT NULL,
  `min_value` varchar(10) NOT NULL,
  `max_value` varchar(25) NOT NULL,
  `last_edit` varchar(20) NOT NULL,
  `amount` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`bill_id`, `item_name`, `billing_type`, `min_value`, `max_value`, `last_edit`, `amount`) VALUES
(4, 'business_subscription', 'Monthly Income', '60000', '120000', '2019-01-01', '2000'),
(5, 'business_subscription', 'Monthly Income', '120001', '240000', '2019-01-01', '4000'),
(6, 'business_subscription', 'Monthly Income', '240001', '360000', '2019-01-01', '6000'),
(7, 'advert', 'duration', '0', '30', '2019-01-01', '10000'),
(8, 'advert', 'duration', '31', '60', '2019-01-01', '20000'),
(9, 'job', 'duration', '0', '14', '2019-01-01', '2000'),
(10, 'job', 'duration', '15', '21', '2019-01-01', '5000'),
(11, 'business_subscription', 'viewers', '0', '1200', '2019-01-03', '10000');

-- --------------------------------------------------------

--
-- Table structure for table `business_profile`
--

CREATE TABLE `business_profile` (
  `business_profile_id` int(11) NOT NULL,
  `Business_Name` varchar(200) NOT NULL,
  `Business_Address` varchar(200) NOT NULL,
  `Business_Phone` varchar(50) NOT NULL,
  `Business_Email` varchar(100) NOT NULL,
  `Business_Category` varchar(200) NOT NULL,
  `Services_Offered` varchar(500) NOT NULL,
  `Geographical_Area` varchar(200) NOT NULL,
  `vander_longitude` varchar(500) DEFAULT NULL,
  `vander_latitude` varchar(500) DEFAULT NULL,
  `time_taken` varchar(50) DEFAULT NULL,
  `bzns_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 default, 1 received, 2 accepted, 3 cancelled',
  `client_id` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_profile`
--

INSERT INTO `business_profile` (`business_profile_id`, `Business_Name`, `Business_Address`, `Business_Phone`, `Business_Email`, `Business_Category`, `Services_Offered`, `Geographical_Area`, `vander_longitude`, `vander_latitude`, `time_taken`, `bzns_status`, `client_id`) VALUES
(22, 'BANDA COMPUTER CENTER', 'Kampala', '98888888888', 'joan@gmail.com', 'Computer training', '1,3,4', 'BANDA KAMPALA', '', '', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(60) NOT NULL,
  `industry_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `industry_id`) VALUES
(2, 'Computer Repair', 2),
(3, 'computer accesssories', 2),
(4, 'Computer training ', 2),
(5, 'Cabbages ', 3),
(6, 'Broilers ', 4);

-- --------------------------------------------------------

--
-- Table structure for table `client_quotation`
--

CREATE TABLE `client_quotation` (
  `client_quotation_id` int(11) NOT NULL,
  `client_email` varchar(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `service_address` varchar(200) NOT NULL,
  `service_needed` varchar(200) NOT NULL,
  `area_selected` varchar(200) NOT NULL,
  `quotation_details` varchar(1000) NOT NULL,
  `vander_appointed` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 default,1 received, 2 confirmed, 3 finalized ',
  `vander_quotation` varchar(500) DEFAULT NULL,
  `vander_latitude` varchar(200) DEFAULT NULL,
  `vander_longitude` varchar(200) DEFAULT NULL,
  `latitude` varchar(200) DEFAULT NULL,
  `longitude` varchar(200) DEFAULT NULL,
  `time_taken` varchar(50) DEFAULT NULL,
  `on_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_quotation`
--

INSERT INTO `client_quotation` (`client_quotation_id`, `client_email`, `location`, `service_address`, `service_needed`, `area_selected`, `quotation_details`, `vander_appointed`, `status`, `vander_quotation`, `vander_latitude`, `vander_longitude`, `latitude`, `longitude`, `time_taken`, `on_date`) VALUES
(6, 'suzan@gmail.com', 'Kampala Uganda', 'kampala', 'ICT Technology', '', 'hello, i need 10 computers', '', 0, NULL, '', '', '', '', NULL, '2019-06-13 16:52:43'),
(7, 'suzan@gmail.com', 'Kampala Uganda', 'sdf', 'Education Training', '', 'dfgfhdfg', '', 2, NULL, '', '', '', '', NULL, '2019-06-13 17:00:05');

-- --------------------------------------------------------

--
-- Table structure for table `fs_staff`
--

CREATE TABLE `fs_staff` (
  `staff_id` int(11) NOT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `current_address` varchar(100) DEFAULT NULL,
  `permanent_address` varchar(100) DEFAULT NULL,
  `id_no` varchar(40) DEFAULT NULL,
  `position` varchar(40) DEFAULT NULL,
  `workstation` varchar(50) NOT NULL,
  `ur_email` varchar(50) DEFAULT NULL,
  `image_name` varchar(40) DEFAULT NULL,
  `image` blob NOT NULL,
  `date_reg` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fs_staff`
--

INSERT INTO `fs_staff` (`staff_id`, `fname`, `lname`, `dob`, `phone_no`, `current_address`, `permanent_address`, `id_no`, `position`, `workstation`, `ur_email`, `image_name`, `image`, `date_reg`) VALUES
(4, 'Anthon', 'Turyahikayo', '29-april-1992', '0752334382', 'kireka', 'kabale', 'CXV9008K', 'Multimedi Architect', 'Banda', 'anthonturyahikayo@gmail.com', 'banxie.jpg', 0x75706c6f6164732f31322d30312d323031392d313534373238373935352e6a7067, '2019-01-12'),
(5, 'Stephen', 'Kabeiraho', '21-May-1991', '0704578518', 'kyaliwajala', 'kabale', 'YF005angt', 'Web developer', 'Banda', 'stvo@gmail.com', 'jesus.jpg', 0x75706c6f6164732f31322d30312d323031392d313534373238383038312e6a7067, '2019-01-12');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `industry_id` int(11) NOT NULL,
  `industry_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`industry_id`, `industry_name`) VALUES
(2, 'Information Technology'),
(3, 'Agriculture'),
(4, 'Poultry');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `language_id` int(11) NOT NULL,
  `language` varchar(40) NOT NULL,
  `read_efficiency` varchar(40) NOT NULL,
  `write_efficiency` varchar(40) NOT NULL,
  `ur_email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`language_id`, `language`, `read_efficiency`, `write_efficiency`, `ur_email`) VALUES
(1, 'English', 'Good', 'Good', 'banxie@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `like_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `ur_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`like_id`, `item_id`, `ur_email`) VALUES
(1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `sender_names` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `sent_text` varchar(300) NOT NULL,
  `msg_date` varchar(20) NOT NULL,
  `reply` varchar(300) DEFAULT NULL,
  `reply_date` varchar(20) DEFAULT NULL,
  `msg_read` varchar(6) DEFAULT NULL,
  `sent` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `sender_names`, `email`, `subject`, `sent_text`, `msg_date`, `reply`, `reply_date`, `msg_read`, `sent`) VALUES
(3, 'Gilbert Agaba', 'agaba@gmail.com', 'Registering many business', 'can one create more than one business', '2019-01-03', '', '', '', ''),
(4, 'Judith Ayine ', 'ayine@gmail.com', 'login issues', 'is it possible to use a phone number and email on login', '2019-01-03', '', '', '', ''),
(5, 'Alla Teba', 'anthonturyahikayo@gmail.com', 'payments', 'How do i make payments', '2019-01-03', 'Good afternoon \r\nMonthly subscription is our payment term \r\nThanks, regards', '2019-01-08', 'YES', 'YES'),
(6, 'timothy aine ', 'aine@yahoo.com', 'advertising ', 'how much s advrtising per moth for big companies ', '2019-01-03', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notifications_id` int(11) NOT NULL,
  `client_id` varchar(11) DEFAULT NULL,
  `service_id` varchar(11) DEFAULT NULL,
  `no_of_views` int(11) NOT NULL DEFAULT '0',
  `viewed` varchar(11) NOT NULL DEFAULT 'no',
  `viewed_by` varchar(11) DEFAULT NULL,
  `confirmed` varchar(11) NOT NULL DEFAULT 'no',
  `on_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `price_range` varchar(11) DEFAULT NULL,
  `fs_charge` varchar(11) DEFAULT NULL,
  `vander_quotation` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notifications_id`, `client_id`, `service_id`, `no_of_views`, `viewed`, `viewed_by`, `confirmed`, `on_date`, `status`, `price_range`, `fs_charge`, `vander_quotation`) VALUES
(8, '1', '3', 0, 'no', NULL, 'no', '2019-06-13 16:52:43', 0, NULL, NULL, NULL),
(9, '7', '4', 0, 'yes', '22', 'no', '2019-06-13 17:00:05', 0, '10000', '300', '9000');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `industry_id` varchar(11) NOT NULL,
  `category_id` varchar(11) NOT NULL,
  `service_name` varchar(50) NOT NULL,
  `requested` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `industry_id`, `category_id`, `service_name`, `requested`) VALUES
(1, '1', '1', 'Computer Repair', 'no'),
(2, '2', '2', 'Farming Cultivation', 'no'),
(3, '2', '2', 'ICT Technology', 'yes'),
(4, '1', '3', 'Education Training', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `ur_login`
--

CREATE TABLE `ur_login` (
  `ur_login_id` int(11) NOT NULL,
  `ur_email` varchar(30) NOT NULL,
  `ur_pas` varchar(100) NOT NULL,
  `ur_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ur_login`
--

INSERT INTO `ur_login` (`ur_login_id`, `ur_email`, `ur_pas`, `ur_type`) VALUES
(13, 'joan@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'user'),
(14, 'suzan@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `user_education`
--

CREATE TABLE `user_education` (
  `education_id` int(11) NOT NULL,
  `educ_level` varchar(20) NOT NULL,
  `institution` varchar(60) NOT NULL,
  `award` varchar(70) NOT NULL,
  `grade` varchar(30) NOT NULL,
  `year` varchar(5) NOT NULL,
  `ur_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_education`
--

INSERT INTO `user_education` (`education_id`, `educ_level`, `institution`, `award`, `grade`, `year`, `ur_email`) VALUES
(3, 'Primary', 'Kikungiri Primary School', 'pass slip', 'First', '2004', 'banxie@gmail.com'),
(4, 'Secondary', 'St Marys College Rushoroza ', 'UCE Ceritificate', 'First ', '2008', 'banxie@gmail.com'),
(5, 'Secondary', 'St Marys College Rushoroza', 'UACE Certificate', '19 points', '2010', 'banxie@gmail.com'),
(6, 'Tertiary', 'Kyambogo University ', 'Bachelors in IT and Computing ', '2 class upper ', '2015', 'banxie@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `profile_id` int(11) NOT NULL,
  `names` varchar(70) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `ur_email` varchar(50) NOT NULL,
  `gender` varchar(7) NOT NULL,
  `date_of_birth` varchar(20) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `id_type` varchar(20) NOT NULL,
  `id_no` varchar(40) NOT NULL,
  `current_address` varchar(100) NOT NULL,
  `permanent_address` varchar(100) NOT NULL,
  `referee` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`profile_id`, `names`, `phone_no`, `ur_email`, `gender`, `date_of_birth`, `nationality`, `id_type`, `id_no`, `current_address`, `permanent_address`, `referee`) VALUES
(3, 'Anold Ahurire', '0782334543', 'anold@gmail.com', 'M', '2018-12-10', 'anan', 'Passport', 'e33', 'sdndnd', 'ffd', ' dddd'),
(4, 'anthon turyahikayo', '0785574424', 'banxie@gmail.com', '', '', '', '', '', '', '', ''),
(7, 'Fastservice admin', '', 'fastservice@fs.com', '', '', '', '', '', '', '', ''),
(8, 'Steven Kabeiraho', '0999999', 'sam@gmail.com', '', '', '', '', '', '', '', ''),
(9, 'musoke', '067845', 'musoke@gmail.com', '', '', '', '', '', '', '', ''),
(10, 'andrew', '0832678', 'andrew@gmail.com', '', '', '', '', '', '', '', ''),
(11, 'joan', '0834389', 'joan@gmail.com', '', '', '', '', '', '', '', ''),
(12, 'suzan', '076856785', 'suzan@gmail.com', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_skills`
--

CREATE TABLE `user_skills` (
  `skill_id` int(11) NOT NULL,
  `skill` varchar(50) NOT NULL,
  `ur_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_skills`
--

INSERT INTO `user_skills` (`skill_id`, `skill`, `ur_email`) VALUES
(1, 'Programming', 'banxie@gmail.com'),
(2, 'MC', 'banxie@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `working_experience`
--

CREATE TABLE `working_experience` (
  `experience_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `location` varchar(100) NOT NULL,
  `from_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `ur_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `working_experience`
--

INSERT INTO `working_experience` (`experience_id`, `title`, `company`, `location`, `from_date`, `end_date`, `ur_email`) VALUES
(3, 'Research Associate', 'Eden Resource Center', 'Banda', '2015-01-13', '2016-01-03', 'banxie@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advert_posts`
--
ALTER TABLE `advert_posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `business_profile`
--
ALTER TABLE `business_profile`
  ADD PRIMARY KEY (`business_profile_id`),
  ADD KEY `business_profile_id` (`business_profile_id`),
  ADD KEY `Business_Email` (`Business_Email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `client_quotation`
--
ALTER TABLE `client_quotation`
  ADD PRIMARY KEY (`client_quotation_id`);

--
-- Indexes for table `fs_staff`
--
ALTER TABLE `fs_staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`industry_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`like_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notifications_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `ur_login`
--
ALTER TABLE `ur_login`
  ADD PRIMARY KEY (`ur_login_id`),
  ADD KEY `ur_email` (`ur_email`),
  ADD KEY `ur_pas` (`ur_pas`);

--
-- Indexes for table `user_education`
--
ALTER TABLE `user_education`
  ADD PRIMARY KEY (`education_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `user_skills`
--
ALTER TABLE `user_skills`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indexes for table `working_experience`
--
ALTER TABLE `working_experience`
  ADD PRIMARY KEY (`experience_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advert_posts`
--
ALTER TABLE `advert_posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `business_profile`
--
ALTER TABLE `business_profile`
  MODIFY `business_profile_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `client_quotation`
--
ALTER TABLE `client_quotation`
  MODIFY `client_quotation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fs_staff`
--
ALTER TABLE `fs_staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `industry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `like_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notifications_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ur_login`
--
ALTER TABLE `ur_login`
  MODIFY `ur_login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_education`
--
ALTER TABLE `user_education`
  MODIFY `education_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `profile_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_skills`
--
ALTER TABLE `user_skills`
  MODIFY `skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `working_experience`
--
ALTER TABLE `working_experience`
  MODIFY `experience_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
