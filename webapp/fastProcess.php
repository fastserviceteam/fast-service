<?php 

try
{
    $conn = new PDO("mysql:host=localhost;dbname=bandacomputer_fastservice_db", "root", "");
    
	// set the PDO error mode to exception
	
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
			$conn->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
    
    }
	
catch(PDOException $e)

		{
		
			echo "could not connect: " . $e->getMessage();
	
				}	



		/*----------Creating account---------------- */
	
	if(isset($_GET['userEduc'])){
		
		$educ_level = $_GET['educ_level'];
		
			$institution = $_GET['institution'];
			
				$award = $_GET['award'];
				
					$grade = $_GET['grade'];
					
						$year = $_GET['year'];
					
							$ur_email = $_GET['ur_email'];
		
if ((isset($educ_level) && !empty($educ_level)) && (isset($institution) && !empty($institution)) && (isset($award) && !empty($award))


		&& (isset($grade) && !empty($grade)) && (isset($year) && !empty($year))) {		
	
	
	$sqx = "INSERT INTO user_education (educ_level, institution, award, grade, year, ur_email) VALUES (?, ?, ?, ?, ?, ?)";
	
		$query = $conn->prepare($sqx);
	
			$qum = $query->execute(array($educ_level, $institution, $award, $grade, $year, $ur_email));  
	

	if($query==true){
		
		
		echo 'success';
	}
	
	 elseif($query==false){
		
		
		echo 'error occurred';
	}
	
}
else {
	
	
echo 'fill all the fields';
}

	}
	
	if(isset($_GET['userWork'])){
		
		$title = $_GET['title'];
		
			$company = $_GET['company'];
			
				$location = $_GET['location'];
				
					$from_date = $_GET['from_date'];
					
						$end_date = $_GET['end_date'];
					
							$ur_email = $_GET['ur_email'];
		
if ((isset($title) && !empty($title)) && (isset($location) && !empty($location)) && (isset($from_date) && !empty($from_date))


		&& (isset($end_date) && !empty($end_date)) && (isset($ur_email) && !empty($ur_email))) {		
	
	
	$sqx = "INSERT INTO working_experience (title, company, location, from_date, end_date, ur_email) VALUES (?, ?, ?, ?, ?, ?)";
	
		$query = $conn->prepare($sqx);
	
			$qum = $query->execute(array($title, $company, $location, $from_date, $end_date, $ur_email));  
	

	if($query==true){
		
		
		echo 'success';
	}
	
	 elseif($query==false){
		
		
		echo 'error occurred';
	}
	
}
else {
	
	
echo 'fill all the fields';
}

	}
	
	
	//adding skills 
	
	if(isset($_GET['userSkill'])){
		
		$skill = $_GET['skill'];
					
			$ur_email = $_GET['ur_email'];
		
if ((isset($skill) && !empty($skill)) && (isset($ur_email) && !empty($ur_email))) {		
	
	
	$sqx = "INSERT INTO user_skills (skill, ur_email) VALUES (?, ?)";
	
		$query = $conn->prepare($sqx);
	
			$qum = $query->execute(array($skill, $ur_email));  
	

	if($query==true){
		
		
		echo 'success';
	}
	
	 elseif($query==false){
		
		
		echo 'error occurred';
	}
	
}
else {
	
	
echo 'fill all the fields';
}

	}
	
	
	//adding skills 
	
	if(isset($_GET['userLang'])){
		
		$language = $_GET['language'];
		
			$read_efficiency = $_GET['read_efficiency'];
			
				$write_efficiency = $_GET['write_efficiency'];
					
					$ur_email = $_GET['ur_email'];
		
if ((isset($read_efficiency) && !empty($read_efficiency)) && (isset($write_efficiency) && !empty($write_efficiency))) {		
	
	
	$sqx = "INSERT INTO languages (language, read_efficiency, write_efficiency, ur_email) VALUES (?, ?, ?, ?)";
	
		$query = $conn->prepare($sqx);
	
			$qum = $query->execute(array($language, $read_efficiency, $write_efficiency, $ur_email));  
	

	if($query==true){
		
		
		echo 'success';
	}
	
	 elseif($query==false){
		
		
		echo 'error occurred';
	}
	
}
else {
	
	
echo 'fill all the fields';
}

	}
	
	
	//adding jobs
	
	if(isset($_GET['jobSave'])){
		
		$business_profile_id = $_GET['business_profile_id'];
		
			$job_vacancy = $_GET['job_vacancy'];
			
				$location = $_GET['location'];
					
					$job_type = $_GET['job_type'];
					
						$job_function = $_GET['job_function'];
						
		$salary = $_GET['salary'];
		
			$application_mode = $_GET['application_mode'];
			
				$date_posted = $_GET['date_posted'];
				
					$deadline = $_GET['deadline'];
					
						$description = $_GET['description'];
						
							$tobe_posted = $_GET['tobe_posted']; 
	
		
if ((isset($business_profile_id) && !empty($business_profile_id))) {		
	
	
	$sqx = "INSERT INTO jobs (business_profile_id, job_vacancy, location, job_type, job_function, salary, application_mode, date_posted, deadline, description, tobe_posted) 
	
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
		$query = $conn->prepare($sqx);
	
			$qum = $query->execute(array($business_profile_id, $job_vacancy, $location, $job_type, $job_function, $salary, $application_mode, $date_posted, $deadline, $description, $tobe_posted));  
	

	if($query==true){
		
		
		echo 'success';
	}
	
	 elseif($query==false){
		
		
		echo 'error occurred';
	}
	
}
else {
	
	
echo 'fill all the fields';
}

	}
	
	
	
	//function to manage image upload
	
if(isset($_POST['savePost'])){ 
	
	function GetImageExtension($imagetype)

     {
       if(empty($imagetype)) return false;
       switch($imagetype)
       {
           case 'image/bmp': return '.bmp';
           case 'image/gif': return '.gif';
           case 'image/jpeg': return '.jpg';
           case 'image/png': return '.png';
		   case 'application/pdf': return '.pdf';
           default: return false;
	 } } 

	 if (!empty($_FILES["image_name"]["name"])) {
		$file_name=$_FILES["image_name"]["name"];
		$temp_name=$_FILES["image_name"]["tmp_name"];
		$imgtype=$_FILES["image_name"]["type"];
		$ext= GetImageExtension($imgtype);
		$imagename=date("d-m-Y")."-".time().$ext;
		$target_path = "posts/".$imagename;
		
		
			$description = $_POST[ 'description' ];
			
				$date_post = date('Y-m-d');
					
					$business_profile_id = $_POST[ 'business_profile_id' ];
				
		
			if(move_uploaded_file($temp_name, $target_path)) {
				
																					

$sql = "INSERT INTO advert_posts (business_profile_id, description, image_name, image, date_post) 
	
		VALUES (?, ?, ?, ?, ?)";

				$sq = $conn->prepare($sql);
				
				$querry= $sq->execute( array( $business_profile_id, $description, $file_name, $target_path, $date_post) );
								
					
					
		if($querry==true)
		{
		
		header('location:adverts.php?afterPost');
		}	
		}}
		
		
}
	
?>