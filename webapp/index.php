<?php  require 'connect.php'; ?>


<!DOCTYPE html> 

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FAST SERVICE </title>
    <!-- Google Font -->
    <link href='#' rel='stylesheet' type='text/css'>

    <link href='#' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

		<link rel="stylesheet" href="css/bootstrap.css">

				 <script src="js/jquery.min.js"></script>
 
 
					<script src="js/bootstrap.min.js"></script>
					
						<script src="saveScript.js"></script>
		

 
    <!-- Owl Carousel Assets -->
	
    <link href="css/owl.carousel.css" rel="stylesheet">
	
    <link href="css/owl.theme.css" rel="stylesheet">


    <!-- Pixeden Icon Font -->
	
    <link rel="stylesheet" href="css/Pe-icon-7-stroke.css">

    <!-- Font Awesome -->
	
    <link href="css/font-awesome.min.css" rel="stylesheet">
	
	 <!-- w3 css -->
	 
    <link href="css/w3.css" rel="stylesheet"> 


    <!-- PrettyPhoto -->
    <link href="css/prettyPhoto.css" rel="stylesheet"> 
    
    <!-- Favicon -->
	
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

    <!-- Style -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
	
    <!-- Responsive CSS -->
	
    <link href="css/responsive.css" rel="stylesheet">
 
</head>

<body>
    <!-- 
    <div class="spn_hol">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>   -->

 <!-- =========================
     START ABOUT US SECTION
============================== -->
    <section class="header parallax home-parallax page" id="HOME">
        
        <div class="section_overlay">
            <nav class="navbar  navbar-fixed-top" role="navigation" style="background-color:#212121;border-bottom:2px solid khaki;color:cyan;">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="images/logo2.png" class="w3-circle" alt="Logo">
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <!-- NAV -->
                            <li><a href="#HOME">HOME</a> </li>
                            <li><a href="#ABOUT">ABOUT US </a> </li>
                            <li><a href="#FEATURES">FEATURES</a></li>
                            <li><a href="#SCREENS">BUSINESS ADVERTS</a> </li>
                            <li><a href="#CONTACT">CONTACT </a> </li>
							 <li><a href="#DOWNLOAD">HELP</a> </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container- -->
            </nav>

            <div class="w3-container" style="height:700px;padding-top:2em;">
                
                   <div class="col-lg-4">
                        
                             <!-- LOGO -->
								
                       <img class="w3-circle" width="60px" height="60px"  src="images/logo2.png" alt="" /> <font color="#388E3C" size="3vw"><b> 
							
							FAST SERVICE </b></font>
                        
                 </div>
					
					
					
	<div class="col-lg-8">
					
                       
		<a class="tuor btn wow fadeInRight" href="#ABOUT"><b>Take a tour </b> <i class="fa fa-angle-down"></i></a>
		
                            
             <a class="w3-btn w3-teal" data-toggle="modal" data-target="#myModal">REGISTER FREE</a>
								
				 <a class="w3-btn w3-teal" onclick="document.getElementById('id01').style.display='block'">LOGIN</a>
									 
                
                           
                   </div>
					
                
                
				
            </div>
        </div>
    </section>

    <!-- END HEADER SECTION -->

	
	 <!-- START OF REGISTRATION MODAL -->

	 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
	  
        <div class="modal-header">
		
          <button type="button" class="close" data-dismiss="modal">&times;</button>
		  
          <h4 class="modal-title"><center><b> <img width="85" height="45" style="border:1px solid teal;" class="w3-circle" src="images/logo.jpg" alt=""> &nbsp; &nbsp;<font color="blue">SIGNUP FOR FREE</font></b></center></h4>
		  
        </div>
		
        <div class="modal-body">
		
         
		 <div class="panel-body">
		
		  <div id="result"> </div> 
		  
		  
						<form id="reg_form">
						
							<div class="form-group mb-lg">
							
								<label>Name <font color="red">*</font></label>
								
								<input  name="full_name" type="text" class="form-control input-lg" required/>
								
							</div>
							
							<div class="form-group mb-lg">
							
								<label>Phone number <font color="red">*</font></label>
								
								<input name="phone_no" type="number" class="form-control input-lg" required/>
							</div>

							<div class="form-group mb-lg">
							
								<label>E-mail Address <font color="red">*</font></label>
								
								<input name="ur_email" type="email" class="form-control input-lg" required/>
								
							</div>

							<div class="form-group mb-none">
							
								<div class="row">
								
									<div class="col-sm-6 mb-lg">
									
										<label>Password <font color="red">*</font></label>
										
										<input name="ur_pas" type="password" class="form-control input-lg" required/>
										
									</div>
									
									<div class="col-sm-6 mb-lg">
									
										<label>Password Confirmation <font color="red">*</font></label>
										
										<input name="pwd_confirm" type="password" class="form-control input-lg" required/>
										
									</div>
									
								</div>
								
							</div>

							<div class="row">
							
								<div class="col-sm-8">
								
									<div class="checkbox-custom checkbox-default">
									
										<input id="AgreeTerms" name="agreeterms" type="checkbox"/>
										
										<label for="AgreeTerms">I agree with <a href="#">terms of use</a></label>
										
									</div>
									
								</div>
								
								<div class="col-sm-4 text-right">
								
									<button type="button" id="submit_btn" class="btn btn-success">Submit</button>
									
								</div>
								
								
							</div>
							
							
						</form>
					</div>
		 
		 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" style="background-color:teal;color:#FFF;" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

	 <!-- END OF REGISTRATION MODAL -->
	 
	  
	  
	  
	  
	  
	  
	  
	  <!-- START OF LOGIN MODAL-->
	<script src="login/jquery-1.11.3-jquery.min.js"></script>
	<script src="login/validation.min.js"></script>
	<script src="login/loginScript.js"></script>
	   <div id="id01" class="w3-modal">	   
    <div class="w3-modal-content w3-card-8 w3-animate-zoom w3-round" style="max-width:400px">
      <div class="w3-center"><br>	
        <span onclick="document.getElementById('id01').style.display='none'" class="w3-closebtn w3-hover-red w3-container w3-padding-8 w3-display-topright" title="Close Modal">&times;</span>
        
				</div> 
					<form class="w3-container" role="form" method="post" action="" id="loginform">
	  
				<div id="error"></div> 
					<div class="w3-section">
				  <label><b><span class="fa fa-user"></span>&nbsp;&nbsp;User Email</b></label> 
					<input class="w3-input w3-border w3-margin-bottom w3-round" type="text" placeholder="Enter user email" name="ur_email" required autofocus >
					<label><b><span class="fa fa-lock"></span> Password</b></label> 
				  <input class="w3-input w3-border w3-round" type="password" placeholder="Enter Password" name="pword" required> 
				
				<button class="w3-btn-block w3-teal w3-section w3-padding w3-round" name="submit-login"  id="submit-login"   type="submit">
				<span class="" id="a"> Login</span> 
				</button>
          
				<input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Remember me 
				</div>
				</form> 
      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey w3-round">
        
		<div  id="reset_form" style="display:none">
					<div class=" ">
						<div class="panel panel-primary">
							<div class="panel-body">
						  <button type="button" onclick="closex();" class="btn btn-sm btn-danger  bold pull-right">X</button> 
					 <div class="text-center">
										  <span id="show_success">
										  <h3><i class="fa green fa-lock fa-3x"></i></h3>
										  <h2 class="text-center">Forgot Password?</h2>
										   </span> 
											<form class="form">
												<fieldset>
												  <div class="form-group">
													<div class="input-group">
													  <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
													  
													  <input id="getemail" name="getemail" placeholder="email address" autocomplete="off" class="form-control" type="email" oninvalid="setCustomValidity('Please enter a valid email address!')" onchange="try{setCustomValidity('')}catch(e){}" required="">
													</div>
												  </div>
												  <div class="form-group">
													<button id="resetpass" onclick="requestlink();" name="resetpass"  type="button" class="btn btn-lg btn-primary btn-block">Get Password Reset Link</button>
												  </div>
												</fieldset>
											  </form>
										</div>
									</div>
								</div>
							</div>
						</div>
		
		
		
			<button onclick="document.getElementById('id01').style.display='none'" type="button" class="btn-xs w3-button w3-round w3-pink">Cancel</button>
			<span class="w3-right w3-padding w3-hide-small w3-round"><b>Forgot <a  onclick="forgot();" href="#">password ?</b></a></span>
      
					</div>
				</div>
			</div>
								
		 <!-- END OF LOGIN MODAL -->						
		 <!-- START OF FORGOT PASSWORD MODAL -->	
 
 <script>
					function forgot() {
						$('#reset_form').fadeIn(); 
						$('#loginform').hide(); 
					}
					function closex() {
						$('#reset_form').hide(); 
						$('#loginform').slideDown(); 
					}
			function requestlink(){
				if(document.getElementById('getemail').value !=''){
				document.getElementById("resetpass").innerHTML = '<i class="fa fa-spinner fa-spin "></i>  Processing ...';
			 	var getemail ='getemail='+ document.getElementById('getemail').value;
				$.ajax({
					type: "POST",
					url: "login/resetpass.php",
					async: true,
					data: getemail, 
					cache: false, 
					success: function(data){						
						show_success("new", data); 
						document.getElementById("resetpass").innerHTML = 'Get Password Reset Link';
					}
				}); 
					function show_success(type, msg){ $('#show_success').html(msg);}			
				}else{
					alert('Please Enter email address');
				}				
			}
					</script>		 
		 <!-- END OF LOGIN MODAL -->						
		
 <!-- =========================
     START ABOUT US SECTION
============================== -->


    <section class="about page" id="ABOUT">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- ABOUT US SECTION TITLE-->
                    <div class="section_title">
                        <h2>About Us</h2>
                        <p>   Fast Service is a platform that has been developed to provide fast search for services and products of your choice from different 
						
						vendors in the nearest place at a your convenient time
						
						</p>
						
						
                    </div>
                </div>

            </div>
        </div>
        <div class="inner_about_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                        <!-- PHONE -->
                            <img src="images/about_iphone.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6  wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
                        <!-- TITLE -->
                        <div class="inner_about_title">
                            <h2>Why we are the first choice <br> for you</h2>
                            <p>This platform provides  24/7 update on the available products, services to clients and Vendors in their nearest location</p>
                        </div>
                        <div class="inner_about_desc">

                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                                <!-- ICON -->
                                <div><i class="pe-7s-timer"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>Users</h3>
                                <p>Users will enjoy all time supply of services from potential vendors in their area of selection and also vendors will get
								an opportunity to provide services to  a large number clients in their area of reach.</p>
                            </div>
                            <!-- END SINGLE DESC -->


                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1.5s">
                                <!-- ICON -->
                                <div><i class="pe-7s-target"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>Working hours</h3>
                                <p>Fast service platform is available to connect clients and vendors at any time of the day.</p>
                            </div>
                            <!-- END SINGLE DESC -->


                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="2s">
                                <!-- ICON -->
                                <div><i class="pe-7s-stopwatch"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>Usage</h3>
                                <p>Simply <a href="#">Login</a> or <a href="#">Register</a> an account now to request for a service or to view business requests from your potential clients.</p>
                            </div>
                            <!-- END SINGLE DESC -->

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="video_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeftBig">
                    <!-- VIDEO LEFT TITLE -->
                        <div class="video_title">
                            <h2>Best Business platform <br>in the market</h2>
                            <p>We help you expand your Business clientele through helping you to identify clients that could be looking for your service. </p>
                        </div>
                        <div class="video-button">
                            <!-- BUTTON -->
                            <a class="btn btn-primary btn-video" href="#FEATURES" role="button">Features</a>
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInRightBig">
                         <!-- VIDEO -->
                        <div class="video">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/HJUm-MwLJMg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->




 <!-- =========================
     START TESTIMONIAL SECTION------------------------//commented out developers here
============================== -->
<!--
    <section id="TESTIMONIAL" class="testimonial parallax">
        <div class="section_overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow bounceInDown">
                        <div id="carousel-example-caption-testimonial" class="carousel slide" data-ride="carousel">
                            <!-- Indicators ->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-caption-testimonial" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-caption-testimonial" data-slide-to="1"></li>
                                <li data-target="#carousel-example-caption-testimonial" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides ->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <!-- IMAGE ->
                                                <img src="images/client_1.png" alt="">
                                                <div class="testimonial_caption">
                                                   <!-- DESCRIPTION ->  
                                                    <h2>Dan Harmon</h2>
                                                    <h4><span>SR. UI Designer,</span> Dcrazed</h4>
                                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                 <!-- IMAGE ->
                                                <img src="images/client_2.png" alt="">
                                                <div class="testimonial_caption">
                                                <!-- DESCRIPTION -> 
                                                    <h2>Allie Kingsley</h2>
                                                    <h4><span>SR. Content Strategist,</span> Designscrazed</h4>
                                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <!-- IMAGE -->
                                                <img src="images/client_3.png" alt="">
                                                <div class="testimonial_caption">
                                                <!-- DESCRIPTION -> 
                                                    <h2>Joel Mchale</h2>
                                                    <h4><span>SR. Developer,</span> Treehouse</h4>
                                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
	-->



    <!-- END TESTIMONIAL SECTION -->



<!-- =========================
     START FEATURES
============================== -->
    <section id="FEATURES" class="features page">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- FEATURES SECTION TITLE -->
                    <div class="section_title wow fadeIn" data-wow-duration="1s">
                        <h2>Features</h2>
                        <p> fast service provides a great features that will prove our users with a greatest experience when using out platform, these include:-.</p>
                    </div>
                    <!-- END FEATURES SECTION TITLE -->
                </div>
            </div>
        </div>

        <div class="feature_inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 right_no_padding wow fadeInLeft" data-wow-duration="1s">
                        <!-- FEATURE -->

                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-like"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>USER FRIENLY DESIGN<span>/</span></h3>
                            <p>Fast Service Website has been developed modified and simplified user interfaces that will be used to perform different tasks</p>
                        </div>

                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-science"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>RUN ON ALL DEVICES<span>/</span></h3>
                            <p> Fast Service Website has been designed with ability to run all devices with different sizes and operating platforms</p>
                        </div>
                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-look"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>MINIMAL DATA USAGE<span>/</span></h3>
                            <p> Fast Service App has light but quality media (pictures) that have an optimised data consumption </p>
                        </div>
                        <!-- END SINGLE FEATURE -->

                    </div>
					
                    <div class="col-md-4">
					
                        <div class="feature_iphone">
						
                            <!-- FEATURE PHONE IMAGE -->
							
                            <img class="wow bounceIn" data-wow-duration="1s" src="images/iPhone02.png" alt="">
							
                        </div>
						
                    </div>
					
                    <div class="col-md-4 left_no_padding wow fadeInRight" data-wow-duration="1s">

                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-monitor"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><span>/</span>OPERATES 24/7</h3>
                            <p>The website and mobile application will be accessed for 24 hours</p>
                        </div>
                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-phone"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><span>/</span>FREE ANDROID APP</h3>
							
                            <p>  Fast Service App has a mobile application that runs on Android phones and its can downloaded freely on play store</p>
							
                        </div>
                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-gleam"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><span>/</span>SUPPORT CENTER</h3>
                            <p>Fast Service App has customer support center that attends to the users questions and other requests</p>
                        </div>
                        <!-- END SINGLE FEATURE -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END FEATURES SECTION -->



<!-- =========================
     START CALL TO ACTION--------------------------------commented out by timo to be introduced later
============================== ->
    <div class="call_to_action">
        <div class="container">
            <div class="row wow fadeInLeftBig" data-wow-duration="1s">
                <div class="col-md-9">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et olore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                </div>
                <div class="col-md-3">
                    <a class="btn btn-primary btn-action" href="#" role="button">Download Now</a>
                </div>
            </div>
        </div>
    </div>

    <!-- END CALL TO ACTION -->


<!-- =========================
     Start APPS SCREEN SECTION
============================== ->
    <section class="apps_screen page" id="SCREENS">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 wow fadeInBig" data-wow-duration="1s">
                        <!-- APPS SCREEN TITLE ->
                        <div class="section_title">
                            <h2>Screenshots</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>                           
                        </div>
                        <!-- END APPS SCREEN TITLE ->
                    </div>
                </div>
            </div>
			===========================================================end of comment out=======================
						-->
        <div class="screen_slider">
            <div id="demo" class="wow bounceInRight" data-wow-duration="1s">
                <div id="owl-demo" class="owl-carousel">

                    <!-- APPS SCREEN IMAGES -->
                    <div class="item">
                    <a href="images/screens/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone05.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone05.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone06.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone06.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone07.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone07.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone08.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone08.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone09.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone09.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- ENS APPS SCREEN -->





<!-- =========================
     Start FUN FACTS
============================== -->


    <section class="fun_facts parallax">
        <div class="section_overlay">
            <div class="container wow bounceInLeft" data-wow-duration="1s">
                <div class="row text-center">
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-cloud-download"></i>
                            <h2><span  class="counter_num">699</span> <span>+</span></h2>
                            <p>Downloads</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-look"></i>
                            <h2><span  class="counter_num">1999</span> <span>+</span></h2>
                            <p>Likes</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-comment"></i>
                            <h2><span  class="counter_num">199</span> <span>+</span></h2>
                            <p>Feedbacks</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-cup"></i>
                            <h2><span  class="counter_num">10</span> <span>+</span></h2>
                            <p>Awards</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- END FUN FACTS -->




<!-- =========================
     START DOWNLOAD NOW 
============================== -->
    <section class="download page" id="DOWNLOAD">
        
		<!--
		<div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- DOWNLOAD NOW SECTION TITLE ////->
                    <div class="section_title">
                        <h2>download now</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                    </div>
                    <!--END DOWNLOAD NOW SECTION TITLE \\\\\\\\\\\\\\\->
                </div>
            </div>
        </div>
		-->

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="download_screen text-center wow fadeInUp" data-wow-duration="1s">
                        <img src="images/download_screen.png" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="available_store">
            <div class="container  wow bounceInRight" data-wow-duration="1s">
                <div class="col-md-6">
                    <div class="available_title">
                        <h2>Available on</h2>
                        <p>Mobile version is available for Android users </p>
                    </div>
                </div>

                <!-- DOWNLOADABLE STORE -->
                <div class="col-md-6">
                    <div class="row">
                        <a href="">
                            <div class="col-md-4 no_padding">
                                <!--<div class="single_store">
                                    <i class="fa fa-apple"></i>
                                    <div class="store_inner">
                                        <h2>iOS</h2>
                                    </div>
                                </div>-->
                            </div>
                        </a>
                        <div class="col-md-4 no_padding">
                            <a href="">
                                <div class="single_store">
                                    <i class="fa fa-android"></i>
                                    <div class="store_inner">
                                        <h2>ANDROID</h2>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 no_padding">
                            <a href="">
							<!--
                                <div class="single_store last">
                                    <i class="fa fa-windows"></i>
                                    <div class="store_inner">
                                        <h2>WINDOWS</h2>
                                    </div>
                                </div>
								-->
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END DOWNLOADABLE STORE -->
            </div>
        </div>
    </section>
    <!-- END DOWNLOAD -->

<!-- =========================
     START CONTCT FORM AREA
============================== -->
    <section class="contact page" id="CONTACT">
        <div class="section_overlay">
            <div class="container">
                <div class="col-md-10 col-md-offset-1 wow bounceIn">
                    <!-- Start Contact Section Title-->
                    <div class="section_title">
                        <h2>Get in touch</h2>
                        <p>You can feel free to get in tough with us through the form bellow our customer service agents will give be able to give you feed back as soon as possible </p>
                    </div>
                </div>
            </div>

            <div class="contact_form wow bounceIn">
			
                <div class="container">


                <!-- CONTACT FORM starts here, Go to contact.php and add your email ID, thats it.-->    
                      <div id="result_msg"> </div> 
		  
		  
						<form  id="msg_form">
						
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="sender_names" placeholder="Name">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                                <input type="text" class="form-control" name="subject" placeholder="Subject">
								
                            </div>


                            <div class="col-md-8">
							
                                <textarea class="form-control" name="sent_text" rows="25" cols="10" placeholder="  Message Texts..."></textarea>
								
								<input type="hidden" class="form-control" name="msg_date" value="<?php echo date('Y-m-d') ?>">
								
                                <button type="button" class="btn btn-block btn-success" onclick="sendMsg()" >SEND MESSAGE</button>
                            </div>
							
							
							
                        </div>
                    </form>
                    <!-- END FORM --> 
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft">
                        <div class="social_icons">
                            <ul>
                                <li target="_blank"><a href="https://web.facebook.com/FAST-Service-357041338282888/"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-dribbble"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-behance"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-youtube-play"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END CONTACT -->

<!-- =========================
     Start Subscription Form 
============================== -->



    <section class="subscribe parallax subscribe-parallax" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
        <div class="section_overlay wow lightSpeedIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">

                        <!-- Start Subscribe Section Title -->
                        <div class="section_title">
                            <h2>OUR LOCATION</h2>
                            <p>Our physical address is  Banda kyambogo Road, Nakawa Division, Kampala uganda.</p>
                        </div>
                        <!-- End Subscribe Section Title -->
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row  wow lightSpeedIn">
                    <div class="col-md-6 col-md-offset-3">
                        <!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
                        <div class="subscription-success"></div>
                        <div class="subscription-error"></div>
                        <!-- Check this topic on how to add email subscription list, http://kb.mailchimp.com/lists/signup-forms/host-your-own-signup-forms -->
                       <!-- ====================commented out the leave a comment thing by timo

					   <form id="mc-form" action="https://designscrazed.us8.list-manage.com/subscribe/post" method="POST" class="subscribe_form">                         
                        <input type="hidden" name="u" value="6908378c60c82103f3d7e8f1c">
                        <input type="hidden" name="id" value="8c5074025d">
                            <div class="form-group">
                                <!-- EMAIL INPUT BOX =========->
                                <input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" class="required email form-control" id="mce-EMAIL" placeholder="Enter Email Address" value="" >                                 
                            </div>
                                <!-- SUBSCRIBE BUTTON ============->
                            <button type="submit" class="btn btn-default subs-btn">Leave Comment</button>
                        </form>
						-->

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- END SUBSCRIPBE FORM -->


<!-- =========================
     FOOTER 
============================== -->

    <section class="copyright">
        <h2></h2>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copy_right_text">
                    <!-- COPYRIGHT TEXT -->
                        <p>Copyright &copy; 2018. All Rights Reserved.</p>        
                        <p>Created by <a href="#">Fast Service Co Ltd</a></p>
                    </div>
                     
                </div>

                <div class="col-md-6">
                    <div class="scroll_top">
                        <a href="#HOME"><i class="fa fa-angle-up"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END FOOTER -->


<!-- =========================
     SCRIPTS 

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.ajaxchimp.langs.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
   
============================== -->
 <script src="js/script.js"></script>
 
 <script>

 //send message from index page 

		
    function  sendMsg() {
 
        // using serialize function of jQuery to get all values of form
        var serializedData = $("#msg_form").serialize();
 
        // Variable to hold request
        var request;
        // Fire off the request to process_registration_form.php
		
		var resultMessage = $('#result_msg');
		
        request = $.ajax({
            url: "fastMsg.php",
            type: "post",
            data: serializedData
        });
		
		//clearing the form fields 
		
		$('#msg_form')[0].reset();
 
        // Callback handler that will be called on success
		
        request.done(function(jqXHR, textStatus, response) {
			
            // you will get response from your php page (what you echo or print)
             // show successfully for submit message
			 
            $("#result_msg").html('<div class="alert alert-success alert-dismissible">  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <center> Your mesage has been sent..<br> Reply will be sent on ur email shortly</center></div>');
        });
 
        // Callback handler that will be called on failure
		
        request.fail(function(jqXHR, textStatus, errorThrown) {
			
            // Log the error to the console
            // show error
            $("#result_msg").html('There is some error while submit');
			
            console.error(
			
                "The following error occurred: " +
                
				textStatus, errorThrown
            );
        });
 
        return false;
 
    }
	
	</script>

</body>
</html>