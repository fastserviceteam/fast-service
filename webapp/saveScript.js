
	
	//js for creating account 
	
	
	$(document).ready(function() {
		
    $("#submit_btn").click(function() {
 
        // using serialize function of jQuery to get all values of form
        var serializedData = $("#reg_form").serialize();
 
        // Variable to hold request
        var request;
        // Fire off the request to process_registration_form.php
		
		var resultMessage = $('#result');
		
        request = $.ajax({
            url: "fastCodes.php",
            type: "post",
            data: serializedData
        });
		
		//clearing the form fields 
		
		$('#reg_form')[0].reset();
 
        // Callback handler that will be called on success
		
        request.done(function(jqXHR, textStatus, response) {
			
            // you will get response from your php page (what you echo or print)
             // show successfully for submit message
			 
            $("#result").html('<div class="alert alert-success"> <strong>Success!</strong> Your account has been created. <a href="index.php">'

			+'<button class="w3-btn w3-teal w3-round" >Login Now</button></a></div>');
        });
 
        // Callback handler that will be called on failure
		
        request.fail(function(jqXHR, textStatus, errorThrown) {
			
            // Log the error to the console
            // show error
            $("#result").html('There is some error while submit');
			
            console.error(
			
                "The following error occurred: " +
                
				textStatus, errorThrown
            );
        });
 
        return false;
 
    });
	
	
	
});




