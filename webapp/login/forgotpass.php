	<?php

	require_once('PHPMailer/src/Exception.php');
	require_once('PHPMailer/src/PHPMailer.php');
	require_once('PHPMailer/src/SMTP.php');

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	//Load Composer's autoloader
	require 'PHPMailer/PHPMailerAutoload.php';

	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
//	try {
		//Server settings
		$mail->SMTPDebug = 2;                                 // debugging: 1 = errors and messages, 2 = messages only
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'stvo.org@gmail.com';                 // SMTP username
		$mail->Password = 'stvo.com';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;      // or  465                             // TCP port to connect to

		//Recipients
		$mail->setFrom('stvo.org@gmail.com', 'Forgot Password ');
		$mail->addAddress($_POST['getemail'], 'Reset Password');     // Add a recipient
		
		//Attachments
	  //  $mail->addAttachment('attachments/research.docx');         // Add attachments
	  // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'Forgot Password';
		$mail->Body    = 'Hello Admin, <b>a request has been sent to resert your password
		<br><br><br><br> 
		<br />
		<a href="127.0.0.1/primary/forgotpass/reset.php">click here to reset your password</a>
		<br />';
		
		
		$mail->AltBody = 'Hello Admin, <b>a request has been sent to resert your password
		<br><br><br><br> 
		<a href="127.0.0.1/primary/forgotpass/reset.php">click here to reset your password</a>
		';
		$mail->send();
//		echo 'Message has been sent';
//	} catch (Exception $e) {
//		echo 'Message could not be sent.';
//	}
	?>