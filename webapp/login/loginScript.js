	
	$('document').ready(function(){   
	 $("#loginform").validate({
      rules: {
			password: {
			required: true,
			},
			ur_email: {
            required: true
            },
	   },
       messages:  {
            pword:{
				required: "Please enter your password"
                },
				ur_email: "Please enter your email",
       },
	   submitHandler: submitForm	
       });
	  
	  function submitForm(){		
			var r_data = $("#loginform").serialize();
			$.ajax({				
			type : 'POST',
			url  : 'login/login_process.php',
			data : r_data,
			beforeSend: function(){	
				$("#error").fadeOut();
				$("#a").html('connecting <img src="login/lodning/connecting.gif" style="height:30px"></img>');
			},
			success :  function(response){						
					if(response=="Success_URL_noErroR"){									
						$("#a").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Signing in .. <img src="login/lodning/connecting.gif" style="height:30px"></img>');
						setTimeout('window.location.href = "main/dashboard.php"; ',3000);
					}
					else{
						$("#error").fadeIn().delay(4000).fadeOut(); 						
						$("#tost").hide().delay(4800).fadeIn(); 						
						$("#error").html(response);
						$("#submit-login").html('Login');
					}
			  }
			});
				return false;
		} 
	});
