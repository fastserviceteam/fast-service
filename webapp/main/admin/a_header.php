
<?php 
 
 session_start();
 

 //searching for user details by the use of session email
 
 require '../config.php';
 
	/* if(isset($_SESSION['ur_email'])){
	
	$mail = $_SESSION['ur_email'];

		$sq = "SELECT p.names, p.ur_email, p.phone_no, u.ur_type FROM ur_login u 
		
		INNER JOIN user_profile p 
		
			ON u.ur_email = p.ur_email WHERE u.ur_email = '{$mail}'";
			
				$exe = $conn->prepare($sq);
				
					$exe->execute();
					
							$ro = $exe->fetch(PDO::FETCH_ASSOC);
							
							
			   $ruq = "SELECT *FROM jobs WHERE approved = ''";
			
				$exeq = $conn->prepare($ruq);
				
					$exeq->execute();  
					
							$cox = $exeq->fetchAll(PDO::FETCH_ASSOC); 
							
	}
		
			*/ ?>	
			
<!doctype html> 

<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Fast Service APP</title>

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="../assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="../assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">
		
		<!-- fast styles customised -->
		
		<link rel="stylesheet" href="../fastStyles.css">

		<!-- Head Libs -->
		<script src="../assets/vendor/modernizr/modernizr.js"></script>
		
		<script type="text/javascript" src="../jssor/jssor.js"></script>
		
    <script type="text/javascript" src="../jssor/jssor.slider.js"></script>
		
		<!-- Head Libs -->
		<!-- <link rel="stylesheet" href="../serveplus/css/w3.css" /> -->
		
 <script type="text/javascript" src="../jssor/jquery-1.9.1.min.js"></script>
 

<script type="text/javascript" src="saveProf.js"></script>

<script type="text/javascript" src="admin_script.js"></script>

<script type="text/javascript" src="../assets/javascripts/ui-elements/examples.notifications.js"></script>
 
 <script src="../assets/javascripts/ui-elements/examples.charts.js"></script>
 
 <style>
 
	#panel {
	
    display: none;
}

</style>

	</head>
	
	<body>
	
		<section class="body">

			<!-- start: header -->
			<header class="header" >
			
				<div style="width:100%;background-color:#34495e;height:100%;float:left;">
				
					<a href="#" class="logo">
					
						<img src="../../images/logo.jpg"  class="img-circle" height="35" style="border:1px solid #00838F;"/>
						
						<font size="5.8vw" color="cyan" font face="constantia">&nbsp; <b>FAST SERVICE </b></font>
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
					
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				
			
				<!-- start: search & user box -->
				<div class="header-right" style="background-color:#34495e;">
			 
					<ul class="notifications">
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
								<i class="fa fa-tasks"></i>
								<span class="badge" style="background-color:orange;color:#FFF;">3</span>
							</a>
			
							<div class="dropdown-menu notification-menu large">
							
								<div class="notification-title">
								
									<span class="pull-right label label-default">3</span>
									
									My Tasks
								</div>
			
								<div class="content">
									<ul>
										<li>
											<p class="clearfix mb-xs">
												<span class="message pull-left">Generating Sales Report</span>
												<span class="message pull-right text-dark">60%</span>
											</p>
											<div class="progress progress-xs light">
												<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
											</div>
										</li>
			
										<li>
											<p class="clearfix mb-xs">
												<span class="message pull-left">Importing Contacts</span>
												<span class="message pull-right text-dark">98%</span>
											</p>
											<div class="progress progress-xs light">
												<div class="progress-bar" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;"></div>
											</div>
										</li>
			
										<li>
											<p class="clearfix mb-xs">
												<span class="message pull-left">Uploading something big</span>
												<span class="message pull-right text-dark">33%</span>
											</p>
											<div class="progress progress-xs light mb-xs">
												<div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%;"></div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
								<i class="fa fa-envelope"  title="Messages"></i>
								<span class="badge">4</span>
							</a>
			
							<div class="dropdown-menu notification-menu">
								<div class="notification-title">
									<span class="pull-right label label-default">230</span>
									Messages
								</div>
			
								<div class="content">
									<ul>
									
		<?php /*
		
		$wt = "SELECT *from messages ORDER BY msg_date DESC LIMIT 4";
	
			$ext = $conn->prepare($wt);
					
				$ext->execute();
						
					$von = $ext->fetchAll( PDO::FETCH_ASSOC);
					
						foreach($von as $wox){ */
					
						?>
						
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<?php  //echo $wox['sender_names'] ?>
												</figure>
												<span class="title"><?php  // echo $wox['msg_date'] ?></span>
												<span class="message"> <?php  // echo $wox['subject'] ?> </span>
											</a>
										</li>
										
										
						<?php //} ?>
									</ul>
			
									<hr />
			
									<div class="text-right">
										<a onclick="loadMail()"  style="cursor:pointer;" class="view-more">View All</a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
								<i class="fa fa-bell"></i>
								<span class="badge" style="background-color:#F50057;color:#FFF;">3</span>
							</a>
			
							<div class="dropdown-menu notification-menu">
								<div class="notification-title">
									<span class="badge">4</span>
									Alerts
								</div>
			
								<div class="content">
									<ul>
										<li>
											<a href="#" class="clearfix" onclick="loadJobs()">
												<div class="image">
													<i class="fa fa-graduation-cap bg-danger"></i>
												</div>
												<span class="title">Job requests</span>
												<span class="message"><?php //echo $exeq->rowCount(); ?> pending</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<div class="image">
													<i class="fa fa-gears bg-warning"></i>
												</div>
												<span class="title">Advert requests</span>
												<span class="message">15 minutes ago</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<div class="image">
													<i class="fa fa-dollar bg-success"></i>
												</div>
												<span class="title">Subscriptions this month</span>
												<span class="message">10/10/2014</span>
											</a>
										</li>
									</ul>
			
									<hr />
			
									<div class="text-right">
										<a href="#">FastService App Corporation</a>
									</div>
								</div>
							</div>
						</li>
					</ul>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
					
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="../assets/images/!logged-user.jpg" class="img-circle" />
							</figure>
							<div class="profile-info"  data-lock-name="Banxie" data-lock-email="fastservice.com">
								<span class="name"><font color="cyan">  <?php  // echo $ro['names']; ?></font></span>
								<span class="role"><font color="#FFF">Web developer </font></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
						
							<ul class="list-unstyled">
							
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="userProfile.php"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="../../login/logout.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				</div>
				<!-- end: search & user box -->
			</header> 
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
					
						<div class="sidebar-title">
							
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div> 
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-active">
										<a onclick="loadHome()" style="cursor:pointer;">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<li>
										<a onclick="loadMail()" style="cursor:pointer;">
										
											<span class="pull-right label label-primary">182</span>
											<i class="fa fa-envelope" aria-hidden="true"></i>
											<span>Messenger</span>
										</a>
									</li>
									
									
		<li>
			
			<a onclick="loadBusiness()" style="cursor:pointer;">
				
				<i class="fa fa-money" aria-hidden="true"></i>
					
					<span>Business Management</span>
						
						</a>
										
							</li>
				
									
			<li>
					
					<a onclick="loadCat()" style="cursor:pointer;">
					
						<i class="fa fa-globe" aria-hidden="true"></i>
											
							<span>Products and Services</span>
								
								</a>
										
									</li>
									
			<li>
					
					<a onclick="loadBill()" style="cursor:pointer;">
					
						<i class="fa fa-money" aria-hidden="true"></i>
											
							<span>Billing</span>
								
								</a>
										
									</li>
									
		<li>
					
					<a onclick="loadDoc()" style="cursor:pointer;">
					
						<i class="fa fa-money" aria-hidden="true"></i>
											
							<span id="usr">System users</span>
								
								</a>
										
									</li>
									 
									
									<li>
										<a onclick="loadJobs()" style="cursor:pointer;">
										
											<span class="pull-right label label-primary">23</span>
											<i class="fa fa-edit" aria-hidden="true"></i>
											<span>Jobs</span>
										</a>
									</li>
									
									
					<li>
										<a onclick="loadAdverts()" style="cursor:pointer;">
										
											<span class="pull-right label label-primary">23</span>
											<i class="fa fa-edit" aria-hidden="true"></i>
											<span>Adverts Mgt</span>
										</a>
									</li>						
			<li>
					
					<a onclick="loadAlerts()" style="cursor:pointer;">
					
						<i class="fa fa-info" aria-hidden="true"></i>
											
							<span>System Alerts</span>
								
								</a>
										
									</li>				
									
				<li>
					<a onclick="loadSurveys()" style="cursor:pointer;">
						
							<i class="fa fa-hand-o-up" aria-hidden="true"></i>
								<span>Market Survey</span>
									</a>
										</li>
									
									
	
									
								</ul>
							</nav>
							
							
							
							
				
						 
				
							
						</div>
				
					</div>
				
				</aside>
				
				<!-- end: sidebar -->
				
				

				<section  class="content-body" style="padding-top:0px;">  
				 
				 



					
						<div id="homepage"></div>
						
					<div id="messenger"></div>
						
				
				<div id="callBiz"></div>
						
						
				<div id="demo"></div> 
					
	
	<p id="services"></p> 
	
	
		<div id="bills"></div> 
	
			<div id="jobs"></div> 
	
				<div id="adverts"></div>
				
					<div id="alerts"></div>
					
						<div id="surveys"></div>
	<!-- pre-loader -->
	
	
	<div id="wait" style="display:none;position:absolute;top:50%;left:50%;padding:2px;">
	
	
	<img src='../../images/126.gif' width="64" height="64" /><br>Loading...</div>
	
	<!-- end of pre-loader -->
	
	
	<script>
	
	//loading home page 
	
	
function loadHome() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("homepage").innerHTML = this.responseText;
	   $("#services").hide(1000);
	    $("#demo").hide(1000);
		 $("#homepage").fadeIn(2500);
			$("#messenger").hide(1000);
			$("#callBiz").hide(1000);
			 $("#bills").hide(1000);
				 $("#jobs").hide(1000);
					$("#adverts").hide(1000);
					
					$("#alerts").hide(1000);
				
						$("#surveys").hide(1000);
    }
  };
  xhttp.open("GET", "home.php", true);
  xhttp.send();
}



//loading messages 
	
	
function loadMail() {
	
	$(document).ajaxStart(function(){
    $("#wait").css("display", "block");
  });
  $(document).ajaxComplete(function(){
    $("#wait").css("display", "none");
  });
  
  
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("messenger").innerHTML = this.responseText;
	   $("#services").hide(1000);
	    $("#demo").hide(1000);
		 $("#homepage").hide(1000);
			$("#callBiz").hide(1000);
		  $("#messenger").fadeIn(2500);
		  
		  $("#bills").hide(1000);
		  
			 $("#jobs").hide(1000);
			 
			 $("#adverts").hide(1000);
			 
			 $("#alerts").hide(1000);
				
						$("#surveys").hide(1000);
		  
    }
  };
  xhttp.open("GET", "a_mailbox-folder.php", true);
  xhttp.send();
}

	
	//loading business management
	
	
function loadBusiness() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("callBiz").innerHTML = this.responseText;
	   $("#services").hide(1000);
	    $("#demo").hide(1000);
		 $("#homepage").hide(1000);
		  $("#messenger").hide(1000);
		   $("#callBiz").fadeIn(2500);
			 $("#bills").hide(1000);
				 $("#jobs").hide(1000);
					$("#adverts").hide(1000);
						
				$("#alerts").hide(1000);
				
						$("#surveys").hide(1000);
    }
  };
  xhttp.open("GET", "a_business.php", true);
  xhttp.send();
}


	//loading users page 
	
function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("demo").innerHTML = this.responseText;
	   $("#services").hide(1000);
	    $("#demo").fadeIn(2500);
		$("#homepage").hide(1000);
		$("#messenger").hide(1000);
		$("#callBiz").hide(1000);
		 $("#bills").hide(1000);
		  $("#jobs").hide(1000);
		  $("#adverts").hide(1000);
		  	$("#alerts").hide(1000);
				$("#surveys").hide(1000);
    }
  };
  xhttp.open("GET", "a_users.php", true);
  xhttp.send();
}


//loading categories and services page 
	
	
function loadCat() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("services").innerHTML = this.responseText;
	  $("#demo").hide(1000);
	   $("#services").fadeIn(2500);
	   $("#homepage").hide(1000);
	   $("#messenger").hide(1000);
	   $("#callBiz").hide(1000);
	   $("#bills").hide(1000);
	    $("#jobs").hide(1000);
		$("#adverts").hide(1000);
		$("#alerts").hide(1000);
		$("#surveys").hide(1000);
    }
  };
  xhttp.open("GET", "a_categories.php?firstOpen", true);
  xhttp.send();
}




function loadInd(showindust) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
			
      document.getElementById("services").innerHTML = this.responseText;
	  
	  $("#demo").hide(1000);
	   $("#services").fadeIn(2500);
	   $("#homepage").hide(1000);
	   $("#messenger").hide(1000);
	   $("#callBiz").hide(1000);
	   $("#bills").hide(1000);
	    $("#jobs").hide(1000);
		$("#adverts").hide(1000);
		$("#alerts").hide(1000);
		$("#surveys").hide(1000);
    }
  };
  xhttp.open("GET", "a_categories.php?viewCat="+ showindust.getAttribute("data-id"), true);
  xhttp.send();
}								
	


	//loading billing page
	
	
function loadBill() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("bills").innerHTML = this.responseText;
	  $("#demo").hide(1000);
	   $("#services").hide(1000);
	   $("#homepage").hide(1000);
	   $("#messenger").hide(1000);
	   $("#callBiz").hide(1000);
	    $("#bills").fadeIn(2500);
		 $("#jobs").hide(1000);
		 $("#surveys").hide(1000);
		 	$("#alerts").hide(1000);
		 $("#adverts").hide(1000);
    }
  };
  xhttp.open("GET", "billing.php", true);
  xhttp.send();
}


		//loading jobs page
	
	
function loadJobs() {
	
	
  
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("jobs").innerHTML = this.responseText;
	  $("#demo").hide(1000);
	   $("#services").hide(1000);
	   $("#homepage").hide(1000);
	   $("#messenger").hide(1000);
	   $("#callBiz").hide(1000);
	    $("#bills").hide(1000);
		
		$("#adverts").hide(1000);
		
		$("#alerts").hide(1000);
		
			$("#surveys").hide(1000);
		
		 $("#jobs").fadeIn(2500);
    }
  };
  xhttp.open("GET", "a_jobs.php", true);
  xhttp.send();
}


//loading adverts page
	
	
function loadAdverts() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("adverts").innerHTML = this.responseText;
	  $("#demo").hide(1000);
	   $("#services").hide(1000);
	   $("#homepage").hide(1000);
	   $("#messenger").hide(1000);
	   $("#callBiz").hide(1000);
	    $("#bills").hide(1000);
		
		 $("#jobs").hide(1000);
		 
		 $("#alerts").hide(1000);
		 
		 $("#surveys").hide(1000);
		 
			 $("#adverts").fadeIn(2500);
    }
  };
  xhttp.open("GET", "a_adverts.php", true);
  xhttp.send();
}




//loading alerts page
	
	
function loadAlerts() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("alerts").innerHTML = this.responseText;
	  $("#demo").hide(1000);
	   $("#services").hide(1000);
	   $("#homepage").hide(1000);
	   $("#messenger").hide(1000);
	   $("#callBiz").hide(1000);
	    $("#bills").hide(1000);
		
		 $("#jobs").hide(1000);
		 
			 $("#adverts").hide(1000);
			 
			 $("#surveys").hide(1000);
			 
				$("#alerts").fadeIn(2500);
    }
  };
  xhttp.open("GET", "a_alerts.php", true);
  xhttp.send();
}


//loading surveys page
	
function loadSurveys() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("surveys").innerHTML = this.responseText;
	  $("#demo").hide(1000);
	   $("#services").hide(1000);
	   $("#homepage").hide(1000);
	   $("#messenger").hide(1000);
	   $("#callBiz").hide(1000);
	    $("#bills").hide(1000);
		
		 $("#jobs").hide(1000);
		 
			 $("#adverts").hide(1000);
			 
				$("#alerts").hide(1000);
				
					$("#surveys").fadeIn(2500);
    }
  };
  xhttp.open("GET", "a_marketSurvey.php", true);
  xhttp.send();
}

</script>
		
		
		
		<!-- displaying the registration for a user-->	
		
		<script>
		
			function showReg(){
			 

				$("#profForm").fadeIn(2500);
				
				$("#logDetail").fadeIn(2500);
				
				$("#hideTable").hide(1000);
				
			 
			}
			
			function closeReg(){
			 

				$("#profForm").hide(1000);
				
				$("#logDetail").hide(1000);
				
				$("#hideTable").fadeIn(2500);
				
			 
			}
			
	
//running model for adding services	
 
		function showAll(modalBootstrap) {
	
			var cat_id = modalBootstrap.getAttribute("data-id");
  
				var indust_name = modalBootstrap.getAttribute("data-industry");
				
					var cat_name = modalBootstrap.getAttribute("data-cat");
	  
					document.getElementById("showId").value = cat_id;
	 
						document.getElementById("showName").innerHTML = indust_name;
						
						document.getElementById("showCat").innerHTML = cat_name;
						
						}
						
		function refresh(){
			
			
			setTimeout(function(){
      
			location.reload();
  
					},2000);
		}
						
						
		
		//code for live search for business and linked to a php file gethint.php 
		
function showResult(str) {
  if (str.length==0) { 
    document.getElementById("livesearch").innerHTML="";
    document.getElementById("livesearch").style.border="0px";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("livesearch").innerHTML=this.responseText;
     
    }
  }
  xmlhttp.open("GET","getHint.php?q="+str,true);
  xmlhttp.send();
}


//calling the div to add bills for business subscription 
		
			function addBizBill(){
			 

				$("#bizBill").fadeIn(2500);
				
				$("#hideBiz").hide(1000);
				$("#hideAdvert").hide(1000);
				$("#hideJob").hide(1000);
			 
			}

//closing the prof div
			function shutdiv(){
			 
				$("#bizBill").hide(1000);
				
				$("#hideBiz").show(1000);
				$("#hideAdvert").show(1000);
				$("#hideJob").show(1000);
			  
			}
			
	//opening the advert bill 
	
		function addAdvertBill(){
			 

				$("#advertBill").fadeIn(2500);
				
				$("#hideBiz").hide(1000);
				$("#hideAdvert").hide(1000);
				$("#hideJob").hide(1000);
			 
			}
			
	//closing the advert bill div
	
			function shutAdvert(){
			 
				$("#advertBill").hide(1000);
				
				$("#hideBiz").show(1000);
				$("#hideAdvert").show(1000);
				$("#hideJob").show(1000);
			  
			}
			
			
	//opening the job bill 
	
		function addJobBill(){
			 

				$("#jobBill").fadeIn(2500);
				
				$("#hideBiz").hide(1000);
				$("#hideAdvert").hide(1000);
				$("#hideJob").hide(1000);
			 
			}
			
	//closing the job bill div
	
			function shutJob(){
			 
				$("#jobBill").hide(1000);
				
				$("#hideBiz").show(1000);
				$("#hideAdvert").show(1000);
				$("#hideJob").show(1000);
			  
			}
			


<!-- function for showing sender !-->
	
	
 
		function showReceiver(zoomOutIn5) {
  
				var msg_receiver = zoomOutIn5.getAttribute("data-receiver");
				
					var message_id = zoomOutIn5.getAttribute("data-msgid");
				
	 
						document.getElementById("msgReceiver").value = msg_receiver;
						
							document.getElementById("msgeid").value = message_id;
						
						
						
						
						}				
							
							
				 <!-- function for displaying full message !-->
	
	
 
		function showMsg(zoomOutIn6) {
  
				var msg_text = zoomOutIn6.getAttribute("data-msgText");
				
				var msg_date = zoomOutIn6.getAttribute("data-msgDate");
				
				var msg_sender = zoomOutIn6.getAttribute("data-msgSender");
				
				var msg_subject = zoomOutIn6.getAttribute("data-msgSubject");
				
	 
						document.getElementById("msgText").innerHTML = msg_text;
						
						document.getElementById("msgDate").innerHTML = msg_date;
						
						document.getElementById("msgSender").innerHTML = msg_sender;
						
						document.getElementById("msgSubject").innerHTML = msg_subject;
						
						
						}	
							
							

	
	function showJobDetails(zoomOutIn7) {
		
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
			
      document.getElementById("displayJobs").innerHTML = this.responseText;  

    }
  };
  xhttp.open("GET", "jobDetails.php?viewJob="+ zoomOutIn7.getAttribute("data-jobid"), true);
  xhttp.send();
}	


//calling page to display zoomed advert image

		function getAdvertImage(zoomOutIn9) {
		
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
			
      document.getElementById("advertImage").innerHTML = this.responseText;  

    }
  };
  xhttp.open("GET", "advertImage.php?viewImage="+ zoomOutIn9.getAttribute("data-advertid"), true);
  xhttp.send();
}


//calling page to display staff and edit details

		function getStaff(zoomOutIn10) {
		
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
			
      document.getElementById("editStaff").innerHTML = this.responseText;  

    }
  };
  xhttp.open("GET", "advertImage.php?updateStaff="+ zoomOutIn10.getAttribute("data-staffid"), true);
  xhttp.send();
}
	
	</script>
			
		
		