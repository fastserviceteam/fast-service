<?php 
    // require files
    require_once "_require-file.php";

    // init class
    $_gObj = new globalClass()

 ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome to Fast Service</title>
    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="_assets/images/fs-logo.png" />
    <!-- ================== FONT ================== -->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- ================== STYLING ================== -->
    <!-- Bootstrap core CSS -->
    <link href="_assets/plugins/bootstrap4.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="_assets/plugins/fontawesome/css/all.css">
    <!-- animate css -->
    <link href="_assets/plugins/animate/animate.min.css" rel="stylesheet">
    <!-- owl carousel slides -->
    <link rel="stylesheet" href="_assets/plugins/owlcarousel/assets/owl.carousel.css">
    <link rel="stylesheet" href="_assets/plugins/owlcarousel/assets/owl.theme.default.min.css">
    <!-- fast service styling -->
    <link href="_assets/css/fast-service-styling.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="?">
                <img src="_assets/images/fs-logo.png" alt="Fast Service" class=""> <span id="navBar-brand-title">Fast Service</span>
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                <!--
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#page-top">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#about">About</a>
                    </li>
                -->
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#how-section">How it works</a>
                    </li>
                <!--   
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#features-section">Features</a>
                    </li>
                -->
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#contactus">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" data-login-register-form href="#page-top">Register</a>
                    </li>
                    <!--
            <li class="nav-item dropdown">
              <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" id="navBarLoginDropDown">Log In <span class="caret"></span></a>
              <ul class="dropdown-menu form-wrapper" aria-labelledby="navBarLoginDropDown">
                <li>
                  <form role="form" autocomplete="off">
                    <h4 class="hint-text">Sign in</h4>
                    <div class="form-group">
                      <input type="email" class="form-control" placeholder="email address" required="required">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control" placeholder="Password" required="required">
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="Login">
                    <div class="form-footer">
                      <a href="#">Forgot Your password?</a>
                    </div>
                  </form>
                </li>
              </ul>
              
            </li>
            -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- Header -->
    <header class="masthead">
        <div class="container d-flex h-100 align-items-center">
            <div class="row" id="signupform-title-row">
                <!-- title -->
                <div class="col-md-7 col-12" id="ff-left-title">
                    <div class="text-center pb-4 ff-title-wrapper">
                        <h1 class="my-0 text-capitalize mb-5 text-center wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">Fast Service</h1>
                        <h4 class="text-white-50 mt-2 mb-2 text-center wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">Your reliable service provision site</h4>
                        <!--<a href="#about" class="btn btn-primary js-scroll-trigger">Get Started</a>-->
                        <div class="thim-click-to-bottom">
                            <a href="#about" class="js-scroll-trigger">
                                <i class="fas fa-long-arrow-alt-down" aria-hidden="true"></i>
                            </a>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-md-1">
                </div>

                <!-- access forms -->
                <div class="col-md-4 col-12" id="register-login-form-div">

                    <!-- password reset form -->
                    <?php
                        if(isset($_GET['confirmedEmailAddress']) && isset($_GET['stringpath']) && isset($_GET['autogenerated_int'])) {

                            $emailAddress       = $_GET['confirmedEmailAddress']; // email address
                            $verificationToken  = $_GET['stringpath']; // verification token
                            $loginId            = $_GET['autogenerated_int']; // `ur_login` tbl pkey

                            // check password reset parameters
                            $checkParameters = $_gObj->validatePasswordreset($emailAddress, $verificationToken, $loginId);

                            if ($checkParameters == false) {
                                
                            } else {
                                ?>

                    <!-- reset password form -->
                    <fieldset id="fast-service-passwordreset-fieldset" class="access-forms-toggle">

                        <div id="login-fieldset-container">
                            <legend class="text-white text-center">Reset Password</legend>
                            <form role="form" id="dataForm-account-passwordReset-client-form">
                                <div class="w3-section">
                                    
                                    <!-- hidden fields -->
                                    <input type="hidden" value="<?php echo $emailAddress; ?>" id="input_pwdReset_eMAILaDDRESS" name="input_pwdReset_eMAILaDDRESS">
                                    <input type="hidden" value="<?php echo $verificationToken; ?>" id="input_pwdReset_tOKEnStrInG" name="input_pwdReset_tOKEnStrInG">
                                    <input type="hidden" value="<?php echo $loginId; ?>" id="input_pwdReset_aUtoGEnInTERger" name="input_pwdReset_aUtoGEnInTERger">
                    
                                    
                                    <!-- visible fields -->
                                    <div class="form-group">
                                        <label for="input_pwdReset_email_address">Email Address</label>
                                        <div id="input_pwdReset_email_address" class="text-info"><?php echo $emailAddress; ?></div>
                                    </div>


                                    <div class="form-group">
                                        <label for="input_pwdReset_password">New Password</label>
                                        <input type="password" class="form-control" placeholder="enter new Password" name="input_pwdReset_password" id="input_pwdReset_password" autocomplete="off" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="input_pwdReset_Confirmpassword">Confirm Password</label>
                                        <input type="password" class="form-control" placeholder="confirm new Password" name="input_pwdReset_Confirmpassword" id="input_pwdReset_Confirmpassword" autocomplete="off" required>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col">
                                            <button type="submit" class="btn btn-outline-info btn-flat btn-lg" name="submit-NewPasword-btnName" id="submit-NewPasword-btnId">
                                                Reset
                                            </button>                                            
                                        </div>
                                    </div>

                                </div>
                            </form>                            
                        </div>
                        <!-- reset password-fieldset -->

                        <div class="dataAlert-pwdReset"></div>
                    </fieldset>
                    <!-- reset password form -->


                                <?php
                            }
 
                        } else {
                    ?>

                    <!-- login form -->
                    <fieldset id="fast-service-login-fieldset" class="access-forms-toggle wow slideInRight" data-wow-duration="1.5s">

                        <div id="login-fieldset-container">
                            <legend class="text-white text-center">Login</legend>
                            <form role="form" id="dataForm-account-login-client-form">
                                <div class="w3-section">
                                    <div class="form-group">
                                        <label for="input_login_email_address">Email Address</label>
                                        <input type="email" class="form-control" placeholder="enter your email address" name="input_login_email_address" id="input_login_email_address" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="input_login_password">Password</label>
                                        <input type="password" class="form-control" placeholder="enter Password" name="input_login_password" id="input_login_password" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" value="1" id="input_remember_me" name="input_remember_me">
                                            <label class="form-check-label aria-hint-text" for="input_remember_me">Remember me </label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col">
                                            <button type="submit" class="btn btn-outline-success btn-flat btn-lg" name="submit-login" id="submit-login">
                                                Login
                                            </button>                                            
                                        </div>
                                        <div class="col">
                                            <a href="#forgotPasswordModal" data-toggle="modal" class="text-info">Forgot password?</a>
                                        </div>
                                    </div>

                                </div>
                            </form>                            
                        </div>
                        <!-- login-fieldset -->

                        <div class="dataAlert-login"></div>
                    </fieldset>
                    <!-- signin form -->

                    <!-- signup form -->
                    <fieldset id="fast-service-signup-fieldset" class="element-display-none access-forms-toggle">
                        <legend class="text-white text-center">Register</legend>
                        <form role="form" id="dataForm-account-registration-client-form">
                            <div class="form-group">
                                <input name="input_fullNames" id="input_fullNames" type="text" class="form-control input-md" placeholder="enter your full names" aria-describedby="fullNamesExample" autocomplete="off" maxlength="30">
                                <small id="fullNamesExample" class="aria-hint-text">eg. John Doe</small>
                            </div>
                            <div class="form-group">
                                <input name="input_phoneNumber" id="input_phoneNumber" type="text" class="form-control input-md" placeholder="enter your phone number" aria-describedby="phonenumberHelp" autocomplete="off" maxlength="13">
                                <small id="phonenumberHelp" class="aria-hint-text">eg. 0777xxxxxx</small>
                            </div>
                            <div class="form-group">
                                <input name="input_valid_emailAddress" id="input_valid_emailAddress" type="email" class="form-control input-md" placeholder="enter your valid email address" maxlength="64" aria-describedby="emailAddressHelp" autocomplete="off">
                                <small id="emailAddressHelp" class="aria-hint-text">eg. johndoe@something.com</small>
                            </div>
                            <div class="form-group">
                                <input name="input_strong_password" type="password" class="form-control input-md" placeholder="enter your password" aria-describedby='passwordHelp' id="input_strong_password" autocomplete="off">
                                <small id="passwordHelp" class="aria-hint-text">Mininum of 8 characters</small>
                            </div>

                            <div class="form-group">
                                <input name="input_confirm_strong_password" type="password" class="form-control input-md" placeholder="confirm your password" id="input_confirm_strong_password" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="AgreeTerms" name="agreeterms" value="yes">
                                    <label class="form-check-label aria-hint-text" for="AgreeTerms">I agree with <a href="#" class="text-danger">TERMS OF USE</a> </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" id="registrationSubmitBtn" class="btn btn-outline-success btn-flat" disabled>
                                    Register
                                </button>
                            </div>
                        </form>
                    </fieldset>
                    <!-- signup form -->

                <?php } ?>


                </div>
                <!-- access forms -->
            </div>
            <!-- row -->

        </div>
        <!-- container -->
    </header>
    <!-- Header -->
    <!-- About Section -->
    <section id="about" class="about-section text-center grey-bg">
        <div class="container">

            <div class="row">
                <div class="col-12 mx-auto">
                    <h2 class="title text-center mb-4">About <strong>Us</strong></h2>
                    <p>
                        Fast Service is a mobile and web application which brings fast access to daily services and products, from a wide range of vendors nearer, to you at your own convenient time and location.
                    </p>

                </div>
            </div>

        </div>
    </section>
    <!-- about us -->

    <!-- how section -->
    <section id="how-section" class="the-how-section">
        <div class="container">

            <div class="row">
                <div class="col-12 mx-auto">
                    <h2 class="title mb-4 mt-0 py-0 text-center">How it works</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 col-12">
                    <h4 class="text-center text-muted mb-4">Why us?</h4>

                        <div class="owl-carousel owl-theme">

                            <div class="item ser-bottom">
                                <div class="card mb-4">
                                    <h4 class="card-header text-white">Why we are the first choice for you</h4>
                                    <div class="card-body">
                                        <p><i class="fas fa-asterisk" aria-hidden="true"></i>
                                            Our Site/App provides a 24/7 update on the available products, vendors in the nearest place
                                        </p>
                                    </div>
                                </div>                                

                                <div class="card">
                                    <h4 class="card-header text-white">Services</h4>
                                    <div class="card-body">
                                        <p><i class="fas fa-asterisk" aria-hidden="true"></i>
                                            We have a wide range of service providers ready to cater for all your needs
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="item ser-bottom">
                                <div class="card mb-4">
                                    <h4 class="card-header text-white">Support</h4>
                                    <div class="card-body">
                                        <p><i class="fas fa-asterisk" aria-hidden="true"></i>
                                            We are ready to listen to all your questions and inquiries and assist you in any way possible.
                                        </p>
                                    </div>
                                </div>

                                <div class="card">
                                    <h4 class="card-header text-white">Convenience</h4>
                                    <div class="card-body">
                                        <p><i class="fas fa-asterisk" aria-hidden="true"></i>
                                            Order for what you want and when you want it at your own convenience with no hurries and worries.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <!-- video ; we shall create a youtube channel and just place url here -->
                <div class="col-12 col-md-5 mx-auto" id="ff-video-div">
                    <h4 class="text-center text-muted mb-4">Our short getting started clip</h4>
                    <div class="embed-responsive embed-responsive-4by3">
                        <iframe src="https://www.youtube.com/embed/-l8Ss4bAk_E" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                <!--
                    <video width="100%" height="auto" controls>
                        <source src="_assets/media/mov_bbb.mp4" type="video/mp4">
                        <source src="_assets/media/mov_bbb.ogg" type="video/ogg">
                        Your browser does not support the video tag. Please update or use another one.
                    </video>
                -->
                </div>
            </div>

        </div>
    </section>
    <!-- how section -->


    <!-- features section -->
    <!--
    <section id="features-section" class="the-features-section">
        <div class="container">

            <div class="row">
                <div class="col-12 mx-auto">
                    <h2 class="text-white title mb-5 text-center">Features</h2>
                </div>
            </div>
            <div class="row text-white my-2">
                <div class="col-6 mx-auto features-list text-center">
                    <span class="fas fa-thumbs-up" aria-hidden="true"></span>
                    <h4>USER FRIENLY DESIGN</h4>
                    <p>
                        No hussle and tussle when navigating. Feel free to check our captivating user interface.
                    </p>
                </div>
                <div class="col-6 mx-auto features-list text-center">
                    <span class="fab fa-android" aria-hidden="true"></span>
                    <h4>RUNS ON ANDROID</h4>
                    <p>
                        Our mobile application has been designed to currently support all android devices with API 16 and above.
                    </p>
                </div>
            </div>
            <div class="row text-white mt-5">
                <div class="col-6 mx-auto features-list text-center">
                    <span class="fas fa-tachometer-alt" aria-hidden="true"></span>
                    <h4>MINIMAL DATA USAGE</h4>
                    <p>
                        We have tried all ways possible to limit data usage combined with smooth operation such that you find what you're looking without consuming alot of your internet data.
                    </p>
                </div>
                <div class="col-6 mx-auto features-list text-center">
                    <span class="fas fa-clock" aria-hidden="true"></span>
                    <h4>SUPPORT CENTER</h4>
                    <p>
                        Our support staff are always available and ready to listen and help you in any way they can. You can also drop by our centre. Our doors are always open during working days and hours. We're looking forward to receiving you.
                    </p>
                </div>
            </div>

        </div>
    </section>
-->
    <!-- features section -->


    <!-- Contact Section -->
    <section id="contactus" class="contact-section">
        <!-- maps -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h2 class="title text-center">Contact <strong>Us</strong></h2>
                <!--
                    <div id="gmap" class="contact-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.7462085154675!2d32.63347151519876!3d0.
                34448076407525763!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x177db9790d227fd5%3A0xf9d6646bcaf9e09d!2sBanda+
                Computer+Centre!5e0!3m2!1sen!2sug!4v1468244061727" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen>
                        </iframe>
                    </div>
                -->

                </div>
            </div>

        </div>
        <!-- container - fluid -->

        <!-- contact details and form -->
        <div class="container my-5">

            <div class="row">

                <div class="col-md-7 mb-3 col-12 mb-md-0 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">

                    <h2 class="title text-center">Get In Touch</h2>

                    <form role="form" id="dataFastService-SendEmail">
                        <div class="form-group">
                            <label for="dataIdText_client_name">Name</label>
                            <input type="text" class="form-control" placeholder="your name" id="dataIdText_client_name" name="dataNameText_client_name">
                        </div>

                        <div class="form-group">
                            <label for="dataIdText_client_emailAddress">Email Address</label>
                            <input type="email" class="form-control" placeholder="your email address" id="dataIdText_client_emailAddress" name="dataNameText_client_emailAddress">
                        </div>

                        <div class="form-group">
                            <label for="dataIdText_client_subject">Subject</label>
                            <input type="text" class="form-control" placeholder="your subject" id="dataIdText_client_subject" name="dataNameText_client_subject">
                        </div>

                        <div class="form-group">
                            <label for="dataIdTextArea_client_message">Message</label>
                            <textarea name="dataNameTextArea_client_message" id="dataIdTextArea_client_message" cols="30" rows="10" class="form-control" aria-describedby="messageHelpText" maxlength="150" placeholder="your message"></textarea>
                            <small id="messageHelpText" class="aria-hint-text">150 characters ONLY</small>
                        </div>
                        
                        <input type="hidden" id="fast_service_msg_date" name="dataNameTextArea_client_msgDate" value="<?php echo date('Y-m-d') ?>">

                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-primary btn-flat btn-lg" name="dataNameButton_sendMessage" id="dataIdButton_sendMessage">Send</button>
                        </div>
                    </form>

                </div>

                <div class="col-md-5 mb-md-0 col-12 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">

                    <h2 class="title text-center">Contact Info</h2>

                    <div class="address-block mt-2">
                        <ul class="pb-2 pt-2">
                            <li>
                                BANDA COMMUNITY PREMISES Kyambogo road,
                            </li>
                            <li>
                                Next To Banda Police Post, Kampala Uganda.
                            </li>
                            <li>P.O Box 183, Kyambogo</li>
                            <li>Office: +256 (0) 700 000 000</li>
                            <li>Email: <a href="mailto:info@fastserviceug.com">info@fastserviceug.com</a> </li>
                            <li>Website: <a href="https://www.fastserviceug.com/" target="_blank">www.fastserviceug.com/</a></li>
                        </ul>
                    </div>

                    <h5 class="social-pages text-success">find us:-</h5>
                    <div class="social d-flex justify-content-left">
                        <a href="https://twitter.com/FASTSERVICE18" class="mr-2 twitter-link">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="https://web.facebook.com/FAST-Service-357041338282888" class="mx-2 facebook-link">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="https://www.youtube.com/channel/UCpMN3p8_ADr1KSsEmxqybYg?view_as=subscriber" class="mx-2 youtube-link">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </div>

                </div>
                <!-- col-md-5 mb-md-0 - right -->

            </div>

        </div>
    </section>
    <!-- contact us section -->


    <!-- Footer -->
    <footer class="small text-white-50">

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-12 footer-links">
                    <h3>Quick Links</h3>
                    <div class="strip"></div>
                    <ul class="footer-links-list">
                    <!--   
                        <li><i class="fas fa-long-arrow-alt-right"></i> <a href="#page-top" class="js-scroll-trigger">Home</a></li>
                        <li><i class="fas fa-long-arrow-alt-right"></i> <a href="#about" class="js-scroll-trigger"> About</a></li>
                        <li><i class="fas fa-long-arrow-alt-right"></i> <a href="#" class="js-scroll-trigger"> FAQ</a></li>
                        <li><i class="fas fa-long-arrow-alt-right"></i> <a href="#" class="js-scroll-trigger"> Career</a></li>
                    -->
                        <li><i class="fas fa-long-arrow-alt-right"></i> <a href="#contactus" class="js-scroll-trigger"> Contact</a></li>
                        <li><i class="fas fa-long-arrow-alt-right"></i> <a href="#the-how-section" class="js-scroll-trigger"> How it works</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-lg-4 col-6 footer-contact">
                    <h3>Facebook</h3>
                    <div class="strip"></div>
                    <div class="footer-addr">
                        <div class="fb-page" data-href="https://www.facebook.com/bandacomputercenter/" data-tabs="timeline" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/bandacomputercenter/" class="fb-xfbml-parse-ignore">
                                <a href="https://www.facebook.com/bandacomputercenter/">
                                    Banda Computer Centre
                                </a>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-6 footer-contact">
                    <h3>Twitter</h3>
                    <div class="strip"></div>
                    <div class="footer-addr">
                        <a class="twitter-timeline" data-theme="dark" data-height="200" data-link-color="#F5F8FA" href="https://twitter.com/bandacomputerc1?ref_src=twsrc%5Etfw">Tweets by bandacomputerc1
                        </a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                </div>

            </div>
        </div>
        <!-- top container -->

        <!-- copyright -->
        <div class="container-fluid text-center text-white mt-5 mb-0 pb-0" id="footer-copyright">
            <hr>
            Copyright &copy; Fast Service - <?php echo date('Y'); ?>
        </div>
        <!-- bottom container -->

    </footer>
    <!-- footer -->

    <!-- ================== SCRIPTS ================== -->
    <!-- JQuery -->
    <script src="_assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="_assets/plugins/bootstrap4.3/dist/js/bootstrap.min.js"></script>
    <!-- owlcarousel plugin -->
    <script src="_assets/plugins/owlcarousel/owl.carousel.min.js"></script>
    <script>
    $(function() {
            // Slideshow 4
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:2,
                nav:false,
                animateOut: 'slideOutDown',
                animateIn: 'flipInX',
                smartSpeed: 700,
                autoplay: 4000,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    800:{
                        items:1
                    },
                    1024:{
                        items:1
                    },
                    1200:{
                        items:1
                    }
                }
            });  

        });
    </script>
    <!-- wow -->
    <script src="_assets/plugins/jquery-easing/wow.min.js"></script>
    <!-- jQuery ease -->
    <script src="_assets/plugins/jquery-easing/jquery.easing.min.js"></script>
    <!-- notify -->
    <script src="_assets/plugins/pnotify/bootbox.min.js"></script>
    <script type="text/javascript">
        // Prevent dropdown menu from closing when click inside the form
        $(document).on("click", ".dropdown .dropdown-menu", function(e) {
            e.stopPropagation();
        });
        // You can also use "$(window).load(function() {"

    </script>


    <!-- fast service JavaScript -->
    <script src="_assets/js/fast-service-script.js"></script>
    <!-- =========================
         VIEWS 
    ============================== -->
    <?php require "_partials/_modal-views.php"; ?>
</body>

</html>