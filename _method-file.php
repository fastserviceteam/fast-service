<?php
    /* -------------------------------------------------- */
    /* method call file
    /* -------------------------------------------------- */

    require_once "_require-file.php";

  // --- >> instantiate object
    $_globalObj = new globalClass();


    // --- >> register new account from index.php << --- //
    if(
        isset($_POST['input_fullNames']) && 
        isset($_POST['input_phoneNumber']) && 
        isset($_POST['input_valid_emailAddress']) && 
        isset($_POST['input_strong_password']) && 
        isset($_POST['input_confirm_strong_password']))
    {
        $input_fullNames                = $_POST['input_fullNames'];
        $input_phoneNumber              = $_POST['input_phoneNumber'];
        $input_valid_emailAddress       = $_POST['input_valid_emailAddress'];
        $input_strong_password          = $_POST['input_strong_password'];
        $input_confirm_strong_password  = $_POST['input_confirm_strong_password'];
        
        $_globalObj->registerNewAccount($input_fullNames,  $input_phoneNumber,  $input_valid_emailAddress,  $input_strong_password, $input_confirm_strong_password);
    }

    // --- >> loging in << --- //
    if(
       isset($_POST['input_login_email_address']) &&
       isset($_POST['input_login_password']))
    {
        if (!isset($_POST['input_remember_me'])) 
        {
            $_POST['input_remember_me'] = NULL;
        }
        //$input_login_email_address  = $_POST['input_login_email_address']; // --- >> email address
        //$input_login_password       = $_POST['input_login_password']; // --- >> password
       //$input_remember_me          = $_POST['input_remember_me']; // --- >> remember me checkbox
        
        $_globalObj->login($_POST['input_login_email_address'],  $_POST['input_login_password'], $_POST['input_remember_me']);
    } //

    // --- >> reset password
    if (isset($_POST['registeredEmailAddressUpdatePassword'])) {
        $_globalObj->resetPassword(trim($_POST['registeredEmailAddressUpdatePassword']));
    } //

    // --- new password reset form
    if(
       isset($_POST['input_pwdReset_eMAILaDDRESS']) &&
       isset($_POST['input_pwdReset_tOKEnStrInG']) &&
       isset($_POST['input_pwdReset_aUtoGEnInTERger']) &&
       isset($_POST['input_pwdReset_password']) &&
       isset($_POST['input_pwdReset_Confirmpassword']))
    {
        $_globalObj->finalPasswordResetSubmission(trim($_POST['input_pwdReset_password']), trim($_POST['input_pwdReset_Confirmpassword']), $_POST['input_pwdReset_eMAILaDDRESS'], $_POST['input_pwdReset_tOKEnStrInG'], $_POST['input_pwdReset_aUtoGEnInTERger']);

    } //


    // --- >> send email from index.php << --- //
    if(
        isset($_POST['dataNameText_client_name']) && 
        isset($_POST['dataNameText_client_emailAddress']) && 
        isset($_POST['dataNameText_client_subject']) && 
        isset($_POST['dataNameTextArea_client_message']) && 
        isset($_POST['dataNameTextArea_client_msgDate']))
    {
        $fast_service_sender_names  = $_POST['dataNameText_client_name'];
        $fast_service_email         = $_POST['dataNameText_client_emailAddress'];
        $fast_service_subject       = $_POST['dataNameText_client_subject'];
        $fast_service_sent_text     = $_POST['dataNameTextArea_client_message'];
        $fast_service_msg_date      = $_POST['dataNameTextArea_client_msgDate'];
        
        $_globalObj->sendEmail($fast_service_sender_names,  $fast_service_email,  $fast_service_subject,  $fast_service_sent_text, $fast_service_msg_date);
    }
    
    
    // --- >> form feedbacks << --- //
    $_globalObj->_feedbacks();