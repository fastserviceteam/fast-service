/* ------------------------------------------
 * script for
 * function calls
 * 31 July, '19  
 * ------------------------------------------ */
let noti = 1;

// --- >> load clicked menu content script << --- //
function loadTemplateView(nav) {

    switch (nav) {
        case "main":
            // call loading function
            _loadContentFunction("a_home.php");
            // add active link class
            $("#nav_admin_main").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_inbox, #nav_admin_businessmgmt, #nav_admin_prodservices, #nav_admin_billing, #nav_admin_users, #nav_admin_jobs, #nav_admin_adverts, #nav_admin_sysalerts, #nav_admin_market_survey").removeClass("nav-active");

            break;

        case "mail":
            // call loading function
            _loadContentFunction("a_mailbox.php");
            // add active link class
            $("#nav_admin_inbox").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_businessmgmt, #nav_admin_prodservices, #nav_admin_billing, #nav_admin_users, #nav_admin_jobs, #nav_admin_adverts, #nav_admin_sysalerts, #nav_admin_market_survey").removeClass("nav-active");

            break;

        case "businessMgmt":
            // call loading function
            _loadContentFunction("a_business.php");
            // add active link class
            $("#nav_admin_businessmgmt").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_inbox, #nav_admin_prodservices, #nav_admin_billing, #nav_admin_users, #nav_admin_jobs, #nav_admin_adverts, #nav_admin_sysalerts, #nav_admin_market_survey").removeClass("nav-active");
            break;

        case "prod-serv":
            // call loading function
            _loadContentFunction("a_categories.php?firstOpen");
            // add active link class
            $("#nav_admin_prodservices").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_businessmgmt, #nav_admin_inbox, #nav_admin_billing, #nav_admin_users, #nav_admin_jobs, #nav_admin_adverts, #nav_admin_sysalerts, #nav_admin_market_survey").removeClass("nav-active");
            break;

        case "billing":
            // call loading function
            _loadContentFunction("billing.php");
            // add active link class
            $("#nav_admin_billing").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_businessmgmt, #nav_admin_prodservices, #nav_admin_inbox, #nav_admin_users, #nav_admin_jobs, #nav_admin_adverts, #nav_admin_sysalerts, #nav_admin_market_survey").removeClass("nav-active");
            break;

        case "users":
            // call loading function
            _loadContentFunction("a_users.php");
            // add active link class
            $("#nav_admin_users").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_businessmgmt, #nav_admin_prodservices, #nav_admin_billing, #nav_admin_inbox, #nav_admin_jobs, #nav_admin_adverts, #nav_admin_sysalerts, #nav_admin_market_survey").removeClass("nav-active");
            break;

        case "jobs":
            // call loading function
            _loadContentFunction("a_jobs.php");
            // add active link class
            $("#nav_admin_jobs").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_businessmgmt, #nav_admin_prodservices, #nav_admin_billing, #nav_admin_users, #nav_admin_inbox, #nav_admin_adverts, #nav_admin_sysalerts, #nav_admin_market_survey").removeClass("nav-active");
            break;

        case "advertsMgmt":
            // call loading function
            _loadContentFunction("a_adverts.php");
            // add active link class
            $("#nav_admin_adverts").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_businessmgmt, #nav_admin_prodservices, #nav_admin_billing, #nav_admin_users, #nav_admin_jobs, #nav_admin_inbox, #nav_admin_sysalerts, #nav_admin_market_survey").removeClass("nav-active");
            break;

        case "sysAlerts":
            // call loading function
            _loadContentFunction("a_alerts.php");
            // add active link class
            $("#nav_admin_sysalerts").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_businessmgmt, #nav_admin_prodservices, #nav_admin_billing, #nav_admin_users, #nav_admin_jobs, #nav_admin_adverts, #nav_admin_inbox, #nav_admin_market_survey").removeClass("nav-active");
            break;

        case "mktSurvey":
            // call loading function
            _loadContentFunction("a_marketSurvey.php");
            // add active link class
            $("#nav_admin_market_survey").addClass("nav-active");
            // remove other active link class
            $("#nav_admin_businessmgmt, #nav_admin_prodservices, #nav_admin_billing, #nav_admin_users, #nav_admin_jobs, #nav_admin_adverts, #nav_admin_sysalerts, #nav_admin_inbox").removeClass("nav-active");
            break;

        default:
    }

}

function _loadContentFunction(url) {
    // show preloader
    $("#content-view-div").html('<p class="text-center text-info"><i class="fa fa-spinner fa-spin fa-2x"></i></p>');

    // perform get request
    /*
    $.get(url, function(data){
        $("#content-view-div").fadeIn(2500).html(data);
    }, "html");   
    */

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            document.getElementById("content-view-div").innerHTML = this.responseText;

            $("#content-view-div").fadeIn(2500);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}


/**
 *   Auto reload call; after every 3s
 * ---------------------------------- */
setInterval(function() {
    pendingAlertCounter();
    checkNotificationCounter();
    adminMessages();
    adminMsgCount();
}, 3000);


// load admin messages in dropdown list
function adminMessages() {
    //$("#admin-messages-notifn").html("<p class='text-center text-info'><i class='fa fa-spinner fa-spin'></i></p>");
    $.post("_admin-requests.php", {
        admin_messages: noti
    }, function(data) {
        $("#admin-messages-notifn").html(data);
    });

}; //

// admin new messages count
function adminMsgCount() {
    $.post("_admin-requests.php", {
        admin_messages_count: noti
    }, function(data) {
        $(".admin-messages-count").html(data);
    });
}

// show clicked navbar message
function showthisMessagefunc(msgId) {

}

// notifications floating number on navbar
function checkNotificationCounter() {

    $.post("_admin-requests.php", {
        countNotifn: noti
    }, function(data) {
        $(".ad_jobs_business_span").html(data);
    });
}

// dropdown alert pending counter
function pendingAlertCounter() {
    var alertc = 1;

    $.ajax({
        url: "_admin-requests.php",
        cache: false,
        dataType: "json",
        type: "post",
        data: {
            pendingAlertCount: alertc
        },
        success: function(data) {
            $("#jobs_nav_counter").html(data.jobsCount)
            $("#jobs-awaiting").html(data.jobsCount + " pending");
            $("#adverts-awaiting").html(data.advertsCount + " pending");
            $("#adverts_nav_count").html(data.advertsCount)
        }
    });
}


/* ------------------------------------------ *
 *    modal for jobs and adverts
 * ------------------------------------------ */


// --- >>  advert mgmt starts << --- //
  
  // trending adverts
  /*
  function trending_adverts() {
    
    $("#advert-trends-div").html("<p class='text-center pink-text'><i class='fa fa-spinner fa-spin fa-2x'></i></p>");

    let adTrends = "advert-trending"

    $.get("_admin-requests.php", {
        adTrends:adTrends
    }, function(data){
        $("#advert-trends-div").html(data)
    }, "html");    
  }
  */


// --------------------------------- //

// view advert details
function switchToAdvertDetails(advertPkey, bannerView) {
    $.post("_admin-requests.php", {
        advertPkeyViewDetails: advertPkey
    }, function(data) {

        if (bannerView == "yes") {
            $("#advert-approval-modal-dialog").addClass("modal-lg"); // enlarge modal for advert details
        }
        $("#advertApprovalModalLabel").html("Advert Details"); // change modal title   
        $("#advertDetailsDiv").html(data); // load advert details
        $("#advertapprovalDiv").hide(); // hide advert list  
        $("#advertDetailsDiv").fadeIn(); // show modal 
    });
}


/* -----------------------
 *  cancel advert starts
 * ----------------------- */
function cancelAdvertfunc(advertPkey, advertSerial, advert_view) {
    $.post("_admin-requests.php", {
        canceladvertPkey: advertPkey,
        canceladvertSerial: advertSerial,
        advertView: advert_view
    }, function(data) {
        $("#CancelAdvertFormDiv").html(data)
        $("#advertApprovalLabel").html("Cancel Advert")

        $("#advertapprovalDiv").hide();
        $("#CancelAdvertFormDiv").fadeIn();
    });
}

function cancelAdvertfunc2(count) {

    var advertPkey   = $("#advert_post_id"+count).val();
    var advertSerial = $("#advert_serial_id"+count).val();
    var advertView   = $("#advert_view"+count).val();

    $.post("_admin-requests.php", {
        canceladvertPkey: advertPkey,
        canceladvertSerial: advertSerial,
        advertView: advertView
    }, function(data) {
        $("#load_advert_cancellation_form").html(data)
    });
}

function cancelAdvertfunction() {
    // form fields
    let dataCancel_advertPkey    = $("#dataCancel_advertPkey").val();
    let dataCancel_advertSerial  = $("#dataCancel_advertSerial").val();
    let dataCancel_AdvertReason  = $("#dataCancel_AdvertReason").val();
    let dataCancel_advertEmail   = $("#dataCancel_advertEmail").val();
    let dataCancel_advertTitle   = $("#dataCancel_advertTitle").val();
    let dataCancel_advertView    = $("#dataCancel_advertView").val();

    // button
    $("#btnCancelAdvert").prop("disabled", true);
    $("#btnCancelAdvert").html("please wait..")

    // validations
    if (dataCancel_AdvertReason == '') {
        _errorNotify("please enter reason for cancelling advert!")
        $("#btnCancelAdvert").prop("disabled", false);
        $("#btnCancelAdvert").html("Submit")
    } else if (dataCancel_AdvertReason.length > 150) {
        _errorNotify("Reason should not exceed 150 characters!")
        $("#btnCancelAdvert").prop("disabled", false);
        $("#btnCancelAdvert").html("Submit")
    } else {
        $.ajax({
            url: "_admin-requests.php",
            method: "post",
            cache: false,
            data: $("#cancelAdvertForm").serialize(),
            success: function(data) {
                if (data == 1) {

                    _successNotify("Advert successfully cancelled and notification mail sent!");
                    $("#cancelAdvertForm").slideUp("slow");
                    //$("#cancelAdvertForm")[0].reset();
                    //$("#cancelAdvert_wrapper").html('<button type="button" class="btn btn-flat bg-primary" id="backtoAdvertApprovaltblBtn" onclick="switchToAdvertList()"><i class="fa fa-angle-left"></i> Back</button>').fadeIn(5000);

                    if (dataCancel_advertView == 2) {
                        $("#advert_approval_td" + dataCancel_advertPkey).parents("#advert_approval_tr" + dataCancel_advertPkey).animate({
                            backgroundColor: "#595959"
                        }, "slow").animate({
                            opacity: "hide"
                        }, "slow");
                    } else {
                      $("#advert-cancelled"+dataCancel_advertPkey).html('Yes');
                    }

                } else {
                    _errorNotify("failed to cancel Advert!")
                    $("#btnCancelAdvert").prop("disabled", false);
                    $("#btnCancelAdvert").html("Submit")
                }
                //$("#cancel-advert-span").html(data);            
            }
        });
    }
}

  // undo advert cancelling
  function undoAdvertCancelfunc() {

      $("#btnUndoAdvertCancel").prop("disabled", true);
      
      let undoCancel_advertPkey   = $("#undoCancel_advertPkey").val();
      let undoCancel_advertSerial = $("#undoCancel_advertSerial").val();
      let undoCancel_advertView   = $("#undoCancel_advertView").val();

      $.ajax({
          url: "_admin-requests.php", 
          type: "POST",       
          data: {undoCancel_advertPkey:undoCancel_advertPkey, undoCancel_advertSerial:undoCancel_advertSerial,undoCancel_advertView:undoCancel_advertView},
          success: function(data){
              if (data == 1) {
                  $("#undoAdvertCancellingForm").slideUp("slow");
                  $("#undo-feedback").html("<p class='text-center text-success'>Info: advert cancelling undone</p>")
                  $("#advert-cancelled"+undoCancel_advertPkey).html("No");
              }
              else {
                  $("#btnUndoAdvertCancel").prop("disabled", false);
              }                
          }
      });
  };
// cancel advert ends << =============



// switch back to advert approval table
function switchToAdvertList() {
    $("#advert-approval-modal-dialog").removeClass("modal-lg");
    $("#advertapprovalDiv").fadeIn();
    $("#advertDetailsDiv").hide();
    $("#CancelAdvertFormDiv").hide();

    $("#advertApprovalModalLabel").html("Modals Approvals");
}

// approve advert
function approveAdvertfunc(advertPkey) {
    if (confirm("Approve this advert?")) {
        $("#advert_approval_td" + advertPkey).html('<i class="fa fa-spinner fa-spin approvalIcons t-20"></i>')


        $.post("_admin-requests.php", {
            advertApprovalId: advertPkey
        }, function(data) {


            $("#advert_approval_td" + advertPkey).html('<i class="fa fa-check-circle-o approvalIcons t-20"></i>');


            if (data == 1) {

                _successNotify("This advert Has been successfully approved!");

                $("#advert_approval_td" + advertPkey).parents("#advert_approval_tr" + advertPkey).animate({
                    backgroundColor: "#595959"
                }, "slow").animate({
                    opacity: "hide"
                }, "slow");

            } else if (data == 2) {
                _errorNotify("Failed to approve advert")
            } else if (response == 4) {
                _infoNotify("Advert has been disapproved")
            } else if (response == 5) {
                _errorNotify("Failed to disapproved advert")
            } else {
                _errorNotify("External Server error ")

            }

        });
    } // confirm
} // approveAdvertfunc



// toggle betwwen job details and job list
function swipeview_jobDetails(jobIdView) {
    $(".jobs_approval_wrapper_list").hide();
    $(".jobs_approval_wrapper_details, .btn-job-action").fadeIn("slow");
    $(".job-approval-details-wrapper").html("<p class='text-center text-info'><i class='fa fa-spinner fa-spin'></i></p>")

    $.ajax({
        url: "_admin-requests.php",
        method: "post",
        data: {
            jobIdDetails: jobIdView
        },
        success: function(data) {
            $(".job-approval-details-wrapper").html(data)
        }
    });
};

// currently viewing job details; return to previous job list view
function returnToJobApprovalList() {
    $(".jobs_approval_wrapper_list").fadeIn("slow");
    $(".jobs_approval_wrapper_details, .btn-job-action").hide();
};

// approve a job
// in table view
function approveJobfunction(jbApprovalId) {
    $.ajax({
        url: "_admin-requests.php",
        method: "post",
        data: {
            jbApprovalId: jbApprovalId
        },
        success: function(data) {
            if ($.trim(data) == 1) {
                // success notifn to admin
                _successNotify("Job successfully approved!")
                    // animate table row
                    /*
                $("#a_approved_td" + jbApprovalId).parents("#awaiting-job-approval-list" + jbApprovalId)
                    .animate({
                        backgroundColor: "#595959"
                    }, "slow")
                    .animate({
                        opacity: "hide"
                    }, "slow");
                    */
            } else {
                // error or failure notifn to admin
                _errorNotify("Failed to approve job!")
            }
        }
    });
}
// in detail view
function approveJobApproval() {
    let jbApprovalId = $("#data_job_id_awaiting_approval").val();

    $.ajax({
        url: "_admin-requests.php",
        method: "post",
        data: {
            jbApprovalId: jbApprovalId
        },
        success: function(data) {
            if ($.trim(data) == 1) {
                // success notifn to admin
                _successNotify("Job successfully approved!");
                // disable approve button in footer
                $("#approve-job-footer-btn").prop("disabled", true);
                // animate table row
                $("#a_approved_td" + jbApprovalId).parents("#awaiting-job-approval-list" + jbApprovalId)
                    .animate({
                        backgroundColor: "#595959"
                    }, "slow")
                    .animate({
                        opacity: "hide"
                    }, "slow");
            } else {
                // error or failure notifn to admin
                _errorNotify("Failed to approve job!")
            }
        }
    });
}

// load selected job details
function showJobDetails(zoomOutIn7) {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("displayJobs").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "jobDetails.php?viewJob=" + zoomOutIn7.getAttribute("data-jobid"), true);
    xhttp.send();
}

// pnotify
function _successNotify(msg) {
    new PNotify({
        title: 'Regular Notice',
        text: msg,
        type: 'success'
    });
};

function _errorNotify(err) {
    new PNotify({
        title: 'Error Notice',
        text: err,
        type: 'error'
    });
};

function _infoNotify(info) {
    new PNotify({
        title: 'Regular Notice',
        text: info,
        type: 'info'
    });
}


function showReg() {

    $("#profForm").fadeIn(2500);

    $("#logDetail").fadeIn(2500);

    $("#hideTable").hide(1000);
}

function closeReg() {

    $("#profForm").hide(1000);

    $("#logDetail").hide(1000);

    $("#hideTable").fadeIn(2500);
}


//running model for adding services	

function showAll(modalBootstrap) {

    var cat_id = modalBootstrap.getAttribute("data-id");

    var indust_name = modalBootstrap.getAttribute("data-industry");

    var cat_name = modalBootstrap.getAttribute("data-cat");

    document.getElementById("showId").value = cat_id;

    document.getElementById("showName").innerHTML = indust_name;

    document.getElementById("showCat").innerHTML = cat_name;

}

function refresh() {


    setTimeout(function() {

        location.reload();

    }, 2000);
}



//code for live search for business and linked to a php file gethint.php 

function showResult(str) {
    if (str.length == 0) {
        document.getElementById("livesearch").innerHTML = "";
        document.getElementById("livesearch").style.border = "0px";
        return;
    }
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("livesearch").innerHTML = this.responseText;

        }
    }
    xmlhttp.open("GET", "getHint.php?q=" + str, true);
    xmlhttp.send();
}


//calling the div to add bills for business subscription 

function addBizBill() {


    $("#bizBill").fadeIn(2500);

    $("#hideBiz").hide(1000);
    $("#hideAdvert").hide(1000);
    $("#hideJob").hide(1000);

}

//closing the prof div
function shutdiv() {

    $("#bizBill").hide(1000);

    $("#hideBiz").show(1000);
    $("#hideAdvert").show(1000);
    $("#hideJob").show(1000);

}

//opening the advert bill 

function addAdvertBill() {


    $("#advertBill").fadeIn(2500);

    $("#hideBiz").hide(1000);
    $("#hideAdvert").hide(1000);
    $("#hideJob").hide(1000);
}


//closing the advert bill div

function shutAdvert() {

    $("#advertBill").hide(1000);

    $("#hideBiz").show(1000);
    $("#hideAdvert").show(1000);
    $("#hideJob").show(1000);

}


//opening the job bill 

function addJobBill() {


    $("#jobBill").fadeIn(2500);

    $("#hideBiz").hide(1000);
    $("#hideAdvert").hide(1000);
    $("#hideJob").hide(1000);

}

//closing the job bill div

function shutJob() {

    $("#jobBill").hide(1000);

    $("#hideBiz").show(1000);
    $("#hideAdvert").show(1000);
    $("#hideJob").show(1000);

}



// --- >> function for showing sender



function showReceiver(zoomOutIn5) {

    var msg_receiver = zoomOutIn5.getAttribute("data-receiver");

    var message_id = zoomOutIn5.getAttribute("data-msgid");


    document.getElementById("msgReceiver").value = msg_receiver;

    document.getElementById("msgeid").value = message_id;
}


// --- >> function for displaying full message

function showMsg(zoomOutIn6) {

    var msg_text = zoomOutIn6.getAttribute("data-msgText");

    var msg_date = zoomOutIn6.getAttribute("data-msgDate");

    var msg_sender = zoomOutIn6.getAttribute("data-msgSender");

    var msg_subject = zoomOutIn6.getAttribute("data-msgSubject");


    document.getElementById("msgText").innerHTML = msg_text;

    document.getElementById("msgDate").innerHTML = msg_date;

    document.getElementById("msgSender").innerHTML = msg_sender;

    document.getElementById("msgSubject").innerHTML = msg_subject;

}



//calling page to display zoomed advert image

function getAdvertImage(zoomOutIn9) {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            document.getElementById("advertImage").innerHTML = this.responseText;

        }
    };
    xhttp.open("GET", "advertImage.php?viewImage=" + zoomOutIn9.getAttribute("data-advertid"), true);
    xhttp.send();
}


//calling page to display staff and edit details

function getStaff(zoomOutIn10) {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            document.getElementById("editStaff").innerHTML = this.responseText;

        }
    };
    xhttp.open("GET", "advertImage.php?updateStaff=" + zoomOutIn10.getAttribute("data-staffid"), true);
    xhttp.send();
}

//js for adding skills


function saveInd() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#indust_form").serialize();

    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterInd');

    request = $.ajax({
        url: "adminProcess.php?industSave",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#indust_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message

        setTimeout(function() {

            location.reload();

        }, 2000);

        $("#afterInd").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> You added another Industry </div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterInd").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}




//js for adding skills


function saveCat() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#cat_form").serialize();

    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterCat');

    request = $.ajax({
        url: "adminProcess.php?catSave",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#cat_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message

        setTimeout(function() {

            location.reload();

        }, 2000);

        $("#afterCat").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> You added another cateogory </div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterCat").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}

//saving a service 


function saveService() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#serv_form").serialize();


    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterServ');

    request = $.ajax({
        url: "adminProcess.php?servSave",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#serv_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message



        $("#afterServ").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> You added another service </div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterServ").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}



//reating and posting a job


function saveJob() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#job_form").serialize();


    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterJob');

    request = $.ajax({
        url: "adminProcess.php?jobSave",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#job_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message



        $("#afterJob").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <b>A job was posted successfully </b></div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterJob").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}



//sign a new biz bill 


function saveBizBill() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#bizBill_form").serialize();


    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterBizBill');

    request = $.ajax({
        url: "adminProcess.php?bizBillSave",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#bizBill_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message



        $("#afterBizBill").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <b>A new subscription bill has been set </b></div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterBizBill").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}



//add a new advert bill 


function saveAdvertBill() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#advertBill_form").serialize();


    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterAdvertBill');

    request = $.ajax({
        url: "adminProcess.php?advertBillSave",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#advertBill_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message



        $("#afterAdvertBill").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <b>A new advertising bill has been set </b></div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterAdvertBill").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}


//add a new job bill 


function saveJobBill() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#jobBill_form").serialize();


    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterJobBill');

    request = $.ajax({
        url: "adminProcess.php?jobBillSave",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#jobBill_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message



        $("#afterJobBill").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <b>A new  bill has been set for posting a job</b></div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterJobBill").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}



//deleting a message 


function delMsg() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#msgdelete_form").serialize();

    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterInd');

    request = $.ajax({
        url: "adminProcess.php?delMsg",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#msgdelete_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message

        setTimeout(function() {

            location.reload();

        }, 2000);

        $("#afterMsgDel").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong>Message deleted </div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterMsgDel").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}


//new fs staff


function newStaff() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#staff_form").serialize();

    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterStaff');

    request = $.ajax({
        url: "saveStaff.php",
        type: "POST",
        data: serializedData
    });


    //clearing the form fields 

    $('#staff_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message

        /* setTimeout(function(){
          
          location.reload();
  
              },2000); */

        $("#afterStaff").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong>Operation successful</div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterStaff").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}




//sending mail to an open msg before account creation 


function openRepMail() {

    // using serialize function of jQuery to get all values of form
    var serializedData = $("#repMail_form").serialize();


    // Variable to hold request
    var request;
    // Fire off the request to process_registration_form.php

    var resultMessage = $('#afterMail');

    request = $.ajax({
        url: "adminProcess.php?sendRepMail",
        type: "GET",
        data: serializedData
    });


    //clearing the form fields 

    $('#repMail_form')[0].reset();

    // Callback handler that will be called on success

    request.done(function(jqXHR, textStatus, response) {

        // you will get response from your php page (what you echo or print)
        // show successfully for submit message



        $("#afterMail").html('<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <b>Mail was sent successful</b></div>');
    });

    // Callback handler that will be called on failure

    request.fail(function(jqXHR, textStatus, errorThrown) {

        // Log the error to the console
        // show error
        $("#afterMail").html('There is some error while submit');

        console.error(

            "The following error occurred: " +

            textStatus, errorThrown
        );
    });

    return false;

}

//saving new category 

function showInd(zoomOutInM2) {

    var indust_id = zoomOutInM2.getAttribute("data-industid");

    var industry_name = zoomOutInM2.getAttribute("data-industry");


    document.getElementById("industid").value = indust_id;

    document.getElementById("industry").innerHTML = industry_name;
}



//load industry summary  

function loadIndust(zoomOutInM3) {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            document.getElementById("industSum").innerHTML = this.responseText;

        }
    };
    xhttp.open("GET", "_fetch.php?industSummary=" + zoomOutInM3.getAttribute("data-id"), true);
    xhttp.send();
}