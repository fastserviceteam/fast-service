/* ---------------------- *
    fast service script
 * ---------------------- */


$(function(){



    /**
    *  ADVERTS
    *------------------------*/

    // load trending adverts in advert management
    let adTrends = "advert-trending"

    $("#advert-trends-div").html("<p class='text-center pink-text'><i class='fa fa-spinner fa-spin fa-2x'></i></p>");
    $.get("_admin-requests.php", {
        adTrends:adTrends
    }, function(data){
        $("#advert-trends-div").html(data)
    }, "html");  
    
    // --- >> login
    $("#fast_service_admin_login").on("submit", function(e){
        
        e.preventDefault();
        
        let fastserviceEmail      = $("#fastservice_data_email").val();
        let fastservicePriviledge = $("#fastservice_data_priviledge").val();
        let fastservicePassword   = $("#fastservice_data_password").val();
        
        $("#fastserviceLoginBtn").prop("disabled", true);
        
        if(fastserviceEmail == '' || fastserviceEmail == '' || fastserviceEmail == '')
            {
                alert("Error: please check for empty fields!!");
                $("#fastserviceLoginBtn").prop("disabled", false);
            }
        else
        {        
            $.ajax({
              url: "_admin-requests.php", 
              type: "POST",       
              data: new FormData(this),
              contentType: false,   
              cache: false,         
              processData:false, 
              success: function(data)
              {
                $(".span-feedbacks").fadeIn("slow").html(data);
                $("#fastserviceLoginBtn").prop("disabled", false);
              }
            });	            
        }
    });
    
    
    
    $("#prof_btn").click(function() {
 
        // using serialize function of jQuery to get all values of form
        var serializedData = $("#prof_form").serialize();
 
        // Variable to hold request
        var request;
        // Fire off the request to process_registration_form.php
    
    var resultMessage = $('#finish');
    
        request = $.ajax({
            url: "profsave.php",
            type: "post",
            data: serializedData
        });
    
    //clearing the form fields 
    
    $('#prof_form')[0].reset();
 
        // Callback handler that will be called on success
    
        request.done(function(jqXHR, textStatus, response) {
      
            // you will get response from your php page (what you echo or print)
             // show successfully for submit message
       
            $("#finish").html('<div class="alert alert-success"> <strong>Success!</strong> Your profile has been updated. <a href="prof.php">'

      +'<button class="w3-btn w3-teal w3-round" >Review</button></a></div>');
        });
 
        // Callback handler that will be called on failure
    
        request.fail(function(jqXHR, textStatus, errorThrown) {
      
            // Log the error to the console
            // show error
            $("#finish").html('<div class="alert alert-danger"> <strong>Success!</strong> An error occured. <a href="prof.php"><button class="w3-btn w3-teal w3-round" >Review</button></a></div>');
      
            console.error(
      
                "The following error occurred: " +
                
        textStatus, errorThrown
            );
        });
 
        return false;
 
    });
    
});