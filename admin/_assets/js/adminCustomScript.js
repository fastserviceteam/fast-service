$(function() {



    // --- >> create new industry 

    $("#industForm").on("submit", function(e) {
        e.preventDefault();

        $("#dataBtn_newIndustry").prop("disabled", true);
        $("#dataBtn_newIndustry").html("please wait...");

        $.ajax({
            url: "adminProcess.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                alert("a new industry has been registered.........!");

                loadTemplateView('prod-serv')

                $("#dataBtn_newIndustry").prop("disabled", false);
                $("#dataBtn_newIndustry").html("Submit");

                //clearing the form fields 

                $('#industForm')[0].reset();

            }
        });
    });


    // --- >> create new category in an industry

    $("#categoryForm").on("submit", function(e) {
        e.preventDefault();

        $("#dataBtn_newCategory").prop("disabled", true);
        $("#dataBtn_newCategory").html("please wait...");

        $.ajax({
            url: "adminProcess.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                alert("a new category has been registered.........!");

                loadTemplateView('prod-serv')

                $("#dataBtn_newCategory").prop("disabled", false);
                $("#dataBtn_newCategory").html("Submit");

                //clearing the form fields 

                $('#categoryForm')[0].reset();

            }
        });
    });



    // --- >> create new services 

    $("#serviceForm").on("submit", function(e) {
        e.preventDefault();

        $("#dataBtn_newService").prop("disabled", true);
        $("#dataBtn_newService").html("please wait...");

        $.ajax({
            url: "adminProcess.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                alert("a new service has been registered.........!");

                loadTemplateView('prod-serv')

                $("#dataBtn_newService").prop("disabled", false);
                $("#dataBtn_newService").html("Submit");

                //clearing the form fields 

                $('#serviceForm')[0].reset();

            }
        });
    });

    // magnificent popup    
    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true
        }

    });

});