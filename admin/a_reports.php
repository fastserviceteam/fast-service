<?php
// require global file
require "_admin-config.php";
// instantiate class
$_adminObj = new AdminClass();
?>
<!doctype html>
<html class="fixed">
    <head>
        <!-- Basic -->
        <meta charset="UTF-8">
        <title>Fast Service - Admin</title>
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- top css file -->
        <?php include "_includes/_topCss.php";?>
    </head>
    <body>
        <section class="body" >
            
            <?php
            # include header ~ top-nav
            require "_includes/_topNav.php";
            ?>
            <div class="inner-wrapper">
                <!-- start: sidebar -->
                <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">
                    <div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">
                        <div class="sidebar-title">
                            FastService Corp
                        </div>
                        <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                        </div>
                    </div>
                    <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                        <div class="nano-content" >
                            <nav id="menu" class="nav-main" role="navigation" >
                                <ul class="nav nav-main" style="color:#607D8B;">
                                    <li id="nav_admin_main">
                                        <a href="a_dashboard.php">
                                            <i class="fa fa-dashboard" aria-hidden="true"></i>
                                            <span>Dashboard</span>
                                        </a>
                                    </li>
                                    <li id="nav_admin_businessmgmt">
                                        <a href="a_business.php">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                            <span>Business Management</span>
                                        </a>
                                    </li>
                                    <li id="nav_admin_prodservices">
                                        <a href="a_categories.php?firstOpen">
                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                            <span>Products and Services</span>
                                        </a>
                                    </li>
                                    <li id="nav_admin_billing">
                                        <a href="a_billing.php">
                                            <i class="fa fa-exchange" aria-hidden="true"></i>
                                            <span>Billing</span>
                                        </a>
                                    </li>
                                    <li id="nav_admin_users">
                                        <a href="a_users.php">
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                            <span id="usr">Human Resource</span>
                                        </a>
                                    </li>
                                    <li id="nav_admin_jobs">
                                        <a href="a_jobs.php">
                                            <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                            <span>Jobs</span>
                                        </a>
                                    </li>
                                    <li id="nav_admin_adverts">
                                        <a href="a_adverts.php">
                                            <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            <span>Adverts Mgt</span>
                                        </a>
                                    </li>
                                    <li id="nav_admin_market_survey">
                                        <a href="a_marketSurvey.php">
                                            <i class="fa fa-clipboard" aria-hidden="true"></i>
                                            <span>Market Survey</span>
                                        </a>
                                    </li>
                                    <li class="nav-active" id="nav_admin_reports">
                                        <a href="a_reports.php">
                                            <i class="fa fa-copy" aria-hidden="true"></i>
                                            <span>Reports</span>
                                        </a>
                                    </li>
                                    
                                    <li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>
                                    <li id="nav_admin_sysalerts">
                                        <a href="a_alerts.php">
                                            <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                            <span>System Alerts</span>
                                        </a>
                                    </li>
                                    <li id="nav_admin_inbox">
                                        <a href="a_mailbox.php">
                                            <span class="pull-right label label-primary admin-messages-count">0</span>
                                            
                                            <i class="fa fa-inbox" aria-hidden="true"></i>
                                            <span>Inbox</span>
                                            
                                        </a>
                                    </li>
                                    <li id="nav_admin_logout">
                                        <a href="index.php?logout">
                                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                                            <span>Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            <hr class="separator">
                        </div>
                    </div>
                </aside>
                <!-- end: sidebar -->
                <!-- right content -->
                <section role="main" class="content-body">
                    
                    <!-- ========== /. breadcrumb header ========== -->
                    <div class="content-header">
                        <h1>
                        Reports
                        <small>Generation</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-copy"></i> Home</a></li>
                            <li class="active">Reports</li>
                        </ol>
                    </div>
                    <!-- ========== /. home content ========== -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="tabs tabs-primary">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active">
                                        <a href="#jobsReportsTab" data-toggle="tab" class="text-center"><i class="fa fa-briefcase"></i> Jobs</a>
                                    </li>
                                    <li>
                                        <a href="#advertReportsTab" data-toggle="tab" class="text-center"><i class="fa fa-info-circle"></i> Adverts</a>
                                    </li>
                                </ul>
                                <div class="tab-content">

                                    <div id="jobsReportsTab" class="tab-pane active">
                                        <form role="form" id="dataform-jobs-reports" class="form-inline mt-md mdb-color-text text-center">

                                            <div class="form-group">
                                                <select name="dataselect_jobsBusinessname" data-plugin-selectTwo id="dataselect-jobsBusinessname" class="form-control populate placeholder reset-fields-select1" data-plugin-options='{ "placeholder": "--- select business name ---", "allowClear": true }'>
                                                    <option></option>
                                                    <option value="all">All Businesses</option>
                                                    <?php foreach($_adminObj->businessinfo() as $row) { ?>
                                                        <option value="<?php echo $row['business_profile_id']; ?>"><?php echo $row['Business_Name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <div class="input-daterange input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <input type="text" class="form-control datainput-tofrom-dates" name="datainput_from_date" placeholder="From: " id="datainput_from_date">
                                                    <span class="input-group-addon">to</span>
                                                    <input type="text" class="form-control datainput-tofrom-dates" name="datainput_to_date" placeholder="To: " id="datainput_to_date">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button type="button" id="dataInput-jobsreport-btn" class="btn btn-flat bg-blue">Submit</button>
                                            </div>

                                            <div class="form-group form-group-jobs-class" id="form-group-jobs">
                                                <button type="button" id="dataInput-jobsreport-resetbtn" class="btn btn-flat bg-orange">Reset</button>
                                            </div>
                                            
                                        </form>
                                        <!-- /. dataform-jobs-reports -->
                                        <hr>
                                        <div id="jobs-tbl-wrapper">
                                            
                                        </div>
                                        <!-- jobs-tbl-wrapper -->
                                    </div>
                                    <!-- jobsReportsTab -->

                                    <div id="advertReportsTab" class="tab-pane">
                                        <form role="form" id="dataform-advert-reports" class="form-inline mt-md mdb-color-text text-center">

                                            <div class="form-group">
                                                <select name="dataselect_advertsBusinessname" data-plugin-selectTwo id="dataselect-advertsBusinessname" class="form-control populate placeholder reset-fields-select2" data-plugin-options='{ "placeholder": "--- select business name ---", "allowClear": true }'>
                                                    <option></option>
                                                    <option value="all">All Businesses</option>
                                                    <?php foreach($_adminObj->businessinfo() as $row) { ?>
                                                        <option value="<?php echo $row['business_profile_id']; ?>"><?php echo $row['Business_Name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <div class="input-daterange input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <input type="text" class="form-control datainput-tofrom-dates" name="datainput_adverts_from_date" placeholder="From: " id="datainput_adverts_from_date">
                                                    <span class="input-group-addon">to</span>
                                                    <input type="text" class="form-control datainput-tofrom-dates" name="datainput_adverts_to_date" placeholder="To: " id="datainput_adverts_to_date">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button type="button" id="dataInput-advertsreport-btn" class="btn btn-flat bg-blue">Submit</button>
                                            </div>

                                            <div class="form-group form-group-adverts-class" id="form-group-adverts">
                                                <button type="button" id="dataInput-advertssreport-resetbtn" class="btn btn-flat bg-orange">Reset</button>
                                            </div>
                                            
                                        </form>
                                        <!-- /. dataform-advert-reports -->
                                        <hr>
    
                                        <div id="advert-tbl-wrapper">


                                        </div>
                                        <!-- advert-tbl-wrapper -->
                                    </div>
                                    <!-- advertReportsTab -->
                                </div>
                            </div>
                            
                        </div>
                        <!-- col-xs-12 col-sm-12 col-md-12 col-lg-12 -->
                    </div>
                    <!-- row -->
                </section>
                <!-- /. content-body -->
            </div>
            <!-- /. inner-wrapper -->
        </section>
        <!-- /. body -->
        
        <!-- /. require plugins and scripts -->
        <?php  include '_includes/_bottomJs.php';?>
        <!-- page specific scripts -->
        <script>
            // datatable init
            (function( $ ) {
                'use strict';

                // load jobs
                var loadJobReportList = function() {
                    
                    var jobslistValue = "job-report";

                    $("#jobs-tbl-wrapper").html("<p class='text-center text-warning'><i class='fa fa-spinner fa-spin fa-3x'></i></p>");
                    
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {

                            $("#jobs-tbl-wrapper").html(this.responseText).fadeIn("slow");
                        }
                    };
                    xhttp.open("GET", "_admin-requests.php?jobreportlist="+jobslistValue, true);
                    xhttp.send();
                };

                // load advert
                /*
                $("#advert-tbl-wrapper").html("<p class='text-center text-warning'><i class='fa fa-spinner fa-spin fa-3x'></i></p>");
                var loadAdvertReportList = function() {
                    var advertslistValue = "advert-report";
                        $.get("_admin-requests.php", {
                            advertreportlist:advertslistValue
                        }, function(data){
                            $("#advert-tbl-wrapper").html(data).fadeIn("slow");
                        }, "html");
                    };
                    */
                
                var loadAdvertReportList = function() {

                    var advertslistValue = "advert-report";

                    $("#advert-tbl-wrapper").html("<p class='text-center text-warning'><i class='fa fa-spinner fa-spin fa-3x'></i></p>");
                    
                    var xhttpObj = new XMLHttpRequest();
                    xhttpObj.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {

                            $("#advert-tbl-wrapper").html(this.responseText).fadeIn("slow");
                        }
                    };
                    xhttpObj.open("GET", "_admin-requests.php?advertreportlist="+advertslistValue, true);
                    xhttpObj.send();
                };
                
                
                $(function() {

                    // load advert list function
                    loadAdvertReportList();

                    // load job list function
                    loadJobReportList();

                    // init datepicker
                    $('.datainput-tofrom-dates').datepicker({
                        format: "yyyy-mm-dd",
                        autoclose: true
                    });

                    // onclick event

                    // jobs
                    $("#dataInput-jobsreport-btn").on("click", function(){

                        var datainputFromDate           = $("#datainput_from_date").val();
                        var datainputToDate             = $("#datainput_to_date").val();
                        var dataselectjobsBusinessname  = $("#dataselect-jobsBusinessname").val();

                        $("#dataInput-jobsreport-btn").prop("disabled", true);
                        $("#dataInput-jobsreport-btn").html("please wait..");

                        if (datainputFromDate == '' || datainputToDate == '' || dataselectjobsBusinessname == '') {
                            _errorNotify("Error; please check for empty fields!")

                            $("#dataInput-jobsreport-btn").prop("disabled", false);
                            $("#dataInput-jobsreport-btn").html("Submit");
                        } else {

                            $.ajax({
                                url: "_admin-requests.php",
                                method: "post",
                                data: "datainputFromDate="+datainputFromDate+"&datainputToDate="+datainputToDate+"&dataselectjobsBusinessname="+dataselectjobsBusinessname,
                                success: function(data){
                                    $("#jobs-tbl-wrapper").html(data).fadeIn();

                                    $("#dataInput-jobsreport-btn").prop("disabled", false);
                                    $("#dataInput-jobsreport-btn").html("Submit");
                                    $("#form-group-jobs").removeClass('form-group-jobs-class');
                                }
                            });
                        } // else
                    });

                    // toggle job report reset
                    $("#dataInput-jobsreport-resetbtn").on("click", function(){
                        $(this).addClass("form-group-jobs-class"); // add the visibility hidden class to button
                        $("#dataform-jobs-reports")[0].reset();
                        $(".reset-fields-select1").val('').trigger("change");
                        loadJobReportList(); // call the default job view function
                    });

                    // adverts
                    $("#dataInput-advertsreport-btn").on("click", function(){

                        var datainputadvertsFromDate       = $("#datainput_adverts_from_date").val();
                        var datainputadvertsToDate         = $("#datainput_adverts_to_date").val();
                        var dataselectadvertsBusinessname  = $("#dataselect-advertsBusinessname").val();

                        $("#dataInput-advertsreport-btn").prop("disabled", true);
                        $("#dataInput-advertsreport-btn").html("please wait..");

                        if (datainputadvertsFromDate == '' || datainputadvertsToDate == '' || dataselectadvertsBusinessname == '') {
                            _errorNotify("Error; please check for empty fields!");

                            $("#dataInput-advertsreport-btn").prop("disabled", false);
                            $("#dataInput-advertsreport-btn").html("Submit");
                        } else {

                            $.ajax({
                                url: "_admin-requests.php",
                                method: "post",
                                data: "datainputadvertsFromDate="+datainputadvertsFromDate+"&datainputadvertsToDate="+datainputadvertsToDate+"&dataselectadvertsBusinessname="+dataselectadvertsBusinessname,
                                success: function(data){
                                    $("#advert-tbl-wrapper").html(data).fadeIn();

                                    $("#dataInput-advertsreport-btn").prop("disabled", false);
                                    $("#dataInput-advertsreport-btn").html("Submit");
                                    $("#form-group-adverts").removeClass('form-group-adverts-class');
                                }
                            });
                        } // else
                    });

                    // toggle advert report reset
                    $("#dataInput-advertssreport-resetbtn").on("click", function(){
                        $(this).addClass("form-group-adverts-class"); // add visibility hidden class to button
                        $("#dataform-advert-reports")[0].reset();
                        $(".reset-fields-select2").val('').trigger("change");
                        loadAdvertReportList(); // call the default advert report view function
                    });


                });

            }).apply( this, [ jQuery ]);

            // onchange date event
            /*
                function jobReportsDatefunction(whichInput, inputValue) {
                    console.log("Input: "+whichInput+" Value: " + inputValue)
                    $.ajax({
                        url: "_admin-requests.php",
                        method: "post",
                        data: "jobReportInput="+whichInput+"&jobReportdateValue="+inputValue,
                        success: function(data){
                            $("#advert-tbl-wrapper").html(data).fadeIn();
                        }
                    });
                };
            */
    
        </script>
    </body>
</html>