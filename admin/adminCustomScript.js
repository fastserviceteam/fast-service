// --- >> create new industry 
	
	$("#industForm").on("submit", function(e)
	{		
		e.preventDefault();

		$("#dataBtn_newIndustry").prop("disabled", true);
		$("#dataBtn_newIndustry").html("please wait...");	

		$.ajax({
	      url: "adminProcess.php", 
		  type: "POST",       
		  data: new FormData(this),
		  contentType: false,   
		  cache: false,         
		  processData:false, 
		  success: function(data)
		  {
			alert("a new industry has been registered.........!");
			
			loadTemplateView('prod-serv')
			
				$("#dataBtn_newIndustry").prop("disabled", false);
				$("#dataBtn_newIndustry").html("Submit");	
				
		//clearing the form fields 
		
		$('#industForm')[0].reset();
		
		  }
		});	
	});