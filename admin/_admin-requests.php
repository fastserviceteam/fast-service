<?php
/* ---------------------------------- */
    # fast service
    # admin requests
    # 30 July, '19
/* ---------------------------------- */
// require global file
require "_admin-config.php";

// instantiate class
$_adminObj = new AdminClass();

// ------ >> post and get requests starts here << ------ //


/**
* Jobs
*------------------*/
// job analysis under job management
if (isset($_POST['jobAnalysisAction'])) {

    /*
    $dataInputSearchJob    = "";
    $dataSelectApprovalJob = "";
    $dataSelectOrderJob    = "";
    $dataSelectDistrictJob = "";

    $dataInputSearchJob    = isset($_REQUEST['dataInputSearchJob'])    ? $_REQUEST['dataInputSearchJob']    : "";
    $dataSelectApprovalJob = isset($_REQUEST['dataSelectApprovalJob']) ? $_REQUEST['dataSelectApprovalJob'] : "";
    $dataSelectOrderJob    = isset($_REQUEST['dataSelectOrderJob'])    ? $_REQUEST['dataSelectOrderJob']    : "";
    $dataSelectDistrictJob = isset($_REQUEST['dataSelectDistrictJob']) ? $_REQUEST['dataSelectDistrictJob'] : "";

    $_adminObj->jobAnalysis(strtolower(trim($dataInputSearchJob)), $dataSelectApprovalJob, $dataSelectOrderJob, $dataSelectDistrictJob);
    */

    $_adminObj->jobAnalysis(strtolower(trim($_POST["dataInputSearchJob"])), $_POST["dataSelectApprovalJob"], $_POST["dataSelectOrderJob"], $_POST["dataSelectDistrictJob"]);
} // end


// load jobs requiring approval
if(isset($_POST['jobs_requiring_approval']))
{
    $_adminObj->loadJobsRequiringApprovalDatatbl();
} // isset // post // job details

// load selected job details
if(isset($_POST['jobIdDetails']))
{
    $jobIdDetails  = $_POST["jobIdDetails"];
    $_adminObj->_loadJobDetails($jobIdDetails);
} // isset // post // job details

// approve a job in modal
if(isset($_POST['jbApprovalId']))
{
    $jbApprovalId  = $_POST["jbApprovalId"];
    $_adminObj->_approveJobMethod($jbApprovalId);    
} // isset // post // approve a job

// (dis)approve job under job management
if(isset($_POST['dataJobApproval_Id']) && isset($_POST['dataJob_value']))
{
    $jbApprovalId  = $_POST["dataJobApproval_Id"];
    $dataJob_value  = $_POST["dataJob_value"];
    $_adminObj->dis_or_approveJobMethod($jbApprovalId, $dataJob_value);
} // isset

// reload notification number
if(isset($_POST['countNotifn']))
{
    echo $_adminObj->_totalNotifnCounter();
} //


// pending alerts counter
if(isset($_POST['pendingAlertCount']))
{
    echo $_adminObj->_advertsjobsAlerts();
} //

// show cancel advert form
if(isset($_POST['canceladvertPkey']) && isset($_POST['canceladvertSerial']) && isset($_POST['advertView']))
{
    $canceladvertPkey    = $_POST["canceladvertPkey"];
    $canceladvertSerial  = $_POST["canceladvertSerial"];
    $advertView          = $_POST["advertView"];

    echo $_adminObj->_cancelAdvertForm($canceladvertPkey, $canceladvertSerial, $advertView);
} //
// undo advert cancelling
if(isset($_POST['undoCancel_advertPkey']) && isset($_POST['undoCancel_advertSerial']) && isset($_POST['undoCancel_advertView']))
{
    $undoadvertCancelPkey    = $_POST["undoCancel_advertPkey"];
    $undoadvertCancelSerial  = $_POST["undoCancel_advertSerial"];
    $advertView              = $_POST["undoCancel_advertView"];

    echo $_adminObj->_undoAdvertCancelling($undoadvertCancelPkey, $undoadvertCancelSerial, $advertView);
} //


// cancel advert submit
if(isset($_POST['dataCancel_advertPkey']) && isset($_POST['dataCancel_advertSerial']) && isset($_POST['dataCancel_AdvertReason']) && isset($_POST['dataCancel_advertEmail']) && isset($_POST['dataCancel_advertTitle']))
{
    $dataCancel_advertPkey    = $_POST["dataCancel_advertPkey"];
    $dataCancel_advertSerial  = $_POST["dataCancel_advertSerial"];
    $dataCancel_AdvertReason  = $_POST["dataCancel_AdvertReason"];
    $dataCancel_advertEmail   = $_POST["dataCancel_advertEmail"];
    $dataCancel_advertTitle   = $_POST["dataCancel_advertTitle"];

    $_adminObj->_cancelAdvertSave($dataCancel_advertPkey, $dataCancel_advertSerial, $dataCancel_AdvertReason, $dataCancel_advertEmail, $dataCancel_advertTitle);
} //


// load advert details
if(isset($_POST['advertPkeyViewDetails']))
{
    $advertPkeyViewDetails  = $_POST["advertPkeyViewDetails"];
    $_adminObj->_viewAdvertDetails($advertPkeyViewDetails);    
} // 

// approve advert
if(isset($_POST['advertApprovalId']))
{
    $advertApprovalId  = $_POST["advertApprovalId"];
    $_adminObj->_approveAdvertMethod($advertApprovalId);    
} //

// --- >> advert list page

    // advert filter / order
if(isset($_POST['filterAdvertData']))
{
    $filterSearchKeyword  = strtolower(trim($_POST["filterSearchKeyword"]));
    $filterSelectOrder    = $_POST["filterSelectOrder"];
    $filterSelectFilter   = $_POST["filterSelectFilter"];

    $_adminObj->_advertfilterSelect($filterSearchKeyword, $filterSelectOrder, $filterSelectFilter); 
}

// load trending adverts
if (isset($_GET['adTrends'])) {
    $_adminObj->_getTrendingAdverts(); 
}

/**
* Reports Mgmt
* ------------ */
// load jobs list under reports - datatable view
if(isset($_GET['jobreportlist']))
{
    $_adminObj->jobReportDefaultViewList();
} // isset // post

// jobs report dates onchange filter
if(isset($_POST['datainputFromDate']) && isset($_POST['datainputToDate']) && isset($_POST['dataselectjobsBusinessname']))
{
    $_adminObj->jobReportsDateFilter($_POST['datainputFromDate'], $_POST['datainputToDate'], $_POST['dataselectjobsBusinessname']);
} // isset // post

// load adverts list under reports - datatable view
if(isset($_GET['advertreportlist']))
{
    $_adminObj->advertReportDefaultViewList();
} // isset // post

// adverts report dates onchange filter
if(isset($_POST['datainputadvertsFromDate']) && isset($_POST['datainputadvertsToDate']) && isset($_POST['dataselectadvertsBusinessname']))
{
    $_adminObj->advertsReportsDateFilter($_POST['datainputadvertsFromDate'], $_POST['datainputadvertsToDate'], $_POST['dataselectadvertsBusinessname']);
} // isset // post

// -----------------------



// login
if(isset($_POST['fastservice_data_email']) && isset($_POST['fastservice_data_priviledge']) && isset($_POST['fastservice_data_password']))
{
    $fastserviceEmail        = $_POST["fastservice_data_email"]; // email
    $fastservicePriviledge   = $_POST["fastservice_data_priviledge"]; // priviledge
    $fastservicePassword     = $_POST["fastservice_data_password"]; // password
    $fastservice_remember_me = $_POST["fastservice_data_remember_me"]; // remember me
    
    $_adminObj->adminLoginRequest($fastserviceEmail, $fastservicePriviledge, $fastservicePassword, $fastservice_remember_me); // login method
    $_adminObj->feedbacks(); // messages or errors return
    
} // isset // post // login

// logout
if(isset($_GET['logout']))
{
    $_adminObj->_adminlogout();
    
} // isset // get // logout


// admin messages on navbar
if(isset($_POST['admin_messages']))
{
    $_adminObj->_adminMessages();    
} //

// admin messages count
if(isset($_POST['admin_messages_count']))
{
    $_adminObj->_messagesBadgeCounter();    
} //

