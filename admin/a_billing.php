<?php 
    // require global file
    require "_admin-config.php";

    // instantiate class
    $_adminObj = new AdminClass();
?>

<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title>Fast Service - Admin</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- top css file -->
    <?php include "_includes/_topCss.php";?>
</head>

<body>

    <section class="body" >

       
        <?php
            # include header ~ top-nav 
        require "_includes/_topNav.php";
        ?>

        <div class="inner-wrapper">
            <!-- start: sidebar -->

            <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">

                <div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">

                    <div class="sidebar-title">
                        FastService Corp
                    </div>
                    <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                       <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
                <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                    <div class="nano-content" >
                        <nav id="menu" class="nav-main" role="navigation" >
                            <ul class="nav nav-main" style="color:#607D8B;">
                                <li id="nav_admin_main">
                                    <a href="a_dashboard.php"><!--  onclick="loadTemplateView('main')" -->
                                         <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>

                                <li id="nav_admin_businessmgmt">

                                    <a href="a_business.php"> <!--  onclick="loadTemplateView('businessMgmt')" -->

                                      <i class="fa fa-money" aria-hidden="true"></i>

                                        <span>Business Management</span>

                                    </a>

                                </li>


                                <li id="nav_admin_prodservices">

                                    <a href="a_categories.php?firstOpen"><!--onclick="loadTemplateView('prod-serv')"-->

                                        <i class="fa fa-globe" aria-hidden="true"></i>

                                        <span>Products and Services</span>

                                    </a>

                                </li>

                                <li class="nav-active" id="nav_admin_billing">

                                    <a href="a_billing.php"><!--onclick="loadTemplateView('billing')"-->

                                        <i class="fa fa-exchange" aria-hidden="true"></i>

                                        <span>Billing</span>

                                    </a>

                                </li>

                                <li id="nav_admin_users">

                                    <a href="a_users.php"><!--onclick="loadTemplateView('users')"-->

										<i class="fa fa-users" aria-hidden="true"></i>

                                        <span id="usr">Human Resource</span>

                                    </a>

                                </li>


                                <li id="nav_admin_jobs">
                                    <a href="a_jobs.php"><!-- onclick="loadTemplateView('jobs')"-->

                                        <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
										<i class="fa fa-briefcase" aria-hidden="true"></i>
                                        <span>Jobs</span>
                                    </a>
                                </li>


                                <li id="nav_admin_adverts">
                                    <a href="a_adverts.php"><!--onclick="loadTemplateView('advertsMgmt')"-->

                                        <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                         <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span>Adverts Mgt</span>
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="a_marketSurvey.php"><!--onclick="loadTemplateView('mktSurvey')"-->

                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                        <span>Market Survey</span>
                                    </a>
                                </li>

                                <li id="nav_admin_reports">

                                    <a href="a_reports.php">

                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
                                    
                            <li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>

                                <li id="nav_admin_sysalerts">

                                    <a href="a_alerts.php"><!--onclick="loadTemplateView('sysAlerts')"-->

                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>

                                        <span>System Alerts</span>

                                    </a>

                                </li>

                                <li id="nav_admin_inbox">
                                    <a href="a_mailbox.php"><!--onclick="loadTemplateView('mail')"-->

                                        <span class="pull-right label label-primary admin-messages-count">0</span>
                                        
										<i class="fa fa-inbox" aria-hidden="true"></i>
                                        <span>Inbox</span>
										
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="index.php?logout">

                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                </li>

                            </ul>
                        </nav>

                        <hr class="separator">

                    </div>

                </div>

            </aside>

            <!-- end: sidebar -->

           <!-- right content -->
            <section role="main" class="content-body">
	
<!-- /. title-->
<div class="content-header">
  <h1>
    Billing
    <small>Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-exchange"></i> Home</a></li>
    <li class="active">Billing</li>
  </ol>
</div>

<div class="row">

<div class="col-md-8">
		
		
		<div class="panel-body">
		
		<!-- div for saving business subscription bill -->
		
		<div class="panel-body" style="background-color:#EEEEEE;display:none;" id="bizBill">
		
		<p><span onclick="shutdiv()" class="badge" style="background-color:#FFA726;cursor:pointer;">x</span> &nbsp;<font color="#00BCD4"><b>Setting Business Subscription Bill</b></font></p>
		
		<div id="afterBizBill"></div>
		<form id="bizBill_form">
		
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Item Name</font></label>
												
					<input type="text" name="item_name" value="business_subscription" class="form-control" readonly>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Billing type</font></label>
												
					<select type="text" name="billing_type"  class="form-control"> <option></option> <option>viewers</option> <option>duration</option> 
					
						<option>Monthly Income</option></select>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Min Value</font></label>
												
					<input type="number" name="min_value" class="form-control">
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Max Value</font></label>
												
					<input type="number" name="max_value" class="form-control">
											
						</div>
										
							</div>
							
			<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Fee</font></label>
												
					<input type="number" name="amount" class="form-control">
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Last Edit</font></label>
												
					<input type="date" name="last_edit" value="<?php echo date('Y-m-d') ?>" class="form-control" readonly>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">&nbsp;</font></label>
												
					<button type="button" onclick="saveBizBill()" class="form-control" style="background-color:#009688;color:#FFF;">Submit</button>
											
						</div>
										
							</div> </form>
							
			
		
			</div>
		

		
		
	<!-- div for saving advert bill -->
	
	
		
		<div class="panel-body" style="background-color:#EEEEEE;display:none;" id="advertBill">
		
		<p><span onclick="shutAdvert()" class="badge" style="background-color:#FFA726;cursor:pointer;">x</span> &nbsp;<font color="#00BCD4"><b>Setting advertising Bills</b></font></p>
		
		<div id="afterAdvertBill"></div>
		
		<form id="advertBill_form">
		
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Item Name</font></label>
												
					<input type="text" name="item_name" value="advert" class="form-control" readonly>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Billing type</font></label>
												
					<select type="text" name="billing_type"  class="form-control"> <option></option> <option>viewers</option> <option>duration</option> 
					
						<option>Monthly Income</option></select>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Min Value</font></label>
												
					<input type="number" name="min_value" class="form-control">
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Max Value</font></label>
												
					<input type="number" name="max_value" class="form-control">
											
						</div>
										
							</div>
							
			<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Fee</font></label>
												
					<input type="number" name="amount" class="form-control">
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Last Edit</font></label>
												
					<input type="date" name="last_edit" value="<?php echo date('Y-m-d') ?>" class="form-control" readonly>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">&nbsp;</font></label>
												
					<button type="button" onclick="saveAdvertBill()" class="form-control" style="background-color:#009688;color:#FFF;">Submit</button>
											
						</div>
										
							</div> </form>
							
			
		
			</div>
		
		
		
		
		<!-- div for saving job bill -->
	
	
		
		<div class="panel-body" style="background-color:#EEEEEE;display:none;" id="jobBill">
		
		<p><span onclick="shutJob()" class="badge" style="background-color:#FFA726;cursor:pointer;">x</span> &nbsp;<font color="#00BCD4"><b>Setting Bills for Posting Jobs</b></font></p>
		
		<div id="afterJobBill"></div>
		
		<form id="jobBill_form">
		
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Item Name</font></label>
												
					<input type="text" name="item_name" value="job" class="form-control" readonly>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Billing type</font></label>
												
					<select type="text" name="billing_type"  class="form-control"> <option></option> <option>viewers</option> <option>duration</option> 
					
						<option>Monthly Income</option></select>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Min Value</font></label>
												
					<input type="number" name="min_value" class="form-control">
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Max Value</font></label>
												
					<input type="number" name="max_value" class="form-control">
											
						</div>
										
							</div>
							
			<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Fee</font></label>
												
					<input type="number" name="amount" class="form-control">
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">Last Edit</font></label>
												
					<input type="date" name="last_edit" value="<?php echo date('Y-m-d') ?>" class="form-control" readonly>
											
						</div>
										
							</div>
							
		<div class="col-sm-8">
			
			<div class="form-group">
					
				<label class="control-label"><font color="#212121">&nbsp;</font></label>
												
					<button type="button" onclick="saveJobBill()" class="form-control" style="background-color:#009688;color:#FFF;">Submit</button>
											
						</div>
										
							</div> </form>
							
			
		
			</div>
		
		
		
			<div class="col-md-12" id="hideBiz">

							<section class="panel panel-featured panel-featured-success">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<center><h6 class="panel-title"> Settings for business Subscription Billing</h6></center>
									
								</header>
								<div class="panel-body">
								
									<table class="table table-striped">
    <thead>
      <tr>
        <th>Billing type</th>
        <th>From (value)</th>
        <th>To (value)</th>
		 <th>Fee</th>
		 <th>Modify</th>
      </tr>
    </thead>
    <tbody>
	
	<?php $neq = "SELECT *from billing WHERE item_name = 'business_subscription'";
	
			$exg = $dbh->prepare($neq);
					
				$exg->execute();
						
					$po = $exg->fetchAll( PDO::FETCH_ASSOC); 
					
						foreach($po as $bo){?>
      <tr>
	  
        <td><?php echo  $bo['billing_type'] ?></td>
        
			<td> <?php echo  $bo['min_value'] ?> </td>
			
				<td> <?php echo  $bo['max_value'] ?> </td>
				
					<td> <?php echo  $bo['amount'] ?> </td>
				
					<td> <span onclick="" class="badge" style="background-color:#FFA726;cursor:pointer;">x</span> &nbsp;&nbsp;

					<span onclick="" class="badge" style="background-color:teal;cursor:pointer;"><span class="glyphicon glyphicon-edit"></span></span></td>
		
      </tr>
	  
						<?php  } ?>
      
    </tbody>
  </table>
									
								</div>
							</section>
						</div>
						
						
					
						
	
	
	<div class="col-md-12"  id="hideAdvert">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<center><h6 class="panel-title"> Settings for adverts Billing</h6></center>
									
								</header>
								<div class="panel-body">
	<table class="table table-striped">
    <thead>
      <tr>
        <th>Billing type</th>
        <th>From (value)</th>
        <th>To (value)</th>
		 <th>Fee</th>
		 <th>Modify</th>
      </tr>
    </thead>
    <tbody>
	
	<?php $neq = "SELECT *from billing WHERE item_name = 'advert'";
	
			$exg = $dbh->prepare($neq);
					
				$exg->execute();
						
					$po = $exg->fetchAll( PDO::FETCH_ASSOC); 
					
						foreach($po as $bo){?>
      <tr>
	  
        <td><?php echo  $bo['billing_type'] ?></td>
        
			<td> <?php echo  $bo['min_value'] ?> </td>
			
				<td> <?php echo  $bo['max_value'] ?> </td>
				
					<td> <?php echo  $bo['amount'] ?> </td>
				
					<td> <span onclick="" class="badge" style="background-color:#FFA726;cursor:pointer;">x</span> &nbsp;&nbsp;

					<span onclick="" class="badge" style="background-color:teal;cursor:pointer;"><span class="glyphicon glyphicon-edit"></span></span></td>
		
      </tr>
	  
						<?php  } ?>
      
    </tbody>
	
  </table>
	
	
	
	
								</div>
							</section>
						</div>
						
						
	<div class="col-md-12"  id="hideJob">
							<section class="panel panel-featured panel-featured-success">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<center><h6 class="panel-title"> Settings for Jobs' Billing</h6></center>
									
								</header>
								<div class="panel-body">
		<table class="table table-striped">
    <thead>
      <tr>
        <th>Billing type</th>
        <th>From (value)</th>
        <th>To (value)</th>
		 <th>Fee</th>
		 <th>Modify</th>
      </tr>
    </thead>
    <tbody>
	
	<?php $neq = "SELECT *from billing WHERE item_name = 'job'";
	
			$exg = $dbh->prepare($neq);
					
				$exg->execute();
						
					$po = $exg->fetchAll( PDO::FETCH_ASSOC); 
					
						foreach($po as $bo){?>
      <tr>
	  
        <td><?php echo  $bo['billing_type'] ?></td>
        
			<td> <?php echo  $bo['min_value'] ?> </td>
			
				<td> <?php echo  $bo['max_value'] ?> </td>
				
					<td> <?php echo  $bo['amount'] ?> </td>
				
					<td> <span onclick="" class="badge" style="background-color:#FFA726;cursor:pointer;">x</span> &nbsp;&nbsp;

					<span onclick="" class="badge" style="background-color:teal;cursor:pointer;"><span class="glyphicon glyphicon-edit"></span></span></td>
		
      </tr>
	  
						<?php  } ?>
      
    </tbody>
	
  </table>							
									
									
								</div>
							</section>
						</div>
						
						</div>
						
						</div>


<div class="col-md-4">
		
		<div class="panel-body">
		
		<div class="col-md-12 col-lg-12">
								
								<section class="panel panel-featured-bottom panel-featured-success">
									<div class="panel-body">
										<div class="widget-summary widget-summary-xs">
											<div class="widget-summary-col widget-summary-col-icon">
												<div class="summary-icon bg-tertiary">
													<i class="fa fa-building" onclick="addBizBill()" style="cursor:pointer;"></i>
												</div>
											</div>
											<div class="widget-summary-col">
												<div class="summary">
													<h4 class="title">New bill setting-Subscription</h4>
													<div class="info">
														<strong class="amount">&nbsp;</strong>
														<span class="text-primary">&nbsp;</span>
													</div>
												</div>
												<div class="summary-footer">
													<a class="text-muted text-uppercase">(view all)</a>
												</div>
											</div>
										</div>
									</div>
								</section>
							</div>
							
				<hr>			
			
			<div class="col-md-12 col-lg-12">
								
								<section class="panel panel-featured-bottom panel-featured-info">
									<div class="panel-body">
										<div class="widget-summary widget-summary-xs">
											<div class="widget-summary-col widget-summary-col-icon">
												<div class="summary-icon bg-primary">
													<i class="fa fa-bell" style="cursor:pointer;" onclick="addAdvertBill()"></i>
												</div>
											</div>
											<div class="widget-summary-col">
												<div class="summary">
													<h4 class="title">New bill setting-Adverts</h4>
													<div class="info">
														<strong class="amount">&nbsp;</strong>
														<span class="text-primary">&nbsp;</span>
													</div>
												</div>
												<div class="summary-footer">
													<a class="text-muted text-uppercase">(view all)</a>
												</div>
											</div>
										</div>
									</div>
								</section>
							</div>
							
							
							
				<div class="col-md-12 col-lg-12">
								
								<section class="panel panel-featured-bottom panel-featured-success">
									<div class="panel-body">
										<div class="widget-summary widget-summary-xs">
											<div class="widget-summary-col widget-summary-col-icon">
												<div class="summary-icon bg-tertiary">
													<i class="fa fa-graduation-cap" onclick="addJobBill()"  style="cursor:pointer;"></i>
												</div>
											</div>
											<div class="widget-summary-col">
												<div class="summary">
													<h4 class="title">New bill setting-Jobs</h4>
													<div class="info">
														<strong class="amount">&nbsp;</strong>
														<span class="text-primary">&nbsp;</span>
													</div>
												</div>
												<div class="summary-footer">
													<a class="text-muted text-uppercase">(view all)</a>
												</div>
											</div>
										</div>
									</div>
								</section>
							</div>
							
			
			
			<div class="col-md-12 col-lg-12">
								
								<section class="panel panel-featured-bottom panel-featured-info">
									<div class="panel-body">
										<div class="widget-summary widget-summary-xs">
											<div class="widget-summary-col widget-summary-col-icon">
												<div class="summary-icon bg-primary">
													<i class="fa fa-certificate" style="cursor:pointer;"></i>
												</div>
											</div>
											<div class="widget-summary-col">
												<div class="summary">
													<h4 class="title">Other billings</h4>
													<div class="info">
														<strong class="amount">&nbsp;</strong>
														<span class="text-primary">&nbsp;</span>
													</div>
												</div>
												<div class="summary-footer">
													<a class="text-muted text-uppercase">(view all)</a>
												</div>
											</div>
										</div>
									</div>
								</section>
							</div>
		
		
		</div>
		
		</div>

		</div>
	<!-- row -->

            </section>
            <!-- /. content-body -->

    </div>
    <!-- /. inner-wrapper -->

</section>
<!-- /. body -->
 


<!-- /. require plugins and scripts -->				
<?php  include '_includes/_bottomJs.php';?>