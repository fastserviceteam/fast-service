<?php 
    // require global file
    require "_admin-config.php";

    // instantiate class
    $_adminObj = new AdminClass();

?>

<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title>Fast Service - Admin</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- top css file -->
    <?php include "_includes/_topCss.php";?>
</head>

<body>

    <section class="body" >

       
        <?php
            # include header ~ top-nav 
        require "_includes/_topNav.php";
        ?>

        <div class="inner-wrapper">
            <!-- start: sidebar -->

            <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">

                <div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">

                    <div class="sidebar-title">
                        FastService Corp
                    </div>
                    <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                       <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
                <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                    <div class="nano-content" >
                        <nav id="menu" class="nav-main" role="navigation" >
                            <ul class="nav nav-main" style="color:#607D8B;">
                                <li id="nav_admin_main">
                                    <a href="a_dashboard.php"><!--  onclick="loadTemplateView('main')" -->
                                         <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>

                                <li id="nav_admin_businessmgmt">

                                    <a href="a_business.php"> <!--  onclick="loadTemplateView('businessMgmt')" -->

                                      <i class="fa fa-money" aria-hidden="true"></i>

                                        <span>Business Management</span>

                                    </a>

                                </li>


                                <li id="nav_admin_prodservices">

                                    <a href="a_categories.php?firstOpen"><!--onclick="loadTemplateView('prod-serv')"-->

                                        <i class="fa fa-globe" aria-hidden="true"></i>

                                        <span>Products and Services</span>

                                    </a>

                                </li>

                                <li id="nav_admin_billing">

                                    <a href="a_billing.php"><!--onclick="loadTemplateView('billing')"-->

                                        <i class="fa fa-exchange" aria-hidden="true"></i>

                                        <span>Billing</span>

                                    </a>

                                </li>

                                <li class="nav-active" id="nav_admin_users">

                                    <a href="a_users.php"><!--onclick="loadTemplateView('users')"-->

										<i class="fa fa-users" aria-hidden="true"></i>

                                        <span id="usr">Human Resource</span>

                                    </a>

                                </li>


                                <li id="nav_admin_jobs">
                                    <a href="a_jobs.php"><!-- onclick="loadTemplateView('jobs')"-->

                                        <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
										<i class="fa fa-briefcase" aria-hidden="true"></i>
                                        <span>Jobs</span>
                                    </a>
                                </li>


                                <li id="nav_admin_adverts">
                                    <a href="a_adverts.php"><!--onclick="loadTemplateView('advertsMgmt')"-->

                                        <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                         <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span>Adverts Mgt</span>
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="a_marketSurvey.php"><!--onclick="loadTemplateView('mktSurvey')"-->

                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                        <span>Market Survey</span>
                                    </a>
                                </li>

                                <li id="nav_admin_reports">

                                    <a href="a_reports.php">

                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
                                    
                            <li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>

                                <li id="nav_admin_sysalerts">

                                    <a href="a_alerts.php"><!--onclick="loadTemplateView('sysAlerts')"-->

                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>

                                        <span>System Alerts</span>

                                    </a>

                                </li>

                                <li id="nav_admin_inbox">
                                    <a href="a_mailbox.php"><!--onclick="loadTemplateView('mail')"-->

                                        <span class="pull-right label label-primary admin-messages-count">0</span>
                                        
										<i class="fa fa-inbox" aria-hidden="true"></i>
                                        <span>Inbox</span>
										
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="index.php?logout">

                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                </li>

                            </ul>
                        </nav>

                        <hr class="separator">

                    </div>

                </div>

            </aside>

            <!-- end: sidebar -->

           <!-- right content -->
            <section role="main" class="content-body">
	
<!-- /. title-->
<div class="content-header">
  <h1>
    Human Resource
    <small>Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-users"></i> Home</a></li>
    <li class="active">Human Resource</li>
  </ol>
</div>

<div class="row">

<div class="col-md-12">

<div class="panel-body">

<table class="table table-condensed" id="hideTable">
    <thead>
      <tr>
        <th>Names</th>
        <th>User type</th>
		<th>Contact</th>
        <th>Account created on </th>
		<th>Actions  &nbsp;&nbsp;<span class="badge" onclick="showReg()" style="background-color:#0097A7;cursor:pointer;">+</span>&nbsp;<font color="#00BCD4" font face="times new roman">Add user</font></th>
      </tr>
    </thead>
    <tbody> 
	
	
<?php 


//querrying all categories ..getting count for categories
							
							
	$qn = "SELECT *FROM  fs_staff";
			
		$qr = $dbh->prepare($qn);
				
			$qr->execute();
					
				$get = $qr->fetchAll(PDO::FETCH_ASSOC); 
				
					foreach($get as $wo){
				
				?>
				
			<tr><td> <?php echo $wo['fname']." ".$wo['lname'] ?></td>	
			
				<td> <?php echo $wo['position'] ?></td> 
				
					<td> <?php echo $wo['date_reg'] ?></td> 
					
						<td> <?php echo $wo['phone_no'] ?></td> 
						
								<td> <i data-saffid="<?php echo $wo['staff_id'] ?>" data-toggle="modal" data-target="#zoomOutIn10" onclick="getStaff(this)" class="fa fa-edit" title="Edit user details" style="cursor:pointer;"></i>  &nbsp; &nbsp;
								
								
									<i class="fa fa-user" title="User privileges and permissions" style="cursor:pointer;"></i>  &nbsp; &nbsp;
									
											<i class="fa fa-trash-o" title="Delete user" style="cursor:pointer;"></i>  </td> </tr>
	
					<?php } ?>
	</tbody>
	</table>
	
	
	<div class="col-md-6" id="profForm" style="display:none;">
	
	
	<div id="afterStaff"></div>
			
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										
									</div>

									<h2 class="panel-title"><span style="cursor:pointer;" class="glyphicon glyphicon-user"></span> User Profile &nbsp;&nbsp;</h2>	
									
									
								</header>
								
								<div class="panel-body bg-white">
								
		<div style="width:100%;height:auto;overflow:auto;color:#424242;padding:1.5em;">  
			
	<form  id="staff_form" enctype="multipart/form-data" >
			
			<div class="form-group">
				
				<label class="col-md-12 control-label">First Name</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="fname"  type="text">
												
							</div>
								</div>	
								
		<input  name="date_reg" type="hidden" value="<?php echo date('Y-m-d')?>">
	
			<div class="form-group">
				
				<label class="col-md-12 control-label" >Last Name</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="lname"  type="text">
												
							</div>
								</div>
								
			<div class="form-group">
				
				<label class="col-md-12 control-label">Date of birth</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="dob" type="text">
												
							</div>
								</div>
								
		<div class="form-group">
				
				<label class="col-md-12 control-label" >Phone Contact</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="phone_no" type="text">
												
							</div>
								</div>
								
		<div class="form-group">
				
				<label class="col-md-12 control-label" >Current Address</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="current_address"  type="text">
												
							</div>
								</div>
								
		<div class="form-group">
				
				<label class="col-md-12 control-label">Permanent Address</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="permanent_address" type="text">
												
							</div>
							
								</div>
								
		<div class="form-group">
				
				<label class="col-md-12 control-label" >National Id No</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="id_no"  type="text">
												
							</div>
							
								</div>
							

</div>							
								</div>
								
								</section>
							</div>
							
							
							
							
							
							
	
	<div class="col-md-6" id="logDetail" style="display:none;">
			
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<span onclick="closeReg()" class="badge" style="background-color:#0097A7;cursor:pointer;">x</span>
									</div>

									<h2 class="panel-title"><span style="cursor:pointer;" class="glyphicon glyphicon-off"></span> Position and Login Details &nbsp;&nbsp; </h2>
									
									
								</header>
								
								<div class="panel-body bg-white">
								
		<div style="width:100%;height:auto;overflow:auto;color:#424242;padding:1.5em;">  
								
			
			<div class="form-group">
				
				<label class="col-md-12 control-label" >Position in <i><b>FS</b></i></label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="position"  type="text">
												
							</div>
							
								</div>	
								
			<div class="form-group">
				
				<label class="col-md-12 control-label" >Work Station (Area)</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="workstation" type="text">
												
							</div>
							
								</div>	
								
								
			
			<div class="form-group">
				
				<label class="col-md-12 control-label" >Email Address</label>
												
					<div class="col-md-12">
													
						<input class="form-control"  name="ur_email" type="text">
												
							</div>
							
								</div>	
								
			<div class="form-group">
				
				<label class="col-md-12 control-label" >User type</label>
												
					<div class="col-md-12">
													
						<input class="form-control" type="text" name="ur_type" >
												
							</div>
							
								</div>	
								
			<div class="form-group">
				
				<label class="col-md-12 control-label" >User password</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="ur_pas" type="text" required>
												
							</div>
							
								</div>
								
			<div class="form-group">
				
				<label class="col-md-12 control-label" for="inputFocus">Upload user photo</label>
												
					<div class="col-md-12">
													
						<input class="form-control" name="image_name" type="file">
												
							</div>
							
								</div>
					
			<div class="form-group">
				
				<label class="col-md-12 control-label" for="inputFocus">&nbsp;</label>
												
					<div class="col-md-12">
													
						<button class="form-control" style="background-color:#00796B;color:#FFF;" type="button" onclick="newStaff()"> Save</button>
												
							</div>
							
								</div>
								
								</form>
							

</div>							
								</div>
								
								</section>
							</div>
	
	
	
	
	<?php 
	
	if(isset($_REQUEST['afterStaff'])){
	
	?>
	<body onload="launch_toast()"></body>

<div id="toast"><div id="img"><li class="fa fa-user"></li></div><div id="desc">Operation successful..staff saved....</div></div>

<?php } ?>
	
	
	<script>
	
	//notification after saving a user
	
	function launch_toast() {
   
		var x = document.getElementById("toast")
    
				x.className = "show";
    
					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
			}
	
	</script>
	 
	 
	
		</div>
		<!-- col-md-12 -->

	</div>
	<!-- /. panel -->

</div>
<!-- row -->


<!-- modal for updating user details -->
 
  <div class="modal fade " id="zoomOutIn10" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
      <div class="modal-dialog modal-lg" role="document">
	  
         <div class="modal-content">
		 
            <div class="modal-header">
			
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			   
               <h4 class="modal-title" id="myModalLabel"><center><b><font color="#00ACC1">Update staff details</font></b></center></h4>
			   
            </div>
			
            <div class="modal-body" style="padding:3em;">
			
			<div id="editStaff">


			</div>
	
            
			</div>
			
            <div class="modal-footer">  
			   
		<button type="button" class="btn btn-primary"> Save updates</button>
			   
			   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			   
            </div>
         </div>
      </div>
   </div> 


            </section>
            <!-- /. content-body -->

    </div>
    <!-- /. inner-wrapper -->

</section>
<!-- /. body -->
 


<!-- /. require plugins and scripts -->				
<?php  include '_includes/_bottomJs.php';?>