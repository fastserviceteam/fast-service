<?php

    // require global file
    require "_admin-config.php";

    // instantiate class
    $_adminObj = new AdminClass();

    //if(isset($_SESSION['fastService_fadminEmail_Session']))
    if($_adminObj->_isLoggedIn() == true)
    {
        header("Location: a_dashboard.php");
    }

    else
    {
       header("Location: login.php");
    }
