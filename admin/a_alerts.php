<?php 
    // require global file
    require "_admin-config.php";

    // instantiate class
    $_adminObj = new AdminClass();
?>

<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title>Fast Service - Admin</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- top css file -->
    <?php include "_includes/_topCss.php";?>
</head>

<body>

    <section class="body" >

       
        <?php
            # include header ~ top-nav 
        require "_includes/_topNav.php";
        ?>

        <div class="inner-wrapper">
            <!-- start: sidebar -->

            <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">

                <div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">

                    <div class="sidebar-title">
                        FastService Corp
                    </div>
                    <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                       <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
                <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                    <div class="nano-content" >
                        <nav id="menu" class="nav-main" role="navigation" >
                            <ul class="nav nav-main" style="color:#607D8B;">
                                <li id="nav_admin_main">
                                    <a href="a_dashboard.php"><!--  onclick="loadTemplateView('main')" -->
                                         <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>

                                <li id="nav_admin_businessmgmt">

                                    <a href="a_business.php"> <!--  onclick="loadTemplateView('businessMgmt')" -->

                                      <i class="fa fa-money" aria-hidden="true"></i>

                                        <span>Business Management</span>

                                    </a>

                                </li>


                                <li id="nav_admin_prodservices">

                                    <a href="a_categories.php?firstOpen"><!--onclick="loadTemplateView('prod-serv')"-->

                                        <i class="fa fa-globe" aria-hidden="true"></i>

                                        <span>Products and Services</span>

                                    </a>

                                </li>

                                <li id="nav_admin_billing">

                                    <a href="a_billing.php"><!--onclick="loadTemplateView('billing')"-->

                                        <i class="fa fa-exchange" aria-hidden="true"></i>

                                        <span>Billing</span>

                                    </a>

                                </li>

                                <li id="nav_admin_users">

                                    <a href="a_users.php"><!--onclick="loadTemplateView('users')"-->

										<i class="fa fa-users" aria-hidden="true"></i>

                                        <span id="usr">Human Resource</span>

                                    </a>

                                </li>


                                <li id="nav_admin_jobs">
                                    <a href="a_jobs.php"><!-- onclick="loadTemplateView('jobs')"-->

                                        <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
										<i class="fa fa-briefcase" aria-hidden="true"></i>
                                        <span>Jobs</span>
                                    </a>
                                </li>


                                <li id="nav_admin_adverts">
                                    <a href="a_adverts.php"><!--onclick="loadTemplateView('advertsMgmt')"-->

                                        <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                         <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span>Adverts Mgt</span>
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="a_marketSurvey.php"><!--onclick="loadTemplateView('mktSurvey')"-->

                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                        <span>Market Survey</span>
                                    </a>
                                </li>

                                <li id="nav_admin_reports">

                                    <a href="a_reports.php">

                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
                                    
                            <li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>

                                <li class="nav-active" id="nav_admin_sysalerts">

                                    <a href="a_alerts.php"><!--onclick="loadTemplateView('sysAlerts')"-->

                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>

                                        <span>System Alerts</span>

                                    </a>

                                </li>

                                <li id="nav_admin_inbox">
                                    <a href="a_mailbox.php"><!--onclick="loadTemplateView('mail')"-->

                                        <span class="pull-right label label-primary admin-messages-count">0</span>
                                        
										<i class="fa fa-inbox" aria-hidden="true"></i>
                                        <span>Inbox</span>
										
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="index.php?logout">

                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                </li>

                            </ul>
                        </nav>

                        <hr class="separator">

                    </div>

                </div>

            </aside>

            <!-- end: sidebar -->

           <!-- right content -->
            <section role="main" class="content-body">

<!-- /. title-->
<div class="content-header">
  <h1>
    System
    <small>Alerts</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-bullhorn"></i> Home</a></li>
    <li class="active">System Alerts</li>
  </ol>
</div>

<div class="row">

<div class="col-md-8">
		
		<div class="panel-body">
		
		<div class="search-control-wrapper">
							<form action="pages-search-results.html">
								<div class="form-group">
									<div class="input-group">
										<input type="text" class="form-control" onkeyup="run" placeholder="Such by business names">
										<span class="input-group-btn">
											<button class="btn btn-default" style="background-color:#E0F7FA;" type="button">Search</button>
										</span>
									</div>
								</div>
							</form>
						</div>
						
			</div>
			
			
				</div>
 



 
<div class="col-md-4">
		
		<div class="panel-body">
	
		
			<ul class="list-group">
			
				<li class="list-group-item" style="background-color:#C8E6C9;"><center> <b>Alerts History</b></center></li>
  
					<li class="list-group-item">Deleted <span class="badge">5</span></li> 
 
						<li class="list-group-item">Warnings <span class="badge">3</span></li> 

							</ul>

		
			</div>
			
			
				</div>

	
</div>


            </section>
            <!-- /. content-body -->

    </div>
    <!-- /. inner-wrapper -->

</section>
<!-- /. body -->
 


<!-- /. require plugins and scripts -->				
<?php  include '_includes/_bottomJs.php';?>

