<!-- ================= >> JS/JQ plugins and Scripts << ================= -->

<!-- function calls -->
<script src="_assets/js/fastservice-function-calls.js"></script>
<!-- query -->
<script src="_assets/plugins/jquery/jquery-2.2.3.min.js"></script>
<!-- bootstrap -->
<script src="_assets/plugins/bootstrap/js/bootstrap.js"></script>
		
<!-- scroll to fixed -->
<script src="_assets/plugins/ScrollToFixed-master/jquery-scrolltofixed.js"></script>
<!--  bootstrap-editable js -->
<script src="_assets/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script>
<!-- bootstrap-datepicker -->
<script src="_assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- bootstrap-multiselect -->
<script src="_assets/plugins/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<!-- jquery-browser-mobile -->
<script src="_assets/plugins/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<!-- jquery-ui-1 -->
<script src="_assets/plugins/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
<!-- jquery-ui-touch-punch -->
<script src="_assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
<!-- nanoscroller -->
<script src="_assets/plugins/nanoscroller/nanoscroller.js"></script>
<!-- magnific-popup -->
<script src="_assets/plugins/magnific-popup/magnific-popup.js"></script>
<!-- jquery-placeholder -->
<script src="_assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
<!-- jquery.appear -->
<script src="_assets/plugins/jquery-appear/jquery.appear.js"></script>
<!-- select2 -->
<script src="_assets/plugins/select2/select2.js"></script>
<!-- datatables -->
<script src="_assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="_assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- datatables extensions to offer extra functionalities -->
<script src="_assets/plugins/datatables/Buttons/js/dataTables.buttons.min.js"></script>
<script src="_assets/plugins/datatables/jszip/dist/jszip.min.js"></script>
<script src="_assets/plugins/datatables/pdfmake/pdfmake.js"></script>
<script src="_assets/plugins/datatables/pdfmake/vfs_fonts.js"></script>
<script src="_assets/plugins/datatables/Buttons/js/buttons.html5.min.js"></script>
<script src="_assets/plugins/datatables/Buttons/js/buttons.print.min.js"></script>
<!--
<script src="_assets/plugins/jquery-datatables/media/js/jquery.dataTables.js"></script>
<script src="_assets/plugins/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="_assets/plugins/jquery-datatables-bs3/assets/js/datatables.js"></script>
-->
<!-- pnotify -->
<script src="_assets/plugins/pnotify/pnotify.custom.js"></script>
<!-- Custom scripts -->
<script src="_assets/js/fastservice-script.js"></script>
<!-- Charts and Graphs Plugins -->
<!--
<script src="_assets/plugins/jquery-easypiechart/jquery.easypiechart.js"></script>
<script src="_assets/plugins/flot/jquery.flot.js"></script>
<script src="_assets/plugins/flot-tooltip/jquery.flot.tooltip.js"></script>
<script src="_assets/plugins/flot/jquery.flot.pie.js"></script>
<script src="_assets/plugins/flot/jquery.flot.categories.js"></script>
<script src="_assets/plugins/flot/jquery.flot.resize.js"></script>
<script src="_assets/plugins/raphael/raphael.js"></script>
<script src="_assets/plugins/morris/morris.js"></script>
<script src="_assets/plugins/snap-svg/snap.svg.js"></script>
<script src="_assets/plugins/liquid-meter/liquid.meter.js"></script>
<script src="_assets/plugins/jqvmap/jquery.vmap.js"></script>
<script src="_assets/plugins/jqvmap/data/jquery.vmap.sampledata.js"></script>
<script src="_assets/plugins/jqvmap/maps/jquery.vmap.world.js"></script>
<script src="_assets/plugins/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
<script src="_assets/plugins/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
<script src="_assets/plugins/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
<script src="_assets/plugins/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
<script src="_assets/plugins/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
<script src="_assets/plugins/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
-->
<!-- marquee -->
<script src="_assets/plugins/marquee/jquery.marquee.min.js"></script>
<!-- owl-carousel -->
<script src="_assets/plugins/owl-carousel/owl.carousel.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="_assets/js/theme.js"></script>
<!-- custom js -->
<script src="_assets/js/adminCustomScript.js"></script>
<!-- Theme Initialization Files -->
<script src="_assets/js/theme.init.js"></script>
<!--for running modals -->
<script src="_assets/js/ui-elements/examples.modals.js"></script>
<!-- Examples; left this for notification -->
<script src="_assets/js/ui-elements/examples.notifications.js"></script>
<!-- Examples; left this for future graph displays -->
<!--<script src="_assets/js/ui-elements/examples.charts.js"></script>-->
<!-- modal views -->
<?php include "_includes/_modalViews.php"; ?>  
