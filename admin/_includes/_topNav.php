<!-- start: header -->
<header class="header" id="header_top_navigation">
    <div style="width:100%;background-color:#009688;height:100%;float:left;border-bottom:3px solid #00BCD4">
        <a href="?" class="logo">
            <div style="background-color:#FFF;padding:0.2em;box-sizing:border-box;border-radius:30px;"><img src="<?php echo ff_logo; ?>" class="img-circle" id="ff-logo" alt="logo">
            </div>
        </a>
        <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
        <!-- start: search & user box -->
        <div class="header-right">
            <ul class="notifications">
                <li>
                    <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                        <i class="fa fa-tasks"></i>
                        <span class="badge">3</span>
                    </a>
                    <div class="dropdown-menu notification-menu large">
                        <div class="notification-title">
                            <span class="pull-right label label-default">3</span>
                            My Tasks
                        </div>
                        <div class="content">
                            <ul>
                                <li>
                                    <p class="clearfix mb-xs">
                                        <span class="message pull-left">Generating Sales Report</span>
                                        <span class="message pull-right text-dark">60%</span>
                                    </p>
                                    <div class="progress progress-xs light">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                        <i class="fa fa-envelope" title="Messages"></i>
                        <span class="badge admin-messages-count">0</span>
                    </a>
                    <div class="dropdown-menu notification-menu">
                        <div class="notification-title">
                            <span class="pull-right label label-default admin-messages-count">0</span>
                            Messages
                        </div>
                        <div class="content" id="admin-messages-notifn">
                        </div>
                    </div>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                        <i class="fa fa-bell"></i>
                        <span class="badge ad_jobs_business_span">0</span>
                    </a>
                    <div class="dropdown-menu notification-menu">
                        <div class="notification-title">
                            <span class="pull-right label label-default ad_jobs_business_span">0</span>
                            Alerts
                        </div>
                        <div class="content">
                            <ul>
                                <li>
                                    <a href="#JobsApproval-Modal" class="clearfix" data-toggle="modal">
                                        <div class="image">
                                            <i class="fa fa-graduation-cap blue-gradient-rgba  text-white"></i>
                                        </div>
                                        <span class="title mdb-color-text">Jobs Awaiting Approval</span>
                                        <span class="message text-danger" id="jobs-awaiting"></span>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#advertApproval-Modal" class="clearfix" data-toggle="modal">
                                        <div class="image">
                                            <i class="fa fa-info-circle blue-gradient-rgba text-white"></i>
                                        </div>
                                        <span class="title mdb-color-text">Advert Awaiting Approval</span>
                                        <span class="message text-danger" id="adverts-awaiting"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            <span class="separator"></span>
            <div id="userbox" class="userbox">
                <a href="#" data-toggle="dropdown">
                    <figure class="profile-picture">
                        <!--<img src="_assets/images/userm.jpg" class="img-circle" />-->
                        <span id="fa-user-icon"><i class="fa fa-user fa-2x"></i></span>
                    </figure>
                    <div class="profile-info" data-lock-name="Banxie" data-lock-email="fastservice.com">
                        <span class="name">
                            <font color="cyan"> <?php  echo $_SESSION['fastService_uadminEmail_Session']; ?></font>
                        </span>
                        <span class="role">
                            <font color="#FFF"><?php echo $_SESSION["fastService_adminPriviledge"]; ?></font>
                        </span>
                    </div>
                    <i class="fa custom-caret"></i>
                </a>
                <div class="dropdown-menu">
                    <ul class="list-unstyled">
                        <li class="divider"></li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="userProfile.php"><i class="fa fa-user"></i> My Profile</a>
                        </li>
                        <li>
                            <a role="menuitem" tabindex="-1" href="index.php?logout"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>