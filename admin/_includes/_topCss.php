<!-- bootstrap CSS -->
<link rel="stylesheet" href="_assets/plugins/bootstrap/css/bootstrap.css" />
<!-- font-awesome CSS -->
<link rel="stylesheet" href="_assets/plugins/font-awesome/css/font-awesome.css" />
<!--  bootstrap-editable css -->
<link rel="stylesheet" href="_assets/plugins/bootstrap-editable/css/bootstrap-editable.css">
<!-- magnific-popup CSS -->
<link rel="stylesheet" href="_assets/plugins/magnific-popup/magnific-popup.css" />
<!-- bootstrap-datepicker CSS -->
<link rel="stylesheet" href="_assets/plugins/bootstrap-datepicker/css/datepicker3.css" />
<!-- pnotify CSS -->
<link rel="stylesheet" href="_assets/plugins/pnotify/pnotify.custom.css">
<!-- owl-carousel -->
<link rel="stylesheet" href="_assets/plugins/owl-carousel/owl.carousel.css" />
<link rel="stylesheet" href="_assets/plugins/owl-carousel/owl.theme.css" />
<!-- jquery-ui -->
<link rel="stylesheet" href="_assets/plugins/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
<!-- bootstrap-multiselect -->
<link rel="stylesheet" href="_assets/plugins/bootstrap-multiselect/bootstrap-multiselect.css" />
<!-- datatable -->
<link rel="stylesheet" href="_assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="_assets/plugins/datatables/Buttons/css/buttons.bootstrap.min.css">

<!--<link rel="stylesheet" href="_assets/plugins/jquery-datatables-bs3/assets/css/datatables.css">-->
<!--<link rel="stylesheet" href="_assets/plugins/jquery-datatables/extras/TableTools/css/dataTables.tableTools.css">-->
<!--<link rel="stylesheet" href="_assets/plugins/morris/morris.css" />-->
<!-- nanoscroller -->
<link href="_assets/plugins/nanoscroller/nanoscroller.css" rel="stylesheet">
<!-- select2 -->
<link rel="stylesheet" href="_assets/plugins/select2/select2.css">
<!-- Theme CSS -->
<link rel="stylesheet" href="_assets/css/admin-theme.css" />
<!-- Skin CSS -->
<link rel="stylesheet" href="_assets/css/skins/default.css" />
<!-- modernizr -->
<script src="_assets/plugins/modernizr/modernizr.js"></script>