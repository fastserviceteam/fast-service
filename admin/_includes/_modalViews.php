<!-- ============= DECLARE ALL MODAL VIEWS HERE ============= -->


<!-- modal for viewing job details -->
<div class="modal fade " id="jobDetailsModal" tabindex="-1" role="dialog" aria-labelledby="jobModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header blue-grey darken-3">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title text-center text-white" id="jobModalLabel">
                    Job details
                </h4>
            </div>

            <div class="modal-body" style="padding:3em;">

                <form id="msgdelete_form">


                    <input id="msgId" type="hidden" name="message_id" value="">


                    <div id="displayJobs"></div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-warning">Approve</button> </form>

                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<!-- advert banner modal zoom -->
<!-- modal for zooming the image -->
<div class="modal fade " id="zoomOutIn9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <center><b>
                            <font color="#00ACC1">Advert Post-Zoomed</font>
                        </b></center>
                </h4>
            </div>
            <div class="modal-body" style="padding:3em;">
                <div id="advertImage"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- advert approval modal -->
<div class="modal fade" id="advertApproval-Modal" tabindex="-1" role="dialog" aria-labelledby="advertApprovalModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="advert-approval-modal-dialog">

        <div class="modal-content">
            <div class="modal-header blue-grey darken-3">
                <button type="button" class="close text-white" data-dismiss="modal">
                    <span aria-hidden="true"><i class="fa fa-times text-white"></i></span><span class="sr-only">Close</span>
                </button>

                <h4 class="modal-title text-center text-white" id="advertApprovalModalLabel">Advert Approvals</h4>
            </div>

            <div class="modal-body">
                <?php $_adminObj->_approveAdvertListModal(); ?>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- cancel advert modal -->
<div class="modal fade" id="advertCancel-Modal" tabindex="-1" role="dialog" aria-labelledby="advertCancelModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header blue-grey darken-3">
                <button type="button" class="close text-white" data-dismiss="modal">
                    <span aria-hidden="true"><i class="fa fa-times text-white"></i></span><span class="sr-only">Close</span>
                </button>

                <h4 class="modal-title text-center text-white" id="advertCancelModalLabel">Advert Cancel</h4>
            </div>

            <div class="modal-body">
                <div id="load_advert_cancellation_form"></div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!--
<div id="approveAdvertActionModal" class="modal-block mfp-hide">
	<section class="panel">
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text text-center">
					<p>Are you sure that you want to approve this advert?</p>
					<button class="btn btn-primary modal-confirm">Confirm</button>
					<button class="btn btn-default modal-dismiss">Cancel</button>
				</div>
			</div>
		</div>
	</section>
</div>
-->



<!-- jobs approval modal -->
<div class="modal fade" id="JobsApproval-Modal" tabindex="-1" role="dialog" aria-labelledby="jobApprovalModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header blue-grey darken-3">
                <button type="button" class="close text-white" data-dismiss="modal">
                    <span aria-hidden="true"><i class="fa fa-times text-white"></i></span><span class="sr-only">Close</span>
                </button>

                <h4 class="modal-title text-center text-white" id="jobApprovalModalLabel">Job Approvals</h4>
            </div>

            <div class="modal-body">

                <div class="jobs_approval_wrapper_list">
                    <?php echo $_adminObj->approveJobListModal(); ?>
                </div>
                <div class="jobs_approval_wrapper_details display-none">
                    <div class="job-approval-details-wrapper">
                    </div>
                    <!-- job-approval-details-wrapper -->
                </div>
                <!-- jobs_approval_wrapper_details --->

            </div>
            <div class="modal-footer">
                <button type="button" onclick="returnToJobApprovalList()" class="btn btn-flat btn-sm btn-warning display-none btn-job-action" title="Back to job list"><i class="fa fa-angle-left"></i> Back</button>
                <button type="button" id="approve-job-footer-btn" onclick="approveJobApproval()" class="btn btn-flat btn-sm btn-primary display-none btn-job-action" title="Back to job list"><i class="fa fa-check-circle-o"></i> Approve</button>
            </div>
        </div>
    </div>
</div>



<!-- modal for creating new industry -->
<div class="modal fade " id="zoomOutInM1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    <center><b>
                            <font color="#00ACC1">Add New Industry</font>
                        </b></center>
                </h4>

            </div>

            <div class="modal-body" style="padding:3em;">

                <form role="form" id="industForm">

                    <div class="form-group">

                        <label class="col-sm-12 control-label">Specify Industry:</label>

                        <div class="col-sm-12">

                            <input type="text" name="indust_name" class="form-control" required />

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-12 control-label">Save:</label>

                        <div class="col-sm-12">

                            <button type="submit" class="btn btn-block" id="dataBtn_newIndustry" style="background-color:#009688;color:#FFF;"> Submit </button>

                        </div>

                    </div>

                </form>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>



<!-- modal for creating new categories -->

<div class="modal fade " id="zoomOutInM2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    <center><b>
                            <font color="#00ACC1">Add New Category</font>
                        </b></center>
                </h4>

            </div>

            <div class="modal-body" style="padding:3em;">

                <p>
                    <font color="#000"><b>Industry Name</b>: <span id="industry"></span></font>
                </p>

                <form role="form" id="categoryForm">

                    <input type="hidden" name="industry_id" value="" id="industid" class="form-control">

                    <div class="form-group">

                        <label class="col-sm-4 control-label">Category Name:</label>

                        <div class="col-sm-8">

                            <input type="text" name="cat_name" class="form-control">

                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Save Category:</label>

                        <div class="col-sm-8">

                            <button type="submit" id="dataBtn_newCategory" class="form-control" style="background-color:teal;color:#FFF;"> Submit </button>
                        </div>
                    </div>

                </form>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>


<!-- adding new services -->

<div class="modal fade" id="modalBootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <center>Add service </center>
                </h4>
            </div>
            <div class="modal-body">

                <p><b>
                        <font color="#212121">Industry:&nbsp; &nbsp;</font>
                    </b>
                    <font color="#00B8D4" id="showName"> </font>


                    &nbsp; &nbsp;&nbsp;Category:&nbsp; &nbsp;</font></b>
                    <font color="#00B8D4" id="showCat"> </font>
                </p>


                <div id="afterServ"></div>

                <form id="serviceForm" role="form">

                    <input id="showId" type="hidden" name="cat_id" value="">


                    <div class="form-group">

                        <label class="col-sm-4 control-label">Service/Product:</label>

                        <div class="col-sm-8">

                            <input type="text" name="service_name" class="form-control" required />

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-4 control-label">click to save:</label>

                        <div class="col-sm-8">

                            <button type="submit" class="btn btn-block" style="background-color:teal;color:#FFF;" id="dataBtn_newService">Save</button>

                        </div>

                    </div>



                </form>
            </div>

            <div class="modal-footer">



                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>



            </div>

        </div>
    </div>
</div>


<!-- modal for viewing all the services -->

<div class="modal fade " id="zoomOutInM3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">
                    <center><b>
                            <font color="#00ACC1">Industry Summary</font>
                        </b></center>
                </h4>

            </div>

            <div class="modal-body" style="padding:3em;">


                <div id="industSum" style="color:#000"> </div>


            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>