<?php
// require global file
require "_admin-config.php";
// instantiate class
$_adminObj = new AdminClass();
?>
<!doctype html>
<html class="fixed">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>Fast Service - Admin</title>
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- top css file -->
		<?php include "_includes/_topCss.php"; ?>
		<!-- page specific css -->
		<style>
			.loading-overlay{
				position: relative;
				display: none;
			}
			.loading-overlay p{
				position: absolute;
				top: 0;
				margin: 50% 0 50% 0;
				text-align: center;
				left: 50%;
				z-index: 1;
			}
		</style>
	</head>
	<body>
		<section class="body">
			
			<?php
# include header ~ top-nav
require "_includes/_topNav.php";
?>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">
					<div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">
						<div class="sidebar-title">
							FastService Corp
						</div>
						<div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
					<div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
						<div class="nano-content" >
							<nav id="menu" class="nav-main" role="navigation" >
								<ul class="nav nav-main" style="color:#607D8B;">
									<li id="nav_admin_main">
										<a href="a_dashboard.php"><!--  onclick="loadTemplateView('main')" -->
										<i class="fa fa-dashboard" aria-hidden="true"></i>
										<span>Dashboard</span>
									</a>
								</li>
								<li id="nav_admin_businessmgmt">
									<a href="a_business.php"> <!--  onclick="loadTemplateView('businessMgmt')" -->
									<i class="fa fa-money" aria-hidden="true"></i>
									<span>Business Management</span>
								</a>
							</li>
							<li id="nav_admin_prodservices">
								<a href="a_categories.php?firstOpen"><!--onclick="loadTemplateView('prod-serv')"-->
								<i class="fa fa-globe" aria-hidden="true"></i>
								<span>Products and Services</span>
							</a>
						</li>
						<li id="nav_admin_billing">
							<a href="a_billing.php"><!--onclick="loadTemplateView('billing')"-->
							<i class="fa fa-exchange" aria-hidden="true"></i>
							<span>Billing</span>
						</a>
					</li>
					<li id="nav_admin_users">
						<a href="a_users.php"><!--onclick="loadTemplateView('users')"-->
						<i class="fa fa-users" aria-hidden="true"></i>
						<span id="usr">Human Resource</span>
					</a>
				</li>
				<li id="nav_admin_jobs">
					<a href="a_jobs.php"><!-- onclick="loadTemplateView('jobs')"-->
					<span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
					<i class="fa fa-briefcase" aria-hidden="true"></i>
					<span>Jobs</span>
				</a>
			</li>
			<li class="nav-active" id="nav_admin_adverts">
				<a href="a_adverts.php"><!--onclick="loadTemplateView('advertsMgmt')"-->
				<span class="pull-right label label-primary" id="adverts_nav_count">0</span>
				<i class="fa fa-info-circle" aria-hidden="true"></i>
				<span>Adverts Mgt</span>
			</a>
		</li>
		<li id="nav_admin_market_survey">
			<a href="a_marketSurvey.php"><!--onclick="loadTemplateView('mktSurvey')"-->
			<i class="fa fa-clipboard" aria-hidden="true"></i>
			<span>Market Survey</span>
		</a>
	</li>

                                <li id="nav_admin_reports">

                                    <a href="a_reports.php">

                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
	
	<li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>
	<li id="nav_admin_sysalerts">
		<a href="a_alerts.php"><!--onclick="loadTemplateView('sysAlerts')"-->
		<i class="fa fa-bullhorn" aria-hidden="true"></i>
		<span>System Alerts</span>
	</a>
</li>
<li id="nav_admin_inbox">
	<a href="a_mailbox.php"><!--onclick="loadTemplateView('mail')"-->
	<span class="pull-right label label-primary admin-messages-count">0</span>
	
	<i class="fa fa-inbox" aria-hidden="true"></i>
	<span>Inbox</span>
	
</a>
</li>
<li id="nav_admin_market_survey">
<a href="index.php?logout">
	<i class="fa fa-sign-out" aria-hidden="true"></i>
	<span>Logout</span>
</a>
</li>
</ul>
</nav>
<hr class="separator">
</div>
</div>
</aside>
<!-- end: sidebar -->
<!-- right content -->
<section role="main" class="content-body">
<!-- /. title-->
<div class="content-header">
<h1>
Adverts
<small>Management</small>
</h1>
<ol class="breadcrumb">
<li><a href="#"><i class="fa fa-info-circle"></i> Home</a></li>
<li class="active">Adverts</li>
</ol>
</div>
<div class="row">
<div class="col-md-8 col-xs-12 col-sm-12">
<section class="panel-top-body panel panel-featured panel-featured-danger custom-section-panel">
<div class="panel-body" id="custom_body_panel">
<form action="#">
<div class="row">
	<div class="col-sm-5 col-xs-12 col-md-5">
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon bg-maroon"><i class="fa fa-search"></i></span>
				<input type="text" class="form-control" name="input_search_filter" id="input_search_filter" onkeyup="advertfilterfunc()" placeholder="Search adverts...">
			</div>
		</div>
	</div>
	<div class="col-sm-3 col-xs-6 col-md-3">
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon bg-olive"><i class="fa fa-sort"></i></span>
				<select name="input_select_order" id="input_select_order" class="form-control" onchange="advertfilterfunc()">
					<option value="">--- Order ---</option>
					<option value="asc">Old-New</option>
					<option value="desc">New-Old</option>
				</select>
			</div>
		</div>
	</div>
	<div class="col-sm-4 col-xs-6 col-md-4">
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon bg-fuchsia"><i class="fa fa-filter"></i></span>
				<select name="input_select_filter" id="input_select_filter" class="form-control" onchange="advertfilterfunc()">
					<option value="">--- filter ---</option>
					<option value="approved">Approved</option>					
					<option value="unapproved">UnApproved</option>
					<option value="havebanners">Have Banners</option>
					<option value="nobanners">No Banners</option>					
					<option value="cancelled">Cancelled</option>	
				</select>
			</div>
		</div>
	</div>
</div>
</form>
</div>
</section>

<div class="loading-overlay">
	<p class="text-center deep-purple-text"><i class="fa fa-spinner fa-spin fa-3x"></i></p>
</div>

<!-- advert-list-container -->
<div id="advert-list-container">
	
<?php
if ($_adminObj->_advertlist()) {
    $sil = $dbh->prepare("SELECT Business_Name, Business_Phone FROM business_profile WHERE business_profile_id = :business_profile_id");
    foreach ($_adminObj->_advertlist() as $vo) {

        $post_id = $vo['advert_post_id'];
        $advertSerial = $vo['advert_serial'];

        $sil->execute(["business_profile_id" => $vo['business_profile_id']]);
        $re = $sil->fetchObject();

        $advertPostDateTimeObj = new DateTime($vo['date_post']);
        $advertPostDateTime = $advertPostDateTimeObj->format('d M, Y - h:i a');
?>
<section class="panel panel-featured panel-featured-success">
<header class="panel-heading">
<h2 class="panel-title hr_font_weight_700"><?php echo $vo['advert_title']; ?></h2>
<div class="panel-actions">
	<a href="#" class="fa fa-caret-down"></a>
	<a href="#" class="fa fa-times"></a>
</div>
</header>
<div class="panel-body mdb-color-text">
<?php echo trim(htmlspecialchars_decode($vo['description']), " ") ?>
<div class="text-navy">
<p class="t-15">
	Business Name: <?php echo $re ? $re->Business_Name : "N/A"; ?>
</p>
<p class="t-15">
	<?php echo "Runs from <b>" . $_adminObj->_returnformatedDate($vo['start_period']) . "</b> To <b>" . $_adminObj->_returnformatedDate($vo['end_period']) . "</b>"; ?>
</p>
<p class="t-15">
	Tel: <i class="fa fa-phone text-olive"></i> <?php echo $re->Business_Phone; ?>
</p>

<p class="t-15">
	<?php 
		$run = $vo['status'] == 0 ? "Ended" : "On-going";

		echo "
		<span class='light-blue-text'>Admin Approved:</span> <a href='javascript:void(0)' class='text-capitalize mdb-color-text data-admin-approved' data-type='select' data-title='Approval status' data-pk='".$post_id."' data-name='approved' data-placeholder='required' data-value='".$vo['approved']."'>" . $vo['approved']. "</a> - 
		<span class='light-blue-text'>Client Published:</span> <span class='text-capitalize mdb-color-text'>". $vo['publish']."</span> - 
		<span class='light-blue-text'>Cancelled:</span> <a href='#advertCancel-Modal' data-toggle='modal' class='text-capitalize mdb-color-text data-admin-cancelled' onclick='cancelAdvertfunc2(".$post_id.")' id='advert-cancelled".$post_id."'>" .$vo['cancelled']."</a> -
		 <span class='light-blue-text'>Run:</span> <span class='text-capitalize mdb-color-text'>" .$run."</span>"; 
//  data-pk='".$post_id."' data-serial='".$vo['advert_serial']."' data-name='cancelled' data-placeholder='required' data-value='".$vo['cancelled']."'
	?>
	<input type="hidden" id="advert_post_id<?php echo $post_id; ?>" value="<?php echo $post_id; ?>">
	<input type="hidden" id="advert_serial_id<?php echo $post_id; ?>" value="<?php echo $advertSerial; ?>">
	<input type="hidden" id="advert_view<?php echo $post_id; ?>" value="1">
</p>
<?php if ($vo['primary_banner_1'] != NULL) { ?>
<div class="t-15 btn-group">
		<a class="image-popup-vertical-fit btn btn-default btn-flat" href="<?php echo '../client/_assets/clientUploads/'.$vo['primary_banner_1']; ?>">
			<img class="img-responsive display-none" src="<?php echo '../client/_assets/clientUploads/'.$vo['primary_banner_1']; ?>" width="75">
			<i class="fa fa-file-picture-o text-olive"></i> Banner 1
		</a>
<?php if ($vo['s_banner_2'] != NULL || $vo['s_banner_3'] != NULL) { ?>
		<a class="image-popup-vertical-fit btn btn-default btn-flat" href="<?php echo '../client/_assets/clientUploads/'.$vo['s_banner_2']; ?>">
			<img class="img-responsive display-none" src="<?php echo '../client/_assets/clientUploads/'.$vo['s_banner_2']; ?>" width="75">
			<i class="fa fa-file-picture-o text-olive"></i> Banner 2
		</a>		

		<a class="image-popup-vertical-fit btn btn-default btn-flat" href="<?php echo '../client/_assets/clientUploads/'.$vo['s_banner_3']; ?>">
			<img class="img-responsive display-none" src="<?php echo '../client/_assets/clientUploads/'.$vo['s_banner_3']; ?>" width="75">
			<i class="fa fa-file-picture-o text-olive"></i> Banner 3
		</a>	
<?php } ?>	
</div>
<?php } ?>

    </div>
    <!-- text-navy -->
</div>
<!-- panel body -->
<div class="panel-footer">
<div class="row">
<div class="col-xs-7">
	<p class="mdb-color-text"><?php echo "<b>Posted:</b> " . $advertPostDateTime; ?></p>
</div>
<div class="col-xs-5">
	<p class="mdb-color-text"><?php echo "<b>By:</b> " . $vo['loggedEmail']; ?></p>
</div>
</div>
</div>
</section>
<?php
    } // loop
    
} else {
    echo "<p class='text-center text-warning hr_font_weight_600'>No Adverts Available</p>";
}
?>
		</div>
		<!-- advert-list-container -->

	</div>
	<!-- /. adverts list col-md-8 col-xs-12 col-sm-12 -->

	<div class="col-md-4 col-sm-12 col-xs-12">
		<section class="panel-top-body panel panel-featured panel-featured-danger custom-section-panel">
			<header class="panel-heading">
				<h2 class="panel-title">Trending adverts</h2>
			</header>
			<div class="panel-body">				
					<span id="advert-trends-div"></span>		
			</div>
		</section>
	</div>
	<!-- trending panels col-md-4 col-sm-12 col-xs-12 -->
</div>
<!-- row -->

</section>
<!-- /. content-body -->
</div>
<!-- /. inner-wrapper -->
</section>
<!-- /. body -->




<!-- /. require plugins and scripts -->
<?php include '_includes/_bottomJs.php'; ?>
<script>
	$(function($){

		// scroll to fixed position plugin init
		 $('.custom-section-panel').scrollToFixed({ 
		 	marginTop: 62,
		 	preFixed: function() { $(this).find('#custom_body_panel').css('background-color', 'rgba(51, 53, 63, 0.7)'); },
		 	postFixed: function() { $(this).find('#custom_body_panel').css('background-color', 'rgba(255, 255, 255, 1)'); }
		 });

		 // bootstrap inline editing

		 // admin approved
		 $(".data-admin-approved").editable({
			url:   "_admin-requests.php",    
			pk:    $(this).attr('data-pk'),       
			name:  $(this).attr('data-name'),
			value: $(this).attr('data-value'), 
		 	source: [
		 		{value: 'yes', text: 'Yes'},
		 		{value: 'no', text: 'No'}
		 	],        
			params: function(params){
			  let advert_approval_data = {};
			  //advert_approval_data['budget_col_name']  = params.name;
			  advert_approval_data['advertApprovalId']   = params.pk;
			  //advert_approval_data['budget_col_value'] = params.value;
			 return advert_approval_data;
			},
			ajaxOptions: {
				type: 'POST'
			},
			success: function (response, newValue) {   

			    if(response == 1) {
	               _successNotify("This advert Has been successfully approved!");
	               //refresh()
	           	}
	            else if(response == 2){
	                _errorNotify("Failed to approve advert")
	            }
	            else if(response == 4){
	                _infoNotify("Advert has been disapproved")
	            }
	            else if(response == 5){
	                _errorNotify("Failed to disapproved advert")
	            }
	            else {
	                _errorNotify("External Server error ") 
	            }
        	}
		 });

		 // admin cancelled
		 /*
		 $(".data-admin-cancelled").editable({
			url:   "_admin-requests.php",    
			pk:    $(this).attr('data-pk'),       
			name:  $(this).attr('data-name'),
			value: $(this).attr('data-value'), 
		 	source: [
		 		{value: 'yes', text: 'Yes'},
		 		{value: 'no', text: 'No'}
		 	],        
			params: function(params){
			  let advert_cancelled_data = {};
			  params.serial = $(this).editable().data('serial');
			  advert_cancelled_data['advertPkey']   = params.pk;
			  advert_cancelled_data['advertSerial']   = params.serial;

			  //console.log(advert_cancelled_data['advertPkey'] + '\n'+ advert_cancelled_data['advertSerial']);
			 return advert_cancelled_data;
			},
			ajaxOptions: {
				type: 'POST'
			},
			success: function (response, newValue) {   

			    if(response == 1) {
	               _successNotify("This advert Has been successfully approved!");
	               //refresh()
	           	}
	            else if(response == 2){
	                _errorNotify("Failed to approve advert")
	            }
	            else {
	                _errorNotify("External Server error ") 
	            }
	            
        	}
		 });
		 */

	});

	// filter function
	function advertfilterfunc() {

	    var filterSearchKeyword = $('#input_search_filter').val();
	    var filterSelectOrder   = $('#input_select_order').val();
	    var filterSelectFilter  = $('#input_select_filter').val();
	    var filterAdvertData    = 'filter_advert_data';

	    $.ajax({
	        type: 'POST',
	        url: '_admin-requests.php',
	        data: 'filterAdvertData='+filterAdvertData+'&filterSearchKeyword='+filterSearchKeyword+'&filterSelectOrder='+filterSelectOrder+'&filterSelectFilter='+filterSelectFilter,
	        beforeSend: function () {
	            $('.loading-overlay').show();
	        },
	        success: function (html) {
	            $('#advert-list-container').html(html);
	            $('.loading-overlay').fadeOut("slow");
	        }
	    });

	} //
	
/*window.onscroll = function() {
	myFunction()
};
	var header = document.getElementById("testing");
	var navHeader = document.getElementById("header_top_navigation");
	var sticky = header.offsetTop;
function myFunction() {
	if (window.pageYOffset > sticky) {
	header.classList.add("sticky-group");
	console.log(sticky)
	} else {
	header.classList.remove("sticky-group");
	console.log(sticky)
	}
}
*/
</script>