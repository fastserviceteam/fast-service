<?php 

// require global file
require "_admin-config.php";

// instantiate class
$_adminObj = new AdminClass();


//querrying all industries ..getting count for industries

$des = "SELECT *FROM  industries ORDER BY industry_name ASC";
			
				$eds = $dbh->prepare($des);
				
					$eds->execute();
					
							$ques = $eds->fetchAll(PDO::FETCH_ASSOC); 	

//querrying all categories ..getting count for categories
							
							
	$qn = "SELECT *FROM  categories";
			
		$qr = $dbh->prepare($qn);
				
			$qr->execute();
					
				$get = $qr->fetchAll(PDO::FETCH_ASSOC); 

//querrying all services ..getting count for services
							
							
	$serv = "SELECT *FROM  services";
			
		$se = $dbh->prepare($serv);
				
			$se->execute();
					
				$set = $se->fetchAll(PDO::FETCH_ASSOC); 				
				
	
							
							
									?>


<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title>Fast Service - Admin</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- top css file -->
    <?php include "_includes/_topCss.php";?>
</head>

<body>

    <section class="body" >

       
        <?php
            # include header ~ top-nav 
        require "_includes/_topNav.php";
        ?>

        <div class="inner-wrapper">
            <!-- start: sidebar -->

            <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">

                <div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">

                    <div class="sidebar-title">
                        FastService Corp
                    </div>
                    <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                       <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
                <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                    <div class="nano-content" >
                        <nav id="menu" class="nav-main" role="navigation" >
                            <ul class="nav nav-main" style="color:#607D8B;">
                                <li id="nav_admin_main">
                                    <a href="a_dashboard.php"><!--  onclick="loadTemplateView('main')" -->
                                         <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>

                                <li id="nav_admin_businessmgmt">

                                    <a href="a_business.php"> <!--  onclick="loadTemplateView('businessMgmt')" -->

                                      <i class="fa fa-money" aria-hidden="true"></i>

                                        <span>Business Management</span>

                                    </a>

                                </li>


                                <li class="nav-active" id="nav_admin_prodservices">

                                    <a href="a_categories.php?firstOpen"><!--onclick="loadTemplateView('prod-serv')"-->

                                        <i class="fa fa-globe" aria-hidden="true"></i>

                                        <span>Products and Services</span>

                                    </a>

                                </li>

                                <li id="nav_admin_billing">

                                    <a href="a_billing.php"><!--onclick="loadTemplateView('billing')"-->

                                        <i class="fa fa-exchange" aria-hidden="true"></i>

                                        <span>Billing</span>

                                    </a>

                                </li>

                                <li id="nav_admin_users">

                                    <a href="a_users.php"><!--onclick="loadTemplateView('users')"-->

										<i class="fa fa-users" aria-hidden="true"></i>

                                        <span id="usr">Human Resource</span>

                                    </a>

                                </li>


                                <li id="nav_admin_jobs">
                                    <a href="a_jobs.php"><!-- onclick="loadTemplateView('jobs')"-->

                                        <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
										<i class="fa fa-briefcase" aria-hidden="true"></i>
                                        <span>Jobs</span>
                                    </a>
                                </li>


                                <li id="nav_admin_adverts">
                                    <a href="a_adverts.php"><!--onclick="loadTemplateView('advertsMgmt')"-->

                                        <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                         <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span>Adverts Mgt</span>
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="a_marketSurvey.php"><!--onclick="loadTemplateView('mktSurvey')"-->

                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                        <span>Market Survey</span>
                                    </a>
                                </li>

                                <li id="nav_admin_reports">

                                    <a href="a_reports.php">

                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
                                    
                            <li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>

                                <li id="nav_admin_sysalerts">

                                    <a href="a_alerts.php"><!--onclick="loadTemplateView('sysAlerts')"-->

                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>

                                        <span>System Alerts</span>

                                    </a>

                                </li>

                                <li id="nav_admin_inbox">
                                    <a href="a_mailbox.php"><!--onclick="loadTemplateView('mail')"-->

                                        <span class="pull-right label label-primary admin-messages-count">0</span>
                                        
										<i class="fa fa-inbox" aria-hidden="true"></i>
                                        <span>Inbox</span>
										
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="index.php?logout">

                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                </li>

                            </ul>
                        </nav>

                        <hr class="separator">

                    </div>

                </div>

            </aside>

            <!-- end: sidebar -->

           <!-- right content -->
            <section role="main" class="content-body">

<!-- /. title-->
<div class="content-header">
  <h3>
    <font color="#4FC3F7">Product and Services</font>
    <small></small>
  </h3>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-globe"></i> Home</a></li>
    <li class="active">Product and Services</li>
  </ol>
</div>

<div class="row">

		
			
			<div class="col-md-12 col-lg-12 col-xl-6">
								
				<section class="panel panel-horizontal">
									
					<header class="panel-heading bg-white">
									
									
									</header>
									
									<div class="panel-body p-sm">
										
										<h3 class="text-semibold mt-sm">Summary of Industries and Categories</h3>
										
										<p><span style="background-color:#43A047;" class="badge"><?php echo $eds->rowCount(); ?></span> Industries
										
											<span style="background-color:#26C6DA;" class="badge"> <?php echo $qr->rowCount(); ?> </span> Categories 
											
												<span style="background-color:#0097A7" class="badge"> <?php echo $se->rowCount(); ?> </span> Products/services </p>
									
									</div>
								
								</section>
								
								</div>
								<!-- col-md-12 col-lg-12 col-xl-6 -->
								
								
								
					
					
					<div class="col-sm-12 col-md-12 col-lg-12">
					
							<section class="panel panel-featured panel-featured-info">
								
								<div class="panel-body">
								
									<div style="width:100%;height:400px;overflow:auto;color:#424242;">  

				<table class="table table-striped">
    <thead>
      <tr>
        <th><li class="fa fa-plus" title="Add New industry" data-toggle="modal" data-target="#zoomOutInM1" style="cursor:pointer;"></li>&nbsp;Industry Name</th>
		
			<th><font color="green"></font>&nbsp; Categories</th>
		 
					<th><center>  No of Categories </center>  </th>  <th>View</th>
       
      </tr>
	  
    </thead>
    <tbody>
	
	<?php 
	
	//quesrring all industries from  the industries table 

	$des = "SELECT *FROM  industries ORDER BY industry_name ASC";
			
				$eds = $dbh->prepare($des);
				
					$eds->execute();
					
							$ques = $eds->fetchAll(PDO::FETCH_ASSOC);
							
								foreach($ques as $wo){
									
						$industry_id = $wo['industry_id'];
		
		$weq = "SELECT *FROM  categories WHERE industry_id ='{$industry_id}'";
			
				$rev = $dbh->prepare($weq);
				
					$rev->execute();
					
							$query = $rev->fetchAll(PDO::FETCH_ASSOC);


		?>			
		
      <tr>
	  
        <td><?php  echo $wo['industry_name']; ?></td>
		
		
		<td>  
		
		<li class="dropdown">
	
	           <span class="fa fa-plus" title="Add New Category" data-industid="<?php echo $wo['industry_id'] ?>" data-industry="<?php echo $wo['industry_name'] ?>"  data-toggle="modal" data-target="#zoomOutInM2" onclick="showInd(this)" style="cursor:pointer;"></span>   <a href="#" class="dropdown-toggle" data-toggle="dropdown">select Category to add service<b class="caret"></b></a>
	            
				<ul class="dropdown-menu">
				
				<?php foreach($query as $vo){?>
				
				<li><a style="cursor:pointer;" data-industry="<?php echo $wo['industry_name'] ?>" data-id="<?php echo $vo['category_id'] ?>"  data-cat="<?php echo $vo['category_name'] ?>" class="openUp" data-toggle="modal"  data-target="#modalBootstrap" onclick="showAll(this)"><?php echo $vo['category_name'] ?></a></li>
				
				<?php }  ?>
				
				</ul>
				
	        </li>   </td>
			
        
		<td> 
		
		<p>
		
		<center><span style="background-color:#FFC107;" class="badge"><?php  echo $rev->rowCount(); ?></span> </center>
										
			
					</p></td>
				
					
					 
				<td> <a data-toggle="modal" data-target="#zoomOutInM3" onclick="loadIndust(this)"  data-id="<?php echo $wo['industry_id'] ?>"  style="cursor:pointer;"> <span class="glyphicon glyphicon-resize-full"></span> </a></td>	 
					 
       
      </tr>
	

								<?php } ?>
	  
	  </table>
	  
		
		
									
									
									
									</div>
									
									
								</div>
							</section>
						</div>
						
			
			
			
			<div class="col-md-12 col-xl-6">
			
										<section class="panel panel-group">
										
											<header class="panel-heading bg-white">
						
												<div class="widget-profile-info">
												
													<div class="profile-picture" style="background-color:#E0E0E0;padding:0.5em;border-radius:90px;">
													
														<center> <img src="../../images/logo.jpg"> </center>
													</div>
													<div class="profile-info">
														<h4 class="name text-semibold">Product and Service Suggestions</h4>
														<h5 class="role">From different Users</h5>
														<div class="profile-footer">
															<a href="#">(Accept and delete)</a>
														</div>
													</div>
												</div>
						
											</header>
											<div id="accordion">
												<div class="panel panel-accordion panel-accordion-first">
													<div class="panel-heading">
														<h4 class="panel-title">
															<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
																<i class="fa fa-check"></i> Collapse List
															</a>
														</h4>
													</div>
													<div id="collapse1One" class="accordion-body collapse in">
														<div class="panel-body">
															<ul class="widget-todo-list">
																<li>
																	<div class="checkbox-custom checkbox-default">
																		<input type="checkbox" checked="" id="todoListItem1" class="todo-check">
																		<label class="todo-label" for="todoListItem1"><span>belts</span></label>
																	</div>
																	<div class="todo-actions">
																		<a class="todo-remove" href="#">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</li>
																<li>
																	<div class="checkbox-custom checkbox-default">
																		<input type="checkbox" id="todoListItem2" class="todo-check">
																		<label class="todo-label" for="todoListItem2"><span> sandals</span></label>
																	</div>
																	<div class="todo-actions">
																		<a class="todo-remove" href="#">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</li>
																<li>
																	<div class="checkbox-custom checkbox-default">
																		<input type="checkbox" id="todoListItem3" class="todo-check">
																		<label class="todo-label" for="todoListItem3"><span> African Mats</span></label>
																	</div>
																	<div class="todo-actions">
																		<a class="todo-remove" href="#">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>

								</section></div> 
				</div>
				<!-- col-md-12 col-xl-6 -->
		
		</div>
		<!-- row -->
				
	            </section>
            <!-- /. content-body -->

    </div>
    <!-- /. inner-wrapper -->

</section>
<!-- /. body -->
 


<!-- /. require plugins and scripts -->				
<?php  include '_includes/_bottomJs.php';?>
