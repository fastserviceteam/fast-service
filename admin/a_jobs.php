<?php
// require global file
require "_admin-config.php";
// instantiate class
$_adminObj = new AdminClass();
?>

<!doctype html>
<html class="fixed">

<head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title>Fast Service - Admin</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- top css file -->
    <?php include "_includes/_topCss.php";?>
</head>

<body>
    <section class="body">

        <?php
			# include header ~ top-nav
			require "_includes/_topNav.php";
			?>
        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">
                <div class="sidebar-header" style="border-bottom:1px solid #CFD8DC;">
                    <div class="sidebar-title">
                        FastService Corp
                    </div>
                    <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
                <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main" style="color:#607D8B;">
                                <li id="nav_admin_main">
                                    <a href="a_dashboard.php">
                                        <!--  onclick="loadTemplateView('main')" -->
                                        <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>
                                <li id="nav_admin_businessmgmt">
                                    <a href="a_business.php">
                                        <!--  onclick="loadTemplateView('businessMgmt')" -->
                                        <i class="fa fa-money" aria-hidden="true"></i>
                                        <span>Business Management</span>
                                    </a>
                                </li>
                                <li id="nav_admin_prodservices">
                                    <a href="a_categories.php?firstOpen">
                                        <!--onclick="loadTemplateView('prod-serv')"-->
                                        <i class="fa fa-globe" aria-hidden="true"></i>
                                        <span>Products and Services</span>
                                    </a>
                                </li>
                                <li id="nav_admin_billing">
                                    <a href="a_billing.php">
                                        <!--onclick="loadTemplateView('billing')"-->
                                        <i class="fa fa-exchange" aria-hidden="true"></i>
                                        <span>Billing</span>
                                    </a>
                                </li>
                                <li id="nav_admin_users">
                                    <a href="a_users.php">
                                        <!--onclick="loadTemplateView('users')"-->
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <span id="usr">Human Resource</span>
                                    </a>
                                </li>
                                <li class="nav-active" id="nav_admin_jobs">
                                    <a href="a_jobs.php">
                                        <!-- onclick="loadTemplateView('jobs')"-->
                                        <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
                                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        <span>Jobs</span>
                                    </a>
                                </li>
                                <li id="nav_admin_adverts">
                                    <a href="a_adverts.php">
                                        <!--onclick="loadTemplateView('advertsMgmt')"-->
                                        <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span>Adverts Mgt</span>
                                    </a>
                                </li>
                                <li id="nav_admin_market_survey">
                                    <a href="a_marketSurvey.php">
                                        <!--onclick="loadTemplateView('mktSurvey')"-->
                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                        <span>Market Survey</span>
                                    </a>
                                </li>
                                
                                <li id="nav_admin_reports">
                                    <a href="a_reports.php">
                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>

                                <li class="li-header" style="background-color:#00BCD4;color:#FFF;"><b>OTHERS</b></li>
                                <li id="nav_admin_sysalerts">
                                    <a href="a_alerts.php">
                                        <!--onclick="loadTemplateView('sysAlerts')"-->
                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                        <span>System Alerts</span>
                                    </a>
                                </li>
                                <li id="nav_admin_inbox">
                                    <a href="a_mailbox.php">
                                        <!--onclick="loadTemplateView('mail')"-->
                                        <span class="pull-right label label-primary admin-messages-count">0</span>

                                        <i class="fa fa-inbox" aria-hidden="true"></i>
                                        <span>Inbox</span>

                                    </a>
                                </li>
                                <li id="nav_admin_market_survey">
                                    <a href="index.php?logout">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <hr class="separator">
                    </div>
                </div>
            </aside>
            <!-- end: sidebar -->
            <!-- right content -->
            <section role="main" class="content-body" style="border-left:2px solid #00BCD4;">

                <!-- /. title-->
                <div class="content-header">
                    <h1>
                        Jobs
                        <small>Management</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-briefcase"></i> Home</a></li>
                        <li class="active">Jobs</li>
                    </ol>
                </div>
                <div class="row">
                    <!-- left - jobs managment list and filter -->
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">

					<!-- filter, search, order panel form -->
					<section class="panel panel-featured panel-featured-primary">
						<div class="panel-body">
							<form role="form" id="dataform-jobs-filter">

								<div class="row">
									<div class="col-xs-6 col-sm-3 col-md-3">
										<div class="form-group">
											<input type="text" class="form-control" id="datainput-search-jobs" placeholder="Search..." onkeyup="jobAnalysisfunction()">
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 col-md-3">
										<div class="form-group">
											<select id="datainput-approval-jobs" class="form-control" onchange="jobAnalysisfunction()">
												<option value=""> --- Approved --- </option>
												<option value="yes">Yes</option>
												<option value="no">No</option>
											</select>
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 col-md-3">
										<div class="form-group">
											<select id="datainput-order-jobs" class="form-control" onchange="jobAnalysisfunction()">
												<option value=""> --- Order --- </option>
												<option value="desc">New</option>
												<option value="asc">Old</option>
											</select>
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 col-md-3">
										<div class="form-group">
											<select id="datainput-select-districts-jobs" class="form-control" onchange="jobAnalysisfunction()">
												<option value=""> --- District --- </option>
												<?php
													foreach($_adminObj->_districts() as $row) {
													 echo '<option value="'.$row['district_name'].'">'.$row['district_name'].'</option>';
													} 
												?>
											</select>
										</div>
									</div>
								</div>								
							</form>
						</div>
					</section>


<!-- job list -->
<div id="jobsListWrapper">
<?php 

$deq = $_adminObj->_returnJobsList();

if (mysqli_num_rows($deq) > 0) 
{		
	// get business name associated with job
	$sql1 = $dbh->prepare("SELECT Business_Name from  business_profile where business_profile_id = :business_profile_id");

	// get job responsibilities
	$sql2 = $dbh->prepare("SELECT responsibility from job_responsibility where job_id = :job_id and job_serial = :job_serial");

	while($woq = mysqli_fetch_assoc($deq))
	{

		$biz_id 	= $woq['business_profile_id'];	
		$job_id 	= $woq['job_id'];	
		$job_serial = $woq['job_serial'];		

		$sql1->execute([
			"business_profile_id" => $biz_id
		]);	
		$qo = $sql1->fetchObject();

		$sql2->execute([
			"job_id" => $job_id,
			"job_serial" => $job_serial
		]);
		$jresp = $sql2->fetchAll();
									
?>

                        <section class="panel panel-featured panel-featured-purple">
                            <header class="panel-heading">
                                <div class="panel-actions more-details-panel-actions">
                                    <a href="#" class="fa fa-caret-up more-details-caret" data-toggle="tooltip" title="View more details"></a>
                                    <a href="#" class="fa fa-times" data-toggle="tooltip" title="Close"></a>
                                </div>

                                <h2 class="panel-title hr_font_weight_800">
                                    <?php echo $woq['job_vacancy']; ?>
                                </h2>
                                <p class="panel-subtitle hr_font_weight_600">
                                    <?php echo $qo->Business_Name; ?>
                                </p>
                                <p>
                                    <?php echo $woq['jcName']; ?>
                                </p>
                            </header>
                            <div class="panel-body text-navy t-15 display-none">
                            	<div class="row">
                            		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		                                <p>
		                                	<strong>District:</strong> <?php echo $woq['job_district']; ?>
		                                </p>
		                                <p>
		                                	<strong>Location:</strong> <?php echo $woq['location']; ?>
		                                </p>   
		                                <p>
		                                	<strong>Posted:</strong> <?php echo $_adminObj->_returnformatedDate($woq['date_posted']); ?>
		                                </p>                           			
                            		</div>
                            		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		                                <p>
		                                	<strong>Type:</strong> <?php echo $woq['job_type']; ?>
		                                </p>
		                                <p>
		                                	<strong>Salary:</strong> <?php echo $woq['salary']; ?>
		                                </p>  
		                                <p>
		                                	<strong>Deadline:</strong> <?php echo $woq['deadline'] != 'N/A' ? $_adminObj->_returnformatedDate($woq['deadline']) : $woq['deadline']; ?>
		                                </p>                            			
                            		</div>
                            	</div>
                            	<div class="row">
                            		<div class="col-sm-12 col-xs-12 col-md-12">
					                    <h4 class="pink-text">Qualifications</h4>
					                    <span class="text-justify mdb-color-text">
					                        <?php echo  $woq['job_qualification']; ?>
					                    </span>

					                    <h4 class="pink-text">Key Duties and Responsibilities</h4>
					                    <p class="text-justify mdb-color-text">
					                        <ul>
					                        <?php foreach ($jresp as $row) 
					                        {
					                        ?>
					                            <li><?php echo $row['responsibility']; ?></li>
					                        <?php
					                        } 
					                        ?>
					                        </ul>
					                    </p>
					                    <h4 class="pink-text">Job application procedure</h4>
					                    <p class="text-justify mdb-color-text">
					                        <?php echo  $woq['application_mode']; ?>
					                    </p>
                            		</div>
                            	</div>
                            </div>

                            <div class="panel-footer">
                            	<div class="row">
                            		<div class="col-sm-6 col-xs-6 col-md-6">
                            			<p>
                            				<strong>Approved:</strong> 
                            				<a href='javascript:void(0)' class='text-capitalize datajob-admin-approved' data-type='select' data-title='Approval status' data-pk='<?php echo $job_id; ?>' data-name='approved' data-placeholder='required' data-value='<?php echo $woq['approved']; ?>'>
                            					<?php echo $woq['approved']; ?>                            							
                            				</a>
                            			</p>
                            		</div>
                            		<div class="col-sm-6 col-xs-6 col-md-6">
                            			<p>
                            				<strong>Posted By:</strong> <span><?php echo $woq['loggedin_email']; ?></span>
                            			</p>
                            		</div>
                            	</div>
                            </div>
                        
                        </section>


                        <?php 
	} // while loop

} // checking ok
                        else {
                            echo '<div class="alert alert-info text-center"><strong class="text-olive">Info: No jobs created yet!</strong></div>';
                        } // checking noo
?>
						</div>
						<!-- jobs list wrapper -->

                    </div>
					<!-- left - jobs managment list and filter -->

                    <!-- end of post job form -->

                    <!-- statistics for posted jobs -->
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <h4 class="panel-title text-center">Businesses with Jobs</h4>
                            </header>
                            <div class="panel-body">
                                   <?php $_adminObj->numberOfJobsUnderEachBusiness(); ?>
                            </div>
                        </section>
                    </div>
                    <!-- end of job statistics -->
                </div>
                <!-- row -->
            </section>
            <!-- /. content-body -->
        </div>
        <!-- /. inner-wrapper -->
    </section>
    <!-- /. body -->

    <!-- /. require plugins and scripts -->
    <?php  include '_includes/_bottomJs.php';?>

    <!-- page specific scripts -->
    <script>
    	$(function(){

    		 // admin approved
			 $(".datajob-admin-approved").editable({
				url:   "_admin-requests.php",    
				pk:    $(this).attr('data-pk'),       
				name:  $(this).attr('data-name'),
				value: $(this).attr('data-value'), 
			 	source: [
			 		{value: 'yes', text: 'Yes'},
			 		{value: 'no', text: 'No'}
			 	],        
				params: function(params){
				  var job_approval_data = {};
				  //advert_approval_data['budget_col_name']  = params.name;
				  job_approval_data['dataJobApproval_Id'] = params.pk;
				  job_approval_data['dataJob_value'] 	  = params.value;
				 return job_approval_data;
				},
				ajaxOptions: {
					type: 'POST'
				},
				success: function (response, newValue) {   

				    if(response == 1) {
		               _successNotify("This Job Has been successfully approved!");
		               //refresh()
		           	}
		            else if(response == 2){
		                _errorNotify("Failed to approve Job")
		            }
		            else if(response == 4){
		                _infoNotify("Job has been disapproved")
		            }
		            else if(response == 5){
		                _errorNotify("Failed to disapproved Job")
		            }
		            else {
		                _errorNotify("External Server error ") 
		            }
	        	}
			 });



			 // run events
			 /*
				 $("#datainput-search-jobs").on("keyup", function(){
				 	jobAnalysisfunction()
				 });
				 $("#datainput-approval-jobs").on("change", function(){
				 	jobAnalysisfunction()
				 });
				 $("#datainput-order-jobs").on("change", function(){
				 	jobAnalysisfunction()
				 });
				 $("#datainput-select-districts-jobs").on("change", function(){
				 	jobAnalysisfunction()
				 });
			*/

    	});

    		// job analysis; filtering, ordering, searching
			 //jobAnalysisfunction();

			 function jobAnalysisfunction() {

			 	var jobAnalysisAction = "job-analysis";

			 	var dataInputSearchJob      = $("#datainput-search-jobs").val();
			 	var dataSelectApprovalJob   = $("#datainput-approval-jobs").val();
			 	var dataSelectOrderJob      = $("#datainput-order-jobs").val();
			 	var dataSelectDistrictJob   = $("#datainput-select-districts-jobs").val();

		        $.ajax({
		            url:"_admin-requests.php",
		            method:"POST",
		            data:'jobAnalysisAction='+jobAnalysisAction+'&dataInputSearchJob='+dataInputSearchJob+'&dataSelectApprovalJob='+dataSelectApprovalJob+'&dataSelectOrderJob='+dataSelectOrderJob+'&dataSelectDistrictJob='+dataSelectDistrictJob,
	            	beforeSend: function() {
	            		$('#jobsListWrapper').html("<p class='text-center text-maroon'><i class='fa fa-spinner fa-spin fa-3x'></i></p>");
	            	},
		            success:function(data){
		                $('#jobsListWrapper').html(data);
		            }
		        });

			 } // analysis function
    </script>