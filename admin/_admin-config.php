<?php

// --- >> admin global files << --- //

session_start(); // init session

require '../_require-file.php'; // config
require '../_server-functions.php'; // functions
require "../_modules/classes/_adminClass.php"; // admin class

// --- >> definitions << --- //
define("ff_logo", "../client/_assets/images/fs-logo.png");