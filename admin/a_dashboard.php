<?php 
    // require global file
    require "_admin-config.php";

    // instantiate class
    $_adminObj = new AdminClass();

    if($_adminObj->_isLoggedIn() == false)
    {
        header("Location: login.php");
    }
    $mail = $_SESSION['fastService_fadminEmail_Session'];

    $ruq = "SELECT *FROM jobs WHERE approved = ''";
    $exeq = $dbh->prepare($ruq);
    $exeq->execute(); 
    $cox = $exeq->fetchAll(PDO::FETCH_ASSOC); 

?>

<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title>Fast Service - Admin</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- top css file -->
    <?php include "_includes/_topCss.php";?>
</head>

<body>

    <section class="body" >

       
        <?php
            # include header ~ top-nav 
        require "_includes/_topNav.php";
        ?>

        <div class="inner-wrapper">
            <!-- start: sidebar -->

            <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">

                <div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">

                    <div class="sidebar-title">
                        FastService Corp
                    </div>
                    <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                       <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
                <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                    <div class="nano-content" >
                        <nav id="menu" class="nav-main" role="navigation" >
                            <ul class="nav nav-main" style="color:#607D8B;">
                                <li class="nav-active" id="nav_admin_main">
                                    <a href="a_dashboard.php"><!--  onclick="loadTemplateView('main')" -->
                                         <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>

                                <li id="nav_admin_businessmgmt">

                                    <a href="a_business.php"> <!--  onclick="loadTemplateView('businessMgmt')" -->

                                      <i class="fa fa-money" aria-hidden="true"></i>

                                        <span>Business Management</span>

                                    </a>

                                </li>


                                <li id="nav_admin_prodservices">

                                    <a href="a_categories.php?firstOpen"><!--onclick="loadTemplateView('prod-serv')"-->

                                        <i class="fa fa-globe" aria-hidden="true"></i>

                                        <span>Products and Services</span>

                                    </a>

                                </li>

                                <li id="nav_admin_billing">

                                    <a href="a_billing.php"><!--onclick="loadTemplateView('billing')"-->

                                        <i class="fa fa-exchange" aria-hidden="true"></i>

                                        <span>Billing</span>

                                    </a>

                                </li>

                                <li id="nav_admin_users">

                                    <a href="a_users.php"><!--onclick="loadTemplateView('users')"-->

										<i class="fa fa-users" aria-hidden="true"></i>

                                        <span id="usr">Human Resource</span>

                                    </a>

                                </li>


                                <li id="nav_admin_jobs">
                                    <a href="a_jobs.php"><!-- onclick="loadTemplateView('jobs')"-->

                                        <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
										<i class="fa fa-briefcase" aria-hidden="true"></i>
                                        <span>Jobs</span>
                                    </a>
                                </li>


                                <li id="nav_admin_adverts">
                                    <a href="a_adverts.php"><!--onclick="loadTemplateView('advertsMgmt')"-->

                                        <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                         <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span>Adverts Mgt</span>
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="a_marketSurvey.php"><!--onclick="loadTemplateView('mktSurvey')"-->

                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                        <span>Market Survey</span>
                                    </a>
                                </li>

                                <li id="nav_admin_reports">

                                    <a href="a_reports.php">

                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
                                    
                            <li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>

                                <li id="nav_admin_sysalerts">

                                    <a href="a_alerts.php"><!--onclick="loadTemplateView('sysAlerts')"-->

                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>

                                        <span>System Alerts</span>

                                    </a>

                                </li>

                                <li id="nav_admin_inbox">
                                    <a href="a_mailbox.php"><!--onclick="loadTemplateView('mail')"-->

                                        <span class="pull-right label label-primary admin-messages-count">0</span>
                                        
										<i class="fa fa-inbox" aria-hidden="true"></i>
                                        <span>Inbox</span>
										
                                    </a>
                                </li>

                                <li id="nav_admin_logout">

                                    <a href="index.php?logout">

                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                </li>

                            </ul>
                        </nav>

                        <hr class="separator">

                    </div>

                </div>

            </aside>

            <!-- end: sidebar -->

           <!-- right content -->
            <section role="main" class="content-body">
  
                <!-- /. loading clicked menu content wrapper --
                        <div id="content-view-div">
                            <?php # include "a_home.php"; ?>
                        </div>
                -- /. loading clicked menu content wrapper -->
                <!-- ========== /. breadcrumb header ========== -->
<div class="content-header">
  <h1>
    Dashboard
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</div>

<!-- ========== /. start of stats rows ========== -->
<div class="row">
<div class="col-md-3 col-lg-3 col-xs-12">                                
    <section class="panel panel-featured-left panel-featured-primary">
        <div class="panel-body">
            <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                    <div class="summary-icon bg-primary">
                        <i class="fa fa-life-ring"></i>
                    </div>
                </div>
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Service Providers</h4>
                        <div class="info">
                            <strong class="amount">1281</strong>
                        </div>
                    </div>
                    <div class="summary-footer">
                        <a class="text-muted text-uppercase">view</a>
                    </div>
                </div>
            </div>
        </div>
    </section>                                    
</div>

<div class="col-md-3 col-lg-3 col-xs-12">                        
    <section class="panel panel-featured-left panel-featured-secondary">
        <div class="panel-body">
            <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                    <div class="summary-icon bg-secondary">
                        <i class="fa fa-usd"></i>
                    </div>
                </div>
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Clients</h4>
                        <div class="info">
                            <strong class="amount">57</strong>
                        </div>
                    </div>
                    <div class="summary-footer">
                        <a class="text-muted text-uppercase">view</a>
                    </div>
                </div>
            </div>
        </div>
    </section>                                    
</div>                        

<div class="col-md-3 col-lg-3 col-xs-12">                        
    <section class="panel panel-featured-left panel-featured-tertiary">
        <div class="panel-body">
            <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                    <div class="summary-icon bg-tertiary">
                        <i class="fa fa-info-circle"></i>
                    </div>
                </div>
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Adverts</h4>
                        <div class="info">
                            <strong class="amount">38</strong>
                        </div>
                    </div>
                    <div class="summary-footer">
                        <a class="text-muted text-uppercase">view</a>
                    </div>
                </div>
            </div>
        </div>
    </section>                                    
</div>

<div class="col-md-3 col-lg-3 col-xs-12">                        
    <section class="panel panel-featured-left panel-featured-quartenary">
        <div class="panel-body">
            <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                    <div class="summary-icon bg-quartenary">
                        <i class="fa fa-money"></i>
                    </div>
                </div>
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Businesses</h4>
                        <div class="info">
                            <strong class="amount">89</strong>
                        </div>
                    </div>
                    <div class="summary-footer">
                        <a class="text-muted text-uppercase">view</a>
                    </div>
                </div>
            </div>
        </div>
    </section>                                    
</div>

</div>
<!-- row 2 -->

<!-- ========== /. end of stats rows ========== -->


<!-- ========== /. home content ========== -->
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>
                <h2 class="panel-title">Heading</h2>
                <p class="panel-subtitle">Sub-heading</a>.</p>
            </header>
            <div class="panel-body">


            </div>
        </section>
    </div>
    
</div>








            </section>
            <!-- /. content-body -->

    </div>
    <!-- /. inner-wrapper -->

</section>
<!-- /. body -->
 


<!-- /. require plugins and scripts -->				
<?php  include '_includes/_bottomJs.php';?>


				
				