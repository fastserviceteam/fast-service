<?php 
    // require global file
    require "_admin-config.php";

    // instantiate class
    $_adminObj = new AdminClass();
?>

<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title>Fast Service - Admin</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- top css file -->
    <?php include "_includes/_topCss.php";?>
</head>

<body>

    <section class="body" >

       
        <?php
            # include header ~ top-nav 
        require "_includes/_topNav.php";
        ?>

        <div class="inner-wrapper">
            <!-- start: sidebar -->

            <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">

                <div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">

                    <div class="sidebar-title">
                        FastService Corp
                    </div>
                    <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                       <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
                <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                    <div class="nano-content" >
                        <nav id="menu" class="nav-main" role="navigation" >
                            <ul class="nav nav-main" style="color:#607D8B;">
                                <li id="nav_admin_main">
                                    <a href="a_dashboard.php"><!--  onclick="loadTemplateView('main')" -->
                                         <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>

                                <li class="nav-active" id="nav_admin_businessmgmt">

                                    <a href="a_business.php"> <!--  onclick="loadTemplateView('businessMgmt')" -->

                                      <i class="fa fa-money" aria-hidden="true"></i>

                                        <span>Business Management</span>

                                    </a>

                                </li>


                                <li id="nav_admin_prodservices">

                                    <a href="a_categories.php?firstOpen"><!--onclick="loadTemplateView('prod-serv')"-->

                                        <i class="fa fa-globe" aria-hidden="true"></i>

                                        <span>Products and Services</span>

                                    </a>

                                </li>

                                <li id="nav_admin_billing">

                                    <a href="a_billing.php"><!--onclick="loadTemplateView('billing')"-->

                                        <i class="fa fa-exchange" aria-hidden="true"></i>

                                        <span>Billing</span>

                                    </a>

                                </li>

                                <li id="nav_admin_users">

                                    <a href="a_users.php"><!--onclick="loadTemplateView('users')"-->

                                        <i class="fa fa-users" aria-hidden="true"></i>

                                        <span id="usr">Human Resource</span>

                                    </a>

                                </li>


                                <li id="nav_admin_jobs">
                                    <a href="a_jobs.php"><!-- onclick="loadTemplateView('jobs')"-->

                                        <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
                                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        <span>Jobs</span>
                                    </a>
                                </li>


                                <li id="nav_admin_adverts">
                                    <a href="a_adverts.php"><!--onclick="loadTemplateView('advertsMgmt')"-->

                                        <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                         <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span>Adverts Mgt</span>
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="a_marketSurvey.php"><!--onclick="loadTemplateView('mktSurvey')"-->

                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                        <span>Market Survey</span>
                                    </a>
                                </li>

                                <li id="nav_admin_reports">

                                    <a href="a_reports.php">

                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
                                    
                            <li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>

                                <li id="nav_admin_sysalerts">

                                    <a href="a_alerts.php"><!--onclick="loadTemplateView('sysAlerts')"-->

                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>

                                        <span>System Alerts</span>

                                    </a>

                                </li>

                                <li id="nav_admin_inbox">
                                    <a href="a_mailbox.php"><!--onclick="loadTemplateView('mail')"-->

                                        <span class="pull-right label label-primary admin-messages-count">0</span>
                                        
                                        <i class="fa fa-inbox" aria-hidden="true"></i>
                                        <span>Inbox</span>
                                        
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="index.php?logout">

                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                </li>

                            </ul>
                        </nav>

                        <hr class="separator">

                    </div>

                </div>

            </aside>

            <!-- end: sidebar -->

           <!-- right content -->
            <section role="main" class="content-body">
    
<!-- /. title-->
<div class="content-header">
  <h1>
    Business
    <small>Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-money"></i> Home</a></li>
    <li class="active">Business</li>
  </ol>
</div>

<div class="row">

<!-- col-md-8 . left -->
<div class="col-md-8">

    <div class="panel-body">
        <div class="search-control-wrapper">
            <form action="pages-search-results.html">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" onkeyup="showResult(this.value)" placeholder="Start typing: search by business name">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button">Search</button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
	<!-- panel-body -->


    <div id="livesearch"></div>

    <br>

    <?php  if(isset($_REQUEST['activity'])){ 


$prof_id = $_REQUEST['activity']; 

$dt= "SELECT *from business_profile where business_profile_id = '{$prof_id}'";

$ex = $dbh->prepare($dt);

$ex->execute();

$vo = $ex->fetch( PDO::FETCH_ASSOC);


$own_email = $vo['Business_Email']; 


?>

    <div class="panel-body" style="background-color:#E0E0E0;" id="main_business_panel_body">

        <div class="col-md-12 col-lg-12 col-xl-3">

            <section class="panel panel-featured-bottom panel-featured-tertiary">
                <div class="panel-body">
                    <div class="widget-summary widget-summary-md">


                        <center>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"><b>
                                            <font size="3vw"><?php echo $vo['Business_Name'] ?></font>
                                        </b></h4>
                                    <div class="info">
                                        <strong class="amount">
                                            <font size="2.5vw">Located at :</font>
                                        </strong>
                                        <span class="text-primary">(<?php  echo $vo['Business_Address'] ?>)</span>

                                        &nbsp; &nbsp;<strong class="amount">
                                            <font size="2.5vw">Contact No :</font>
                                        </strong>

                                        <span class="text-primary">(<?php  echo $vo['Business_Phone'] ?>)</span>


                                    </div>
                                </div>

                            </div>
                        </center>
                    </div>
                </div>
            </section>
        </div>
		<!-- /. col-md-12 col-lg-12 col-xl-3 -->


        <!-- displaying the registration certificate -->
        <script>
            function showBizProf() {


                $("#bizCertificate").fadeIn(2500);

                $("#hideReg").hide(1000);
                $("#hidePerform").hide(1000);
                $("#hideServ").hide(1000);
                $("#hideOwn").hide(1000);
                $("#hideTrans").hide(1000);

            }

            //closing the prof div
            function shutdiv() {

                $("#bizCertificate").hide(1000);

                $("#hideReg").show(1000);
                $("#hidePerform").show(1000);
                $("#hideServ").show(1000);
                $("#hideOwn").show(1000);
                $("#hideTrans").show(1000);

            }

            // showing ownership details

            function showOwnerProf() {


                $("#bizOwner").fadeIn(2500);

                $("#hideReg").hide(1000);
                $("#hidePerform").hide(1000);
                $("#hideServ").hide(1000);
                $("#hideOwn").hide(1000);
                $("#hideTrans").hide(1000);

            }


            //closing the prof div
            function shutOwner() {

                $("#bizOwner").hide(1000);

                $("#hideReg").show(1000);
                $("#hidePerform").show(1000);
                $("#hideServ").show(1000);
                $("#hideOwn").show(1000);
                $("#hideTrans").show(1000);

            }
        </script>



        <div class="col-md-12 col-lg-12 col-xl-3" style="display:none;" id="bizCertificate">


            <section class="panel panel-featured-bottom panel-featured-default">
                <div class="panel-body">


                    <span onclick="shutdiv()" class="badge" style="background-color:#FFA726;cursor:pointer;">x</span>
                    <center>



                        <p style="text-transform:uppercase;font-size:1.2vw;">
                            <font color="#000"><b><?php echo $vo['Business_Name']; ?></b></font>
                        </p>

                        <p style="text-transform:lowercase;font-size:1vw;">
                            <font color="#757575"><b>(Incorporated under FastService App Act 14)</b></font>
                        </p>

                        <p style="text-transform:lowercase;font-size:1vw;">
                            <font color="#757575"><b>Official location: <?php echo $vo['Business_Address']; ?> <?php ?></b></font>
                        </p>

                        <hr>

                        <p style="text-transform:uppercase;font-size:1.2vw;">
                            <font color="#00BCD4"><b> DIGITAL CERTIFICATE</b></font>
                        </p>



                        <p style="font-size:0.9vw;">
                            <font color="#424242">


                                This is to certify that the the fore-mentioned business in the registered holders bearing the distinctive business number his full registered with

                                FastService Web App. </font>
                        </p>


                        <table class="table" border="0px;">

                            <tr>
                                <td> <b>Registration No:&nbsp;&nbsp;</b>
                                    <font color="#00BCD4"><?php echo $vo['business_profile_id']; ?> </font>
                                </td>



                                <td> <b>Registered on:</b>
                                    <font color="#00BCD4">14-Dec-2019 </font>
                                </td>
                            </tr>

                            <tr>
                                <td> <b>Name of the Shareholder:&nbsp;&nbsp;</b>
                                    <font color="#00BCD4">.................... </font>
                                </td>

                                <td> <b>Business Industry:</b>
                                    <font color="#00BCD4"><?php echo $vo['Business_Category']; ?> </font>
                                </td>
                            </tr>

                            <tr>
                                <td> <b>Operation Area:&nbsp;&nbsp;</b>
                                    <font color="#00BCD4">.................... </font>
                                </td>
                                <td></td>
                            </tr>


                            <tr>
                                <td> <b>Business Contact:</b>
                                    <font color="#00BCD4"> <?php echo $vo['Business_Phone']; ?> </font>
                                </td>

                                <td> <b>Email:&nbsp;&nbsp;</b>
                                    <font color="#00BCD4"><?php echo $vo['Business_Email']; ?></font>
                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td> </td>
                            </tr>

                            <tr>
                                <td colspan="2">Given under is the Common seal of Fast Service App Incorporation</td>
                            </tr>


                            <tr>
                                <td></td>
                                <td>
                                    <p>
                                        <font color="#000"><b><i>Project Manager</i></b></font>
                                    </p>


                                    <font color="#000"><b><i>Mr Ainebyona Timothy</i></b></font>
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td> <img src="../../images/logo.jpg" class="img-circle" height="35" style="border:1px solid #00838F;" /> </td>
                            </tr>




                        </table>

                    </center>
                </div>
            </section>
        </div>
		<!-- /. col-md-12 col-lg-12 col-xl-3 . bizCertificate -->


        <?php $neq = "SELECT *from user_profile where ur_email = '{$own_email}'";

$exg = $dbh->prepare($neq);

$exg->execute();

$po = $exg->fetch( PDO::FETCH_ASSOC);

?>

        <div class="col-md-12 col-lg-12 col-xl-3" style="display:none;" id="bizOwner">


            <section class="panel panel-featured-bottom panel-featured-default">
                <div class="panel-body">


                    <span onclick="shutOwner()" class="badge" style="background-color:#FFA726;cursor:pointer;">x</span>
                    <center>


                        <div class="pricing-table">


                            <div class="col-lg-12 col-sm-6">
                                <div class="plan">
                                    <h3> Registered by <?php echo $po['names'] ?><span><img src="../../images/logo.jpg" height="35" style="border:1px solid #00838F;" class="img-circle"></span></h3>

                                    <a class="btn btn-lg btn-warning" href="#"><?php echo $po['ur_email'] ?></a>
                                    <ul>
                                        <li><b>Gender:&nbsp;&nbsp;</b> <?php echo $po['gender'] ?></li>

                                        <li><b>Date of birth:&nbsp;&nbsp;</b> <?php echo $po['date_of_birth'] ?></li>

                                        <li><b>Nationality:&nbsp;&nbsp;</b> <?php echo $po['nationality'] ?></li>

                                        <li><b>Current Address:&nbsp;&nbsp;</b> <?php echo $po['current_address'] ?></li>

                                        <li><b>Personal Contact:&nbsp;&nbsp;</b> <?php echo $po['phone_no'] ?></li>

                                    </ul>
                                </div>
                            </div>

                        </div>






                </div>
            </section>
        </div>
		<!-- bizOwner -->



        <div class="col-md-6 col-lg-6 col-xl-3" id="hideReg">

            <section class="panel panel-featured-left panel-featured-primary">
                <div class="panel-body">
                    <div class="widget-summary widget-summary-md">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-tertiary">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><b>Registration</b></h4>
                                <div class="info">
                                    <strong class="amount">
                                        <font size="2.4vw">Started on</font>
                                    </strong>
                                    <span class="text-primary">(14-Dec-2019) &nbsp;<span class="fa fa-eye" style="cursor:pointer;font-size:1.5vw;" onclick="showBizProf()"></span></span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted">(View reg form)</a>
                            </div>
                        </div>
                    </div>
				</div>
            </section>

        </div>
		<!-- hideReg -->




        <div class="col-md-6 col-lg-6 col-xl-3" id="hideTrans">

            <section class="panel panel-featured-left panel-featured-primary">
                <div class="panel-body">
                    <div class="widget-summary widget-summary-md">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-tertiary">
                                <i class="fa fa-money"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><b>Transaction History</b></h4>
                                <div class="info">
                                    <strong class="amount">
                                        <font size="2.5vw">Count</font>
                                    </strong>
                                    <span class="text-primary">(14 unread)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted">(View reg form)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
		<!-- /. hideTrans -->




        <div class="col-md-6 col-lg-6 col-xl-3" id="hidePerform">

            <section class="panel panel-featured-left panel-featured-primary">
                <div class="panel-body">
                    <div class="widget-summary widget-summary-md">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-tertiary">
                                <i class="fa fa-signal"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><b>Performance Details</b></h4>
                                <div class="info">
                                    <strong class="amount">
                                        <font size="2.4vw">Monthly and </font>
                                    </strong>
                                    <span class="text-primary">(Annual)</span>
                                </div>
                            </div>

                            <div class="summary-footer">

                                <a class="text-muted">(View reg form)</a>
                            </div>
                        </div>
                    </div>
				</div>
            </section>
        </div>
		<!-- hidePerform -->



        <div class="col-md-6 col-lg-6 col-xl-3" id="hideServ">

            <section class="panel panel-featured-left panel-featured-primary">
                <div class="panel-body">
                    <div class="widget-summary widget-summary-md">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-tertiary">
                                <i class="fa fa-tasks"></i>
                            </div>
                        </div>

                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><b>Service Actions</b></h4>
                                <div class="info">
                                    <strong class="amount">
                                        <font size="2.5vw">Adverts:</font>
                                    </strong>
                                    <span class="text-primary">(Jobs:)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted">(View reg form)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
		<!-- hideServ -->


        <div class="col-md-12 col-lg-12 col-xl-3" id="hideOwn">

            <section class="panel panel-featured-left panel-featured-primary">
                <div class="panel-body">
                    <div class="widget-summary widget-summary-md">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-tertiary">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>

                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><b>Ownership details</b></h4>
                                <div class="info">
                                    <strong class="amount">
                                        <font size="2.5vw">Legal Info and :</font>
                                    </strong>
                                    <span class="text-primary">(Owner details) &nbsp;<span class="fa fa-eye" style="cursor:pointer;font-size:1.5vw;" onclick="showOwnerProf()"></span> </span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted">(View reg form)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
		<!-- hideOwn -->



    </div>
	<!-- /. main_business_panel_body -->

    <?php 	}



?>

</div>
<!-- col-md-8 . left -->


	<!-- col-md-4 . right -->
	<div class="col-md-4">

		<div class="panel-body">

			<ul class="list-group">

				<li class="list-group-item" style="background-color:#E0F7FA;color:#000;">
					<center> <b>Industries &amp; number of business </b></center>
				</li>

				<?php  $des = "SELECT *FROM  industries ORDER BY industry_name ASC";

	$eds = $dbh->prepare($des);

	$eds->execute();

	$ques = $eds->fetchAll(PDO::FETCH_ASSOC);

	foreach($ques as $nox){
	?>

				<li class="list-group-item" style="font-family:constantia;"><?php echo $nox['industry_name'] ?> <span class="badge">5</span></li>

				<?php } ?>

			</ul>

		</div>
	</div>
	<!-- col-md-4 . right -->

</div>
<!-- row -->



            </section>
            <!-- /. content-body -->

    </div>
    <!-- /. inner-wrapper -->

</section>
<!-- /. body -->
 


<!-- /. require plugins and scripts -->             
<?php  include '_includes/_bottomJs.php';?>