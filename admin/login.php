<?php
// require global file
require "_admin-config.php";
// instantiate class
$_adminObj = new AdminClass();
if($_adminObj->_isLoggedIn() == true)
{
	
header("Location: a_dashboard.php");
}
?>
<!doctype html>
<html class="fixed">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>Fast Service - Admin</title>
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- top css file -->
		<?php include "_includes/_topCss.php";?>
		<style>
		#login-logo-title span{
		font-size: 20px;
		font-weight: 700;
		}
		#login-logo-title:hover,
		#login-logo-title
		{
		text-decoration: none;
		}
		</style>
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body style="background-image:url(../_assets/img/home.jpg);background-repeat:no-repeat;background-size:100%;">
		<!-- start: page -->
		
		<section class="body-sign">
			
			<div class="center-sign">
				<a href="" class="logo pull-left" id="login-logo-title">
					<img src="<?php echo ff_logo; ?>" height="54" alt="Fastservice-logo" />
					<span class="white-text">Fast Service</span>
				</a>
				<div class="panel panel-sign">
					
					<div class="panel-title-sign mt-xl text-right" >
						
						<h2 class="title text-uppercase text-bold m-none" style="background-color:#2E7D32;"><i class="fa fa-user-secret mr-xs" ></i>LOGIN HERE</h2>
					</div>
					<div class="panel-body">
						
						<div class="span-feedbacks text-center"></div>
						
						<form role="form" id="fast_service_admin_login">
							
							<div class="form-group mb-md">
								<label for="fastservice_data_email">Email Address</label>
								<div class="input-group input-group-icon">
									<input type="email" class="form-control input-md" placeholder="Email" id="fastservice_data_email" name="fastservice_data_email" autocomplete="off" required>
									<span class="input-group-addon">
										<span class="icon icon-md">
											<i class="fa fa-envelope"></i>
										</span>
									</span>
								</div>
							</div>
							
							<div class="form-group mb-md">
								<label for="fastservice_data_priviledge">Priviledge</label>
								<div class="">
									<select class="form-control input-md" name="fastservice_data_priviledge" id="fastservice_data_priviledge" required>
										<option selected disabled> --- select priviledge --- </option>
										<option value="Admin">Admin</option>
										<option value="Guest">Guest</option>
									</select>
								</div>
							</div>
							<div class="form-group mb-md">
								<div class="clearfix">
									<label class="pull-left"  for="fastservice_data_password">Password</label>
									<a href="#" class="pull-right">Lost Password?</a>
								</div>
								<div class="input-group input-group-icon">
									<input type="password" placeholder="Password" class="form-control input-md"  id="fastservice_data_password" name="fastservice_data_password" autocomplete="off" required>
									<span class="input-group-addon">
										<span class="icon icon-md">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="RememberMe" name="fastservice_data_remember_me" type="checkbox"/>
										<label for="RememberMe">Remember Me</label>
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn blue-gradient-rgba waves-effect white-text btn-flat btn-block" id="fastserviceLoginBtn">Sign In</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<p class="text-center text-muted mt-md mb-md"><font color="white">&copy; Copyright <?php echo date('Y');?>. All rights reserved. <a href="#"><font color="#FFF"><b>Fast Service</b></font></a>.</font></p>
			</div>
		</section>
		<!-- end: page -->
		<!-- login specific scripts -->
		<script src="_assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script src="_assets/plugins/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="_assets/plugins/bootstrap/js/bootstrap.js"></script>
		<script src="_assets/plugins/nanoscroller/nanoscroller.js"></script>
		<script src="_assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="_assets/plugins/magnific-popup/magnific-popup.js"></script>
		<script src="_assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="_assets/js/theme.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="_assets/js/theme.init.js"></script>
		<!-- login -->
		<script src="_assets/js/fastservice-script.js"></script>
	</body>
	
</html>