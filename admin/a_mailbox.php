<?php
// require global file

require "_admin-config.php";

// instantiate class
$_adminObj = new AdminClass();
/*
$run = "SELECT * FROM msg_inbox";
$exeb = $dbh->prepare($run);
$exeb->execute();
$roq = $exeb->fetchAll(PDO::FETCH_ASSOC);
*/
$ruq = "SELECT * FROM msg_outbox";
$exeq = $dbh->prepare($ruq);
$exeq->execute();
$rom = $exeq->fetchAll(PDO::FETCH_ASSOC);

$ruk = "SELECT * FROM jobs WHERE approved =''";
$exet = $dbh->prepare($ruk);
$exet->execute();
$ros = $exet->fetchAll(PDO::FETCH_ASSOC);

?>


<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title>Fast Service - Admin</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- top css file -->
    <?php include "_includes/_topCss.php";?>
</head>

<body>

    <section class="body" >

       
        <?php
            # include header ~ top-nav 
        require "_includes/_topNav.php";
        ?>

        <div class="inner-wrapper">
            <!-- start: sidebar -->

            <aside id="sidebar-left" class="sidebar-left" style="background-color:#ECEFF1;color:#000;">

                <div class="sidebar-header"style="border-bottom:1px solid #CFD8DC;">

                    <div class="sidebar-title">
                        FastService Corp
                    </div>
                    <div class="sidebar-toggle hidden-xs" style="background-color:#00BCD4;color:#FFF;border-right:1px solid #80CBC4" data-toggle-class="sidebar-left-collapsed " data-target="html" data-fire-event="sidebar-left-toggle">
                       <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
                <div class="nano" style="background-color:#ECEFF1;border-right:2px solid #00BCD4">
                    <div class="nano-content" >
                        <nav id="menu" class="nav-main" role="navigation" >
                            <ul class="nav nav-main" style="color:#607D8B;">
                                <li id="nav_admin_main">
                                    <a href="a_dashboard.php"><!--  onclick="loadTemplateView('main')" -->
                                         <i class="fa fa-dashboard" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>

                                <li id="nav_admin_businessmgmt">

                                    <a href="a_business.php"> <!--  onclick="loadTemplateView('businessMgmt')" -->

                                      <i class="fa fa-money" aria-hidden="true"></i>

                                        <span>Business Management</span>

                                    </a>

                                </li>


                                <li id="nav_admin_prodservices">

                                    <a href="a_categories.php?firstOpen"><!--onclick="loadTemplateView('prod-serv')"-->

                                        <i class="fa fa-globe" aria-hidden="true"></i>

                                        <span>Products and Services</span>

                                    </a>

                                </li>

                                <li id="nav_admin_billing">

                                    <a href="a_billing.php"><!--onclick="loadTemplateView('billing')"-->

                                        <i class="fa fa-exchange" aria-hidden="true"></i>

                                        <span>Billing</span>

                                    </a>

                                </li>

                                <li id="nav_admin_users">

                                    <a href="a_users.php"><!--onclick="loadTemplateView('users')"-->

										<i class="fa fa-users" aria-hidden="true"></i>

                                        <span id="usr">Human Resource</span>

                                    </a>

                                </li>


                                <li id="nav_admin_jobs">
                                    <a href="a_jobs.php"><!-- onclick="loadTemplateView('jobs')"-->

                                        <span class="pull-right label label-primary" id="jobs_nav_counter">0</span>
										<i class="fa fa-briefcase" aria-hidden="true"></i>
                                        <span>Jobs</span>
                                    </a>
                                </li>


                                <li id="nav_admin_adverts">
                                    <a href="a_adverts.php"><!--onclick="loadTemplateView('advertsMgmt')"-->

                                        <span class="pull-right label label-primary" id="adverts_nav_count">0</span>
                                         <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span>Adverts Mgt</span>
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="a_marketSurvey.php"><!--onclick="loadTemplateView('mktSurvey')"-->

                                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                                        <span>Market Survey</span>
                                    </a>
                                </li>

                                <li id="nav_admin_reports">

                                    <a href="a_reports.php">

                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>
                                    
                            <li class="li-header" style="background-color:#00BCD4;"><b class="text-white">OTHERS</b></li>

                                <li id="nav_admin_sysalerts">

                                    <a href="a_alerts.php"><!--onclick="loadTemplateView('sysAlerts')"-->

                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>

                                        <span>System Alerts</span>

                                    </a>

                                </li>

                                <li class="nav-active" id="nav_admin_inbox">
                                    <a href="a_mailbox.php"><!--onclick="loadTemplateView('mail')"-->

                                        <span class="pull-right label label-primary admin-messages-count">0</span>
                                        
										<i class="fa fa-inbox" aria-hidden="true"></i>
                                        <span>Inbox</span>
										
                                    </a>
                                </li>

                                <li id="nav_admin_market_survey">

                                    <a href="index.php?logout">

                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                </li>

                            </ul>
                        </nav>

                        <hr class="separator">

                    </div>

                </div>

            </aside>

            <!-- end: sidebar -->

           <!-- right content -->
            <section role="main" class="content-body">



<!-- ========== /. breadcrumb header ========== -->
<div class="content-header">
	<h1>
	Inbox
	<small>Messages</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-inbox"></i> Home</a></li>
		<li class="active">Inbox</li>
	</ol>
</div>

<!-- ========== /. messages content starts ========== -->
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs nav-justified">
			<li class="active">
				<a href="#mail1" data-toggle="tab" class="text-center"><i class="fa fa-envelope"></i> Inbox <span class="badge" style="background-color:#FF6F00;"><?php echo $_adminObj->_messagesBadgeCounter(); ?></span></a>
			</li>
			<li>
				<a href="#mail2" data-toggle="tab" class="text-center"><i class="fa fa-paper-plane"></i>&nbsp;Sent Messages  <span class="badge" style="background-color:#FFC107;"><?php echo $exeq->rowCount(); ?></span></a>
			</li>
			<li>
				<a href="#mail3" data-toggle="tab" class="text-center"><i class="fa fa-graduation-cap"></i>&nbsp;Job Requests <span class="badge" style="background-color:#FF8F00;"><?php echo $exet->rowCount(); ?></span></a>
			</li>
			<li>
				<a href="#mail4" data-toggle="tab" class="text-center"><i class="fa fa-bell"></i>&nbsp;Notifications and Alerts <span class="badge" style="background-color:#FFA000;">3</span></a>
			</li>
		</ul>
		<div class="tabs tabs-bottom tabs-primary">
			<div class="tab-content">
				<!-- pane for viewing main messages -->
				<div id="mail1" class="tab-pane active">
				<?php 
					$inbox = $_adminObj->_messagesSection();
					
					if($inbox) {
				?>
					<table class="table">
						<thead>
							<tr>
								<th>Sender</th>
								<th>Subject</th>
								<th>Last modified</th>
								<th>Msg Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($inbox as $rov){ 
							    $msgDateObj = new DateTime($rov['inbox_date_time']);
								$msddate    =  $msgDateObj->format('d M, Y - h:i a');
							?>
							<tr>
								<td><i class="fa fa-star"></i> <?php echo $rov['sender_names'] ?></td>
								<td> 
									<a class="modal-with-zoom-anim" style="cursor:pointer;color:#000;" 
									data-msgSubject="<?php echo $rov['inbox_subject']; ?>" 
									data-msgSender="<?php echo $rov['sender_names']; ?>" 
									data-msgDate="<?php echo $msddate; ?>" 
									data-msgText="<?php echo $rov['inbox_message']; ?>" 
									data-toggle="modal" data-target="#zoomOutIn6" onclick="showMsg(this)">
									<?php echo $rov['inbox_subject'] ?>
									</a> 
								</td>
								<td>
									<?php echo $msddate; ?>
								</td>
								<td> 
									<i class="fa fa-reply" title="reply message" style="cursor:pointer;" data-receiver="<?php echo $rov['inbox_email'] ?>" data-msgid="<?php echo $rov['inbox_id'] ?>" data-toggle="modal" data-target="#zoomOutIn5" onclick="showReceiver(this)">&nbsp;</i>
									<i class="fa fa-trash-o" style="cursor:pointer;" title="delete Message" data-desc="<?php echo $rov['inbox_message'] ?>"  data-msgid="<?php echo $rov['inbox_id'] ?>" data-toggle="modal" data-target="#zoomOutIn4" onclick="getMsgid(this)">&nbsp;</i>
									<i class="fa fa-envelope-o" title="mark as read" style="cursor:pointer;">&nbsp;</i>
								</td>
							</tr>
							<?php  } ?>
						</tbody>
					</table>
					<?php } ?>
				</div>
				<!-- end of pane for main messages -->
				<!-- pane for viewing sent messages -->
				<div id="mail2" class="tab-pane">
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Subject</th>
								<th>Sent On</th>
								<th>Msg Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($rom as $rox){ ?>
							<tr>
								<td><i class="fa fa-star"></i> <?php echo $rox['sender_names'] ?></td>
								<td> 
									<a class=" modal-with-zoom-anim" style="cursor:pointer;color:#000;" 
										data-msgSubject="<?php echo $rov['outbox_subject'] ?>" 
										data-msgSender="<?php echo $rov['sender_names'] ?>" 
										data-msgDate="<?php echo $rov['outbox_date_time'] ?>" 
										data-msgText="<?php echo $rov['outbox_message'] ?>" href="#modalAnim" onclick="showMsg(this)">
										<?php echo $rox['outbox_subject'] ?>
									</a> 
								</td>
								<td><?php echo $rox['reply_date'] ?></td>
								<td>
									<i class="fa fa-trash-o" style="cursor:pointer;" title="delete Message" data-desc="<?php echo $rox['sent_text'] ?>"  data-msgid="<?php echo $rox['outbox_id'] ?>" data-toggle="modal" data-target="#zoomOutIn4" onclick="getMsgid(this)">&nbsp;</i>
									<i class="fa fa-envelope-o" title="mark as read" style="cursor:pointer;">&nbsp;</i>
								</td>
							</tr>
							<?php  } ?>
						</tbody>
					</table>
				</div>
				<!-- Pane for viewing sent jobs to post -->
				<div id="mail3" class="tab-pane">
					<table class="table">
						<thead>
							<tr>
								<th>Company Name</th>
								<th>Job title</th>
								<th>To posted on</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($ros as $rob){
							$biz_id = $rob['business_profile_id'];
							$sqm = "SELECT * FROM business_profile WHERE business_profile_id = '{$biz_id}'";
							$exem = $dbh->prepare($sqm);
							$exem->execute();
							$mox = $exem->fetch(PDO::FETCH_ASSOC);
							?>
							<tr>
								<td><i class="fa fa-star"></i> <?php echo $mox['Business_Name'] ?></td>
								<td><?php echo $rob['job_vacancy'] ?></td>
								<td><?php echo $rob['tobe_posted'] ?></td>
								<td>
									<i class="fa fa-reply" title="reply message" style="cursor:pointer;">&nbsp;</i>
									<i class="fa fa-trash-o" style="cursor:pointer;" title="delete Message" data-desc="<?php echo $rox['sent_text'] ?>"  data-msgid="<?php echo $rox['message_id'] ?>" data-toggle="modal" data-target="#zoomOutIn4" onclick="getMsgid(this)">&nbsp;</i>
								<i class="fa fa-envelope-o" title="mark as read" style="cursor:pointer;">&nbsp;</i></td>
							</tr>
							<?php  } ?>
						</tbody>
					</table>
				</div>
				<div id="mail4" class="tab-pane">
					<p class="text-info text-center"><i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Loading </p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- row -->

<?php
if(isset($_REQUEST['mailSent'])){
?>
<body onload="launch_toast()"></body>
<div id="toast"><div id="img"><li class="fa fa-envelope"></li></div><div id="desc">Message sent successful....</div></div>
<?php } ?>
<script>
//notification after sending an email
function launch_toast() {
var x = document.getElementById("toast")
x.className = "show";
setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}
</script>
<!-- Modal Animation  for viewing message -->
<div class="modal fade " id="zoomOutIn6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><center><b><font color="#00ACC1">Message from <font id="msgSender"></font></font></b></center></h4>
		</div>
		<div class="modal-body" style="padding:3em;">
			<p id="msgText"></p>
			<script>
			</script>
		</div>
		<div class="modal-footer">
			Sent on <font color="#424242" id="msgDate"></font>&nbsp;&nbsp;&nbsp;
			<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
		</div>
	</div>
</div>
</div>
<!-- modal for deleting -->
<div class="modal fade " id="zoomOutIn4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><center><b><font color="#00ACC1">Are you sure you want to delete this message?</font></b></center></h4>
		</div>
		<div class="modal-body" style="padding:3em;">
			<div id="afterMsgDel"></div>
			<form  id="msgdelete_form">
				<input id="msgId" type="hidden" name="message_id" value="">
				<div id="msgText"></div>
				<script>
				function getMsgid(zoomOutIn4) {
				var msg_id = zoomOutIn4.getAttribute("data-msgid");
				var msg_text = zoomOutIn4.getAttribute("data-desc");
				document.getElementById("msgId").value = msg_id;
				document.getElementById("msgText").innerHTML = msg_text;
				} </script>
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-warning" onclick="delMsg()">Yes</button> </form>
			<button type="button" class="btn btn-info" data-dismiss="modal">No</button>
		</div>
	</div>
</div>
</div>
<!-- modal for send a reply to an email -->
<div class="modal fade  " id="zoomOutIn5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><center><b><font color="#00ACC1"><li class="fa fa-pencil"></li>&nbsp;Compose</font></b></center></h4>
		</div>
		<div class="modal-body" style="padding:1em;">
			<div id="afterMail"></div>
			<form id="repMail_form">
				<p><div class="form-group">
					<label>Sending to:</label>
					<input type="text"  id="msgReceiver" name="receiverMail" value="" class="form-control">
					<input type="hidden"  id="msgeid" name="msg_id" value="" class="form-control">
					<input type="hidden"  name="reply_date" value="<?php echo date('Y-m-d') ?>" class="form-control">
				</div>	</p>
				<textarea class="form-control" rows="10" name="replyMsg" placeholder="Type your reply here" required></textarea>
			</div>
			<div class="modal-footer">
				<button type="submit" onclick="openRepMail()" class="btn btn-info"><li class="fa fa-envelope"></li>&nbsp;Send Mail</button> </form>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

            </section>
            <!-- /. content-body -->

    </div>
    <!-- /. inner-wrapper -->

</section>
<!-- /. body -->
 


<!-- /. require plugins and scripts -->				
<?php  include '_includes/_bottomJs.php';?>
